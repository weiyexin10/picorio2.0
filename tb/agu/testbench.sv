module testbench
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import riscv_pkg::*;
(

);

    logic clk;
    logic rst;
    logic deque_en;

    initial begin
        $vcdpluson();
        // $fsdbDumpvars(0, testbench);
    end
    
    initial begin
        clk = 0;
        forever begin
            #1;
            clk = 1;
            #1;
            clk = 0;
        end
    end

    logic                          issue_vld_i;
    logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i;
    logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i;
    logic [      LSU_OP_WIDTH-1:0] issue_opcode_i;
    logic [              XLEN-1:0] issue_operand0_i;
    logic [              XLEN-1:0] issue_operand1_i;
    logic                          issue_rdy_o;

    logic                          wb_vld_o;
    logic [     ROB_TAG_WIDTH-1:0] wb_rob_tag_o;
    logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o;
    logic [              XLEN-1:0] wb_data_o;
    logic                          wb_rdy_i;
    logic [      LSU_OP_WIDTH-1:0] wb_opcode_o;
    logic                          wb_fu_type_o;
    logic [       VADDR_WIDTH-1:0] wb_address_o;
    logic                          wb_excp_vld_o;

    logic                          flush_i;


    rvh_agu agu_u(
        // Issue
        .issue_vld_i,
        .issue_rob_tag_i,
        .issue_fu_type_i(1'b0),
        .issue_opcode_i,
        .issue_phy_rd_i,
        .issue_operand0_i,
        .issue_imm_i(1'b0),
        .issue_rdy_o,

        // Write Back
        .wb_vld_o,
        .wb_rob_tag_o,
        .wb_opcode_o,
        .wb_phy_rd_o,
        .wb_fu_type_o,
        .wb_address_o,
        .wb_excp_vld_o,
        .wb_rdy_i,

        .flush_i,

        .clk,
        .rst
    );

endmodule

