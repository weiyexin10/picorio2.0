#include "rrvtb.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <pthread.h>
#include "rvh_core.hpp"
#include <queue>
#include <array>
using namespace std;



rvh_core *dut;
Dromajo *model;
ofstream rpt_f;


pthread_t model_thread, idle_thread;

const uint64_t max_cycle = 10000000;

void *model_fn(void *arg)
{
    uint64_t cycle = max_cycle;

    dut->Reset();
    while (1 <-- cycle)
    {
        dut->try_exit(rpt_f);
        dut->Evaluate();
        dut->Advance();
    }
    fprintf(stderr,"TIMEOUT\n");
    if (rpt_f.is_open()) rpt_f << "TIMEOUT" << endl;
    finish();
	return((void *)0);
}

initial(initial_func)
{
    char *file_name;
    char *rpt_file_name;

    if (!(file_name = (char*)scan_plusargs("image=")))
    {
        ERROR("no image input!\n");
    }
    if (rpt_file_name = (char*)scan_plusargs("report="))
    {
        rpt_f.open(rpt_file_name, ios::app);
    }

    dut = new rvh_core(file_name,0x80000000);
    model = new Dromajo(file_name,0x80000000);
    int err = pthread_create(&model_thread, NULL, &model_fn, NULL);
	if (err != 0)
		ERROR("Can't create model thread\n");
    sleep(0);
}

