module testbench
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_l1d_pkg::*;
(
    input logic [       XLEN-1:0] hart_id_i,
    input logic [VADDR_WIDTH-1:0] boot_address_i,
    input logic                   rst

);

  localparam int unsigned CLK_PERIOD = 10;

  logic clk;

  initial begin
    int dumpon = 1;
    int vcdplus = 0;
    $value$plusargs("dumpon=%d", dumpon);
    $value$plusargs("vcdplus=%d", vcdplus);

    if (dumpon > 0) begin
      $fsdbDumpvars(0, testbench);
      $fsdbDumpvars("+struct");
      $fsdbDumpvars("+mda");
      $fsdbDumpvars("+all");
      $fsdbDumpon;
    end
    if (vcdplus > 0) begin
      $vcdpluson();
    end
  end

  initial begin : generate_clk
    forever begin
      #(CLK_PERIOD / 2) clk = 1'b0;
      #(CLK_PERIOD / 2) clk = 1'b1;
    end
  end

  logic               [2-1:0]       l1_l2_arvalid;
  logic               [2-1:0]       l1_l2_arready;
  cache_mem_if_ar_t   [2-1:0]       l1_l2_ar;

  logic               [2-1:0]       l1_l2_rvalid;
  logic               [2-1:0]       l1_l2_rready;
  cache_mem_if_r_t    [2-1:0]       l1_l2_r;

  logic               [2-1:0]       l1_l2_awvalid;
  logic               [2-1:0]       l1_l2_awready;
  cache_mem_if_aw_t   [2-1:0]       l1_l2_aw;

  logic               [2-1:0]       l1_l2_wvalid;
  logic               [2-1:0]       l1_l2_wready;
  cache_mem_if_w_t    [2-1:0]       l1_l2_w;

  logic               [2-1:0]       l1_l2_bvalid;
  logic               [2-1:0]       l1_l2_bready;
  cache_mem_if_b_t    [2-1:0]       l1_l2_b;


  logic              l1_l2_arvalid_from_arb;
  logic              l1_l2_arready_from_arb;
  cache_mem_if_ar_t  l1_l2_ar_from_arb;

  logic              l1_l2_rvalid_from_arb;
  logic              l1_l2_rready_from_arb;
  cache_mem_if_r_t   l1_l2_r_from_arb;

  logic              l1_l2_awvalid_from_arb;
  logic              l1_l2_awready_from_arb;
  cache_mem_if_aw_t  l1_l2_aw_from_arb;

  logic              l1_l2_wvalid_from_arb;
  logic              l1_l2_wready_from_arb;
  cache_mem_if_w_t   l1_l2_w_from_arb;

  logic              l1_l2_bvalid_from_arb;
  logic              l1_l2_bready_from_arb;
  cache_mem_if_b_t   l1_l2_b_from_arb;

///////////////////////////////////////////////////////////////////////////////////////////////////////

// CSR Status
  logic [     PRIV_LVL_WIDTH-1:0]                          priv_lvl;
  logic                                                    tvm;
  logic                                                    tw;
  logic                                                    tsr;
  logic [               XLEN-1:0]                          mstatus;
  logic [               XLEN-1:0]                          satp;

  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_vld;
  logic [       DECODE_WIDTH-1:0][                   31:0] ifu_dec_inst;
  logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] ifu_dec_inst_pc;
  logic [       DECODE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] ifu_dec_inst_btq_tag;
  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_pred_taken;
  logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] ifu_dec_inst_pred_target;
  logic [    EXCP_TVAL_WIDTH-1:0]                          ifu_dec_inst_excp_tval;
  logic                                                    ifu_dec_inst_excp_vld;
  logic [   EXCP_CAUSE_WIDTH-1:0]                          ifu_dec_inst_excp_cause;
  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_rdy;

  // Decoder -> Rename
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_vld;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_is_rvc;
  logic [       RENAME_WIDTH-1:0][        VADDR_WIDTH-1:0] dec_rn_inst_pc;
  logic [       RENAME_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] dec_rn_inst_btq_tag;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_pred_taken;
  logic [       RENAME_WIDTH-1:0][        VADDR_WIDTH-1:0] dec_rn_inst_pred_target;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_excp_vld;
  logic [       RENAME_WIDTH-1:0][     MAJOR_OP_WIDTH-1:0] dec_rn_inst_major_op;
  logic [       RENAME_WIDTH-1:0][     MINOR_OP_WIDTH-1:0] dec_rn_inst_minor_op;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_use_rs1;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_use_rs2;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_use_rd;
  logic [       RENAME_WIDTH-1:0][  ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rs1;
  logic [       RENAME_WIDTH-1:0][  ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rs2;
  logic [       RENAME_WIDTH-1:0][  ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rd;
  logic [       RENAME_WIDTH-1:0][                   31:0] dec_rn_inst_imm;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_use_imm;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_alu_use_pc;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_is_fence;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_control_flow;
  // Exception
  logic [    EXCP_TVAL_WIDTH-1:0]                          dec_rn_inst_excp_tval;
  logic [   EXCP_CAUSE_WIDTH-1:0]                          dec_rn_inst_excp_cause;
  logic [       RENAME_WIDTH-1:0]                          dec_rn_inst_rdy;
  // Bru Update
  logic [          BRU_COUNT-1:0]                          bru_resolve_vld;
  logic [          BRU_COUNT-1:0][        VADDR_WIDTH-1:0] bru_resolve_pc;
  logic [          BRU_COUNT-1:0][PC_LOB_WIDTH-1:0]        bru_resolve_pc_lob;
  logic [          BRU_COUNT-1:0]                          bru_resolve_taken;
  logic [          BRU_COUNT-1:0]                          bru_resolve_mispred;
  logic [          BRU_COUNT-1:0][      BTQ_TAG_WIDTH-1:0] bru_resolve_btq_tag;
  logic [          BRU_COUNT-1:0][        VADDR_WIDTH-1:0] bru_resolve_target;
  // Redirect
  logic                                                    redirect_vld;
  logic [        VADDR_WIDTH-1:0]                          redirect_target;
  logic [        VADDR_WIDTH-1:0]                          redirect_pc;
  logic [PC_LOB_WIDTH-1:0]                                 redirect_pc_lob;
  logic [      BTQ_TAG_WIDTH-1:0]                          redirect_btq_tag;
  logic                                                    redirect_is_taken;
  // Redirect Port
  logic                                                    csr_redirect_vld;
  logic [        VADDR_WIDTH-1:0]                          csr_redirect_pc;
  logic [PC_LOB_WIDTH-1:0]                                 csr_redirect_pc_lob;
  logic [        VADDR_WIDTH-1:0]                          csr_redirect_target;
  logic [      BTQ_TAG_WIDTH-1:0]                          csr_redirect_btq_tag;
  // Commit
  logic [       RETIRE_WIDTH-1:0]                          retire_insn_vld;
  logic [       RETIRE_WIDTH-1:0][        VADDR_WIDTH-1:0] retire_insn_pc;
  logic [       RETIRE_WIDTH-1:0][PC_LOB_WIDTH-1:0]        retire_insn_pc_lob;
  logic [       RETIRE_WIDTH-1:0]                          retire_insn_is_br;
  logic [       RETIRE_WIDTH-1:0]                          retire_insn_mispred;
  logic [       RETIRE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag;
  // DTLB -> MMU
  // L1 TLB -> STLB : Look up Request
  logic                                                    dtlb_stlb_req_vld;
  logic [DTLB_TRANS_ID_WIDTH-1:0]                          dtlb_stlb_req_trans_id;
  logic [         ASID_WIDTH-1:0]                          dtlb_stlb_req_asid;
  logic [   PMA_ACCESS_WIDTH-1:0]                          dtlb_stlb_req_access_type;
  logic [          VPN_WIDTH-1:0]                          dtlb_stlb_req_vpn;
  logic                                                    dtlb_stlb_req_rdy;
  // STLB -> L1 TLB : Look up Response
  logic                                                    dtlb_stlb_resp_vld;
  logic [DTLB_TRANS_ID_WIDTH-1:0]                          dtlb_stlb_resp_trans_id;
  logic [         ASID_WIDTH-1:0]                          dtlb_stlb_resp_asid;
  logic [      PTE_LVL_WIDTH-1:0]                          dtlb_stlb_resp_pte_lvl;
  logic [               XLEN-1:0]                          dtlb_stlb_resp_pte;
  logic [   PMA_ACCESS_WIDTH-1:0]                          dtlb_stlb_resp_access_type;
  logic [          VPN_WIDTH-1:0]                          dtlb_stlb_resp_vpn;
  logic                                                    dtlb_stlb_resp_access_fault;
  logic                                                    dtlb_stlb_resp_page_fault;
  logic                                                    dtlb_stlb_resp_rdy;
  // DTLB Evict
  logic                                                    dtlb_evict_vld;
  logic [          PTE_WIDTH-1:0]                          dtlb_evict_pte;
  logic [     PAGE_LVL_WIDTH-1:0]                          dtlb_evict_page_lvl;
  logic [          VPN_WIDTH-1:0]                          dtlb_evict_vpn;
  logic [         ASID_WIDTH-1:0]                          dtlb_evict_asid;
  // ITLB Entry Evict
  logic                                                    itlb_evict_vld;
  logic [          PTE_WIDTH-1:0]                          itlb_evict_pte;
  logic [     PAGE_LVL_WIDTH-1:0]                          itlb_evict_page_lvl;
  logic [          VPN_WIDTH-1:0]                          itlb_evict_vpn;
  logic [         ASID_WIDTH-1:0]                          itlb_evict_asid;
  // L1 TLB -> STLB : Look up Request
  logic                                                    itlb_stlb_req_vld;
  logic [ITLB_TRANS_ID_WIDTH-1:0]                          itlb_stlb_req_trans_id;
  logic [         ASID_WIDTH-1:0]                          itlb_stlb_req_asid;
  logic [   PMA_ACCESS_WIDTH-1:0]                          itlb_stlb_req_access_type;
  logic [          VPN_WIDTH-1:0]                          itlb_stlb_req_vpn;
  logic                                                    itlb_stlb_req_rdy;
  // STLB -> L1 TLB : Look up Response
  logic                                                    itlb_stlb_resp_vld;
  logic [ITLB_TRANS_ID_WIDTH-1:0]                          itlb_stlb_resp_trans_id;
  logic [         ASID_WIDTH-1:0]                          itlb_stlb_resp_asid;
  logic [      PTE_LVL_WIDTH-1:0]                          itlb_stlb_resp_pte_lvl;
  logic [               XLEN-1:0]                          itlb_stlb_resp_pte;
  logic [          VPN_WIDTH-1:0]                          itlb_stlb_resp_vpn;
  logic [   PMA_ACCESS_WIDTH-1:0]                          itlb_stlb_resp_access_type;
  logic                                                    itlb_stlb_resp_access_fault;
  logic                                                    itlb_stlb_resp_page_fault;
  // R/W pmp config
  logic                                                    pmp_cfg_vld;
  logic [  PMP_CFG_TAG_WIDTH-1:0]                          pmp_cfg_tag;
  logic [               XLEN-1:0]                          pmp_cfg_wr_data;
  logic [               XLEN-1:0]                          pmp_cfg_rd_data;
  // R/W pmp addr
  logic                                                    pmp_addr_vld;
  logic [ PMP_ADDR_TAG_WIDTH-1:0]                          pmp_addr_tag;
  logic [               XLEN-1:0]                          pmp_addr_wr_data;
  logic [               XLEN-1:0]                          pmp_addr_rd_data;
  // ptw walk request port
  logic                                                    ptw_walk_req_vld;
  logic [       PTW_ID_WIDTH-1:0]                          ptw_walk_req_id;
  logic [        PADDR_WIDTH-1:0]                          ptw_walk_req_addr;
  logic                                                    ptw_walk_req_rdy;
  // ptw walk response port
  logic                                                    ptw_walk_resp_vld;
  logic [       PTW_ID_WIDTH-1:0]                          ptw_walk_resp_id;
  logic [          PTE_WIDTH-1:0]                          ptw_walk_resp_pte;
  logic                                                    ptw_walk_resp_rdy;

  logic                                                    interrupt_ack;
  logic [   EXCP_CAUSE_WIDTH-1:0]                          interrupt_cause;
  // Execute FENCE.I : Flush Icache
  logic                                                    flush_icache_req;
  logic                                                    flush_icache_gnt;
  // Execute SFENCE.VMA : Flush ITLB
  logic                                                    flush_itlb_req;
  logic                                                    flush_itlb_use_vpn;
  logic                                                    flush_itlb_use_asid;
  logic [          VPN_WIDTH-1:0]                          flush_itlb_vpn;
  logic [         ASID_WIDTH-1:0]                          flush_itlb_asid;
  logic                                                    flush_itlb_gnt;
  // Execute SFENCE.VMA : Flush STLB
  logic                                                    flush_stlb_req;
  logic                                                    flush_stlb_use_vpn;
  logic                                                    flush_stlb_use_asid;
  logic [          VPN_WIDTH-1:0]                          flush_stlb_vpn;
  logic [          ASID_WIDTH-1:0]                         flush_stlb_asid;
  logic                                                    flush_stlb_gnt;

  generate
    assign redirect_pc_lob = redirect_pc[PC_LOB_WIDTH-1:0];
    assign csr_redirect_pc_lob = csr_redirect_pc[PC_LOB_WIDTH-1:0];
    for (genvar i = 0; i < BRU_COUNT; i++) begin
      assign bru_resolve_pc_lob[i] = bru_resolve_pc[i][PC_LOB_WIDTH-1:0];
    end
    for (genvar i = 0; i < RETIRE_WIDTH; i++) begin
      assign retire_insn_pc_lob[i] = retire_insn_pc[i][PC_LOB_WIDTH-1:0];
    end
  endgenerate

  rvh_ifu u_rvh_ifu(
      .priv_lvl_i(priv_lvl),
      .mstatus_mxr(mstatus[19]),
      .mstatus_sum(mstatus[18]),
      .satp_mode_i(satp[63:60]),
      .satp_asid_i(satp[59:44]),
    // fe > be
      .fe_dec_instr_valid_o(ifu_dec_inst_vld),
      .fe_dec_btq_tag_o(ifu_dec_inst_btq_tag),
      .fe_dec_pc_o(ifu_dec_inst_pc),
      .fe_dec_pred_target_o(ifu_dec_inst_pred_target),
      .fe_dec_instr_o(ifu_dec_inst),
      .fe_dec_is_rvc_o(),
      .fe_dec_pred_taken_o(ifu_dec_inst_pred_taken),
      .fe_dec_excp_tval_o(ifu_dec_inst_excp_tval),
      .fe_dec_excp_vld_o(ifu_dec_inst_excp_vld),
      .fe_dec_excp_cause_o(ifu_dec_inst_excp_cause),
      .fe_dec_ready_i(ifu_dec_inst_rdy[0]),
    // bru redirect
      .bru_redirect_valid_i  (redirect_vld),
      .bru_redirect_pc_lob_i (redirect_pc_lob),
      .bru_redirect_target_i (redirect_target),
      .bru_redirect_btq_tag_i(redirect_btq_tag),
      .bru_redirect_taken_i  (redirect_is_taken),
    // cmt > btq                   
      .cmt_valid_i  (retire_insn_vld),
      .cmt_pc_lob_i (retire_insn_pc_lob),
      .cmt_btq_tag_i(retire_insn_btq_tag),
    // cmt redirect
      .cmt_redirect_valid_i (csr_redirect_vld),
      .cmt_redirect_pc_lob_i(csr_redirect_pc_lob),
      .cmt_redirect_target_i(csr_redirect_target),
      .cmt_redirect_btq_tag_i(csr_redirect_btq_tag),

      .boot_address_i(39'h80000000),

    // L1 TLB -> STLB : Look up Request
      .stlb_req_vld_o(itlb_stlb_req_vld),
      .stlb_req_trans_id_o(itlb_stlb_req_trans_id),
      .stlb_req_asid_o(itlb_stlb_req_asid),
      .stlb_req_access_type_o(itlb_stlb_req_access_type),
      .stlb_req_vpn_o(itlb_stlb_req_vpn),
      .stlb_req_rdy_i(itlb_stlb_req_rdy),

      .stlb_resp_vld_i(itlb_stlb_resp_vld),
      .stlb_resp_trans_id_i(itlb_stlb_resp_trans_id),
      .stlb_resp_asid_i(itlb_stlb_resp_asid),
      .stlb_resp_pte_lvl_i(itlb_stlb_resp_pte_lvl),
      .stlb_resp_pte_i(itlb_stlb_resp_pte),
      .stlb_resp_vpn_i(itlb_stlb_resp_vpn),
      .stlb_resp_access_type_i(itlb_stlb_resp_access_type),
      .stlb_resp_access_fault_i(itlb_stlb_resp_access_fault),
      .stlb_resp_page_fault_i(itlb_stlb_resp_page_fault),

    // ITLB Entry Evict
      .itlb_evict_vld_o(itlb_evict_vld),
      .itlb_evict_pte_o(itlb_evict_pte),
      .itlb_evict_page_lvl_o(itlb_evict_page_lvl),
      .itlb_evict_vpn_o(itlb_evict_vpn),
      .itlb_evict_asid_o(itlb_evict_asid),

    // Execute SFENCE.VMA : Flush ITLB
      .flush_itlb_req_i               (flush_itlb_req),
      .flush_itlb_use_vpn_i           (flush_itlb_use_vpn),
      .flush_itlb_use_asid_i          (flush_itlb_use_asid),
      .flush_itlb_vpn_i               (flush_itlb_vpn),
      .flush_itlb_asid_i              (flush_itlb_asid),
      .flush_itlb_gnt_o               (flush_itlb_gnt),

    // L1I -> L2 : Request
      .l1i_l2_req_arvalid_o(l1_l2_req_arvalid_o[1]),
      .l1i_l2_req_arready_i(l1_l2_req_arready_i[1]),
      .l1i_l2_req_ar_o     (l1_l2_req_ar_o[1]),
      .l1i_l2_req_awvalid_o(l1_l2_req_awvalid_o[1]),
      .l1i_l2_req_awready_i(l1_l2_req_awready_i[1]),
      .l1i_l2_req_aw_o     (l1_l2_req_aw_o[1]),
      .l1i_l2_req_wvalid_o (l1_l2_req_wvalid_o[1]),
      .l1i_l2_req_wready_i (l1_l2_req_wready_i[1]),
      .l1i_l2_req_w_o      (l1_l2_req_w_o[1]),
      .l2_l1i_resp_bvalid_i(l2_l1_resp_bvalid_i[1]),
      .l2_l1i_resp_bready_o(l2_l1_resp_bready_o[1]),
      .l2_l1i_resp_b_i     (l2_l1_resp_b_i[1]),
      .l2_l1i_resp_rvalid_i(l2_l1_resp_rvalid_i[1]),
      .l2_l1i_resp_rready_o(l2_l1_resp_rready_o[1]),
      .l2_l1i_resp_r_i     (l2_l1_resp_r_i[1]),

      .flush_icache_req_i  (flush_icache_req),
      .flush_icache_gnt_o  (flush_icache_gnt),

      .clk(clk),
      .rst(rst)
);


  rvh_decode u_rvh_decode (
      .priv_lvl_i(priv_lvl),
      .debug_mode_i(1'b0),
      .tvm_i(tvm),
      .tw_i(tw),
      .tsr_i(tsr),
      .interrupt_i(interrupt_ack),
      .interrupt_cause_i(interrupt_cause),
      .ifu_dec_inst_vld_i(ifu_dec_inst_vld),
      .ifu_dec_inst_i(ifu_dec_inst),
      .ifu_dec_inst_pc_i(ifu_dec_inst_pc),
      .ifu_dec_inst_btq_tag_i(ifu_dec_inst_btq_tag),
      .ifu_dec_inst_pred_taken_i(ifu_dec_inst_pred_taken),
      .ifu_dec_inst_pred_target_i(ifu_dec_inst_pred_target),
      .ifu_dec_inst_excp_tval_i(ifu_dec_inst_excp_tval),
      .ifu_dec_inst_excp_vld_i(ifu_dec_inst_excp_vld),
      .ifu_dec_inst_excp_cause_i(ifu_dec_inst_excp_cause),
      .ifu_dec_inst_rdy_o(ifu_dec_inst_rdy),
      .dec_rn_inst_vld_o(dec_rn_inst_vld),
      .dec_rn_inst_excp_vld_o(dec_rn_inst_excp_vld),
      .dec_rn_inst_pc_o(dec_rn_inst_pc),
      .dec_rn_inst_is_rvc_o(dec_rn_inst_is_rvc),
      .dec_rn_inst_btq_tag_o(dec_rn_inst_btq_tag),
      .dec_rn_inst_pred_taken_o(dec_rn_inst_pred_taken),
      .dec_rn_inst_pred_target_o(dec_rn_inst_pred_target),
      .dec_rn_inst_major_op_o(dec_rn_inst_major_op),
      .dec_rn_inst_minor_op_o(dec_rn_inst_minor_op),
      .dec_rn_inst_use_rs1_o(dec_rn_inst_use_rs1),
      .dec_rn_inst_use_rs2_o(dec_rn_inst_use_rs2),
      .dec_rn_inst_use_rd_o(dec_rn_inst_use_rd),
      .dec_rn_inst_rs1_o(dec_rn_inst_rs1),
      .dec_rn_inst_rs2_o(dec_rn_inst_rs2),
      .dec_rn_inst_rd_o(dec_rn_inst_rd),
      .dec_rn_inst_imm_o(dec_rn_inst_imm),
      .dec_rn_inst_use_imm_o(dec_rn_inst_use_imm),
      .dec_rn_inst_alu_use_pc_o(dec_rn_inst_alu_use_pc),
      .dec_rn_inst_is_fence_o(dec_rn_inst_is_fence),
      .dec_rn_inst_control_flow_o(dec_rn_inst_control_flow),
      .dec_rn_inst_excp_tval_o(dec_rn_inst_excp_tval),
      .dec_rn_inst_excp_cause_o(dec_rn_inst_excp_cause),
      .dec_rn_inst_rdy_i(dec_rn_inst_rdy),
      .flush_i(redirect_vld | csr_redirect_vld),
      .clk(clk),
      .rst(rst)
  );

  rvh_backend u_rvh_backend (
      .hart_id_i                      (hart_id_i),
      .boot_address_i                 (boot_address_i),
      .priv_lvl_o                     (priv_lvl),
      .tvm_o                          (tvm),
      .tw_o                           (tw),
      .tsr_o                          (tsr),
      .mstatus_o                      (mstatus),
      .satp_o                         (satp),
      .dec_rn_inst_vld_i              (dec_rn_inst_vld),
      .dec_rn_inst_is_rvc_i           (dec_rn_inst_is_rvc),
      .dec_rn_inst_pc_i               (dec_rn_inst_pc),
      .dec_rn_inst_btq_tag_i          (dec_rn_inst_btq_tag),
      .dec_rn_inst_pred_taken_i       (dec_rn_inst_pred_taken),
      .dec_rn_inst_pred_target_i      (dec_rn_inst_pred_target),
      .dec_rn_inst_excp_vld_i         (dec_rn_inst_excp_vld),
      .dec_rn_inst_major_op_i         (dec_rn_inst_major_op),
      .dec_rn_inst_minor_op_i         (dec_rn_inst_minor_op),
      .dec_rn_inst_use_rs1_i          (dec_rn_inst_use_rs1),
      .dec_rn_inst_use_rs2_i          (dec_rn_inst_use_rs2),
      .dec_rn_inst_use_rd_i           (dec_rn_inst_use_rd),
      .dec_rn_inst_rs1_i              (dec_rn_inst_rs1),
      .dec_rn_inst_rs2_i              (dec_rn_inst_rs2),
      .dec_rn_inst_rd_i               (dec_rn_inst_rd),
      .dec_rn_inst_imm_i              (dec_rn_inst_imm),
      .dec_rn_inst_use_imm_i          (dec_rn_inst_use_imm),
      .dec_rn_inst_alu_use_pc_i       (dec_rn_inst_alu_use_pc),
      .dec_rn_inst_is_fence_i         (dec_rn_inst_is_fence),
      .dec_rn_inst_control_flow_i     (dec_rn_inst_control_flow),
      .dec_rn_inst_excp_tval_i        (dec_rn_inst_excp_tval),
      .dec_rn_inst_excp_cause_i       (dec_rn_inst_excp_cause),
      .dec_rn_inst_rdy_o              (dec_rn_inst_rdy),
      .bru_resolve_vld_o              (bru_resolve_vld),
      .bru_resolve_pc_o               (bru_resolve_pc),
      .bru_resolve_taken_o            (bru_resolve_taken),
      .bru_resolve_mispred_o          (bru_resolve_mispred),
      .bru_resolve_btq_tag_o          (bru_resolve_btq_tag),
      .bru_resolve_target_o           (bru_resolve_target),
      .redirect_vld_o                 (redirect_vld),
      .redirect_target_o              (redirect_target),
      .redirect_pc_o                  (redirect_pc),
      .redirect_btq_tag_o             (redirect_btq_tag),
      .redirect_is_taken_o            (redirect_is_taken),
      .csr_redirect_vld_o             (csr_redirect_vld),
      .csr_redirect_pc_o              (csr_redirect_pc),
      .csr_redirect_target_o          (csr_redirect_target),
      .csr_redirect_btq_tag_o         (csr_redirect_btq_tag),
      .retire_insn_vld_o              (retire_insn_vld),
      .retire_insn_pc_o               (retire_insn_pc),
      .retire_insn_is_br_o            (retire_insn_is_br),
      .retire_insn_mispred_o          (retire_insn_mispred),
      .retire_insn_btq_tag_o          (retire_insn_btq_tag),
      .l1_tlb_stlb_req_vld_o          (dtlb_stlb_req_vld),
      .l1_tlb_stlb_req_trans_id_o     (dtlb_stlb_req_trans_id),
      .l1_tlb_stlb_req_asid_o         (dtlb_stlb_req_asid),
      .l1_tlb_stlb_req_access_type_o  (dtlb_stlb_req_access_type),
      .l1_tlb_stlb_req_vpn_o          (dtlb_stlb_req_vpn),
      .l1_tlb_stlb_req_rdy_i          (dtlb_stlb_req_rdy),
      .l1_tlb_stlb_resp_vld_i         (dtlb_stlb_resp_vld),
      .l1_tlb_stlb_resp_trans_id_i    (dtlb_stlb_resp_trans_id),
      .l1_tlb_stlb_resp_asid_i        (dtlb_stlb_resp_asid),
      .l1_tlb_stlb_resp_pte_lvl_i     (dtlb_stlb_resp_pte_lvl),
      .l1_tlb_stlb_resp_pte_i         (dtlb_stlb_resp_pte),
      .l1_tlb_stlb_resp_access_type_i (dtlb_stlb_resp_access_type),
      .l1_tlb_stlb_resp_vpn_i         (dtlb_stlb_resp_vpn),
      .l1_tlb_stlb_resp_access_fault_i(dtlb_stlb_resp_access_fault),
      .l1_tlb_stlb_resp_page_fault_i  (dtlb_stlb_resp_page_fault),
      .l1_tlb_stlb_resp_rdy_o         (dtlb_stlb_resp_rdy),
      .dtlb_evict_vld_o               (dtlb_evict_vld),
      .dtlb_evict_pte_o               (dtlb_evict_pte),
      .dtlb_evict_page_lvl_o          (dtlb_evict_page_lvl),
      .dtlb_evict_vpn_o               (dtlb_evict_vpn),
      .dtlb_evict_asid_o              (dtlb_evict_asid),
      // AR
      .l1d_l2_req_arvalid_o           (l1_l2_req_arvalid_o[0]),
      .l1d_l2_req_arready_i           (l1_l2_req_arready_i[0]),
      .l1d_l2_req_ar_o                (l1_l2_req_ar_o[0]),
      // ewrq -> mem bus                                
      // AW                                             
      .l1d_l2_req_awvalid_o           (l1_l2_req_awvalid_o[0]),
      .l1d_l2_req_awready_i           (l1_l2_req_awready_i[0]),
      .l1d_l2_req_aw_o                (l1_l2_req_aw_o[0]),
      // W                                              
      .l1d_l2_req_wvalid_o            (l1_l2_req_wvalid_o[0]),
      .l1d_l2_req_wready_i            (l1_l2_req_wready_i[0]),
      .l1d_l2_req_w_o                 (l1_l2_req_w_o[0]),
      // L1D -> L2 : Response                             
      // B                             
      .l2_l1d_resp_bvalid_i           (l2_l1_resp_bvalid_i[0]),
      .l2_l1d_resp_bready_o           (l2_l1_resp_bready_o[0]),
      .l2_l1d_resp_b_i                (l2_l1_resp_b_i[0]),
      // mem bus -> mlfb                                
      // R                             
      .l2_l1d_resp_rvalid_i           (l2_l1_resp_rvalid_i[0]),
      .l2_l1d_resp_rready_o           (l2_l1_resp_rready_o[0]),
      .l2_l1d_resp_r_i                (l2_l1_resp_r_i[0]),
      .interrupt_sw_i                 (interrupt_sw_i),
      .interrupt_timer_i              (interrupt_timer_i),
      .interrupt_ext_i                (interrupt_ext_i),
      .pmp_cfg_vld_o                  (pmp_cfg_vld),
      .pmp_cfg_tag_o                  (pmp_cfg_tag),
      .pmp_cfg_wr_data_o              (pmp_cfg_wr_data),
      .pmp_cfg_rd_data_i              (pmp_cfg_rd_data),
      .pmp_addr_vld_o                 (pmp_addr_vld),
      .pmp_addr_tag_o                 (pmp_addr_tag),
      .pmp_addr_wr_data_o             (pmp_addr_wr_data),
      .pmp_addr_rd_data_i             (pmp_addr_rd_data),
      .ptw_walk_req_vld_i             (ptw_walk_req_vld),
      .ptw_walk_req_id_i              (ptw_walk_req_id),
      .ptw_walk_req_addr_i            (ptw_walk_req_addr),
      .ptw_walk_req_rdy_o             (ptw_walk_req_rdy),
      .ptw_walk_resp_vld_o            (ptw_walk_resp_vld),
      .ptw_walk_resp_id_o             (ptw_walk_resp_id),
      .ptw_walk_resp_pte_o            (ptw_walk_resp_pte),
      .ptw_walk_resp_rdy_i            (ptw_walk_resp_rdy),
      .flush_icache_req_o             (flush_icache_req),
      .flush_icache_gnt_i             (flush_icache_gnt),
      .flush_itlb_req_o               (flush_itlb_req),
      .flush_itlb_use_vpn_o           (flush_itlb_use_vpn),
      .flush_itlb_use_asid_o          (flush_itlb_use_asid),
      .flush_itlb_vpn_o               (flush_itlb_vpn),
      .flush_itlb_asid_o              (flush_itlb_asid),
      .flush_itlb_gnt_i               (flush_itlb_gnt),
      .flush_stlb_req_o               (flush_stlb_req),
      .flush_stlb_use_vpn_o           (flush_stlb_use_vpn),
      .flush_stlb_use_asid_o          (flush_stlb_use_asid),
      .flush_stlb_vpn_o               (flush_stlb_vpn),
      .flush_stlb_asid_o              (flush_stlb_asid),
      .flush_stlb_gnt_i               (flush_stlb_gnt),
      .interrupt_ack_o                (interrupt_ack),
      .interrupt_cause_o              (interrupt_cause),
      .clk                            (clk),
      .rst                            (rst)
  );


  rvh_mmu #(
      .PTW_ID_WIDTH(PTW_ID_WIDTH),
      .TRANS_ID_WIDTH(DTLB_TRANS_ID_WIDTH),
      .PADDR_WIDTH(PADDR_WIDTH),
      .EXCP_CAUSE_WIDTH(EXCP_CAUSE_WIDTH),
      .VPN_WIDTH(VPN_WIDTH),
      .PMP_ENTRY_COUNT(PMP_ENTRY_COUNT)
  ) u_rvh_mmu (
      .priv_lvl_i(priv_lvl),
      .satp_mode_i(satp[63:60]),
      .satp_ppn_i(satp[43:0]),
      .pmp_cfg_set_vld_i(pmp_cfg_vld),
      .pmp_cfg_set_addr_i(pmp_cfg_tag),
      .pmp_cfg_set_payload_i(pmp_cfg_wr_data),
      .pmp_cfg_origin_payload_o(pmp_cfg_rd_data),
      .pmp_addr_set_vld_i(pmp_addr_vld),
      .pmp_addr_set_addr_i(pmp_addr_tag),
      .pmp_addr_set_payload_i(pmp_addr_wr_data),
      .pmp_addr_origin_payload_o(pmp_addr_rd_data),
      .dtlb_miss_req_vld_i(dtlb_stlb_req_vld),
      .dtlb_miss_req_trans_id_i(dtlb_stlb_req_trans_id),
      .dtlb_miss_req_asid_i(dtlb_stlb_req_asid),
      .dtlb_miss_req_vpn_i(dtlb_stlb_req_vpn),
      .dtlb_miss_req_access_type_i(dtlb_stlb_req_access_type),
      .dtlb_miss_req_rdy_o(dtlb_stlb_req_rdy),
      .dtlb_miss_resp_vld_o(dtlb_stlb_resp_vld),
      .dtlb_miss_resp_trans_id_o(dtlb_stlb_resp_trans_id),
      .dtlb_miss_resp_asid_o(dtlb_stlb_resp_asid),
      .dtlb_miss_resp_pte_o(dtlb_stlb_resp_pte),
      .dtlb_miss_resp_page_lvl_o(dtlb_stlb_resp_pte_lvl),
      .dtlb_miss_resp_vpn_o(dtlb_stlb_resp_vpn),
      .dtlb_miss_resp_access_type_o(dtlb_stlb_resp_access_type),
      .dtlb_miss_resp_access_fault_o(dtlb_stlb_resp_access_fault),
      .dtlb_miss_resp_page_fault_o(dtlb_stlb_resp_page_fault),
      .dtlb_evict_vld_i(dtlb_evict_vld),
      .dtlb_evict_pte_i(dtlb_evict_pte),
      .dtlb_evict_page_lvl_i(dtlb_evict_page_lvl),
      .dtlb_evict_vpn_i(dtlb_evict_vpn),
      .dtlb_evict_asid_i(dtlb_evict_asid),
      .itlb_miss_req_vld_i(itlb_stlb_req_vld),
      .itlb_miss_req_trans_id_i(itlb_stlb_req_trans_id),
      .itlb_miss_req_asid_i(itlb_stlb_req_asid),
      .itlb_miss_req_vpn_i(itlb_stlb_req_vpn),
      .itlb_miss_req_access_type_i(itlb_stlb_req_access_type),
      .itlb_miss_req_rdy_o(itlb_stlb_req_rdy),
      .itlb_miss_resp_vld_o(itlb_stlb_resp_vld),
      .itlb_miss_resp_trans_id_o(itlb_stlb_resp_trans_id),
      .itlb_miss_resp_asid_o(itlb_stlb_resp_asid),
      .itlb_miss_resp_pte_o(itlb_stlb_resp_pte),
      .itlb_miss_resp_page_lvl_o(itlb_stlb_resp_pte_lvl),
      .itlb_miss_resp_vpn_o(itlb_stlb_resp_vpn),
      .itlb_miss_resp_access_type_o(itlb_stlb_resp_access_type),
      .itlb_miss_resp_access_fault_o(itlb_stlb_resp_access_fault),
      .itlb_miss_resp_page_fault_o(itlb_stlb_resp_page_fault),
      .itlb_evict_vld_i(itlb_evict_vld),
      .itlb_evict_pte_i(itlb_evict_pte),
      .itlb_evict_page_lvl_i(itlb_evict_page_lvl),
      .itlb_evict_vpn_i(itlb_evict_vpn),
      .itlb_evict_asid_i(itlb_evict_asid),
      .ptw_walk_req_vld_o(ptw_walk_req_vld),
      .ptw_walk_req_id_o(ptw_walk_req_id),
      .ptw_walk_req_addr_o(ptw_walk_req_addr),
      .ptw_walk_req_rdy_i(ptw_walk_req_rdy),
      .ptw_walk_resp_vld_i(ptw_walk_resp_vld),
      .ptw_walk_resp_id_i(ptw_walk_resp_id),
      .ptw_walk_resp_pte_i(ptw_walk_resp_pte),
      .ptw_walk_resp_rdy_o(ptw_walk_resp_rdy),
      .tlb_flush_vld_i(flush_stlb_req),
      .tlb_flush_use_asid_i(flush_stlb_use_asid),
      .tlb_flush_use_vpn_i(flush_stlb_use_vpn),
      .tlb_flush_vpn_i(flush_stlb_vpn),
      .tlb_flush_asid_i(flush_stlb_asid),
      .tlb_flush_grant_o(flush_stlb_gnt),
      .clk(clk),
      .rstn(~rst)
  );





























































/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  rvh_core u_rvh_core (
      .hart_id_i          (0),
      .boot_address_i     (39'h80000000),
      .interrupt_sw_i     ('0),
      .interrupt_timer_i  ('0),
      .interrupt_ext_i    ('0),
      // AR
      .l1_l2_req_arvalid_o(l1_l2_arvalid),
      .l1_l2_req_arready_i(l1_l2_arready),
      .l1_l2_req_ar_o     (l1_l2_ar),
      // ewrq -> mem bus
      // AW
      .l1_l2_req_awvalid_o(l1_l2_awvalid),
      .l1_l2_req_awready_i(l1_l2_awready),
      .l1_l2_req_aw_o     (l1_l2_aw),
      // W
      .l1_l2_req_wvalid_o (l1_l2_wvalid),
      .l1_l2_req_wready_i (l1_l2_wready),
      .l1_l2_req_w_o      (l1_l2_w),
      // L1D -> L2 : Response
      // B
      .l2_l1_resp_bvalid_i(l1_l2_bvalid),
      .l2_l1_resp_bready_o(l1_l2_bready),
      .l2_l1_resp_b_i     (l1_l2_b),
      // mem bus -> mlfb
      // R
      .l2_l1_resp_rvalid_i(l1_l2_rvalid),
      .l2_l1_resp_rready_o(l1_l2_rready),
      .l2_l1_resp_r_i     (l1_l2_r),

      .clk                (clk),
      .rst                (rst)
  );

rvh_l1d_bank_axi_arb
#(
  .INPUT_PORT_NUM (2),
  .RESP_PORT_SELECT_BID_LSB($clog2(L1D_BANK_ID_NUM)) // bid low 3 bit for cache bank id, the highest used for distinguish i$/d$
)
L1_CACHE_AXI_ARB_U
(
    // L1D banks -> axi arb
      // AR
    .l1d_bank_axi_arb_arvalid                 (l1_l2_arvalid  ),
    .l1d_bank_axi_arb_arready                 (l1_l2_arready  ),
    .l1d_bank_axi_arb_ar                      (l1_l2_ar       ),
      // AW
    .l1d_bank_axi_arb_awvalid                 (l1_l2_awvalid  ),
    .l1d_bank_axi_arb_awready                 (l1_l2_awready  ),
    .l1d_bank_axi_arb_aw                      (l1_l2_aw       ),
      // W
    .l1d_bank_axi_arb_wvalid                  (l1_l2_wvalid   ),
    .l1d_bank_axi_arb_wready                  (l1_l2_wready   ),
    .l1d_bank_axi_arb_w                       (l1_l2_w        ),
      // B
    .l1d_bank_axi_arb_bvalid                  (l1_l2_bvalid   ),
    .l1d_bank_axi_arb_bready                  (l1_l2_bready   ),
    .l1d_bank_axi_arb_b                       (l1_l2_b        ),
      // R
    .l1d_bank_axi_arb_rvalid                  (l1_l2_rvalid   ),
    .l1d_bank_axi_arb_rready                  (l1_l2_rready   ),
    .l1d_bank_axi_arb_r                       (l1_l2_r        ),

    // axi arb -> L2
      // AR
    .axi_arb_l2_arvalid                       (l1_l2_arvalid_from_arb      ),
    .axi_arb_l2_arready                       (l1_l2_arready_from_arb      ),
    .axi_arb_l2_ar                            (l1_l2_ar_from_arb           ),
      // AW
    .axi_arb_l2_awvalid                       (l1_l2_awvalid_from_arb      ),
    .axi_arb_l2_awready                       (l1_l2_awready_from_arb      ),
    .axi_arb_l2_aw                            (l1_l2_aw_from_arb           ),
      // W
    .axi_arb_l2_wvalid                        (l1_l2_wvalid_from_arb       ),
    .axi_arb_l2_wready                        (l1_l2_wready_from_arb       ),
    .axi_arb_l2_w                             (l1_l2_w_from_arb            ),
      // B
    .axi_arb_l2_bvalid                        (l1_l2_bvalid_from_arb       ),
    .axi_arb_l2_bready                        (l1_l2_bready_from_arb       ),
    .axi_arb_l2_b                             (l1_l2_b_from_arb            ),
      // R
    .axi_arb_l2_rvalid                        (l1_l2_rvalid_from_arb       ),
    .axi_arb_l2_rready                        (l1_l2_rready_from_arb       ),
    .axi_arb_l2_r                             (l1_l2_r_from_arb            ),

    .clk    (clk),
    .rst    (rst)

);

`ifndef SYNTHESIS
  axi_mem #(
      .ID_WIDTH($bits(mem_tid_t)),
      .MEM_SIZE(1 << 29),  //byte 512MB
      .mem_clear(0),
      .mem_simple_seq(0),
      .READ_DELAY_CYCLE(1 << 7),
      .READ_DELAY_CYCLE_RANDOMLIZE(1),
      .READ_DELAY_CYCLE_RANDOMLIZE_UPDATE_CYCLE(1 << 10),
      .AXI_DATA_WIDTH(MEM_DATA_WIDTH)  // bit
  ) u_axi_mem (
        .clk      (clk),
        .rst_n    (~rst)
      //AW
      , .i_awid   (l1_l2_aw_from_arb.awid),
        .i_awaddr (l1_l2_aw_from_arb.awaddr),
        .i_awlen  (l1_l2_aw_from_arb.awlen),
        .i_awsize (l1_l2_aw_from_arb.awsize),
        .i_awburst(l1_l2_aw_from_arb.awburst)   // INCR mode
      , .i_awvalid(l1_l2_awvalid_from_arb),
        .o_awready(l1_l2_awready_from_arb)
      //AR
      , .i_arid   (l1_l2_ar_from_arb.arid),
        .i_araddr (l1_l2_ar_from_arb.araddr),
        .i_arlen  (l1_l2_ar_from_arb.arlen),
        .i_arsize (l1_l2_ar_from_arb.arsize),
        .i_arburst(l1_l2_ar_from_arb.arburst),
        .i_arvalid(l1_l2_arvalid_from_arb),
        .o_arready(l1_l2_arready_from_arb)
      //W
      , .i_wdata  (l1_l2_w_from_arb.wdata),
        .i_wstrb  ('1),
        .i_wlast  (l1_l2_w_from_arb.wlast),
        .i_wvalid (l1_l2_wvalid_from_arb),
        .o_wready (l1_l2_wready_from_arb)
      //B
      , .o_bid    (l1_l2_b_from_arb.bid),
        .o_bresp  (l1_l2_b_from_arb.bresp),
        .o_bvalid (l1_l2_bvalid_from_arb),
        .i_bready (l1_l2_bready_from_arb)
      //R
      , .o_rid    (l1_l2_r_from_arb.rid),
        .o_rdata  (l1_l2_r_from_arb.dat),
        .o_rresp  (l1_l2_r_from_arb.rresp),
        .o_rlast  (l1_l2_r_from_arb.rlast),
        .o_rvalid (l1_l2_rvalid_from_arb),
        .i_rready (l1_l2_rready_from_arb)

  );

  assign l1_l2_r_from_arb.mesi_sta = EXCLUSIVE;
  assign l1_l2_r_from_arb.err      = 1'b0;
`endif

endmodule : testbench
