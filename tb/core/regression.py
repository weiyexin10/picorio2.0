#!/usr/bin/env python3

import os,sys,stat 
import subprocess
import signal
from threading import Timer

def kill_command(p):
    os.killpg(os.getpgid(p.pid),signal.SIGTERM)

def is_elf_file(filepath):
    try:
        FileStates = os.stat(filepath)
        FileMode = FileStates[stat.ST_MODE]
        if not stat.S_ISREG(FileMode) or stat.S_ISLNK(FileMode):  # 如果文件既不是普通文件也不是链接文件
            return False
        with open(filepath, 'rb') as f:
            header = (bytearray(f.read(4))[1:4]).decode(encoding="utf-8")
            # logger.info("header is {}".format(header))
            if header in ["ELF"]:
                # print header
                return True
        return False
    except Exception:
        pass
   

def walk_dir(dir):
    for root, dirs, files in os.walk(dir):
        for file in files:
            path = os.path.join(root, file)
            if (is_elf_file(path) and ('isa' not in path) and ('interrupt' not in path) and ('performance' not in path)):
                print ('exec elf file: {}'.format(path))
                os.system('echo \'{}\' >> sim_test_report.txt'.format(path))
                print('spike {} > null 2>&1'.format(path))
                p = subprocess.Popen('spike {} > null 2>&1'.format(path),shell=True,stderr=subprocess.PIPE)
                timer = Timer(20, lambda x: x.terminate(), [p])
                try:
                    timer.start()
                    return_code = p.returncode
                    p.wait()
                except Exception as ex:
                    timer.cancel()
                    print ('spike timeout')
                    os.system('echo \'spike timeout\' >> sim_test_report.txt')
                    os.system('echo -e >> sim_test_report.txt')
                else:
                    timer.cancel()
                    if (p.returncode != 0):
                        print ('spike fail')
                        os.system('echo \'spike fail\' >> sim_test_report.txt')
                        os.system('echo -e >> sim_test_report.txt')
                    else :
                        p = subprocess.Popen('./simv +image={} +report=./sim_test_report.txt +dumpon=0 > null 2>&1'.format(path),shell=True,stderr=subprocess.PIPE)
                        p.wait()
                        os.system('echo -e >> sim_test_report.txt')
                
os.system('rm -rf sim_test_report.txt')
walk_dir('/work/stu/lzhang/workSpace/tortureRegression')
# walk_dir('/work/stu/ywei/rivai/p600/hardware/verification/test')

