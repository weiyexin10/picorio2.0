#ifndef __DROMAJO_HPP__
#define __DROMAJO_HPP__

#include <string>
#include <iostream>
#include "dromajo.h"
#include "LiveCacheCore.h"
#include "cutils.h"
#include "iomem.h"
#include "riscv_machine.h"
#include "virtio.h"

class Dromajo
{
public:
    Dromajo(const std::string image, uint64_t initial_pc)
    {
        char tbuf[19];
        snprintf(tbuf, sizeof(tbuf), "0x%x", initial_pc);
        this->next_pc = initial_pc;

        const char *argv[] = {
            "dromajo",
            image.c_str(),
            "--reset_vector",
            tbuf
        };
        m = virt_machine_main(sizeof(argv) / sizeof(argv[0]), (char**)argv);
        if (!m)
        { 
            printf("simulator create fail!\n");
            exit(-1);
        }

    }

    ~Dromajo()
    {
        virt_machine_end(m);
    }

    enum 
    {
        TYPE_OTHER,
        TYPE_JAL,
        TYPE_JALR,
        TYPE_BRANCH,
        TYPE_CALL,
        TYPE_RET
    };
    
    bool iterate()
    {
        RISCVCPUState *cpu = m->cpu_state[0];

        /* Instruction that raises exceptions should be marked as such in
        * the trace of retired instructions.
        */
        riscv_set_pc(cpu, next_pc);
        last_pc  = virt_machine_get_pc(m, 0);
        int      priv     = riscv_get_priv_level(cpu);
        uint32_t insn_raw = 0;
        (void)riscv_read_insn(cpu, &insn_raw, last_pc);
        bool keep_going = virt_machine_run(m, 0);
        uint64_t cur_pc  = virt_machine_get_pc(m, 0);

        next_pc = cur_pc;
        taken = false;

        insn = (insn_raw & 3) == 3 ? insn_raw : (uint16_t)insn_raw;

        if (last_pc == cur_pc)
        {
            printf("endless loop @ pc: %x\n", cur_pc);
            // exit(-1);
        }
        else
        {
            // printf("0x%08x (0x%08x) ", last_pc,
            //     (insn_raw & 3) == 3 ? insn_raw : (uint16_t)insn_raw);
            if ((cur_pc == last_pc + 2) && ((insn_raw & 0x03) != 0x03))
            {
                if (get_last_insn_cfi_type() == TYPE_OTHER)
                {
                    // printf("rvc\n");
                }
                else if (get_last_insn_cfi_type() == TYPE_BRANCH)
                {
                    // printf("not taken branch\n");
                }
                else 
                {
                    taken = true;
                    // printf("jmp from %x to %x\n", last_pc, cur_pc);
                }
            }
            else if ((cur_pc == last_pc + 4) && ((insn_raw & 0x03) == 0x03))
            {
                if (get_last_insn_cfi_type() == TYPE_OTHER)
                {
                    // printf("rvi\n");
                }
                else if (get_last_insn_cfi_type() == TYPE_BRANCH)
                {
                    // printf("not taken branch\n");
                }
                else 
                {
                    taken = true;
                    // printf("jmp from %x to %x\n", last_pc, cur_pc);
                }
            }
            else 
            {
                taken = true;
                // printf("jmp from %x to %x\n", last_pc, cur_pc);
            }
            
        }
        return keep_going;
    }

    int get_last_insn_cfi_type() const
    {
        bool is_rvi_jal = get_bits(insn, 6, 0) == 0x6f;
        bool is_rvi_jalr = get_bits(insn, 6, 0) == 0x67;

        bool is_rvi_call = (is_rvi_jal || is_rvi_jalr) && (get_bits(insn, 11, 7) == 1 || get_bits(insn, 11, 7) == 5);
        bool is_rvi_return = is_rvi_jalr && (get_bits(insn, 19, 15) == 1 || get_bits(insn, 19, 15) == 5) && (get_bits(insn, 19, 15) != get_bits(insn, 11, 7));
        bool is_rvi_branch = get_bits(insn, 6, 0) == 0x63;

        bool is_rvc_jal = get_bits(insn, 15, 13) == 5 && get_bits(insn, 1, 0) == 1;
        bool is_rvc_jr_or_jalr = (get_bits(insn, 15, 13) == 4) && (get_bits(insn, 6, 2) == 0) && (get_bits(insn, 1, 0) == 2);
        bool is_rvc_jalr = is_rvc_jr_or_jalr && (get_bits(insn, 12, 12) == 1);
        bool is_rvc_jr = is_rvc_jr_or_jalr && (get_bits(insn, 12, 12) == 0);

        bool is_rvc_return = (get_bits(insn, 11, 7) == 1 || get_bits(insn, 11, 7) == 5) && is_rvc_jr;
        bool is_rvc_call = is_rvc_jalr;
        bool is_rvc_branch = (get_bits(insn, 15, 13) == 6 || get_bits(insn, 15, 13) == 7) && (get_bits(insn, 1, 0) == 1);

        if (is_rvi_call || is_rvc_call) return TYPE_CALL;
        if (is_rvi_return || is_rvc_return) return TYPE_RET;
        if (is_rvi_branch || is_rvc_branch) return TYPE_BRANCH;
        if (is_rvi_jal || is_rvc_jal) return TYPE_JAL;
        if (is_rvi_jalr || is_rvc_jr_or_jalr) return TYPE_JALR;
        return TYPE_OTHER;
    }

    uint64_t get_pc()
    {
        return next_pc;
    }

    uint64_t get_last_pc()
    {
        return last_pc;
    }

    void set_pc(uint64_t pc)
    {
        next_pc = pc;
    }

    uint32_t get_last_insn()
    {
        return insn;
    }

    bool last_insn_is_taken()
    {
        return taken;
    }

    int get_exit_code()
    {
        return riscv_benchmark_exit_code(m->cpu_state[0]);
    }

    int get_most_recently_written_reg()
    {
        return riscv_get_most_recently_written_reg(m->cpu_state[0]);
    }

    uint64_t get_reg(int rn)
    {
        return riscv_get_reg(m->cpu_state[0], rn);
    }

    void set_reg(uint64_t reg_id, uint64_t val)
    {
        riscv_set_reg(m->cpu_state[0], reg_id, val);
    }

    bool is_last_insn_reading_csr()
    {
        return get_bits(insn, 6, 0) == 0x73 && get_bits(insn, 14, 12) != 0 &&  get_bits(insn, 14, 12) != 4;
    }

    bool get_st_info(uint64_t &addr, uint64_t &data, uint64_t &len)
    {
        addr = m->cpu_state[0]->last_data_paddr;
        data = m->cpu_state[0]->last_data_value;

        switch (get_bits(insn, 14, 12))
        {
            case 0:
                len = 1;
                break;
            case 1:
                len = 2;
                break;
            case 2:
                len = 4;
                break;
            case 3:
                len = 8;
                break;
            default:
                len = 0;
                break;
        }

        return get_bits(insn, 6, 0) == 0x23 && len != 0;
    }

    uint32_t get_insn(uint64_t addr)
    {
        uint32_t ret;
        uint64_t val0, val1;
        riscv_cpu_read_memory(m->cpu_state[0], &val0, addr, 1);
        riscv_cpu_read_memory(m->cpu_state[0], &val1, addr + 2, 1);
        ret = val1;
        ret = (ret << 16) | val0;
        return ret;
    }

    bool get_ld_info(uint64_t &addr, uint64_t &data, uint64_t &len)
    {
        addr = m->cpu_state[0]->last_data_paddr;
        data = m->cpu_state[0]->last_data_value;
        len = 0;
        if (get_bits(insn, 6, 0) == 0x03)
        {
            switch (get_bits(insn, 14, 12))
            {
                case 0b000:
                case 0b100:
                    len = 1;
                    break;
                case 0b001:
                case 0b101:
                    len = 2;
                    break;
                case 0b010:
                case 0b110:
                    len = 4;
                    break;
                case 0b011:
                    len = 8;
                    break;
                default:
                    len = 0;
                    break;
            }
        }
        else if ((get_bits(insn, 1, 0) == 0b00 || get_bits(insn, 1, 0) == 0b10) && get_bits(insn, 15, 13) == 0b011)
        {
            len = 8;
        }
        else if ((get_bits(insn, 1, 0) == 0b00 || get_bits(insn, 1, 0) == 0b10) && get_bits(insn, 15, 13) == 0b010)
        {
            len = 4;
        }
        
        return (len != 0);
    }

private:
    uint32_t insn;
    bool taken; 
    uint64_t next_pc;
    uint64_t last_pc;
    RISCVMachine *m;
private:
    uint64_t get_bits(uint64_t d, uint8_t hi, uint8_t lo) const
    {
        assert(hi >= lo);
        assert(hi <= sizeof(d) * 8);
        return (d >> lo) & ((1ull << (hi - lo + 1)) - 1);
    }
};















#endif
