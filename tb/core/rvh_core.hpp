#ifndef __FRONTEND_H__
#define __FRONTEND_H__
#include "rrvtb.hpp"
#include "dromajo.hpp"
// #include "pseudo_icache.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <algorithm>

class rvh_core;
// static void advance_cb(void *);
// static void reset_cb(void *);
static void clock_gen_cb(void *);
static void clock_cb(void *, const vector<Bits> &);

static constexpr uint64_t catch_addr = 0x800010b8;
uint64_t tohost_addr = 0x80001000;
uint64_t fromhost_addr = 0x80001040;
map<uint64_t, uint64_t> misp_br_map, misp_call_map, misp_ret_map;

class rvh_core
{
public:
    rvh_core(string elfpath, uint64_t boot_address)
    // init SVObject here
    : cnt(0)
    , golden_ghist(0)
    , last_btq_tag(0)
    , last_saw_not_taken_br(false)
    , last_saw_taken_br(false)
    , rst("testbench.rst")
    , boot_addr("testbench.boot_address_i")
    , fe_bound_cnt(0)
    , be_bound_cnt(0)
    , both_avail_cnt(0)
    , both_busy_cnt(0)
    , insn_cnt(0)
    , mem("testbench.u_axi_mem.ram.mem")
    , cmt_valid("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_insn_vld_o")
    , cmt_pc("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_insn_pc_o")
    , cmt_isa_rd("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_insn_isa_rd_o")
    , cmt_phy_rd("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_insn_phy_rd_o")
    , cmt_branch("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_insn_is_br_o")
    , cmt_mispred("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_insn_mispred_o")
    , cmt_exception_valid("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_excp_vld_o")
    , cmt_exception_cause("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_excp_cause_o")
    , cmt_exception_pc("testbench.u_rvh_core.u_rvh_backend.u_rvh_rcu.u_rvh_rob.retire_excp_pc_o")
    , bru_insn_vld("testbench.u_rvh_core.u_rvh_backend.bru_resolve_vld")
    , bru_insn_pc("testbench.u_rvh_core.u_rvh_backend.bru_resolve_pc")
    , bru_insn_btq_tag("testbench.u_rvh_core.u_rvh_backend.bru_resolve_btq_tag")
    , bru_insn_taken("testbench.u_rvh_core.u_rvh_backend.bru_resolve_taken")
    , bru_insn_mispred("testbench.u_rvh_core.u_rvh_backend.bru_resolve_mispred")
    , bru_insn_target("testbench.u_rvh_core.u_rvh_backend.bru_resolve_target")
    , int_prf("testbench.u_rvh_core.u_rvh_backend.u_rvh_iew.u_int_prf.prf_q")
    , dcache_exit_flag("testbench.u_rvh_core.u_rvh_backend.u_rvh_lsu.u_rvh_l1d.l1d_exit_flag")
    , rtl_tohost_addr("testbench.u_rvh_core.u_rvh_backend.u_rvh_lsu.u_rvh_l1d.tohost_addr")
    , update_ghist_info("testbench.u_rvh_core.u_rvh_ifu.update_ghist_info")
    , ghist_queue("testbench.u_rvh_core.u_rvh_ifu.ghist_u.ghist_queue")
    , update_ghist("testbench.u_rvh_core.u_rvh_ifu.ghist_u.update_ghist_o")
    , update_valid("testbench.u_rvh_core.u_rvh_ifu.u_btq.bpu_update_valid_o")
    , update_btq_tag("testbench.u_rvh_core.u_rvh_ifu.u_btq.head")
    , cmt_btq_tag("testbench.u_rvh_core.retire_insn_btq_tag")
    , ifu_dec_inst_vld("testbench.u_rvh_core.ifu_dec_inst_vld")
    , ifu_dec_inst_pc("testbench.u_rvh_core.ifu_dec_inst_pc")
    , ifu_dec_inst("testbench.u_rvh_core.ifu_dec_inst")
    , ifu_dec_is_rvc("testbench.u_rvh_core.u_rvh_ifu.fe_dec_is_rvc_o")
    , fe_dec_ready("testbench.u_rvh_core.u_rvh_ifu.fe_dec_ready_i")
    , elfpath(elfpath)
    , golden(elfpath, 0x80000000)
    {
        // Assign initial value to signal before simulation starts
        uint64_t pos = 0;
        ostringstream ostr;
        ifstream file;
        string line;

        rst = 1;
        boot_addr = boot_address;
        ostr << "../../utils/elf2hex/elf2hex --bit-width 128 --input " << elfpath << " > elf.hex";
        system(ostr.str().c_str());

        file.open("elf.hex", ios::in);
        if (!file.is_open())
            ERROR("Cannot open file: elf.hex\n");
        
        while (getline(file, line))
        {
            if (line.empty()) ERROR("Invalid hex file format!\n");
            if (line.size() != 32) ERROR("The hex file is not in 128-bit format!\n");
            for(int i = 0 ; i < 2; i++) {
                mem(pos*2+i) = "64'h" + line.substr((1-i)*16,16);
            }
            pos += 1;
        }
        file.close();
        system("rm -rf elf.hex");

        ostr.str("");
        ostr << "nm " << elfpath << " > elf.tab";
        INFO("cmd: %s\n", ostr.str().c_str());
        system(ostr.str().c_str());
        file.open("elf.tab", ios::in);
        if (!file.is_open())
            ERROR("Cannot open file: elf.tab\n");
        while (getline(file, line))
        {
            if (line.find("tohost") != std::string::npos) 
            {
                tohost_addr = strtoull(line.c_str(), NULL, 16);
                INFO("tohost found! addr: %x\n", tohost_addr);
                break;
            }
        }
        file.close();
        system("rm -rf elf.tab");

        sem_init(&begin_mtx, 0, 0);
        sem_init(&finish_mtx, 0, 0);
        // register_timer_cb(clock_gen_cb, this, 1, true);
        register_signal_cb(clock_cb, this, {"testbench.clk"});
    }

    void sync_csr_read(uint32_t rtl_isa_rd, uint64_t rtl_rd_value)
    {
        if (golden.is_last_insn_reading_csr())
        {
            uint32_t golden_isa_rd = golden.get_most_recently_written_reg();
            uint64_t golden_rd_value = golden.get_reg(golden_isa_rd);
            if (golden_isa_rd != 0 && golden_isa_rd == rtl_isa_rd && golden_rd_value != rtl_rd_value) 
            {
                INFO("[%d]sync csr reading -> pc[%016lx] golden[%016lx] = our[%016lx]\n", get_time(), golden.get_last_pc(), golden_rd_value, rtl_rd_value);
                golden.set_reg(golden_isa_rd, rtl_rd_value);
            } 
        }
    }

    void sync_fromhost_read(uint32_t rtl_isa_rd, uint64_t rtl_rd_value)
    {
        uint64_t addr;
        uint64_t data;
        uint64_t len;
        bool is_load = golden.get_ld_info(addr, data, len);
        // INFO("time(%d) is_load: %s, access addr: %08x, fromhost addr: %08x\n", 
        //         get_time(), (is_load ? "Y" : "N"), addr, fromhost_addr);
        if (is_load && (addr == fromhost_addr))
        {
            uint32_t golden_isa_rd = golden.get_most_recently_written_reg();
            uint64_t golden_rd_value = golden.get_reg(golden_isa_rd);
            if (golden_isa_rd != 0 && golden_isa_rd == rtl_isa_rd && golden_rd_value != rtl_rd_value) 
            {
                INFO("[%d]sync fromhost reading -> pc[%016lx] golden[%016lx] = our[%016lx]\n", get_time(), golden.get_last_pc(), golden_rd_value, rtl_rd_value);
                golden.set_reg(golden_isa_rd, rtl_rd_value);
            }    
        }
    }

    void print_br_misp_info(map<uint64_t, uint64_t> &im)
    {
        map<uint64_t,uint64_t>::iterator it;
        vector<uint64_t> v;
        for(it=im.begin(); it!=im.end(); ++it)
        {
            v.push_back(it->second);
        }
        sort(v.begin(), v.end(), greater<uint64_t>());
        int n;
        for(int i = 0; (i < v.size()) && (i < 10); i++)
        {
            map<uint64_t,uint64_t>::iterator it;
            for(it = im.begin(); it != im.end(); it++)
            {
                if(v[i]==it->second)
                {
                    fprintf(stderr, "%08x: %d\n", it->first, it->second);
                }
            }
        }
    }

    uint64_t get_ghist(uint64_t ghist_ptr)
    {
        uint64_t ghist_raw;
        uint64_t ghist = 0;
        uint64_t size = ghist_queue.size();
        if (ghist_ptr == 0)
        {
            ghist_raw = ghist_queue(size - 1, size - 64).value().to_ulong();
        }
        else if (ghist_ptr < 64)
        {
            uint64_t head = size - (64 - ghist_ptr);
            uint64_t ghist_upper;
            ghist_raw = (ghist_queue >> head).to_ulong();
            ghist_upper = (ghist_queue & ((1ULL << ghist_ptr) - 1)).to_ulong();
            ghist_upper <<= size - head;
            ghist_raw |= ghist_upper;
        }
        else 
        {
            ghist_raw = ghist_queue(ghist_ptr - 1, ghist_ptr - 64).value().to_ulong();
        }

        for (int i = 0; i < 64; i++)
        {
            ghist <<= 1;
            if ((ghist_raw >> i) & 0x1)
            {
                ghist |= 1;
            } 
        }

        return ghist;
    }

    void check_ghist(uint64_t pc, uint64_t cmt_btq_tag)
    {
        uint64_t ghist_ptr;
        if (cmt_btq_tag != last_btq_tag)
        {
            if (last_saw_taken_br)
            {
                golden_ghist = (golden_ghist << 1) | 1;
            }
            else if (last_saw_not_taken_br) 
            {
                golden_ghist = (golden_ghist << 1);
            }
            last_btq_tag = cmt_btq_tag;
            last_saw_taken_br = false;
            last_saw_not_taken_br = false;
            ghist_map[cmt_btq_tag] = golden_ghist;
        }
         // check ghist
        ghist_ptr = update_ghist_info["tail_ptr"].value().to_ulong();
        uint64_t calc_ghist = get_ghist(ghist_ptr);
        uint64_t rtl_ghist = update_ghist.value().to_ulong();
        uint64_t t_update_btq_tag = update_btq_tag.value().to_ulong();
        if (update_valid == 1) 
        {
             if (calc_ghist != rtl_ghist) 
            {
                INFO("(%d) ghist calc inconsistancy! pc: %08x, btq_tag: %d, calc_ghist: %016llx, rtl: %016llx, ptr: 0x%x\n", 
                        get_time(), pc, t_update_btq_tag, calc_ghist, rtl_ghist, ghist_ptr);
            }
            if (rtl_ghist != ghist_map[t_update_btq_tag]) 
            {
                INFO("(%d) ghist inconsistancy! pc: %08x, btq_tag: %d, golden: %016llx, rtl: %016llx, ptr: 0x%x\n", 
                        get_time(), pc, t_update_btq_tag, ghist_map[t_update_btq_tag], rtl_ghist, ghist_ptr);
            }
        }
    }

    void tmp_collect_bru_info()
    {
        uint64_t btq_tag = bru_insn_btq_tag.value().to_ulong();
        uint64_t ghist = get_ghist(btq_tag);
        if ((bru_insn_vld == 1))
        {
            if (bru_insn_mispred == 1)
            {
                if (bru_insn_pc == catch_addr)
                {
                    if (bru_insn_taken == 1)
                    {
                        fprintf(stderr, "(%d) [%016llx] %08x Misp pred[%s] should be [%s]\n", get_time(), ghist, bru_insn_pc.value().to_ulong(), "NT", "T ");
                    }
                    else
                    {
                        fprintf(stderr, "(%d) [%016llx] %08x Misp pred[%s] should be [%s]\n", get_time(), ghist, bru_insn_pc.value().to_ulong(), "T ", "NT");
                    }
                }
            }
            else if (bru_insn_pc == catch_addr)
            {
                if (bru_insn_taken == 1)
                {
                    fprintf(stderr, "(%d) [%016llx] %08x Corr pred[%s]\n", get_time(), ghist, bru_insn_pc.value().to_ulong(), "T ");
                }
                else
                {
                    fprintf(stderr, "(%d) [%016llx] %08x Corr pred[%s]\n", get_time(), ghist, bru_insn_pc.value().to_ulong(), "NT");
                }
            }
        }
    }

    void get_max_sub_str(const string &str)
    {
        int Max = 0;
        std::vector<std::vector<int>> dp(2, std::vector<int>(str.size()));
        for(int i = 1; i < str.size(); ++i)
        {
            for(int j = i+2; j < str.size(); ++j)
            {
                if(str[j] == str[i])    dp[i&1][j] = dp[1^i&1][j-1]+1;
                else dp[i&1][j] = 0;
                if(dp[i&1][j] >= j-i)   dp[i&1][j] = j-i-1;
                Max = max(Max, dp[i&1][j]);
            }
        }
        printf("%d", Max+1);
        return ;
    }

    void check_frontend()
    {
        for (int i = 0; i < 4; i++) 
        {
            if (ifu_dec_inst_vld(i) == 1) 
            {
                uint32_t insn_data;
                insn_data = golden.get_insn(ifu_dec_inst_pc(i).value().to_ulong());
                if (ifu_dec_is_rvc(i) == 1) 
                {
                    if ((ifu_dec_inst(i).value().to_ulong() & 0xffff) != (insn_data & 0xffff))
                    {
                        ERROR("[%d] <> instret[%d] -> instruction inconsistancy pc[%016lx] our[%016lx] golden[%016lx]\n", get_time(), minstret, 
                                                            ifu_dec_inst_pc(i).value().to_ulong(), ifu_dec_inst(i).value().to_ulong() & 0xffff, insn_data & 0xffff);
                    }
                }
                else 
                {
                    if ((ifu_dec_inst(i).value().to_ulong() & 0xffffffff) != insn_data)
                    {
                        ERROR("[%d] <> instret[%d] -> instruction inconsistancy pc[%016lx] our[%016lx] golden[%016lx]\n", get_time(), minstret, 
                                                            ifu_dec_inst_pc(i).value().to_ulong(), ifu_dec_inst(i).value().to_ulong() & 0xffffffff, insn_data);
                    }
                }
            }
        }
        
    }

    // void loop_compare()
    // {
    //     uint64_t tage_rslt, loop_rslt, align_pc;
    //     tage_rslt = (composed_tage_pred_b2["valid"] & composed_tage_pred_b2["taken"]).to_ulong();
    //     loop_rslt = (composed_tage_pred_b2["valid"] & loop_pred_b2).to_ulong();
    //     align_pc = pc_b2.value().to_ulong() & ~0xfULL;
    //     for (int i = 0; i < 8; i++)
    //     {
    //         if ((tage_rslt ^ loop_rslt) & 0x1)
    //         {
    //             fprintf(stderr, "[%d] loop diff at: %08x. tage: %d, loop: %d\n", get_time(), 
    //                             align_pc + i * 2, tage_rslt & 0x1, loop_rslt & 0x1);
    //             break;
    //         }
    //         tage_rslt >>= 1;
    //         loop_rslt >>= 1;
    //     }
    // }

    void stall_static()
    {
        if ((ifu_dec_inst_vld != 0) && (fe_dec_ready == 1))
        {
            both_avail_cnt++;
            if (ifu_dec_inst_vld == 0x1) insn_cnt += 1;
            else if (ifu_dec_inst_vld == 0x3) insn_cnt += 2;
            else if (ifu_dec_inst_vld == 0x7) insn_cnt += 3;
            else if (ifu_dec_inst_vld == 0xf) insn_cnt += 4;
            else assert(0);
        }
        else if ((ifu_dec_inst_vld != 0) && (fe_dec_ready == 0))
        {
            be_bound_cnt++;
        }
        else if ((ifu_dec_inst_vld == 0) && (fe_dec_ready == 1))
        {
            fe_bound_cnt++;
        }
        else if ((ifu_dec_inst_vld == 0) && (fe_dec_ready == 0))
        {
            both_busy_cnt++;
        }
        else 
        {
            assert(0);
        }
    }

    void Evaluate()
    {
        // icache.do_req();
        // icache.send_req();
        // icache.Evaluate();
        // loop_compare();
        // if ((minstret % 2000) == 0) fprintf(stderr, "[%d] instret: %d\n", get_time(), minstret);
        tmp_collect_bru_info();
        stall_static();
        // check_frontend();
#ifndef COSIM
        if (get_time() % 50000 == 0)
        {
            INFO("[%d] <> instret[%d]\n", get_time(), minstret);
        }
#endif
        if((cmt_exception_valid == 1) && (cmt_exception_cause != 24)) {
            minstret++;
#ifdef COSIM
            uint64_t t_exception_cmt_pc = cmt_exception_pc.value().to_ulong();
            if (t_exception_cmt_pc != (golden.get_pc() & 0x7fffffffff)) 
            {
                ERROR("[%d] <> instret[%d] -> pc inconsistancy@%016lx: golden: %016lx\n", get_time(), minstret, t_exception_cmt_pc, golden.get_pc());
            }
            // INFO("[%d] <> instret[%d] -> cmt exception[%016lx]\n",get_time(), minstret, t_exception_cmt_pc);
            golden.iterate();
#endif
        } else {
            for (int i = 0; i < 4; i++)
            {
                if (cmt_valid(i) == 1)
                {
                    if(cmt_branch(i) == 1){
                        cfi_cnt++;
                        if(cmt_mispred(i) == 1){
                            mispred_cnt++;
                        }
                    }
                    minstret++;
#ifdef COSIM
                    uint64_t t_cmt_pc = cmt_pc(i).value().to_ulong();
                    uint64_t t_cmt_btq_tag = cmt_btq_tag(i).value().to_ulong();

                    if (t_cmt_pc != (golden.get_pc() & 0x7fffffffff)) 
                    {
                        ERROR("[%d] <> instret[%d] -> pc inconsistancy@%016lx: golden: %016lx\n", get_time(), minstret, t_cmt_pc, golden.get_pc());
                    }

                    check_ghist(t_cmt_pc, t_cmt_btq_tag);

                    golden.iterate();

                    switch (golden.get_last_insn_cfi_type())
                    {
                        case Dromajo::TYPE_CALL:
                            call_cnt++;
                            if (cmt_mispred(i) == 1) 
                            {
                                misp_call_cnt++;
                                if (misp_call_map.find(t_cmt_pc) != misp_call_map.end())
                                {
                                    misp_call_map[t_cmt_pc]++;
                                }
                                else 
                                {
                                    misp_call_map[t_cmt_pc] = 1;
                                }
                            }
                            break;
                        case Dromajo::TYPE_RET:
                            ret_cnt++;
                            if (cmt_mispred(i) == 1) 
                            {
                                misp_ret_cnt++;
                                if (misp_ret_map.find(t_cmt_pc) != misp_ret_map.end())
                                {
                                    misp_ret_map[t_cmt_pc]++;
                                }
                                else 
                                {
                                    misp_ret_map[t_cmt_pc] = 1;
                                }
                            }
                            break;
                        case Dromajo::TYPE_BRANCH:
                            br_cnt++;
                            if (cmt_mispred(i) == 1) 
                            {
                                misp_br_cnt++;
                                if (misp_br_map.find(t_cmt_pc) != misp_br_map.end())
                                {
                                    misp_br_map[t_cmt_pc]++;
                                }
                                else 
                                {
                                    misp_br_map[t_cmt_pc] = 1;
                                }
                            }
                            break;
                        case Dromajo::TYPE_JAL:
                            jal_cnt++;
                            if(cmt_mispred(i) == 1) misp_jal_cnt++;
                            break;
                        case Dromajo::TYPE_JALR:
                            jalr_cnt++;
                            if(cmt_mispred(i) == 1) misp_jalr_cnt++;
                            break;
                        default:
                            other_cnt++;
                            break;
                    }

                    // collect br info
                    if (golden.get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH)
                    {
                        if (golden.last_insn_is_taken())
                        {
                            last_saw_taken_br = true;
                        }
                        else 
                        {
                            last_saw_not_taken_br = true;
                        }
                    }

                    uint32_t t_cmt_isa_rd = cmt_isa_rd(i).value().to_ulong();
                    uint32_t t_cmt_phy_rd = cmt_phy_rd(i).value().to_ulong();
                    uint64_t t_cmt_rd_value = int_prf(t_cmt_phy_rd).value().to_ulong();

                    sync_csr_read(t_cmt_isa_rd, t_cmt_rd_value);
                    sync_fromhost_read(t_cmt_isa_rd, t_cmt_rd_value);

                    uint32_t golden_isa_rd = golden.get_most_recently_written_reg();
                    uint64_t golden_rd_value = golden.get_reg(golden_isa_rd);

                    if (t_cmt_isa_rd != 0) {
                        if(t_cmt_isa_rd != golden_isa_rd) {
                            ERROR("[%d] <> instret[%d] -> rd inconsistancy pc[%016lx] our[%016lx] golden[%016lx]\n", get_time(), minstret, t_cmt_pc, t_cmt_isa_rd, golden_isa_rd);
                        } else {
                            if(t_cmt_rd_value != golden_rd_value){
                                ERROR("[%d] <> instret[%d] -> value inconsistancy pc[%016lx] rd[%d] prd[%x] our[%016lx] golden[%016lx]\n", get_time(), minstret, t_cmt_pc, t_cmt_isa_rd, t_cmt_phy_rd , t_cmt_rd_value, golden_rd_value);
                            }
                        }
                    }
                    if(t_cmt_isa_rd != 0){
                        INFO("[%d] <> instret[%d] cmt insn[%016lx] -> write rd[%d] prd[%x] 0x%016lx\n",get_time(),minstret, t_cmt_pc,t_cmt_isa_rd, t_cmt_phy_rd, t_cmt_rd_value);
                    } else {
                        INFO("[%d] <> instret[%d] cmt insn[%016lx]\n",get_time(),minstret, t_cmt_pc);
                    }
#else                   
                    uint32_t t_cmt_isa_rd = cmt_isa_rd(i).value().to_ulong();
                    uint32_t t_cmt_phy_rd = cmt_phy_rd(i).value().to_ulong();
                    uint64_t t_cmt_rd_value = int_prf(t_cmt_phy_rd).value().to_ulong();
                    uint64_t t_cmt_pc = cmt_pc(i).value().to_ulong();
                    uint64_t t_cmt_btq_tag = cmt_btq_tag(i).value().to_ulong();

                    if(t_cmt_isa_rd != 0){
                        INFO("[%d] <> instret[%d] cmt insn[%016lx] -> write rd[%d] prd[%x] 0x%016lx\n",get_time(),minstret, t_cmt_pc,t_cmt_isa_rd, t_cmt_phy_rd, t_cmt_rd_value);
                    } else {
                        INFO("[%d] <> instret[%d] cmt insn[%016lx]\n",get_time(),minstret, t_cmt_pc);
                    }
#endif
                }
            }
        }
    }

    void Advance()
    {
        int begin, finish;
        sem_getvalue(&begin_mtx, &begin);
        assert(begin == 0);
        sem_getvalue(&finish_mtx, &finish);
        assert(finish == 0);
        sem_post(&begin_mtx);
        sem_wait(&finish_mtx);
    }
    char read_byte(uint64_t addr) {
        char data = read_double(addr) & 0xff;
        return data;
    }
    uint64_t read_double(uint64_t addr) {
        uint64_t data = 0;
        data = (mem((addr - 0x80000000)/8).value().to_ulong() & 0xffffffffffffffff);
        return data;
    }
    void write_double(uint64_t addr, uint64_t data) {
        mem((addr - 0x80000000)/8) =  (data) & 0xffffffffffffffff;
    }

    void try_exit(ofstream &of)
    {
        uint64_t tohost = read_double(tohost_addr);
        uint64_t exit_flag = dcache_exit_flag.value().to_ulong();
        if(tohost || exit_flag) {
            if((tohost & 0x1) || exit_flag){
                uint64_t exit_code = tohost >> 1;
                if(exit_code) {
                    fprintf(stderr,"%s : FAIL, ExitCode = [%d]\n", elfpath.c_str(), exit_code);
                    if (of.is_open()) of << elfpath.c_str() << " : FAIL, ExitCode = [" << exit_code << "]" << endl;
                } else {
                    fprintf(stderr,"%s : PASS, ExitCode = [%d]\n", elfpath.c_str(), exit_code);
                    if (of.is_open()) of << elfpath.c_str() << " : PASS, ExitCode = [" << exit_code << "]" << endl;
                }
                fprintf(stderr, "\nTotal instret[%d] -> Total cfi instret[%d] --- Misprediction instret[%d] \n",minstret,cfi_cnt,mispred_cnt);
                fprintf(stderr, "call cnt: %d, misp: %d, corr rate: %f\n", call_cnt, misp_call_cnt, (float)(call_cnt - misp_call_cnt) / (float)call_cnt);
                fprintf(stderr, "ret  cnt: %d, misp: %d, corr rate: %f\n", ret_cnt, misp_ret_cnt, (float)(ret_cnt - misp_ret_cnt) / (float)ret_cnt);
                fprintf(stderr, "br   cnt: %d, misp: %d, corr rate: %f\n", br_cnt, misp_br_cnt, (float)(br_cnt - misp_br_cnt) / (float)br_cnt);
                fprintf(stderr, "jal  cnt: %d, misp: %d, corr rate: %f\n", jal_cnt, misp_jal_cnt, (float)(jal_cnt - misp_jal_cnt) / (float)jal_cnt);
                fprintf(stderr, "jalr cnt: %d, misp: %d, corr rate: %f\n", jalr_cnt, misp_jalr_cnt, (float)(jalr_cnt - misp_jalr_cnt) / (float)jalr_cnt);
                fprintf(stderr, "total cfi cnt: %d, total cnt: %d\n", 
                                    call_cnt + ret_cnt + br_cnt + jal_cnt + jalr_cnt, 
                                    call_cnt + ret_cnt + br_cnt + jal_cnt + jalr_cnt + other_cnt);
                fprintf(stderr, "Top 10 misp branch: \n");
                print_br_misp_info(misp_br_map);
                fprintf(stderr, "Top 10 misp call: \n");
                print_br_misp_info(misp_call_map);
                fprintf(stderr, "Top 10 misp ret: \n");
                print_br_misp_info(misp_ret_map);
                fprintf(stderr, "stall static: \n");
                fprintf(stderr, "fe_bound: %d, be_bound: %d, both_avail: %d, both_busy: %d, pass_insn: %d\n", fe_bound_cnt, be_bound_cnt, both_avail_cnt, both_busy_cnt, insn_cnt);
                finish();
            } else {
                uint64_t magic_mem = read_double(tohost_addr);
                bool print_event = read_double(magic_mem) == 64;
                fprintf(stderr, "magic_mem: %x, print_event: %d\n", magic_mem, print_event);
                if(print_event){
                    uint64_t stringAddr = read_double(magic_mem + 16);
                    uint64_t stringLen  = read_double(magic_mem + 24);
                    std::stringstream outString;
                    fprintf(stderr, "stringAddr: %x, strlen: %d\n", stringAddr, stringLen);
                    for(int i = 0 ; i < stringLen; i++) {
                        outString << read_byte(stringAddr + i);
                    }
                    write_double(fromhost_addr,1);
                    write_double(tohost_addr,0);
                    fprintf(stderr,"%s",outString.str().c_str());
                }
            }
        }
    }

    void Reset()
    {
        sem_wait(&finish_mtx);
        minstret = 0;
        cfi_cnt = 0;
        mispred_cnt = 0;

        misp_call_cnt = 0;
        misp_ret_cnt = 0;
        misp_br_cnt = 0;
        misp_jal_cnt = 0;
        misp_jalr_cnt = 0;

        call_cnt = 0;
        ret_cnt = 0;
        br_cnt = 0;
        jal_cnt = 0;
        jalr_cnt = 0;
        other_cnt = 0;

        rtl_tohost_addr = tohost_addr;
        rst = 0;
    }

    friend void clock_cb(void *p, const vector<Bits> &sigs)
    {
        static bool has_last = false;
        static int last_sigs = 0;
        rvh_core *pcore = (rvh_core*)p;
        int newsigs = sigs[0] == 1 ? 1 : 0;
        if (sigs[0] == 1)
        {
            sem_post(&pcore->finish_mtx);
            sem_wait(&pcore->begin_mtx);
        }
        assert(!has_last || (has_last && last_sigs != newsigs));
        has_last = true;
        last_sigs = sigs[0] == 1 ? 1 : 0;
    }

    ~rvh_core();
private:

    uint64_t cnt;
    SVObject rst;

    SVObject boot_addr;

    // PseudoIcache    icache;
    SVObject mem;
    SVObject cmt_valid;
    SVObject cmt_pc;
    SVObject cmt_isa_rd;
    SVObject cmt_phy_rd;
    SVObject cmt_branch;
    SVObject cmt_mispred;
    SVObject cmt_exception_valid;
    SVObject cmt_exception_cause;
    SVObject cmt_exception_pc;
    SVObject bru_insn_vld;
    SVObject bru_insn_pc;
    SVObject bru_insn_taken;
    SVObject bru_insn_mispred;
    SVObject bru_insn_target;
    SVObject bru_insn_btq_tag;
    SVObject int_prf;
    SVObject dcache_exit_flag;
    SVObject rtl_tohost_addr;
    SVObject cmt_btq_tag;
    SVObject ghist_ram;
    SVObject ifu_dec_inst_vld;
    SVObject ifu_dec_inst_pc;
    SVObject ifu_dec_inst;
    SVObject ifu_dec_is_rvc;
    SVObject fe_dec_ready;

    SVObject update_ghist_info;
    SVObject ghist_queue;
    SVObject update_ghist;
    SVObject update_valid;
    SVObject update_btq_tag;

    string   elfpath;
    uint64_t minstret;
    uint64_t cfi_cnt;
    uint64_t mispred_cnt;

    uint64_t misp_call_cnt;
    uint64_t misp_ret_cnt;
    uint64_t misp_br_cnt;
    uint64_t misp_jal_cnt;
    uint64_t misp_jalr_cnt;

    uint64_t call_cnt;
    uint64_t ret_cnt;
    uint64_t br_cnt;
    uint64_t jal_cnt;
    uint64_t jalr_cnt;
    uint64_t other_cnt;

    uint64_t golden_ghist;

    uint64_t fe_bound_cnt;
    uint64_t be_bound_cnt;
    uint64_t both_avail_cnt;
    uint64_t both_busy_cnt;
    uint64_t insn_cnt;

    uint64_t last_btq_tag;
    bool last_saw_not_taken_br;
    bool last_saw_taken_br;
    map<uint64_t, uint64_t> ghist_map;

    Dromajo golden;

    sem_t           finish_mtx;
    sem_t           begin_mtx;
};











#endif