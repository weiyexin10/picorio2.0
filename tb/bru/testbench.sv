module testbench
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import riscv_pkg::*;
(

);

    logic clk;
    logic rst;
    logic deque_en;

    initial begin
        $vcdpluson();
        // $fsdbDumpvars(0, testbench);
    end
    
    initial begin
        clk = 0;
        forever begin
            #1;
            clk = 1;
            #1;
            clk = 0;
        end
    end

    logic                          issue_vld_i;
    logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i;
    logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i;
    logic [      BRU_OP_WIDTH-1:0] issue_opcode_i;
    logic                          issue_is_rvc_i;
    logic [              XLEN-1:0] issue_operand0_i;
    logic [              XLEN-1:0] issue_operand1_i;
    logic [       VADDR_WIDTH-1:0] issue_pc_i;
    logic [                  31:0] issue_imm_i;
    logic                          issue_pred_taken_i;
    logic [       VADDR_WIDTH-1:0] issue_pred_target_i;
    logic                          issue_rdy_o;

    // Write Back
    logic                          wb_vld_o;
    logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o;
    logic [              XLEN-1:0] wb_data_o;
    logic                          wb_mispred_o;
    logic [              XLEN-1:0] wb_target_o;
    logic                          wb_rdy_i;

    logic flush_i;


    rvh_bru bru_u(
        // Issue
        .issue_vld_i,
        .issue_rob_tag_i,
        .issue_phy_rd_i,
        .issue_opcode_i,
        .issue_is_rvc_i,
        .issue_operand0_i,
        .issue_operand1_i,
        .issue_pc_i,
        .issue_imm_i,
        .issue_pred_taken_i,
        .issue_pred_target_i,
        .issue_rdy_o,

        // Write Back
        .wb_vld_o,
        .wb_phy_rd_o,
        .wb_data_o,
        .wb_mispred_o,
        .wb_target_o,
        .wb_rdy_i,

        .flush_i,

        .clk,
        .rst
    );


endmodule

