module testbench
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import riscv_pkg::*;
(

);

    logic clk;
    logic rst;
    logic deque_en;

    initial begin
        $vcdpluson();
        // $fsdbDumpvars(0, testbench);
    end
    
    initial begin
        clk = 0;
        forever begin
            #1;
            clk = 1;
            #1;
            clk = 0;
        end
    end

    logic                          issue_vld_i;
    logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i;
    logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i;
    logic [      ALU_OP_WIDTH-1:0] issue_opcode_i;
    logic [              rvh_pkg::XLEN-1:0] issue_operand0_i;
    logic [              rvh_pkg::XLEN-1:0] issue_operand1_i;
    logic                          issue_rdy_o;

    logic                          wb_vld_o;
    logic [     ROB_TAG_WIDTH-1:0] wb_rob_tag_o;
    logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o;
    logic [              rvh_pkg::XLEN-1:0] wb_data_o;
    logic                          wb_rdy_i;

    logic                          flush_i;

    rvh_alu alu_u(
        // Issue
        .issue_vld_i,
        .issue_rob_tag_i,
        .issue_phy_rd_i,
        .issue_opcode_i,
        .issue_operand0_i,
        .issue_operand1_i,
        .issue_rdy_o,

        // Write Back
        .wb_vld_o,
        .wb_rob_tag_o,
        .wb_phy_rd_o,
        .wb_data_o,
        .wb_rdy_i,

        .flush_i,

        .clk,
        .rst
    );


endmodule

