TEST_RR_OP( 2, div_,  3,  20,   6 )
TEST_RR_OP( 3, div_, -3, -20,   6 )
TEST_RR_OP( 4, div_, -3,  20,  -6 )
TEST_RR_OP( 5, div_,  3, -20,  -6 )
TEST_RR_OP( 6, div_, -1<<63, -1<<63,  1 )
TEST_RR_OP( 7, div_, -1<<63, -1<<63, -1 )
TEST_RR_OP( 8, div_, -1, -1<<63, 0 )
TEST_RR_OP( 9, div_, -1,      1, 0 )
TEST_RR_OP(10, div_, -1,      0, 0 )
TEST_RR_OP( 2, divu,                   3,  20,   6 )
TEST_RR_OP( 3, divu, 3074457345618258599, -20,   6 )
TEST_RR_OP( 4, divu,                   0,  20,  -6 )
TEST_RR_OP( 5, divu,                   0, -20,  -6 )
TEST_RR_OP( 6, divu, -1<<63, -1<<63,  1 )
TEST_RR_OP( 7, divu,     0,  -1<<63, -1 )
TEST_RR_OP( 8, divu, -1, -1<<63, 0 )
TEST_RR_OP( 9, divu, -1,      1, 0 )
TEST_RR_OP(10, divu, -1,      0, 0 )
TEST_RR_OP( 2, divuw,         3,  20,   6 )
TEST_RR_OP( 3, divuw, 715827879, -20 << 32 >> 32,   6 )
TEST_RR_OP( 4, divuw,         0,  20,  -6 )
TEST_RR_OP( 5, divuw,         0, -20,  -6 )
TEST_RR_OP( 6, divuw, -1<<31, -1<<31,  1 )
TEST_RR_OP( 7, divuw, 0,      -1<<31, -1 )
TEST_RR_OP( 8, divuw, -1, -1<<31, 0 )
TEST_RR_OP( 9, divuw, -1,      1, 0 )
TEST_RR_OP(10, divuw, -1,      0, 0 )
TEST_RR_OP( 2, divw,  3,  20,   6 )
TEST_RR_OP( 3, divw, -3, -20,   6 )
TEST_RR_OP( 4, divw, -3,  20,  -6 )
TEST_RR_OP( 5, divw,  3, -20,  -6 )
TEST_RR_OP( 6, divw, -1<<31, -1<<31,  1 )
TEST_RR_OP( 7, divw, -1<<31, -1<<31, -1 )
TEST_RR_OP( 8, divw, -1, -1<<31, 0 )
TEST_RR_OP( 9, divw, -1,      1, 0 )
TEST_RR_OP(10, divw, -1,      0, 0 )
TEST_RR_OP( 2, rem,  2,  20,   6 )
TEST_RR_OP( 3, rem, -2, -20,   6 )
TEST_RR_OP( 4, rem,  2,  20,  -6 )
TEST_RR_OP( 5, rem, -2, -20,  -6 )
TEST_RR_OP( 6, rem,  0, -1<<63,  1 )
TEST_RR_OP( 7, rem,  0, -1<<63, -1 )
TEST_RR_OP( 8, rem, -1<<63, -1<<63, 0 )
TEST_RR_OP( 9, rem,      1,      1, 0 )
TEST_RR_OP(10, rem,      0,      0, 0 )
TEST_RR_OP( 2, remu,   2,  20,   6 )
TEST_RR_OP( 3, remu,   2, -20,   6 )
TEST_RR_OP( 4, remu,  20,  20,  -6 )
TEST_RR_OP( 5, remu, -20, -20,  -6 )
TEST_RR_OP( 6, remu,      0, -1<<63,  1 )
TEST_RR_OP( 7, remu, -1<<63, -1<<63, -1 )
TEST_RR_OP( 8, remu, -1<<63, -1<<63, 0 )
TEST_RR_OP( 9, remu,      1,      1, 0 )
TEST_RR_OP(10, remu,      0,      0, 0 )
TEST_RR_OP( 2, remuw,   2,  20,   6 )
TEST_RR_OP( 3, remuw,   2, -20,   6 )
TEST_RR_OP( 4, remuw,  20,  20,  -6 )
TEST_RR_OP( 5, remuw, -20, -20,  -6 )
TEST_RR_OP( 6, remuw,      0, -1<<31,  1 )
TEST_RR_OP( 7, remuw, -1<<31, -1<<31, -1 )
TEST_RR_OP( 8, remuw, -1<<31, -1<<31, 0 )
TEST_RR_OP( 9, remuw,      1,      1, 0 )
TEST_RR_OP(10, remuw,      0,      0, 0 )
TEST_RR_OP( 2, remw,  2,  20,   6 )
TEST_RR_OP( 3, remw, -2, -20,   6 )
TEST_RR_OP( 4, remw,  2,  20,  -6 )
TEST_RR_OP( 5, remw, -2, -20,  -6 )
TEST_RR_OP( 6, remw,  0, -1<<31,  1 )
TEST_RR_OP( 7, remw,  0, -1<<31, -1 )
TEST_RR_OP( 8, remw, -1<<31, -1<<31, 0 )
TEST_RR_OP( 9, remw,      1,      1, 0 )
TEST_RR_OP(10, remw,      0,      0, 0 )
TEST_RR_OP(11, remw, 0xfffffffffffff897,0xfffffffffffff897, 0 )
// the case below has been tested above. we list them here in order to wait for the output of last two case

