#ifndef __DUT_H__
#define __DUT_H__
#include "rrvtb.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>


class Dut;
static void clock_cb(void *, const vector<Bits> &);

class Dut
{
public:
    Dut()
    // init SVObject here
    : issue_vld_i("testbench.issue_vld_i")
    , issue_rob_tag_i("testbench.issue_rob_tag_i")
    , issue_phy_rd_i("testbench.issue_phy_rd_i")
    , issue_opcode_i("testbench.issue_opcode_i")
    , issue_operand0_i("testbench.issue_operand0_i")
    , issue_operand1_i("testbench.issue_operand1_i")
    , issue_rdy_o("testbench.issue_rdy_o")
    , wb_vld_o("testbench.wb_vld_o")
    , wb_rob_tag_o("testbench.wb_rob_tag_o")
    , wb_phy_rd_o("testbench.wb_phy_rd_o")
    , wb_data_o("testbench.wb_data_o")
    , wb_rdy_i("testbench.wb_rdy_i")
    , flush_i("testbench.flush_i")
    , rst("testbench.rst")
    {
        rst = 1;
        flush_i = 0;
        wb_rdy_i = 1;
        sem_init(&begin_mtx, 0, 0);
        sem_init(&finish_mtx, 0, 0);
        register_signal_cb(clock_cb, this, {"testbench.clk"});
    }
    
    bool Input(uint64_t no, uint64_t type, uint64_t op1, uint64_t op2)
    {
        static bool last_ready = false;
        static uint64_t cnt = 0;
        if (issue_rdy_o != 1) 
        {
            cnt = 0;
            last_ready = false;
            return false;
        }

        if (cnt >= 10) 
        {
            issue_vld_i = 1;
            issue_rob_tag_i = no;
            issue_phy_rd_i = 1;
            issue_opcode_i = type;
            issue_operand0_i = op1;
            issue_operand1_i = op2;
            cnt = 0;

            last_ready = true;
            return true;
        }
        else 
        {
            issue_vld_i = 0;
            cnt++;
            last_ready = true;
            return false;
        }
    }

    bool Output(uint64_t &no, uint64_t &rslt)
    {
        if (wb_vld_o != 1) return false;
        no = wb_rob_tag_o.value().to_ulong();
        rslt = wb_data_o.value().to_ulong();
        return true;
    }

    void Advance()
    {
        int begin, finish;
        sem_getvalue(&begin_mtx, &begin);
        assert(begin == 0);
        sem_getvalue(&finish_mtx, &finish);
        assert(finish == 0);
        sem_post(&begin_mtx);
        sem_wait(&finish_mtx);
    }

    void Reset()
    {
        sem_wait(&finish_mtx);
        rst = 0;
    }

    friend void clock_cb(void *p, const vector<Bits> &sigs)
    {
        static bool has_last = false;
        static int last_sigs = 0;
        Dut *pDut = (Dut*)p;
        int newsigs = sigs[0] == 1 ? 1 : 0;
        if (sigs[0] == 1)
        {
            sem_post(&pDut->finish_mtx);
            sem_wait(&pDut->begin_mtx);
        }
        assert(!has_last || (has_last && last_sigs != newsigs));
        has_last = true;
        last_sigs = sigs[0] == 1 ? 1 : 0;
    }

    ~Dut();
private:

    uint64_t cnt;

    SVObject issue_vld_i;
    SVObject issue_rob_tag_i;
    SVObject issue_phy_rd_i;
    SVObject issue_opcode_i;
    SVObject issue_operand0_i;
    SVObject issue_operand1_i;
    SVObject issue_rdy_o;
    SVObject wb_vld_o;
    SVObject wb_rob_tag_o;
    SVObject wb_phy_rd_o;
    SVObject wb_data_o;
    SVObject wb_rdy_i;
    SVObject flush_i;
    SVObject rst;

    sem_t           finish_mtx;
    sem_t           begin_mtx;
};











#endif