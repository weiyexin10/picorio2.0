#include "rrvtb.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <pthread.h>
#include "dut.hpp"
#include <queue>
using namespace std;

Dut *dut;
pthread_t model_thread, idle_thread;

enum {
    div_,
    divu,
    rem,
    remu,
    // RV64 Only
    divw,
    divuw,
    remw,
    remuw
};

typedef struct {
    uint64_t type;
    uint64_t op1;
    uint64_t op2;
    uint64_t rslt;
} test_case_t;

#ifndef TEST_RR_OP
    #define TEST_RR_OP(testnum, type, result, val1, val2 ) {type, (uint64_t)val1, (uint64_t)val2, (uint64_t)result},
    vector<test_case_t> test_case = {
        #include "testcase.h"
    };
    #undef TEST_RR_OP
#endif

void *model_fn(void *arg)
{
    uint64_t cnt = 0;
    dut->Reset();
    while (true)
    {
        if (dut->Input(cnt, test_case[cnt].type, test_case[cnt].op1, test_case[cnt].op2))
        {
            cnt++;
        }
        uint64_t no, rslt;
        if (dut->Output(no, rslt))
        {
            if (test_case[no].rslt == rslt)
            {
                if (test_case[no].type == div_ || test_case[no].type == divu || test_case[no].type == divw || test_case[no].type == divuw)
                    fprintf(stderr, "case[%d] PASS! (%llx / %llx = %llx)\n", no, test_case[no].op1, test_case[no].op2, test_case[no].rslt);
                else 
                    fprintf(stderr, "case[%d] PASS! (%llx %% %llx = %llx)\n", no, test_case[no].op1, test_case[no].op2, test_case[no].rslt);
            }
            else 
            {
                if (test_case[no].type == div_ || test_case[no].type == divu || test_case[no].type == divw || test_case[no].type == divuw)
                    fprintf(stderr, "case[%d] FAIL! (%llx / %llx = %llx. should be %llx)\n", no, test_case[no].op1, test_case[no].op2, rslt, test_case[no].rslt);
                else 
                    fprintf(stderr, "case[%d] FAIL! (%llx %% %llx = %llx. should be %llx)\n", no, test_case[no].op1, test_case[no].op2, rslt, test_case[no].rslt);
            }
        }   
        dut->Advance();
        if (no >= test_case.size() - 1) break;
    }
    finish();
	return((void *)0);
}

void *idle_fn(void *arg)
{
    sleep(60);
    ERROR("Timeout exit!\n");
}

initial(initial_func)
{
    dut = new Dut();
    int err = pthread_create(&model_thread, NULL, &model_fn, NULL);
	if (err != 0)
		ERROR("Can't create model thread\n");
    err = pthread_create(&idle_thread, NULL, &idle_fn, NULL);
	if (err != 0)
		ERROR("Can't create idle thread\n");
    sleep(0);
}

