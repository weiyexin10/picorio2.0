module frontend_wrapper
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_l1d_pkg::*;
#(
    parameter AXI_ID_WIDTH   = 0,
    parameter AXI_ADDR_WIDTH = 64,
    parameter AXI_DATA_WIDTH = 512
) (
    input  logic             [       XLEN-1:0] hart_id_i,
    input  logic             [VADDR_WIDTH-1:0] boot_address_i,
    // Interrupt
    input  logic                               interrupt_sw_i,
    input  logic                               interrupt_timer_i,
    input  logic                               interrupt_ext_i,
    // AR
    output logic             [          2-1:0] l1_l2_req_arvalid_o,
    input  logic             [          2-1:0] l1_l2_req_arready_i,
    output cache_mem_if_ar_t [          2-1:0] l1_l2_req_ar_o,
    // ewrq -> mem bus
    // AW 
    output logic             [          2-1:0] l1_l2_req_awvalid_o,
    input  logic             [          2-1:0] l1_l2_req_awready_i,
    output cache_mem_if_aw_t [          2-1:0] l1_l2_req_aw_o,
    // W 
    output logic             [          2-1:0] l1_l2_req_wvalid_o,
    input  logic             [          2-1:0] l1_l2_req_wready_i,
    output cache_mem_if_w_t  [          2-1:0] l1_l2_req_w_o,
    // L1D -> L2 : Response
    // B
    input  logic             [          2-1:0] l2_l1_resp_bvalid_i,
    output logic             [          2-1:0] l2_l1_resp_bready_o,
    input  cache_mem_if_b_t  [          2-1:0] l2_l1_resp_b_i,
    // mem bus -> mlfb
    // R
    input  logic             [          2-1:0] l2_l1_resp_rvalid_i,
    output logic             [          2-1:0] l2_l1_resp_rready_o,
    input  cache_mem_if_r_t  [          2-1:0] l2_l1_resp_r_i,

    input clk,
    input rst
);

  // CSR Status
  logic [     PRIV_LVL_WIDTH-1:0]                          priv_lvl;
  logic                                                    tvm;
  logic                                                    tw;
  logic                                                    tsr;
  logic [               XLEN-1:0]                          mstatus;
  logic [               XLEN-1:0]                          satp;

  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_vld;
  logic [       DECODE_WIDTH-1:0][                   31:0] ifu_dec_inst;
  logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] ifu_dec_inst_pc;
  logic [       DECODE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] ifu_dec_inst_btq_tag;
  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_pred_taken;
  logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] ifu_dec_inst_pred_target;
  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_is_rvc;
  logic [    EXCP_TVAL_WIDTH-1:0]                          ifu_dec_inst_excp_tval;
  logic                                                    ifu_dec_inst_excp_vld;
  logic [   EXCP_CAUSE_WIDTH-1:0]                          ifu_dec_inst_excp_cause;
  logic [       DECODE_WIDTH-1:0]                          ifu_dec_inst_rdy;

  // Bru Update
  logic [          BRU_COUNT-1:0]                          bru_resolve_vld;
  logic [          BRU_COUNT-1:0][        VADDR_WIDTH-1:0] bru_resolve_pc;
  logic [          BRU_COUNT-1:0][PC_LOB_WIDTH-1:0]        bru_resolve_pc_lob;
  logic [          BRU_COUNT-1:0]                          bru_resolve_taken;
  logic [          BRU_COUNT-1:0]                          bru_resolve_mispred;
  logic [          BRU_COUNT-1:0][      BTQ_TAG_WIDTH-1:0] bru_resolve_btq_tag;
  logic [          BRU_COUNT-1:0][        VADDR_WIDTH-1:0] bru_resolve_target;
  // Redirect
  logic                                                    redirect_vld;
  logic [        VADDR_WIDTH-1:0]                          redirect_target;
  logic [        VADDR_WIDTH-1:0]                          redirect_pc;
  logic [PC_LOB_WIDTH-1:0]                                 redirect_pc_lob;
  logic [      BTQ_TAG_WIDTH-1:0]                          redirect_btq_tag;
  logic                                                    redirect_is_taken;
  // Redirect Port
  logic                                                    csr_redirect_vld;
  logic [        VADDR_WIDTH-1:0]                          csr_redirect_pc;
  logic [PC_LOB_WIDTH-1:0]                                 csr_redirect_pc_lob;
  logic [        VADDR_WIDTH-1:0]                          csr_redirect_target;
  logic [      BTQ_TAG_WIDTH-1:0]                          csr_redirect_btq_tag;
  // Commit
  logic [       RETIRE_WIDTH-1:0]                          retire_insn_vld;
  logic [       RETIRE_WIDTH-1:0][        VADDR_WIDTH-1:0] retire_insn_pc;
  logic [       RETIRE_WIDTH-1:0][PC_LOB_WIDTH-1:0]        retire_insn_pc_lob;
  logic [       RETIRE_WIDTH-1:0]                          retire_insn_is_br;
  logic [       RETIRE_WIDTH-1:0]                          retire_insn_mispred;
  logic [       RETIRE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag;

  logic                                                    interrupt_ack;
  logic [   EXCP_CAUSE_WIDTH-1:0]                          interrupt_cause;
  // Execute FENCE.I : Flush Icache
  logic                                                    flush_icache_req;
  logic                                                    flush_icache_gnt;
  // Execute SFENCE.VMA : Flush ITLB
  logic                                                    flush_itlb_req;
  logic                                                    flush_itlb_use_vpn;
  logic                                                    flush_itlb_use_asid;
  logic [          VPN_WIDTH-1:0]                          flush_itlb_vpn;
  logic [          VPN_WIDTH-1:0]                          flush_itlb_asid;
  logic                                                    flush_itlb_gnt;
  // Execute SFENCE.VMA : Flush STLB
  logic                                                    flush_stlb_req;
  logic                                                    flush_stlb_use_vpn;
  logic                                                    flush_stlb_use_asid;
  logic [          VPN_WIDTH-1:0]                          flush_stlb_vpn;
  logic [          VPN_WIDTH-1:0]                          flush_stlb_asid;
  logic                                                    flush_stlb_gnt;


  assign l1_l2_req_arvalid_o[0] = 0;
  assign l1_l2_req_ar_o[0] = 0;
  assign l1_l2_req_awvalid_o[0] = 0;
  assign l1_l2_req_aw_o[0] = 0;
  assign l1_l2_req_wvalid_o[0] = 0;
  assign l1_l2_req_w_o[0] = 0;
  assign l2_l1_resp_bready_o[0] = 0;
  assign l2_l1_resp_rready_o[0] = 1;


  generate
    assign redirect_pc_lob = redirect_pc[PC_LOB_WIDTH-1:0];
    assign csr_redirect_pc_lob = csr_redirect_pc[PC_LOB_WIDTH-1:0];
    for (genvar i = 0; i < BRU_COUNT; i++) begin
      assign bru_resolve_pc_lob[i] = bru_resolve_pc[i][PC_LOB_WIDTH-1:0];
    end
    for (genvar i = 0; i < RETIRE_WIDTH; i++) begin
      assign retire_insn_pc_lob[i] = retire_insn_pc[i][PC_LOB_WIDTH-1:0];
    end
  endgenerate

    rvh_ifu u_rvh_ifu(
      .priv_lvl_i(2'h3),
      .mstatus_mxr('b0),
      .mstatus_sum('b0),
      .satp_mode_i('b0),
      .satp_asid_i('b0),
    // fe > be
      .fe_dec_instr_valid_o(ifu_dec_inst_vld),
      .fe_dec_btq_tag_o(ifu_dec_inst_btq_tag),
      .fe_dec_pc_o(ifu_dec_inst_pc),
      .fe_dec_pred_target_o(ifu_dec_inst_pred_target),
      .fe_dec_instr_o(ifu_dec_inst),
      .fe_dec_is_rvc_o(ifu_dec_inst_is_rvc),
      .fe_dec_pred_taken_o(ifu_dec_inst_pred_taken),
      .fe_dec_excp_tval_o(ifu_dec_inst_excp_tval),
      .fe_dec_excp_vld_o(ifu_dec_inst_excp_vld),
      .fe_dec_excp_cause_o(ifu_dec_inst_excp_cause),
      .fe_dec_ready_i(ifu_dec_inst_rdy[0]),
    // bru redirect
      .bru_redirect_valid_i  (redirect_vld),
      .bru_redirect_pc_lob_i (redirect_pc_lob),
      .bru_redirect_target_i (redirect_target),
      .bru_redirect_btq_tag_i(redirect_btq_tag),
      .bru_redirect_taken_i(redirect_is_taken),
    // cmt > btq                   
      .cmt_valid_i  (retire_insn_vld),
      .cmt_pc_lob_i (retire_insn_pc_lob),
      .cmt_btq_tag_i(retire_insn_btq_tag),
    // cmt redirect
      .cmt_redirect_valid_i (csr_redirect_vld),
      .cmt_redirect_pc_lob_i(csr_redirect_pc_lob),
      .cmt_redirect_target_i(csr_redirect_target),
      .cmt_redirect_btq_tag_i(csr_redirect_btq_tag),

      .boot_address_i(39'h80000000),

    // L1 TLB -> STLB : Look up Request
      .stlb_req_vld_o(),
      .stlb_req_trans_id_o(),
      .stlb_req_asid_o(),
      .stlb_req_access_type_o(),
      .stlb_req_vpn_o(),
      .stlb_req_rdy_i('b1),

      .stlb_resp_vld_i('b0),
      .stlb_resp_trans_id_i('b0),
      .stlb_resp_asid_i('b0),
      .stlb_resp_pte_lvl_i('b0),
      .stlb_resp_pte_i('b0),
      .stlb_resp_vpn_i('b0),
      .stlb_resp_access_type_i('b0),
      .stlb_resp_access_fault_i('b0),
      .stlb_resp_page_fault_i('b0),

    // ITLB Entry Evict
      .itlb_evict_vld_o(),
      .itlb_evict_pte_o(),
      .itlb_evict_page_lvl_o(),
      .itlb_evict_vpn_o(),
      .itlb_evict_asid_o(),

    // Execute SFENCE.VMA : Flush ITLB
      .flush_itlb_req_i               ('b0),
      .flush_itlb_use_vpn_i           ('b0),
      .flush_itlb_use_asid_i          ('b0),
      .flush_itlb_vpn_i               ('b0),
      .flush_itlb_asid_i              ('b0),
      .flush_itlb_gnt_o               (),

    // L1I -> L2 : Request
      .l1i_l2_req_arvalid_o(l1_l2_req_arvalid_o[1]),
      .l1i_l2_req_arready_i(l1_l2_req_arready_i[1]),
      .l1i_l2_req_ar_o     (l1_l2_req_ar_o[1]),
      .l1i_l2_req_awvalid_o(l1_l2_req_awvalid_o[1]),
      .l1i_l2_req_awready_i(l1_l2_req_awready_i[1]),
      .l1i_l2_req_aw_o     (l1_l2_req_aw_o[1]),
      .l1i_l2_req_wvalid_o (l1_l2_req_wvalid_o[1]),
      .l1i_l2_req_wready_i (l1_l2_req_wready_i[1]),
      .l1i_l2_req_w_o      (l1_l2_req_w_o[1]),
      .l2_l1i_resp_bvalid_i(l2_l1_resp_bvalid_i[1]),
      .l2_l1i_resp_bready_o(l2_l1_resp_bready_o[1]),
      .l2_l1i_resp_b_i     (l2_l1_resp_b_i[1]),
      .l2_l1i_resp_rvalid_i(l2_l1_resp_rvalid_i[1]),
      .l2_l1i_resp_rready_o(l2_l1_resp_rready_o[1]),
      .l2_l1i_resp_r_i     (l2_l1_resp_r_i[1]),

      .flush_icache_req_i  ('b0),
      .flush_icache_gnt_o  (),

      .clk(clk),
      .rst(rst)
);

endmodule : frontend_wrapper
