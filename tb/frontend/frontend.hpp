#ifndef __FRONTEND_H__
#define __FRONTEND_H__
#include "rrvtb.hpp"
#include "dromajo.hpp"
#include "pseudo_icache.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <map>
#include <algorithm>

struct insn_t
{
    uint32_t    data;
    bool        is_br;
    bool        is_rvc;
    uint64_t    pc;
    uint64_t    btq_idx;
    bool        pred_taken;
    uint64_t    pred_target;
    bool        is_cacheline_tail;  
    bool        is_mispred;
    bool        valid;
};

class Frontend;
// static void advance_cb(void *);
// static void reset_cb(void *);
static void clock_gen_cb(void *);
static void clock_cb(void *, const vector<Bits> &);
map<uint64_t, uint64_t> misp_br_map, misp_call_map, misp_ret_map;

static constexpr uint64_t catch_addr = 0x8000228e;

class Frontend
{
public:
    Frontend(string elfpath, uint64_t boot_address)
    // init SVObject here
    : rst("testbench.rst")
    , last_btq_tag(0)
    , last_saw_not_taken_br(false)
    , last_saw_taken_br(false)
    , golden_ghist(0)
    , mem("testbench.u_axi_mem.ram.mem")
    , ifu_dec_inst_vld("testbench.frontend_wrapper_u.ifu_dec_inst_vld")
    , ifu_dec_inst("testbench.frontend_wrapper_u.ifu_dec_inst")
    , ifu_dec_inst_pc("testbench.frontend_wrapper_u.ifu_dec_inst_pc")
    , ifu_dec_inst_btq_tag("testbench.frontend_wrapper_u.ifu_dec_inst_btq_tag")
    , ifu_dec_inst_pred_taken("testbench.frontend_wrapper_u.ifu_dec_inst_pred_taken")
    , ifu_dec_inst_pred_target("testbench.frontend_wrapper_u.ifu_dec_inst_pred_target")
    , ifu_dec_inst_excp_tval("testbench.frontend_wrapper_u.ifu_dec_inst_excp_tval")
    , ifu_dec_inst_excp_vld("testbench.frontend_wrapper_u.ifu_dec_inst_excp_vld")
    , ifu_dec_inst_excp_cause("testbench.frontend_wrapper_u.ifu_dec_inst_excp_cause")
    , ifu_dec_inst_is_rvc("testbench.frontend_wrapper_u.ifu_dec_inst_is_rvc")
    , ifu_dec_inst_rdy("testbench.frontend_wrapper_u.ifu_dec_inst_rdy")
    , redirect_vld("testbench.frontend_wrapper_u.redirect_vld")
    , redirect_target("testbench.frontend_wrapper_u.redirect_target")
    , redirect_pc("testbench.frontend_wrapper_u.redirect_pc")
    , redirect_pc_lob("testbench.frontend_wrapper_u.redirect_pc_lob")
    , redirect_btq_tag("testbench.frontend_wrapper_u.redirect_btq_tag")
    , redirect_is_taken("testbench.frontend_wrapper_u.redirect_is_taken")
    , csr_redirect_vld("testbench.frontend_wrapper_u.csr_redirect_vld")
    , csr_redirect_pc("testbench.frontend_wrapper_u.csr_redirect_pc")
    , csr_redirect_pc_lob("testbench.frontend_wrapper_u.csr_redirect_pc_lob")
    , csr_redirect_target("testbench.frontend_wrapper_u.csr_redirect_target")
    , retire_insn_vld("testbench.frontend_wrapper_u.retire_insn_vld")
    , retire_insn_pc("testbench.frontend_wrapper_u.retire_insn_pc")
    , retire_insn_pc_lob("testbench.frontend_wrapper_u.retire_insn_pc_lob")
    , retire_insn_is_br("testbench.frontend_wrapper_u.retire_insn_is_br")
    , retire_insn_mispred("testbench.frontend_wrapper_u.retire_insn_mispred")
    , retire_insn_btq_tag("testbench.frontend_wrapper_u.retire_insn_btq_tag")
    , update_ghist_info("testbench.frontend_wrapper_u.u_rvh_ifu.update_ghist_info")
    , ghist_queue("testbench.frontend_wrapper_u.u_rvh_ifu.ghist_u.ghist_queue")
    , update_ghist("testbench.frontend_wrapper_u.u_rvh_ifu.ghist_u.update_ghist_o")
    , update_valid("testbench.frontend_wrapper_u.u_rvh_ifu.u_btq.bpu_update_valid_o")
    , update_btq_tag("testbench.frontend_wrapper_u.u_rvh_ifu.u_btq.head")
    , update_pc("testbench.frontend_wrapper_u.u_rvh_ifu.u_btq.bpu_update_target_o")
    , ghist_info_ram("testbench.frontend_wrapper_u.u_rvh_ifu.u_btq.entry_ghist_info")
    {
        // Assign initial value to signal before simulation starts
        uint64_t pos = 0;
        ostringstream ostr;
        ifstream file;
        string line;

        rst = 1;
        ostr << "elf2hex 16 4096 " << elfpath << " 2147483648 > elf.hex";
        system(ostr.str().c_str());

        file.open("elf.hex", ios::in);
        if (!file.is_open())
            ERROR("Cannot open file: elf.hex\n");
        
        while (getline(file, line))
        {
            if (line.empty()) ERROR("Invalid hex file format!\n");
            if (line.size() != 32) ERROR("The hex file is not in 128-bit format!\n");
            for(int i = 0 ; i < 2; i++) {
                mem(pos*2+i) = "64'h" + line.substr((1-i)*16,16);
            }
            
            pos += 1;
        }
        file.close();
        system("rm -rf elf.hex");

        ostr.str("");
        ostr << "nm " << elfpath << " > elf.tab";
        INFO("cmd: %s\n", ostr.str().c_str());
        system(ostr.str().c_str());
        file.open("elf.tab", ios::in);
        if (!file.is_open())
            ERROR("Cannot open file: elf.tab\n");
        while (getline(file, line))
        {
            if (line.find("tohost") != std::string::npos) 
            {
                tohost_addr = strtoull(line.c_str(), NULL, 16);
                INFO("tohost found! addr: %x\n", tohost_addr);
                break;
            }
        }
        file.close();
        system("rm -rf elf.tab");

        sem_init(&begin_mtx, 0, 0);
        sem_init(&finish_mtx, 0, 0);
        // register_timer_cb(clock_gen_cb, this, 1, true);
        register_signal_cb(clock_cb, this, {"testbench.clk"});
    }

    uint64_t get_ghist(uint64_t ghist_ptr)
    {
        uint64_t ghist_raw;
        uint64_t ghist = 0;
        uint64_t size = ghist_queue.size();
        if (ghist_ptr == 0)
        {
            ghist_raw = ghist_queue(size - 1, size - 64).value().to_ulong();
        }
        else if (ghist_ptr < 64)
        {
            uint64_t head = size - (64 - ghist_ptr);
            uint64_t ghist_upper;
            ghist_raw = (ghist_queue >> head).to_ulong();
            ghist_upper = (ghist_queue & ((1ULL << ghist_ptr) - 1)).to_ulong();
            ghist_upper <<= size - head;
            ghist_raw |= ghist_upper;
        }
        else 
        {
            ghist_raw = ghist_queue(ghist_ptr - 1, ghist_ptr - 64).value().to_ulong();
        }

        for (int i = 0; i < 64; i++)
        {
            ghist <<= 1;
            if ((ghist_raw >> i) & 0x1)
            {
                ghist |= 1;
            } 
        }

        return ghist;
    }

    void check_ghist_when_update()
    {
        // check ghist when update
        uint64_t ghist_ptr;
        ghist_ptr = update_ghist_info["tail_ptr"].value().to_ulong();
        uint64_t calc_ghist = get_ghist(ghist_ptr);
        uint64_t rtl_ghist = update_ghist.value().to_ulong();
        uint64_t t_update_btq_tag = update_btq_tag.value().to_ulong();
        uint64_t pc = update_pc.value().to_ulong();
        if (update_valid == 1) 
        {
             if (calc_ghist != rtl_ghist) 
            {
                ERROR("(%d) ghist calc inconsistancy! pc: %08x, btq_tag: %d, calc_ghist: %016llx, rtl: %016llx, ptr: 0x%x\n", 
                        get_time(), pc, t_update_btq_tag, calc_ghist, rtl_ghist, ghist_ptr);
            }
            if (rtl_ghist != ghist_map[t_update_btq_tag]) 
            {
                ERROR("(%d) ghist inconsistancy! pc: %08x, btq_tag: %d, golden: %016llx, rtl: %016llx, ptr: 0x%x\n", 
                        get_time(), pc, t_update_btq_tag, ghist_map[t_update_btq_tag], rtl_ghist, ghist_ptr);
            }
        }
    }

    void gen_golden_ghist_when_commit(uint64_t cmt_btq_tag)
    {
        // calc ghist when commit
        if (cmt_btq_tag != last_btq_tag)
        {
            if (last_saw_taken_br)
            {
                golden_ghist = (golden_ghist << 1) | 1;
            }
            else if (last_saw_not_taken_br) 
            {
                golden_ghist = (golden_ghist << 1);
            }
            last_btq_tag = cmt_btq_tag;
            last_saw_taken_br = false;
            last_saw_not_taken_br = false;
            ghist_map[cmt_btq_tag] = golden_ghist;
        }
    }

    void Evaluate(vector<insn_t> &insn_pkg)
    {
        redirect_vld = 0;
        redirect_target = 0;
        redirect_pc = 0;
        redirect_pc_lob = 0;
        redirect_btq_tag = 0;
        redirect_is_taken = 0;
        csr_redirect_vld = 0;
        csr_redirect_pc = 0;
        csr_redirect_pc_lob = 0;
        csr_redirect_target = 0;
        retire_insn_vld = 0;
        retire_insn_pc = 0;
        retire_insn_pc_lob = 0;
        retire_insn_is_br = 0;
        retire_insn_mispred = 0;
        retire_insn_btq_tag = 0;

        for (int i = 0; i < 4; i++)
        {
            if (ifu_dec_inst_vld(i) == 1)
            {
                insn_t insn;
                insn.pc = ifu_dec_inst_pc(i).value().to_ulong();
                insn.data = ifu_dec_inst(i).value().to_ulong();
                insn.is_rvc = ifu_dec_inst_is_rvc(i).value().to_ulong();
                insn.btq_idx = ifu_dec_inst_btq_tag(i).value().to_ulong();
                insn.pred_taken = ifu_dec_inst_pred_taken(i).value().to_ulong();
                insn.is_cacheline_tail = 0;//deq_is_cacheline_tail_c(i).value().to_ulong();
                insn.pred_target = ifu_dec_inst_pred_target(i).value().to_ulong();// rtl_target_ram(insn.btq_idx & 0x7).value().to_ulong();
                insn.is_mispred = false;
                insn.valid = false;
                insn_pkg.emplace_back(insn);
            }
        }
    }

    bool BruExec(insn_t &insn, bool is_taken, uint64_t target, int cfi_type)
    {
        bool mispred = false;
        if (is_taken && insn.pred_taken)
        {
            redirect_is_taken = 1;
            if (insn.pred_target != target) 
            {
                mispred = true;
            }
        }
        else if (is_taken)
        {
            redirect_is_taken = 1;
            // INFO("(%d) mispred: pc[%08x] [NT] should be [ T]\n", get_time(), insn.pc);
            mispred = true;
        }
        else if (insn.pred_taken)
        {
            redirect_is_taken = 0;
            // INFO("(%d) mispred: pc[%08x] [T] should be [NT]\n", get_time(), insn.pc);
            mispred = true;
        }
        switch (cfi_type)
        {
            case Dromajo::TYPE_CALL:
                if (mispred) 
                {
                    if (misp_call_map.find(insn.pc) != misp_call_map.end())
                    {
                        misp_call_map[insn.pc]++;
                    }
                    else 
                    {
                        misp_call_map[insn.pc] = 1;
                    }
                }
                break;
            case Dromajo::TYPE_RET:
                if (mispred) 
                {
                    if (misp_ret_map.find(insn.pc) != misp_ret_map.end())
                    {
                        misp_ret_map[insn.pc]++;
                    }
                    else 
                    {
                        misp_ret_map[insn.pc] = 1;
                    }
                }
                break;
            case Dromajo::TYPE_BRANCH:
                if (mispred) 
                {
                    if (misp_br_map.find(insn.pc) != misp_br_map.end())
                    {
                        misp_br_map[insn.pc]++;
                    }
                    else 
                    {
                        misp_br_map[insn.pc] = 1;
                    }
                }
                break;
            case Dromajo::TYPE_JAL:
                break;
            case Dromajo::TYPE_JALR:
                break;
            default:
                other_cnt++;
                break;
        }
        
        // if (catch_addr == insn.pc)
        {
            uint64_t ghist_ptr = ghist_info_ram(insn.btq_idx)["tail_ptr"].value().to_ulong();
            uint64_t ghist = get_ghist(ghist_ptr);

            if (mispred) 
            {
                INFO("(%d) [%02x: %016lx]misp: pc[%08x] [%s] misp[%s]\n", get_time(), ghist_ptr, ghist, insn.pc, is_taken ? " T" : "NT", is_taken ? "NT" : " T");
            }
            // else 
            // {
            //     INFO("(%d) [%02x: %016lx]corr: pc[%08x] [%s]\n", get_time(), ghist_ptr, ghist, insn.pc, is_taken ? " T" : "NT");
            // }
        }

        return mispred;
    }

    void Update(vector<insn_t> &insn_pkg)
    {
        for (int i = 0; i < insn_pkg.size(); i++)
        {
            if (insn_pkg.at(i).valid)
            {
                retire_insn_vld(i) = 1;
                retire_insn_pc(i) = insn_pkg.at(i).pc;
                retire_insn_pc_lob(i) = insn_pkg.at(i).pc & 0x1f;
                retire_insn_is_br(i) = insn_pkg.at(i).is_br;
                retire_insn_mispred(i) = insn_pkg.at(i).is_mispred;
                retire_insn_btq_tag(i) = insn_pkg.at(i).btq_idx;
            }
        }
        
    }

    void Redirect(uint64_t pc, uint64_t target, uint64_t btq_idx)
    {
        redirect_vld = 1;
        redirect_target = target;
        redirect_pc = pc;
        redirect_pc_lob = pc & 0x1f;
        redirect_btq_tag = btq_idx;
    }

    void Advance()
    {
        int begin, finish;
        sem_getvalue(&begin_mtx, &begin);
        assert(begin == 0);
        sem_getvalue(&finish_mtx, &finish);
        assert(finish == 0);
        sem_post(&begin_mtx);
        sem_wait(&finish_mtx);
    }

    void Reset()
    {
        sem_wait(&finish_mtx);
        rst = 0;
        ifu_dec_inst_rdy = 1;
        minstret = 0;
        cfi_cnt = 0;
        misp_call_cnt = 0;
        misp_ret_cnt = 0;
        misp_br_cnt = 0;
        misp_jal_cnt = 0;
        misp_jalr_cnt = 0;
        call_cnt = 0;
        ret_cnt = 0;
        br_cnt = 0;
        jal_cnt = 0;
        jalr_cnt = 0;
        other_cnt = 0;

    }

    friend void clock_cb(void *p, const vector<Bits> &sigs)
    {
        static bool has_last = false;
        static int last_sigs = 0;
        Frontend *pFe = (Frontend*)p;
        int newsigs = sigs[0] == 1 ? 1 : 0;
        if (sigs[0] == 1)
        {
            sem_post(&pFe->finish_mtx);
            sem_wait(&pFe->begin_mtx);
        }
        assert(!has_last || (has_last && last_sigs != newsigs));
        has_last = true;
        last_sigs = sigs[0] == 1 ? 1 : 0;
    }

    ~Frontend();

public:
    uint64_t last_btq_tag;
    bool last_saw_not_taken_br;
    bool last_saw_taken_br;

private:


    sem_t           finish_mtx;
    sem_t           begin_mtx;

    SVObject rst;
    SVObject ifu_dec_inst_vld;
    SVObject ifu_dec_inst;
    SVObject ifu_dec_inst_pc;
    SVObject ifu_dec_inst_btq_tag;
    SVObject ifu_dec_inst_pred_taken;
    SVObject ifu_dec_inst_pred_target;
    SVObject ifu_dec_inst_is_rvc;
    SVObject ifu_dec_inst_excp_tval;
    SVObject ifu_dec_inst_excp_vld;
    SVObject ifu_dec_inst_excp_cause;
    SVObject ifu_dec_inst_rdy;
    SVObject redirect_vld;
    SVObject redirect_target;
    SVObject redirect_pc;
    SVObject redirect_pc_lob;
    SVObject redirect_btq_tag;
    SVObject redirect_is_taken;
    SVObject csr_redirect_vld;
    SVObject csr_redirect_pc;
    SVObject csr_redirect_pc_lob;
    SVObject csr_redirect_target;
    SVObject retire_insn_vld;
    SVObject retire_insn_pc;
    SVObject retire_insn_pc_lob;
    SVObject retire_insn_is_br;
    SVObject retire_insn_mispred;
    SVObject retire_insn_btq_tag;
    SVObject update_ghist_info;
    SVObject ghist_queue;
    SVObject mem;
    SVObject boot_addr;
    SVObject update_ghist;
    SVObject update_valid;
    SVObject update_btq_tag;
    SVObject ghist_info_ram;
    SVObject update_pc;

    uint64_t tohost_addr;
    uint64_t fromhost_addr;

    uint64_t minstret;
    uint64_t cfi_cnt;
    uint64_t mispred_cnt;

    uint64_t misp_call_cnt;
    uint64_t misp_ret_cnt;
    uint64_t misp_br_cnt;
    uint64_t misp_jal_cnt;
    uint64_t misp_jalr_cnt;

    uint64_t call_cnt;
    uint64_t ret_cnt;
    uint64_t br_cnt;
    uint64_t jal_cnt;
    uint64_t jalr_cnt;
    uint64_t other_cnt;

    uint64_t golden_ghist;

    map<uint64_t, uint64_t> ghist_map;

    void statistic(const Dromajo &m, uint64_t &jal, uint64_t &jalr, uint64_t &branch, uint64_t &other)
    {
        if (m.get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH) branch++;
        else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_CALL) jalr++;
        else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_JALR) jalr++;
        else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_RET) jalr++;
        else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_JAL) jal++;
        else other++;
    }
};











#endif