#include "dromajo.hpp"
#include "rrvtb.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <pthread.h>
#include "frontend.hpp"
#include <queue>
using namespace std;
Dromajo *core, *golden;
Frontend *frontend;
ofstream rpt_f;
pthread_t model_thread, idle_thread;
uint64_t golden_ghist = 0;

// for simulation controlling and display
uint64_t insn_cnt = 0;
uint64_t jal_cnt = 0, jalr_cnt = 0, branch_cnt = 0, other_cnt = 0;
uint64_t mispred_jal_cnt = 0, mispred_jalr_cnt = 0, mispred_branch_cnt = 0, mispred_other_cnt = 0;
void statistic(const Dromajo &m, uint64_t &jal, uint64_t &jalr, uint64_t &branch, uint64_t &other)
{
    if (m.get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH) branch++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_CALL) jalr++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_JALR) jalr++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_RET) jalr++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_JAL) jal++;
    else other++;
}

void print_br_misp_info(map<uint64_t, uint64_t> &im)
{
    map<uint64_t,uint64_t>::iterator it;
    vector<uint64_t> v;
    for(it=im.begin(); it!=im.end(); ++it)
    {
        v.push_back(it->second);
    }
    sort(v.begin(), v.end(), greater<uint64_t>());
    int n;
    for(int i = 0; (i < v.size()) && (i < 10); i++)
    {
        map<uint64_t,uint64_t>::iterator it;
        for(it = im.begin(); it != im.end(); it++)
        {
            if(v[i]==it->second)
            {
                fprintf(stderr, "%08x: %d\n", it->first, it->second);
            }
        }
    }
}

uint64_t br_cnt = 0, tage_correct_cnt = 0;
void *model_fn(void *arg){
    bool keep_going = true;
    uint8_t cmt_no;
    std::queue<std::vector<insn_t>> cmt_insn_queue;
    frontend->Reset();
    while (true)
    {
        std::vector<insn_t> insn_pkg;
        frontend->Evaluate(insn_pkg);
        frontend->check_ghist_when_update();
        
        // write back
        for (auto &insn : insn_pkg)
        {
            // core->set_pc(insn.pc);
            core->iterate();
            bool mispred = frontend->BruExec(insn, core->last_insn_is_taken(), core->get_pc(), core->get_last_insn_cfi_type());
            insn.valid = true;
            insn.is_br = false;
            if (core->get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH) 
            {
                br_cnt++;
                insn.is_br = true;
            }
            if (mispred)
            {
                insn.is_mispred = true;
                if (core->get_last_insn_cfi_type() == Dromajo::TYPE_RET) 
                {
                    INFO("[%d]RET MISPRED PC: %x PRED: %x, CORR:%x\n", get_time(), insn.pc, insn.pred_target, core->get_pc());
                }
                if (core->get_last_insn_cfi_type() == Dromajo::TYPE_CALL)
                {
                    INFO("[%d]CALL MISPRED PC: %x PRED: %x, CORR:%x\n", get_time(), insn.pc, insn.pred_target, core->get_pc());
                }
                if (core->get_last_insn_cfi_type() == Dromajo::TYPE_JALR)
                {
                    INFO("[%d]JALR MISPRED PC: %x PRED: %x, CORR:%x\n", get_time(), insn.pc, insn.pred_target, core->get_pc());
                }

                frontend->Redirect(insn.pc, core->get_pc(), insn.btq_idx);
                break;
            }
            else 
            {
                // if (core->get_last_insn_cfi_type() == Dromajo::TYPE_RET) 
                // {
                //     INFO("[%d]RET CORRECT PC: %x PRED: %x\n", get_time(), insn.pc, insn.pred_target);
                // }
                // if (core->get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH)
                // {
                //     INFO("[%d]JALR CORRECT PC: %x PRED: %s\n", get_time(), insn.pc, insn.pred_taken ? "T" : "N");
                // }
            }
        }

        // commit
        while (cmt_insn_queue.size() >= 1)
        {
            cmt_no = 0;
            for (auto &insn : cmt_insn_queue.front())
            {
                if (insn.valid)
                {
                    // if across cacheline, we generate new ghist
                    frontend->gen_golden_ghist_when_commit(insn.btq_idx);

                    if (insn.pc != golden->get_pc())
                    {
                        ERROR("inconsistancy@%08x: golden: %08x\n", insn.pc, golden->get_pc());
                    }
                    keep_going = golden->iterate();

                    // collect br info
                    if (golden->get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH)
                    {
                        if (golden->last_insn_is_taken())
                        {
                            frontend->last_saw_taken_br = true;
                        }
                        else 
                        {
                            frontend->last_saw_not_taken_br = true;
                        }
                    }

                    if (!keep_going)
                    {
                        int exitcode = core->get_exit_code();
                        INFO("EXIT! code = %d\n", exitcode);
                        if (rpt_f.is_open())
                        {
                            rpt_f << "exit: " << exitcode << endl;
                        }
                        INFO("==================statistic==================\n");
                        INFO("total: %d, branch: %d, jalr: %d, jal: %d (other: %d)\n",
                            branch_cnt + jalr_cnt + jal_cnt, branch_cnt, jalr_cnt, jal_cnt, other_cnt);
                        INFO("mis_total: %d, mis_branch: %d, mis_jalr: %d, mis_jal: %d, (mis_other: %d)\n",
                            mispred_branch_cnt + mispred_jalr_cnt + mispred_jal_cnt, 
                            mispred_branch_cnt, mispred_jalr_cnt, mispred_jal_cnt, mispred_other_cnt);
                        INFO("accuracy: %f\n", 
                            (double)(mispred_branch_cnt + mispred_jalr_cnt + mispred_jal_cnt)/(double)(branch_cnt + jalr_cnt + jal_cnt));
                        
                        INFO("br cnt: %d, tage correct: %d, correct rate: %f\n", br_cnt, tage_correct_cnt, (double)tage_correct_cnt/(double)br_cnt);
                        fprintf(stderr, "Top 10 misp branch: \n");
                        print_br_misp_info(misp_br_map);
                        fprintf(stderr, "Top 10 misp call: \n");
                        print_br_misp_info(misp_call_map);
                        fprintf(stderr, "Top 10 misp ret: \n");
                        print_br_misp_info(misp_ret_map);
                        finish();
                    }

                    if ( ((insn.is_rvc == false) && (golden->get_last_insn() != insn.data)) ||
                            ((insn.is_rvc == true) && (golden->get_last_insn() != (insn.data & 0xffff))) )
                    {
                        printf("[%d] instr inconsistancy! [%s] SIM: %08x, RTL: %08x\n", get_time(), insn.is_rvc ? "rvc" : "insn", golden->get_last_insn(), insn.data);
                        printf("Pc Inconsistancy [%08x]: golden: %08x\n", insn.pc, golden->get_last_pc());
                        if (rpt_f.is_open())
                        {
                            rpt_f << "exit: pc: " << insn.pc << "instr inconsistancy! SIM: "<< golden->get_last_insn() << ", RTL: " << insn.data << endl;
                        }
                        finish();
                    }
                    statistic(*golden, jal_cnt, jalr_cnt, branch_cnt, other_cnt);
                    if (insn.is_mispred)
                    {
                        statistic(*golden, mispred_jal_cnt, mispred_jalr_cnt, mispred_branch_cnt, mispred_other_cnt);
                    }
                    // INFO("Cycle(%d) Insn[%d] Commit insn %x\n", get_time(), ++insn_cnt, insn.pc);
                }
            }
            frontend->Update(cmt_insn_queue.front());
            cmt_insn_queue.pop();
        }

        for (int i = 0; i < insn_pkg.size(); i++)
        {
            if (insn_pkg.at(i).is_mispred)
            {
                insn_pkg.resize(i + 1);
                break;
            }
        }
        cmt_insn_queue.push(insn_pkg);

        frontend->Advance();   
    }
    
	return((void *)0);
}


initial(initial_func)
{
    char *file_name;
    char *rpt_file_name;

    if (!(file_name = (char*)scan_plusargs("image=")))
    {
        ERROR("no image input!\n");
    }
    if (!(rpt_file_name = (char*)scan_plusargs("report=")))
    {
        INFO("no image input!\n");
    }
    else 
    {
        rpt_f.open(rpt_file_name, ios::app);
    }
    core = new Dromajo(file_name, 0x80000000);
    golden = new Dromajo(file_name, 0x80000000);
    frontend = new Frontend(file_name, 0x80000000);

    int err = pthread_create(&model_thread, NULL, &model_fn, NULL);
	if (err != 0)
		ERROR("Can't create model thread\n");
    sleep(0);
}

