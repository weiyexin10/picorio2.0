#ifndef __PSEUDO_ICACHE_H__
#define __PSEUDO_ICACHE_H__
#include "rrvtb.hpp"
#include "dromajo.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <queue>
#include <set>

typedef struct transaction
{
    bool valid;
    uint64_t line_addr;
    uint64_t dst_tick;
    uint64_t tran_id;
    enum {
        L1,
        MEM
    } pos;
    friend  bool operator<(const transaction &a,const transaction &b) { return  a.dst_tick > b.dst_tick; }
} transaction_t;

class PseudoIcache
{
public:
    PseudoIcache(string elfpath, uint64_t mshr_size, uint64_t l1_size)
    // init SVObject here
    : m_tick(0)
    , m_l1_size(l1_size)
    , m_stalled(false)
    , m_mshr_size(mshr_size)
    , rst("testbench.rst")
    , req_vld("testbench.ic_req_vld")
    , req_tag("testbench.ic_req_tag")
    , req_pc("testbench.ic_req_pc")
    , req_rdy("testbench.ic_req_rdy")
    , resp_vld("testbench.ic_resp_vld")
    , resp_tag("testbench.ic_resp_tag")
    , resp_cacheline("testbench.ic_resp_cacheline")
    , tlb_resp_vld("testbench.tlb_resp_vld")
    , tlb_resp_hit("testbench.tlb_resp_hit")
    , ic_flush("testbench.ic_flush")
    {
        // // Assign initial value to signal before simulation starts
        // int pos = 0;
        ostringstream ostr;
        ifstream file;
        string line;

        // rst = 1;
        // deque_en = 0;
        ostr << "elf2hex 16 2048 " << elfpath << " 2147483648 > elf.hex";
        system(ostr.str().c_str());

        file.open("elf.hex", ios::in);
        if (!file.is_open())
            ERROR("Cannot open file: elf.hex\n");
        
        while (getline(file, line))
        {
            if (line.empty()) ERROR("Invalid hex file format!\n");
            if (line.size() != 32) ERROR("The hex file is not in 128-bit format!\n");
            m_mem.emplace_back("128'h" + line);
            cout << "mem[" << m_mem.size() - 1 << "] = " << line << endl;
        } 
        system("rm -rf elf.hex");

        m_t_f1.valid = false;
        m_t_f2.valid = false;
    }

    void do_req()           // f1 stage
    {
        m_t_f2.valid = false;
        if (m_t_f1.valid && tlb_resp_vld == 1 && tlb_resp_hit == 1) 
        {
            m_t_f2 = m_t_f1;
            m_t_f2.pos = transaction_t::L1;

            // if (m_l1.find(m_t_f1.line_addr) != m_l1.end()) 
            // {
            //     m_t_f2 = m_t_f1;
            //     m_t_f2.pos = transaction_t::L1;
            //     // INFO("[%d] icache hit: %x\n", get_time(), m_t_f1.line_addr << 4);
            // }
            // else if (m_mshr.size() < m_mshr_size)
            // {
            //     transaction_t t;
            //     t = m_t_f1;
            //     t.pos = transaction_t::MEM;
            //     t.dst_tick = m_tick + 10;
            //     m_mshr.emplace(t);
            //     // INFO("[%d] mshr alloc: %x\n", get_time(), t.line_addr << 4);
            // }  
            // else 
            // {
            //     assert(0);
            // }
        }
        else if (!m_t_f1.valid)
        {
            if (!m_mshr.empty() && m_mshr.top().dst_tick <= m_tick)
            {
                m_t_f2 = m_mshr.top();
                if (m_l1.size() >= m_l1_size) 
                {
                    int i = 0;
                    int evict_idx = rand() % m_l1_size;
                    for (auto it = m_l1.begin(); it != m_l1.end(); it++)
                    {
                        if (i == evict_idx)
                        {
                            m_l1.erase(it);
                            break;
                        }
                    }
                }
                m_l1.insert(m_t_f2.line_addr);
                m_mshr.pop();
                // INFO("[%d] cache miss return: %08x\n", get_time(), m_t_f2.line_addr << 4);
            }
        }
        else 
        {
            m_t_f2.valid = false;
        }
    }
    
    void send_req()  // f0 stage
    {
        if (!m_stalled)
        {
            m_t_f1.valid = (req_vld == 1);
            m_t_f1.line_addr = req_pc.value().to_ulong() >> 4; // 128 bit fetch packet
            m_t_f1.tran_id = req_tag.value().to_ulong();
        }
        else 
        {
            m_t_f1.valid = false;
        }
    }

    void Evaluate()
    {
        resp_vld = 0;
        resp_tag = 0;
        resp_cacheline = 0;
        if (ic_flush == 1)
        {
            m_t_f1.valid = false;
            m_t_f2.valid = false;
            while(!m_mshr.empty()) m_mshr.pop();
            // INFO("[%d] cache flush\n", get_time());
        }

        if (m_t_f2.valid)
        {
            resp_vld = 1;
            resp_tag = m_t_f2.tran_id;
            if (m_t_f2.line_addr == 0)
                resp_cacheline = (const string)"128'h0";
            else 
                resp_cacheline = m_mem.at(m_t_f2.line_addr - (0x80000000ULL >> 4));
        }

        if ((m_mshr.size() >= m_mshr_size) || (!m_mshr.empty() && m_mshr.top().dst_tick <= m_tick))
        {
            m_stalled = 1;
            req_rdy = 0;
        }
        else
        {
            m_stalled = 0;
            req_rdy = 1;
        }
        m_tick++;
    }

    ~PseudoIcache(){}
private:
    uint64_t m_tick;

    SVObject rst;

    SVObject req_vld;
    SVObject req_tag;
    SVObject req_pc;
    SVObject req_rdy;
    SVObject resp_vld;
    SVObject resp_tag;
    SVObject resp_cacheline;

    SVObject tlb_resp_vld;
    SVObject tlb_resp_hit;

    SVObject ic_flush;

    set<uint64_t> m_l1;
    transaction_t m_t_f1, m_t_f2;
    vector<string> m_mem;
    priority_queue<transaction_t> m_mshr; 
    const uint64_t m_mshr_size;
    bool m_stalled;
    const uint64_t m_l1_size;

};











#endif