module testbench
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_l1d_pkg::*;
();

    logic clk;
    logic rst;
    logic deque_en;

    initial begin
        $vcdpluson();
        // $fsdbDumpvars(0, testbench);
    end

    initial begin
        clk = 0;
        forever begin
            #1;
            clk = 1;
            #1;
            clk = 0;
        end
    end


  logic               [2-1:0]       l1_l2_arvalid;
  logic               [2-1:0]       l1_l2_arready;
  cache_mem_if_ar_t   [2-1:0]       l1_l2_ar;

  logic               [2-1:0]       l1_l2_rvalid;
  logic               [2-1:0]       l1_l2_rready;
  cache_mem_if_r_t    [2-1:0]       l1_l2_r;

  logic               [2-1:0]       l1_l2_awvalid;
  logic               [2-1:0]       l1_l2_awready;
  cache_mem_if_aw_t   [2-1:0]       l1_l2_aw;

  logic               [2-1:0]       l1_l2_wvalid;
  logic               [2-1:0]       l1_l2_wready;
  cache_mem_if_w_t    [2-1:0]       l1_l2_w;

  logic               [2-1:0]       l1_l2_bvalid;
  logic               [2-1:0]       l1_l2_bready;
  cache_mem_if_b_t    [2-1:0]       l1_l2_b;


  logic              l1_l2_arvalid_from_arb;
  logic              l1_l2_arready_from_arb;
  cache_mem_if_ar_t  l1_l2_ar_from_arb;

  logic              l1_l2_rvalid_from_arb;
  logic              l1_l2_rready_from_arb;
  cache_mem_if_r_t   l1_l2_r_from_arb;

  logic              l1_l2_awvalid_from_arb;
  logic              l1_l2_awready_from_arb;
  cache_mem_if_aw_t  l1_l2_aw_from_arb;

  logic              l1_l2_wvalid_from_arb;
  logic              l1_l2_wready_from_arb;
  cache_mem_if_w_t   l1_l2_w_from_arb;

  logic              l1_l2_bvalid_from_arb;
  logic              l1_l2_bready_from_arb;
  cache_mem_if_b_t   l1_l2_b_from_arb;


  frontend_wrapper frontend_wrapper_u (
      .hart_id_i          (64'b0),
      .boot_address_i     (39'h80000000),
      .interrupt_sw_i     ('0),
      .interrupt_timer_i  ('0),
      .interrupt_ext_i    ('0),
      // AR
      .l1_l2_req_arvalid_o(l1_l2_arvalid),
      .l1_l2_req_arready_i(l1_l2_arready),
      .l1_l2_req_ar_o     (l1_l2_ar),
      // ewrq -> mem bus
      // AW
      .l1_l2_req_awvalid_o(l1_l2_awvalid),
      .l1_l2_req_awready_i(l1_l2_awready),
      .l1_l2_req_aw_o     (l1_l2_aw),
      // W
      .l1_l2_req_wvalid_o (l1_l2_wvalid),
      .l1_l2_req_wready_i (l1_l2_wready),
      .l1_l2_req_w_o      (l1_l2_w),
      // L1D -> L2 : Response
      // B
      .l2_l1_resp_bvalid_i(l1_l2_bvalid),
      .l2_l1_resp_bready_o(l1_l2_bready),
      .l2_l1_resp_b_i     (l1_l2_b),
      // mem bus -> mlfb
      // R
      .l2_l1_resp_rvalid_i(l1_l2_rvalid),
      .l2_l1_resp_rready_o(l1_l2_rready),
      .l2_l1_resp_r_i     (l1_l2_r),

      .clk                (clk),
      .rst                (rst)
  );

rvh_l1d_bank_axi_arb
#(
  .INPUT_PORT_NUM (2),
  .RESP_PORT_SELECT_BID_LSB($clog2(L1D_BANK_ID_NUM)) // bid low 3 bit for cache bank id, the highest used for distinguish i$/d$
)
L1_CACHE_AXI_ARB_U
(
    // L1D banks -> axi arb
      // AR
    .l1d_bank_axi_arb_arvalid                 (l1_l2_arvalid  ),
    .l1d_bank_axi_arb_arready                 (l1_l2_arready  ),
    .l1d_bank_axi_arb_ar                      (l1_l2_ar       ),
      // AW
    .l1d_bank_axi_arb_awvalid                 (l1_l2_awvalid  ),
    .l1d_bank_axi_arb_awready                 (l1_l2_awready  ),
    .l1d_bank_axi_arb_aw                      (l1_l2_aw       ),
      // W
    .l1d_bank_axi_arb_wvalid                  (l1_l2_wvalid   ),
    .l1d_bank_axi_arb_wready                  (l1_l2_wready   ),
    .l1d_bank_axi_arb_w                       (l1_l2_w        ),
      // B
    .l1d_bank_axi_arb_bvalid                  (l1_l2_bvalid   ),
    .l1d_bank_axi_arb_bready                  (l1_l2_bready   ),
    .l1d_bank_axi_arb_b                       (l1_l2_b        ),
      // R
    .l1d_bank_axi_arb_rvalid                  (l1_l2_rvalid   ),
    .l1d_bank_axi_arb_rready                  (l1_l2_rready   ),
    .l1d_bank_axi_arb_r                       (l1_l2_r        ),

    // axi arb -> L2
      // AR
    .axi_arb_l2_arvalid                       (l1_l2_arvalid_from_arb      ),
    .axi_arb_l2_arready                       (l1_l2_arready_from_arb      ),
    .axi_arb_l2_ar                            (l1_l2_ar_from_arb           ),
      // AW
    .axi_arb_l2_awvalid                       (l1_l2_awvalid_from_arb      ),
    .axi_arb_l2_awready                       (l1_l2_awready_from_arb      ),
    .axi_arb_l2_aw                            (l1_l2_aw_from_arb           ),
      // W
    .axi_arb_l2_wvalid                        (l1_l2_wvalid_from_arb       ),
    .axi_arb_l2_wready                        (l1_l2_wready_from_arb       ),
    .axi_arb_l2_w                             (l1_l2_w_from_arb            ),
      // B
    .axi_arb_l2_bvalid                        (l1_l2_bvalid_from_arb       ),
    .axi_arb_l2_bready                        (l1_l2_bready_from_arb       ),
    .axi_arb_l2_b                             (l1_l2_b_from_arb            ),
      // R
    .axi_arb_l2_rvalid                        (l1_l2_rvalid_from_arb       ),
    .axi_arb_l2_rready                        (l1_l2_rready_from_arb       ),
    .axi_arb_l2_r                             (l1_l2_r_from_arb            ),

    .clk    (clk),
    .rst    (rst)

);

`ifndef SYNTHESIS
  axi_mem #(
      .ID_WIDTH($bits(mem_tid_t)),
      .MEM_SIZE(1 << 29),  //byte 512MB
      .mem_clear(0),
      .mem_simple_seq(0),
      .READ_DELAY_CYCLE(1 << 7),
      .READ_DELAY_CYCLE_RANDOMLIZE(1),
      .READ_DELAY_CYCLE_RANDOMLIZE_UPDATE_CYCLE(1 << 10),
      .AXI_DATA_WIDTH(MEM_DATA_WIDTH)  // bit
  ) u_axi_mem (
        .clk      (clk),
        .rst_n    (~rst)
      //AW
      , .i_awid   (l1_l2_aw_from_arb.awid),
        .i_awaddr (l1_l2_aw_from_arb.awaddr),
        .i_awlen  (l1_l2_aw_from_arb.awlen),
        .i_awsize (l1_l2_aw_from_arb.awsize),
        .i_awburst(l1_l2_aw_from_arb.awburst)   // INCR mode
      , .i_awvalid(l1_l2_awvalid_from_arb),
        .o_awready(l1_l2_awready_from_arb)
      //AR
      , .i_arid   (l1_l2_ar_from_arb.arid),
        .i_araddr (l1_l2_ar_from_arb.araddr),
        .i_arlen  (l1_l2_ar_from_arb.arlen),
        .i_arsize (l1_l2_ar_from_arb.arsize),
        .i_arburst(l1_l2_ar_from_arb.arburst),
        .i_arvalid(l1_l2_arvalid_from_arb),
        .o_arready(l1_l2_arready_from_arb)
      //W
      , .i_wdata  (l1_l2_w_from_arb.wdata),
        .i_wstrb  ('1),
        .i_wlast  (l1_l2_w_from_arb.wlast),
        .i_wvalid (l1_l2_wvalid_from_arb),
        .o_wready (l1_l2_wready_from_arb)
      //B
      , .o_bid    (l1_l2_b_from_arb.bid),
        .o_bresp  (l1_l2_b_from_arb.bresp),
        .o_bvalid (l1_l2_bvalid_from_arb),
        .i_bready (l1_l2_bready_from_arb)
      //R
      , .o_rid    (l1_l2_r_from_arb.rid),
        .o_rdata  (l1_l2_r_from_arb.dat),
        .o_rresp  (l1_l2_r_from_arb.rresp),
        .o_rlast  (l1_l2_r_from_arb.rlast),
        .o_rvalid (l1_l2_rvalid_from_arb),
        .i_rready (l1_l2_rready_from_arb)

  );

  assign l1_l2_r_from_arb.mesi_sta = EXCLUSIVE;
  assign l1_l2_r_from_arb.err      = 1'b0;
`endif


endmodule

