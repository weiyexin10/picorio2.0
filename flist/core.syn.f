-f $PROJ_ROOT/flist/util.syn.f
-f $PROJ_ROOT/flist/include.syn.f

-f $PROJ_ROOT/flist/l1d.f
-f $PROJ_ROOT/flist/l1i.f
-f $PROJ_ROOT/flist/axi_crossbar.syn.f

-f $PROJ_ROOT/flist/mmu.syn.f
-f $PROJ_ROOT/flist/frontend.syn.f
-f $PROJ_ROOT/flist/backend.syn.f
-f $PROJ_ROOT/flist/peripheral.syn.f
-f $PROJ_ROOT/flist/clint.syn.f
$PROJ_ROOT/rtl/rvh_core/rvh_core.sv