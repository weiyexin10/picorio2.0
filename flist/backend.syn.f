-f $PROJ_ROOT/flist/include.syn.f
-f $PROJ_ROOT/flist/util.syn.f

$PROJ_ROOT/rtl/rvh_decode/rvc_expender.sv
$PROJ_ROOT/rtl/rvh_decode/riscv_decoder.sv
$PROJ_ROOT/rtl/rvh_decode/rvh_decode.sv

$PROJ_ROOT/rtl/rvh_rcu/rvh_rcu_pkg.sv
$PROJ_ROOT/rtl/rvh_rcu/prf_freelist.sv
$PROJ_ROOT/rtl/rvh_rcu/rvh_rat.sv
$PROJ_ROOT/rtl/rvh_rcu/rvh_brq.sv
$PROJ_ROOT/rtl/rvh_rcu/rvh_rob.sv
$PROJ_ROOT/rtl/rvh_rcu/rvh_rcu.sv

$PROJ_ROOT/rtl/rvh_rename/rvh_rename.sv

$PROJ_ROOT/rtl/rvh_dispatch/int_disp_buffer.sv
$PROJ_ROOT/rtl/rvh_dispatch/int_disp_router.sv
$PROJ_ROOT/rtl/rvh_dispatch/ls_disp_buffer.sv
$PROJ_ROOT/rtl/rvh_dispatch/ls_disp_router.sv
$PROJ_ROOT/rtl/rvh_dispatch/rvh_dispatch.sv

$PROJ_ROOT/rtl/rvh_iew/rvh_cdb.sv
$PROJ_ROOT/rtl/rvh_iew/int_prf.sv

$PROJ_ROOT/rtl/rvh_iew/br_iq_entry.sv
$PROJ_ROOT/rtl/rvh_iew/br_iq.sv
$PROJ_ROOT/rtl/rvh_fu_pool/rvh_bru.sv
$PROJ_ROOT/rtl/rvh_iew/br_ctrl_pipe.sv

$PROJ_ROOT/rtl/rvh_iew/int_iq_entry.sv
$PROJ_ROOT/rtl/rvh_iew/int_iq.sv
$PROJ_ROOT/rtl/rvh_fu_pool/rvh_alu.sv
$PROJ_ROOT/rtl/rvh_iew/int_ctrl_pipe.sv

$PROJ_ROOT/rtl/rvh_iew/complex_int_iq_entry.sv
$PROJ_ROOT/rtl/rvh_iew/complex_int_iq.sv
$PROJ_ROOT/rtl/rvh_fu_pool/rvh_mul.sv
-f $PROJ_ROOT/flist/dw.syn.f
$PROJ_ROOT/rtl/rvh_fu_pool/rvh_div.sv
$PROJ_ROOT/rtl/rvh_iew/complex_int_ctrl_pipe.sv

$PROJ_ROOT/rtl/rvh_iew/agu_iq_entry.sv
$PROJ_ROOT/rtl/rvh_iew/agu_iq.sv
$PROJ_ROOT/rtl/rvh_fu_pool/rvh_agu.sv
$PROJ_ROOT/rtl/rvh_iew/agu_ctrl_pipe.sv

$PROJ_ROOT/rtl/rvh_iew/sdu_iq_entry.sv
$PROJ_ROOT/rtl/rvh_iew/sdu_iq.sv
$PROJ_ROOT/rtl/rvh_iew/sdu_ctrl_pipe.sv

$PROJ_ROOT/rtl/rvh_misc/csr_regfile.sv
$PROJ_ROOT/rtl/rvh_misc/fence_unit.sv
$PROJ_ROOT/rtl/rvh_iew/csr_ctrl_pipe.sv

$PROJ_ROOT/rtl/rvh_iew/rvh_iew.sv

$PROJ_ROOT/rtl/rvh_lsu/rvh_lsu_pkg.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_rar_queue_entry.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_rar_queue.sv  
$PROJ_ROOT/rtl/rvh_lsu/rvh_raw_queue_entry.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_raw_queue.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_lrq_entry.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_lrq.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_stq_entry.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_stq.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_raw_check.sv 
$PROJ_ROOT/rtl/rvh_lsu/rvh_ls_pipe.sv 

$PROJ_ROOT/rtl/rvh_lsu/rvh_lsu.sv

$PROJ_ROOT/rtl/rvh_backend/rvh_backend.sv
