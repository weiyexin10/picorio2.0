# Picorio2.0
PicoRio2.0 is a high-performance processor that supports RV64IMAC instruction set and SV39 privileged instruction set. It features a 4-issue 11-stage superscalar out-of-order pipeline, achieves ~7 CoreMark/MHz with -O2 optimization. In PicoRio2.0, we implemented:
* A fetch pipeline adopt FDP architecture, a decoupled branch prediction pipeline. The fetch pipeline effectively hides the latency of ICache misses, improving instruction throughput. The branch prediction pipeline adopt a multi-stage design, achieving a balance between prediction accuracy and delay. The BPU support up to 2 branches at the same time.
* An out-of-order execution pipeline with powerful parallel execution capabilities,
further enhanced through architectural optimizations. Structural hazards are
resolved during dispatch stage by adding a dispatch buffer. In the read operand
stage, shared read ports and the common data bus design reduce PRF power
consumption, alleviate timing pressure and power consumption of PRF while maintaining performance.
* A high-performance, speculative load-store unit. Achieving higher memory instruction throughput by speculatively executing load
instructions. Traditional LDQ
is decomposed into LRQ, RARQ and RAWQ, resulting in optimized timing and
area. The pre-commit mechanism for store increases store instruction throughput.

The figure below shows the uArch of Picorio2.0.

![](docs/figures/uArch.jpg)

# Build

The repo contains only the core of Picorio2.0 and the corresponding testbench for the core, does not contains the SoC part. To build the core with VCS:

    $ cd tb/core
    $ source ../../env/sourceme
    $ make vcs

# Run
This repository provides the basic ISA test located in `tb/regression`. To run the regression, please run the following command in `tb/core`:

    $ make regression

To run any elf file, please run the following command in `tb/core`:

    $ ./simv +image=<path to elf file>

Note:
* The testbench here adopt the same address mapping scheme and the same to/from host interaction method with Dromajo. Before running your ELF file in this testbench, you may want to run it with Dromajo first to ensure that your ELF file can run correctly here.

