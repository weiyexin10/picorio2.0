//      Memory map 
//------------------------------------------------------------------------------------------------------------
//      Reg         |   Offset Address          | Description
//------------------|---------------------------|-------------------------------------------------------------
//      msip(n)     |   0x0000 + 0x4 * n        | (n inside [0:N_CPU-1]
//      mtimecmp(n) |   0x4000 + 0x8 * n        | (n inside [0:N_CPU-1]
//      timer_en    |   0xbff0                  | timer enable signal ,default is 1'b0, means default is off.
//      mtime lo    |   0xbff8                  | 64-bit mtime low  32-bit,counting up if timer_en is 1'b1, will keep when timer_en is 1'b0
//      mtime hi    |   0xbffc                  | 64-bit mtime high 32-bit,counting up if timer_en is 1'b1, will keep when timer_en is 1'b0
//==========================================================================================================================================

module rvh_plic
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned  ID_WIDTH          = 0,
    parameter int unsigned  AXI_ADDR_WIDTH    = 0,
    parameter int unsigned  AXI_DATA_WIDTH    = 0,
    parameter int unsigned  AXI_USER_WIDTH    = 0,
    parameter logic [AXI_ADDR_WIDTH-1:0]  BASE_ADDR    = 0
)
(
    input                                       clk,
    input                                       rstn,
    
    // aw
    input  logic [ID_WIDTH-1:0]                   slave_aw_id,
    input  logic [AXI_ADDR_WIDTH-1:0] 	        slave_aw_addr,
    input  logic [7:0] 	                        slave_aw_len,
    input  logic [2:0] 	                        slave_aw_size,
    input  logic [1:0] 	                        slave_aw_burst,
    input  logic     		                        slave_aw_valid,
    output logic 	     	                        slave_aw_ready,
    // R
    input  logic [ID_WIDTH-1:0]                   slave_ar_id,
    input  logic [AXI_ADDR_WIDTH-1:0] 	        slave_ar_addr,
    input  logic [7:0] 	                        slave_ar_len,
    input  logic [2:0] 	                        slave_ar_size,
    input  logic [1:0] 	                        slave_ar_burst,
    input  logic 		                            slave_ar_valid,
    output logic 		                            slave_ar_ready,
    // W
    input  logic [AXI_DATA_WIDTH-1:0] 	        slave_w_data,
    input  logic [AXI_DATA_WIDTH/8-1:0] 	        slave_w_strb,
    input  logic 		                            slave_w_last,
    input  logic 		                            slave_w_valid,
    output logic 		                            slave_w_ready,
    // B
    output logic [ID_WIDTH-1:0]                   slave_b_id,
    output logic [1:0] 	                        slave_b_resp,
    output logic 		                            slave_b_valid,
    input  logic 		                            slave_b_ready,
    // R
    output logic [ID_WIDTH-1:0]                   slave_r_id,
    output logic [AXI_DATA_WIDTH-1:0] 	        slave_r_data,
    output logic [1:0] 	                        slave_r_resp,
    output logic 		                            slave_r_last,
    output logic 		                            slave_r_valid,
    input  logic 		                            slave_r_ready,


    input logic                                uart_int_i
);

    logic                           ce;
    logic                           we;
    logic [AXI_DATA_WIDTH/8-1:0]    be;
    logic [AXI_DATA_WIDTH-1:0]      wdata;
    logic [AXI_DATA_WIDTH-1:0]      rdata;
    logic [AXI_ADDR_WIDTH-1:0]      addr;
    logic [25:0]                    offset;

    logic [2:0]                     priority_q;     // 0x4
    logic [31:0]                    pending_irq_q;  // 0x1000
    logic [31:0]                    irq_enable_q;   // 0x2000
    logic [2:0]                     threshold_q;   // 0x200000
    logic [31:0]                    claim_complete_q;   // 0x200004

    logic                           uart_int;

    

    logic [AXI_DATA_WIDTH-1:0] 	 w_biten;
    always_comb begin
        for (int i = 0; i < 64; i++) begin
            w_biten[i] = slave_w_strb[i / 8];
        end
    end

    axi2mem #(
        .ID_WIDTH          (ID_WIDTH      ),
        .AXI_ADDR_WIDTH    (AXI_ADDR_WIDTH),
        .AXI_DATA_WIDTH    (AXI_DATA_WIDTH),
        .AXI_USER_WIDTH    (AXI_USER_WIDTH)
    )axi2mem_u(
        .clk_i               (clk),   
        .rst_ni              (rstn),  
    //AW
        .slave_aw_id         (slave_aw_id   ),
        .slave_aw_addr       (slave_aw_addr ),
        .slave_aw_len        (slave_aw_len  ),
        .slave_aw_size       (slave_aw_size ),
        .slave_aw_burst      (slave_aw_burst),
        .slave_aw_valid      (slave_aw_valid),
        .slave_aw_ready      (slave_aw_ready),
    //AR
        .slave_ar_id         (slave_ar_id   ),
        .slave_ar_addr       (slave_ar_addr ),
        .slave_ar_len        (slave_ar_len  ),
        .slave_ar_size       (slave_ar_size ),
        .slave_ar_burst      (slave_ar_burst),
        .slave_ar_valid      (slave_ar_valid),
        .slave_ar_ready      (slave_ar_ready),
    //W
        .slave_w_data        (slave_w_data),
        .slave_w_strb        (w_biten),
        .slave_w_last        (slave_w_last ),
        .slave_w_valid       (slave_w_valid),
        .slave_w_ready       (slave_w_ready),
    //B
        .slave_b_id          (slave_b_id   ),
        .slave_b_resp        (slave_b_resp ),
        .slave_b_valid       (slave_b_valid),
        .slave_b_ready       (slave_b_ready),
    //R
        .slave_r_id          (slave_r_id   ),
        .slave_r_data        (slave_r_data ),
        .slave_r_resp        (slave_r_resp ),
        .slave_r_last        (slave_r_last ),
        .slave_r_valid       (slave_r_valid),
        .slave_r_ready       (slave_r_ready),

        .req_o               (ce),
        .we_o                (we),
        .addr_o              (addr),
        .be_o                (be),
        .data_o              (wdata),
        .data_i              (rdata)
    );

    assign offset = ((addr - BASE_ADDR) & 'h3FFFFFF);
    assign uart_int = uart_int_i & (priority_q > threshold_q) & irq_enable_q[1];
    

    // priority
    always_ff @(posedge clk) begin
        if (~rstn) begin
            priority_q <= '0;
        end
        else if (ce & we) begin
            if (offset == 'h4) begin
                priority_q <= wdata[2:0];
            end
        end
    end

    // enable
    always_ff @(posedge clk) begin
        if (~rstn) begin
            irq_enable_q <= '0;
        end
        else if (ce & we) begin
            if (offset == 'h2000) begin
                irq_enable_q <= wdata[31:0];
            end
        end
    end

    // threshold
    always_ff @(posedge clk) begin
        if (~rstn) begin
            threshold_q <= '0;
        end
        else if (ce & we) begin
            if ((offset == 'h200000) & be[0]) begin
                threshold_q <= wdata[2:0];
            end
        end
    end

    // claim & complete
    always_ff @(posedge clk) begin
        if (~rstn) begin
            claim_complete_q <= 0;
        end
        else if (ce & we) begin
            if ((offset == 'h200000) & be[4]) begin
                if (wdata[34:32] == 'h1) begin
                    claim_complete_q <= 0;
                end
            end
        end
        else if (uart_int & (claim_complete_q == 'h0)) begin
            claim_complete_q <= 'h1;
        end
    end

    always_ff @(posedge clk) begin
        if (~rstn) begin
            pending_irq_q <= 0;
        end
        else if (ce & ~we) begin
            if (offset == 'h200004) begin
                pending_irq_q <= 0;
            end
        end
        else if (uart_int & (claim_complete_q == 'h0) & (claim_complete_q == 'h0)) begin
            pending_irq_q <= 'h1;
        end
    end

    // read
    always_comb begin
        if ((offset & ~('b111)) == 'h4) begin
            rdata = priority_q;
        end
        else if ((offset & ~('b111)) == 'h1000) begin
            rdata = pending_irq_q;
        end
        else if ((offset & ~('b111)) == 'h2000) begin
            rdata = irq_enable_q;
        end
        else if ((offset & ~('b111)) == 'h200000) begin
            rdata = {claim_complete_q, threshold_q};
        end
        else begin
            rdata = 0;
        end
    end




endmodule
