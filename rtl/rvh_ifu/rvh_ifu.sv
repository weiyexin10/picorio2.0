module rvh_ifu
  import rvh_pkg::*;
  import riscv_pkg::*;
  import rvh_l1i_pkg::*;
#(
) (
    input        [                    1:0]                          priv_lvl_i,
    // mstatus
    input                                                           mstatus_mxr,
    input                                                           mstatus_sum,
    // Satp
    input        [         MODE_WIDTH-1:0]                          satp_mode_i,
    input        [         ASID_WIDTH-1:0]                          satp_asid_i,
    // fe > be
    output logic [       DECODE_WIDTH-1:0]                          fe_dec_instr_valid_o,
    output logic [       DECODE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] fe_dec_btq_tag_o,
    output logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] fe_dec_pc_o,
    output logic [       DECODE_WIDTH-1:0][                   31:0] fe_dec_instr_o,
    output logic [       DECODE_WIDTH-1:0]                          fe_dec_is_rvc_o,
    output logic [       DECODE_WIDTH-1:0]                          fe_dec_pred_taken_o,
    output logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] fe_dec_pred_target_o,
    output logic                                                    fe_dec_excp_vld_o,
    output logic [    EXCP_TVAL_WIDTH-1:0]                          fe_dec_excp_tval_o,
    output logic [   EXCP_CAUSE_WIDTH-1:0]                          fe_dec_excp_cause_o,
    input  logic                                                    fe_dec_ready_i,
    // bru redirect
    input  logic                                                    bru_redirect_valid_i,
    input  logic [PC_LOB_WIDTH-1:0]                                 bru_redirect_pc_lob_i,
    input  logic [        VADDR_WIDTH-1:0]                          bru_redirect_target_i,
    input  logic [      BTQ_TAG_WIDTH-1:0]                          bru_redirect_btq_tag_i,
    input  logic                                                    bru_redirect_taken_i,
    // cmt > btq                   
    input  logic [       RETIRE_WIDTH-1:0]                          cmt_valid_i,
    input  logic [       RETIRE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] cmt_btq_tag_i,
    input  logic [       RETIRE_WIDTH-1:0][PC_LOB_WIDTH-1:0]        cmt_pc_lob_i,
    // cmt redirect
    input  logic                                                    cmt_redirect_valid_i,
    input  logic [PC_LOB_WIDTH-1:0]                                 cmt_redirect_pc_lob_i,
    input  logic [        VADDR_WIDTH-1:0]                          cmt_redirect_target_i,
    input  logic [      BTQ_TAG_WIDTH-1:0]                          cmt_redirect_btq_tag_i,

    input logic [VADDR_WIDTH-1:0]                                   boot_address_i,

    // L1 TLB -> STLB : Look up Request
    output logic                                                    stlb_req_vld_o,
    output logic [ITLB_TRANS_ID_WIDTH-1:0]                          stlb_req_trans_id_o,
    output logic [         ASID_WIDTH-1:0]                          stlb_req_asid_o,
    output logic [   PMA_ACCESS_WIDTH-1:0]                          stlb_req_access_type_o,
    output logic [          VPN_WIDTH-1:0]                          stlb_req_vpn_o,
    input  logic                                                    stlb_req_rdy_i,
    // STLB -> L1 TLB : Look up Response
    input  logic                                                    stlb_resp_vld_i,
    input  logic [ITLB_TRANS_ID_WIDTH-1:0]                          stlb_resp_trans_id_i,
    input  logic [         ASID_WIDTH-1:0]                          stlb_resp_asid_i,
    input  logic [      PTE_LVL_WIDTH-1:0]                          stlb_resp_pte_lvl_i,
    input  logic [               XLEN-1:0]                          stlb_resp_pte_i,
    input  logic [          VPN_WIDTH-1:0]                          stlb_resp_vpn_i,
    input  logic [   PMA_ACCESS_WIDTH-1:0]                          stlb_resp_access_type_i,
    input  logic                                                    stlb_resp_access_fault_i,
    input  logic                                                    stlb_resp_page_fault_i,

    // ITLB Entry Evict
    output logic                                                    itlb_evict_vld_o,
    output logic [PTE_WIDTH-1:0]                                    itlb_evict_pte_o,
    output logic [PAGE_LVL_WIDTH-1:0]                               itlb_evict_page_lvl_o,
    output logic [VPN_WIDTH-1:0]                                    itlb_evict_vpn_o,
    output logic [ASID_WIDTH-1:0]                                   itlb_evict_asid_o,

    // Execute SFENCE.VMA : Flush ITLB
    input logic                                                     flush_itlb_req_i,
    input logic                                                     flush_itlb_use_vpn_i,
    input logic                                                     flush_itlb_use_asid_i,
    input logic [VPN_WIDTH-1:0]                                     flush_itlb_vpn_i,
    input logic [ASID_WIDTH-1:0]                                    flush_itlb_asid_i,
    output logic                                                    flush_itlb_gnt_o,

    input logic                                                     flush_icache_req_i,
    output logic                                                    flush_icache_gnt_o,

     // AR
    output logic                                                    l1i_l2_req_arvalid_o,
    input  logic                                                    l1i_l2_req_arready_i,
    output cache_mem_if_ar_t                                        l1i_l2_req_ar_o,
    // AW 
    output logic                                                    l1i_l2_req_awvalid_o,
    input  logic                                                    l1i_l2_req_awready_i,
    output cache_mem_if_aw_t                                        l1i_l2_req_aw_o,
    // W 
    output logic                                                    l1i_l2_req_wvalid_o,
    input  logic                                                    l1i_l2_req_wready_i,
    output cache_mem_if_w_t                                         l1i_l2_req_w_o,
    // Response
    // B
    input  logic                                                    l2_l1i_resp_bvalid_i,
    output logic                                                    l2_l1i_resp_bready_o,
    input  cache_mem_if_b_t                                         l2_l1i_resp_b_i,
    // mem bus -> mlfb
    // R
    input  logic                                                    l2_l1i_resp_rvalid_i,
    output logic                                                    l2_l1i_resp_rready_o,
    input  cache_mem_if_r_t                                         l2_l1i_resp_r_i,

    input logic clk,
    input logic rst
);

function logic [$clog2(GHIST_CAPACITY)-1:0] calc_ghist_ptr(input logic [$clog2(GHIST_CAPACITY)-1:0] old_ptr, input logic saw_taken_br, input logic saw_not_taken_br);
begin
    calc_ghist_ptr = old_ptr;
    if (saw_taken_br | saw_not_taken_br) begin
        if (old_ptr == GHIST_CAPACITY - 1) begin
            calc_ghist_ptr = 0;
        end
        else begin
            calc_ghist_ptr = old_ptr + 1'b1;
        end
    end
end
endfunction

function ghist_info_t calc_ghist_info(input ghist_info_t old_info, input logic saw_taken_br, input logic saw_not_taken_br);
begin
    calc_ghist_info.tail_ptr = calc_ghist_ptr(old_info.tail_ptr, saw_taken_br, saw_not_taken_br);
    if (saw_taken_br | saw_not_taken_br) begin
        calc_ghist_info.lsb = saw_taken_br;
    end
    else begin
        calc_ghist_info.lsb = old_info.lsb;
    end
end
endfunction

//////////////////////////////////// itlb/icache ///////////////////////////////////////////
logic                                               tlb_fetch_req_rdy;
logic [VPN_WIDTH-1:0]                               tlb_fetch_req_vpn;
logic                                               tlb_fetch_resp_vld;
logic                                               tlb_fetch_resp_excp_vld;
logic [XLEN-1:0]                                    tlb_fetch_resp_excp_cause;
logic [PPN_WIDTH-1:0]                               tlb_fetch_resp_ppn;
logic                                               tlb_fetch_resp_hit;

logic                                               tlb_prefetch_req_rdy;
logic [VPN_WIDTH-1:0]                               tlb_prefetch_req_vpn;
logic                                               tlb_prefetch_resp_vld;
logic                                               tlb_prefetch_resp_excp_vld;
logic [XLEN-1:0]                                    tlb_prefetch_resp_excp_cause;
logic [PPN_WIDTH-1:0]                               tlb_prefetch_resp_ppn;
logic                                               tlb_prefetch_resp_hit;

logic                                               ic_fetch_req_vld;
logic [FTQ_TAG_LEN-1:0]                             ic_fetch_req_tag;
logic [VADDR_WIDTH-1:0]                             ic_fetch_req_pc;
logic                                               ic_fetch_req_rdy;

logic                                               ic_prefetch_req_vld;
logic [FTQ_TAG_LEN-1:0]                             ic_prefetch_req_tag;
logic [VADDR_WIDTH-1:0]                             ic_prefetch_req_pc;
logic                                               ic_prefetch_req_rdy;

logic                                               ic_prefetch_probe_hit;
logic                                               ic_fetch_probe_hit;
logic                                               ic_fetch_probe_inflight;

logic                                               _ic_fetch_resp_vld, ic_fetch_resp_vld;
logic                                               ic_fetch_resp_excp_valid;
logic [    EXCP_TVAL_WIDTH-1:0]                     ic_fetch_resp_excp_tval;
logic [   EXCP_CAUSE_WIDTH-1:0]                     ic_fetch_resp_excp_cause;
logic [FTQ_TAG_LEN-1:0]                             ic_fetch_resp_tag;
logic [L1I_FETCH_WIDTH-1:0]                         ic_fetch_resp_cacheline;
logic [L1I_FETCH_WIDTH/16*2-1:0]                    ic_fetch_resp_boundary;
logic [L1I_FETCH_WIDTH/16*2-1:0]                    ic_fetch_resp_rvc_mask;
logic                                               ic_fetch_resp_two_blks;
logic                                               ic_fetch_resp_next_valid;
logic [VADDR_WIDTH-1:0]                             ic_fetch_resp_next_pc;
logic [BTQ_TAG_WIDTH-1:0]                           ic_fetch_resp_btq_tag;
logic                                               ic_fetch_resp_is_pred_taken;
logic                                               ic_fetch_resp_is_pred_truncate;
logic [$clog2(FETCH_WIDTH/16)-1:0]                  ic_fetch_resp_taken_truncate_offset;
logic [VADDR_WIDTH-1:0]                             ic_fetch_resp_pc;
logic                                               ic_flush;

////////////////////////////////////ftq///////////////////////////////////////////
    logic                                           ftq_enq_valid;
    logic [VADDR_WIDTH-1:0]                         ftq_enq_pc;
    logic [FTQ_TAG_LEN-1:0]                         ftq_enq_tag;
    logic                                           ftq_enq_ready;
    logic                                           btq_ftq_two_blk_b1;
    logic                                           bpu_ftq_is_taken_b1;
    logic                                           bpu_ftq_is_truncate_b1;
    logic[$clog2(FETCH_WIDTH/16)-1:0]               bpu_ftq_taken_truncate_offset_b1;

////////////////////////////////////btq///////////////////////////////////////////
    logic                                           btq_ftq_ready;
    logic [1:0]                                     btq_precheck_valid_banks;
    logic                                           btq_precheck_ready;

    logic                                           btq_alloc_ready;
    logic [1:0]                                     btq_alloc_valid_banks;
    ghist_info_t [1:0]                              btq_alloc_ghist_info_banks;
    ubtb_typedef::ubtb_meta_t[1:0]                  btq_alloc_ubtb_meta_banks;
    bht_typedef::bht_meta_t[1:0]                    btq_alloc_bht_meta_banks;
    tage_typedef::composed_tage_pred_meta_t[1:0]    btq_alloc_tage_meta_banks;
    btb_typedef::btb_meta_t[1:0]                    btq_alloc_btb_meta_banks;
    ras_typedef::ras_pred_t [1:0]                   btq_alloc_ras_pred_banks;
    ras_typedef::ras_meta_t [1:0]                   btq_alloc_ras_meta_banks;
    logic [1:0][BTQ_TAG_WIDTH-1:0]                  btq_alloc_tag_banks;

    logic [1:0]                                     fetch_btq_valid_banks;
    ghist_info_t [1:0]                              btq_fetch_ghist_info_banks;
    ghist_info_t [1:0]                              btq_fetch_next_ghist_info_banks;
    logic [1:0]                                     btq_fetch_next_ghist_info_valid_banks;
    ras_typedef::ras_pred_t [1:0]                   btq_fetch_ras_pred_banks;
    ras_typedef::ras_meta_t [1:0]                   btq_fetch_ras_meta_banks;

    ghist_info_t                                    fetch_btq_next_ghist_info;
    logic                                           fetch_btq_next_ghist_info_valid;

    logic  [DECODE_WIDTH-1:0][PC_HIB_WIDTH-1:0]     btq_ibuf_pc_hib;
    logic [DECODE_WIDTH-1:0][VADDR_WIDTH-1:0]       btq_ibuf_pred_target;
////////////////////////////////////btq redirect///////////////////////////////////////////
    logic                                           btq_redirect_is_misalign_insn;
    logic                                           btq_redirect_valid;
    logic [FETCH_WIDTH/16-1:0]                      btq_redirect_br_full_mask;
    ghist_info_t                                    btq_redirect_new_ghist_info;
    loop_typedef::loop_meta_t                       btq_redirect_loop_meta;
    ras_typedef::ras_pred_t                         btq_redirect_ras_pred;
    ras_typedef::ras_meta_t                         btq_redirect_ras_meta;
    logic [VADDR_WIDTH-1:0]                         btq_redirect_target;
    logic                                           btq_redirect_taken;
    logic [PC_LOB_WIDTH-1:0]                        btq_redirect_pc_lob;
    ghist_info_t                                    btq_redirect_ghist_info;
////////////////////////////////////btq update///////////////////////////////////////////
    logic                                           update_valid;
    logic [PREDS_PER_LINE-1:0]                      update_pred_valid;
    logic                                           update_truncate;
    logic [1:0]                                     update_valid_banks;
    logic [VADDR_WIDTH-1:0]                         update_req_pc;
    logic [VADDR_WIDTH-1:0]                         update_target;
    logic [PREDS_PER_LINE-1:0]                      update_cfi_mask;
    logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]  update_cfi_offset_vec;
    logic [PREDS_PER_LINE-1:0]                      update_cfi_taken_vec;
    logic [PREDS_PER_LINE-1:0]                      update_rvc_mask;
    logic [PREDS_PER_LINE-1:0]                      update_call_mask;
    logic [PREDS_PER_LINE-1:0]                      update_ret_mask;
    logic [PREDS_PER_LINE-1:0]                      update_br_mask;
    tage_typedef::composed_tage_pred_meta_t         update_composed_tage_meta;
    btb_typedef::btb_meta_t                         update_btb_meta;
    ubtb_typedef::ubtb_meta_t                       update_ubtb_meta;
    bht_typedef::bht_meta_t                         update_bht_meta;
    loop_typedef::loop_meta_t                       update_loop_meta;

    ghist_info_t                                    update_ghist_info;
    logic [GHIST_LEN-1:0]                           update_ghist;
    logic                                           update_is_misalign;

////////////////////////////////////bp pipe///////////////////////////////////////////
    logic [           VADDR_WIDTH-1:0]           pc_f0;
    logic [           VADDR_WIDTH-1:0]           next_pkt_pc_f0;
    logic                                        next_pkt_valid_f0;
    logic [1:0]                                  req_valid_banks_f0;
    logic [1:0][           VADDR_WIDTH-1:0]      pc_banks_f0;
    logic [1:0]                                  is_older_banks_f0;

    logic [1:0]                                  bpp_vld_banks_b1;
    logic [1:0]                                  bpp_other_vld_banks_b1;
    logic [1:0][           VADDR_WIDTH-1:0]      pc_banks_b1;
    logic [1:0]                                  is_older_banks_b1;
    logic [1:0][       FTQ_TAG_LEN-1:0]          bpp_ftq_tag_banks_b1;
    logic [1:0]                                  bpp_is_taken_banks_b1;
    logic [1:0]                                  bpp_is_truncate_banks_b1;
    logic [1:0][$clog2(FETCH_WIDTH/16)-1:0]      bpp_offset_banks_b1;
    logic [1:0]                                  saw_taken_br_banks_b1;
    logic [1:0]                                  saw_not_taken_br_banks_b1;
    logic [1:0][           VADDR_WIDTH-1:0]      next_pc_banks_b1;
    ras_typedef::ras_pred_t[1:0]                 ras_pred_banks_b1;
    ras_typedef::ras_pred_t[1:0]                 ras_pred_ff_banks_b1;
    ras_typedef::ras_meta_t                      ras_meta_b1;
    logic [VADDR_WIDTH-1:0]                      ras_target_b1;

    logic [1:0]                                  bpp_vld_banks_b2;
    logic [1:0]                                  bpp_other_vld_banks_b2;
    logic [1:0][           VADDR_WIDTH-1:0]      pc_banks_b2;
    logic [1:0]                                  is_older_banks_b2;
    logic [1:0][       FTQ_TAG_LEN-1:0]          bpp_ftq_tag_banks_b2;
    logic [1:0]                                  bpp_is_taken_banks_b2;
    logic [1:0]                                  bpp_is_truncate_banks_b2;
    logic [1:0][$clog2(FETCH_WIDTH/16)-1:0]      bpp_offset_banks_b2;
    ghist_info_t [1:0]                           ghist_info_banks_b2;
    logic [1:0]                                  saw_taken_br_banks_b2;
    logic [1:0]                                  saw_not_taken_br_banks_b2;
    logic [1:0][           VADDR_WIDTH-1:0]      bpp_red_target_banks_b2;
    ras_typedef::ras_pred_t[1:0]                 ras_pred_banks_b2;
    ras_typedef::ras_pred_t[1:0]                 ras_pred_ff_banks_b2;
    ras_typedef::ras_meta_t[1:0]                 ras_meta_banks_b2;
    ubtb_typedef::ubtb_meta_t [1:0]              ubtb_meta_banks_b2;
    bht_typedef::bht_meta_t [1:0]                bht_meta_banks_b2;
    tage_typedef::composed_tage_pred_meta_t [1:0]composed_tage_meta_banks_b2;
    btb_typedef::btb_meta_t [1:0]                btb_meta_banks_b2;
    loop_typedef::loop_meta_t [1:0]              loop_meta_banks_b2;
    
    // logic [1:0]                                  bpp_req_rdy_banks;
    // logic                                        bpp_req_rdy;
////////////////////////////////////bp selected signals////////////////////////////////////
    logic                                   older_ptr_b1;
    logic                                   newer_ptr_b1;
    logic                                   taken_truncate_ptr_b1;
    logic                                   bpu_vld_b1;
    logic [           VADDR_WIDTH-1:0]      pc_b1;
    logic [           VADDR_WIDTH-1:0]      next_pc_b1;
    logic [GHIST_LEN-1:0]                   ghist_b1;
    ghist_info_t                            ghist_info_b1;
    ghist_info_t                            older_pred_ghist_info_b1;
    ghist_info_t                            pred_ghist_info_b1;
    logic                                   fix_inner_stage_pred_b1;
    logic                                   fix_inter_stage_pred_b1;
    logic [1:0]                             invalidate_banks_b1;

    logic                                   older_ptr_b2;
    logic                                   newer_ptr_b2;
    logic                                   is_taken_b2;
    logic                                   is_truncate_b2;
    logic [$clog2(FETCH_WIDTH/16)-1:0]      pred_taken_truncate_offset_b2;
    logic                                   taken_truncate_ptr_b2;
    logic                                   ras_fixing_ptr_b2;
    logic                                   fix_ftq_b2;
    logic                                   fix_req_two_blks_b2;
    logic                                   fix_pred_b2;
    logic                                   fix_ghist_pred_b2;
    logic                                   bpu_red_vld_b2;
    logic [           VADDR_WIDTH-1:0]      pc_b2;
    logic [           VADDR_WIDTH-1:0]      bpu_red_target_b2;
    ghist_info_t                            ghist_info_b2;
    ghist_info_t                            older_pred_ghist_info_b2;
    ghist_info_t                            newer_pred_ghist_info_b2;
    ghist_info_t                            pred_ghist_info_b2;
    logic                                   bpu_vld_b2;
    logic                                   fix_inner_stage_pred_b2;
    logic                                   fix_inter_stage_pred_b2;
    logic                                   fix_ras_b2;
    logic [1:0]                             invalidate_banks_b2;

////////////////////////////////////pre-decoder///////////////////////////////////////////  
    logic [1:0]                                 fetch_valid_banks;
    logic [1:0]                                 fetch_taken_banks;
    logic [1:0][VADDR_WIDTH-1:0]                fetch_pc_banks;
    logic [1:0][FETCH_WIDTH-1:0]                fetch_fpkt_banks;
    logic [1:0][FETCH_WIDTH/16*2-1:0]           fetch_fpkt_boundary_banks;
    logic [1:0][FETCH_WIDTH/16*2-1:0]           fetch_fpkt_rvc_mask_banks;
    logic [1:0][$clog2(FETCH_WIDTH/16)-1:0]     fetch_taken_truncate_offset_banks;
    logic [1:0]                                 fetch_next_valid_banks;
    logic [1:0][VADDR_WIDTH-1:0]                fetch_next_pc_banks;
    logic [1:0][BTQ_TAG_WIDTH-1:0]              fetch_btq_tag_banks;

    logic                                        pd_cfi_taken;
    logic                                        pd_pred_fix_ptr;
    logic                                        pd_ghist_fix_ptr;
    logic [1:0]                                  pd_cfi_taken_banks;
    logic [1:0]                                  pd_truncate_banks;
    logic [1:0]                                  pd_cfi_taken_for_sel;
    logic [1:0]                                  pd_truncate_for_sel;
    logic [1:0][           VADDR_WIDTH-1:0]      pd_cfi_target_banks;
    logic [1:0][           VADDR_WIDTH-1:0]      pd_cfi_target_banks_for_sel;

    logic [1:0][PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0] pd_cfi_offset_vec_banks;
    logic [1:0][PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0] pd_cfi_offset_vec_banks_for_sel;
    logic [1:0][PREDS_PER_LINE-1:0]                             pd_cfi_taken_vec_banks;
    logic [1:0][PREDS_PER_LINE-1:0]                             pd_cfi_taken_vec_banks_for_sel;
    logic [1:0][PREDS_PER_LINE-1:0]                             pd_cfi_valid_vec_banks;
    logic [1:0][PREDS_PER_LINE-1:0]                             pd_cfi_valid_vec_banks_for_sel;

    logic [1:0][FETCH_WIDTH/16-1:0]              pd_rvc_mask_for_ras_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_inst_rvc_mask_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_inst_rvc_mask_for_sel;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_jal_mask_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_jal_mask_for_sel;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_jalr_mask_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_jalr_mask_for_sel;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_return_mask_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_return_mask_for_sel;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_call_mask_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_call_mask_for_sel;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_br_mask_banks;
    logic [1:0][FETCH_WIDTH/16-1:0]              pd_br_mask_for_sel;
    logic [1:0][(FETCH_WIDTH/16)-1:0]            pd_cfi_taken_mask_banks; 
    logic [1:0][(FETCH_WIDTH/16)-1:0]            pd_cfi_taken_mask_for_sel; 
    logic [1:0][(FETCH_WIDTH/16)-1:0]            pd_cfi_taken_mask_aligned_banks;
    logic [1:0]                                  pd_has_misalign_banks;
    logic [1:0]                                  pd_has_misalign_for_sel;
    logic [1:0][                    15:0]        pd_misalign_half_banks; 
    logic [1:0][                    15:0]        pd_misalign_half_for_sel; 
    logic [1:0][PC_LOB_WIDTH-1:0]                pd_fpkt_tail_pc_lob_banks;
    logic [1:0][PC_LOB_WIDTH-1:0]                pd_cacheline_tail_pc_lob_for_sel;
    logic [1:0]                                  pd_call_taken_banks, pd_ret_taken_banks;
    logic [1:0]                                  pd_call_taken_for_sel, pd_ret_taken_for_sel;
    ghist_info_t [1:0]                           pd_pred_ghist_info_banks;
    ghist_info_t [1:0]                           pd_pred_ghist_info_for_sel;
    logic [1:0][VADDR_WIDTH-1:0]                 pd_addr_next_to_cfi_banks;
    logic [1:0][VADDR_WIDTH-1:0]                 pd_addr_next_to_cfi_for_sel;
    logic [1:0][$clog2(FETCH_WIDTH/16):0]        pd_hword_num_banks;
    logic [1:0][$clog2(FETCH_WIDTH/16):0]        pd_hword_num_for_sel;
    ras_typedef::ras_pred_t [1:0]                pd_ras_pred_banks;
    ras_typedef::ras_pred_t [1:0]                pd_ras_pred_banks_for_sel;
    
////////////////////////////////////pre-decoder///////////////////////////////////////////  

    logic                                        ftq_ready;

    logic                                        check_ghist_in_btq_f3;
    logic [1:0]                                  btq_enq_fire_f3;
    logic                                        fetch_btq_invalid;
    logic [BTQ_TAG_WIDTH-1:0]                    fetch_btq_invalid_tag;

    logic                                   ras_read_valid_f3;
    logic                                   ras_write_valid_f3;
    logic                                   ras_cancel_f3;
    logic                                   ras_fixing_ptr_f3;
    logic [1:0][VADDR_WIDTH-1:0]            ret_target_banks_f3;
    ghist_info_t [1:0]                      ghist_info_banks_f3;
    ghist_info_t                            pred_ghist_info_f3;
    logic                                   fix_inner_stage_ghist_f3;
    logic                                   fix_inter_stage_ghist_f3;
    logic                                   fetch_fix_ghist, fetch_fix_ghist_ff, fix_ghist_f3_pulse;
    logic                                   fix_inner_stage_pred_f3;
    logic                                   fix_inter_stage_pred_f3;
    logic                                   deq_dual_blks_f3;
    logic                                   fix_pred_f3;
    logic                                   fix_ras_f3;
    ras_typedef::ras_meta_t[1:0]            ras_meta_banks_f3;

    logic                                   fetch_redirect, fetch_redirect_ff, fetch_redirect_pulse;
    logic [        VADDR_WIDTH-1:0]         redirect_target_f3;
    logic [BTQ_TAG_WIDTH-1:0]               redirect_btq_tag_f3;
    logic [        VADDR_WIDTH-1:0]         fix_ghist_redirect_target_f3;

    
    logic                                   has_misalign_ff;
    
    
    logic [                    15:0]        misalign_half_ff;
    

    
////////////////////////////////////ibuf///////////////////////////////////////////  
    logic [(FETCH_WIDTH*2/16+1)*BTQ_TAG_WIDTH-1:0]  fetch_ibuf_tag_flatten;
    logic [         FETCH_WIDTH*2-1:0]              fetch_ibuf_fpkt_aligned;
    logic [    (FETCH_WIDTH*2/16)-1:0]              fetch_ibuf_rvc_mask_aligned;
    logic [    (FETCH_WIDTH*2/16)-1:0]              fetch_ibuf_cfi_taken_mask_aligned;
    logic [$clog2(FETCH_WIDTH*2/16):0]              fetch_ibuf_hword_num;
    logic                                           ibuf_ready;
    logic                                           ibuf_empty;

    logic [DECODE_WIDTH-1:0][  VADDR_WIDTH-1:0]     ibuf_deq_pc;
    logic [DECODE_WIDTH-1:0][             31:0]     ibuf_deq_instr;
    logic [DECODE_WIDTH-1:0]                        ibuf_deq_is_rvc;
    logic [DECODE_WIDTH-1:0]                        ibuf_deq_pred_taken;
    logic [DECODE_WIDTH-1:0]                        ibuf_deq_valid;
    logic [DECODE_WIDTH-1:0][BTQ_TAG_WIDTH-1:0]     ibuf_deq_btq_tag;
    

    ////////////////////////////////
    logic                                   be_redirect_valid;
    logic [PC_LOB_WIDTH-1:0]                be_redirect_pc_lob;
    logic [VADDR_WIDTH-1:0]                 be_redirect_target;
    logic [BTQ_TAG_WIDTH-1:0]               be_redirect_btq_tag;


    ////////////////////////////////
    assign be_redirect_valid   = cmt_redirect_valid_i | bru_redirect_valid_i;
    assign be_redirect_pc_lob  = cmt_redirect_valid_i ? cmt_redirect_pc_lob_i : bru_redirect_pc_lob_i;
    assign be_redirect_target  = cmt_redirect_valid_i ? cmt_redirect_target_i : bru_redirect_target_i;
    assign be_redirect_btq_tag = cmt_redirect_valid_i ? cmt_redirect_btq_tag_i : bru_redirect_btq_tag_i;

    assign btq_ftq_ready = ftq_enq_ready & btq_precheck_ready;
    ////////////////////////////////

    npc_gen npc_gen_u (
        .npc_o(pc_f0),

        .f1_npc_vld_i(bpu_vld_b1),
        .f1_npc_i(next_pc_b1),

        .f2_redirect_valid_i(bpu_red_vld_b2),
        .f2_redirect_pc_i(bpu_red_target_b2),

        .f3_redirect_valid_i(fetch_redirect_pulse),
        .f3_redirect_pc_i(redirect_target_f3),

        .bru_redirect_valid_i(btq_redirect_valid),
        .bru_redirect_pc_i(btq_redirect_target),

        .cmt_redirect_valid_i(1'b0),
        .cmt_redirect_pc_i('0),

        .boot_address_i(boot_address_i),

        .stall_i(~btq_ftq_ready),

        .clk(clk),
        .rst(rst)
    );

  /////////////////////////////ghist//////////////////////////////
    always_comb begin
        btq_redirect_new_ghist_info = btq_redirect_ghist_info;
        if (btq_redirect_taken) begin
            if (btq_redirect_is_misalign_insn) begin
                if (btq_redirect_br_full_mask[0]) begin
                    btq_redirect_new_ghist_info = calc_ghist_info(btq_redirect_ghist_info, 1'b1, 1'b0);
                end
            end 
            else if (btq_redirect_br_full_mask[btq_redirect_pc_lob[PC_LOB_WIDTH-2:1]]) begin
                btq_redirect_new_ghist_info = calc_ghist_info(btq_redirect_ghist_info, 1'b1, 1'b0);
            end 
            else if (|btq_redirect_br_full_mask) begin
                btq_redirect_new_ghist_info = calc_ghist_info(btq_redirect_ghist_info, 1'b0, 1'b1);
            end
        end 
        else if (~btq_redirect_taken & (|btq_redirect_br_full_mask)) begin
            btq_redirect_new_ghist_info = calc_ghist_info(btq_redirect_ghist_info, 1'b0, 1'b1);
        end
    end

    ghist ghist_u
    (
        .ghist_info_b1_o(ghist_info_b1),
        .ghist_b1_o(ghist_b1),

        .b1_fix_ghist_i(bpu_vld_b1),
        .b1_old_valid_i(&bpp_vld_banks_b1 & (older_pred_ghist_info_b1 != pred_ghist_info_b1)),
        .ghist_info_old_b1_i(older_pred_ghist_info_b1),
        .ghist_info_new_b1_i(pred_ghist_info_b1),

        .b2_fix_ghist_i(bpu_red_vld_b2),
        .b2_old_valid_i(&bpp_vld_banks_b2 & (older_pred_ghist_info_b2 != pred_ghist_info_b2)),
        .ghist_info_old_b2_i(older_pred_ghist_info_b2),
        .ghist_info_new_b2_i(pred_ghist_info_b2),
    
        .f3_fix_ghist_i(fetch_redirect_pulse),
        .f3_old_valid_i(&fetch_valid_banks & (pd_pred_ghist_info_banks[0] != pred_ghist_info_f3)),
        .ghist_info_old_f3_i(pd_pred_ghist_info_banks[0]),
        .ghist_info_new_f3_i(pred_ghist_info_f3),

        .be_fix_ghist_i(btq_redirect_valid),
        .ghist_info_be_i(btq_redirect_new_ghist_info),

        .update_ghist_info_i(update_ghist_info),
        .update_ghist_o(update_ghist),

`ifndef SYNTHESIS
        .ori_info_be_i(btq_redirect_ghist_info),
        .dbg_be_ghist_o(),

        .ori_info_f3_i(ghist_info_banks_f3[0]),
        .dbg_f3_ghist_o(),
`endif
        .req_ready_i(btq_ftq_ready),
        
        .clk(clk),
        .rst(rst)
    );


    rvh_itlb #(
        .TRANSLATE_WIDTH(2),
        .ENTRY_COUNT(ITLB_ENTRY_COUNT),
        .TRANS_ID_WIDTH(ITLB_TRANS_ID_WIDTH),
        .PADDR_WIDTH(PADDR_WIDTH),
        .EXCP_CAUSE_WIDTH(EXCP_CAUSE_WIDTH),
        .VPN_WIDTH(VPN_WIDTH),
        .ASID_WIDTH(ASID_WIDTH)
    ) u_rvh_itlb (
        .priv_lvl_i                     (priv_lvl_i),
        .mstatus_mxr                    (mstatus_mxr),
        .mstatus_sum                    (mstatus_sum),
        .satp_mode_i                    (satp_mode_i),
        .satp_asid_i                    (satp_asid_i),
        .translate_req_vld_i            ({ic_prefetch_req_vld, ic_fetch_req_vld}),
        .translate_req_vpn_i            ({tlb_prefetch_req_vpn, tlb_fetch_req_vpn}),
        .translate_req_rdy_o            ({tlb_prefetch_req_rdy, tlb_fetch_req_rdy}),
        .translate_resp_vld_o           ({tlb_prefetch_resp_vld, tlb_fetch_resp_vld}),
        .translate_resp_ppn_o           ({tlb_prefetch_resp_ppn, tlb_fetch_resp_ppn}),
        .translate_resp_excp_vld_o      ({tlb_prefetch_resp_excp_vld, tlb_fetch_resp_excp_vld}),
        .translate_resp_excp_cause_o    ({tlb_prefetch_resp_excp_cause, tlb_fetch_resp_excp_cause}),
        .translate_resp_miss_o          ( ),
        .translate_resp_hit_o           ({tlb_prefetch_resp_hit, tlb_fetch_resp_hit}),
        .next_lvl_req_vld_o             (stlb_req_vld_o),
        .next_lvl_req_trans_id_o        (stlb_req_trans_id_o),
        .next_lvl_req_asid_o            (stlb_req_asid_o),
        .next_lvl_req_vpn_o             (stlb_req_vpn_o),
        .next_lvl_req_access_type_o     (stlb_req_access_type_o),
        .next_lvl_req_rdy_i             (stlb_req_rdy_i),
        .next_lvl_resp_vld_i            (stlb_resp_vld_i),
        .next_lvl_resp_trans_id_i       (stlb_resp_trans_id_i),
        .next_lvl_resp_asid_i           (stlb_resp_asid_i),
        .next_lvl_resp_pte_i            (stlb_resp_pte_i),
        .next_lvl_resp_page_lvl_i       (stlb_resp_pte_lvl_i),
        .next_lvl_resp_vpn_i            (stlb_resp_vpn_i),
        .next_lvl_resp_access_type_i    (stlb_resp_access_type_i),
        .next_lvl_resp_access_fault_i   (stlb_resp_access_fault_i),
        .next_lvl_resp_page_fault_i     (stlb_resp_page_fault_i),
        .tlb_evict_vld_o                (itlb_evict_vld_o),
        .tlb_evict_pte_o                (itlb_evict_pte_o),
        .tlb_evict_page_lvl_o           (itlb_evict_page_lvl_o),
        .tlb_evict_vpn_o                (itlb_evict_vpn_o),
        .tlb_evict_asid_o               (itlb_evict_asid_o),
        .tlb_flush_vld_i                (flush_itlb_req_i),
        .tlb_flush_use_asid_i           (flush_itlb_use_asid_i),
        .tlb_flush_use_vpn_i            (flush_itlb_use_vpn_i),
        .tlb_flush_vpn_i                (flush_itlb_vpn_i),
        .tlb_flush_asid_i               (flush_itlb_asid_i),
        .tlb_flush_grant_o              (flush_itlb_gnt_o),
        .clk                            (clk),
        .rstn                           (~rst)
    );

    assign ic_flush = fetch_redirect_pulse | btq_redirect_valid | (ic_fetch_resp_vld & ~ibuf_ready);
    assign tlb_prefetch_req_vpn = ic_fetch_req_pc[VADDR_WIDTH-1-:VPN_WIDTH];
    assign tlb_fetch_req_vpn = ic_fetch_req_pc[VADDR_WIDTH-1-:VPN_WIDTH];

    rvh_l1i u_rvh_l1i (
      // FETCH -> I$ : IFETCH reques
        .fetch_l1i_if_req_vld_i                 (ic_fetch_req_vld),
        .fetch_l1i_if_req_if_tag_i              (ic_fetch_req_tag),
        .fetch_l1i_if_req_index_i               (ic_fetch_req_pc[L1I_INDEX_WIDTH+L1I_OFFSET_WIDTH-1:L1I_OFFSET_WIDTH]),
        .fetch_l1i_if_req_offset_i              (ic_fetch_req_pc[L1I_OFFSET_WIDTH-1:0]),
        .fetch_l1i_if_req_vtag_i                (ic_fetch_req_pc[VADDR_WIDTH-1-:L1I_TAG_WIDTH]),
        .fetch_l1i_if_req_rdy_o                 (ic_fetch_req_rdy),

        // prefetch -> I$   
        .prefetch_l1i_if_req_vld_i              (ic_prefetch_req_vld),
        .prefetch_l1i_if_req_if_tag_i           (ic_prefetch_req_tag),
        .prefetch_l1i_if_req_index_i            (ic_prefetch_req_pc[L1I_INDEX_WIDTH+L1I_OFFSET_WIDTH-1:L1I_OFFSET_WIDTH]),
        .prefetch_l1i_if_req_offset_i           (ic_prefetch_req_pc[L1I_OFFSET_WIDTH-1:0]),
        .prefetch_l1i_if_req_vtag_i             (ic_prefetch_req_pc[VADDR_WIDTH-1-:L1I_TAG_WIDTH]), // vtag
        .prefetch_l1i_if_req_rdy_o              (ic_prefetch_req_rdy),

        // FETCH -> I$ : ITLB response  
        .fetch_l1i_if_itlb_resp_vld_i           (tlb_fetch_resp_vld),
        .fetch_l1i_if_itlb_resp_ppn_i           (tlb_fetch_resp_ppn),
        .fetch_l1i_if_itlb_resp_excp_vld_i      (tlb_fetch_resp_excp_vld),
        .fetch_l1i_if_itlb_resp_hit_i           (tlb_fetch_resp_hit),

        .prefetch_l1i_if_itlb_resp_vld_i        (tlb_prefetch_resp_vld),
        .prefetch_l1i_if_itlb_resp_ppn_i        (tlb_prefetch_resp_ppn),
        .prefetch_l1i_if_itlb_resp_excp_vld_i   (tlb_prefetch_resp_excp_vld),
        .prefetch_l1i_if_itlb_resp_hit_i        (tlb_prefetch_resp_hit),

        // I$ -> FETCH : resp in cycle 2
        .l1i_fetch_if_resp_vld_o                  (_ic_fetch_resp_vld),
        .l1i_fetch_if_resp_if_tag_o               (ic_fetch_resp_tag),
        .l1i_fetch_if_resp_data_o                 (ic_fetch_resp_cacheline),
        .l1i_fetch_if_resp_two_blks_o             (),
        .l1i_fetch_if_resp_boundary_o             (ic_fetch_resp_boundary),
        .l1i_fetch_if_resp_rvc_mask_o             (ic_fetch_resp_rvc_mask),

        // I$ -> FETCH: resp in cycle 1
        .fetch_l1i_if_hit_o(ic_fetch_probe_hit),
        .fetch_l1i_if_inflight_o(ic_fetch_probe_inflight),
        .prefetch_l1i_if_hit_o(ic_prefetch_probe_hit),

        // L1I -> L2 : Request
        .l1i_l2_req_arvalid_o                     (l1i_l2_req_arvalid_o),
        .l1i_l2_req_arready_i                     (l1i_l2_req_arready_i),
        .l1i_l2_req_ar_o                          (l1i_l2_req_ar_o),
        .l1i_l2_req_awvalid_o                     (l1i_l2_req_awvalid_o),
        .l1i_l2_req_awready_i                     (l1i_l2_req_awready_i),
        .l1i_l2_req_aw_o                          (l1i_l2_req_aw_o),
        .l1i_l2_req_wvalid_o                      (l1i_l2_req_wvalid_o),
        .l1i_l2_req_wready_i                      (l1i_l2_req_wready_i),
        .l1i_l2_req_w_o                           (l1i_l2_req_w_o),
        .l2_l1i_resp_bvalid_i                     (l2_l1i_resp_bvalid_i),
        .l2_l1i_resp_bready_o                     (l2_l1i_resp_bready_o),
        .l2_l1i_resp_b_i                          (l2_l1i_resp_b_i),
        .l2_l1i_resp_rvalid_i                     (l2_l1i_resp_rvalid_i),
        .l2_l1i_resp_rready_o                     (l2_l1i_resp_rready_o),
        .l2_l1i_resp_r_i                          (l2_l1i_resp_r_i),

        .flush_i                                  (ic_flush),

        .fencei_flush_vld_i                       (flush_icache_req_i),
        .fencei_flush_grant_o                     (flush_icache_gnt_o),

        .clk(clk),
        .rst(rst)
    );

    assign btq_ftq_two_blk_b1 = bpp_vld_banks_b1[0] & bpp_vld_banks_b1[1] & ~bpp_is_taken_banks_b1[older_ptr_b1] & ~bpp_is_truncate_banks_b1[older_ptr_b1];
    assign ftq_enq_valid = |req_valid_banks_f0 & btq_precheck_ready;
    assign ftq_enq_pc = pc_f0;
    ftq u_ftq(
    // allocate
        // .enq_valid_i(bpu_ftq_vld_b1),
        // .enq_pc_i(pc_b1),
        // .enq_req_two_blk_i(enq_two_blk_b1),
        // .enq_tag_o(ftq_tag_b1),
        // .enq_ready_o(ftq_enq_ready),
        // .enq_is_taken_i(bpu_ftq_is_taken_b1),
        // .enq_taken_offset_i(bpu_ftq_taken_offset_b1),

        .enq_valid_i(ftq_enq_valid),
        .enq_pc_i(ftq_enq_pc),
        .enq_req_two_blk_i('0),
        .enq_tag_o(ftq_enq_tag),
        .enq_ready_o(ftq_enq_ready),
        .enq_is_taken_i('0),
        .enq_taken_offset_i('0),

    // redirect by level 2 BP
        .lvl1_bp_fix_valid_i(bpu_vld_b1),
        .lvl1_bp_fix_req_two_blk_i(btq_ftq_two_blk_b1),
        .lvl1_bp_fix_tag_i(bpp_ftq_tag_banks_b1[0]),
        .lvl1_bp_fix_is_taken_i(bpu_ftq_is_taken_b1),
        .lvl1_bp_fix_is_truncate_i(bpu_ftq_is_truncate_b1),
        .lvl1_bp_fix_taken_truncate_offset_i(bpu_ftq_taken_truncate_offset_b1),

        .lvl2_bp_valid_i(bpu_vld_b2),
        .lvl2_bp_fix_valid_i(fix_ftq_b2),
        .lvl2_bp_fix_req_two_blk_i(fix_req_two_blks_b2),
        .lvl2_bp_fix_tag_i(bpp_ftq_tag_banks_b2[0]),
        .lvl2_bp_fix_is_taken_i(is_taken_b2),
        .lvl2_bp_fix_is_truncate_i(is_truncate_b2),
        .lvl2_bp_fix_taken_truncate_offset_i(pred_taken_truncate_offset_b2),

        .btq_alloc_valid_i(|btq_alloc_valid_banks & btq_alloc_ready),
        .btq_alloc_ftq_tag_i(bpp_ftq_tag_banks_b2[0]),
        .btq_alloc_btq_tag_i(btq_alloc_tag_banks[0]),

    // prefetch
        .prefetch_req_valid_o(ic_prefetch_req_vld),
        .prefetch_req_pc_o(ic_prefetch_req_pc),
        .prefetch_req_tag_o(ic_prefetch_req_tag),
        .prefetch_req_ready_i(ic_prefetch_req_rdy),
        .prefetch_req_tlb_hit_i(tlb_prefetch_resp_hit),
        .prefetch_req_tlb_ready_i(tlb_prefetch_req_rdy),
        .prefetch_req_in_ic_i(1'b0),
        .prefetch_req_in_pfb_i(1'b0),

    // fetch
        .fetch_req_ready_i(ic_fetch_req_rdy),
        .fetch_req_valid_o(ic_fetch_req_vld),
        .fetch_req_pc_o(ic_fetch_req_pc),
        .fetch_req_tag_o(ic_fetch_req_tag),
        .fetch_req_tlb_hit_i(tlb_fetch_resp_hit),
        .fetch_req_tlb_excp_vld_i(tlb_fetch_resp_excp_vld),
        .fetch_req_tlb_excp_cause_i(tlb_fetch_resp_excp_cause),
        .fetch_req_tlb_ready_i(tlb_fetch_req_rdy),
        .fetch_req_ic_probe_hit_i(ic_fetch_probe_hit),
        
    // fetch resp
        .fetch_resp_valid_i(_ic_fetch_resp_vld),
        .fetch_resp_replay_i(~ibuf_ready),
        .fetch_resp_tag_i(ic_fetch_resp_tag),
        .fetch_resp_valid_o(ic_fetch_resp_vld),
        .fetch_resp_excp_valid_o(ic_fetch_resp_excp_valid),
        .fetch_resp_excp_tval_o(ic_fetch_resp_excp_tval),
        .fetch_resp_excp_cause_o(ic_fetch_resp_excp_cause),
        .fetch_resp_is_pred_taken_o(ic_fetch_resp_is_pred_taken),
        .fetch_resp_is_pred_truncate_o(ic_fetch_resp_is_pred_truncate),
        .fetch_resp_taken_truncate_offset_o(ic_fetch_resp_taken_truncate_offset),
        .fetch_resp_two_blks_o(ic_fetch_resp_two_blks),
        .fetch_resp_pc_o(ic_fetch_resp_pc),
        .fetch_resp_next_valid_o(ic_fetch_resp_next_valid),
        .fetch_resp_next_pc_o(ic_fetch_resp_next_pc),
        .fetch_resp_btq_tag_o(ic_fetch_resp_btq_tag),
    // backend redirect
        .fe_flush_i(fetch_redirect_pulse),
        .be_flush_i(btq_redirect_valid),

        .clk(clk),
        .rst(rst)
    );

    // assign fetch_valid_banks[0] = ic_fetch_resp_vld;
    // assign fetch_valid_banks[1] = ic_fetch_resp_two_blks;
    // assign fetch_pc_banks[0] = ic_fetch_resp_pc;
    // assign fetch_pc_banks[1] = {ic_fetch_resp_pc[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
    // assign fetch_taken_banks[0] = ~ic_fetch_resp_two_blks & ic_fetch_resp_is_pred_taken;
    // assign fetch_taken_banks[1] = ic_fetch_resp_two_blks & ic_fetch_resp_is_pred_taken;
    // assign fetch_fpkt_banks[0] = ic_fetch_resp_cacheline[FETCH_WIDTH-1:0];
    // assign fetch_fpkt_banks[1] = ic_fetch_resp_cacheline[L1I_FETCH_WIDTH-1:FETCH_WIDTH];
    // assign fetch_fpkt_boundary_banks[0] = ic_fetch_resp_boundary[FETCH_WIDTH/16*2-1:0];
    // assign fetch_fpkt_boundary_banks[1] = ic_fetch_resp_boundary[L1I_FETCH_WIDTH/16*2-1:FETCH_WIDTH/16*2];
    // assign fetch_fpkt_rvc_mask_banks[0] = ic_fetch_resp_rvc_mask[FETCH_WIDTH/16*2-1:0];
    // assign fetch_fpkt_rvc_mask_banks[1] = ic_fetch_resp_rvc_mask[L1I_FETCH_WIDTH/16*2-1:FETCH_WIDTH/16*2];
    // assign fetch_taken_truncate_offset_banks[0] = ic_fetch_resp_taken_truncate_offset;
    // assign fetch_taken_truncate_offset_banks[1] = ic_fetch_resp_taken_truncate_offset;
    // assign fetch_next_valid_banks[0] = ic_fetch_resp_two_blks ? 1'b1 : ic_fetch_resp_next_valid;
    // assign fetch_next_valid_banks[1] = ic_fetch_resp_two_blks ? ic_fetch_resp_next_valid : 1'b0;
    // assign fetch_next_pc_banks[0] = ic_fetch_resp_two_blks ? fetch_pc_banks[1] : ic_fetch_resp_next_pc;
    // assign fetch_next_pc_banks[1] = ic_fetch_resp_next_pc;
    // assign fetch_btq_tag_banks[0] = ic_fetch_resp_btq_tag;
    // assign fetch_btq_tag_banks[1] = ic_fetch_resp_btq_tag + 1'b1;

    assign fetch_valid_banks[0] = ic_fetch_resp_vld;
    assign fetch_valid_banks[1] = 0;
    assign fetch_pc_banks[0] = ic_fetch_resp_pc;
    assign fetch_pc_banks[1] = 0;
    assign fetch_taken_banks[0] = ic_fetch_resp_is_pred_taken;
    assign fetch_taken_banks[1] = 0;
    assign fetch_fpkt_banks[0] = ic_fetch_resp_cacheline[FETCH_WIDTH-1:0];
    assign fetch_fpkt_banks[1] = 0;
    assign fetch_fpkt_boundary_banks[0] = ic_fetch_resp_boundary[FETCH_WIDTH/16*2-1:0];
    assign fetch_fpkt_boundary_banks[1] = 0;
    assign fetch_fpkt_rvc_mask_banks[0] = ic_fetch_resp_rvc_mask[FETCH_WIDTH/16*2-1:0];
    assign fetch_fpkt_rvc_mask_banks[1] = 0;
    assign fetch_taken_truncate_offset_banks[0] = ic_fetch_resp_taken_truncate_offset;
    assign fetch_taken_truncate_offset_banks[1] = 0;
    assign fetch_next_valid_banks[0] = ic_fetch_resp_next_valid;
    assign fetch_next_valid_banks[1] = 0;
    assign fetch_next_pc_banks[0] = ic_fetch_resp_next_pc;
    assign fetch_next_pc_banks[1] = 0;
    assign fetch_btq_tag_banks[0] = ic_fetch_resp_btq_tag;
    assign fetch_btq_tag_banks[1] = 0;



  /////////////////////////////bp pipe//////////////////////////////
    // assign next_pkt_pc_f0 = {pc_f0[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
    // assign next_pkt_valid_f0 = ~(&pc_f0[$clog2(L1I_CACHELINE_WIDTH/8)-1:$clog2(FETCH_WIDTH/8)]);
    // assign pc_banks_f0[0] = pc_f0[$clog2(FETCH_WIDTH/8)] ? next_pkt_pc_f0 : pc_f0;
    // assign pc_banks_f0[1] = pc_f0[$clog2(FETCH_WIDTH/8)] ? pc_f0 : next_pkt_pc_f0;
    // assign req_valid_banks_f0[0] = pc_f0[$clog2(FETCH_WIDTH/8)] ? next_pkt_valid_f0 : 1'b1;
    // assign req_valid_banks_f0[1] = pc_f0[$clog2(FETCH_WIDTH/8)] ? 1'b1 : next_pkt_valid_f0;
    // assign is_older_banks_f0[0] = pc_f0[$clog2(FETCH_WIDTH/8)] == 1'b0;
    // assign is_older_banks_f0[1] = pc_f0[$clog2(FETCH_WIDTH/8)] == 1'b1;
    // assign bpp_other_vld_banks_b1[0] = bpp_vld_banks_b1[1];
    // assign bpp_other_vld_banks_b1[1] = bpp_vld_banks_b1[0];
    // assign bpp_other_vld_banks_b2[0] = bpp_vld_banks_b2[1];
    // assign bpp_other_vld_banks_b2[1] = bpp_vld_banks_b2[0];
    // assign update_valid_banks[0] = update_valid & (update_req_pc[$clog2(FETCH_WIDTH/8)] == 1'b0);
    // assign update_valid_banks[1] = update_valid & (update_req_pc[$clog2(FETCH_WIDTH/8)] == 1'b1);

    assign next_pkt_pc_f0 = 0;
    assign next_pkt_valid_f0 = 0;
    assign pc_banks_f0[0] = pc_f0;
    assign pc_banks_f0[1] = 0;
    assign req_valid_banks_f0[0] = 1;
    assign req_valid_banks_f0[1] = 0;
    assign is_older_banks_f0[0] = 1;
    assign is_older_banks_f0[1] = 0;
    assign bpp_other_vld_banks_b1[0] = 0;
    assign bpp_other_vld_banks_b1[1] = bpp_vld_banks_b1[0];
    assign bpp_other_vld_banks_b2[0] = 0;
    assign bpp_other_vld_banks_b2[1] = bpp_vld_banks_b2[0];
    assign update_valid_banks[0] = update_valid;
    assign update_valid_banks[1] = 0;


    generate 
        for (genvar bank = 0; bank < 2; bank++) begin : gen_old_bp_banks
            bp_pipe bp_pipe_u (
                .req_valid_b0_i(req_valid_banks_f0[bank] & btq_ftq_ready),
                .req_is_older_bank_i(is_older_banks_f0[bank]),
                .req_ftq_tag_i(ftq_enq_tag),
                .req_pc_b0_i(pc_banks_f0[bank]),
                .req_ready_o(/*bpp_req_rdy_banks[bank]*/),

                // bp pipe <> ftq
                // stage1
                .vld_b1_o(bpp_vld_banks_b1[bank]),
                .other_bpp_vld_b1_i(bpp_other_vld_banks_b1[bank]),
                .is_older_bank_b1_o(is_older_banks_b1[bank]),
                .ftq_tag_b1_o(bpp_ftq_tag_banks_b1[bank]),
                .pc_b1_o(pc_banks_b1[bank]),
                .next_pc_b1_o(next_pc_banks_b1[bank]),
                .is_taken_b1_o(bpp_is_taken_banks_b1[bank]),
                .is_truncate_b1_o(bpp_is_truncate_banks_b1[bank]),
                .offset_b1_o(bpp_offset_banks_b1[bank]),
                .fix_ghist_b1_o(),
                .saw_taken_br_b1_o(saw_taken_br_banks_b1[bank]),
                .saw_not_taken_br_b1_o(saw_not_taken_br_banks_b1[bank]),
                .ras_pred_b1_o(ras_pred_banks_b1[bank]),
                .ras_pred_b1_ff_o(ras_pred_ff_banks_b1[bank]),
                .ras_meta_b1_i(ras_meta_b1),
                .ras_target_b1_i(ras_target_b1),
                .invalidate_b1_i(invalidate_banks_b1[bank] /*is_older_banks_b1[bank] ? 1'b0 : bpp_is_taken_banks_b1[older_ptr_b1]*/),
                .new_ghist_info_b1_i(is_older_banks_b1[bank] ? ghist_info_b1 : older_pred_ghist_info_b1),
                .ghist_b1_i(ghist_b1),
                .ready_b1_i(1'b1),

                // stage2
                .older_bank_saw_not_taken_br_b2_i(saw_not_taken_br_banks_b2[older_ptr_b2]),
                .vld_b2_o(bpp_vld_banks_b2[bank]),
                .other_bpp_vld_b2_i(bpp_other_vld_banks_b2[bank]),
                .is_older_bank_b2_o(is_older_banks_b2[bank]),
                .pc_b2_o(pc_banks_b2[bank]),
                .next_pc_b2_o(bpp_red_target_banks_b2[bank]),
                .ftq_tag_b2_o(bpp_ftq_tag_banks_b2[bank]),
                .is_taken_b2_o(bpp_is_taken_banks_b2[bank]),
                .is_truncate_b2_o(bpp_is_truncate_banks_b2[bank]),
                .offset_b2_o(bpp_offset_banks_b2[bank]),
                .saw_taken_br_b2_o(saw_taken_br_banks_b2[bank]),
                .saw_not_taken_br_b2_o(saw_not_taken_br_banks_b2[bank]),
                .ras_meta_b2_o(ras_meta_banks_b2[bank]),
                .ghist_info_b2_o(ghist_info_banks_b2[bank]),
                .ras_pred_b2_o(ras_pred_banks_b2[bank]),
                .ras_pred_b2_ff_o(ras_pred_ff_banks_b2[bank]),
                .invalidate_b2_i(invalidate_banks_b2[bank]),
                .new_ghist_info_b2_i(is_older_banks_b2[bank] ? ghist_info_b2 : older_pred_ghist_info_b2),
                .ubtb_meta_b2_o(ubtb_meta_banks_b2[bank]),
                .bht_meta_b2_o(bht_meta_banks_b2[bank]),
                .composed_tage_meta_b2_o(composed_tage_meta_banks_b2[bank]),
                .btb_meta_b2_o(btb_meta_banks_b2[bank]),
                .loop_meta_b2_o(loop_meta_banks_b2[bank]),

                // update
                .update_valid_i(update_valid_banks[bank]),
                .update_pred_valid_i(update_pred_valid),
                .update_req_pc_i(update_req_pc),
                .update_target_i(update_target),
                .update_truncate_i(update_truncate),
                .update_cfi_taken_vec_i(update_cfi_taken_vec),
                .update_cfi_offset_vec_i(update_cfi_offset_vec),
                .update_cfi_mask_i(update_cfi_mask),
                .update_rvc_mask_i(update_rvc_mask),
                .update_call_mask_i(update_call_mask),
                .update_ret_mask_i(update_ret_mask),
                .update_br_mask_i(update_br_mask),
                .update_composed_tage_meta_i(update_composed_tage_meta),
                .update_btb_meta_i(update_btb_meta),
                .update_ubtb_meta_i(update_ubtb_meta),
                .update_bht_meta_i(update_bht_meta),
                .update_loop_meta_i(update_loop_meta),
                .update_ghist_i(update_ghist),
                .update_is_misalign_i(update_is_misalign),
                .redirect_b2_i(bpu_red_vld_b2),
                .ready_b2_i(1'b1),

                .fetch_redirect_i(fetch_redirect_pulse),
                .fetch_redirect_taken_i(pd_cfi_taken),
                .be_redirect_i(btq_redirect_valid),
                .be_redirect_taken_i(btq_redirect_taken),

                .clk(clk),
                .rst(rst)
            );
        end
    endgenerate
////////////////////////////////////////////////////////////////////////////////////////////
    assign ras_read_valid_f3 = pd_ret_taken_banks[pd_pred_fix_ptr] & fetch_valid_banks[pd_pred_fix_ptr] & ibuf_ready;
    assign ras_write_valid_f3 = pd_call_taken_banks[pd_pred_fix_ptr] & fetch_valid_banks[pd_pred_fix_ptr] & ibuf_ready;
    assign ras_cancel_f3 = pd_pred_fix_ptr & (fix_inner_stage_pred_f3 | fix_inner_stage_ghist_f3);
    ras ras_u (
        // b1 stage
        .pc_b1_i(pc_banks_b1[taken_truncate_ptr_b1]),
        .ret_target_b1_o(ras_target_b1),
        .ras_pred_b1_i(ras_pred_banks_b1[taken_truncate_ptr_b1]),
        .ras_meta_b1_o(ras_meta_b1),

        // b2 stage
        // fix b1's prediction in ras
        .fix_ras_valid_b2_i(bpu_red_vld_b2 | fix_ras_b2),
        .pc_b2_i(pc_banks_b2[ras_fixing_ptr_b2]),
        .ras_pred_b2_i(ras_pred_banks_b2[ras_fixing_ptr_b2]),
        .ras_meta_b2_i(ras_meta_banks_b2[0]),

        // f3 stage
        // read ret's target from ras according to meta data
        .ras_meta_banks_f3_i(ras_meta_banks_f3),
        .ret_target_banks_f3_o(ret_target_banks_f3),
        // fix former prediction in ras
        .fix_ras_valid_f3_i(fetch_redirect_pulse | fix_ras_f3),
        .fix_ptr_f3_i(ras_fixing_ptr_f3),
        .pc_f3_i(fetch_pc_banks[ras_fixing_ptr_f3]),
        .ras_pred_f3_i(pd_ras_pred_banks[ras_fixing_ptr_f3]),

        //fix from be
        .redirect_valid_i(btq_redirect_valid),
        .redirect_insn_pc_lob_i(btq_redirect_is_misalign_insn ? '0 : btq_redirect_pc_lob),
        .redirect_ras_pred_i(btq_redirect_ras_pred),
        .redirect_ras_meta_i(btq_redirect_ras_meta),

        .req_ready_i(btq_ftq_ready),

        .clk(clk),
        .rst(rst)
    );

//////////////////////// sel a bank in b1 stage /////////////////////////////////////////
    assign older_ptr_b1 = (bpp_vld_banks_b1[0] & is_older_banks_b1[0]) ? 1'b0 : 1'b1;
    assign newer_ptr_b1 = bpp_vld_banks_b1[~older_ptr_b1] ? ~older_ptr_b1 : older_ptr_b1;

    always_comb begin
        taken_truncate_ptr_b1 = newer_ptr_b1;
        if (bpp_is_taken_banks_b1[older_ptr_b1]) begin
            taken_truncate_ptr_b1 = older_ptr_b1;
        end
        else if (bpp_is_truncate_banks_b1[older_ptr_b1]) begin
            taken_truncate_ptr_b1 = older_ptr_b1;
        end
    end

    assign bpu_vld_b1 = |bpp_vld_banks_b1;
    assign pc_b1 = pc_banks_b1[older_ptr_b1];
    assign next_pc_b1 = next_pc_banks_b1[taken_truncate_ptr_b1];

    assign bpu_ftq_is_taken_b1 = btq_ftq_two_blk_b1 ? |bpp_is_taken_banks_b1 : bpp_is_taken_banks_b1[older_ptr_b1];
    assign bpu_ftq_is_truncate_b1 = btq_ftq_two_blk_b1 ? |bpp_is_truncate_banks_b1 : bpp_is_truncate_banks_b1[older_ptr_b1];
    assign bpu_ftq_taken_truncate_offset_b1 = bpp_offset_banks_b1[taken_truncate_ptr_b1];

    assign older_pred_ghist_info_b1 = calc_ghist_info(ghist_info_b1, saw_taken_br_banks_b1[older_ptr_b1], saw_not_taken_br_banks_b1[older_ptr_b1]);

    always_comb begin
        invalidate_banks_b1 = 0;
        invalidate_banks_b1[newer_ptr_b1] = bpp_is_taken_banks_b1[older_ptr_b1] | bpp_is_truncate_banks_b1[older_ptr_b1];
        invalidate_banks_b1[older_ptr_b1] = 1'b0;
    end

    always_comb begin
        pred_ghist_info_b1 = ghist_info_b1;
        if (bpp_vld_banks_b1[0] & bpp_vld_banks_b1[1] & ((~bpu_ftq_is_taken_b1 & ~bpu_ftq_is_truncate_b1) | (taken_truncate_ptr_b1 == newer_ptr_b1))) begin
            if (saw_taken_br_banks_b1[older_ptr_b1]) begin
                pred_ghist_info_b1 = older_pred_ghist_info_b1;
            end
            else begin
                pred_ghist_info_b1 = calc_ghist_info(older_pred_ghist_info_b1, saw_taken_br_banks_b1[newer_ptr_b1], saw_not_taken_br_banks_b1[newer_ptr_b1]);
            end
        end
        else if (bpp_vld_banks_b1[0] & bpp_vld_banks_b1[1]) begin
            pred_ghist_info_b1 = older_pred_ghist_info_b1;
        end
        else if (bpp_vld_banks_b1[0]) begin
            pred_ghist_info_b1 = calc_ghist_info(ghist_info_b1, saw_taken_br_banks_b1[0], saw_not_taken_br_banks_b1[0]);
        end
        else if (bpp_vld_banks_b1[1]) begin
            pred_ghist_info_b1 = calc_ghist_info(ghist_info_b1, saw_taken_br_banks_b1[1], saw_not_taken_br_banks_b1[1]);
        end
    end

//////////////////////// sel a bank in b2 stage /////////////////////////////////////////
    assign older_ptr_b2 = (bpp_vld_banks_b2[0] & is_older_banks_b2[0]) ? 1'b0 : 1'b1;
    assign newer_ptr_b2 = bpp_vld_banks_b2[~older_ptr_b2] ? ~older_ptr_b2 : older_ptr_b2;
    assign fix_inner_stage_pred_b2 = bpp_vld_banks_b2[0] & bpp_vld_banks_b2[1] & (bpp_is_taken_banks_b2[older_ptr_b2] | bpp_is_truncate_banks_b2[older_ptr_b2]);
    assign fix_inter_stage_pred_b2 = bpp_vld_banks_b2[newer_ptr_b2] & (~bpu_vld_b1 | (bpp_red_target_banks_b2[newer_ptr_b2] != pc_b1));
    assign fix_inner_stage_ghist_b2 = bpp_vld_banks_b2[0] & bpp_vld_banks_b2[1] & (ghist_info_banks_b2[newer_ptr_b2] != older_pred_ghist_info_b2);
    assign fix_inter_stage_ghist_b2 = bpp_vld_banks_b2[newer_ptr_b2] & (~bpu_vld_b1 | (newer_pred_ghist_info_b2 != ghist_info_b1));
    
    always_comb begin
        if (bpp_vld_banks_b2[0] & bpp_vld_banks_b2[1]) begin
            if (is_taken_b2 & (taken_truncate_ptr_b2 == older_ptr_b2)) begin
                fix_ras_b2 = 1'b1;
                ras_fixing_ptr_b2 = older_ptr_b2;
            end
            else begin
                if (fix_inner_stage_pred_b2 | fix_inner_stage_ghist_b2) begin
                    ras_fixing_ptr_b2 = older_ptr_b2;
                    fix_ras_b2 = ras_typedef::ras_pred_incompatible(ras_pred_banks_b2[older_ptr_b2], ras_pred_ff_banks_b1[older_ptr_b2]);
                end
                else begin
                    fix_ras_b2 = ras_typedef::ras_pred_incompatible(ras_pred_banks_b2[newer_ptr_b2], ras_pred_ff_banks_b1[newer_ptr_b2]);
                    ras_fixing_ptr_b2 = newer_ptr_b2;
                end
            end
        end
        else if (|bpp_vld_banks_b2) begin
            fix_ras_b2 = ras_typedef::ras_pred_incompatible(ras_pred_banks_b2[newer_ptr_b2], ras_pred_ff_banks_b1[newer_ptr_b2]);
            ras_fixing_ptr_b2 = newer_ptr_b2;
        end
        else begin
            fix_ras_b2 = 1'b0;
            ras_fixing_ptr_b2 = 0;
        end
    end

    always_comb begin
        taken_truncate_ptr_b2 = newer_ptr_b2;
        if (bpp_is_taken_banks_b2[older_ptr_b2]) begin
            taken_truncate_ptr_b2 = older_ptr_b2;
        end
        else if (bpp_is_truncate_banks_b2[older_ptr_b2]) begin
            taken_truncate_ptr_b2 = older_ptr_b2;
        end
    end

    assign is_taken_b2 = fix_req_two_blks_b2 ? |bpp_is_taken_banks_b2 : bpp_is_taken_banks_b2[older_ptr_b2];
    assign is_truncate_b2 = fix_req_two_blks_b2 ? |bpp_is_truncate_banks_b2 : bpp_is_truncate_banks_b2[older_ptr_b2];
    assign pred_taken_truncate_offset_b2 = bpp_offset_banks_b2[taken_truncate_ptr_b2];
    always_comb begin
        invalidate_banks_b2 = 0;
        invalidate_banks_b2[newer_ptr_b2] = fix_inner_stage_pred_b2 | fix_inner_stage_ghist_b2;
        invalidate_banks_b2[older_ptr_b2] = 1'b0;
    end
    assign older_pred_ghist_info_b2 = calc_ghist_info(ghist_info_b2, saw_taken_br_banks_b2[older_ptr_b2], saw_not_taken_br_banks_b2[older_ptr_b2]);

    always_comb begin
        newer_pred_ghist_info_b2 = ghist_info_b2;
        if (bpp_vld_banks_b2[0] & bpp_vld_banks_b2[1] & ((~is_taken_b2 & ~is_truncate_b2) | (taken_truncate_ptr_b2 == newer_ptr_b2))) begin
            if (saw_taken_br_banks_b2[older_ptr_b2]) begin
                newer_pred_ghist_info_b2 = older_pred_ghist_info_b2;
            end
            else begin
                newer_pred_ghist_info_b2 = calc_ghist_info(older_pred_ghist_info_b2, saw_taken_br_banks_b2[newer_ptr_b2], saw_not_taken_br_banks_b2[newer_ptr_b2]);
            end
        end
        else if (bpp_vld_banks_b2[0] & bpp_vld_banks_b2[1]) begin
            newer_pred_ghist_info_b2 = older_pred_ghist_info_b2;
        end
        else if (bpp_vld_banks_b2[0]) begin
            newer_pred_ghist_info_b2 = calc_ghist_info(ghist_info_b2, saw_taken_br_banks_b2[0], saw_not_taken_br_banks_b2[0]);
        end
        else if (bpp_vld_banks_b2[1]) begin
            newer_pred_ghist_info_b2 = calc_ghist_info(ghist_info_b2, saw_taken_br_banks_b2[1], saw_not_taken_br_banks_b2[1]);
        end
    end

    always_comb begin
        if (fix_inner_stage_pred_b2) begin
            pred_ghist_info_b2 = older_pred_ghist_info_b2;
        end
        else if (fix_inner_stage_ghist_b2) begin
            pred_ghist_info_b2 = older_pred_ghist_info_b2;
        end
        else if (fix_inter_stage_pred_b2) begin
            pred_ghist_info_b2 = newer_ptr_b2 == older_ptr_b2 ? older_pred_ghist_info_b2 : newer_pred_ghist_info_b2;
        end
        else begin 
            pred_ghist_info_b2 = newer_ptr_b2 == older_ptr_b2 ? older_pred_ghist_info_b2 : newer_pred_ghist_info_b2;
        end
    end

    always_comb begin
        if (fix_inner_stage_pred_b2) begin
            bpu_red_target_b2 = bpp_red_target_banks_b2[older_ptr_b2];
        end
        else if (fix_inner_stage_ghist_b2) begin
            bpu_red_target_b2 = bpp_red_target_banks_b2[older_ptr_b2];
        end
        else if (fix_inter_stage_pred_b2) begin
            bpu_red_target_b2 = bpp_red_target_banks_b2[newer_ptr_b2];
        end
        else begin 
            bpu_red_target_b2 = bpp_red_target_banks_b2[newer_ptr_b2];
        end
    end

    assign fix_pred_b2 = fix_inner_stage_pred_b2 | fix_inter_stage_pred_b2;
    assign fix_ghist_pred_b2 = fix_inner_stage_ghist_b2 | fix_inter_stage_ghist_b2;

    assign bpu_vld_b2 = |bpp_vld_banks_b2;
    assign ghist_info_b2 = ghist_info_banks_b2[older_ptr_b2];
    assign pc_b2 = pc_banks_b2[older_ptr_b2];
    assign bpu_red_vld_b2 = bpu_vld_b2 & (fix_pred_b2 | fix_ghist_pred_b2 | fix_ras_b2);
    assign fix_ftq_b2 = bpu_red_vld_b2;
    assign fix_req_two_blks_b2 = &bpp_vld_banks_b2 & ~invalidate_banks_b2[newer_ptr_b2];

//////////////////////////////pre-decode////////////////////////////////////////
    //asseration
`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (fetch_taken_banks[0] & fetch_valid_banks[0] & fetch_valid_banks[1]) begin
            $display("The 2nd block is valid when the first block is taken!");
            $finish();
        end
    end
`endif
    logic [1:0]         fpkt_misalign_valid_banks_f3;
    logic [1:0][15:0]   fpkt_misalign_half_f3;
    logic               sel_predec_f3;
    assign fpkt_misalign_valid_banks_f3[0] = 1'b0;
    assign fpkt_misalign_valid_banks_f3[1] = 1'b1;
    assign fpkt_misalign_half_f3[0] = 'b0;
    assign fpkt_misalign_half_f3[1] = fetch_fpkt_banks[0][FETCH_WIDTH-1-:16];
    assign ghist_info_banks_f3[0] = btq_fetch_ghist_info_banks[0];
    assign ghist_info_banks_f3[1] = btq_fetch_ghist_info_banks[1];

    pre_decoder pre_decoder_u(
        .fpkt_vld_i(fetch_valid_banks[0]),
        .fpkt_pc_i(fetch_pc_banks[0]),
        .fpkt_tail_offset_i(fetch_taken_truncate_offset_banks[0]),
        .fpkt_ghist_info_i(ghist_info_banks_f3[0]),
        .fpkt_is_taken_i(fetch_taken_banks[0]),
        .fpkt_cacheline_i(fetch_fpkt_banks[0]),
        .fpkt_boundary_i(fetch_fpkt_boundary_banks[0]),
        .fpkt_rvc_mask_i(fetch_fpkt_rvc_mask_banks[0]),
        .fpkt_next_pc_vld_i(fetch_next_valid_banks[0]),
        .fpkt_next_pc_i(fetch_next_pc_banks[0]),

        .fpkt_misalign_valid_i(has_misalign_ff),
        .fpkt_misalign_inst_i(misalign_half_ff),

        .ras_target_i(ras_meta_banks_f3[0].top_member),

        // instruction fetch queue
        .pdec_taken_o(pd_cfi_taken_banks[0]),
        .pdec_truncate_o(pd_truncate_banks[0]),
        .pdec_call_taken_o(pd_call_taken_banks[0]),
        .pdec_ret_taken_o(pd_ret_taken_banks[0]),
        .pdec_tail_insn_pc_lob_o(pd_fpkt_tail_pc_lob_banks[0]),
        .pdec_addr_next_to_cfi_o(pd_addr_next_to_cfi_banks[0]),
        .pdec_pred_ghist_info_o(pd_pred_ghist_info_banks[0]),
        .pdec_rvc_mask_o(pd_inst_rvc_mask_banks[0]),
        .pdec_taken_mask_o(pd_cfi_taken_mask_banks[0]),
        .pdec_jalr_mask_o(pd_jalr_mask_banks[0]),
        .pdec_jal_mask_o(pd_jal_mask_banks[0]),
        .pdec_ret_mask_o(pd_return_mask_banks[0]),
        .pdec_call_mask_o(pd_call_mask_banks[0]),
        .pdec_br_mask_o(pd_br_mask_banks[0]),
        .pdec_target_o(pd_cfi_target_banks[0]),
        .pdec_misalign_valid_o(pd_has_misalign_banks[0]),
        .pdec_misalign_inst_o(pd_misalign_half_banks[0]),
        .pdec_hword_num_o(pd_hword_num_banks[0]),
        .pdec_ras_pred_o(pd_ras_pred_banks[0]),
        .pdec_cfi_valid_vec_o(pd_cfi_valid_vec_banks[0]),
        .pdec_cfi_taken_vec_o(pd_cfi_taken_vec_banks[0]),
        .pdec_cfi_offset_vec_o(pd_cfi_offset_vec_banks[0]),

        .clk(clk),
        .rst(rst)
    );
    generate 
        for (genvar bank = 0; bank < 2; bank++) begin : gen_predcoder
            pre_decoder pre_decoder_u(
                .fpkt_vld_i(fetch_valid_banks[1]),
                .fpkt_pc_i(fetch_pc_banks[1]),
                .fpkt_tail_offset_i(fetch_taken_truncate_offset_banks[1]),
                .fpkt_ghist_info_i(ghist_info_banks_f3[1]),
                .fpkt_is_taken_i(fetch_taken_banks[1]),
                .fpkt_cacheline_i(fetch_fpkt_banks[1]),
                .fpkt_boundary_i(fetch_fpkt_boundary_banks[1]),
                .fpkt_rvc_mask_i(fetch_fpkt_rvc_mask_banks[1]),
                .fpkt_next_pc_vld_i(fetch_next_valid_banks[1]),
                .fpkt_next_pc_i(fetch_next_pc_banks[1]),

                .fpkt_misalign_valid_i(fpkt_misalign_valid_banks_f3[bank]),
                .fpkt_misalign_inst_i(fpkt_misalign_half_f3[bank]),

                .ras_target_i(ras_meta_banks_f3[1].top_member),

                // instruction fetch queue
                .pdec_taken_o(pd_cfi_taken_for_sel[bank]),
                .pdec_truncate_o(pd_truncate_for_sel[bank]),
                .pdec_call_taken_o(pd_call_taken_for_sel[bank]),
                .pdec_ret_taken_o(pd_ret_taken_for_sel[bank]),
                .pdec_tail_insn_pc_lob_o(pd_cacheline_tail_pc_lob_for_sel[bank]),
                .pdec_addr_next_to_cfi_o(pd_addr_next_to_cfi_for_sel[bank]),
                .pdec_pred_ghist_info_o(pd_pred_ghist_info_for_sel[bank]),
                .pdec_rvc_mask_o(pd_inst_rvc_mask_for_sel[bank]),
                .pdec_taken_mask_o(pd_cfi_taken_mask_for_sel[bank]),
                .pdec_jalr_mask_o(pd_jalr_mask_for_sel[bank]),
                .pdec_jal_mask_o(pd_jal_mask_for_sel[bank]),
                .pdec_ret_mask_o(pd_return_mask_for_sel[bank]),
                .pdec_call_mask_o(pd_call_mask_for_sel[bank]),
                .pdec_br_mask_o(pd_br_mask_for_sel[bank]),
                .pdec_target_o(pd_cfi_target_banks_for_sel[bank]),
                .pdec_misalign_valid_o(pd_has_misalign_for_sel[bank]),
                .pdec_misalign_inst_o(pd_misalign_half_for_sel[bank]),
                .pdec_hword_num_o(pd_hword_num_for_sel[bank]),
                .pdec_ras_pred_o(pd_ras_pred_banks_for_sel[bank]),
                .pdec_cfi_valid_vec_o(pd_cfi_valid_vec_banks_for_sel[bank]),
                .pdec_cfi_taken_vec_o(pd_cfi_taken_vec_banks_for_sel[bank]),
                .pdec_cfi_offset_vec_o(pd_cfi_offset_vec_banks_for_sel[bank]),

                .clk(clk),
                .rst(rst)
            );
        end
    endgenerate

    assign sel_predec_f3 = pd_has_misalign_banks[0];
    assign pd_cfi_taken_banks[1] = pd_cfi_taken_for_sel[sel_predec_f3];
    assign pd_truncate_banks[1] = pd_truncate_for_sel[sel_predec_f3];
    assign pd_call_taken_banks[1] = pd_call_taken_for_sel[sel_predec_f3];
    assign pd_ret_taken_banks[1] = pd_ret_taken_for_sel[sel_predec_f3];
    assign pd_fpkt_tail_pc_lob_banks[1] = pd_cacheline_tail_pc_lob_for_sel[sel_predec_f3];
    assign pd_addr_next_to_cfi_banks[1] = pd_addr_next_to_cfi_for_sel[sel_predec_f3];
    assign pd_pred_ghist_info_banks[1] = pd_pred_ghist_info_for_sel[sel_predec_f3];
    assign pd_inst_rvc_mask_banks[1] = pd_inst_rvc_mask_for_sel[sel_predec_f3];
    assign pd_cfi_taken_mask_banks[1] = pd_cfi_taken_mask_for_sel[sel_predec_f3];
    assign pd_jalr_mask_banks[1] = pd_jalr_mask_for_sel[sel_predec_f3];
    assign pd_jal_mask_banks[1] = pd_jal_mask_for_sel[sel_predec_f3];
    assign pd_return_mask_banks[1] = pd_return_mask_for_sel[sel_predec_f3];
    assign pd_call_mask_banks[1] = pd_call_mask_for_sel[sel_predec_f3];
    assign pd_br_mask_banks[1] = pd_br_mask_for_sel[sel_predec_f3];
    assign pd_cfi_target_banks[1] = pd_cfi_target_banks_for_sel[sel_predec_f3];
    assign pd_has_misalign_banks[1] = pd_has_misalign_for_sel[sel_predec_f3];
    assign pd_misalign_half_banks[1] = pd_misalign_half_for_sel[sel_predec_f3];
    assign pd_hword_num_banks[1] = pd_hword_num_for_sel[sel_predec_f3];
    assign pd_ras_pred_banks[1] = pd_ras_pred_banks_for_sel[sel_predec_f3];
    assign pd_cfi_taken_vec_banks[1] = pd_cfi_taken_vec_banks_for_sel[sel_predec_f3];
    assign pd_cfi_offset_vec_banks[1] = pd_cfi_offset_vec_banks_for_sel[sel_predec_f3];
    assign pd_cfi_valid_vec_banks[1] = pd_cfi_valid_vec_banks_for_sel[sel_predec_f3];

    always_comb begin
        if (has_misalign_ff) begin
            pd_rvc_mask_for_ras_banks[0] = {pd_inst_rvc_mask_banks[0][FETCH_WIDTH/16-1:1], 1'b1};
        end
        else begin
            pd_rvc_mask_for_ras_banks[0] = pd_inst_rvc_mask_banks[0];
        end
    end

    always_comb begin
        if (pd_has_misalign_banks[0]) begin
            pd_rvc_mask_for_ras_banks[1] = {pd_inst_rvc_mask_banks[1][FETCH_WIDTH/16-1:1], 1'b1};
        end
        else begin
            pd_rvc_mask_for_ras_banks[1] = pd_inst_rvc_mask_banks[1];
        end
    end

//////////////////////////////sel a bank in f3 stage////////////////////////////////////////
    // assertion
`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (~fetch_valid_banks[0] & fetch_valid_banks[1]) begin
            $display("the first bank is invalid and the second is valid");
            $finish();
        end
    end
`endif
    // check if 2nd ghist is correct
    assign pd_ghist_fix_ptr = fix_inner_stage_ghist_f3 ? 1'b0 : fetch_valid_banks[1];
    assign fix_inner_stage_ghist_f3 = fetch_valid_banks[0] & fetch_valid_banks[1] & (ghist_info_banks_f3[1] != pd_pred_ghist_info_banks[0]);
    assign fetch_btq_next_ghist_info_valid = btq_fetch_next_ghist_info_valid_banks[pd_ghist_fix_ptr];
    assign fetch_btq_next_ghist_info = btq_fetch_next_ghist_info_banks[pd_ghist_fix_ptr];

    always_comb begin
        if (fetch_btq_next_ghist_info_valid) begin
            fix_inter_stage_ghist_f3 = fetch_valid_banks[pd_ghist_fix_ptr] & (pd_pred_ghist_info_banks[pd_ghist_fix_ptr] != fetch_btq_next_ghist_info);
            fix_ghist_redirect_target_f3 = fetch_next_pc_banks[pd_ghist_fix_ptr];
        end
        else if (bpu_vld_b2) begin
            fix_inter_stage_ghist_f3 = fetch_valid_banks[pd_ghist_fix_ptr] & (pd_pred_ghist_info_banks[pd_ghist_fix_ptr] != ghist_info_b2);
            fix_ghist_redirect_target_f3 = pc_b2;
        end
        else if (bpu_vld_b1) begin
            fix_inter_stage_ghist_f3 = fetch_valid_banks[pd_ghist_fix_ptr] & (pd_pred_ghist_info_banks[pd_ghist_fix_ptr] != ghist_info_b1);
            fix_ghist_redirect_target_f3 = pc_b1;
        end
        else begin
            fix_inter_stage_ghist_f3 = 0;
            fix_ghist_redirect_target_f3 = 0;
        end
    end

    assign fetch_fix_ghist = fix_inner_stage_ghist_f3 | fix_inter_stage_ghist_f3;
    assign fix_inner_stage_pred_f3 = fetch_valid_banks[0] & fetch_valid_banks[1] & (pd_truncate_banks[0] | pd_cfi_taken_banks[0]);
    assign pd_pred_fix_ptr = fix_inner_stage_pred_f3 ? 1'b0 : fetch_valid_banks[1];
    assign fix_inter_stage_pred_f3 = fetch_valid_banks[pd_pred_fix_ptr] & (~fetch_next_valid_banks[pd_pred_fix_ptr] | 
                                    (pd_cfi_target_banks[pd_pred_fix_ptr] != fetch_next_pc_banks[pd_pred_fix_ptr]));
    assign fix_pred_f3 = fix_inner_stage_pred_f3 | fix_inter_stage_pred_f3;

    assign ras_meta_banks_f3[0] = btq_fetch_ras_meta_banks[0];
    assign ras_meta_banks_f3[1] = btq_fetch_ras_meta_banks[1];

    always_comb begin
        fix_ras_f3 = 0;
        ras_fixing_ptr_f3 = 0;
        if (fetch_valid_banks[0]) begin
            if (ras_typedef::ras_pred_incompatible(pd_ras_pred_banks[0], btq_fetch_ras_pred_banks[0])) begin
                fix_ras_f3 = 1'b1;
                ras_fixing_ptr_f3 = 0;
            end
        end
        if (fetch_valid_banks[1]) begin
            if (ras_typedef::ras_pred_incompatible(pd_ras_pred_banks[1], btq_fetch_ras_pred_banks[1])) begin
                fix_ras_f3 = 1'b1;
                ras_fixing_ptr_f3 = (fix_inner_stage_pred_f3 | fix_inner_stage_ghist_f3) ? 1'b0 : 1'b1;
            end
        end
        if ((fix_ras_f3 == 0) & fetch_redirect) begin
            ras_fixing_ptr_f3 = &fetch_valid_banks ? ((fix_inner_stage_pred_f3 | fix_inner_stage_ghist_f3) ? 1'b0 : 1'b1) : 1'b0;
        end
    end

    always_comb begin
        if (fix_inner_stage_pred_f3) begin
            pred_ghist_info_f3 = pd_pred_ghist_info_banks[0];
        end
        else if (fix_inner_stage_ghist_f3) begin
            pred_ghist_info_f3 = pd_pred_ghist_info_banks[0];
        end
        else if (fix_inter_stage_pred_f3) begin
            pred_ghist_info_f3 = pd_pred_ghist_info_banks[pd_pred_fix_ptr];
        end
        else begin // if (fix_inter_stage_pred_f3)
            pred_ghist_info_f3 = pd_pred_ghist_info_banks[pd_ghist_fix_ptr];
        end
    end

    always_comb begin
        if (fix_inner_stage_pred_f3) begin
            redirect_target_f3 = pd_cfi_target_banks[0];
        end
        else if (fix_inner_stage_ghist_f3) begin
            redirect_target_f3 = fetch_pc_banks[1];
        end
        else if (fix_inter_stage_pred_f3) begin
            redirect_target_f3 = pd_cfi_target_banks[pd_pred_fix_ptr];
        end
        else begin // if (fix_inter_stage_pred_f3)
            redirect_target_f3 = fix_ghist_redirect_target_f3;
        end
    end

    assign fetch_redirect = |fetch_valid_banks & (fix_pred_f3 | fetch_fix_ghist | fix_ras_f3) & ibuf_ready;
    assign pd_cfi_taken = |(fetch_valid_banks & pd_cfi_taken_banks);
    assign deq_dual_blks_f3 = fix_inner_stage_pred_f3 | fix_inner_stage_ghist_f3 ? 1'b0 : &fetch_valid_banks;
    always_comb begin
        if (fix_inner_stage_pred_f3 | fix_inner_stage_ghist_f3) begin
            redirect_btq_tag_f3 = fetch_btq_tag_banks[0];
        end
        else if (fix_inter_stage_pred_f3) begin
            if (pd_pred_fix_ptr) begin
                redirect_btq_tag_f3 = fetch_btq_tag_banks[1];
            end
            else begin
                redirect_btq_tag_f3 = fetch_btq_tag_banks[0];
            end
        end
        else begin // if (fix_inter_stage_ghist_f3) begin
            if (pd_ghist_fix_ptr) begin
                redirect_btq_tag_f3 = fetch_btq_tag_banks[1];
            end
            else begin
                redirect_btq_tag_f3 = fetch_btq_tag_banks[0];
            end
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            fetch_redirect_ff  <= 0;
            fetch_fix_ghist_ff <= 0;
        end else begin
            fetch_redirect_ff  <= fetch_redirect;
            fetch_fix_ghist_ff <= fetch_fix_ghist;
        end
    end
    assign fetch_redirect_pulse   = fetch_redirect & ~fetch_redirect_ff;
    assign fix_ghist_f3_pulse  = fetch_fix_ghist & ~fetch_fix_ghist_ff;

//////////////////////////////record misalign////////////////////////////////////////
    always_ff @(posedge clk) begin
        if (rst) begin
            has_misalign_ff <= 0;
        end 
        else if (btq_redirect_valid) begin
            has_misalign_ff <= 0;
        end
        else if (ibuf_ready) begin
            if (fix_inner_stage_pred_f3) begin
                has_misalign_ff <= 0;
            end
            else if (fix_inner_stage_ghist_f3) begin
                has_misalign_ff <= pd_has_misalign_banks[0];
                misalign_half_ff <= pd_misalign_half_banks[0];
            end
            else if (|((pd_cfi_taken_banks | pd_truncate_banks) & fetch_valid_banks)) begin
                has_misalign_ff <= 0;
            end
            else if (fetch_valid_banks[1]) begin
                has_misalign_ff <= pd_has_misalign_banks[1];
                misalign_half_ff <= pd_misalign_half_banks[1];
            end
            else if (fetch_valid_banks[0]) begin
                has_misalign_ff <= pd_has_misalign_banks[0];
                misalign_half_ff <= pd_misalign_half_banks[0];
            end
        end
    end

//////////////////////////////btq////////////////////////////////////////
    assign btq_alloc_valid_banks[0] = bpp_vld_banks_b2[older_ptr_b2];
    assign btq_alloc_valid_banks[1] = &bpp_vld_banks_b2 & ~(|invalidate_banks_b2);
    assign btq_alloc_ghist_info_banks[0] = ghist_info_b2;
    assign btq_alloc_ghist_info_banks[1] = older_pred_ghist_info_b2;
    assign btq_alloc_ubtb_meta_banks = {ubtb_meta_banks_b2[newer_ptr_b2], ubtb_meta_banks_b2[older_ptr_b2]};
    assign btq_alloc_btb_meta_banks = {btb_meta_banks_b2[newer_ptr_b2], btb_meta_banks_b2[older_ptr_b2]};
    assign btq_alloc_bht_meta_banks = {bht_meta_banks_b2[newer_ptr_b2], bht_meta_banks_b2[older_ptr_b2]};
    assign btq_alloc_ras_meta_banks = {ras_meta_banks_b2[newer_ptr_b2], ras_meta_banks_b2[older_ptr_b2]};
    assign btq_alloc_ras_pred_banks = {ras_pred_banks_b2[newer_ptr_b2], ras_pred_banks_b2[older_ptr_b2]};
    assign btq_alloc_tage_meta_banks = {composed_tage_meta_banks_b2[newer_ptr_b2], composed_tage_meta_banks_b2[older_ptr_b2]};

    assign fetch_btq_valid_banks[0] = fetch_valid_banks[0] & (pd_hword_num_banks[0] != 0);
    assign fetch_btq_valid_banks[1] = fetch_valid_banks[1] & (pd_hword_num_banks[1] != 0);

    assign fetch_btq_invalid = (fetch_valid_banks[0] & (pd_hword_num_banks[0] == 0));
    assign fetch_btq_invalid_tag = fetch_btq_tag_banks[0];
    assign btq_precheck_valid_banks = bpp_vld_banks_b1;


    btq u_btq(
        // enqueue
        // br predictor pipeline, allocate entry
        //  must guarantee that the first one is older
        // b1 stage: pre-check if btq has room for fetch package in both b2 & b1 stage 
        .precheck_valid_banks_b0_i(req_valid_banks_f0),
        .precheck_valid_banks_b1_i(btq_precheck_valid_banks),
        .precheck_ready_o(btq_precheck_ready),
        // b2 stage
        .alloc_valid_banks_i(btq_alloc_valid_banks),
        .alloc_ghist_info_banks_i(btq_alloc_ghist_info_banks),
        .alloc_ubtb_meta_banks_i(btq_alloc_ubtb_meta_banks),
        .alloc_bht_meta_banks_i(btq_alloc_bht_meta_banks),
        .alloc_tage_meta_banks_i(btq_alloc_tage_meta_banks),
        .alloc_btb_meta_banks_i(btq_alloc_btb_meta_banks),
        .alloc_ras_pred_banks_i(btq_alloc_ras_pred_banks),
        .alloc_ras_meta_banks_i(btq_alloc_ras_meta_banks),
        .alloc_tag_banks_o(btq_alloc_tag_banks),
        .alloc_ready_o(btq_alloc_ready),
        
        // fetch stage
        .fetch_valid_banks_i(fetch_btq_valid_banks),
        .fetch_btq_tag_banks_i(fetch_btq_tag_banks),
        .fetch_pc_banks_i(fetch_pc_banks),
        .fetch_is_truncate_banks_i(pd_truncate_banks),
        .fetch_cfi_valid_vec_banks_i(pd_cfi_valid_vec_banks),
        .fetch_cfi_taken_vec_banks_i(pd_cfi_taken_vec_banks),
        .fetch_cfi_offset_vec_banks_i(pd_cfi_offset_vec_banks),
        .fetch_tail_pc_lob_banks_i(pd_fpkt_tail_pc_lob_banks),
        .fetch_target_banks_i(pd_cfi_target_banks),
        .fetch_cfi_mask_banks_i(pd_jal_mask_banks|pd_return_mask_banks|pd_jalr_mask_banks|pd_call_mask_banks|pd_br_mask_banks),
        .fetch_rvc_mask_banks_i(pd_rvc_mask_for_ras_banks),
        .fetch_call_mask_banks_i(pd_call_mask_banks),
        .fetch_ret_mask_banks_i(pd_return_mask_banks),
        .fetch_br_mask_banks_i(pd_br_mask_banks),
        .fetch_loop_meta_banks_i('0),
        .fetch_ras_pred_banks_i(pd_ras_pred_banks),

        .fetch_ras_pred_banks_o(btq_fetch_ras_pred_banks),
        .fetch_ras_meta_banks_o(btq_fetch_ras_meta_banks),
        .fetch_ghist_info_banks_o(btq_fetch_ghist_info_banks),
        .fetch_next_ghist_info_vld_banks_o(btq_fetch_next_ghist_info_valid_banks),
        .fetch_next_ghist_info_banks_o(btq_fetch_next_ghist_info_banks),
        
        // f4 read info
        .ibuf_btq_tag_i(fe_dec_btq_tag_o),
        .ibuf_excp_valid_i(fe_dec_excp_vld_o),
        .btq_ibuf_pc_hib_o(btq_ibuf_pc_hib),
        .btq_ibuf_pred_target_o(btq_ibuf_pred_target),

        // f3 redirect
        .f3_redirect_vld_i(fetch_redirect_pulse),
        .f3_redirect_btq_tag_i(redirect_btq_tag_f3),
        .f3_invalid_vld_i(fetch_btq_invalid),
        .f3_invalid_btq_tag_i(fetch_btq_invalid_tag),

        // be redirect
        .be_redirect_valid_i(be_redirect_valid),
        .be_redirect_pc_lob_i(be_redirect_pc_lob),
        .be_redirect_btq_tag_i(be_redirect_btq_tag),
        .be_redirect_taken_i(bru_redirect_taken_i),
        .be_redirect_target_i(be_redirect_target),

        .be_redirect_valid_o(btq_redirect_valid),
        .be_redirect_pc_lob_o(btq_redirect_pc_lob),
        .be_redirect_br_full_mask_o(btq_redirect_br_full_mask),
        .be_redirect_is_misalign_insn_o(btq_redirect_is_misalign_insn),
        .be_redirect_ras_pred_o(btq_redirect_ras_pred),
        .be_redirect_ras_meta_o(btq_redirect_ras_meta),
        .be_redirect_loop_meta_o(btq_redirect_loop_meta),
        .be_redirect_ghist_info_o(btq_redirect_ghist_info),
        .be_redirect_target_o(btq_redirect_target),
        .be_redirect_taken_o(btq_redirect_taken),

        // cmt > btq
        .cmt_valid_i(cmt_valid_i),
        .cmt_btq_tag_i(cmt_btq_tag_i),
        .cmt_pc_lob_i(cmt_pc_lob_i),

        // btq > bpu
        .bpu_update_valid_o(update_valid),
        .bpu_update_is_misalign_o(update_is_misalign),
        .bpu_update_pred_valid_o(update_pred_valid),
        .bpu_update_truncate_o(update_truncate),
        .bpu_update_req_pc_o(update_req_pc),
        .bpu_update_target_o(update_target),
        .bpu_update_tage_meta_o(update_composed_tage_meta),
        .bpu_update_btb_meta_o(update_btb_meta),
        .bpu_update_ubtb_meta_o(update_ubtb_meta),
        .bpu_update_bht_meta_o(update_bht_meta),
        .bpu_update_loop_meta_o(update_loop_meta),
        .bpu_update_cfi_mask_o(update_cfi_mask),
        .bpu_update_cfi_offset_vec_o(update_cfi_offset_vec),
        .bpu_update_cfi_taken_vec_o(update_cfi_taken_vec),
        .bpu_update_rvc_mask_o(update_rvc_mask),
        .bpu_update_call_mask_o(update_call_mask),
        .bpu_update_ret_mask_o(update_ret_mask),
        .bpu_update_br_mask_o(update_br_mask),
        .bpu_update_ghist_info_o(update_ghist_info),

`ifndef SYNTHESIS
        .is_commit_redirect_i(cmt_redirect_valid_i),
`endif 

        .flush_i(1'b0),

        .clk(clk),
        .rst(rst)
    );

//////////////////////////////ibuf////////////////////////////////////////
    generate 
        for (genvar i = 0; i < (FETCH_WIDTH*2/16)+1; i++) begin
            assign fetch_ibuf_tag_flatten[i * BTQ_TAG_WIDTH +: BTQ_TAG_WIDTH] = i < pd_hword_num_banks[0] ? fetch_btq_tag_banks[0] : fetch_btq_tag_banks[1];
        end
    endgenerate
    
    assign fetch_ibuf_fpkt_aligned = {fetch_fpkt_banks[1], fetch_fpkt_banks[0]} >> (fetch_pc_banks[0][$clog2(FETCH_WIDTH/8)-1:0] * 8);
    assign fetch_ibuf_rvc_mask_aligned = {pd_inst_rvc_mask_banks[1], pd_inst_rvc_mask_banks[0]} >> fetch_pc_banks[0][$clog2(FETCH_WIDTH/8)-1:1];
    assign fetch_ibuf_cfi_taken_mask_aligned = pd_has_misalign_banks[0] ? 
                                    {pd_cfi_taken_mask_banks[1][(FETCH_WIDTH/16)-1:1], 1'b0, pd_cfi_taken_mask_banks[1][0], pd_cfi_taken_mask_banks[0][(FETCH_WIDTH/16)-2:0]} >> fetch_pc_banks[0][$clog2(FETCH_WIDTH/8)-1:1] :
                                    {pd_cfi_taken_mask_banks[1], pd_cfi_taken_mask_banks[0]} >> fetch_pc_banks[0][$clog2(FETCH_WIDTH/8)-1:1];
    assign fetch_ibuf_hword_num = (pd_hword_num_banks[0] & {$clog2(FETCH_WIDTH/16)+1{fetch_valid_banks[0]}}) +
                                    (pd_hword_num_banks[1] & {$clog2(FETCH_WIDTH/16)+1{fetch_valid_banks[1] & ~fix_inner_stage_pred_f3 & ~fix_inner_stage_ghist_f3}});
    
    instr_buffer instr_buffer_u(
    // enqueue
        .enq_valid_i(|fetch_btq_valid_banks),
        .enq_pc_i(fetch_pc_banks[0]),
        .enq_has_end_half_i(has_misalign_ff),
        .enq_end_half_i(misalign_half_ff),
        .enq_cacheline_aligned_i(fetch_ibuf_fpkt_aligned),
        .enq_btq_tag_flatten_i(fetch_ibuf_tag_flatten),
        .enq_rvc_mask_aligned_i(fetch_ibuf_rvc_mask_aligned),
        .enq_hword_num_i(fetch_ibuf_hword_num),
        .enq_taken_cfi_mask_aligned_i(fetch_ibuf_cfi_taken_mask_aligned),
        .enq_ready_o(ibuf_ready),
    // dequeue
        .deq_pc_hib_i(btq_ibuf_pc_hib),
        .deq_instr_valid_o(ibuf_deq_valid),
        .deq_btq_tag_o(ibuf_deq_btq_tag),
        .deq_pc_o(ibuf_deq_pc),
        .deq_instr_o(ibuf_deq_instr),
        .deq_is_rvc_o(ibuf_deq_is_rvc),
        .deq_pred_taken_o(ibuf_deq_pred_taken),
        .deq_ready_i(fe_dec_ready_i),

        .flush_i(be_redirect_valid),
        .empty_o(ibuf_empty),

        .clk(clk),
        .rst(rst)
    );

    always_comb begin
        fe_dec_instr_valid_o = ibuf_deq_valid;
        fe_dec_pc_o = ibuf_deq_pc;
        fe_dec_btq_tag_o = ibuf_deq_btq_tag;
        if (fe_dec_excp_vld_o) begin
            fe_dec_instr_valid_o[0] = 1'b1;
            fe_dec_pc_o[0] =  has_misalign_ff ? ic_fetch_resp_excp_tval - 2'd2 : ic_fetch_resp_excp_tval;
            fe_dec_btq_tag_o[0] = ic_fetch_resp_btq_tag;
        end
    end

    assign fe_dec_instr_o = ibuf_deq_instr;
    assign fe_dec_is_rvc_o = ibuf_deq_is_rvc;
    assign fe_dec_pred_taken_o = ibuf_deq_pred_taken;
    assign fe_dec_excp_vld_o = ic_fetch_resp_excp_valid & ibuf_empty;
    assign fe_dec_excp_tval_o = ic_fetch_resp_excp_tval;
    assign fe_dec_excp_cause_o = ic_fetch_resp_excp_cause;
    assign fe_dec_pred_target_o = btq_ibuf_pred_target;

endmodule
