module npc_gen
    import rvh_pkg::*;
#(
)
(   
    output logic [VADDR_WIDTH-1:0]                     npc_o,

    input logic                                      f1_npc_vld_i,
    input logic [VADDR_WIDTH-1:0]                    f1_npc_i,
    
    input logic                                     f2_redirect_valid_i,
    input logic [VADDR_WIDTH-1:0]                    f2_redirect_pc_i,
    
    input logic                                     f3_redirect_valid_i,
    input logic [VADDR_WIDTH-1:0]                    f3_redirect_pc_i,
    
    input logic                                     bru_redirect_valid_i,
    input logic [VADDR_WIDTH-1:0]                    bru_redirect_pc_i,
    
    input logic                                     cmt_redirect_valid_i,
    input logic [VADDR_WIDTH-1:0]                    cmt_redirect_pc_i,
    
    input logic [VADDR_WIDTH-1:0]                   boot_address_i,

    input logic                                     stall_i,
    
    input               clk,
    input               rst
);

logic [VADDR_WIDTH-1:0]                   pc_ff;
logic                                     stall_ff;

always @(posedge clk) begin
    if (rst) begin
        stall_ff <= 0;
    end
    else begin
        stall_ff <= stall_i;
    end
end

always @(posedge clk) begin
    if (cmt_redirect_valid_i | bru_redirect_valid_i | f3_redirect_valid_i | f2_redirect_valid_i | f1_npc_vld_i | (stall_i & ~stall_ff)) begin
        pc_ff <= npc_o;
    end
end

always_comb begin
    if (cmt_redirect_valid_i) begin
        npc_o = cmt_redirect_pc_i;
    end
    else if (bru_redirect_valid_i) begin
        npc_o = bru_redirect_pc_i;
    end
    else if (f3_redirect_valid_i) begin
        npc_o = f3_redirect_pc_i;
    end
    else if (f2_redirect_valid_i) begin
        npc_o = f2_redirect_pc_i;
    end
    else if (f1_npc_vld_i) begin
        npc_o = f1_npc_i;
    end
    else if (stall_ff) begin
        npc_o = pc_ff;
    end
    else begin
        npc_o = boot_address_i;// {pc_ff[VADDR_WIDTH-1:$clog2(FETCH_WIDTH / 8)], {$clog2(FETCH_WIDTH / 8){1'b0}}} + (FETCH_WIDTH / 8);
    end
end





endmodule
