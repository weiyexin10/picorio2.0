module ghist
    import rvh_pkg::*;
#(
)
(
    output ghist_info_t                         ghist_info_b1_o,
    output logic [GHIST_LEN-1:0]                ghist_b1_o,

    input logic                                 b1_fix_ghist_i,
    input logic                                 b1_old_valid_i,
    input ghist_info_t                          ghist_info_old_b1_i,
    input ghist_info_t                          ghist_info_new_b1_i,

    input logic                                 b2_fix_ghist_i,
    input logic                                 b2_old_valid_i,
    input ghist_info_t                          ghist_info_old_b2_i,
    input ghist_info_t                          ghist_info_new_b2_i,
    
    input logic                                 f3_fix_ghist_i,
    input logic                                 f3_old_valid_i,
    input ghist_info_t                          ghist_info_old_f3_i,
    input ghist_info_t                          ghist_info_new_f3_i,

    input logic                                 be_fix_ghist_i,
    input ghist_info_t                          ghist_info_be_i,

    input ghist_info_t                          update_ghist_info_i,
    output logic [GHIST_LEN-1:0]                update_ghist_o,

`ifndef SYNTHESIS
    input ghist_info_t                          ori_info_be_i,
    output logic [GHIST_LEN-1:0]                dbg_be_ghist_o,

    input ghist_info_t                          ori_info_f3_i,
    output logic [GHIST_LEN-1:0]                dbg_f3_ghist_o,
`endif

    input logic                                 req_ready_i,
    
    input logic                                 clk,
    input logic                                 rst
);



logic [GHIST_CAPACITY-1:0]              ghist_queue, next_ghist_queue;
logic [$clog2(GHIST_CAPACITY)-1:0]      head, tail, next_tail, write_ptr, update_head;
logic [GHIST_LEN-1:0]                   ghist_lower_half, ghist_upper_half;
logic [GHIST_LEN-1:0]                   update_ghist_lower_half, update_ghist_upper_half;
logic [GHIST_LEN-1:0]                   ghist_raw_b1, ghist_raw_update;

always_comb begin
    next_ghist_queue = ghist_queue;
    write_ptr = 0;
    if (be_fix_ghist_i) begin
        write_ptr = ghist_info_be_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_be_i.tail_ptr - 1'b1;
        next_ghist_queue[write_ptr] = ghist_info_be_i.lsb; 
    end
    else if (f3_fix_ghist_i) begin
        write_ptr = ghist_info_new_f3_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_new_f3_i.tail_ptr - 1'b1;
        next_ghist_queue[write_ptr] = ghist_info_new_f3_i.lsb; 
        if (f3_old_valid_i) begin
            write_ptr = ghist_info_old_f3_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_old_f3_i.tail_ptr - 1'b1;
            next_ghist_queue[write_ptr] = ghist_info_old_f3_i.lsb; 
        end
    end
    else if (b2_fix_ghist_i) begin
        write_ptr = ghist_info_new_b2_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_new_b2_i.tail_ptr - 1'b1;
        next_ghist_queue[write_ptr] = ghist_info_new_b2_i.lsb;
        if (b2_old_valid_i) begin
            write_ptr = ghist_info_old_b2_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_old_b2_i.tail_ptr - 1'b1;
            next_ghist_queue[write_ptr] = ghist_info_old_b2_i.lsb; 
        end
    end
    else if (b1_fix_ghist_i) begin
        write_ptr = ghist_info_new_b1_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_new_b1_i.tail_ptr - 1'b1;
        next_ghist_queue[write_ptr] = ghist_info_new_b1_i.lsb;
        if (b1_old_valid_i) begin
            write_ptr = ghist_info_old_b1_i.tail_ptr == 0 ? GHIST_CAPACITY - 1'b1 : ghist_info_old_b1_i.tail_ptr - 1'b1;
            next_ghist_queue[write_ptr] = ghist_info_old_b1_i.lsb; 
        end
    end
end

always_comb begin
    next_tail = tail;
    if (be_fix_ghist_i) begin
        next_tail = ghist_info_be_i.tail_ptr;
    end
    else if (f3_fix_ghist_i) begin
        next_tail = ghist_info_new_f3_i.tail_ptr;
    end
    else if (b2_fix_ghist_i) begin
        next_tail = ghist_info_new_b2_i.tail_ptr;
    end
    else if (b1_fix_ghist_i) begin
        next_tail = ghist_info_new_b1_i.tail_ptr;
    end
end

always @(posedge clk) begin
    if (rst) begin
        ghist_queue <= 0;
    end
    else if (be_fix_ghist_i | f3_fix_ghist_i | b2_fix_ghist_i) begin
        ghist_queue <= next_ghist_queue;
    end
    else if (b1_fix_ghist_i & req_ready_i) begin
        ghist_queue <= next_ghist_queue;
    end
end

always @(posedge clk) begin
    if (rst) begin
        tail <= GHIST_LEN;
    end
    else if (be_fix_ghist_i | f3_fix_ghist_i | b2_fix_ghist_i) begin 
        tail <= next_tail;
    end
    else if (b1_fix_ghist_i & req_ready_i) begin
        tail <= next_tail;
    end
end

always_comb begin
    if (tail == 0) begin
        head = GHIST_CAPACITY - GHIST_LEN;
    end
    else if (tail < GHIST_LEN) begin
        head = GHIST_CAPACITY - (GHIST_LEN - tail);
    end
    else begin
        head = tail - GHIST_LEN;
    end
end

assign ghist_info_b1_o.tail_ptr = tail;
assign ghist_info_b1_o.lsb = tail == 0 ? ghist_queue[GHIST_CAPACITY-1] : ghist_queue[tail - 1'b1];

assign ghist_lower_half = ghist_queue >> head;
assign ghist_upper_half = (ghist_queue & ((1 << tail) - 1'b1)) << (GHIST_CAPACITY - head);

always_comb begin
    if (tail < GHIST_LEN) begin
        ghist_raw_b1 = ghist_lower_half | ghist_upper_half;
    end
    else begin
        ghist_raw_b1 = ghist_queue[head +: GHIST_LEN];
    end
end

// invert the result
generate 
    for (genvar i = 0; i < GHIST_LEN; i++) begin
        assign ghist_b1_o[i] = ghist_raw_b1[GHIST_LEN-1-i];
    end
endgenerate
////////////////////////////// read port for update ////////////////////////////////////////
always_comb begin
    if (update_ghist_info_i.tail_ptr == 0) begin
        update_head = GHIST_CAPACITY - GHIST_LEN;
    end
    else if (update_ghist_info_i.tail_ptr < GHIST_LEN) begin
        update_head = GHIST_CAPACITY - (GHIST_LEN - update_ghist_info_i.tail_ptr);
    end
    else begin
        update_head = update_ghist_info_i.tail_ptr - GHIST_LEN;
    end
end

assign update_ghist_lower_half = ghist_queue >> update_head;
assign update_ghist_upper_half = (ghist_queue & ((1 << update_ghist_info_i.tail_ptr) - 1'b1)) << (GHIST_CAPACITY - update_head);

always_comb begin
    if (update_ghist_info_i.tail_ptr < GHIST_LEN) begin
        ghist_raw_update = update_ghist_lower_half | update_ghist_upper_half;
    end
    else begin
        ghist_raw_update = ghist_queue[update_head +: GHIST_LEN];
    end
end
// invert the result
generate 
    for (genvar i = 0; i < GHIST_LEN; i++) begin
        assign update_ghist_o[i] = ghist_raw_update[GHIST_LEN-1-i];
    end
endgenerate

`ifndef SYNTHESIS
    logic [$clog2(GHIST_CAPACITY)-1:0]      dbg_be_head;
    logic [GHIST_LEN-1:0]                   be_ghist_lower_half, be_ghist_upper_half, be_raw_ghist;

    always_comb begin
        if (ori_info_be_i.tail_ptr == 0) begin
            dbg_be_head = GHIST_CAPACITY - GHIST_LEN;
        end
        else if (ori_info_be_i.tail_ptr < GHIST_LEN) begin
            dbg_be_head = GHIST_CAPACITY - (GHIST_LEN - ori_info_be_i.tail_ptr);
        end
        else begin
            dbg_be_head = ori_info_be_i.tail_ptr - GHIST_LEN;
        end
    end

    assign be_ghist_lower_half = ghist_queue >> dbg_be_head;
    assign be_ghist_upper_half = (ghist_queue & ((1 << ori_info_be_i.tail_ptr) - 1'b1)) << (GHIST_CAPACITY - dbg_be_head);

    always_comb begin
        if (ori_info_be_i.tail_ptr < GHIST_LEN) begin
            be_raw_ghist = be_ghist_lower_half | be_ghist_upper_half;
        end
        else begin
            be_raw_ghist = ghist_queue[dbg_be_head +: GHIST_LEN];
        end
    end
    // invert the result
    generate 
        for (genvar i = 0; i < GHIST_LEN; i++) begin
            assign dbg_be_ghist_o[i] = be_raw_ghist[GHIST_LEN-1-i];
        end
    endgenerate


/////////////////////////////////////////////////////////////////////////////////////////////////////////
    logic [$clog2(GHIST_CAPACITY)-1:0]      dbg_f3_head;
    logic [GHIST_LEN-1:0]                   f3_ghist_lower_half, f3_ghist_upper_half, f3_raw_ghist;

    always_comb begin
        if (ori_info_f3_i.tail_ptr == 0) begin
            dbg_f3_head = GHIST_CAPACITY - GHIST_LEN;
        end
        else if (ori_info_f3_i.tail_ptr < GHIST_LEN) begin
            dbg_f3_head = GHIST_CAPACITY - (GHIST_LEN - ori_info_f3_i.tail_ptr);
        end
        else begin
            dbg_f3_head = ori_info_f3_i.tail_ptr - GHIST_LEN;
        end
    end

    assign f3_ghist_lower_half = ghist_queue >> dbg_f3_head;
    assign f3_ghist_upper_half = (ghist_queue & ((1 << ori_info_f3_i.tail_ptr) - 1'b1)) << (GHIST_CAPACITY - dbg_f3_head);

    always_comb begin
        if (ori_info_f3_i.tail_ptr < GHIST_LEN) begin
            f3_raw_ghist = f3_ghist_lower_half | f3_ghist_upper_half;
        end
        else begin
            f3_raw_ghist = ghist_queue[dbg_f3_head +: GHIST_LEN];
        end
    end
    // invert the result
    generate 
        for (genvar i = 0; i < GHIST_LEN; i++) begin
            assign dbg_f3_ghist_o[i] = f3_raw_ghist[GHIST_LEN-1-i];
        end
    endgenerate    



`endif

endmodule
