module instr_buffer
    import rvh_pkg::*;
#(

)
(
    // enqueue
    input   logic                                               enq_valid_i,
    input   logic [VADDR_WIDTH-1:0]                             enq_pc_i,
    input   logic                                               enq_has_end_half_i,
    input   logic [15:0]                                        enq_end_half_i,
    input   logic [(FETCH_WIDTH*2/16+1)*BTQ_TAG_WIDTH-1:0]      enq_btq_tag_flatten_i,
    input   logic [FETCH_WIDTH*2-1:0]                           enq_cacheline_aligned_i,
    input   logic [(FETCH_WIDTH*2/16)-1:0]                      enq_rvc_mask_aligned_i,
    input   logic [(FETCH_WIDTH*2/16)-1:0]                      enq_taken_cfi_mask_aligned_i,
    input   logic [$clog2(FETCH_WIDTH*2/16)  :0]                enq_hword_num_i,
    output  logic                                               enq_ready_o,

    // dequeue
    input logic  [       DECODE_WIDTH-1:0][       PC_HIB_WIDTH-1:0] deq_pc_hib_i,
    output logic [       DECODE_WIDTH-1:0]                          deq_instr_valid_o,
    output logic [       DECODE_WIDTH-1:0][      BTQ_TAG_WIDTH-1:0] deq_btq_tag_o,
    output logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] deq_pc_o,
    output logic [       DECODE_WIDTH-1:0][                   31:0] deq_instr_o,
    output logic [       DECODE_WIDTH-1:0]                          deq_is_rvc_o,
    output logic [       DECODE_WIDTH-1:0]                          deq_pred_taken_o,
    input  logic                                                    deq_ready_i,

    input   logic                                               flush_i,
    output  logic                                               empty_o,

    input   logic                                               clk,
    input   logic                                               rst
);

// logic [IB_PKT_NUM_N-1:0][VADDR_WIDTH-1:0]           pc_base_f4;
logic [(FETCH_WIDTH*2/16)*2*BTQ_TAG_WIDTH-1:0]        btq_tag_flatten_f4;
logic [(FETCH_WIDTH*2/16)*2*PC_LOB_WIDTH-1:0]         pc_lob_flatten_f4;
logic [(FETCH_WIDTH*2/16+1)*PC_LOB_WIDTH-1:0]         pc_lob_flatten_new;

logic [FETCH_WIDTH*2*2-1:0]                           cacheline_aligned_f4;
logic [(FETCH_WIDTH*2/16)*2-1:0]                      rvc_mask_aligned_f4;
logic [$clog2(FETCH_WIDTH*2/16*2):0]                  hword_num_f4;
logic [(FETCH_WIDTH*2/16)*2-1:0]                      taken_mask_aligned_f4;

logic [$clog2(FETCH_WIDTH/16):0]                      deq_hword_num_f4;

logic                                               enq_fire;
logic                                               deq_fire;

logic [       DECODE_WIDTH-1:0][        VADDR_WIDTH-1:0] deq_pc_lob;
logic [DECODE_WIDTH-1:0][$clog2(FETCH_WIDTH/16):0]  offsets;

logic [$clog2(FETCH_WIDTH*2/16*2)-1:0]                rw_tail, wo_tail;

assign rw_tail = hword_num_f4 - deq_hword_num_f4;
assign wo_tail = hword_num_f4;

always_ff @(posedge clk) begin
    if (enq_fire & deq_fire) begin
        cacheline_aligned_f4 <= cacheline_aligned_f4 >> (deq_hword_num_f4 * 16);
        if (enq_has_end_half_i) begin
            cacheline_aligned_f4[rw_tail*16 +: FETCH_WIDTH*2+16] <= {enq_cacheline_aligned_i, enq_end_half_i};
        end
        else begin
            cacheline_aligned_f4[rw_tail*16 +: FETCH_WIDTH*2] <= enq_cacheline_aligned_i;
        end
    end
    else if (enq_fire) begin
        if (enq_has_end_half_i) begin
            cacheline_aligned_f4[wo_tail*16 +: FETCH_WIDTH*2+16] <= {enq_cacheline_aligned_i, enq_end_half_i};
        end
        else begin
            cacheline_aligned_f4[wo_tail*16 +: FETCH_WIDTH*2] <= enq_cacheline_aligned_i;
        end
    end
    else if (deq_fire) begin
        cacheline_aligned_f4 <= cacheline_aligned_f4 >> (deq_hword_num_f4 * 16);
    end
end

always_ff @(posedge clk) begin
    if (enq_fire & deq_fire) begin
        rvc_mask_aligned_f4 <= rvc_mask_aligned_f4 >> deq_hword_num_f4;
        if (enq_has_end_half_i) begin
            rvc_mask_aligned_f4[rw_tail +: (FETCH_WIDTH*2/16)+1] <= {enq_rvc_mask_aligned_i, 1'b0};
        end
        else begin
            rvc_mask_aligned_f4[rw_tail +: (FETCH_WIDTH*2/16)] <= enq_rvc_mask_aligned_i;
        end
    end
    else if (enq_fire) begin
        if (enq_has_end_half_i) begin 
            rvc_mask_aligned_f4[wo_tail +: (FETCH_WIDTH*2/16)+1] <= {enq_rvc_mask_aligned_i, 1'b0};
        end
        else begin
            rvc_mask_aligned_f4[wo_tail +: (FETCH_WIDTH*2/16)] <= enq_rvc_mask_aligned_i;
        end
    end
    else if (deq_fire) begin
        rvc_mask_aligned_f4 <= rvc_mask_aligned_f4 >> deq_hword_num_f4;
    end
end

always_ff @(posedge clk) begin
    if (enq_fire & deq_fire) begin
        taken_mask_aligned_f4 <= taken_mask_aligned_f4 >> deq_hword_num_f4;
        if (enq_has_end_half_i) begin
            taken_mask_aligned_f4[rw_tail +: (FETCH_WIDTH*2/16)+1] <= {enq_taken_cfi_mask_aligned_i[(FETCH_WIDTH*2/16)-1:1], 1'b0, enq_taken_cfi_mask_aligned_i[0]};
        end
        else begin
            taken_mask_aligned_f4[rw_tail +: (FETCH_WIDTH*2/16)] <= enq_taken_cfi_mask_aligned_i;
        end
    end
    else if (enq_fire) begin
        if (enq_has_end_half_i) begin
            taken_mask_aligned_f4[wo_tail +: (FETCH_WIDTH*2/16)+1] <= {enq_taken_cfi_mask_aligned_i[(FETCH_WIDTH*2/16)-1:1], 1'b0, enq_taken_cfi_mask_aligned_i[0]};
        end
        else begin
            taken_mask_aligned_f4[wo_tail +: (FETCH_WIDTH*2/16)] <= enq_taken_cfi_mask_aligned_i;
        end
    end
    else if (deq_fire) begin
        taken_mask_aligned_f4 <= taken_mask_aligned_f4 >> deq_hword_num_f4;
    end
end


always_ff @(posedge clk) begin
    if (enq_fire & deq_fire) begin
        btq_tag_flatten_f4 <= btq_tag_flatten_f4 >> (deq_hword_num_f4 * BTQ_TAG_WIDTH);
        btq_tag_flatten_f4[rw_tail*BTQ_TAG_WIDTH +: (FETCH_WIDTH*2/16+1)*BTQ_TAG_WIDTH] <= enq_btq_tag_flatten_i;
    end
    else if (enq_fire) begin
        btq_tag_flatten_f4[wo_tail*BTQ_TAG_WIDTH +: (FETCH_WIDTH*2/16+1)*BTQ_TAG_WIDTH] <= enq_btq_tag_flatten_i;
    end
    else if (deq_fire) begin
        btq_tag_flatten_f4 <= btq_tag_flatten_f4 >> (deq_hword_num_f4 * BTQ_TAG_WIDTH);
    end
end

always_comb begin
    if (enq_has_end_half_i) begin
        pc_lob_flatten_new[0 +: PC_LOB_WIDTH] = enq_pc_i[PC_LOB_WIDTH-1:0] - 2'd2;
        for (int i = 1; i < (FETCH_WIDTH*2/16+1); i++) begin
            pc_lob_flatten_new[i * PC_LOB_WIDTH +: PC_LOB_WIDTH] = enq_pc_i[PC_LOB_WIDTH-1:0] + (i - 1) * 2;
        end
    end
    else begin
        for (int i = 0; i < (FETCH_WIDTH*2/16+1); i++) begin
            pc_lob_flatten_new[i * PC_LOB_WIDTH +: PC_LOB_WIDTH] = enq_pc_i[PC_LOB_WIDTH-1:0] + i * 2;
        end
    end
end

always_ff @(posedge clk) begin
    if (enq_fire & deq_fire) begin
        pc_lob_flatten_f4 <= pc_lob_flatten_f4 >> (deq_hword_num_f4 * PC_LOB_WIDTH);
        pc_lob_flatten_f4[rw_tail*PC_LOB_WIDTH +: (FETCH_WIDTH*2/16+1)*PC_LOB_WIDTH] <= pc_lob_flatten_new;
    end
    else if (enq_fire) begin
        pc_lob_flatten_f4[wo_tail*PC_LOB_WIDTH +: (FETCH_WIDTH*2/16+1)*PC_LOB_WIDTH] <= pc_lob_flatten_new;
    end
    else if (deq_fire) begin
        pc_lob_flatten_f4 <= pc_lob_flatten_f4 >> (deq_hword_num_f4 * PC_LOB_WIDTH);
    end
end


always_ff @(posedge clk) begin
    if (rst) begin
        hword_num_f4 <= 0;
    end
    else if (flush_i) begin
        hword_num_f4 <= 0;
    end
    else if (enq_fire & deq_fire) begin
        hword_num_f4 <= hword_num_f4 - deq_hword_num_f4 + enq_hword_num_i;
    end
    else if (enq_fire) begin
        hword_num_f4 <= hword_num_f4 + enq_hword_num_i;
    end
    else if (deq_fire) begin
        hword_num_f4 <= hword_num_f4 - deq_hword_num_f4;
    end
end

always_comb begin
    deq_hword_num_f4 = 0;
    for (int i = 0; i < DECODE_WIDTH; i++) begin
        if (deq_instr_valid_o[i]) begin
            deq_hword_num_f4 = deq_is_rvc_o[i] ? offsets[i] + 1'b1 : offsets[i] + 2'd2;
        end
    end
end

// offset generate
assign offsets[0] = 0;
generate
    for (genvar i=1; i < DECODE_WIDTH; i++) begin : GEN_OFFSET
        assign offsets[i] = rvc_mask_aligned_f4[offsets[i-1]] ? offsets[i-1] + 1 : offsets[i-1] + 2;
    end
endgenerate

// instr generate
generate
    for (genvar i=0; i < DECODE_WIDTH; i++) begin : GEN_INSTR
        assign deq_instr_o[i] = cacheline_aligned_f4[offsets[i]*16+:32];
    end
endgenerate

// is_rvc generate
generate
    for (genvar i=0; i < DECODE_WIDTH; i++) begin : GEN_RVC_MASK
        assign deq_is_rvc_o[i] = rvc_mask_aligned_f4[offsets[i]];
    end
endgenerate

// is_taken generate
generate
    for (genvar i=0; i < DECODE_WIDTH; i++) begin : GEN_PRED_MASK
        assign deq_pred_taken_o[i] = taken_mask_aligned_f4[offsets[i]];
    end
endgenerate

// btq_tag generate
generate
    for (genvar i=0; i < DECODE_WIDTH; i++) begin : GEN_btq_tag
        assign deq_btq_tag_o[i] = btq_tag_flatten_f4[offsets[i]*BTQ_TAG_WIDTH+:BTQ_TAG_WIDTH];
    end
endgenerate

// pc_lob generate
generate
    for (genvar i=0; i < DECODE_WIDTH; i++) begin : GEN_PC_LOB
        assign deq_pc_lob[i] = pc_lob_flatten_f4[offsets[i]*PC_LOB_WIDTH+:PC_LOB_WIDTH];
    end
endgenerate

// pc generate
always_comb begin
    for (int i = 0; i < DECODE_WIDTH; i++) begin
        if (deq_pc_hib_i[i][0] == deq_pc_lob[i][PC_LOB_WIDTH-1]) begin
            deq_pc_o[i] = {deq_pc_hib_i[i], deq_pc_lob[i][PC_LOB_WIDTH-2:0]};
        end
        else begin
            deq_pc_o[i] = {(deq_pc_hib_i[i] - 1'b1), deq_pc_lob[i][PC_LOB_WIDTH-2:0]};
        end
    end
end

// instr_valid generate
always_comb begin
    deq_instr_valid_o[0] = 0;
    if (rvc_mask_aligned_f4[offsets[0]]) begin
        deq_instr_valid_o[0] = (hword_num_f4 != 0) & (offsets[0] + 1'b1 <= hword_num_f4);
    end
    else begin
        deq_instr_valid_o[0] = (hword_num_f4 != 0) & (offsets[0] + 2'd2 <= hword_num_f4);
    end
end

always_comb begin
    for (int i = 1; i < DECODE_WIDTH; i++) begin
        deq_instr_valid_o[i] = 0;
        if (deq_instr_valid_o[i - 1'b1]) begin
            if (rvc_mask_aligned_f4[offsets[i]]) begin
                deq_instr_valid_o[i] = (offsets[i] + 1'b1 <= hword_num_f4);
            end
            else begin
                deq_instr_valid_o[i] = (offsets[i] + 2'd2 <= hword_num_f4);
            end
        end
    end
end

assign enq_ready_o = (hword_num_f4 == 0) | (enq_hword_num_i <= ((FETCH_WIDTH/16)*2*2 - hword_num_f4)) | 
                        ((enq_hword_num_i <= ((FETCH_WIDTH/16)*2*2 - hword_num_f4 + deq_hword_num_f4)) & deq_ready_i);

assign empty_o = hword_num_f4 == 0;
assign enq_fire = enq_ready_o & enq_valid_i & (enq_hword_num_i != 0);
assign deq_fire =  (hword_num_f4 != 0) & deq_ready_i;
`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (~rst) begin
            if ((deq_hword_num_f4 > hword_num_f4) & (hword_num_f4 != 0)) begin
                $display("deq length can not greater than total length");
                $finish();
            end
        end
    end
`endif
endmodule
