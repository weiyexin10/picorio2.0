module btq_entry
    import rvh_pkg::*;
    import riscv_pkg::*;
#() (
    // alloc
    input logic                                         alloc_valid_i,
    input ghist_info_t                                  alloc_ghist_info_i,
    input ubtb_typedef::ubtb_meta_t                     alloc_ubtb_meta_i,
    input bht_typedef::bht_meta_t                       alloc_bht_meta_i,
    input tage_typedef::composed_tage_pred_meta_t       alloc_tage_meta_i,
    input btb_typedef::btb_meta_t                       alloc_btb_meta_i,
    input ras_typedef::ras_pred_t                       alloc_ras_pred_i,
    input ras_typedef::ras_meta_t                       alloc_ras_meta_i,

    // f3 fill: from pre-decoder
    input logic                                         fetch_valid_i,
    input logic [VADDR_WIDTH-1:0]                       fetch_pc_i,
    input logic [PC_LOB_WIDTH-1:0]                      fetch_tail_pc_lob_i,
    input logic [VADDR_WIDTH-1:0]                       fetch_target_i,
    input logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]fetch_cfi_offsets_i,
    input logic [PREDS_PER_LINE-1:0]                    fetch_cfi_takens_i,
    input logic [PREDS_PER_LINE-1:0]                    fetch_cfi_valids_i,
    input logic                                         fetch_cfi_is_truncate_i,
    input logic [FETCH_WIDTH/16-1:0]                    fetch_cfi_full_mask_i,
    input logic [FETCH_WIDTH/16-1:0]                    fetch_rvc_full_mask_i,
    input logic [FETCH_WIDTH/16-1:0]                    fetch_call_full_mask_i,
    input logic [FETCH_WIDTH/16-1:0]                    fetch_ret_full_mask_i,
    input logic [FETCH_WIDTH/16-1:0]                    fetch_br_full_mask_i,
    input ras_typedef::ras_pred_t                       fetch_ras_pred_i,

    // f4
    input logic                                         mark_excp_i,

    // be redirect
    input logic                                         be_redirect_valid_i,
    input logic [BRU_COUNT-1:0]                         be_redirect_taken_i,
    input logic [PC_LOB_WIDTH-1:0]                      be_redirect_tail_pc_lob_i,
    input logic [     VADDR_WIDTH-1:0]                  be_redirect_target_i,
    

    // output 
    output logic                                        valid_o,
    output logic [FETCH_WIDTH/16-1:0]                   br_full_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                   call_full_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                   ret_full_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                   cfi_full_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                   rvc_full_mask_o,
    output ghist_info_t                                 ghist_info_o,
    output logic                                        misalign_o,
    output tage_typedef::composed_tage_pred_meta_t      tage_meta_o,
    output btb_typedef::btb_meta_t                      btb_meta_o,
    output ubtb_typedef::ubtb_meta_t                    ubtb_meta_o,
    output bht_typedef::bht_meta_t                      bht_meta_o,
    output ras_typedef::ras_meta_t                      ras_meta_o,
    output ras_typedef::ras_pred_t                      ras_pred_o,
    output logic [PC_HIB_WIDTH-1:0]                     pc_hib_o,
    output logic [PC_LOB_WIDTH-1:0]                     pc_lob_o,
    output logic [PC_LOB_WIDTH-1:0]                     tail_pc_lob_o,
    output logic [VADDR_WIDTH-1:0]                      target_o,
    output logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0] cfi_offsets_o,
    output logic [PREDS_PER_LINE-1:0]                   cfi_takens_o,
    output logic [PREDS_PER_LINE-1:0]                   cfi_valids_o,
    output logic                                        is_truncate_o,
    output logic                                        can_deq_o,
    output logic                                        no_commit_o,

    // invalidate
    input logic                                         fetch_invalidate_i,

    // kill 
    input logic                                         kill_i,

    // commit
    input logic                                         commit_valid_i,

    // dealloc
    input logic                                         dealloc_valid_i,

`ifndef SYNTHESIS
    input logic                                         is_commit_redirect_i,
    output logic                                        is_excp_o,
`endif 
    
    input logic                                         clk,
    input logic                                         rst
);

    logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]  cfi_offsets_q;
    logic [PREDS_PER_LINE-1:0]                              cfi_takens_q;
    logic [PREDS_PER_LINE-1:0]                              cfi_valids_q;
    logic                                                   is_truncate_q;
    logic [FETCH_WIDTH/16-1:0]                              br_full_mask_q;
    logic [FETCH_WIDTH/16-1:0]                              call_full_mask_q;
    logic [FETCH_WIDTH/16-1:0]                              ret_full_mask_q;
    logic [FETCH_WIDTH/16-1:0]                              cfi_full_mask_q;
    logic [FETCH_WIDTH/16-1:0]                              rvc_full_mask_q;
    logic [PC_LOB_WIDTH-1:0]                                tail_pc_lob_q;
    ghist_info_t                                            ghist_info_q;
    logic                                                   misalign_q;
    tage_typedef::composed_tage_pred_meta_t                 tage_meta_q;
    btb_typedef::btb_meta_t                                 btb_meta_q;
    ubtb_typedef::ubtb_meta_t                               ubtb_meta_q;
    bht_typedef::bht_meta_t                                 bht_meta_q;
    ras_typedef::ras_meta_t                                 ras_meta_q;
    ras_typedef::ras_pred_t                                 ras_pred_q;
    logic [PC_HIB_WIDTH-1:0]                                pc_hib_q;
    logic [PC_LOB_WIDTH-1:0]                                pc_lob_q;
    logic [VADDR_WIDTH-1:0]                                 target_q;
    logic                                                   no_commit_q;
    logic                                                   excp_q;

    logic                                                   valid_q;
    logic                                                   can_deq_q;

    logic [PREDS_PER_LINE-1:0]                              be_redirect_hit_mask;
    logic                                                   be_redirect_hit;
    logic [$clog2(PREDS_PER_LINE)-1:0]                      be_redirect_hit_idx;

    always_comb begin
        if (be_redirect_tail_pc_lob_i[PC_LOB_WIDTH-1] != pc_hib_q[0]) begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                be_redirect_hit_mask[i] = cfi_offsets_q[i] == 0;
            end
        end
        else begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                be_redirect_hit_mask[i] = cfi_offsets_q[i] == be_redirect_tail_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1];
            end
        end
    end

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_cfi_mask_prio_encoder (
        .sel_i(be_redirect_hit_mask),
        .id_vld_o(be_redirect_hit),
        .id_o(be_redirect_hit_idx)
    );

    always_ff @(posedge clk) begin
        if (rst) begin
            valid_q <= 0;
            excp_q <= 0;
        end
        else begin
            // alloc: b2 stage
            if (alloc_valid_i) begin
                ghist_info_q <= alloc_ghist_info_i;
                ubtb_meta_q <= alloc_ubtb_meta_i;
                bht_meta_q <= alloc_bht_meta_i;
                tage_meta_q <= alloc_tage_meta_i;
                btb_meta_q <= alloc_btb_meta_i;
                ras_meta_q <= alloc_ras_meta_i;
                ras_pred_q <= alloc_ras_pred_i;
                can_deq_q <= 0;
                valid_q <= 1;
                excp_q <= 0;
                no_commit_q <= 0;
                cfi_takens_q <= 0;
                cfi_valids_q <= 0;
                cfi_offsets_q <= 0;
                is_truncate_q <= 0;
            end
            // f3: fill
            if (fetch_valid_i) begin
                ras_pred_q <= fetch_ras_pred_i;
                pc_hib_q <= fetch_pc_i[VADDR_WIDTH-1-:PC_HIB_WIDTH];
                pc_lob_q <= fetch_pc_i[0+:PC_LOB_WIDTH];
                tail_pc_lob_q <= fetch_tail_pc_lob_i;
                target_q <= fetch_target_i;
                cfi_full_mask_q <= fetch_cfi_full_mask_i;
                rvc_full_mask_q <= fetch_rvc_full_mask_i;
                call_full_mask_q <= fetch_call_full_mask_i;
                ret_full_mask_q <= fetch_ret_full_mask_i;
                br_full_mask_q <= fetch_br_full_mask_i;
                cfi_takens_q <= fetch_cfi_takens_i;
                cfi_valids_q <= fetch_cfi_valids_i;
                cfi_offsets_q <= fetch_cfi_offsets_i;
                is_truncate_q <= fetch_cfi_is_truncate_i;
            end

            // be redirect
            if (be_redirect_valid_i) begin
`ifndef SYNTHESIS
                if (~be_redirect_hit & ~is_commit_redirect_i) $fatal("be redirect taken insn (tail_pc) is not hit");
`endif
                if (be_redirect_hit) begin
                    cfi_takens_q[be_redirect_hit_idx] <= be_redirect_taken_i;
                end
                for (int i = 0; i < PREDS_PER_LINE; i++) begin
                    if (i > be_redirect_hit_idx) cfi_valids_q[i] <= 0;
                end
                tail_pc_lob_q <= be_redirect_tail_pc_lob_i;
                target_q <= be_redirect_target_i;
                if (excp_q) begin
                    can_deq_q <= 1'b1;
                end
            end
            
            if (fetch_invalidate_i) begin
                no_commit_q <= 1;
                can_deq_q <= 1'b1;
            end

            if (commit_valid_i) begin
                if (~excp_q) begin
                    can_deq_q <= 1'b1;
                end
            end
            if (mark_excp_i) begin
                excp_q <= 1;
            end
            if (kill_i) begin
                valid_q <= 0;
                excp_q <= 0;
            end

            if (dealloc_valid_i) begin
                valid_q <= 0;
                excp_q <= 0;
            end
        end
    end

    assign valid_o = valid_q;
    assign br_full_mask_o = br_full_mask_q;
    assign call_full_mask_o = call_full_mask_q;
    assign ret_full_mask_o = ret_full_mask_q;
    assign cfi_full_mask_o = cfi_full_mask_q;
    assign rvc_full_mask_o = rvc_full_mask_q;
    assign ghist_info_o = ghist_info_q;
    assign misalign_o = misalign_q;
    assign tage_meta_o = tage_meta_q;
    assign btb_meta_o = btb_meta_q;
    assign ubtb_meta_o = ubtb_meta_q;
    assign bht_meta_o = bht_meta_q;
    assign ras_meta_o = ras_meta_q;
    assign ras_pred_o = ras_pred_q;
    assign pc_hib_o = pc_hib_q;
    assign pc_lob_o = pc_lob_q;
    assign tail_pc_lob_o = tail_pc_lob_q;
    assign target_o = target_q;
    assign cfi_takens_o = cfi_takens_q;
    assign cfi_valids_o = cfi_valids_q;
    assign can_deq_o = can_deq_q;
    assign no_commit_o = no_commit_q;
    assign cfi_offsets_o = cfi_offsets_q;
    assign is_truncate_o = is_truncate_q;
`ifndef SYNTHESIS
    assign is_excp_o = excp_q;
`endif 

`ifndef SYNTHESIS 
    always_ff @(posedge clk) begin
        if (~rst) begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                if (fetch_valid_i & fetch_cfi_valids_i[i] & ~(fetch_cfi_full_mask_i[fetch_cfi_offsets_i[i]])) begin
                    $fatal("taken insn (tail_pc) is not a cfi");
                end
            end
            if (be_redirect_tail_pc_lob_i[PC_LOB_WIDTH-1] != pc_hib_q[0]) begin
                if (be_redirect_valid_i & ~is_commit_redirect_i & ~(cfi_full_mask_q[0])) begin
                    $fatal("be redirect taken insn (tail_pc) is not a cfi");
                end
            end
            else begin
                if (be_redirect_valid_i & ~is_commit_redirect_i & ~(cfi_full_mask_q[be_redirect_tail_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]])) begin
                    $fatal("be redirect taken insn (tail_pc) is not a cfi");
                end
            end
        end
    end
`endif

endmodule
