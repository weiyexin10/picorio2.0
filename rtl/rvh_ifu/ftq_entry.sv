module ftq_entry
    import rvh_pkg::*;
    import riscv_pkg::*;
#() (
    // allocate
    input logic                                     alloc_valid_i,
    // input logic                                     alloc_is_bp_fix_i,
    input logic [VADDR_WIDTH-1:0]                   alloc_pc_i,
    input logic                                     alloc_req_two_blk_i,
    input logic                                     alloc_taken_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        alloc_taken_offset_i,

    // lvl1 BP fix
    input logic                                     lvl1_bp_fix_valid_i,
    input logic                                     lvl1_bp_fix_req_two_blk_i,
    input logic                                     lvl1_bp_fix_taken_i,
    input logic                                     lvl1_bp_fix_truncate_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        lvl1_bp_fix_taken_truncate_offset_i,

    input logic                                     lvl2_bp_valid_i,
    input logic                                     lvl2_bp_fix_valid_i,
    input logic                                     lvl2_bp_fix_req_two_blk_i,
    input logic                                     lvl2_bp_fix_taken_i,
    input logic                                     lvl2_bp_fix_truncate_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        lvl2_bp_fix_taken_truncate_offset_i,

    // btq info
    input logic                                     btq_alloc_valid_i,
    input logic [BTQ_TAG_WIDTH-1:0]                 btq_alloc_tag_i,
    
    // Deallocate
    input  logic                                    dealloc_vld_i,

    // fetch
    input  logic                                    fetch_vld_i,

    // replay
    input  logic                                    replay_vld_i,

    // excp
    input  logic                                    excp_vld_i,

    // pre-fetch
    input logic                                     selected_i,
    input logic                                     in_ic_i,
    input logic                                     in_pfb_i,
    input logic                                     pf_replay_i,

    output logic                                    valid_o,
    output logic [VADDR_WIDTH-1:0]                  pc_o,
    output logic                                    pf_done_o,
    output logic                                    fetch_sent_o,
    output logic                                    excp_vld_o,
    output logic                                    in_ic_o,
    output logic                                    in_pfb_o,
    output logic                                    req_two_blk_o,
    output logic                                    is_taken_o,
    output logic                                    is_truncate_o,
    output logic [$clog2(FETCH_WIDTH/16)-1:0]       taken_truncate_offset_o,
    output logic [BTQ_TAG_WIDTH-1:0]                btq_tag_o,
    
    // backend redirect
    input logic                                     flush_i,

    input logic                                     clk,
    input logic                                     rst
);

    logic                                           valid_q;
    logic                                           req_two_blk_q;
    logic                                           pf_done_q;
    logic                                           fetch_sent_q;
    logic                                           in_ic_q;
    logic                                           in_pfb_q;
    logic [VADDR_WIDTH-1:0]                         pc_q;
    logic                                           is_taken_q;
    logic                                           is_truncate_q;
    logic [$clog2(FETCH_WIDTH/16)-1:0]              taken_offset_q;
    logic [BTQ_TAG_WIDTH-1:0]                       btq_tag_q;
    logic                                           excp_q;

    // always_ff @(posedge clk) begin
    //     if (rst) begin
    //         pf_done_q <= 0;
    //         fetch_sent_q <= 0;
    //         in_ic_q <= 0;
    //         in_pfb_q <= 0;
    //         valid_q <= 0;
    //         req_two_blk_q <= 0;
    //         is_taken_q <= 0;
    //     end
    //     else if (flush_i) begin
    //         pf_done_q <= 0;
    //         fetch_sent_q <= 0;
    //         in_ic_q <= 0;
    //         in_pfb_q <= 0;
    //         valid_q <= 0;
    //         req_two_blk_q <= 0;
    //         is_taken_q <= 0;
    //     end
    //     else begin
    //         if (pf_replay_i) begin
    //             pf_done_q <= 0;
    //         end
    //         if (alloc_valid_i) begin
    //             pf_done_q <= 0;
    //             fetch_sent_q <= 0;
    //             valid_q <= 1;
    //             is_taken_q <= alloc_taken_i;
    //             taken_offset_q <= alloc_taken_offset_i;
    //             req_two_blk_q <= alloc_req_two_blk_i;
    //             pc_q <= alloc_pc_i;
    //         end
    //         if (lvl1_bp_fix_valid_i) begin
    //             pf_done_q <= 0;
    //             fetch_sent_q <= 0;
    //             valid_q <= 1;
    //             is_taken_q <= lvl1_bp_fix_taken_i;
    //             taken_offset_q <= lvl1_bp_fix_taken_offset_i;
    //             req_two_blk_q <= lvl1_bp_fix_req_two_blk_i;
    //         end
    //         if (lvl2_bp_fix_valid_i) begin
    //             pf_done_q <= 0;
    //             fetch_sent_q <= 0;
    //             valid_q <= 1;
    //             is_taken_q <= lvl2_bp_fix_taken_i;
    //             taken_offset_q <= lvl2_bp_fix_taken_offset_i;
    //             req_two_blk_q <= lvl2_bp_fix_req_two_blk_i;
    //         end
    //         if (dealloc_vld_i) begin
    //             pf_done_q <= 0;
    //             fetch_sent_q <= 0;
    //             valid_q <= 0;
    //             req_two_blk_q <= 0;
    //             is_taken_q <= 0;
    //         end
    //         if (selected_i) begin
    //             pf_done_q <= 1;
    //         end
    //         if (fetch_vld_i) begin
    //             fetch_sent_q <= 1;
    //         end
    //         if (replay_vld_i) begin
    //             fetch_sent_q <= 0;
    //         end
    //         if (btq_alloc_valid_i) begin
    //             btq_tag_q <= btq_alloc_tag_i;
    //         end
    //     end
    // end

    always_ff @(posedge clk) begin
        if (rst) begin
            pf_done_q <= 0;
            fetch_sent_q <= 0;
            in_ic_q <= 0;
            in_pfb_q <= 0;
            valid_q <= 0;
            req_two_blk_q <= 0;
            is_taken_q <= 0;
            is_truncate_q <= 0;
            excp_q <= 0;
        end
        else begin
            if (lvl1_bp_fix_valid_i) begin
                is_taken_q <= lvl1_bp_fix_taken_i;
                is_truncate_q <= lvl1_bp_fix_truncate_i;
                taken_offset_q <= lvl1_bp_fix_taken_truncate_offset_i;
                req_two_blk_q <= lvl1_bp_fix_req_two_blk_i;
            end
            if (lvl2_bp_fix_valid_i) begin
                is_taken_q <= lvl2_bp_fix_taken_i;
                is_truncate_q <= lvl2_bp_fix_truncate_i;
                taken_offset_q <= lvl2_bp_fix_taken_truncate_offset_i;
                req_two_blk_q <= lvl2_bp_fix_req_two_blk_i;
            end
            else if (lvl2_bp_valid_i) begin
                taken_offset_q <= lvl2_bp_fix_taken_truncate_offset_i;
            end
            if (pf_replay_i) begin
                pf_done_q <= 0;
            end
            if (selected_i) begin
                pf_done_q <= 1;
            end
            if (fetch_vld_i) begin
                fetch_sent_q <= 1;
            end
            if (replay_vld_i) begin
                fetch_sent_q <= 0;
            end
            if (btq_alloc_valid_i) begin
                btq_tag_q <= btq_alloc_tag_i;
            end
            if (excp_vld_i) begin
                excp_q <= 1;
            end
            if (dealloc_vld_i) begin
                pf_done_q <= 0;
                fetch_sent_q <= 0;
                valid_q <= 0;
                req_two_blk_q <= 0;
                is_taken_q <= 0;
                is_truncate_q <= 0;
                excp_q <= 0;
            end
            if (flush_i) begin
                pf_done_q <= 0;
                fetch_sent_q <= 0;
                in_ic_q <= 0;
                in_pfb_q <= 0;
                valid_q <= 0;
                req_two_blk_q <= 0;
                is_taken_q <= 0;
                is_truncate_q <= 0;
                excp_q <= 0;
            end
            if (alloc_valid_i) begin
                pf_done_q <= 0;
                fetch_sent_q <= 0;
                valid_q <= 1;
                is_taken_q <= alloc_taken_i;
                is_truncate_q <= 0;
                taken_offset_q <= alloc_taken_offset_i;
                req_two_blk_q <= alloc_req_two_blk_i;
                pc_q <= alloc_pc_i;
                excp_q <= 0;
            end
        end
    end


    // output
    assign valid_o = valid_q;
    assign pc_o = pc_q;
    assign pf_done_o = pf_done_q;
    assign in_ic_o = in_ic_q;
    assign in_pfb_o = in_pfb_q;
    assign req_two_blk_o = req_two_blk_q;
    assign is_taken_o = is_taken_q;
    assign is_truncate_o = is_truncate_q;
    assign taken_truncate_offset_o = taken_offset_q;
    assign fetch_sent_o = fetch_sent_q;
    assign btq_tag_o = btq_tag_q;
    assign excp_vld_o = excp_q;
endmodule
