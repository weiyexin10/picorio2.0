module btq
import rvh_pkg::*;
#() (
    
    // enqueue
    // branch predictor pipeline, allocate entry
    //  must guarantee that the first one is older
    // b1 stage: pre-check if btq has room for fetch package in both b2 & b1 stage 
    input logic [1:0]                                   precheck_valid_banks_b0_i,
    input logic [1:0]                                   precheck_valid_banks_b1_i,
    output logic                                        precheck_ready_o,
    // b2 stage
    input logic [1:0]                                   alloc_valid_banks_i,
    input ghist_info_t [1:0]                            alloc_ghist_info_banks_i,
    input ubtb_typedef::ubtb_meta_t[1:0]                alloc_ubtb_meta_banks_i,
    input bht_typedef::bht_meta_t[1:0]                  alloc_bht_meta_banks_i,
    input tage_typedef::composed_tage_pred_meta_t[1:0]  alloc_tage_meta_banks_i,
    input btb_typedef::btb_meta_t[1:0]                  alloc_btb_meta_banks_i,
    input ras_typedef::ras_pred_t[1:0]                  alloc_ras_pred_banks_i,
    input ras_typedef::ras_meta_t[1:0]                  alloc_ras_meta_banks_i,
    output logic [1:0][BTQ_TAG_WIDTH-1:0]               alloc_tag_banks_o,
    output logic                                        alloc_ready_o,
    
    // fetch stage
    input logic [1:0]                                   fetch_valid_banks_i,
    input logic [1:0][BTQ_TAG_WIDTH-1:0]                fetch_btq_tag_banks_i,
    input logic [1:0][VADDR_WIDTH-1:0]                  fetch_pc_banks_i,
    input logic [1:0][PC_LOB_WIDTH-1:0]                 fetch_tail_pc_lob_banks_i,
    input logic [1:0][VADDR_WIDTH-1:0]                  fetch_target_banks_i,
    input logic [1:0][FETCH_WIDTH/16-1:0]               fetch_cfi_mask_banks_i,
    input logic [1:0][FETCH_WIDTH/16-1:0]               fetch_rvc_mask_banks_i,
    input logic [1:0][FETCH_WIDTH/16-1:0]               fetch_call_mask_banks_i,
    input logic [1:0][FETCH_WIDTH/16-1:0]               fetch_ret_mask_banks_i,
    input logic [1:0][FETCH_WIDTH/16-1:0]               fetch_br_mask_banks_i,
    input logic [1:0][PREDS_PER_LINE-1:0]               fetch_cfi_taken_vec_banks_i,
    input logic [1:0][PREDS_PER_LINE-1:0]               fetch_cfi_valid_vec_banks_i,
    input logic [1:0]                                   fetch_is_truncate_banks_i,
    input logic [1:0][PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]fetch_cfi_offset_vec_banks_i,
    input loop_typedef::loop_meta_t[1:0]                fetch_loop_meta_banks_i,
    input ras_typedef::ras_pred_t[1:0]                  fetch_ras_pred_banks_i,
    

    output ras_typedef::ras_pred_t[1:0]                 fetch_ras_pred_banks_o,
    output ras_typedef::ras_meta_t[1:0]                 fetch_ras_meta_banks_o,
    output ghist_info_t [1:0]                           fetch_ghist_info_banks_o,
    output logic [1:0]                                  fetch_next_ghist_info_vld_banks_o,
    output ghist_info_t [1:0]                           fetch_next_ghist_info_banks_o,
    
    // f4 read info
    input logic [DECODE_WIDTH-1:0][BTQ_TAG_WIDTH-1:0]                       ibuf_btq_tag_i,
    input logic                                                             ibuf_excp_valid_i,
    output logic [DECODE_WIDTH-1:0][PC_HIB_WIDTH-1:0]                       btq_ibuf_pc_hib_o,
    output logic [DECODE_WIDTH-1:0][VADDR_WIDTH-1:0]                        btq_ibuf_pred_target_o,

    // f3 redirect
    input logic                                                             f3_redirect_vld_i,
    input logic [BTQ_TAG_WIDTH-1:0]                                         f3_redirect_btq_tag_i,
    input logic                                                             f3_invalid_vld_i,
    input logic [BTQ_TAG_WIDTH-1:0]                                         f3_invalid_btq_tag_i,

    // be redirect
    input logic                                                             be_redirect_valid_i,
    input logic [PC_LOB_WIDTH-1:0]                                          be_redirect_pc_lob_i,
    input logic [BTQ_TAG_WIDTH-1:0]                                         be_redirect_btq_tag_i,
    input logic [BRU_COUNT-1:0]                                             be_redirect_taken_i,
    input logic [     VADDR_WIDTH-1:0]                                      be_redirect_target_i,

    output logic                                                            be_redirect_valid_o,
    output logic [PC_LOB_WIDTH-1:0]                                         be_redirect_pc_lob_o,
    output logic [FETCH_WIDTH/16-1:0]                                       be_redirect_br_full_mask_o,
    output logic                                                            be_redirect_is_misalign_insn_o,
    output ras_typedef::ras_pred_t                                          be_redirect_ras_pred_o,
    output ras_typedef::ras_meta_t                                          be_redirect_ras_meta_o,
    output loop_typedef::loop_meta_t                                        be_redirect_loop_meta_o,
    output ghist_info_t                                                     be_redirect_ghist_info_o,
    output logic [     VADDR_WIDTH-1:0]                                     be_redirect_target_o,
    output logic                                                            be_redirect_taken_o,
    

    // cmt > btq
    input logic [RETIRE_WIDTH-1:0]                                          cmt_valid_i,
    input logic [RETIRE_WIDTH-1:0][BTQ_TAG_WIDTH-1:0]                       cmt_btq_tag_i,
    input logic [RETIRE_WIDTH-1:0][PC_LOB_WIDTH-1:0]                        cmt_pc_lob_i,
    

    // btq > bpu
    output logic                                                            bpu_update_valid_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_pred_valid_o,
    output logic                                   [   VADDR_WIDTH-1:0]     bpu_update_req_pc_o,
    output logic                                   [   VADDR_WIDTH-1:0]     bpu_update_target_o,
    output logic                                                            bpu_update_is_misalign_o,

    output tage_typedef::composed_tage_pred_meta_t                          bpu_update_tage_meta_o,
    output btb_typedef::btb_meta_t                                          bpu_update_btb_meta_o,
    output ubtb_typedef::ubtb_meta_t                                        bpu_update_ubtb_meta_o,
    output bht_typedef::bht_meta_t                                          bpu_update_bht_meta_o,
    output loop_typedef::loop_meta_t                                        bpu_update_loop_meta_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_cfi_mask_o,
    output logic                                                            bpu_update_truncate_o,
    output logic       [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]     bpu_update_cfi_offset_vec_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_rvc_mask_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_cfi_taken_vec_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_call_mask_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_ret_mask_o,
    output logic                                   [PREDS_PER_LINE-1:0]     bpu_update_br_mask_o,
    output ghist_info_t                                                     bpu_update_ghist_info_o,
    
`ifndef SYNTHESIS
    input logic                                                             is_commit_redirect_i,
`endif 
    input logic flush_i,

    input logic clk,
    input logic rst
);
    localparam HWORDS_PER_PRED = FETCH_WIDTH/16/PREDS_PER_LINE;

    logic [BTQ_TAG_WIDTH:0]                                           avail_cnt;
    logic [BTQ_TAG_WIDTH-1:0]                                         head;
    logic [1:0][BTQ_TAG_WIDTH-1:0]                                    tails;
    logic                                                             alloc_fire, dealloc_fire;
    logic [1:0]                                                       alloc_mask;

    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_valid;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_cfi_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_br_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_call_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_ret_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_rvc_full_mask;
    ghist_info_t [BTQ_ENTRY_COUNT-1:0]                                entry_ghist_info;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_misalign;
    tage_typedef::composed_tage_pred_meta_t [BTQ_ENTRY_COUNT-1:0]     entry_tage_meta;
    btb_typedef::btb_meta_t [BTQ_ENTRY_COUNT-1:0]                     entry_btb_meta;
    ubtb_typedef::ubtb_meta_t [BTQ_ENTRY_COUNT-1:0]                   entry_ubtb_meta;
    bht_typedef::bht_meta_t [BTQ_ENTRY_COUNT-1:0]                     entry_bht_meta;
    ras_typedef::ras_meta_t [BTQ_ENTRY_COUNT-1:0]                     entry_ras_meta;
    ras_typedef::ras_pred_t [BTQ_ENTRY_COUNT-1:0]                     entry_ras_pred;
    logic [BTQ_ENTRY_COUNT-1:0][PC_HIB_WIDTH-1:0]                     entry_pc_hib;
    logic [BTQ_ENTRY_COUNT-1:0][PC_LOB_WIDTH-1:0]                     entry_pc_lob;
    logic [BTQ_ENTRY_COUNT-1:0][PC_LOB_WIDTH-1:0]                     entry_tail_pc_lob;
    loop_typedef::loop_meta_t [BTQ_ENTRY_COUNT-1:0]                   entry_loop_meta;
    logic [BTQ_ENTRY_COUNT-1:0][VADDR_WIDTH-1:0]                      entry_target;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_can_deq;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_no_commit;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_mark_excp;
    logic [BTQ_ENTRY_COUNT-1:0][PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0] entry_cfi_offset;
    logic [BTQ_ENTRY_COUNT-1:0][PREDS_PER_LINE-1:0]                             entry_cfi_taken;
    logic [BTQ_ENTRY_COUNT-1:0][PREDS_PER_LINE-1:0]                             entry_cfi_valid;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_is_truncate;
`ifndef SYNTHESIS
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_is_excp;
`endif 

    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_alloc_valid;
    ghist_info_t [BTQ_ENTRY_COUNT-1:0]                                entry_alloc_ghist_info;
    ubtb_typedef::ubtb_meta_t [BTQ_ENTRY_COUNT-1:0]                   entry_alloc_ubtb_meta;
    bht_typedef::bht_meta_t [BTQ_ENTRY_COUNT-1:0]                     entry_alloc_bht_meta;
    tage_typedef::composed_tage_pred_meta_t [BTQ_ENTRY_COUNT-1:0]     entry_alloc_tage_meta;
    btb_typedef::btb_meta_t [BTQ_ENTRY_COUNT-1:0]                     entry_alloc_btb_meta;
    ras_typedef::ras_meta_t [BTQ_ENTRY_COUNT-1:0]                     entry_alloc_ras_meta;
    ras_typedef::ras_pred_t [BTQ_ENTRY_COUNT-1:0]                     entry_alloc_ras_pred;

    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_fetch_valid;
    logic [BTQ_ENTRY_COUNT-1:0][VADDR_WIDTH-1:0]                      entry_fetch_pc;
    logic [BTQ_ENTRY_COUNT-1:0][PC_LOB_WIDTH-1:0]                     entry_fetch_tail_pc_lob;
    logic [BTQ_ENTRY_COUNT-1:0][VADDR_WIDTH-1:0]                      entry_fetch_target;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_fetch_cfi_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_fetch_rvc_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_fetch_call_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_fetch_ret_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][FETCH_WIDTH/16-1:0]                   entry_fetch_br_full_mask;
    logic [BTQ_ENTRY_COUNT-1:0][PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]  entry_fetch_cfi_offset;
    logic [BTQ_ENTRY_COUNT-1:0][PREDS_PER_LINE-1:0]                   entry_fetch_cfi_taken;
    logic [BTQ_ENTRY_COUNT-1:0][PREDS_PER_LINE-1:0]                   entry_fetch_cfi_valid;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_fetch_is_truncate;
    
    ras_typedef::ras_pred_t [BTQ_ENTRY_COUNT-1:0]                     entry_fetch_ras_pred;
    loop_typedef::loop_meta_t [BTQ_ENTRY_COUNT-1:0]                   entry_fetch_loop_meta;

    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_redirect_valid;
    logic [BTQ_ENTRY_COUNT-1:0][PC_LOB_WIDTH-1:0]                     entry_redirect_tail_pc_lob;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_redirect_taken;
    logic [BTQ_ENTRY_COUNT-1:0][VADDR_WIDTH-1:0]                      entry_redirect_target;

    logic                                                             kill_after_valid;
    logic [BTQ_TAG_WIDTH-1:0]                                         kill_after_btq_tag;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_kill_valid;

    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_invalid;
    logic [BTQ_ENTRY_COUNT-1:0]                                       entry_commit_valid;

    logic [3:0]                                                       precheck_alloc_cnt;

    assign alloc_fire = |alloc_valid_banks_i & alloc_ready_o & ~kill_after_valid;
    assign alloc_mask = alloc_fire ? alloc_valid_banks_i : 0;

    assign dealloc_fire = entry_valid[head] & entry_can_deq[head];


    always_comb begin
        entry_alloc_valid = 0;
        entry_alloc_ghist_info = 0;
        entry_alloc_ubtb_meta = 0;
        entry_alloc_bht_meta = 0;
        entry_alloc_tage_meta = 0;
        entry_alloc_btb_meta = 0;
        entry_alloc_ras_meta = 0;
        entry_alloc_ras_pred = 0;
        for (int i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            if (|alloc_mask & (i == tails[0])) begin
                entry_alloc_valid[i] = 1;
                entry_alloc_ghist_info[i] = alloc_ghist_info_banks_i[0];
                entry_alloc_ubtb_meta[i] = alloc_ubtb_meta_banks_i[0];
                entry_alloc_bht_meta[i] = alloc_bht_meta_banks_i[0];
                entry_alloc_tage_meta[i] = alloc_tage_meta_banks_i[0];
                entry_alloc_btb_meta[i] = alloc_btb_meta_banks_i[0];
                entry_alloc_ras_meta[i] = alloc_ras_meta_banks_i[0];
                entry_alloc_ras_pred[i] = alloc_ras_pred_banks_i[0];
            end
            if (&alloc_mask & (i == tails[1])) begin
                entry_alloc_valid[i] = 1;
                entry_alloc_ghist_info[i] = alloc_ghist_info_banks_i[1];
                entry_alloc_ubtb_meta[i] = alloc_ubtb_meta_banks_i[1];
                entry_alloc_bht_meta[i] = alloc_bht_meta_banks_i[1];
                entry_alloc_tage_meta[i] = alloc_tage_meta_banks_i[1];
                entry_alloc_btb_meta[i] = alloc_btb_meta_banks_i[1];
                entry_alloc_ras_meta[i] = alloc_ras_meta_banks_i[1];
                entry_alloc_ras_pred[i] = alloc_ras_pred_banks_i[1];
            end
        end
    end

    always_comb begin
        entry_fetch_valid = 0;
        entry_fetch_pc = 0;
        entry_fetch_tail_pc_lob = 0;
        entry_fetch_target = 0;
        entry_fetch_cfi_full_mask = 0;
        entry_fetch_rvc_full_mask = 0;
        entry_fetch_call_full_mask = 0;
        entry_fetch_ret_full_mask = 0;
        entry_fetch_br_full_mask = 0;
        entry_fetch_loop_meta = 0;
        entry_fetch_ras_pred = 0;
        entry_fetch_cfi_offset = 0;
        entry_fetch_cfi_taken = 0;
        entry_fetch_cfi_valid = 0;
        entry_fetch_is_truncate = 0;
        for (int i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            if (fetch_valid_banks_i[0] & (fetch_btq_tag_banks_i[0] == i)) begin
                entry_fetch_valid[i] = 1;
                entry_fetch_pc[i] = fetch_pc_banks_i[0];
                entry_fetch_tail_pc_lob[i] = fetch_tail_pc_lob_banks_i[0];
                entry_fetch_target[i] = fetch_target_banks_i[0];
                entry_fetch_br_full_mask[i] = fetch_br_mask_banks_i[0];
                entry_fetch_cfi_full_mask[i] = fetch_cfi_mask_banks_i[0];
                entry_fetch_rvc_full_mask[i] = fetch_rvc_mask_banks_i[0];
                entry_fetch_call_full_mask[i] = fetch_call_mask_banks_i[0];
                entry_fetch_ret_full_mask[i] = fetch_ret_mask_banks_i[0];
                entry_fetch_loop_meta[i] = fetch_loop_meta_banks_i[0];
                entry_fetch_ras_pred[i] = fetch_ras_pred_banks_i[0];
                entry_fetch_cfi_offset[i] = fetch_cfi_offset_vec_banks_i[0];
                entry_fetch_cfi_taken[i] = fetch_cfi_taken_vec_banks_i[0];
                entry_fetch_cfi_valid[i] = fetch_cfi_valid_vec_banks_i[0];
                entry_fetch_is_truncate[i] = fetch_is_truncate_banks_i[0];
            end
            if (fetch_valid_banks_i[1] & (fetch_btq_tag_banks_i[1] == i)) begin
                entry_fetch_valid[i] = 1;
                entry_fetch_pc[i] = fetch_pc_banks_i[1];
                entry_fetch_tail_pc_lob[i] = fetch_tail_pc_lob_banks_i[1];
                entry_fetch_target[i] = fetch_target_banks_i[1];
                entry_fetch_cfi_full_mask[i] = fetch_cfi_mask_banks_i[1];
                entry_fetch_br_full_mask[i] = fetch_br_mask_banks_i[1];
                entry_fetch_rvc_full_mask[i] = fetch_rvc_mask_banks_i[1];
                entry_fetch_call_full_mask[i] = fetch_call_mask_banks_i[1];
                entry_fetch_ret_full_mask[i] = fetch_ret_mask_banks_i[1];
                entry_fetch_loop_meta[i] = fetch_loop_meta_banks_i[1];
                entry_fetch_ras_pred[i] = fetch_ras_pred_banks_i[1];
                entry_fetch_cfi_offset[i] = fetch_cfi_offset_vec_banks_i[1];
                entry_fetch_cfi_taken[i] = fetch_cfi_taken_vec_banks_i[1];
                entry_fetch_cfi_valid[i] = fetch_cfi_valid_vec_banks_i[1];
                entry_fetch_is_truncate[i] = fetch_is_truncate_banks_i[1];
            end
        end
    end

    always_comb begin
        entry_redirect_valid = 0;
        entry_redirect_tail_pc_lob = 0;
        entry_redirect_taken = 0;
        entry_redirect_target = 0;
        for (int i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            if (be_redirect_valid_i & (be_redirect_btq_tag_i == i)) begin
                entry_redirect_valid[i] = 1;
                entry_redirect_tail_pc_lob[i] = be_redirect_pc_lob_i;
                entry_redirect_taken[i] = be_redirect_taken_i;
                entry_redirect_target[i] = be_redirect_target_i;
            end
        end
    end


    
    assign kill_after_valid = be_redirect_valid_i | f3_redirect_vld_i;
    always_comb begin
        if (be_redirect_valid_i) begin
            kill_after_btq_tag = be_redirect_btq_tag_i;
        end
        else begin
            kill_after_btq_tag = f3_redirect_btq_tag_i;
        end
    end

    always_comb begin
        entry_kill_valid = 0;
        for (int i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            if (kill_after_valid) begin
                if (head == tails[0]) begin
                    if (avail_cnt == BTQ_ENTRY_COUNT) begin
                        entry_kill_valid[i] = 0;
                    end
                    else if (kill_after_btq_tag >= head) begin
                        entry_kill_valid[i] = (i < tails[0]) | (i > kill_after_btq_tag);
                    end
                    else begin
                        entry_kill_valid[i] = (i < tails[0]) & (i > kill_after_btq_tag);
                    end
                end
                else if (head < tails[0]) begin
                    entry_kill_valid[i] = (i < tails[0]) & (i > kill_after_btq_tag);
                end
                else begin
                    if (kill_after_btq_tag >= head) begin
                        entry_kill_valid[i] = (i < tails[0]) | (i > kill_after_btq_tag);
                    end
                    else begin
                        entry_kill_valid[i] = (i < tails[0]) & (i > kill_after_btq_tag);
                    end
                end
            end
        end
    end

    // commit
    always_comb begin
        entry_commit_valid = 0;
        for (int i = 0; i < RETIRE_WIDTH; i++) begin
            if (cmt_valid_i[i]) begin
                if (cmt_pc_lob_i[i] == entry_tail_pc_lob[cmt_btq_tag_i[i]]) begin
                    entry_commit_valid[cmt_btq_tag_i[i]] = 1'b1;
                end
            end
        end
    end

    // invalidate
    always_comb begin
        entry_invalid = 0;
        for (int i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            if (f3_invalid_vld_i & (f3_invalid_btq_tag_i == i)) begin
                entry_invalid[i] = 1;
            end
        end
    end

    usage_manager_with_kill #(
        .ENTRY_COUNT(BTQ_ENTRY_COUNT),
        .ENQ_WIDTH(2),
        .DEQ_WIDTH(1),
        .INIT_IS_FULL(0)
    ) u_btq_usage_manager (
        .enq_fire_i(alloc_mask),
        .deq_fire_i(dealloc_fire),
        .kill_after_valid_i(kill_after_valid),
        .kill_after_tag_i(kill_after_btq_tag),
        .head_o(head),
        .tail_o(tails),
        .avail_cnt_o(avail_cnt),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    generate
        for (genvar i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            btq_entry u_btq_entry(
                // alloc
                .alloc_valid_i              (entry_alloc_valid[i]),
                .alloc_ghist_info_i         (entry_alloc_ghist_info[i]),
                .alloc_ubtb_meta_i          (entry_alloc_ubtb_meta[i]),
                .alloc_bht_meta_i           (entry_alloc_bht_meta[i]),
                .alloc_tage_meta_i          (entry_alloc_tage_meta[i]),
                .alloc_btb_meta_i           (entry_alloc_btb_meta[i]),
                .alloc_ras_meta_i           (entry_alloc_ras_meta[i]),
                .alloc_ras_pred_i           (entry_alloc_ras_pred[i]),

                // f3 fill: from pre-decoder
                .fetch_valid_i                 (entry_fetch_valid[i]),
                .fetch_pc_i                    (entry_fetch_pc[i]),
                .fetch_tail_pc_lob_i           (entry_fetch_tail_pc_lob[i]),
                .fetch_target_i                (entry_fetch_target[i]),
                .fetch_cfi_full_mask_i         (entry_fetch_cfi_full_mask[i]),
                .fetch_rvc_full_mask_i         (entry_fetch_rvc_full_mask[i]),
                .fetch_call_full_mask_i        (entry_fetch_call_full_mask[i]),
                .fetch_ret_full_mask_i         (entry_fetch_ret_full_mask[i]),
                .fetch_br_full_mask_i          (entry_fetch_br_full_mask[i]),
                .fetch_ras_pred_i              (entry_fetch_ras_pred[i]),
                .fetch_cfi_offsets_i           (entry_fetch_cfi_offset[i]),
                .fetch_cfi_takens_i            (entry_fetch_cfi_taken[i]),
                .fetch_cfi_valids_i            (entry_fetch_cfi_valid[i]),
                .fetch_cfi_is_truncate_i       (entry_fetch_is_truncate[i]),
                .mark_excp_i                   (entry_mark_excp[i]),
                // be redirect
                .be_redirect_valid_i            (entry_redirect_valid[i]),
                .be_redirect_taken_i            (entry_redirect_taken[i]),
                .be_redirect_tail_pc_lob_i      (entry_redirect_tail_pc_lob[i]),
                .be_redirect_target_i           (entry_redirect_target[i]),
                // output 
                .valid_o                    (entry_valid[i]),
                .br_full_mask_o             (entry_br_full_mask[i]),
                .call_full_mask_o           (entry_call_full_mask[i]),
                .ret_full_mask_o            (entry_ret_full_mask[i]),
                .cfi_full_mask_o            (entry_cfi_full_mask[i]),
                .rvc_full_mask_o            (entry_rvc_full_mask[i]),
                .ghist_info_o               (entry_ghist_info[i]),
                .misalign_o                 (entry_misalign[i]),
                .tage_meta_o                (entry_tage_meta[i]),
                .btb_meta_o                 (entry_btb_meta[i]),
                .ubtb_meta_o                (entry_ubtb_meta[i]),
                .bht_meta_o                 (entry_bht_meta[i]),
                .ras_meta_o                 (entry_ras_meta[i]),
                .ras_pred_o                 (entry_ras_pred[i]),
                .pc_hib_o                   (entry_pc_hib[i]),
                .pc_lob_o                   (entry_pc_lob[i]),
                .tail_pc_lob_o              (entry_tail_pc_lob[i]),
                // .loop_meta_o                (entry_loop_meta[i]),
                .target_o                   (entry_target[i]),
                .can_deq_o                  (entry_can_deq[i]),
                .no_commit_o                (entry_no_commit[i]),
                .cfi_offsets_o              (entry_cfi_offset[i]),
                .cfi_takens_o               (entry_cfi_taken[i]),
                .cfi_valids_o               (entry_cfi_valid[i]),
                .is_truncate_o              (entry_is_truncate[i]),

                // commit
                .commit_valid_i             (entry_commit_valid[i]),
                .fetch_invalidate_i         (entry_invalid[i]),
                // kill
                .kill_i                     (entry_kill_valid[i]),
                // dealloc
                .dealloc_valid_i            (dealloc_fire & (i == head)),
                
`ifndef SYNTHESIS
                .is_commit_redirect_i       (is_commit_redirect_i),
                .is_excp_o                  (entry_is_excp[i]),
`endif 
                .clk                        (clk),
                .rst                        (rst)
            );
        end
    endgenerate

    assign alloc_ready_o = &alloc_valid_banks_i ? avail_cnt >= 2 : avail_cnt >= 1;
    assign alloc_tag_banks_o = tails;

    // intf to f4
    always_comb begin
        for (int i = 0; i < DECODE_WIDTH; i++) begin
            btq_ibuf_pc_hib_o[i] = entry_pc_hib[ibuf_btq_tag_i[i]];
            btq_ibuf_pred_target_o[i] = entry_target[ibuf_btq_tag_i[i]];
        end
    end
    // mark excp in f4
    always_comb begin
        entry_mark_excp = 0;
        for (int i = 0; i < BTQ_ENTRY_COUNT; i++) begin
            if (ibuf_excp_valid_i & (ibuf_btq_tag_i[0] == i)) begin
                entry_mark_excp[i] = 1;
            end
        end
    end
    
    generate
        for (genvar bank = 0; bank < 2; bank++) begin
            assign fetch_ghist_info_banks_o[bank] = entry_ghist_info[fetch_btq_tag_banks_i[bank]];
            assign fetch_ras_pred_banks_o[bank] = entry_ras_pred[fetch_btq_tag_banks_i[bank]];
            assign fetch_ras_meta_banks_o[bank] = entry_ras_meta[fetch_btq_tag_banks_i[bank]];
            assign fetch_next_ghist_info_vld_banks_o[bank] = ((fetch_btq_tag_banks_i[bank] + 1'b1) != tails[0]) & (avail_cnt != BTQ_ENTRY_COUNT);
            assign fetch_next_ghist_info_banks_o[bank] = entry_ghist_info[fetch_btq_tag_banks_i[bank] + 1'b1];
        end
    endgenerate

    // redirect
    assign be_redirect_valid_o = be_redirect_valid_i;
    assign be_redirect_target_o = be_redirect_target_i;
    assign be_redirect_ghist_info_o = entry_ghist_info[be_redirect_btq_tag_i];
    assign be_redirect_ras_meta_o = entry_ras_meta[be_redirect_btq_tag_i];
    assign be_redirect_ras_pred_o = entry_ras_pred[be_redirect_btq_tag_i];
    assign be_redirect_pc_lob_o = be_redirect_pc_lob_i;
    assign be_redirect_taken_o = be_redirect_taken_i;
    assign be_redirect_is_misalign_insn_o = (&be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]) & (be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)] != entry_pc_lob[be_redirect_btq_tag_i][$clog2(FETCH_WIDTH/8)]);
    always_comb begin
        for (int i = 0; i < FETCH_WIDTH/16; i++) begin
            be_redirect_br_full_mask_o[i] = entry_br_full_mask[be_redirect_btq_tag_i][i] & (i <= be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]);
        end
    end

    // update
    always_comb begin
        for (int i = 0; i < PREDS_PER_LINE; i++) begin
            bpu_update_cfi_mask_o[i] = entry_cfi_full_mask[head][entry_cfi_offset[head][i]];
            bpu_update_br_mask_o[i] = entry_br_full_mask[head][entry_cfi_offset[head][i]];
            bpu_update_rvc_mask_o[i] = entry_rvc_full_mask[head][entry_cfi_offset[head][i]];
            bpu_update_call_mask_o[i] = entry_call_full_mask[head][entry_cfi_offset[head][i]];
            bpu_update_ret_mask_o[i] = entry_ret_full_mask[head][entry_cfi_offset[head][i]];
        end
    end

    assign bpu_update_req_pc_o = {entry_pc_hib[head], entry_pc_lob[head][$clog2(FETCH_WIDTH/8)-1:0]};
    assign bpu_update_valid_o = dealloc_fire & ~entry_no_commit[head];
    assign bpu_update_truncate_o = entry_is_truncate[head];
    assign bpu_update_target_o = entry_target[head];
    assign bpu_update_tage_meta_o = entry_tage_meta[head];
    assign bpu_update_btb_meta_o = entry_btb_meta[head];
    assign bpu_update_ubtb_meta_o = entry_ubtb_meta[head];
    assign bpu_update_bht_meta_o = entry_bht_meta[head];
    assign bpu_update_loop_meta_o = entry_loop_meta[head];
    assign bpu_update_cfi_offset_vec_o = entry_cfi_offset[head];
    assign bpu_update_cfi_taken_vec_o = entry_cfi_taken[head];
    assign bpu_update_pred_valid_o = entry_cfi_valid[head];
    assign bpu_update_ghist_info_o = entry_ghist_info[head];
    assign bpu_update_is_misalign_o = entry_pc_hib[head][0] != entry_tail_pc_lob[head][$clog2(FETCH_WIDTH/8)];

    // precheck
    one_counter #(
        .DATA_WIDTH(6)
    ) precheck_counter (
        .data_i({precheck_valid_banks_b0_i, precheck_valid_banks_b1_i, alloc_valid_banks_i}),
        .cnt_o(precheck_alloc_cnt)
    );
    assign precheck_ready_o = precheck_alloc_cnt <= avail_cnt; 

    // assertion
`ifndef SYNTHESIS
    logic [BTQ_TAG_WIDTH:0]     usage_cnt;
    one_counter #(
        .DATA_WIDTH(BTQ_ENTRY_COUNT)
    ) usage_counter (
        .data_i(entry_valid),
        .cnt_o(usage_cnt)
    );

    always_ff @(posedge clk) begin
        if (!rst) begin
            if (usage_cnt + avail_cnt != BTQ_ENTRY_COUNT) $fatal("btq usage_manager is not correspond to entry usage");
            if (&bpu_update_pred_valid_o & bpu_update_truncate_o & (entry_tail_pc_lob[head][$clog2(FETCH_WIDTH/8)-1:1] != entry_cfi_offset[head][PREDS_PER_LINE-1])) begin
                $fatal("entry tail is not equal to the last pred branch when br_mask is all set");
            end
            if (be_redirect_valid_i & ~entry_is_excp[be_redirect_btq_tag_i] & (entry_pc_hib[be_redirect_btq_tag_i][0] == be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)]) & (be_redirect_pc_lob_i > entry_tail_pc_lob[be_redirect_btq_tag_i])) begin
                $fatal("the branch address from be is beyound the scope of btq entry");
            end
            if (be_redirect_valid_i & ~is_commit_redirect_i) begin
                if (entry_pc_hib[be_redirect_btq_tag_i][0] != be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)]) begin
                    if (~entry_cfi_full_mask[be_redirect_btq_tag_i][0]) begin
                        $fatal("the taken address is not a cfi");
                    end
                end
                else begin
                    if (~entry_cfi_full_mask[be_redirect_btq_tag_i][be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]]) begin
                        $fatal("the taken address is not a cfi");
                    end
                end
            end
        end
    end
`endif
    

endmodule
