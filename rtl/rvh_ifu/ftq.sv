module ftq
    import rvh_pkg::*;
    import riscv_pkg::*;
#() (
    // allocate in lvl0
    input logic                                     enq_valid_i, 
    input logic [VADDR_WIDTH-1:0]                   enq_pc_i,
    input logic                                     enq_req_two_blk_i,
    input logic                                     enq_is_taken_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        enq_taken_offset_i,
    output logic [FTQ_TAG_LEN-1:0]                  enq_tag_o,
    output logic                                    enq_ready_o,

    // redirect by level 1 BP
    input logic                                     lvl1_bp_fix_valid_i,
    input logic                                     lvl1_bp_fix_req_two_blk_i,
    input logic [FTQ_TAG_LEN-1:0]                   lvl1_bp_fix_tag_i,
    input logic                                     lvl1_bp_fix_is_taken_i,
    input logic                                     lvl1_bp_fix_is_truncate_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        lvl1_bp_fix_taken_truncate_offset_i,
    // redirect by level 2 BP
    input logic                                     lvl2_bp_valid_i,
    input logic                                     lvl2_bp_fix_valid_i,
    input logic                                     lvl2_bp_fix_req_two_blk_i,
    input logic [FTQ_TAG_LEN-1:0]                   lvl2_bp_fix_tag_i,
    input logic                                     lvl2_bp_fix_is_taken_i,
    input logic                                     lvl2_bp_fix_is_truncate_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        lvl2_bp_fix_taken_truncate_offset_i,
    // input logic [VADDR_WIDTH-1:0]                   bp_fix_pc_i,

    // record btq info in level 2
    input logic                                     btq_alloc_valid_i,
    input logic [FTQ_TAG_LEN-1:0]                   btq_alloc_ftq_tag_i,
    input logic [BTQ_TAG_WIDTH-1:0]                 btq_alloc_btq_tag_i,

    // prefetch
    output logic                                    prefetch_req_valid_o,
    output logic [VADDR_WIDTH-1:0]                  prefetch_req_pc_o,
    output logic [FTQ_TAG_LEN-1:0]                  prefetch_req_tag_o,
    input logic                                     prefetch_req_tlb_hit_i,
    input logic                                     prefetch_req_tlb_ready_i,
    input logic                                     prefetch_req_in_ic_i,
    input logic                                     prefetch_req_in_pfb_i,
    input logic                                     prefetch_req_ready_i,

    // fetch
    input logic                                     fetch_req_ready_i,
    output logic                                    fetch_req_valid_o,
    output logic [VADDR_WIDTH-1:0]                  fetch_req_pc_o,
    output logic [FTQ_TAG_LEN-1:0]                  fetch_req_tag_o,
    input logic                                     fetch_req_tlb_hit_i,
    input logic                                     fetch_req_tlb_ready_i,
    input logic                                     fetch_req_tlb_excp_vld_i,
    input logic [EXCP_CAUSE_WIDTH-1:0]              fetch_req_tlb_excp_cause_i,
    input logic                                     fetch_req_ic_probe_hit_i,   // hit in cycle 1, also means pre-respond
    // fetch resp
    input logic                                     fetch_resp_valid_i,
    input logic                                     fetch_resp_replay_i,
    input logic [FTQ_TAG_LEN-1:0]                   fetch_resp_tag_i,

    output logic                                    fetch_resp_valid_o,
    output logic                                    fetch_resp_excp_valid_o,
    output logic [    EXCP_TVAL_WIDTH-1:0]          fetch_resp_excp_tval_o,
    output logic [   EXCP_CAUSE_WIDTH-1:0]          fetch_resp_excp_cause_o,
    output logic                                    fetch_resp_is_pred_taken_o,
    output logic                                    fetch_resp_is_pred_truncate_o,
    output logic [$clog2(FETCH_WIDTH/16)-1:0]       fetch_resp_taken_truncate_offset_o,
    output logic                                    fetch_resp_two_blks_o,
    output logic [VADDR_WIDTH-1:0]                  fetch_resp_pc_o,
    output logic                                    fetch_resp_next_valid_o,
    output logic [VADDR_WIDTH-1:0]                  fetch_resp_next_pc_o,
    output logic [BTQ_TAG_WIDTH-1:0]                fetch_resp_btq_tag_o,

    // backend redirect
    input logic                                     fe_flush_i,
    input logic                                     be_flush_i,

    input logic                                     clk,
    input logic                                     rst
);

    enum logic[1:0]{
        IDLE,
        REPLAY,
        WAIT_RESP,
        EXCP
    } pf_req_state, next_pf_req_state, fetch_req_state, next_fetch_req_state, last_fetch_req_state;


    logic [FTQ_ENTRY_COUNT-1:0]                     entry_alloc_valid;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_alloc_is_taken;
    logic [FTQ_ENTRY_COUNT-1:0][$clog2(FETCH_WIDTH/16)-1:0] entry_alloc_taken_offset;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_alloc_req_two_blks;
    logic [FTQ_ENTRY_COUNT-1:0][VADDR_WIDTH-1:0]    entry_alloc_pc;

    logic [FTQ_ENTRY_COUNT-1:0]                     lvl1_bp_fix_valid;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl1_bp_fix_is_taken;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl1_bp_fix_is_truncate;
    logic [FTQ_ENTRY_COUNT-1:0][$clog2(FETCH_WIDTH/16)-1:0] lvl1_bp_fix_taken_truncate_offset;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl1_bp_fix_req_two_blks;

    logic [FTQ_ENTRY_COUNT-1:0]                     lvl2_bp_valid;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl2_bp_fix_valid;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl2_bp_fix_is_taken;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl2_bp_fix_is_truncate;
    logic [FTQ_ENTRY_COUNT-1:0][$clog2(FETCH_WIDTH/16)-1:0] lvl2_bp_fix_taken_truncate_offset;
    logic [FTQ_ENTRY_COUNT-1:0]                     lvl2_bp_fix_req_two_blks;

    logic [FTQ_ENTRY_COUNT-1:0]                     entry_replay;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_fetch_fire;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_excp_valid;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_valid, shifted_entry_valid;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_fetch_sent, shifted_entry_fetch_sent;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_dealloc_vld;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_pf_in_ic;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_pf_in_pfb;
    logic [FTQ_ENTRY_COUNT-1:0][VADDR_WIDTH-1:0]    entry_pf_pc;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_pf_done, shifted_entry_pf_done;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_req_two_blks;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_is_taken;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_is_truncate;
    logic [FTQ_ENTRY_COUNT-1:0][BTQ_TAG_WIDTH-1:0]  entry_btq_tag;
    logic [FTQ_ENTRY_COUNT-1:0]                     entry_flush;

    logic [FTQ_ENTRY_COUNT-1:0][$clog2(FETCH_WIDTH/16)-1:0] entry_taken_truncate_offset;
    
    logic [FTQ_TAG_LEN-1:0]                         head, head_plus_1, tail, tail_ff;
    logic [FTQ_TAG_LEN-1:0]                         shifted_prefetch_tag, shifted_fetch_tag;
    logic [FTQ_TAG_LEN-1:0]                         selected_fetch_tag_d, selected_fetch_tag_q;
    logic [FTQ_TAG_LEN-1:0]                         excp_tag;
    logic                                           fetch_req_valid_d, fetch_req_valid_q;
    logic [VADDR_WIDTH-1:0]                         fetch_req_pc_d, fetch_req_pc_q;
//////////////////////to do: delete this signal and use fetch_req_valid_q/////////////////////////
    logic [FTQ_TAG_LEN-1:0]                         last_fired_fetch_tag;
/////////////////////////////////////////////////////////////////////////

    logic [FTQ_TAG_LEN-1:0]                         selected_prefetch_tag_d, selected_prefetch_tag_q;
    logic                                           prefetch_req_valid_d, prefetch_req_valid_q;
    logic [VADDR_WIDTH-1:0]                         prefetch_req_pc_d, prefetch_req_pc_q;
    logic [FTQ_TAG_LEN:0]                           avail_cnt;

    logic [FTQ_ENTRY_COUNT-1:0]                     shifted_fetch_ready_mask;
    logic                                           has_entry_ready_for_fetch;
    logic                                           has_entry_ready_for_prefetch;
    logic                                           alloc_fire;
    logic                                           prefetch_replay;
    logic                                           fetch_replay;
    logic                                           fetch_fire;
    logic                                           fetch_fire_q;
    logic                                           prefetch_fire;
    logic                                           prefetch_fire_q;

    logic                                           tlb_resp_excp_valid;
    logic [EXCP_CAUSE_WIDTH-1:0]                    tlb_excp_cause_q;


// generate alloc signal
    assign alloc_fire = enq_valid_i & enq_ready_o;
    assign enq_ready_o = (avail_cnt > 0) | fe_flush_i | be_flush_i | (lvl2_bp_fix_valid_i & lvl1_bp_fix_valid_i);
    assign enq_tag_o = tail;
    always_comb begin
        entry_alloc_valid = 0;
        entry_alloc_req_two_blks = 0;
        entry_alloc_pc = 0;
        entry_alloc_is_taken = 0;
        entry_alloc_taken_offset = 0;
        for (int i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            entry_alloc_valid[i] = alloc_fire & (i == tail);
            entry_alloc_pc[i] = enq_pc_i;
            entry_alloc_req_two_blks[i] = enq_req_two_blk_i;
            entry_alloc_is_taken[i] = enq_is_taken_i;
            entry_alloc_taken_offset[i] = enq_taken_offset_i;
        end
    end
// generate lvl1 bp fix signal
    always_comb begin
        lvl1_bp_fix_valid = 0;
        lvl1_bp_fix_is_taken = 0;
        lvl1_bp_fix_is_truncate = 0;
        lvl1_bp_fix_taken_truncate_offset = 0;
        lvl1_bp_fix_req_two_blks = 0;
        for (int i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            lvl1_bp_fix_valid[i] = lvl1_bp_fix_valid_i & (i == lvl1_bp_fix_tag_i);
            lvl1_bp_fix_req_two_blks[i] = lvl1_bp_fix_req_two_blk_i;
            lvl1_bp_fix_is_taken[i] = lvl1_bp_fix_is_taken_i;
            lvl1_bp_fix_is_truncate[i] = lvl1_bp_fix_is_truncate_i;
            lvl1_bp_fix_taken_truncate_offset[i] = lvl1_bp_fix_taken_truncate_offset_i;
        end
    end
// generate lvl1 bp fix signal
    always_comb begin
        lvl2_bp_valid = 0;
        lvl2_bp_fix_valid = 0;
        lvl2_bp_fix_is_taken = 0;
        lvl2_bp_fix_is_truncate = 0;
        lvl2_bp_fix_taken_truncate_offset = 0;
        lvl2_bp_fix_req_two_blks = 0;
        for (int i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            lvl2_bp_valid[i] = lvl2_bp_valid_i & (i == lvl2_bp_fix_tag_i);
            lvl2_bp_fix_valid[i] = lvl2_bp_fix_valid_i & (i == lvl2_bp_fix_tag_i);
            lvl2_bp_fix_req_two_blks[i] = lvl2_bp_fix_req_two_blk_i;
            lvl2_bp_fix_is_taken[i] = lvl2_bp_fix_is_taken_i;
            lvl2_bp_fix_is_truncate[i] = lvl2_bp_fix_is_truncate_i;
            lvl2_bp_fix_taken_truncate_offset[i] = lvl2_bp_fix_taken_truncate_offset_i;
        end
    end


// generate replay signal
    assign head_plus_1 = head + 1'b1;
    always_comb begin
        entry_replay = 0;
        for (int i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            entry_replay[i] = fetch_resp_valid_i & fetch_resp_replay_i & ((head == i) | (head_plus_1 == i));
            entry_replay[i] |= fetch_replay & (i == selected_fetch_tag_q);
        end
    end
    // generate fetch_fire signal
    always_comb begin
        entry_fetch_fire = 0;
        for (int i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            entry_fetch_fire[i] = fetch_fire & (i == selected_fetch_tag_d);
            entry_fetch_fire[i] |= (fetch_req_state == WAIT_RESP) & (next_fetch_req_state == IDLE) & (i == last_fired_fetch_tag);
        end
    end

// generate flush signal
    always_comb begin
        entry_flush = 0;
        for (int i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            entry_flush[i] = be_flush_i | (fe_flush_i & (head != i)) | (lvl2_bp_fix_valid_i & lvl1_bp_fix_valid_i & (i == lvl1_bp_fix_tag_i));
        end
    end
    

// prefetch
    cyclic_shifter #(
        .LENGTH(FTQ_ENTRY_COUNT),
        .SHIFT_LEFT(1)
    )u_entry_valid_shifter (
        .data_i(entry_valid),
        .shift_bits_i(head),
        .data_o(shifted_entry_valid)    
    );
    cyclic_shifter #(
        .LENGTH(FTQ_ENTRY_COUNT),
        .SHIFT_LEFT(1)
    )u_fetch_sent_shifter (
        .data_i(entry_fetch_sent),
        .shift_bits_i(head),
        .data_o(shifted_entry_fetch_sent)    
    );
    cyclic_shifter #(
        .LENGTH(FTQ_ENTRY_COUNT),
        .SHIFT_LEFT(1)
    )u_entry_pf_done_shifter (
        .data_i(entry_pf_done),
        .shift_bits_i(head),
        .data_o(shifted_entry_pf_done)    
    );
    one_hot_priority_encoder #(
        .SEL_WIDTH(FTQ_ENTRY_COUNT)
    ) u_fetch_mask_priority_encoder (
        .sel_i(shifted_entry_valid & ~shifted_entry_fetch_sent),
        .sel_o(shifted_fetch_ready_mask)
    );
    priority_encoder #(
        .SEL_WIDTH(FTQ_ENTRY_COUNT)
    )u_pf_selected_priority_encoder (
        .sel_i(shifted_entry_valid & ~shifted_entry_pf_done & ~shifted_entry_fetch_sent/* & ~shifted_fetch_ready_mask*/),
        .id_vld_o(has_entry_ready_for_prefetch),
        .id_o(shifted_prefetch_tag)
    );
    priority_encoder #(
        .SEL_WIDTH(FTQ_ENTRY_COUNT)
    )u_fetch_selected_priority_encoder (
        .sel_i(shifted_entry_valid & ~shifted_entry_fetch_sent),
        .id_vld_o(has_entry_ready_for_fetch),
        .id_o(shifted_fetch_tag)
    );
    assign selected_prefetch_tag_d = shifted_prefetch_tag + head;
    assign prefetch_req_valid_d = has_entry_ready_for_prefetch; // & ~((selected_prefetch_tag_d == lvl2_bp_fix_tag_i) & lvl2_bp_fix_valid_i);
    assign prefetch_req_pc_d = entry_pf_pc[selected_prefetch_tag_d];

    assign prefetch_replay = (pf_req_state == IDLE) & (next_pf_req_state == REPLAY);
    assign prefetch_fire = prefetch_req_valid_d & (next_pf_req_state == IDLE) & prefetch_req_ready_i;

    always_ff @(posedge clk) begin
        if (rst) begin
            selected_prefetch_tag_q <= 1'b0;
            prefetch_req_valid_q <= 1'b0;
        end
        else if (be_flush_i | fe_flush_i) begin
            selected_prefetch_tag_q <= 1'b0;
            prefetch_req_valid_q <= 1'b0;
        end
        else begin
            selected_prefetch_tag_q <= selected_prefetch_tag_d;
            prefetch_req_valid_q <= prefetch_req_valid_d;
            prefetch_req_pc_q <= prefetch_req_pc_d;
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            prefetch_fire_q <= 0;
        end
        else if (be_flush_i | fe_flush_i) begin
            prefetch_fire_q <= 0;
        end
        else begin
            prefetch_fire_q <= prefetch_fire;
        end
    end

    always_comb begin
        next_pf_req_state = pf_req_state;
        case (pf_req_state) 
            IDLE: begin
                if (prefetch_fire_q & ~prefetch_req_tlb_hit_i) begin
                    next_pf_req_state = REPLAY;
                end
            end
            REPLAY: begin
                if (prefetch_req_tlb_ready_i) begin
                    next_pf_req_state = IDLE;
                end
            end
            EXCP:;
            default:;
        endcase
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            pf_req_state <= IDLE;
        end
        else if (be_flush_i | fe_flush_i) begin
            pf_req_state <= IDLE;
        end
        else if (tlb_resp_excp_valid) begin
            pf_req_state <= EXCP;
        end
        else begin
            pf_req_state <= next_pf_req_state;
        end
    end

    assign prefetch_req_valid_o = ~fe_flush_i & ~be_flush_i & ~lvl2_bp_fix_valid_i & (pf_req_state != EXCP) & 
                                    (prefetch_replay ? prefetch_req_valid_q : prefetch_req_valid_d) &
                                    ~(fetch_req_valid_o & (prefetch_req_tag_o == fetch_req_tag_o));
    assign prefetch_req_pc_o = prefetch_replay ? prefetch_req_pc_q : prefetch_req_pc_d;
    assign prefetch_req_tag_o = prefetch_replay ? selected_prefetch_tag_q : selected_prefetch_tag_d;


// fetch
    assign selected_fetch_tag_d = head + shifted_fetch_tag;
    assign fetch_req_valid_d = has_entry_ready_for_fetch; // & ~((selected_fetch_tag_d == lvl2_bp_fix_tag_i) & lvl2_bp_fix_valid_i);
    assign fetch_req_pc_d = entry_pf_pc[selected_fetch_tag_d];


    always_ff @(posedge clk) begin
        if (rst) begin
            selected_fetch_tag_q <= 0;
            fetch_req_valid_q <= 1'b0;
        end
        else if (be_flush_i | fe_flush_i) begin
            selected_fetch_tag_q <= 0;
            fetch_req_valid_q <= 1'b0;
        end
        else begin
            selected_fetch_tag_q <= selected_fetch_tag_d;
            fetch_req_valid_q <= fetch_req_valid_d;
            fetch_req_pc_q <= fetch_req_pc_d;
        end
    end

//////////////////////to do: delete this signal and use fetch_req_valid_q/////////////////////////
    always_ff @(posedge clk) begin
        if (rst) begin
            last_fired_fetch_tag <= 0;
        end
        else if (be_flush_i | fe_flush_i) begin
            last_fired_fetch_tag <= 0;
        end
        else if (fetch_fire) begin
            last_fired_fetch_tag <= selected_fetch_tag_d;
        end
    end
/////////////////////////////////////////////////////////////////////////

    always_ff @(posedge clk) begin
        if (rst) begin
            fetch_fire_q <= 0;
        end
        else if (be_flush_i | fe_flush_i) begin
            fetch_fire_q <= 0;
        end
        else begin
            fetch_fire_q <= fetch_fire;
        end
    end

    always_comb begin
        next_fetch_req_state = fetch_req_state;
        case (fetch_req_state) 
            IDLE: begin
                if (fetch_fire_q & ~fetch_resp_replay_i) begin
                    if (~fetch_req_tlb_hit_i) begin
                        next_fetch_req_state = REPLAY;
                    end
                    else if (~fetch_req_ic_probe_hit_i) begin
                        next_fetch_req_state = WAIT_RESP;
                    end
                end
            end
            REPLAY: begin // 
                if (fetch_req_tlb_ready_i) begin
                    if (fetch_req_ic_probe_hit_i | lvl2_bp_fix_valid_i) begin
                        next_fetch_req_state = IDLE;
                    end
                    else begin
                        next_fetch_req_state = WAIT_RESP;
                    end
                end
            end
            WAIT_RESP: begin
                if (fetch_req_ic_probe_hit_i) begin
                    next_fetch_req_state = IDLE;
                end
            end
            EXCP:;
            default:;
        endcase
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            fetch_req_state <= IDLE;
        end
        else if (be_flush_i | fe_flush_i) begin
            fetch_req_state <= IDLE;
        end
        else if (tlb_resp_excp_valid) begin
            fetch_req_state <= EXCP;
        end
        else begin
            fetch_req_state <= next_fetch_req_state;
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            last_fetch_req_state <= IDLE;
        end
        else begin
            last_fetch_req_state <= fetch_req_state;
        end
    end

    priority_encoder #(
        .SEL_WIDTH(FTQ_ENTRY_COUNT)
    )u_excp_priority_encoder (
        .sel_i(entry_valid & entry_excp_valid),
        .id_vld_o( ),
        .id_o(excp_tag)
    );

    assign fetch_replay = (fetch_req_state == IDLE) & (next_fetch_req_state == REPLAY);
    assign fetch_fire = fetch_req_valid_d & ((next_fetch_req_state == IDLE) | (next_fetch_req_state == EXCP)) & fetch_req_ready_i & ~fetch_resp_replay_i;   

    assign fetch_req_valid_o = ~fe_flush_i & ~be_flush_i & ~lvl2_bp_fix_valid_i & 
                                ((fetch_req_state != EXCP) | (fetch_req_valid_d & (selected_fetch_tag_d == (excp_tag - 1'b1)))) & 
                                (fetch_req_state != WAIT_RESP) & 
                                (fetch_replay ? '0 : fetch_req_valid_d);
    assign fetch_req_pc_o =     fetch_req_pc_d; //fetch_replay ? fetch_req_pc_q : fetch_req_pc_d;
    assign fetch_req_tag_o =    selected_fetch_tag_d; //fetch_replay ? selected_fetch_tag_q : selected_fetch_tag_d;
    
    assign tlb_resp_excp_valid = ((last_fetch_req_state == REPLAY) | fetch_fire_q) & fetch_req_tlb_excp_vld_i;
    always_ff @(posedge clk)begin
        if (tlb_resp_excp_valid) begin
            tlb_excp_cause_q <= fetch_req_tlb_excp_cause_i;
        end
    end

    usage_manager_with_kill #(
        .ENTRY_COUNT(FTQ_ENTRY_COUNT),
        .ENQ_WIDTH(1),
        .DEQ_WIDTH(1),
        .INIT_IS_FULL(0),
        .FLUSH_KILL_DISABLE_ENQ(0),
        .COMB_TAIL_WHEN_FLUSH_KILL(1)
    ) u_ftq_usage_manager (
        .enq_fire_i(alloc_fire),
        .deq_fire_i(fetch_resp_valid_i & ~fetch_resp_replay_i),
        .kill_after_valid_i(lvl2_bp_fix_valid_i & lvl1_bp_fix_valid_i),
        .kill_after_tag_i(lvl2_bp_fix_tag_i),
        .head_o(head),
        .tail_o(tail),
        .tail_ff_o(tail_ff),
        .avail_cnt_o(avail_cnt),
        .flush_i(fe_flush_i | be_flush_i),
        .clk(clk),
        .rst(rst)
    );

    generate
        for (genvar i = 0; i < FTQ_ENTRY_COUNT; i++) begin
            ftq_entry u_ftq_entry (
                .alloc_valid_i              (entry_alloc_valid[i]), 
                .alloc_pc_i                 (entry_alloc_pc[i]),
                .alloc_req_two_blk_i        (entry_alloc_req_two_blks[i]),
                .alloc_taken_i              (entry_alloc_is_taken[i]),
                .alloc_taken_offset_i       (entry_alloc_taken_offset[i]),
                .lvl1_bp_fix_valid_i        (lvl1_bp_fix_valid[i]),
                .lvl1_bp_fix_req_two_blk_i  (lvl1_bp_fix_req_two_blks[i]),
                .lvl1_bp_fix_taken_i        (lvl1_bp_fix_is_taken[i]),
                .lvl1_bp_fix_truncate_i     (lvl1_bp_fix_is_truncate[i]),
                .lvl1_bp_fix_taken_truncate_offset_i (lvl1_bp_fix_taken_truncate_offset[i]),
                .lvl2_bp_valid_i            (lvl2_bp_valid[i]),
                .lvl2_bp_fix_valid_i        (lvl2_bp_fix_valid[i]),
                .lvl2_bp_fix_req_two_blk_i  (lvl2_bp_fix_req_two_blks[i]),
                .lvl2_bp_fix_taken_i        (lvl2_bp_fix_is_taken[i]),
                .lvl2_bp_fix_truncate_i     (lvl2_bp_fix_is_truncate[i]),
                .lvl2_bp_fix_taken_truncate_offset_i (lvl2_bp_fix_taken_truncate_offset[i]),
                .btq_alloc_valid_i          (btq_alloc_valid_i & (btq_alloc_ftq_tag_i == i)),
                .btq_alloc_tag_i            (btq_alloc_btq_tag_i),
                .dealloc_vld_i              (fetch_resp_valid_i & ~fetch_resp_replay_i & (i == head)),
                .fetch_vld_i                (entry_fetch_fire[i]),
                .replay_vld_i               (entry_replay[i]),
                .selected_i                 (prefetch_fire & (i == selected_prefetch_tag_d)),
                .in_ic_i                    ('0),
                .in_pfb_i                   ('0),
                .pf_replay_i                (prefetch_replay & (i == selected_prefetch_tag_q)),
                .excp_vld_i                 (tlb_resp_excp_valid & (i == last_fired_fetch_tag)),

                .valid_o                    (entry_valid[i]),
                .excp_vld_o                 (entry_excp_valid[i]),
                .pc_o                       (entry_pf_pc[i]),
                .pf_done_o                  (entry_pf_done[i]),
                .fetch_sent_o               (entry_fetch_sent[i]),
                .in_ic_o                    (entry_pf_in_ic[i]),
                .in_pfb_o                   (entry_pf_in_pfb[i]),
                .req_two_blk_o              (entry_req_two_blks[i]),
                .is_taken_o                 (entry_is_taken[i]),
                .is_truncate_o              (entry_is_truncate[i]),
                .taken_truncate_offset_o    (entry_taken_truncate_offset[i]),
                .btq_tag_o                  (entry_btq_tag[i]),
                .flush_i                    (entry_flush[i]),
                .clk                        (clk),
                .rst                        (rst)
            );
        end
    endgenerate
// fetch resp
    assign fetch_resp_is_pred_taken_o = entry_is_taken[head];
    assign fetch_resp_is_pred_truncate_o = entry_is_truncate[head];
    assign fetch_resp_taken_truncate_offset_o = entry_taken_truncate_offset[head];
    assign fetch_resp_two_blks_o = fetch_resp_valid_i & entry_valid[head] & entry_req_two_blks[head];
    assign fetch_resp_pc_o = entry_pf_pc[head];
    assign fetch_resp_next_valid_o = (head + 1'b1) != tail_ff;
    assign fetch_resp_next_pc_o = entry_pf_pc[head + 1'b1];
    assign fetch_resp_btq_tag_o = entry_btq_tag[head];
    assign fetch_resp_excp_valid_o = entry_excp_valid[head];
    assign fetch_resp_excp_tval_o = {{EXCP_TVAL_WIDTH-VADDR_WIDTH{fetch_resp_pc_o[VADDR_WIDTH-1]}}, fetch_resp_pc_o};
    assign fetch_resp_excp_cause_o = tlb_excp_cause_q;
    assign fetch_resp_valid_o = fetch_resp_valid_i & ~fetch_resp_excp_valid_o;

`ifndef SYNTHESIS
    logic [FTQ_TAG_LEN:0]                           occupy_cnt;
    one_counter #(
        .DATA_WIDTH(FTQ_ENTRY_COUNT)
    ) occupy_counter (
        .data_i(entry_valid),
        .cnt_o(occupy_cnt)
    );
    always_ff @(posedge clk) begin
        if (FTQ_ENTRY_COUNT - occupy_cnt != avail_cnt) begin
            $display("usage manager is incompatiable with entry");
            $stop();
        end
        if (fetch_resp_valid_i & (fetch_resp_tag_i != head)) begin
            $display("dequeue entry is not head entry of queue");
            $stop();
        end
        if (fetch_resp_valid_i & ~entry_valid[head]) begin
            $display("dequeue entry is not valid");
            $stop();
        end
        if (entry_valid[head] & fetch_resp_next_valid_o & ~entry_valid[head + 1'b1]) begin
            $display("entry between fetch_resp_tag_i tail is not valid");
            $stop();
        end
        if ((fetch_req_state == EXCP) & ~(|entry_excp_valid)) begin
            $fatal("no entry is marked excp while the state is EXCP");
        end
        if ((fetch_req_state != EXCP) & (|entry_excp_valid)) begin
            $fatal("there is entry marked excp while the state is not EXCP");
        end
    end
`endif
endmodule
