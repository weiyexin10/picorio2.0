module pre_decoder
import rvh_pkg::*;
#() (
    
    input logic                                     fpkt_vld_i,
    input logic [VADDR_WIDTH-1:0]                   fpkt_pc_i,
    input logic [$clog2(FETCH_WIDTH/16)-1:0]        fpkt_tail_offset_i,
    input ghist_info_t                              fpkt_ghist_info_i,
    input logic                                     fpkt_is_taken_i,
    input logic [FETCH_WIDTH-1:0]                   fpkt_cacheline_i,
    input logic [FETCH_WIDTH/16*2-1:0]              fpkt_boundary_i,
    input logic [FETCH_WIDTH/16*2-1:0]              fpkt_rvc_mask_i,
    input logic                                     fpkt_next_pc_vld_i,
    input logic [VADDR_WIDTH-1:0]                   fpkt_next_pc_i,

    input logic                                     fpkt_misalign_valid_i,
    input logic [15:0]                              fpkt_misalign_inst_i,
    
    input logic [VADDR_WIDTH-1:0]                   ras_target_i,

    // instruction fetch queue
    output logic                                     pdec_taken_o,
    output logic                                     pdec_truncate_o,
    output logic                                     pdec_call_taken_o,
    output logic                                     pdec_ret_taken_o,
    output logic [PC_LOB_WIDTH-1:0]                  pdec_tail_insn_pc_lob_o,
    output logic [VADDR_WIDTH-1:0]                   pdec_addr_next_to_cfi_o,
    output ghist_info_t                              pdec_pred_ghist_info_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_rvc_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_taken_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_jalr_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_jal_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_ret_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_call_mask_o,
    output logic [FETCH_WIDTH/16-1:0]                pdec_br_mask_o,
    output logic [VADDR_WIDTH-1:0]                   pdec_target_o,
    output logic                                     pdec_misalign_valid_o,
    output logic [15:0]                              pdec_misalign_inst_o,
    output logic [$clog2(FETCH_WIDTH/16)  :0]        pdec_hword_num_o,
    output ras_typedef::ras_pred_t                   pdec_ras_pred_o,
    output logic [PREDS_PER_LINE-1:0]                             pdec_cfi_valid_vec_o,
    output logic [PREDS_PER_LINE-1:0]                             pdec_cfi_taken_vec_o,
    output logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0] pdec_cfi_offset_vec_o,

    input logic                                     clk,
    input logic                                     rst
);

logic [$clog2(FETCH_WIDTH/16)-1:0]  cfi_offset,
                                    uncond_jmp_offset,
                                    branch_offset,
                                    jal_offset,
                                    return_offset,
                                    jalr_offset,
                                    call_offset;

logic                               has_uncond_jmp,
                                    has_branch,
                                    has_jal,
                                    has_return,
                                    has_jalr,
                                    has_call;

logic                               call_taken, 
                                    ret_taken, 
                                    jal_taken, 
                                    jalr_taken, 
                                    br_taken;

logic                               _pdec_is_truncate, pdec_is_truncate;
logic [$clog2(FETCH_WIDTH/16)-1:0]  truncate_offset;
logic                               _pdec_is_taken, pdec_is_taken;
logic [           VADDR_WIDTH-1:0]  pdec_taken_target;
logic [$clog2(FETCH_WIDTH/16)-1:0]  pdec_taken_offset;
logic [           VADDR_WIDTH-1:0]  pdec_next_pc;
logic [$clog2(FETCH_WIDTH/16)-1:0]  pdec_tail_offset;
logic [$clog2(FETCH_WIDTH/16)-1:0]  pdec_tail_insn_is_rvc;

logic [FETCH_WIDTH/16-1:0][VADDR_WIDTH-1:0] imms;

logic [      FETCH_WIDTH/16-1:0]    inst_border_mask;
logic [      FETCH_WIDTH/16-1:0]    inst_rvc_mask;
logic [FETCH_WIDTH/16-1:0]          cfi_mask;
logic [FETCH_WIDTH/16-1:0]          _jal_mask, jal_mask;
logic [FETCH_WIDTH/16-1:0]          _jalr_mask, jalr_mask;
logic [FETCH_WIDTH/16-1:0]          _return_mask, return_mask;
logic [FETCH_WIDTH/16-1:0]          _call_mask, call_mask;
logic [FETCH_WIDTH/16-1:0]          _branch_mask, branch_mask;
logic [FETCH_WIDTH/16-1:0][VADDR_WIDTH-1:0] jal_br_targets;
logic [VADDR_WIDTH-1:0]             misalign_insn_target;

logic [$clog2(FETCH_WIDTH/16):0]    hword_num;

logic                               misalign_taken;

logic                               saw_not_taken_br;
logic                               saw_taken_br;

logic [PREDS_PER_LINE-1:0]                                        cfi_exist_vec;
logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]            cfi_offset_vec;
logic [PREDS_PER_LINE-1:0][FETCH_WIDTH/16-1:0]                    cfi_mask_clear_lower_1_mat;

////////////////////////////////
function logic [$clog2(GHIST_CAPACITY)-1:0] calc_ghist_ptr(input logic [$clog2(GHIST_CAPACITY)-1:0] old_ptr, input logic saw_taken_br, input logic saw_not_taken_br);
begin
    calc_ghist_ptr = old_ptr;
    if (saw_taken_br | saw_not_taken_br) begin
        if (old_ptr == GHIST_CAPACITY - 1) begin
            calc_ghist_ptr = 0;
        end
        else begin
            calc_ghist_ptr = old_ptr + 1'b1;
        end
    end
end
endfunction

function ghist_info_t calc_ghist_info(input ghist_info_t old_info, input logic saw_taken_br, input logic saw_not_taken_br);
begin
    calc_ghist_info.tail_ptr = calc_ghist_ptr(old_info.tail_ptr, saw_taken_br, saw_not_taken_br);
    if (saw_taken_br | saw_not_taken_br) begin
        calc_ghist_info.lsb = saw_taken_br;
    end
    else begin
        calc_ghist_info.lsb = old_info.lsb;
    end
end
endfunction

//////////get insn border / rvc mask//////////
always_comb begin
    if (fpkt_misalign_valid_i) begin
        if (~fpkt_boundary_i[0]) begin
            inst_border_mask = fpkt_boundary_i[FETCH_WIDTH/16-1:0];
            inst_rvc_mask = fpkt_rvc_mask_i[FETCH_WIDTH/16-1:0];
        end
        else begin
            inst_border_mask = fpkt_boundary_i[FETCH_WIDTH/16*2-1:FETCH_WIDTH/16];
            inst_rvc_mask = fpkt_rvc_mask_i[FETCH_WIDTH/16*2-1:FETCH_WIDTH/16];
        end
    end
    else begin
        if (fpkt_boundary_i[fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]]) begin
            inst_border_mask = fpkt_boundary_i[FETCH_WIDTH/16-1:0];
            inst_rvc_mask = fpkt_rvc_mask_i[FETCH_WIDTH/16-1:0];
        end
        else begin
            inst_border_mask = fpkt_boundary_i[FETCH_WIDTH/16*2-1:FETCH_WIDTH/16];
            inst_rvc_mask = fpkt_rvc_mask_i[FETCH_WIDTH/16*2-1:FETCH_WIDTH/16];
        end
    end
end
/////////////calc taken info/////////////////

always_comb begin
    for (int i = 0; i < FETCH_WIDTH/16; i++) begin
        jal_br_targets[i] = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], i[$clog2(FETCH_WIDTH/16)-1:0], 1'b0} + imms[i[$clog2(FETCH_WIDTH/16)-1:0]];
    end
end

assign misalign_insn_target = jal_br_targets[0] - 2'd2;

priority_encoder
#(
    .SEL_WIDTH(FETCH_WIDTH/16)
) u_uncond_jmp_prio_encoder (
    .sel_i(jal_mask | return_mask | jalr_mask | call_mask),
    .id_vld_o(has_uncond_jmp),
    .id_o(uncond_jmp_offset)
);

priority_encoder
#(
    .SEL_WIDTH(FETCH_WIDTH/16)
) u_jal_prio_encoder (
    .sel_i(jal_mask),
    .id_vld_o(has_jal),
    .id_o(jal_offset)
);

priority_encoder
#(
    .SEL_WIDTH(FETCH_WIDTH/16)
) u_return_prio_encoder (
    .sel_i(return_mask),
    .id_vld_o(has_return),
    .id_o(return_offset)
);

priority_encoder
#(
    .SEL_WIDTH(FETCH_WIDTH/16)
) u_jalr_prio_encoder (
    .sel_i(jalr_mask),
    .id_vld_o(has_jalr),
    .id_o(jalr_offset)
);

priority_encoder
#(
    .SEL_WIDTH(FETCH_WIDTH/16)
) u_call_prio_encoder (
    .sel_i(call_mask),
    .id_vld_o(has_call),
    .id_o(call_offset)
);

priority_encoder
#(
    .SEL_WIDTH(FETCH_WIDTH/16)
) u_branch_prio_encoder (
    .sel_i(branch_mask),
    .id_vld_o(has_branch),
    .id_o(branch_offset)
);

always_comb begin
    _pdec_is_taken = 0;
    pdec_taken_target = 0;
    pdec_taken_offset   = 0;

    call_taken = 0;
    ret_taken = 0;
    jal_taken = 0;
    jalr_taken = 0;
    br_taken = 0;

    if (fpkt_vld_i) begin
        if (fpkt_is_taken_i & (~has_uncond_jmp | (uncond_jmp_offset > fpkt_tail_offset_i)) & branch_mask[fpkt_tail_offset_i]) begin
            pdec_taken_offset = fpkt_tail_offset_i;
            _pdec_is_taken = 1;
            br_taken = 1;
            if (fpkt_misalign_valid_i & (fpkt_tail_offset_i == 0)) begin
                pdec_taken_target = misalign_insn_target;
            end 
            else begin
                pdec_taken_target = jal_br_targets[pdec_taken_offset];
            end
        end 
        else if (has_uncond_jmp) begin
            pdec_taken_offset = uncond_jmp_offset;
            if (has_jal & (jal_offset == uncond_jmp_offset)) begin
                _pdec_is_taken = 1;
                jal_taken = 1;
                call_taken = has_call & (call_offset == uncond_jmp_offset);
                if (fpkt_misalign_valid_i & (uncond_jmp_offset == 0)) begin
                    pdec_taken_target = misalign_insn_target;
                end
                else begin
                    pdec_taken_target = jal_br_targets[pdec_taken_offset];  // jal's target
                end
            end 
            else if (has_return & (return_offset == uncond_jmp_offset)) begin
                _pdec_is_taken  = 1;
                ret_taken  = 1;
                pdec_taken_target = ras_target_i;
            end 
            else if (has_jalr & (jalr_offset == uncond_jmp_offset)) begin
                _pdec_is_taken = 1;
                jalr_taken = 1;
                call_taken = has_call & (call_offset == uncond_jmp_offset);
                pdec_taken_target = fpkt_next_pc_i;
            end
        end 
        else begin
            _pdec_is_taken = 0;
            pdec_taken_target = fpkt_next_pc_i;
            pdec_taken_offset =0;
        end
    end
end

////////////calc truncate info//////////////////
assign cfi_mask = jal_mask | return_mask | jalr_mask | call_mask | branch_mask;
always_comb begin
    cfi_mask_clear_lower_1_mat[0] = cfi_mask;
    for (int i = 1; i < PREDS_PER_LINE; i++) begin
        cfi_mask_clear_lower_1_mat[i] = cfi_mask_clear_lower_1_mat[i-1] & (cfi_mask_clear_lower_1_mat[i-1] - 1'b1);
    end
end

generate for (genvar i=0; i < PREDS_PER_LINE; i++) begin
    priority_encoder
    #(
        .SEL_WIDTH(FETCH_WIDTH/16)
    ) u_cfi_mask_prio_encoder (
        .sel_i(cfi_mask_clear_lower_1_mat[i]),
        .id_vld_o(cfi_exist_vec[i]),
        .id_o(cfi_offset_vec[i])
    );
end
endgenerate

assign _pdec_is_truncate = |(cfi_mask_clear_lower_1_mat[PREDS_PER_LINE-1] & (cfi_mask_clear_lower_1_mat[PREDS_PER_LINE-1] - 1'b1));
assign truncate_offset = cfi_offset_vec[PREDS_PER_LINE-1];

//////////////////////////////

always_comb begin
    if (_pdec_is_taken & _pdec_is_truncate) begin
        if (pdec_taken_offset <= truncate_offset) begin
            pdec_is_taken = 1;
            pdec_is_truncate = 0;
        end
        else begin
            pdec_is_taken = 0;
            pdec_is_truncate = 1;
            if (inst_rvc_mask[truncate_offset]) begin
                if (&truncate_offset) begin
                    pdec_is_truncate = 0;
                end
            end
            else if (&truncate_offset[$clog2(FETCH_WIDTH/16)-1:1]) begin
                pdec_is_truncate = 0;
            end
        end
    end
    else if (_pdec_is_taken) begin
        pdec_is_taken = 1;
        pdec_is_truncate = 0;
    end
    else if (_pdec_is_truncate) begin
        pdec_is_taken = 0;
        pdec_is_truncate = 1;
        if (inst_rvc_mask[truncate_offset]) begin
            if (&truncate_offset) begin
                pdec_is_truncate = 0;
            end
        end
        else if (&truncate_offset[$clog2(FETCH_WIDTH/16)-1:1]) begin
            pdec_is_truncate = 0;
        end
    end
    else begin
        pdec_is_taken = 0;
        pdec_is_truncate = 0;
    end
end

assign pdec_tail_offset = pdec_is_taken ? pdec_taken_offset : truncate_offset;
assign pdec_tail_insn_is_rvc = pdec_is_taken ? inst_rvc_mask[pdec_taken_offset] : inst_rvc_mask[truncate_offset];

always_comb begin
    if (pdec_is_taken) begin
        pdec_next_pc = pdec_taken_target;
    end
    else if (pdec_is_truncate) begin
        if (inst_rvc_mask[truncate_offset]) begin
            pdec_next_pc = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], truncate_offset + 1'b1, 1'b0};
        end
        else begin
            pdec_next_pc = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], truncate_offset + 2'd2, 1'b0};
        end
    end
    else begin
        pdec_next_pc = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
    end
end

// predecoders
predecode predecode_u_0 (
      .instr_i(fpkt_misalign_valid_i ? {fpkt_cacheline_i[15:0], fpkt_misalign_inst_i} : fpkt_cacheline_i[31:0]),
      .instr_is_rvc_i(inst_rvc_mask[0]),

      .is_jal_o(_jal_mask[0]),
      .is_jalr_o(_jalr_mask[0]),
      .is_return_o(_return_mask[0]),
      .is_call_o(_call_mask[0]),
      .is_branch_o(_branch_mask[0]),
      .imm_o(imms[0])
  );

generate
for (genvar i = 1; i < FETCH_WIDTH / 16 - 1; i++) begin : PRE_DECS
    predecode predecode_u (
        .instr_i(fpkt_cacheline_i[i*16+:32]),
        .instr_is_rvc_i(inst_rvc_mask[i]),

        .is_jal_o(_jal_mask[i]),
        .is_jalr_o(_jalr_mask[i]),
        .is_return_o(_return_mask[i]),
        .is_call_o(_call_mask[i]),
        .is_branch_o(_branch_mask[i]),
        .imm_o(imms[i])
    );
end
endgenerate

predecode predecode_u_last (
    .instr_i(pdec_misalign_valid_o ? 0 : {16'b0, fpkt_cacheline_i[(FETCH_WIDTH/16-1)*16+:16]}),
    .instr_is_rvc_i(inst_rvc_mask[FETCH_WIDTH/16-1]),

    .is_jal_o(_jal_mask[FETCH_WIDTH/16-1]),
    .is_jalr_o(_jalr_mask[FETCH_WIDTH/16-1]),
    .is_return_o(_return_mask[FETCH_WIDTH/16-1]),
    .is_call_o(_call_mask[FETCH_WIDTH/16-1]),
    .is_branch_o(_branch_mask[FETCH_WIDTH/16-1]),
    .imm_o(imms[FETCH_WIDTH/16-1])
);

always_comb begin
    for (int i = 0; i < FETCH_WIDTH / 16; i++) begin
        jal_mask[i] = _jal_mask[i] & (i >= fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) &
            inst_border_mask[i];
        jalr_mask[i] = _jalr_mask[i] & (i >= fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) &
            inst_border_mask[i];
        return_mask[i] = _return_mask[i] & (i >= fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) &
            inst_border_mask[i];
        call_mask[i] = _call_mask[i] & (i >= fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) &
            inst_border_mask[i];
        branch_mask[i] = _branch_mask[i] & (i >= fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) &
            inst_border_mask[i];

        if ((i == 0) & fpkt_misalign_valid_i) begin
            jal_mask[0] = _jal_mask[0] & (fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] == 0);
            jalr_mask[0] = _jalr_mask[0] & (fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] == 0);
            return_mask[0] = _return_mask[0] & (fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] == 0);
            call_mask[0] = _call_mask[0] & (fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] == 0);
            branch_mask[0] = _branch_mask[i] & (fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] == 0);
        end
    end
end

//////////////////////////////
assign misalign_taken = fpkt_misalign_valid_i & pdec_is_taken & (pdec_taken_offset == 0) & (~inst_rvc_mask[0]);

// calcuate hword_num
always_comb begin
    if (fpkt_misalign_valid_i) begin
        if (pdec_is_taken | pdec_is_truncate) begin
            if (pdec_tail_insn_is_rvc) begin
                hword_num = pdec_tail_offset - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] + 2;
            end 
            else begin
                if (pdec_tail_offset == 0) begin
                    hword_num = pdec_tail_offset - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] + 2;
                end
                else begin
                    hword_num = pdec_tail_offset - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] + 3;
                end
            end
        end 
        else begin
            if (pdec_misalign_valid_o) begin
                hword_num = ((1 << $clog2(FETCH_WIDTH / 8)) - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:0]) / 2;
            end
            else begin
                hword_num = ((1 << $clog2(FETCH_WIDTH / 8)) - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:0]) / 2 + 1;
            end
        end
    end 
    else begin
        if (pdec_is_taken | pdec_is_truncate) begin
            if (pdec_tail_insn_is_rvc) begin
                hword_num = pdec_tail_offset - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] + 1;
            end
            else begin
                hword_num = pdec_tail_offset - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] + 2;
            end
        end 
        else begin
            if (pdec_misalign_valid_o) begin
                hword_num = ((1 << $clog2(FETCH_WIDTH / 8)) - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:0]) / 2 - 1;
            end
            else begin
                hword_num = ((1 << $clog2(FETCH_WIDTH / 8)) - fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:0]) / 2;
            end
        end
    end
end

// global history
// compare pred from f1 with those from f2 to see if we need to redirect pc
always_comb begin
    if (pdec_is_taken | pdec_is_truncate) begin
        if (has_branch & (branch_offset <= pdec_tail_offset)) begin
            saw_not_taken_br = 1;
        end
        else begin
            saw_not_taken_br = 0;
        end
    end
    else begin
        saw_not_taken_br = has_branch;
    end
end

assign saw_taken_br = pdec_is_taken & branch_mask[pdec_taken_offset];  // whether the first taken insn is a branch

assign pdec_pred_ghist_info_o = calc_ghist_info(fpkt_ghist_info_i, saw_taken_br, saw_not_taken_br);

// output 
assign pdec_misalign_valid_o = inst_border_mask[FETCH_WIDTH/16-1] & (fpkt_cacheline_i[FETCH_WIDTH-15:FETCH_WIDTH-16] == 2'b11);

always_comb begin
    if (inst_rvc_mask[uncond_jmp_offset]) begin
        if (&uncond_jmp_offset) begin
            pdec_addr_next_to_cfi_o = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
        end
        else begin
            pdec_addr_next_to_cfi_o = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], uncond_jmp_offset + 1'b1, 1'b0};
        end
    end
    else begin
        if (&uncond_jmp_offset[$clog2(FETCH_WIDTH/16)-1:1]) begin
            if (uncond_jmp_offset[0]) begin
                pdec_addr_next_to_cfi_o = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {{$clog2(FETCH_WIDTH/8)-2'd2{1'b0}}, 2'b10}};
            end
            else begin
                pdec_addr_next_to_cfi_o = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
            end
        end
        else begin
            pdec_addr_next_to_cfi_o = {fpkt_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], uncond_jmp_offset + 2'd2, 1'b0};
        end
    end
end

always_comb begin
    if (misalign_taken) begin
        pdec_tail_insn_pc_lob_o = {~fpkt_pc_i[PC_LOB_WIDTH-1], {PC_LOB_WIDTH-2{1'b1}}, 1'b0};
    end 
    else if (pdec_is_taken | pdec_is_truncate) begin
        pdec_tail_insn_pc_lob_o = {fpkt_pc_i[$clog2(FETCH_WIDTH/8)], pdec_tail_offset, 1'b0};
    end 
    else if (pdec_misalign_valid_o) begin
        if (inst_rvc_mask[FETCH_WIDTH/16-2]) begin
            pdec_tail_insn_pc_lob_o = {fpkt_pc_i[$clog2(FETCH_WIDTH/8)], {($clog2(FETCH_WIDTH / 8) - 2) {1'b1}}, 2'b0};
        end 
        else begin
            pdec_tail_insn_pc_lob_o = {fpkt_pc_i[$clog2(FETCH_WIDTH/8)], {($clog2(FETCH_WIDTH / 8) - 3) {1'b1}}, 3'b010};
        end
    end 
    else if (inst_rvc_mask[FETCH_WIDTH/16-1]) begin
        pdec_tail_insn_pc_lob_o = {fpkt_pc_i[$clog2(FETCH_WIDTH/8)], {($clog2(FETCH_WIDTH / 8) - 1) {1'b1}}, 1'b0};
    end 
    else begin
        pdec_tail_insn_pc_lob_o = {fpkt_pc_i[$clog2(FETCH_WIDTH/8)], {($clog2(FETCH_WIDTH / 8) - 2) {1'b1}}, 2'b0};
    end
end


always_comb begin
    for (int i = 0; i < FETCH_WIDTH / 16; i++) begin
        pdec_taken_mask_o[i] = pdec_is_taken & (pdec_taken_offset == i) & (jal_mask[i] | jalr_mask[i] | return_mask[i] | call_mask[i] | branch_mask[i]);
    end
end

always_comb begin
    pdec_cfi_taken_vec_o = 0;
    pdec_cfi_valid_vec_o = cfi_exist_vec;
    if (pdec_is_taken) begin
        for (int i =0; i < PREDS_PER_LINE; i++) begin
            pdec_cfi_taken_vec_o[i] = pdec_taken_offset == cfi_offset_vec[i];
            // if (pdec_taken_offset < cfi_offset_vec[i]) pdec_cfi_valid_vec_o[i] = 0;
        end
    end
end

assign pdec_jalr_mask_o = jalr_mask;
assign pdec_jal_mask_o = jal_mask;
assign pdec_ret_mask_o = return_mask;
assign pdec_call_mask_o = call_mask;
assign pdec_br_mask_o = branch_mask;
assign pdec_cfi_offset_vec_o = cfi_offset_vec;
assign pdec_taken_o = pdec_is_taken;
assign pdec_truncate_o = pdec_is_truncate;
assign pdec_target_o = pdec_next_pc;
assign pdec_misalign_inst_o = fpkt_cacheline_i[FETCH_WIDTH-16+:16];
assign pdec_hword_num_o = hword_num;
assign pdec_rvc_mask_o = inst_rvc_mask;
assign pdec_call_taken_o = call_taken;
assign pdec_ret_taken_o = ret_taken;
assign pdec_ras_pred_o.is_call = call_taken;
assign pdec_ras_pred_o.is_ret = ret_taken;
assign pdec_ras_pred_o.call_ret_pc_lob = uncond_jmp_offset;
assign pdec_ras_pred_o.is_rvc_call_ret = (fpkt_misalign_valid_i & (uncond_jmp_offset == 0)) ? 1'b1 : inst_rvc_mask[uncond_jmp_offset];


`ifndef SYNTHESIS 
    always_ff @(posedge clk) begin
        if (~rst) begin
            if ((|pdec_cfi_taken_vec_o) ^ (|pdec_taken_mask_o)) begin
                $fatal("pdec_taken_mask_o is different with pdec_cfi_taken_vec_o");
            end
            if ((|pdec_cfi_taken_vec_o) ^ pdec_taken_o) begin
                $fatal("pdec_taken_mask_o is different with pdec_taken_o");
            end
        end
    end

    logic [      FETCH_WIDTH/16-1:0]    golden_inst_border_mask;
    logic [      FETCH_WIDTH/16-1:0]    golden_inst_rvc_mask;
    always_comb begin
        golden_inst_border_mask = 0;
        if (fpkt_misalign_valid_i) begin
            golden_inst_border_mask[0] = 0;
            golden_inst_border_mask[1] = 1;
            for (int i = 2; i < FETCH_WIDTH / 16; i++) begin
                golden_inst_border_mask[i] = golden_inst_border_mask[i - 1'b1] == 0 ? 1 : fpkt_cacheline_i[(i - 1'b1) * 16 +: 2] != 2'b11;
            end
        end 
        else begin
            if (fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1] == 0) begin
                golden_inst_border_mask[0] = 1;
            end
            else begin
                golden_inst_border_mask[0] = 0;
            end
            for (int i = 1; i < FETCH_WIDTH / 16; i++) begin
                if (i < fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
                    golden_inst_border_mask[i] = 0;
                end else if (i == fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
                    golden_inst_border_mask[i] = 1;
                end else begin
                    golden_inst_border_mask[i] = golden_inst_border_mask[i - 1'b1] == 0 ? 1 : fpkt_cacheline_i[(i - 1'b1) * 16 +: 2] != 2'b11;
                end
            end
        end
    end

    always_comb begin
        for (int i = 0; i < FETCH_WIDTH / 16; i++) begin
            golden_inst_rvc_mask[i] = golden_inst_border_mask[i] & (fpkt_cacheline_i[i*16+:2] != 2'b11);
        end
    end

    always_ff @(posedge clk) begin
        if (fpkt_vld_i) begin
            if (fpkt_misalign_valid_i) begin
                if (golden_inst_border_mask != inst_border_mask) $fatal("inst_border_mask is different with golden");
                if (golden_inst_rvc_mask != inst_rvc_mask)  $fatal("inst_rvc_mask is different with golden");
            end
            else begin
                for (int i = 0; i < FETCH_WIDTH/16; i++) begin
                    if (i >= fpkt_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
                        if (golden_inst_border_mask[i] != inst_border_mask[i]) $fatal("inst_border_mask is different with golden");
                        if (golden_inst_rvc_mask[i] != inst_rvc_mask[i])  $fatal("inst_rvc_mask is different with golden");
                    end
                end
            end
        end
    end

`endif

endmodule
