module predecode
    import rvh_pkg::*;
(
    input  [31:0]               instr_i,
    input  logic                instr_is_rvc_i,

    output logic                is_jal_o,
    output logic                is_jalr_o,
    output logic                is_return_o,
    output logic                is_call_o,
    output logic                is_branch_o,
    output logic [VADDR_WIDTH-1:0]    imm_o
);
    /* j type instruction immdiate generate */
    function automatic logic [VADDR_WIDTH-1:0] j_imm (input logic [31:0] instruction_i);
        return { {44+VADDR_WIDTH-64 {instruction_i[31]}}, instruction_i[19:12], instruction_i[20], instruction_i[30:21], 1'b0 };
    endfunction
    /* b type instruction immdiate generate */
    function automatic logic [VADDR_WIDTH-1:0] b_imm (input logic [31:0] instruction_i);
        return { {51+VADDR_WIDTH-64 {instruction_i[31]}}, instruction_i[31], instruction_i[7], instruction_i[30:25], instruction_i[11:8], 1'b0 };
    endfunction
    
    // RVI
    logic    is_rvi_jal;
    logic    is_rvi_jalr; 
    logic    is_rvi_return;
    logic    is_rvi_call;
    logic    is_rvi_branch;
    logic [VADDR_WIDTH-1:0]  rvi_imm;
    // RVC
    logic    is_rvc_jal;
    logic    is_rvc_jr_or_jalr;
    logic    is_rvc_jalr;
    logic    is_rvc_jr; 
    logic    is_rvc_return;
    logic    is_rvc_call;
    logic    is_rvc_branch;
    logic [VADDR_WIDTH-1:0]  rvc_imm;
    
    assign is_rvi_jal    = instr_i[6:0] == riscv_pkg::OpcodeJal;
    assign is_rvi_jalr   = instr_i[6:0] == riscv_pkg::OpcodeJalr;
    /* rd == x1 / x5 */
    assign is_rvi_call   = (is_rvi_jal | is_rvi_jalr) & ((instr_i[11:7] == 5'd1) | instr_i[11:7] == 5'd5);
    /* rs1 == x1 / x5 , rs1 =/= rd */
    assign is_rvi_return = is_rvi_jalr & ((instr_i[19:15] == 5'd1) | instr_i[19:15] == 5'd5) & (instr_i[19:15] != instr_i[11:7]);
    assign is_rvi_branch = instr_i[6:0] == riscv_pkg::OpcodeBranch;
    assign rvi_imm       = (instr_i[3]) ? j_imm(instr_i) : b_imm(instr_i);
    
    assign is_rvc_jal    = ((instr_i[15:13] == riscv_pkg::OpcodeC1J) & (instr_i[1:0] == riscv_pkg::OpcodeC1));
    assign is_rvc_jr_or_jalr   = (instr_i[15:13] == riscv_pkg::OpcodeC2JalrMvAdd) & (instr_i[6:2] == 5'b00000) & (instr_i[1:0] == riscv_pkg::OpcodeC2);
    assign is_rvc_jalr   = is_rvc_jr_or_jalr & instr_i[12];
    assign is_rvc_jr     = is_rvc_jr_or_jalr & ~instr_i[12];
    /* rs1 == x1/x5, rvc_jr bind rd to x0 */
    assign is_rvc_return = ((instr_i[11:7] == 5'd1) | (instr_i[11:7] == 5'd5)) & is_rvc_jr;
    /* rvc_jalr bind rd to x1 */
    assign is_rvc_call   = is_rvc_jalr;
    assign is_rvc_branch = ((instr_i[15:13] == riscv_pkg::OpcodeC1Beqz) | (instr_i[15:13] == riscv_pkg::OpcodeC1Bnez)) & (instr_i[1:0] == riscv_pkg::OpcodeC1);
    assign rvc_imm       = (instr_i[14]) ? {{56+VADDR_WIDTH-64{instr_i[12]}}, instr_i[6:5], instr_i[2], instr_i[11:10], instr_i[4:3], 1'b0}
                                       : {{53+VADDR_WIDTH-64{instr_i[12]}}, instr_i[8], instr_i[10:9], instr_i[6], instr_i[7], instr_i[2], instr_i[11], instr_i[5:3], 1'b0};
    // select final output with is_rvc signal
    assign is_jal_o      = instr_is_rvc_i ? is_rvc_jal        : is_rvi_jal;
    assign is_jalr_o     = instr_is_rvc_i ? is_rvc_jr_or_jalr : is_rvi_jalr;
    assign is_return_o   = instr_is_rvc_i ? is_rvc_return     : is_rvi_return;
    assign is_call_o     = instr_is_rvc_i ? is_rvc_call       : is_rvi_call;
    assign is_branch_o   = instr_is_rvc_i ? is_rvc_branch     : is_rvi_branch;
    assign imm_o         = instr_is_rvc_i ? rvc_imm           : rvi_imm;
endmodule
