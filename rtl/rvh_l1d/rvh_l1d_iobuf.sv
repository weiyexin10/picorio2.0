module rvh_l1d_iobuf
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_l1d_pkg::*;
(

    input  logic                                                    req_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                          req_vld_mask_i,
    input  logic                                                    req_is_ld_i,

    input  logic [     ROB_TAG_WIDTH-1:0]                           req_rob_tag_i,
    input  logic [    PREG_TAG_WIDTH-1:0]                           req_prd_i,
    
    input  logic [      STU_OP_WIDTH-1:0]                           req_st_opcode_i,
    input  logic [       PADDR_WIDTH-1:0]                           req_st_paddr_i,
    input  logic [       XLEN-1:0 ]                                 req_st_dat_i,

    input  logic [       L1D_INDEX_WIDTH-1:0]                       req_ld_index_i,
    input  logic [      L1D_OFFSET_WIDTH-1:0]                       req_ld_offset_i,
    input  logic [      LDU_OP_WIDTH-1:0]                           req_ld_opcode_i,

    output logic                                                    req_rdy_o,
    
    // DTLB -> D$
    output  logic [LSU_ADDR_PIPE_COUNT-1:0]                         req_vld_mask_o,
    input  logic                                                    dtlb_iobuf_resp_vld_i,
    input  logic                                                    dtlb_iobuf_resp_excp_vld_i, // s1 kill
    input  logic                                                    dtlb_iobuf_resp_hit_i,      // s1 kill
    input  logic [       PPN_WIDTH-1:0]                             dtlb_iobuf_resp_ppn_i, // VIPT, get at s1 if tlb hit
    

    // D$ -> ROB : Write Back
    output logic                                                    ld_wb_rob_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0]                           ld_wb_rob_tag_o,
    // D$ -> Int PRF : Write Back
    output logic                                                    ld_wb_prf_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                           ld_wb_prf_tag_o,
    output logic [              XLEN-1:0]                           ld_wb_prf_data_o,
    // D$ -> ROB : Write Back
    output logic                                                    st_wb_rob_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0]                           st_wb_rob_tag_o,

    input logic                                                     ld_wb_rdy_i,
    input logic                                                     st_wb_rdy_i,


    // L1D -> L2 : Request
      // AR
    output logic                                                    iobuf_l2_req_arvalid_o,
    input  logic                                                    iobuf_l2_req_arready_i,
    output cache_mem_if_ar_t                                        iobuf_l2_req_ar_o,
      // AW 
    output logic                                                    iobuf_l2_req_awvalid_o,
    input  logic                                                    iobuf_l2_req_awready_i,
    output cache_mem_if_aw_t                                        iobuf_l2_req_aw_o,
      // W 
    output logic                                                    iobuf_l2_req_wvalid_o,
    input  logic                                                    iobuf_l2_req_wready_i,
    output cache_mem_if_w_t                                         iobuf_l2_req_w_o,
    // L1D -> L2 : Response
      // B
    input  logic                                                    l2_iobuf_resp_bvalid_i,
    output logic                                                    l2_iobuf_resp_bready_o,
    input  cache_mem_if_b_t                                         l2_iobuf_resp_b_i,
      // mem bus -> mlfb
      // R
    input  logic                                                    l2_iobuf_resp_rvalid_i,
    output logic                                                    l2_iobuf_resp_rready_o,
    input cache_mem_if_r_t                                          l2_iobuf_resp_r_i,

    input logic                                                     kill_ld_resp_i,
    input logic                                                     flush_i,

    input  logic                                                    fencei_flush_vld_i,
    output logic                                                    fencei_flush_grant_o,

    input clk,
    input rst
);

    logic                                                    is_ld_q;

    logic                                                    vld_q;
    logic [     ROB_TAG_WIDTH-1:0]                           rob_tag_q;
    logic [    PREG_TAG_WIDTH-1:0]                           prd_q;
    // logic [      LDU_OP_WIDTH-1:0]                           opcode_q;

    logic [       L1D_INDEX_WIDTH-1:0]                       ld_index_q;
    logic [      L1D_OFFSET_WIDTH-1:0]                       ld_offset_q;
    logic [      LDU_OP_WIDTH-1:0]                           ld_opcode_q;

    logic [       PADDR_WIDTH-1:0]                           st_paddr_q;
    logic [       XLEN-1:0 ]                                st_dat_q;
    logic [       XLEN-1:0 ]                                ld_dat_q;
    logic [     XLEN/8-1:0 ]                                _req_st_dat_byte_mask, req_st_dat_byte_mask;
    logic [     XLEN/8-1:0 ]                                st_dat_byte_mask_q;

    logic [       PADDR_WIDTH-1:0]                          paddr_aligned;

    logic                                                   enq_fire, enq_fire_q;
    logic                                                   deq_fire;

    l1d_iobuf_state_t                                       state, next_state;

    rrv64_l1d_req_type_dec_t                                req_st_opcode_dec;

    logic                                                   dtlb_hit;

    logic                                                   killed_d, killed_q;

    logic                                                   fencei_flush_pending;

    logic                                                   fencei_flush_vld_q;

    rvh_l1d_dec l1dc_dec_ruby_rvh_trans_u
    (
        .is_ld_req_vld_i                (1'b0           ),
        .is_st_req_vld_i                (1'b1           ),
        .ls_pipe_l1d_ld_req_opcode_i    ('0             ),
        .ls_pipe_l1d_st_req_opcode_i    (req_st_opcode_i    ),
        .req_type_dec_o                 (req_st_opcode_dec  )
    );

    assign _req_st_dat_byte_mask  = req_st_opcode_dec.op_b  ? {{(L1D_STB_DATA_WIDTH/8-1){1'b0}}, 1'b1} :
                                   req_st_opcode_dec.op_hw ? {{(L1D_STB_DATA_WIDTH/8-2){1'b0}}, 2'b11} :
                                   req_st_opcode_dec.op_w  ? {{(L1D_STB_DATA_WIDTH/8-4){1'b0}}, 4'b1111} :
                                   req_st_opcode_dec.op_dw ? {{(L1D_STB_DATA_WIDTH/8-8){1'b0}}, 8'b11111111} :
                                                                      '0;

    assign req_st_dat_byte_mask = _req_st_dat_byte_mask << req_st_paddr_i[2:0];

    assign paddr_aligned = is_ld_q ? {dtlb_iobuf_resp_ppn_i, ld_index_q, ld_offset_q[L1D_OFFSET_WIDTH-1:3], 3'b0} : {st_paddr_q[PADDR_WIDTH-1:3], 3'b0};
    assign enq_fire = req_vld_i & req_rdy_o;
    assign deq_fire = vld_q & (next_state == IOBUF_IDLE);
    assign dtlb_hit = dtlb_iobuf_resp_vld_i & dtlb_iobuf_resp_hit_i & ~dtlb_iobuf_resp_excp_vld_i;

    assign killed_d = kill_ld_resp_i | flush_i;

    always_ff @(posedge clk) begin
        if (~rst) begin
            killed_q <= 0;
        end
        else if (killed_d & ((state != IOBUF_IDLE) | (next_state != IOBUF_IDLE))) begin
            killed_q <= 1'b1;
        end
        else if (next_state == IOBUF_IDLE) begin
            killed_q <= 1'b0;
        end
    end

    always_ff @(posedge clk) begin
        if (~rst) begin
            fencei_flush_vld_q <= 0;
        end
        else begin
            fencei_flush_vld_q <= fencei_flush_vld_i;
        end
    end

    always_ff @(posedge clk) begin
        if (~rst) begin
            fencei_flush_pending <= 0;
        end
        else if (fencei_flush_vld_q & ((state != IOBUF_IDLE) | (next_state != IOBUF_IDLE))) begin
            fencei_flush_pending <= 1'b1;
        end
        else if (next_state == IOBUF_IDLE) begin
            fencei_flush_pending <= 1'b0;
        end
    end

    assign fencei_flush_grant_o = (fencei_flush_vld_q & ((state == IOBUF_IDLE) & (next_state == IOBUF_IDLE))) | (fencei_flush_pending & (next_state == IOBUF_IDLE));

    always_ff @(posedge clk) begin
        if (~rst) begin
            vld_q <= 0;
        end
        else if (enq_fire_q & is_ld_q & ~dtlb_hit) begin
            vld_q <= 0;
        end 
        else if (enq_fire) begin
            vld_q <= 1;
        end
        else if (deq_fire) begin
            vld_q <= 0;
        end
    end

    always_ff @(posedge clk) begin
        if (~rst) begin
            enq_fire_q <= 0;
        end
        else begin
            enq_fire_q <= enq_fire;
        end
    end

    always_ff @(posedge clk) begin
        req_vld_mask_o <= req_vld_mask_i;
    end

    always_ff @(posedge clk) begin
        if (enq_fire) begin
            is_ld_q <= req_is_ld_i;
            ld_index_q <= req_ld_index_i;
            ld_offset_q <= req_ld_offset_i;
            ld_opcode_q <= req_ld_opcode_i;
            rob_tag_q <= req_rob_tag_i;
            prd_q <= req_prd_i;
            // opcode_q <= req_opcode_i;
            st_paddr_q <= req_st_paddr_i;
            st_dat_q <= req_st_dat_i << (req_st_paddr_i[2:0] * 8);
            st_dat_byte_mask_q <= req_st_dat_byte_mask;
        end
    end

    always_comb begin
        next_state = state;
        if (enq_fire_q & is_ld_q & ~dtlb_hit) begin
            next_state = IOBUF_IDLE;
        end
        else begin
            case (state)
                IOBUF_IDLE: begin
                    if (vld_q) begin
                        if (is_ld_q) begin
                            if (iobuf_l2_req_arready_i) begin
                                next_state = IOBUF_WAIT_FOR_R_VALID;
                            end
                            else begin
                                next_state = IOBUF_WAIT_FOR_AR_READY;
                            end
                        end
                        else begin
                            if (iobuf_l2_req_awready_i & iobuf_l2_req_wready_i) begin
                                next_state = IOBUF_WAIT_FOR_B_VALID;
                            end
                            else if (iobuf_l2_req_awready_i) begin
                                next_state = IOBUF_WAIT_FOR_W_READY;
                            end
                            else if (iobuf_l2_req_wready_i) begin
                                next_state = IOBUF_WAIT_FOR_AW_READY;
                            end
                            else begin
                                next_state = IOBUF_WAIT_FOR_AW_W_READY;
                            end
                        end
                    end
                end

                IOBUF_WAIT_FOR_AW_W_READY: begin
                    if (iobuf_l2_req_awready_i & iobuf_l2_req_wready_i) begin
                        next_state = IOBUF_WAIT_FOR_B_VALID;
                    end
                    else if (iobuf_l2_req_awready_i) begin
                        next_state = IOBUF_WAIT_FOR_W_READY;
                    end
                    else if (iobuf_l2_req_wready_i) begin
                        next_state = IOBUF_WAIT_FOR_AW_READY;
                    end
                end

                IOBUF_WAIT_FOR_AW_READY: begin
                    if (iobuf_l2_req_awready_i) begin
                        next_state = IOBUF_WAIT_FOR_B_VALID;
                    end
                end

                IOBUF_WAIT_FOR_W_READY: begin
                    if (iobuf_l2_req_wready_i) begin
                        next_state = IOBUF_WAIT_FOR_B_VALID;
                    end
                end

                IOBUF_WAIT_FOR_AR_READY: begin
                    if (iobuf_l2_req_arready_i) begin
                        next_state = IOBUF_WAIT_FOR_R_VALID;
                    end
                end
                IOBUF_WAIT_FOR_B_VALID: begin
                    if (l2_iobuf_resp_bvalid_i & l2_iobuf_resp_b_i.bid.is_io) begin
                        if (st_wb_rdy_i) begin
                            next_state = IOBUF_IDLE;
                        end
                        else begin
                            next_state = IOBUF_WAIT_FOR_ST_WB;
                        end
                    end
                end
                IOBUF_WAIT_FOR_R_VALID: begin
                    if (l2_iobuf_resp_rvalid_i & l2_iobuf_resp_r_i.rid.is_io) begin
                        if (ld_wb_rdy_i) begin
                            next_state = IOBUF_IDLE;
                        end
                        else begin
                            next_state = IOBUF_WAIT_FOR_LD_WB;
                        end
                    end
                end
                IOBUF_WAIT_FOR_LD_WB: begin
                    if (ld_wb_rdy_i) begin
                        next_state = IOBUF_IDLE;
                    end
                end
                IOBUF_WAIT_FOR_ST_WB: begin
                    if (st_wb_rdy_i) begin
                        next_state = IOBUF_IDLE;
                    end
                end
                default:;
            endcase
        end
    end

    always_ff @(posedge clk) begin
        if (~rst) begin
            state <= IOBUF_IDLE;
        end
        else begin
            state <= next_state;
        end
    end

    always_ff @(posedge clk) begin
        if ((state == IOBUF_WAIT_FOR_R_VALID) & (next_state != IOBUF_WAIT_FOR_R_VALID)) begin
            ld_dat_q <= l2_iobuf_resp_r_i.dat;
        end
    end

//==========================================================
// AR channel
//==========================================================
    assign iobuf_l2_req_arvalid_o = vld_q & is_ld_q & dtlb_hit & ((state == IOBUF_IDLE) | (state == IOBUF_WAIT_FOR_AR_READY));
    assign iobuf_l2_req_ar_o.arid.is_io = 1;
    assign iobuf_l2_req_ar_o.arid.master_id = 0;
    assign iobuf_l2_req_ar_o.arid.tid = 0;
    assign iobuf_l2_req_ar_o.arid.bid = 0;
    // assign iobuf_l2_req_ar_o.araddr  = paddr_aligned;
    assign iobuf_l2_req_ar_o.araddr = {dtlb_iobuf_resp_ppn_i, ld_index_q, ld_offset_q};
    assign iobuf_l2_req_ar_o.arlen   = 0;
    assign iobuf_l2_req_ar_o.arburst = 2'b00;
    assign iobuf_l2_req_ar_o.arsize  =  (ld_opcode_q == LDU_LB)    ? 'h0 :
                                        (ld_opcode_q == LDU_LBU)   ? 'h0 :
                                        (ld_opcode_q == LDU_LH)    ? 'h1 :
                                        (ld_opcode_q == LDU_LHU)   ? 'h1 :
                                        (ld_opcode_q == LDU_LW)    ? 'h2 :
                                        (ld_opcode_q == LDU_LWU)   ? 'h2 : 'h3;

//==========================================================
// R channel
//==========================================================
    logic [XLEN-1:0] lsu_raw_dat;
    logic [XLEN-1:0] lsu_shifted_dat;
    logic [XLEN-1:0] lsu_ld_dat_valid_bit_mask;
    logic [XLEN-1:0] lsu_ld_dat_sign_ext_bit_mask;


    assign lsu_raw_dat = state == IOBUF_WAIT_FOR_LD_WB ? ld_dat_q : l2_iobuf_resp_r_i.dat;
    assign lsu_shifted_dat = lsu_raw_dat >> (ld_offset_q[2:0]*8);

    assign lsu_ld_dat_valid_bit_mask      = (ld_opcode_q == LDU_LB)    ? 64'hff :
                                            (ld_opcode_q == LDU_LBU)   ? 64'hff :
                                            (ld_opcode_q == LDU_LH)    ? 64'hffff :
                                            (ld_opcode_q == LDU_LHU)   ? 64'hffff :
                                            (ld_opcode_q == LDU_LW)    ? 64'hffff_ffff :
                                            (ld_opcode_q == LDU_LWU)   ? 64'hffff_ffff : 64'hffff_ffff_ffff_ffff;

    assign lsu_ld_dat_sign_ext_bit_mask   = (ld_opcode_q == LDU_LB)    ? {{(XLEN-8){lsu_shifted_dat[8-1]}}, {8{1'b0}}} :
                                            (ld_opcode_q == LDU_LBU)   ? '0 :
                                            (ld_opcode_q == LDU_LH)    ? {{(XLEN-8*2){lsu_shifted_dat[8*2-1]}}, {(8*2){1'b0}}} :
                                            (ld_opcode_q == LDU_LHU)   ? '0 :
                                            (ld_opcode_q == LDU_LW)    ? {{(XLEN-8*4){lsu_shifted_dat[8*4-1]}}, {(8*4){1'b0}}} :
                                            (ld_opcode_q == LDU_LWU)   ? 0 : 0;

    assign ld_wb_prf_vld_o = ~killed_d & ~killed_q & (((state == IOBUF_WAIT_FOR_R_VALID) & (next_state != IOBUF_WAIT_FOR_R_VALID)) | (state == IOBUF_WAIT_FOR_LD_WB));
    assign ld_wb_rob_vld_o = ld_wb_prf_vld_o;
    assign ld_wb_rob_tag_o = rob_tag_q;
    assign ld_wb_prf_tag_o = prd_q;
    assign ld_wb_prf_data_o = (lsu_shifted_dat & lsu_ld_dat_valid_bit_mask) | lsu_ld_dat_sign_ext_bit_mask;

//==========================================================
// AW channel
//==========================================================
    assign  iobuf_l2_req_awvalid_o = vld_q & ~is_ld_q & (   (state == IOBUF_IDLE) | 
                                                            (state == IOBUF_WAIT_FOR_AW_READY) | 
                                                            (state == IOBUF_WAIT_FOR_AW_W_READY)
                                                        );
    assign  iobuf_l2_req_aw_o.awid.is_io   = '1;
    assign  iobuf_l2_req_aw_o.awid.master_id   = 0;
    assign  iobuf_l2_req_aw_o.awid.tid     = '0;
    assign  iobuf_l2_req_aw_o.awid.bid     = 0;
    assign  iobuf_l2_req_aw_o.awaddr   = paddr_aligned;
    assign  iobuf_l2_req_aw_o.awlen    = 0;
    assign  iobuf_l2_req_aw_o.awsize   = AXI_SIZE;
    assign  iobuf_l2_req_aw_o.awburst  = 2'b00;
//==========================================================
// W channel
//==========================================================
    assign  iobuf_l2_req_wvalid_o = vld_q & ~is_ld_q & (    (state == IOBUF_IDLE) | 
                                                            (state == IOBUF_WAIT_FOR_W_READY) | 
                                                            (state == IOBUF_WAIT_FOR_AW_W_READY)
                                                       );
    assign  iobuf_l2_req_w_o.wid.is_io   = '1;
    assign  iobuf_l2_req_w_o.wid.master_id = 0;
    assign  iobuf_l2_req_w_o.wid.tid     = 0;
    assign  iobuf_l2_req_w_o.wid.bid     = 0;
    assign  iobuf_l2_req_w_o.wdata       = st_dat_q;
    assign  iobuf_l2_req_w_o.wlast       = '1;
    assign  iobuf_l2_req_w_o.wstrb       = st_dat_byte_mask_q;

//==========================================================
// B channel
//==========================================================
    assign st_wb_rob_vld_o = ~killed_d & ~killed_q & (((state == IOBUF_WAIT_FOR_B_VALID) & (next_state != IOBUF_WAIT_FOR_B_VALID)) | (state == IOBUF_WAIT_FOR_ST_WB));
    assign st_wb_rob_tag_o = rob_tag_q;
//==========================================================
// output
//==========================================================
    assign req_rdy_o = ~vld_q;
    assign l2_iobuf_resp_bready_o = '1;
    assign l2_iobuf_resp_rready_o = '1;


endmodule
