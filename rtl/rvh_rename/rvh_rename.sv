module rvh_rename
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
(
    // Decoder -> Rename
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_vld_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_is_rvc_i,
    input  logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] dec_rn_inst_pc_i,
    input  logic [    RENAME_WIDTH-1:0][     BTQ_TAG_WIDTH-1:0] dec_rn_inst_btq_tag_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_pred_taken_i,
    input  logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] dec_rn_inst_pred_target_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_excp_vld_i,
    input  logic [    RENAME_WIDTH-1:0][    MAJOR_OP_WIDTH-1:0] dec_rn_inst_major_op_i,
    input  logic [    RENAME_WIDTH-1:0][    MINOR_OP_WIDTH-1:0] dec_rn_inst_minor_op_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_use_rs1_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_use_rs2_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_use_rd_i,
    input  logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rs1_i,
    input  logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rs2_i,
    input  logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rd_i,
    input  logic [    RENAME_WIDTH-1:0][                  31:0] dec_rn_inst_imm_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_use_imm_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_alu_use_pc_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_is_fence_i,
    input  logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_control_flow_i,
    // Exception
    input  logic [ EXCP_TVAL_WIDTH-1:0]                         dec_rn_inst_excp_tval_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                         dec_rn_inst_excp_cause_i,
    output logic [    RENAME_WIDTH-1:0]                         dec_rn_inst_rdy_o,
    // Rename -> INT PRF(Allocate)
    output logic [    RENAME_WIDTH-1:0]                         rn_int_prf_alloc_vld_o,
    output logic [    RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_prf_alloc_preg_o,

    // Rename -> RCU(Allocate)
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_vld_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_excp_o,
    output logic [   EXCP_TVAL_WIDTH-1:0]                         rn_rcu_alloc_inst_excp_tval_o,
    output logic [  EXCP_CAUSE_WIDTH-1:0]                         rn_rcu_alloc_inst_excp_cause_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_control_flow_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_is_rvc_o,
    output logic [      RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_rcu_alloc_inst_pc_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_is_br_o,
    output logic [      RENAME_WIDTH-1:0][     BTQ_TAG_WIDTH-1:0] rn_rcu_alloc_inst_btq_tag_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_pred_taken_o,
    output logic [      RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_rcu_alloc_inst_pred_target_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_use_rd_o,
    output logic [      RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rd_o,
    output logic [      RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rs1_o,
    output logic [      RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rs2_o,
    input  logic [      RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rs1_i,
    input  logic [      RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rs2_i,
    input  logic [      RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rd_i,
    input  logic [      RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_rcu_alloc_inst_rob_tag_i,
    input  logic [      RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_rcu_alloc_inst_brq_tag_i,
    input  logic                                                  rn_rcu_alloc_rcu_rdy_i,
    // Rename -> Dispatch Buffer(INT)
    output logic [      RENAME_WIDTH-1:0]                         rn_int_disp_vld_o,
`ifdef SIMULATION
    output logic [      RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_int_disp_pc_o,
`endif
    output logic [      RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_int_disp_rob_tag_o,
    output logic [      RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_int_disp_brq_tag_o,
    output logic [      RENAME_WIDTH-1:0][    INT_TYPE_WIDTH-1:0] rn_int_disp_major_op_o,
    output logic [      RENAME_WIDTH-1:0][      INT_OP_WIDTH-1:0] rn_int_disp_minor_op_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_rs1_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_rs2_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_rd_o,
    output logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs1_o,
    output logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs2_o,
    output logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rd_o,
    output logic [      RENAME_WIDTH-1:0][                  31:0] rn_int_disp_imm_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_imm_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_int_disp_alu_use_pc_o,
    input  logic                                                  rn_int_disp_rdy_i,
    // Rename -> Dispatch Buffer(LS)
    output logic [      RENAME_WIDTH-1:0]                         rn_ls_disp_vld_o,
`ifdef SIMULATION
    output logic [      RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_ls_disp_pc_o,
`endif
    output logic [      RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_ls_disp_rob_tag_o,
    output logic [      RENAME_WIDTH-1:0][    LSU_TYPE_WIDTH-1:0] rn_ls_disp_major_op_o,
    output logic [      RENAME_WIDTH-1:0][      LSU_OP_WIDTH-1:0] rn_ls_disp_minor_op_o,
    output logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs1_o,
    output logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs2_o,
    output logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rd_o,
    output logic [      RENAME_WIDTH-1:0][                  11:0] rn_ls_disp_imm_o,
    output logic [      RENAME_WIDTH-1:0]                         rn_ls_disp_is_fence_o,
    input  logic                                                  rn_ls_disp_rdy_i,
    // Rename -> CSR Issue Register
    output logic                                                  rn_csr_ir_disp_vld_o,
    output logic [       VADDR_WIDTH-1:0]                         rn_csr_ir_disp_pc_o,
    output logic [     ROB_TAG_WIDTH-1:0]                         rn_csr_ir_disp_rob_tag_o,
    output logic [      CSR_OP_WIDTH-1:0]                         rn_csr_ir_disp_minor_op_o,
    output logic                                                  rn_csr_ir_disp_use_imm_o,
    output logic [                   4:0]                         rn_csr_ir_disp_imm_o,
    output logic                                                  rn_csr_ir_disp_use_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         rn_csr_ir_disp_phy_rs1_o,
    output logic                                                  rn_csr_ir_disp_use_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         rn_csr_ir_disp_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         rn_csr_ir_disp_phy_rd_o,
    output logic [                  11:0]                         rn_csr_ir_disp_csr_addr_o,

    input logic flush_i,

    input clk,
    input rst
);

    // Allocate
    logic alloc_rdy;
    logic [RENAME_WIDTH-1:0] alloc_vld;
    logic [RENAME_WIDTH-1:0] alloc_fire;
    // br mask
    logic [RENAME_WIDTH-1:0] br_mask;
    // ls mask
    logic [RENAME_WIDTH-1:0] ls_mask;
    // int mask
    logic [RENAME_WIDTH-1:0] int_mask;
    // csr mask
    logic [RENAME_WIDTH-1:0] csr_mask;

    always_comb begin : gen_alloc_vld_vec
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            if (i == 0) begin
                alloc_vld[i] = ~flush_i & dec_rn_inst_vld_i[i];
            end else begin
                alloc_vld[i] = dec_rn_inst_vld_i[i] & ((alloc_vld[i-1] & ~dec_rn_inst_control_flow_i[
                    i-1] & ~dec_rn_inst_excp_vld_i[i-1] & ~csr_mask[i-1]) | ~dec_rn_inst_vld_i[i-1]);
            end
        end
    end
    assign alloc_rdy = rn_rcu_alloc_rcu_rdy_i & rn_ls_disp_rdy_i & rn_int_disp_rdy_i;
    assign alloc_fire = {RENAME_WIDTH{alloc_rdy}} & alloc_vld;

    assign dec_rn_inst_rdy_o = alloc_fire;

    always_comb begin : gen_br_mask
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            br_mask[i] = dec_rn_inst_major_op_i[i] == OP_BRU;
        end
    end

    // rn -> rcu
    assign rn_rcu_alloc_inst_vld_o = alloc_fire;
    assign rn_rcu_alloc_inst_excp_o = dec_rn_inst_excp_vld_i;
    assign rn_rcu_alloc_inst_excp_tval_o = dec_rn_inst_excp_tval_i;
    assign rn_rcu_alloc_inst_excp_cause_o = dec_rn_inst_excp_cause_i;
    assign rn_rcu_alloc_inst_control_flow_o = dec_rn_inst_control_flow_i;
    assign rn_rcu_alloc_inst_pc_o = dec_rn_inst_pc_i;
    assign rn_rcu_alloc_inst_is_br_o = br_mask;
    assign rn_rcu_alloc_inst_btq_tag_o = dec_rn_inst_btq_tag_i;
    assign rn_rcu_alloc_inst_pred_taken_o = dec_rn_inst_pred_taken_i;
    assign rn_rcu_alloc_inst_pred_target_o = dec_rn_inst_pred_target_i;
    assign rn_rcu_alloc_inst_is_rvc_o = dec_rn_inst_is_rvc_i;
    assign rn_rcu_alloc_inst_use_rd_o = dec_rn_inst_use_rd_i;
    assign rn_rcu_alloc_inst_isa_rd_o = dec_rn_inst_rd_i;
    assign rn_rcu_alloc_inst_isa_rs1_o = dec_rn_inst_rs1_i;
    assign rn_rcu_alloc_inst_isa_rs2_o = dec_rn_inst_rs2_i; 

    // rn -> Int prf
    assign rn_int_prf_alloc_vld_o = alloc_fire & dec_rn_inst_use_rd_i;
    assign rn_int_prf_alloc_preg_o = rn_rcu_alloc_inst_phy_rd_i;



    // rn -> int disp buffer
    always_comb begin : gen_int_mask
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            int_mask[i] = (dec_rn_inst_major_op_i[i] == OP_ALU) | (dec_rn_inst_major_op_i[i] == OP_MUL) | 
                        (dec_rn_inst_major_op_i[i] == OP_DIV) | (dec_rn_inst_major_op_i[i] == OP_BRU);
        end
    end

    logic [RENAME_WIDTH-1:0][INT_TYPE_WIDTH-1:0] int_type;

    always_comb begin : int_type_adapter
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            int_type[i] = ({INT_TYPE_WIDTH{dec_rn_inst_major_op_i[i] == OP_ALU}} & INT_ALU_TYPE) |
                ({INT_TYPE_WIDTH{dec_rn_inst_major_op_i[i] == OP_MUL}} & INT_MUL_TYPE) |
                ({INT_TYPE_WIDTH{dec_rn_inst_major_op_i[i] == OP_DIV}} & INT_DIV_TYPE) |
                ({INT_TYPE_WIDTH{dec_rn_inst_major_op_i[i] == OP_BRU}} & INT_BRU_TYPE);
        end
    end

    generate
        for(genvar i = 0 ; i  < RENAME_WIDTH; i++) begin
            assign rn_int_disp_vld_o[i]        = alloc_fire[i] & int_mask[i];
        `ifdef SIMULATION
            assign rn_int_disp_pc_o[i]         = dec_rn_inst_pc_i[i];
        `endif
            assign rn_int_disp_rob_tag_o[i]    = rn_rcu_alloc_inst_rob_tag_i[i];
            assign rn_int_disp_brq_tag_o[i]    = rn_rcu_alloc_inst_brq_tag_i[i];
            assign rn_int_disp_major_op_o[i]   = int_type[i];
            assign rn_int_disp_minor_op_o[i]   = dec_rn_inst_minor_op_i[i][INT_OP_WIDTH-1:0];
            assign rn_int_disp_use_rs1_o[i]    = dec_rn_inst_use_rs1_i[i];
            assign rn_int_disp_use_rs2_o[i]    = dec_rn_inst_use_rs2_i[i];
            assign rn_int_disp_use_rd_o[i]     = dec_rn_inst_use_rd_i[i];
            assign rn_int_disp_phy_rs1_o[i]    = rn_rcu_alloc_inst_phy_rs1_i[i];
            assign rn_int_disp_phy_rs2_o[i]    = rn_rcu_alloc_inst_phy_rs2_i[i];
            assign rn_int_disp_phy_rd_o[i]     = rn_rcu_alloc_inst_phy_rd_i[i];
            assign rn_int_disp_imm_o[i]        = dec_rn_inst_imm_i[i];
            assign rn_int_disp_use_imm_o[i]    = dec_rn_inst_use_imm_i[i];
            assign rn_int_disp_alu_use_pc_o[i] = dec_rn_inst_alu_use_pc_i[i];
        end
    endgenerate

    // rn -> ls disp buffer
    always_comb begin : gen_ls_mask
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            ls_mask[i] = dec_rn_inst_major_op_i[i] == OP_LDU | dec_rn_inst_major_op_i[i] == OP_STU;
        end
    end

    logic [RENAME_WIDTH-1:0][LSU_TYPE_WIDTH-1:0] ls_type;

    always_comb begin : ls_type_adapter
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            ls_type[i] = ({LSU_TYPE_WIDTH{dec_rn_inst_major_op_i[i] == OP_LDU}} & LSU_LD_TYPE) |
                ({LSU_TYPE_WIDTH{dec_rn_inst_major_op_i[i] == OP_STU}} & LSU_ST_TYPE);
        end
    end

    generate
        for(genvar i = 0 ; i  < RENAME_WIDTH; i++) begin
            assign rn_ls_disp_vld_o[i]      = alloc_fire[i] & ls_mask[i];
        `ifdef SIMULATION
            assign rn_ls_disp_pc_o[i]       = dec_rn_inst_pc_i[i];
        `endif
            assign rn_ls_disp_rob_tag_o[i]  = rn_rcu_alloc_inst_rob_tag_i[i];
            assign rn_ls_disp_major_op_o[i] = ls_type[i];
            assign rn_ls_disp_minor_op_o[i] = dec_rn_inst_minor_op_i[i][LSU_OP_WIDTH-1:0];
            assign rn_ls_disp_phy_rs1_o [i] = rn_rcu_alloc_inst_phy_rs1_i[i];
            assign rn_ls_disp_phy_rs2_o[i]  = rn_rcu_alloc_inst_phy_rs2_i[i];
            assign rn_ls_disp_phy_rd_o[i]   = rn_rcu_alloc_inst_phy_rd_i[i];
            assign rn_ls_disp_is_fence_o[i] = dec_rn_inst_is_fence_i[i];
            assign rn_ls_disp_imm_o[i]      = dec_rn_inst_imm_i[i][11:0];
        end
    endgenerate

    // rn -> csr ir
    logic [RENAME_WIDTH-1:0][$clog2(RENAME_WIDTH)-1:0] rn_inst_id;
    logic [$clog2(RENAME_WIDTH)-1:0] disp_csr_inst_id;


    always_comb begin : gen_csr_mask
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            rn_inst_id[i] = i[$clog2(RENAME_WIDTH)-1:0];
            csr_mask[i]   = (dec_rn_inst_major_op_i[i] == OP_CSR) & alloc_fire[i];
        end
    end

    assign rn_csr_ir_disp_vld_o = |csr_mask;
    assign rn_csr_ir_disp_pc_o = dec_rn_inst_pc_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_rob_tag_o = rn_rcu_alloc_inst_rob_tag_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_minor_op_o = dec_rn_inst_minor_op_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_imm_o      = dec_rn_inst_imm_i[disp_csr_inst_id][4:0];
    assign rn_csr_ir_disp_use_imm_o  = dec_rn_inst_use_imm_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_use_rs1_o  = dec_rn_inst_use_rs1_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_phy_rs1_o  = rn_rcu_alloc_inst_phy_rs1_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_use_rs2_o  = dec_rn_inst_use_rs2_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_phy_rs2_o  = rn_rcu_alloc_inst_phy_rs2_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_phy_rd_o   = rn_rcu_alloc_inst_phy_rd_i[disp_csr_inst_id];
    assign rn_csr_ir_disp_csr_addr_o = dec_rn_inst_imm_i[disp_csr_inst_id][16:5];


    onehot_mux #(
        .SOURCE_COUNT(RENAME_WIDTH),
        .DATA_WIDTH  ($clog2(RENAME_WIDTH))
    ) u_onehot_mux (
        .sel_i (csr_mask),
        .data_i(rn_inst_id),
        .data_o(disp_csr_inst_id)
    );

endmodule : rvh_rename
