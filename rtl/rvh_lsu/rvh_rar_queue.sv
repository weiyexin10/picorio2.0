module rvh_rar_queue
    import rvh_pkg::*;
    import rvh_rcu_pkg::*;
    import rvh_lsu_pkg::*;
    import uop_encoding_pkg::*;
#(
    localparam int unsigned ENTRY_COUNT = RAR_QUEUE_ENTRY_COUNT
) (
    // LS_PIPE -> RAR : Allocate
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       ls_pipe_rar_alloc_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] ls_pipe_rar_alloc_rob_tag_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] ls_pipe_rar_alloc_paddr_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_alloc_len_i,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       ls_pipe_rar_alloc_rdy_o,

    // LS_PIPE -> RAR : Check RAR Ordering Failure
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       ls_pipe_rar_check_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] ls_pipe_rar_check_rob_tag_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_check_len_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] ls_pipe_rar_check_paddr_i,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       ls_pipe_rar_check_fail_o,

    // LDQ Status
    input logic ldq_empty_i,
    input logic [ROB_TAG_WIDTH-1:0] oldest_ld_rob_tag_i,

    // Cacheline invalid
    input  logic                                                 l1d_rar_invld_vld_i,
    input  logic [L1D_TAG_WIDTH_CORE-1:0]                             l1d_rar_invld_tag_i,    



    input logic flush_i,

    input clk,
    input rst
);
    logic [        ENTRY_COUNT-1:0][RAR_TAG_WIDTH-1:0] entry_tag_list;

    logic [        ENTRY_COUNT-1:0]                    entry_vld;
    logic [LSU_ADDR_PIPE_COUNT-1:0][  ENTRY_COUNT-1:0] avail_entry_mask;

    logic [LSU_ADDR_PIPE_COUNT-1:0]                    alloc_fire;
    logic [LSU_ADDR_PIPE_COUNT-1:0]                    alloc_rdy;
    logic [LSU_ADDR_PIPE_COUNT-1:0][  ENTRY_COUNT-1:0] alloc_entry_mask;
    logic [LSU_ADDR_PIPE_COUNT-1:0][RAR_TAG_WIDTH-1:0] alloc_entry_tag;
    
    logic [        ENTRY_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0] entry_rar_check_res;
    logic [LSU_ADDR_PIPE_COUNT-1:0][        ENTRY_COUNT-1:0] trans_rar_check_res;

    logic [ENTRY_COUNT-1:0]                       ls_pipe_rar_alloc_entry_vld;
    logic [ENTRY_COUNT-1:0][   ROB_TAG_WIDTH-1:0] ls_pipe_rar_alloc_entry_rob_tag;
    logic [ENTRY_COUNT-1:0][     PADDR_WIDTH-1:0] ls_pipe_rar_alloc_entry_paddr;
    logic [ENTRY_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_alloc_entry_len;

    assign ls_pipe_rar_alloc_rdy_o = {LSU_ADDR_PIPE_COUNT{&alloc_rdy}};
    assign alloc_fire = ls_pipe_rar_alloc_vld_i & ls_pipe_rar_alloc_rdy_o;

    always_comb begin
        for(int i = 0 ; i < ENTRY_COUNT; i++) begin
            entry_tag_list[i] = i;
        end
    end

    always_comb begin
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            for (int j = 0; j < ENTRY_COUNT; j++) begin
                trans_rar_check_res[i][j] = entry_rar_check_res[j][i];
            end
        end
    end

    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            assign ls_pipe_rar_check_fail_o[i] = |trans_rar_check_res[i];
        end
    endgenerate

    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if (i == 0) begin
                assign avail_entry_mask[i] = ~entry_vld;
            end else begin
                assign avail_entry_mask[i] = ~alloc_entry_mask[i-1] & avail_entry_mask[i-1];
            end
            
            assign alloc_rdy[i] = |avail_entry_mask[i];
            
            one_hot_priority_encoder #(
                .SEL_WIDTH(ENTRY_COUNT)
            ) u_avail_entry_one_hot_priority_encoder (
                .sel_i(avail_entry_mask[i]),
                .sel_o(alloc_entry_mask[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (RAW_TAG_WIDTH)
            ) u_avail_entry_tag_onehot_mux (
                .sel_i (alloc_entry_mask[i]),
                .data_i(entry_tag_list),
                .data_o(alloc_entry_tag[i])
            );
        end
    endgenerate

    always_comb begin
        ls_pipe_rar_alloc_entry_vld = {ENTRY_COUNT{1'b0}};
        ls_pipe_rar_alloc_entry_rob_tag = {ENTRY_COUNT*ROB_TAG_WIDTH{1'b0}};
        ls_pipe_rar_alloc_entry_paddr = {ENTRY_COUNT*PADDR_WIDTH{1'b0}};
        ls_pipe_rar_alloc_entry_len = {ENTRY_COUNT*MEM_OP_LEN_WIDTH{1'b0}};
        for(int i = 0 ; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if(alloc_fire[i]) begin
                ls_pipe_rar_alloc_entry_vld[alloc_entry_tag[i]] = 1'b1;
                ls_pipe_rar_alloc_entry_rob_tag[alloc_entry_tag[i]] = ls_pipe_rar_alloc_rob_tag_i[i];
                ls_pipe_rar_alloc_entry_paddr[alloc_entry_tag[i]]  = ls_pipe_rar_alloc_paddr_i[i];
                ls_pipe_rar_alloc_entry_len[alloc_entry_tag[i]]  = ls_pipe_rar_alloc_len_i[i];
            end
        end
    end

    generate
        for (genvar i = 0; i < ENTRY_COUNT; i++) begin : gen_rar_entry
            rvh_rar_queue_entry u_rvh_rar_queue_entry (
                .ls_pipe_rar_alloc_vld_i(ls_pipe_rar_alloc_entry_vld[i]),
                .ls_pipe_rar_alloc_rob_tag_i(ls_pipe_rar_alloc_entry_rob_tag[i]),
                .ls_pipe_rar_alloc_len_i(ls_pipe_rar_alloc_entry_len[i]),
                .ls_pipe_rar_alloc_paddr_i(ls_pipe_rar_alloc_entry_paddr[i]),
                .ls_pipe_rar_check_vld_i(ls_pipe_rar_check_vld_i),
                .ls_pipe_rar_check_rob_tag_i(ls_pipe_rar_check_rob_tag_i),
                .ls_pipe_rar_check_len_i(ls_pipe_rar_check_len_i),
                .ls_pipe_rar_check_paddr_i(ls_pipe_rar_check_paddr_i),
                .ls_pipe_rar_check_fail_o(entry_rar_check_res[i]),
                .ldq_empty_i(ldq_empty_i),
                .oldest_ld_rob_tag_i(oldest_ld_rob_tag_i),
                .entry_vld_o(entry_vld[i]),
                .l1d_invld_vld_i(l1d_rar_invld_vld_i),
                .l1d_invld_tag_i(l1d_rar_invld_tag_i),
                .flush_i(flush_i),
                .clk(clk),
                .rst(rst)
            );
        end
    endgenerate


endmodule : rvh_rar_queue
