module rvh_raw_check
    import rvh_rcu_pkg::*;
    import rvh_lsu_pkg::*;    
    import rvh_pkg::*;
    import riscv_pkg::*;
(
    input  logic [ROB_TAG_WIDTH-1:0]    src_rob_tag_i,
    input  logic [MEM_OP_LEN_WIDTH-1:0] src_len_i,
    input  logic [     PADDR_WIDTH-1:0] src_paddr_i,
    
    input  logic [ROB_TAG_WIDTH-1:0]    des_rob_tag_i,
    input  logic [MEM_OP_LEN_WIDTH-1:0] des_len_i,
    input  logic [     PADDR_WIDTH-1:0] des_paddr_i,
    
    output logic                        overlap_o
);
    
    logic [PADDR_WIDTH-1:3] src_tag;
    logic [PADDR_WIDTH-1:3] des_tag;
    
    logic [7:0] src_mask;
    logic [7:0] des_mask;
    
    
    assign src_tag   = src_paddr_i[PADDR_WIDTH-1:3];
    assign des_tag   = des_paddr_i[PADDR_WIDTH-1:3];
    assign src_mask  = generate_byte_mask_with_len(src_paddr_i,src_len_i);
    assign des_mask  = generate_byte_mask_with_len(des_paddr_i,des_len_i);
    assign overlap_o = (src_tag == des_tag) & (|(src_mask & des_mask)) & rob_tag_is_older(des_rob_tag_i,src_rob_tag_i);
    
    
endmodule : rvh_raw_check
