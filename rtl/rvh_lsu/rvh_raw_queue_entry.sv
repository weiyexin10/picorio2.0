module rvh_raw_queue_entry
    import rvh_pkg::*;
    import rvh_rcu_pkg::*;
    import rvh_lsu_pkg::*;
    import uop_encoding_pkg::*;
#(
    localparam int unsigned PADDR_TAG_LSB = 3
) (
    // Allocate
    input  logic                                                 ls_pipe_raw_alloc_vld_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       ls_pipe_raw_alloc_rob_tag_i,
    input  logic [   MEM_OP_LEN_WIDTH-1:0]                       ls_pipe_raw_alloc_len_i,
    input  logic [        PADDR_WIDTH-1:0]                       ls_pipe_raw_alloc_paddr_i,
    // Check RAW Ordering Failure
    input  logic [LSU_DATA_PIPE_COUNT-1:0]                       ls_pipe_raw_check_vld_i,
    input  logic [LSU_DATA_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] ls_pipe_raw_check_rob_tag_i,
    input  logic [LSU_DATA_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_raw_check_len_i,
    input  logic [LSU_DATA_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] ls_pipe_raw_check_paddr_i,
    output logic [LSU_DATA_PIPE_COUNT-1:0]                       ls_pipe_raw_check_fail_o,
    // Deallocate
    input  logic                                                 stq_empty_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       oldest_st_rob_tag_i,
    // Status
    output logic                                                 entry_vld_o,  

    input logic flush_i,

    input clk,
    input rst
);

    logic alloc_clk_en, dealloc_clk_en;

    // Change When allocate & Deallocate
    logic vld_d, vld_q;
    
    // Change When allocate
    logic [ROB_TAG_WIDTH-1:0] rob_tag_d, rob_tag_q;
    logic [PADDR_WIDTH-1:0] paddr_d, paddr_q;
    logic [MEM_OP_LEN_WIDTH-1:0] len_d, len_q;
    


    // Check
    logic [                    7:0]      byte_mask;
    logic [LSU_DATA_PIPE_COUNT-1:0][7:0] check_byte_mask;
    logic [LSU_DATA_PIPE_COUNT-1:0]      check_rob_tag_is_older;
    logic [LSU_DATA_PIPE_COUNT-1:0]      check_addr_overlap;
    logic [LSU_DATA_PIPE_COUNT-1:0]      check_tag_equal;


    assign byte_mask = generate_byte_mask_with_len(paddr_q, len_q);
    generate
        for (genvar i = 0; i < MEM_OP_LEN_WIDTH; i++) begin
            assign check_byte_mask[i] = generate_byte_mask_with_len(
                ls_pipe_raw_check_paddr_i[i], ls_pipe_raw_check_len_i[i]
            );
            assign check_rob_tag_is_older[i] = rob_tag_is_older(
                ls_pipe_raw_check_rob_tag_i[i], rob_tag_q
            );
            assign check_addr_overlap[i] = |(check_byte_mask[i] & byte_mask);
            assign check_tag_equal[i] = ls_pipe_raw_check_paddr_i[i][PADDR_WIDTH-1:PADDR_TAG_LSB] ==
                paddr_q[PADDR_WIDTH-1:PADDR_TAG_LSB];
            assign ls_pipe_raw_check_fail_o[i] = vld_q & ls_pipe_raw_check_vld_i[i] &
                check_tag_equal[i] & check_rob_tag_is_older[i] & check_addr_overlap[i];
        end
    endgenerate


    assign alloc_clk_en = ls_pipe_raw_alloc_vld_i;
    

    // Deallocate
    logic resolve_raw_hazard;
    assign resolve_raw_hazard = vld_q & (stq_empty_i | rob_tag_is_older(
        rob_tag_q, oldest_st_rob_tag_i
    ));
    assign dealloc_clk_en = resolve_raw_hazard;
    assign vld_d = ~flush_i & ~resolve_raw_hazard & ls_pipe_raw_alloc_vld_i;
    
    assign rob_tag_d = ls_pipe_raw_alloc_rob_tag_i;
    assign paddr_d = ls_pipe_raw_alloc_paddr_i;
    assign len_d = ls_pipe_raw_alloc_len_i;

    assign entry_vld_o = vld_q;

    always_ff @(posedge clk) begin : vld_dff
        if (rst) begin
            vld_q <= 1'b0;
        end else begin
            if (alloc_clk_en | dealloc_clk_en | flush_i) begin
                vld_q <= vld_d;
            end
        end
    end

    always_ff @(posedge clk) begin : payload_dff
        if (alloc_clk_en) begin
            rob_tag_q <= rob_tag_d;
            paddr_q   <= paddr_d;
            len_q     <= len_d;
        end
    end


endmodule : rvh_raw_queue_entry
