module rvh_lsu
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_lsu_pkg::*;
  import rvh_l1d_pkg::*;
(
    input logic [PRIV_LVL_WIDTH-1:0] misc_priv_lvl_i,
    input logic [XLEN-1:0] misc_mstatus_i,
    input logic [XLEN-1:0] misc_satp_i,
    // Allocate Port
    input logic [LSU_DISP_WIDTH-1:0] rn_lsu_alloc_vld_i,
    input logic [LSU_DISP_WIDTH-1:0] rn_lsu_alloc_is_fence_i,
    input logic [LSU_DISP_WIDTH-1:0][LSU_OP_WIDTH-1:0] rn_lsu_alloc_minor_op_i,
    input logic [LSU_DISP_WIDTH-1:0][LSU_TYPE_WIDTH-1:0] rn_lsu_alloc_fu_type_i,
    input logic [LSU_DISP_WIDTH-1:0][ROB_TAG_WIDTH-1:0] rn_lsu_alloc_rob_tag_i,
    output logic [LSU_DISP_WIDTH-1:0][LSQ_TAG_WIDTH-1:0] rn_lsu_alloc_lsq_tag_o,
    output logic [LSU_DISP_WIDTH-1:0] rn_lsu_alloc_rdy_o,
    // AGU -> LSU : Issue
    input logic [LSU_ADDR_PIPE_COUNT-1:0] agu_iq_lsu_issue_vld_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0] agu_iq_lsu_issue_excp_vld_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] agu_iq_lsu_issue_rob_tag_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][LSQ_TAG_WIDTH-1:0] agu_iq_lsu_issue_lsq_tag_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][AGU_IQ_TAG_WIDHT-1:0] agu_iq_lsu_issue_entry_tag_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][LSU_TYPE_WIDTH-1:0] agu_iq_lsu_issue_fu_type_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][LSU_OP_WIDTH-1:0] agu_iq_lsu_issue_opcode_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][VADDR_WIDTH-1:0] agu_iq_lsu_issue_vaddr_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] agu_iq_lsu_issue_prd_i,
    output logic [LSU_ADDR_PIPE_COUNT-1:0] agu_iq_lsu_issue_rdy_o,
    // SDU -> LSU(STQ) : Issue
    input logic [LSU_DATA_PIPE_COUNT-1:0] sdu_iq_lsu_issue_vld_i,
    input logic [LSU_DATA_PIPE_COUNT-1:0][STQ_TAG_WIDTH-1:0] sdu_iq_lsu_issue_stq_tag_i,
    input logic [LSU_DATA_PIPE_COUNT-1:0][XLEN-1:0] sdu_iq_lsu_issue_data_i,
    // LS Pipe -> AGU IQ : Commit IQ entry
    output logic [LSU_DISP_WIDTH-1:0] ls_pipe_agu_iq_cmt_vld_o,
    output logic [LSU_DISP_WIDTH-1:0][AGU_IQ_TAG_WIDHT-1:0] ls_pipe_agu_iq_cmt_tag_o,
    output logic [LSU_DISP_WIDTH-1:0] ls_pipe_agu_iq_cmt_dealloc_o,
    output logic [LSU_DISP_WIDTH-1:0] ls_pipe_agu_iq_cmt_reactivate_o,
    // D$ -> ROB : Write Back
    output logic [LSU_ADDR_PIPE_COUNT+LSU_DATA_PIPE_COUNT-1:0] l1d_rob_wb_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT+LSU_DATA_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] l1d_rob_wb_rob_tag_o,
    // LSU -> CDB : Wake Up
    output logic [LSU_ADDR_PIPE_COUNT-1:0] lsu_cdb_wakeup_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] lsu_cdb_wakeup_ptag_o,
    // LSU -> PRF : Write back
    output logic [LSU_ADDR_PIPE_COUNT-1:0] lsu_prf_wr_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] lsu_prf_wr_ptag_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][XLEN-1:0] lsu_prf_wr_data_o,
    // LS Pipe -> ROB : Report exception
    output logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_rob_report_excp_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][EXCP_CAUSE_WIDTH-1:0] ls_pipe_rob_report_excp_cause_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][EXCP_TVAL_WIDTH-1:0] ls_pipe_rob_report_excp_tval_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_rob_report_excp_rob_tag_o,
    // L1 TLB -> STLB : Look up Request
    output logic l1_tlb_stlb_req_vld_o,
    output logic [DTLB_TRANS_ID_WIDTH-1:0] l1_tlb_stlb_req_trans_id_o,
    output logic [ASID_WIDTH-1:0] l1_tlb_stlb_req_asid_o,
    output logic [PMA_ACCESS_WIDTH-1:0] l1_tlb_stlb_req_access_type_o,
    output logic [VPN_WIDTH-1:0] l1_tlb_stlb_req_vpn_o,
    input logic l1_tlb_stlb_req_rdy_i,
    // STLB -> L1 TLB : Look up Response
    input logic l1_tlb_stlb_resp_vld_i,
    input logic [DTLB_TRANS_ID_WIDTH-1:0] l1_tlb_stlb_resp_trans_id_i,
    input logic [ASID_WIDTH-1:0] l1_tlb_stlb_resp_asid_i,
    input logic [PTE_LVL_WIDTH-1:0] l1_tlb_stlb_resp_pte_lvl_i,
    input logic [XLEN-1:0] l1_tlb_stlb_resp_pte_i,
    input logic [PMA_ACCESS_WIDTH-1:0] l1_tlb_stlb_resp_access_type_i,
    input logic [VPN_WIDTH-1:0] l1_tlb_stlb_resp_vpn_i,
    input logic l1_tlb_stlb_resp_access_fault_i,
    input logic l1_tlb_stlb_resp_page_fault_i,
    output logic l1_tlb_stlb_resp_rdy_o,
    // DTLB Evict
    output dtlb_evict_vld_o,
    output [PTE_WIDTH-1:0] dtlb_evict_pte_o,
    output [PAGE_LVL_WIDTH-1:0] dtlb_evict_page_lvl_o,
    output [VPN_WIDTH-1:0] dtlb_evict_vpn_o,
    output [ASID_WIDTH-1:0] dtlb_evict_asid_o,
    // AR
    output logic l1d_l2_req_arvalid_o,
    input logic l1d_l2_req_arready_i,
    output cache_mem_if_ar_t l1d_l2_req_ar_o,
    // ewrq -> mem bus
    // AW 
    output logic l1d_l2_req_awvalid_o,
    input logic l1d_l2_req_awready_i,
    output cache_mem_if_aw_t l1d_l2_req_aw_o,
    // W 
    output logic l1d_l2_req_wvalid_o,
    input logic l1d_l2_req_wready_i,
    output cache_mem_if_w_t l1d_l2_req_w_o,
    // L1D -> L2 : Response
    // B
    input logic l2_l1d_resp_bvalid_i,
    output logic l2_l1d_resp_bready_o,
    input cache_mem_if_b_t l2_l1d_resp_b_i,
    // mem bus -> mlfb
    // R
    input logic l2_l1d_resp_rvalid_i,
    output logic l2_l1d_resp_rready_o,
    input cache_mem_if_r_t l2_l1d_resp_r_i,

    // ptw walk request port
    input                     ptw_walk_req_vld_i,
    input  [PTW_ID_WIDTH-1:0] ptw_walk_req_id_i,
    input  [ PADDR_WIDTH-1:0] ptw_walk_req_addr_i,
    output                    ptw_walk_req_rdy_o,
    // ptw walk response port
    output                    ptw_walk_resp_vld_o,
    output [PTW_ID_WIDTH-1:0] ptw_walk_resp_id_o,
    output [   PTE_WIDTH-1:0] ptw_walk_resp_pte_o,
    input                     ptw_walk_resp_rdy_i,


    // BRQ -> STQ : Wake up
    input logic                     brq_empty_i,
    input logic [ROB_TAG_WIDTH-1:0] oldest_br_rob_tag_i,


      // Execute FENCE.I : Flush Dcache
    input logic flush_dcache_req_i,
    output logic flush_dcache_gnt_o,
    // Execute SFENCE.VMA : Flush DTLB
    input logic flush_dtlb_req_i,
    input logic flush_dtlb_use_vpn_i,
    input logic flush_dtlb_use_asid_i,
    input logic [VPN_WIDTH-1:0] flush_dtlb_vpn_i,
    input logic [ASID_WIDTH-1:0] flush_dtlb_asid_i,
    output logic flush_dtlb_gnt_o,

    input logic flush_i,
    input clk,
    input rst
);

  logic ldq_empty;
  logic [ROB_TAG_WIDTH-1:0] oldest_ld_rob_tag;
  logic stq_empty;
  logic [ROB_TAG_WIDTH-1:0] oldest_st_rob_tag;

  // LRQ -> LS Pipe : Issue
  logic [LSU_ADDR_PIPE_COUNT-1:0] lrq_ls_pipe_issue_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0] lrq_ls_pipe_issue_io;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] lrq_ls_pipe_issue_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LDQ_TAG_WIDTH-1:0] lrq_ls_pipe_issue_lrq_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LDU_OP_WIDTH-1:0] lrq_ls_pipe_issue_opcode;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] lrq_ls_pipe_issue_prd;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] lrq_ls_pipe_issue_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0] lrq_ls_pipe_issue_rdy;
  // STQ -> LS Pipe : Issue
  logic [LSU_ADDR_PIPE_COUNT-1:0] stq_ls_pipe_issue_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0] stq_ls_pipe_issue_io;
  logic [LSU_ADDR_PIPE_COUNT-1:0] stq_ls_pipe_issue_is_fence;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] stq_ls_pipe_issue_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][STU_OP_WIDTH-1:0] stq_ls_pipe_issue_opcode;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] stq_ls_pipe_issue_prd;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] stq_ls_pipe_issue_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0][XLEN-1:0] stq_ls_pipe_issue_data;
  logic [LSU_ADDR_PIPE_COUNT-1:0] stq_ls_pipe_issue_rdy;
  logic [LSU_ADDR_PIPE_COUNT-1:0] stq_ls_pipe_issue_io_rdy;

  // LS Pipe -> STQ : Check pending store
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_stq_check_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_stq_check_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_stq_check_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_stq_check_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_stq_check_raw_hazard;
  // LS Pipe -> RAR : Check RAR Ordering Failure
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_rar_check_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_rar_check_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_check_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_rar_check_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_rar_check_fail;
  // LS Pipe -> RAW : Check RAW Ordering Failure
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_raw_check_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_raw_check_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_raw_check_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_raw_check_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_raw_check_fail;
  // LS Pipe -> LS Pipe : Inter Pipeline Check Master
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_inter_check_master_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_inter_check_master_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_inter_check_master_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_inter_check_master_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_inter_check_master_raw_hazard;

  // LS Pipe -> LS Pipe : Inter Pipeline Check Slave
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_inter_check_slave_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_inter_check_slave_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_inter_check_slave_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_inter_check_slave_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_inter_check_slave_raw_hazard;

  // LS Pipe -> RAR : Allocate
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_rar_alloc_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_rar_alloc_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_rar_alloc_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_alloc_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_rar_alloc_rdy;
  // LS Pipe -> RAW : Allocate
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_raw_alloc_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_raw_alloc_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_raw_alloc_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ls_pipe_raw_alloc_len;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_raw_alloc_rdy;

  // LS Pipe -> STQ : Write Back
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_stq_wb_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][STQ_TAG_WIDTH-1:0] ls_pipe_stq_wb_stq_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_pipe_stq_wb_prd;
  logic [LSU_ADDR_PIPE_COUNT-1:0][STU_OP_WIDTH-1:0] ls_pipe_stq_wb_opcode;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_stq_wb_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_stq_wb_io;
  // LS Pipe -> LRQ : Sleep or Deallocate
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_lrq_wb_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_lrq_wb_sleep;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_lrq_wb_dealloc;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LDQ_TAG_WIDTH-1:0] ls_pipe_lrq_wb_lrq_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_pipe_lrq_wb_prd;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LDU_OP_WIDTH-1:0] ls_pipe_lrq_wb_opcode;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ls_pipe_lrq_wb_paddr;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_lrq_wb_io;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_lrq_wb_pending_st;

  // LS Pipe -> DTLB : Translation request
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_req_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PMA_ACCESS_WIDTH-1:0] ls_pipe_dtlb_req_access_type;
  logic [LSU_ADDR_PIPE_COUNT-1:0][VPN_WIDTH-1:0] ls_pipe_dtlb_req_vpn;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_req_rdy;
  // LS Pipe -> DTLB : Translation response
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_resp_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PPN_WIDTH-1:0] ls_pipe_dtlb_resp_ppn;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_resp_excp_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][EXCP_CAUSE_WIDTH-1:0] ls_pipe_dtlb_resp_excp_cause;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_resp_hit;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_resp_miss;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_dtlb_resp_rdy;

  // LS Pipe -> D$ : Load request
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_ld_req_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_ld_req_io;
  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_l1d_ld_req_rob_tag;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_pipe_l1d_ld_req_prd;
  logic [LSU_ADDR_PIPE_COUNT-1:0][LDU_OP_WIDTH-1:0] ls_pipe_l1d_ld_req_opcode;
  logic [LSU_ADDR_PIPE_COUNT-1:0][L1D_INDEX_WIDTH_CORE-1:0] ls_pipe_l1d_ld_req_index_mid;
  logic [LSU_ADDR_PIPE_COUNT-1:0][L1D_TAG_WIDTH_CORE-1:0] ls_pipe_l1d_ld_req_tag_mid;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_ld_req_rdy;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_ld_io_req_rdy;
  // LS Pipe -> D$ : DTLB response
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_dtlb_resp_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0][PPN_WIDTH-1:0] ls_pipe_l1d_dtlb_resp_ppn;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_dtlb_resp_excp_vld;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_dtlb_resp_hit;
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_dtlb_resp_miss;
  // LS Pipe -> D$ : Store request
  logic [LSU_DATA_PIPE_COUNT-1:0] ls_pipe_l1d_st_req_vld;
  logic [LSU_DATA_PIPE_COUNT-1:0] ls_pipe_l1d_st_req_io;
  logic [LSU_DATA_PIPE_COUNT-1:0] ls_pipe_l1d_st_req_is_fence;
  logic [LSU_DATA_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0] ls_pipe_l1d_st_req_rob_tag;
  logic [LSU_DATA_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_pipe_l1d_st_req_prd;
  logic [LSU_DATA_PIPE_COUNT-1:0][STU_OP_WIDTH-1:0] ls_pipe_l1d_st_req_opcode;
  logic [LSU_DATA_PIPE_COUNT-1:0][L1D_INDEX_WIDTH_CORE-1:0] ls_pipe_l1d_st_req_index_mid;
  logic [LSU_DATA_PIPE_COUNT-1:0][L1D_TAG_WIDTH_CORE-1:0] ls_pipe_l1d_st_req_tag_mid;
  logic [LSU_DATA_PIPE_COUNT-1:0][XLEN-1:0] ls_pipe_l1d_st_req_data;
  logic [LSU_DATA_PIPE_COUNT-1:0] ls_pipe_l1d_st_req_rdy;
  logic [LSU_DATA_PIPE_COUNT-1:0] ls_pipe_l1d_st_io_req_rdy;
  // L1D -> LS Pipe : D-Cache MSHR Full, Replay load
  logic [LSU_ADDR_PIPE_COUNT-1:0] l1d_ls_pipe_replay_mshr_full;
  // LS Pipe -> L1D : Kill D-Cache Response
  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_l1d_kill_resp;


  logic [LSU_ALLOC_WIDTH-1:0] stq_alloc_rdy, lrq_alloc_rdy;
  logic [LSU_ALLOC_WIDTH-1:0] alloc_st_type, alloc_ld_type;
  logic [LSU_ALLOC_WIDTH-1:0][STQ_TAG_WIDTH-1:0] alloc_stq_tag;
  logic [LSU_ALLOC_WIDTH-1:0][ STU_OP_WIDTH-1:0] alloc_stq_minor_op;
  logic [LSU_ALLOC_WIDTH-1:0][LDQ_TAG_WIDTH-1:0] alloc_lrq_tag;

  generate
    for (genvar i = 0; i < LSU_ALLOC_WIDTH; i++) begin
      assign alloc_ld_type[i] = rn_lsu_alloc_fu_type_i[i] == LSU_LD_TYPE;
      assign alloc_st_type[i] = ~alloc_ld_type[i];
      assign alloc_stq_minor_op[i] = rn_lsu_alloc_minor_op_i[i][STU_OP_WIDTH-1:0];
      assign rn_lsu_alloc_rdy_o[i] = stq_alloc_rdy & lrq_alloc_rdy;
      assign rn_lsu_alloc_lsq_tag_o[i] = alloc_ld_type[i] ? alloc_lrq_tag[i] : alloc_stq_tag[i];
    end
  endgenerate

  assign ls_pipe_dtlb_resp_miss            = ~ls_pipe_dtlb_resp_hit;

  assign ls_pipe_inter_check_slave_vld     = ls_pipe_inter_check_master_vld;
  assign ls_pipe_inter_check_slave_rob_tag = ls_pipe_inter_check_master_rob_tag;
  assign ls_pipe_inter_check_slave_len     = ls_pipe_inter_check_master_len;
  assign ls_pipe_inter_check_slave_paddr   = ls_pipe_inter_check_master_paddr;

  assign lsu_cdb_wakeup_vld_o              = lsu_prf_wr_vld_o;
  assign lsu_cdb_wakeup_ptag_o             = lsu_prf_wr_ptag_o;

  generate
    for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
      always_comb begin
        for (int j = 0; j < LSU_ADDR_PIPE_COUNT; j++) begin
          ls_pipe_inter_check_master_raw_hazard[i][j] = ls_pipe_inter_check_slave_raw_hazard[j][i];
        end
      end
    end
  endgenerate


  rvh_stq u_rvh_stq (
      .disp_stq_alloc_vld_i(rn_lsu_alloc_vld_i & alloc_st_type),
      .disp_stq_alloc_is_fence_i(rn_lsu_alloc_is_fence_i),
      .disp_stq_alloc_minor_op_i(alloc_stq_minor_op),
      .disp_stq_alloc_rob_tag_i(rn_lsu_alloc_rob_tag_i),
      .tail_stq_tag_o(alloc_stq_tag),
      .disp_stq_alloc_rdy_o(stq_alloc_rdy),
      .agu_stq_issue_vld_i(ls_pipe_stq_wb_vld),
      .agu_stq_issue_io_region_i(ls_pipe_stq_wb_io),
      .agu_stq_issue_prd_i(ls_pipe_stq_wb_prd),
      .agu_stq_issue_opcode_i(ls_pipe_stq_wb_opcode),
      .agu_stq_issue_paddr_i(ls_pipe_stq_wb_paddr),
      .agu_stq_issue_stq_tag_i(ls_pipe_stq_wb_stq_tag),
      .sdu_stq_issue_vld_i(sdu_iq_lsu_issue_vld_i),
      .sdu_stq_issue_stq_tag_i(sdu_iq_lsu_issue_stq_tag_i),
      .sdu_stq_issue_data_i(sdu_iq_lsu_issue_data_i),
      .ldu_stq_check_vld_i(ls_pipe_stq_check_vld),
      .ldu_stq_check_rob_tag_i(ls_pipe_stq_check_rob_tag),
      .ldu_stq_check_len_i(ls_pipe_stq_check_len),
      .ldu_stq_check_paddr_i(ls_pipe_stq_check_paddr),
      .ldu_stq_check_raw_hazard_o(ls_pipe_stq_check_raw_hazard),
      .lrq_empty_i(ldq_empty),
      .oldest_ld_rob_tag_i(oldest_ld_rob_tag),
      .brq_empty_i(brq_empty_i),
      .oldest_br_rob_tag_i(oldest_br_rob_tag_i),
      .sb_empty_i(1'b1),
      .stq_empty_o(stq_empty),
      .oldest_st_rob_tag_o(oldest_st_rob_tag),
      .stq_l1d_exec_vld_o(stq_ls_pipe_issue_vld),
      .stq_l1d_exec_rob_tag_o(stq_ls_pipe_issue_rob_tag),
      .stq_l1d_exec_io_region_o(stq_ls_pipe_issue_io),
      .stq_l1d_exec_is_fence_o(stq_ls_pipe_issue_is_fence),
      .stq_l1d_exec_prd_o(stq_ls_pipe_issue_prd),
      .stq_l1d_exec_opcode_o(stq_ls_pipe_issue_opcode),
      .stq_l1d_exec_paddr_o(stq_ls_pipe_issue_paddr),
      .stq_l1d_exec_data_o(stq_ls_pipe_issue_data),
      .stq_l1d_exec_rdy_i(stq_ls_pipe_issue_rdy),
      .stq_l1d_exec_io_rdy_i(stq_ls_pipe_issue_io_rdy),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_lrq u_rvh_lrq (
      .disp_lrq_alloc_vld_i(rn_lsu_alloc_vld_i & alloc_ld_type),
      .disp_lrq_alloc_rob_tag_i(rn_lsu_alloc_rob_tag_i),
      .disp_lrq_alloc_tag_o(alloc_lrq_tag),
      .disp_lrq_alloc_rdy_o(lrq_alloc_rdy),
      .ld_resp_lrq_tag_i(ls_pipe_lrq_wb_lrq_tag),
      .ld_resp_dealloc_vld_i(ls_pipe_lrq_wb_dealloc & ls_pipe_lrq_wb_vld),
      .ld_resp_sleep_vld_i(ls_pipe_lrq_wb_sleep & ls_pipe_lrq_wb_vld),
      .ld_resp_sleep_prd_i(ls_pipe_lrq_wb_prd),
      .ld_resp_sleep_opcode_i(ls_pipe_lrq_wb_opcode),
      .ld_resp_sleep_paddr_i(ls_pipe_lrq_wb_paddr),
      .ld_resp_sleep_io_region_i(ls_pipe_lrq_wb_io),
      .ld_resp_sleep_raw_hazard_i(ls_pipe_lrq_wb_pending_st),
      .lrq_l1d_replay_vld_o(lrq_ls_pipe_issue_vld),
      .lrq_l1d_replay_ldq_tag_o(lrq_ls_pipe_issue_lrq_tag),
      .lrq_l1d_replay_rob_tag_o(lrq_ls_pipe_issue_rob_tag),
      .lrq_l1d_replay_io_region_o(lrq_ls_pipe_issue_io),
      .lrq_l1d_replay_prd_o(lrq_ls_pipe_issue_prd),
      .lrq_l1d_replay_opcode_o(lrq_ls_pipe_issue_opcode),
      .lrq_l1d_replay_paddr_o(lrq_ls_pipe_issue_paddr),
      .lrq_l1d_replay_rdy_i(lrq_ls_pipe_issue_rdy),
      .rar_rdy_i(ls_pipe_rar_alloc_rdy[0]),
      .raw_rdy_i(ls_pipe_raw_alloc_rdy[0]),
      .ldq_empty_o(ldq_empty),
      .oldest_ld_rob_tag_o(oldest_ld_rob_tag),
      .stq_empty_i(stq_empty),
      .oldest_st_rob_tag_i(oldest_st_rob_tag),
      .brq_empty_i(brq_empty_i),
      .oldest_br_rob_tag_i(oldest_br_rob_tag_i),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_rar_queue u_rvh_rar_queue (
      .ls_pipe_rar_alloc_vld_i(ls_pipe_rar_alloc_vld),
      .ls_pipe_rar_alloc_rob_tag_i(ls_pipe_rar_alloc_rob_tag),
      .ls_pipe_rar_alloc_paddr_i(ls_pipe_rar_alloc_paddr),
      .ls_pipe_rar_alloc_len_i(ls_pipe_rar_alloc_len),
      .ls_pipe_rar_alloc_rdy_o(ls_pipe_rar_alloc_rdy),
      .ls_pipe_rar_check_vld_i(ls_pipe_rar_check_vld),
      .ls_pipe_rar_check_rob_tag_i(ls_pipe_rar_check_rob_tag),
      .ls_pipe_rar_check_len_i(ls_pipe_rar_check_len),
      .ls_pipe_rar_check_paddr_i(ls_pipe_rar_check_paddr),
      .ls_pipe_rar_check_fail_o(ls_pipe_rar_check_fail),
      .ldq_empty_i(ldq_empty),
      .oldest_ld_rob_tag_i(oldest_ld_rob_tag),
      .l1d_rar_invld_vld_i('0),
      .l1d_rar_invld_tag_i('0),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_raw_queue u_rvh_raw_queue (
      .ls_pipe_raw_alloc_vld_i(ls_pipe_raw_alloc_vld),
      .ls_pipe_raw_alloc_rob_tag_i(ls_pipe_raw_alloc_rob_tag),
      .ls_pipe_raw_alloc_paddr_i(ls_pipe_raw_alloc_paddr),
      .ls_pipe_raw_alloc_len_i(ls_pipe_raw_alloc_len),
      .ls_pipe_raw_alloc_rdy_o(ls_pipe_raw_alloc_rdy),
      .ls_pipe_raw_check_vld_i(ls_pipe_raw_check_vld),
      .ls_pipe_raw_check_rob_tag_i(ls_pipe_raw_check_rob_tag),
      .ls_pipe_raw_check_len_i(ls_pipe_raw_check_len),
      .ls_pipe_raw_check_paddr_i(ls_pipe_raw_check_paddr),
      .ls_pipe_raw_check_fail_o(ls_pipe_raw_check_fail),
      .stq_empty_i(stq_empty),
      .oldest_st_rob_tag_i(oldest_st_rob_tag),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_dtlb #(
      .TRANSLATE_WIDTH(LSU_ADDR_PIPE_COUNT),
      .ENTRY_COUNT(DTLB_ENTRY_COUNT),
      .MSHR_COUNT(DTLB_MSHR_COUNT),
      .TRANS_ID_WIDTH(DTLB_TRANS_ID_WIDTH),
      .PADDR_WIDTH(PADDR_WIDTH),
      .EXCP_CAUSE_WIDTH(EXCP_CAUSE_WIDTH),
      .VPN_WIDTH(VPN_WIDTH),
      .ASID_WIDTH(ASID_WIDTH)
  ) u_rvh_dtlb (
      .priv_lvl_i(misc_priv_lvl_i),
      .mstatus_mprv(misc_mstatus_i[17]),
      .mstatus_mpp(misc_mstatus_i[12:11]),
      .mstatus_mxr(misc_mstatus_i[19]),
      .mstatus_sum(misc_mstatus_i[18]),
      .satp_mode_i(misc_satp_i[63:60]),
      .satp_asid_i(misc_satp_i[59:44]),
      .translate_req_vld_i(ls_pipe_dtlb_req_vld),
      .translate_req_access_type_i(ls_pipe_dtlb_req_access_type),
      .translate_req_vpn_i(ls_pipe_dtlb_req_vpn),
      .translate_req_rdy_o(ls_pipe_dtlb_req_rdy),
      .translate_resp_vld_o(ls_pipe_dtlb_resp_vld),
      .translate_resp_ppn_o(ls_pipe_dtlb_resp_ppn),
      .translate_resp_excp_vld_o(ls_pipe_dtlb_resp_excp_vld),
      .translate_resp_excp_cause_o(ls_pipe_dtlb_resp_excp_cause),
      .translate_resp_miss_o(),
      .translate_resp_hit_o(ls_pipe_dtlb_resp_hit),
      .next_lvl_req_vld_o(l1_tlb_stlb_req_vld_o),
      .next_lvl_req_trans_id_o(l1_tlb_stlb_req_trans_id_o),
      .next_lvl_req_asid_o(l1_tlb_stlb_req_asid_o),
      .next_lvl_req_vpn_o(l1_tlb_stlb_req_vpn_o),
      .next_lvl_req_access_type_o(l1_tlb_stlb_req_access_type_o),
      .next_lvl_req_rdy_i(l1_tlb_stlb_req_rdy_i),
      .next_lvl_resp_vld_i(l1_tlb_stlb_resp_vld_i),
      .next_lvl_resp_trans_id_i(l1_tlb_stlb_resp_trans_id_i),
      .next_lvl_resp_asid_i(l1_tlb_stlb_resp_asid_i),
      .next_lvl_resp_pte_i(l1_tlb_stlb_resp_pte_i),
      .next_lvl_resp_page_lvl_i(l1_tlb_stlb_resp_pte_lvl_i),
      .next_lvl_resp_vpn_i(l1_tlb_stlb_resp_vpn_i),
      .next_lvl_resp_access_type_i(l1_tlb_stlb_resp_access_type_i),
      .next_lvl_resp_access_fault_i(l1_tlb_stlb_resp_access_fault_i),
      .next_lvl_resp_page_fault_i(l1_tlb_stlb_resp_page_fault_i),
      .tlb_evict_vld_o(dtlb_evict_vld_o),
      .tlb_evict_pte_o(dtlb_evict_pte_o),
      .tlb_evict_page_lvl_o(dtlb_evict_page_lvl_o),
      .tlb_evict_vpn_o(dtlb_evict_vpn_o),
      .tlb_evict_asid_o(dtlb_evict_asid_o),
      .tlb_flush_vld_i(flush_dtlb_req_i),
      .tlb_flush_use_asid_i(flush_dtlb_use_asid_i),
      .tlb_flush_use_vpn_i(flush_dtlb_use_vpn_i),
      .tlb_flush_vpn_i(flush_dtlb_vpn_i),
      .tlb_flush_asid_i(flush_dtlb_asid_i),
      .tlb_flush_grant_o(flush_dtlb_gnt_o),
      .clk(clk),
      .rstn(~rst)
  );

  logic                                                             l1d_l2_arvalid;
  logic                                                             l1d_l2_arready;
  cache_mem_if_ar_t                                                 l1d_l2_ar;

  logic                                                             l1d_l2_rvalid;
  logic                                                             l1d_l2_rready;
  cache_mem_if_r_t                                                  l1d_l2_r;

  logic                                                             l1d_l2_awvalid;
  logic                                                             l1d_l2_awready;
  cache_mem_if_aw_t                                                 l1d_l2_aw;

  logic                                                             l1d_l2_wvalid;
  logic                                                             l1d_l2_wready;
  cache_mem_if_w_t                                                  l1d_l2_w;

  logic                                                             l1d_l2_bvalid;
  logic                                                             l1d_l2_bready;
  cache_mem_if_b_t                                                  l1d_l2_b;

  logic             [LSU_ADDR_PIPE_COUNT-1:0][ L1D_INDEX_WIDTH-1:0] ls_pipe_l1d_ld_req_index;
  logic             [LSU_ADDR_PIPE_COUNT-1:0][L1D_OFFSET_WIDTH-1:0] ls_pipe_l1d_ld_req_offset;
  logic             [LSU_ADDR_PIPE_COUNT-1:0][   L1D_TAG_WIDTH-1:0] ls_pipe_l1d_ld_req_vtag;
  logic             [LSU_DATA_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] ls_pipe_l1d_st_req_paddr;

  generate
    for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin : gen_ls_pipe_l1d_ld_req_vaddr
      assign ls_pipe_l1d_ld_req_index   [i] = ls_pipe_l1d_ld_req_index_mid[i][L1D_INDEX_WIDTH+L1D_OFFSET_WIDTH-1:L1D_OFFSET_WIDTH];
      assign ls_pipe_l1d_ld_req_offset[i] = ls_pipe_l1d_ld_req_index_mid[i][L1D_OFFSET_WIDTH-1:0];
      assign ls_pipe_l1d_ld_req_vtag[i] = ls_pipe_l1d_ld_req_tag_mid[i];
    end
  endgenerate

  generate
    for (genvar i = 0; i < LSU_DATA_PIPE_COUNT; i++) begin : gen_ls_pipe_l1d_st_req_paddr
      assign ls_pipe_l1d_st_req_paddr[i] = {
        ls_pipe_l1d_st_req_tag_mid[i][(L1D_TAG_WIDTH_CORE-1)-:L1D_TAG_WIDTH],
        ls_pipe_l1d_st_req_index_mid[i]
      };
    end
  endgenerate

  rvh_l1d u_rvh_l1d (
      // LS Pipe -> D$ : Load request
      .ls_pipe_l1d_ld_req_vld_i        (ls_pipe_l1d_ld_req_vld),
      .ls_pipe_l1d_ld_req_io_i         (ls_pipe_l1d_ld_req_io),
      .ls_pipe_l1d_ld_req_rob_tag_i    (ls_pipe_l1d_ld_req_rob_tag),
      .ls_pipe_l1d_ld_req_prd_i        (ls_pipe_l1d_ld_req_prd),
      .ls_pipe_l1d_ld_req_opcode_i     (ls_pipe_l1d_ld_req_opcode),
      .ls_pipe_l1d_ld_req_index_i      (ls_pipe_l1d_ld_req_index),
      .ls_pipe_l1d_ld_req_offset_i     (ls_pipe_l1d_ld_req_offset),
      .ls_pipe_l1d_ld_req_vtag_i       (ls_pipe_l1d_ld_req_vtag),
      .ls_pipe_l1d_ld_req_rdy_o        (ls_pipe_l1d_ld_req_rdy),
      .ls_pipe_l1d_ld_io_req_rdy_o     (ls_pipe_l1d_ld_io_req_rdy),
      // LS Pipe -> D$ : DTLB response
      .ls_pipe_l1d_dtlb_resp_vld_i     (ls_pipe_l1d_dtlb_resp_vld),
      .ls_pipe_l1d_dtlb_resp_ppn_i     (ls_pipe_l1d_dtlb_resp_ppn),
      .ls_pipe_l1d_dtlb_resp_excp_vld_i(ls_pipe_l1d_dtlb_resp_excp_vld),
      .ls_pipe_l1d_dtlb_resp_hit_i     (ls_pipe_l1d_dtlb_resp_hit),
      .ls_pipe_l1d_dtlb_resp_miss_i    (ls_pipe_l1d_dtlb_resp_miss),
      // LS Pipe -> D$ : Store request
      .ls_pipe_l1d_st_req_vld_i        (ls_pipe_l1d_st_req_vld),
      .ls_pipe_l1d_st_req_io_i         (ls_pipe_l1d_st_req_io),
      .ls_pipe_l1d_st_req_is_fence_i   (ls_pipe_l1d_st_req_is_fence),
      .ls_pipe_l1d_st_req_rob_tag_i    (ls_pipe_l1d_st_req_rob_tag),
      .ls_pipe_l1d_st_req_prd_i        (ls_pipe_l1d_st_req_prd),
      .ls_pipe_l1d_st_req_opcode_i     (ls_pipe_l1d_st_req_opcode),
      // .ls_pipe_l1d_st_req_index_i             (ls_pipe_l1d_st_req_index),
      // .ls_pipe_l1d_st_req_tag_i               (ls_pipe_l1d_st_req_tag),
      .ls_pipe_l1d_st_req_paddr_i      (ls_pipe_l1d_st_req_paddr),
      .ls_pipe_l1d_st_req_data_i       (ls_pipe_l1d_st_req_data),
      .ls_pipe_l1d_st_req_rdy_o        (ls_pipe_l1d_st_req_rdy),
      .ls_pipe_l1d_st_io_req_rdy_o     (ls_pipe_l1d_st_io_req_rdy),
      // L1D -> LS Pipe : D-Cache MSHR Full, Replay load
      .l1d_ls_pipe_ld_replay_valid_o   (l1d_ls_pipe_replay_mshr_full),
      // LS Pipe -> L1D : Kill D-Cache Response
      .ls_pipe_l1d_kill_resp_i         (ls_pipe_l1d_kill_resp),
      // D$ -> ROB : Write Back
      .l1d_rob_wb_vld_o                (l1d_rob_wb_vld_o),
      .l1d_rob_wb_rob_tag_o            (l1d_rob_wb_rob_tag_o),
      // D$ -> Int PRF : Write Back
      .l1d_int_prf_wb_vld_o            (lsu_prf_wr_vld_o),
      .l1d_int_prf_wb_tag_o            (lsu_prf_wr_ptag_o),
      .l1d_int_prf_wb_data_o           (lsu_prf_wr_data_o),

      // L1D -> L2 : Request
      // mshr -> mem bus
      // AR
      .l1d_l2_req_arvalid_o(l1d_l2_req_arvalid_o),
      .l1d_l2_req_arready_i(l1d_l2_req_arready_i),
      .l1d_l2_req_ar_o     (l1d_l2_req_ar_o),
      // ewrq -> mem bus                                
      // AW                                             
      .l1d_l2_req_awvalid_o(l1d_l2_req_awvalid_o),
      .l1d_l2_req_awready_i(l1d_l2_req_awready_i),
      .l1d_l2_req_aw_o     (l1d_l2_req_aw_o),
      // W                                              
      .l1d_l2_req_wvalid_o (l1d_l2_req_wvalid_o),
      .l1d_l2_req_wready_i (l1d_l2_req_wready_i),
      .l1d_l2_req_w_o      (l1d_l2_req_w_o),
      // L1D -> L2 : Response                             
      // B                             
      .l2_l1d_resp_bvalid_i(l2_l1d_resp_bvalid_i),
      .l2_l1d_resp_bready_o(l2_l1d_resp_bready_o),
      .l2_l1d_resp_b_i     (l2_l1d_resp_b_i),
      // mem bus -> mlfb                                
      // R                             
      .l2_l1d_resp_rvalid_i(l2_l1d_resp_rvalid_i),
      .l2_l1d_resp_rready_o(l2_l1d_resp_rready_o),
      .l2_l1d_resp_r_i     (l2_l1d_resp_r_i),
      .ptw_walk_req_vld_i  (ptw_walk_req_vld_i),
      .ptw_walk_req_id_i   (ptw_walk_req_id_i),
      .ptw_walk_req_addr_i (ptw_walk_req_addr_i),
      .ptw_walk_req_rdy_o  (ptw_walk_req_rdy_o),
      .ptw_walk_resp_vld_o (ptw_walk_resp_vld_o),
      .ptw_walk_resp_id_o  (ptw_walk_resp_id_o),
      .ptw_walk_resp_pte_o (ptw_walk_resp_pte_o),
      .ptw_walk_resp_rdy_i (ptw_walk_resp_rdy_i),

      .rob_flush_i  (flush_i),

      .fencei_flush_vld_i  (flush_dcache_req_i),
      .fencei_flush_grant_o(flush_dcache_gnt_o),

      .clk          (clk),
      .rst          (~rst)
  );


  generate
    for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin : LSU_PIPE

      rvh_ls_pipe u_rvh_ls_pipe (
          .agu_iq_ls_pipe_issue_vld_i(agu_iq_lsu_issue_vld_i[i]),
          .agu_iq_ls_pipe_issue_excp_vld_i(agu_iq_lsu_issue_excp_vld_i[i]),
          .agu_iq_ls_pipe_issue_rob_tag_i(agu_iq_lsu_issue_rob_tag_i[i]),
          .agu_iq_ls_pipe_issue_lsq_tag_i(agu_iq_lsu_issue_lsq_tag_i[i]),
          .agu_iq_ls_pipe_issue_entry_tag_i(agu_iq_lsu_issue_entry_tag_i[i]),
          .agu_iq_ls_pipe_issue_fu_type_i(agu_iq_lsu_issue_fu_type_i[i]),
          .agu_iq_ls_pipe_issue_opcode_i(agu_iq_lsu_issue_opcode_i[i]),
          .agu_iq_ls_pipe_issue_vaddr_i(agu_iq_lsu_issue_vaddr_i[i]),
          .agu_iq_ls_pipe_issue_prd_i(agu_iq_lsu_issue_prd_i[i]),
          .agu_iq_ls_pipe_issue_rdy_o(agu_iq_lsu_issue_rdy_o[i]),
          .lrq_ls_pipe_issue_vld_i(lrq_ls_pipe_issue_vld[i]),
          .lrq_ls_pipe_issue_io_i(lrq_ls_pipe_issue_io[i]),
          .lrq_ls_pipe_issue_rob_tag_i(lrq_ls_pipe_issue_rob_tag[i]),
          .lrq_ls_pipe_issue_lrq_tag_i(lrq_ls_pipe_issue_lrq_tag[i]),
          .lrq_ls_pipe_issue_opcode_i(lrq_ls_pipe_issue_opcode[i]),
          .lrq_ls_pipe_issue_prd_i(lrq_ls_pipe_issue_prd[i]),
          .lrq_ls_pipe_issue_paddr_i(lrq_ls_pipe_issue_paddr[i]),
          .lrq_ls_pipe_issue_rdy_o(lrq_ls_pipe_issue_rdy[i]),
          .stq_ls_pipe_issue_vld_i(stq_ls_pipe_issue_vld[i]),
          .stq_ls_pipe_issue_io_i(stq_ls_pipe_issue_io[i]),
          .stq_ls_pipe_issue_is_fence_i(stq_ls_pipe_issue_is_fence[i]),
          .stq_ls_pipe_issue_rob_tag_i(stq_ls_pipe_issue_rob_tag[i]),
          .stq_ls_pipe_issue_opcode_i(stq_ls_pipe_issue_opcode[i]),
          .stq_ls_pipe_issue_prd_i(stq_ls_pipe_issue_prd[i]),
          .stq_ls_pipe_issue_paddr_i(stq_ls_pipe_issue_paddr[i]),
          .stq_ls_pipe_issue_data_i(stq_ls_pipe_issue_data[i]),
          .stq_ls_pipe_issue_rdy_o(stq_ls_pipe_issue_rdy[i]),
          .stq_ls_pipe_issue_io_rdy_o(stq_ls_pipe_issue_io_rdy[i]),
          .ls_pipe_dtlb_req_vld_o(ls_pipe_dtlb_req_vld[i]),
          .ls_pipe_dtlb_req_access_type_o(ls_pipe_dtlb_req_access_type[i]),
          .ls_pipe_dtlb_req_vpn_o(ls_pipe_dtlb_req_vpn[i]),
          .ls_pipe_dtlb_req_rdy_i(ls_pipe_dtlb_req_rdy[i]),
          .ls_pipe_dtlb_resp_vld_i(ls_pipe_dtlb_resp_vld[i]),
          .ls_pipe_dtlb_resp_ppn_i(ls_pipe_dtlb_resp_ppn[i]),
          .ls_pipe_dtlb_resp_excp_vld_i(ls_pipe_dtlb_resp_excp_vld[i]),
          .ls_pipe_dtlb_resp_excp_cause_i(ls_pipe_dtlb_resp_excp_cause[i]),
          .ls_pipe_dtlb_resp_hit_i(ls_pipe_dtlb_resp_hit[i]),
          .ls_pipe_dtlb_resp_miss_i(ls_pipe_dtlb_resp_miss[i]),
          .ls_pipe_dtlb_resp_rdy_o(ls_pipe_dtlb_resp_rdy[i]),
          .ls_pipe_agu_iq_cmt_vld_o(ls_pipe_agu_iq_cmt_vld_o[i]),
          .ls_pipe_agu_iq_cmt_tag_o(ls_pipe_agu_iq_cmt_tag_o[i]),
          .ls_pipe_agu_iq_cmt_dealloc_o(ls_pipe_agu_iq_cmt_dealloc_o[i]),
          .ls_pipe_agu_iq_cmt_reactivate_o(ls_pipe_agu_iq_cmt_reactivate_o[i]),
          .ls_pipe_l1d_ld_req_vld_o(ls_pipe_l1d_ld_req_vld[i]),
          .ls_pipe_l1d_ld_req_io_o(ls_pipe_l1d_ld_req_io[i]),
          .ls_pipe_l1d_ld_req_rob_tag_o(ls_pipe_l1d_ld_req_rob_tag[i]),
          .ls_pipe_l1d_ld_req_prd_o(ls_pipe_l1d_ld_req_prd[i]),
          .ls_pipe_l1d_ld_req_opcode_o(ls_pipe_l1d_ld_req_opcode[i]),
          .ls_pipe_l1d_ld_req_index_o(ls_pipe_l1d_ld_req_index_mid[i]),
          .ls_pipe_l1d_ld_req_tag_o(ls_pipe_l1d_ld_req_tag_mid[i]),
          .ls_pipe_l1d_ld_req_rdy_i(ls_pipe_l1d_ld_req_rdy[i]),
          .ls_pipe_l1d_ld_io_req_rdy_i(ls_pipe_l1d_ld_io_req_rdy[i]),
          .ls_pipe_l1d_dtlb_resp_vld_o(ls_pipe_l1d_dtlb_resp_vld[i]),
          .ls_pipe_l1d_dtlb_resp_ppn_o(ls_pipe_l1d_dtlb_resp_ppn[i]),
          .ls_pipe_l1d_dtlb_resp_excp_vld_o(ls_pipe_l1d_dtlb_resp_excp_vld[i]),
          .ls_pipe_l1d_dtlb_resp_hit_o(ls_pipe_l1d_dtlb_resp_hit[i]),
          .ls_pipe_l1d_dtlb_resp_miss_o(ls_pipe_l1d_dtlb_resp_miss[i]),
          .ls_pipe_l1d_st_req_vld_o(ls_pipe_l1d_st_req_vld[i]),
          .ls_pipe_l1d_st_req_io_o(ls_pipe_l1d_st_req_io[i]),
          .ls_pipe_l1d_st_req_is_fence_o(ls_pipe_l1d_st_req_is_fence[i]),
          .ls_pipe_l1d_st_req_rob_tag_o(ls_pipe_l1d_st_req_rob_tag[i]),
          .ls_pipe_l1d_st_req_prd_o(ls_pipe_l1d_st_req_prd[i]),
          .ls_pipe_l1d_st_req_opcode_o(ls_pipe_l1d_st_req_opcode[i]),
          .ls_pipe_l1d_st_req_index_o(ls_pipe_l1d_st_req_index_mid[i]),
          .ls_pipe_l1d_st_req_tag_o(ls_pipe_l1d_st_req_tag_mid[i]),
          .ls_pipe_l1d_st_req_data_o(ls_pipe_l1d_st_req_data[i]),
          .ls_pipe_l1d_st_req_rdy_i(ls_pipe_l1d_st_req_rdy[i]),
          .ls_pipe_l1d_st_io_req_rdy_i(ls_pipe_l1d_st_io_req_rdy[i]),
          .ls_pipe_stq_check_vld_o(ls_pipe_stq_check_vld[i]),
          .ls_pipe_stq_check_rob_tag_o(ls_pipe_stq_check_rob_tag[i]),
          .ls_pipe_stq_check_len_o(ls_pipe_stq_check_len[i]),
          .ls_pipe_stq_check_paddr_o(ls_pipe_stq_check_paddr[i]),
          .ls_pipe_stq_check_raw_hazard_i(ls_pipe_stq_check_raw_hazard[i]),
          .ls_pipe_rar_check_vld_o(ls_pipe_rar_check_vld[i]),
          .ls_pipe_rar_check_rob_tag_o(ls_pipe_rar_check_rob_tag[i]),
          .ls_pipe_rar_check_len_o(ls_pipe_rar_check_len[i]),
          .ls_pipe_rar_check_paddr_o(ls_pipe_rar_check_paddr[i]),
          .ls_pipe_rar_check_fail_i(ls_pipe_rar_check_fail[i]),
          .ls_pipe_raw_check_vld_o(ls_pipe_raw_check_vld[i]),
          .ls_pipe_raw_check_rob_tag_o(ls_pipe_raw_check_rob_tag[i]),
          .ls_pipe_raw_check_len_o(ls_pipe_raw_check_len[i]),
          .ls_pipe_raw_check_paddr_o(ls_pipe_raw_check_paddr[i]),
          .ls_pipe_raw_check_fail_i(ls_pipe_raw_check_fail[i]),
          .ls_pipe_inter_check_master_vld_o(ls_pipe_inter_check_master_vld[i]),
          .ls_pipe_inter_check_master_rob_tag_o(ls_pipe_inter_check_master_rob_tag[i]),
          .ls_pipe_inter_check_master_len_o(ls_pipe_inter_check_master_len[i]),
          .ls_pipe_inter_check_master_paddr_o(ls_pipe_inter_check_master_paddr[i]),
          .ls_pipe_inter_check_master_raw_hazard_i(ls_pipe_inter_check_master_raw_hazard[i]),
          .ls_pipe_inter_check_slave_vld_i(ls_pipe_inter_check_slave_vld),
          .ls_pipe_inter_check_slave_rob_tag_i(ls_pipe_inter_check_slave_rob_tag),
          .ls_pipe_inter_check_slave_len_i(ls_pipe_inter_check_slave_len),
          .ls_pipe_inter_check_slave_paddr_i(ls_pipe_inter_check_slave_paddr),
          .ls_pipe_inter_check_slave_raw_hazard_o(ls_pipe_inter_check_slave_raw_hazard[i]),
          .l1d_ls_pipe_replay_mshr_full_i(l1d_ls_pipe_replay_mshr_full[i]),
          .ls_pipe_l1d_kill_resp_o(ls_pipe_l1d_kill_resp[i]),
          .ls_pipe_rar_alloc_vld_o(ls_pipe_rar_alloc_vld[i]),
          .ls_pipe_rar_alloc_rob_tag_o(ls_pipe_rar_alloc_rob_tag[i]),
          .ls_pipe_rar_alloc_paddr_o(ls_pipe_rar_alloc_paddr[i]),
          .ls_pipe_rar_alloc_len_o(ls_pipe_rar_alloc_len[i]),
          .ls_pipe_rar_alloc_rdy_i(ls_pipe_rar_alloc_rdy[i]),
          .ls_pipe_raw_alloc_vld_o(ls_pipe_raw_alloc_vld[i]),
          .ls_pipe_raw_alloc_rob_tag_o(ls_pipe_raw_alloc_rob_tag[i]),
          .ls_pipe_raw_alloc_paddr_o(ls_pipe_raw_alloc_paddr[i]),
          .ls_pipe_raw_alloc_len_o(ls_pipe_raw_alloc_len[i]),
          .ls_pipe_raw_alloc_rdy_i(ls_pipe_raw_alloc_rdy[i]),
          .ls_pipe_stq_wb_vld_o(ls_pipe_stq_wb_vld[i]),
          .ls_pipe_stq_wb_stq_tag_o(ls_pipe_stq_wb_stq_tag[i]),
          .ls_pipe_stq_wb_prd_o(ls_pipe_stq_wb_prd[i]),
          .ls_pipe_stq_wb_opcode_o(ls_pipe_stq_wb_opcode[i]),
          .ls_pipe_stq_wb_paddr_o(ls_pipe_stq_wb_paddr[i]),
          .ls_pipe_stq_wb_io_o(ls_pipe_stq_wb_io[i]),
          .ls_pipe_lrq_wb_vld_o(ls_pipe_lrq_wb_vld[i]),
          .ls_pipe_lrq_wb_sleep_o(ls_pipe_lrq_wb_sleep[i]),
          .ls_pipe_lrq_wb_dealloc_o(ls_pipe_lrq_wb_dealloc[i]),
          .ls_pipe_lrq_wb_lrq_tag_o(ls_pipe_lrq_wb_lrq_tag[i]),
          .ls_pipe_lrq_wb_prd_o(ls_pipe_lrq_wb_prd[i]),
          .ls_pipe_lrq_wb_opcode_o(ls_pipe_lrq_wb_opcode[i]),
          .ls_pipe_lrq_wb_paddr_o(ls_pipe_lrq_wb_paddr[i]),
          .ls_pipe_lrq_wb_io_o(ls_pipe_lrq_wb_io[i]),
          .ls_pipe_lrq_wb_pending_st_o(ls_pipe_lrq_wb_pending_st[i]),
          .ls_pipe_rob_report_excp_vld_o(ls_pipe_rob_report_excp_vld_o[i]),
          .ls_pipe_rob_report_excp_cause_o(ls_pipe_rob_report_excp_cause_o[i]),
          .ls_pipe_rob_report_excp_tval_o(ls_pipe_rob_report_excp_tval_o[i]),
          .ls_pipe_rob_report_excp_rob_tag_o(ls_pipe_rob_report_excp_rob_tag_o[i]),
          .lrq_empty_i(ldq_empty),
          .oldest_ld_rob_tag_i(oldest_ld_rob_tag),
          .stq_empty_i(stq_empty),
          .oldest_st_rob_tag_i(oldest_st_rob_tag),
          .brq_empty_i(brq_empty_i),
          .oldest_br_rob_tag_i(oldest_br_rob_tag_i),
          .flush_i(flush_i),
          .clk(clk),
          .rst(rst)
      );

    end
  endgenerate

endmodule : rvh_lsu
