package rvh_lsu_pkg;
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;


    localparam int unsigned MEM_OP_LEN_WIDTH = 2;
    localparam logic [1:0] MEM_OP_BYTE = 2'b00;
    localparam logic [1:0] MEM_OP_HALF = 2'b01;
    localparam logic [1:0] MEM_OP_WORD = 2'b11;
    localparam logic [1:0] MEM_OP_DOUBLE = 2'b10;

    function automatic [0:0]is_misc_op;
        input logic [LSU_TYPE_WIDTH-1:0] ls_type;
        input logic [LSU_OP_WIDTH-1:0]   ls_opcode;
        begin
            is_misc_op = 1'b0;
            if(ls_type == LSU_ST_TYPE) begin
                case (ls_opcode)
                    STU_FENCE : is_misc_op  = 1'b1;
                    default:;
                endcase
            end
        end
    endfunction

    function automatic [PMA_ACCESS_WIDTH-1:0]get_access_type;
        input logic [LSU_TYPE_WIDTH-1:0] ls_type;
        input logic [LSU_OP_WIDTH-1:0]   ls_opcode;
        begin
            if(ls_type == LSU_LD_TYPE) begin
                get_access_type = PMA_ACCESS_LOAD;
            end else begin
                get_access_type = PMA_ACCESS_STORE;
            end
        end
    endfunction

    function automatic [MEM_OP_LEN_WIDTH-1:0] get_mem_len;
        input logic [LSU_TYPE_WIDTH-1:0] fu_type_i;
        input logic [LSU_OP_WIDTH-1:0] opcode_i;

        logic is_ld, is_st;
        logic width_8, width_16, width_32, width_64;
        begin
            is_ld    = fu_type_i == LSU_LD_TYPE;
            is_st    = fu_type_i == LSU_ST_TYPE;
            width_8  = (is_ld & (opcode_i == LDU_LB | opcode_i == LDU_LBU)) | (is_st & opcode_i == STU_SB);
            width_16 = (is_ld & (opcode_i == LDU_LH | opcode_i == LDU_LHU)) | (is_st & opcode_i == STU_SH);
            width_32 = (is_ld & (opcode_i == LDU_LW | opcode_i == LDU_LWU)) | (is_st & opcode_i == STU_SW);
            width_64 = (is_ld & (opcode_i == LDU_LD)                      ) | (is_st & opcode_i == STU_SD);

            get_mem_len = (MEM_OP_BYTE & {MEM_OP_LEN_WIDTH{width_8}}) |
                (MEM_OP_HALF & {MEM_OP_LEN_WIDTH{width_16}}) | (MEM_OP_WORD & {MEM_OP_LEN_WIDTH{width_32}}
                ) | (MEM_OP_DOUBLE & {MEM_OP_LEN_WIDTH{width_64}});
        end
    endfunction

    function automatic [MEM_OP_LEN_WIDTH-1:0] get_ld_len;
        input logic [LDU_OP_WIDTH-1:0] opcode_i;

        logic width_8, width_16, width_32, width_64;
        begin
            width_8 = opcode_i == LDU_LB | opcode_i == LDU_LBU;
            width_16 = opcode_i == LDU_LH | opcode_i == LDU_LHU;
            width_32 = opcode_i == LDU_LW | opcode_i == LDU_LWU;
            width_64 = opcode_i == LDU_LD;

            get_ld_len = (MEM_OP_BYTE & {MEM_OP_LEN_WIDTH{width_8}}) |
                (MEM_OP_HALF & {MEM_OP_LEN_WIDTH{width_16}}) | (MEM_OP_WORD & {MEM_OP_LEN_WIDTH{width_32}}
                ) | (MEM_OP_DOUBLE & {MEM_OP_LEN_WIDTH{width_64}});
        end
    endfunction

    function automatic [MEM_OP_LEN_WIDTH-1:0] get_st_len;
        input logic [STU_OP_WIDTH-1:0] opcode_i;

        logic width_8, width_16, width_32, width_64;
        begin
            width_8 = opcode_i == STU_SB;
            width_16 = opcode_i == STU_SH;
            width_32 = opcode_i == STU_SW;
            width_64 = opcode_i == STU_SD;

            get_st_len = (MEM_OP_BYTE & {MEM_OP_LEN_WIDTH{width_8}}) |
                (MEM_OP_HALF & {MEM_OP_LEN_WIDTH{width_16}}) | (MEM_OP_WORD & {MEM_OP_LEN_WIDTH{width_32}}
                ) | (MEM_OP_DOUBLE & {MEM_OP_LEN_WIDTH{width_64}});
        end
    endfunction


    function automatic [7:0] generate_byte_mask_with_len;
        input logic [PADDR_WIDTH-1:0] paddr_i;
        input logic [MEM_OP_LEN_WIDTH-1:0] len_i;

        logic [2:0] offset;
        logic [7:0] mask;

        logic width_8, width_16, width_32, width_64;
        logic off_is0, off_is1, off_is2, off_is3, off_is4, off_is5, off_is6, off_is7;

        begin
            offset = paddr_i[2:0];

            off_is0 = offset == 3'd0;
            off_is1 = offset == 3'd1;
            off_is2 = offset == 3'd2;
            off_is3 = offset == 3'd3;
            off_is4 = offset == 3'd4;
            off_is5 = offset == 3'd5;
            off_is6 = offset == 3'd6;
            off_is7 = offset == 3'd7;

            width_8 = len_i == MEM_OP_BYTE;
            width_16 = len_i == MEM_OP_HALF;
            width_32 = len_i == MEM_OP_WORD;
            width_64 = len_i == MEM_OP_DOUBLE;

            mask[0] = off_is0;
            mask[1] = off_is1 | (off_is0 & ~width_8);
            mask[2] = off_is2 | (off_is0 & (width_32 | width_64));
            mask[3] = off_is3 | (off_is2 & width_16) | off_is0 & (width_32 | width_64);
            mask[4] = off_is4 | (off_is0 & width_64);
            mask[5] = off_is5 | (off_is4 & (width_16 | width_32)) | (off_is0 & width_64);
            mask[6] = off_is6 | (off_is4 & width_32) | (off_is0 & width_64);

            mask[7] = off_is7 | (off_is6 & width_16) | (off_is4 & width_32) | (off_is0 & width_64);

            generate_byte_mask_with_len = mask;

        end
    endfunction : generate_byte_mask_with_len

    function automatic [7:0] generate_st_byte_mask;
        input logic [PADDR_WIDTH-1:0] paddr_i;
        input logic [LDU_OP_WIDTH-1:0] opcode_i;

        logic [2:0] offset;
        logic [7:0] mask;

        logic width_8, width_16, width_32, width_64;
        logic off_is0, off_is1, off_is2, off_is3, off_is4, off_is5, off_is6, off_is7;
        begin
            offset = paddr_i[2:0];

            off_is0 = offset == 3'd0;
            off_is1 = offset == 3'd1;
            off_is2 = offset == 3'd2;
            off_is3 = offset == 3'd3;
            off_is4 = offset == 3'd4;
            off_is5 = offset == 3'd5;
            off_is6 = offset == 3'd6;
            off_is7 = offset == 3'd7;

            width_8 = opcode_i == STU_SB;
            width_16 = opcode_i == STU_SH;
            width_32 = opcode_i == STU_SW;
            width_64 = opcode_i == STU_SD;

            mask[0] = off_is0;
            mask[1] = off_is1 | (off_is0 & ~width_8);
            mask[2] = off_is2 | (off_is0 & (width_32 | width_64));
            mask[3] = off_is3 | (off_is2 & width_16) | off_is0 & (width_32 | width_64);
            mask[4] = off_is4 | (off_is0 & width_64);
            mask[5] = off_is5 | (off_is4 & (width_16 | width_32)) | (off_is0 & width_64);
            mask[6] = off_is6 | (off_is4 & width_32) | (off_is0 & width_64);

            mask[7] = off_is7 | (off_is6 & width_16) | (off_is4 & width_32) | (off_is0 & width_64);

            generate_st_byte_mask = mask;
        end
    endfunction : generate_st_byte_mask


    function automatic [7:0] generate_ld_byte_mask;
        input logic [PADDR_WIDTH-1:0] paddr_i;
        input logic [STU_OP_WIDTH-1:0] opcode_i;

        logic [2:0] offset;
        logic [7:0] mask;

        logic width_8, width_16, width_32, width_64;
        logic off_is0, off_is1, off_is2, off_is3, off_is4, off_is5, off_is6, off_is7;
        begin
            offset = paddr_i[2:0];

            off_is0 = offset == 3'd0;
            off_is1 = offset == 3'd1;
            off_is2 = offset == 3'd2;
            off_is3 = offset == 3'd3;
            off_is4 = offset == 3'd4;
            off_is5 = offset == 3'd5;
            off_is6 = offset == 3'd6;
            off_is7 = offset == 3'd7;

            width_8 = opcode_i == LDU_LB | opcode_i == LDU_LBU;
            width_16 = opcode_i == LDU_LH | opcode_i == LDU_LHU;
            width_32 = opcode_i == LDU_LW | opcode_i == LDU_LWU;
            width_64 = opcode_i == LDU_LD;

            mask[0] = off_is0;
            mask[1] = off_is1 | (off_is0 & ~width_8);
            mask[2] = off_is2 | (off_is0 & (width_32 | width_64));
            mask[3] = off_is3 | (off_is2 & width_16) | off_is0 & (width_32 | width_64);
            mask[4] = off_is4 | (off_is0 & width_64);
            mask[5] = off_is5 | (off_is4 & (width_16 | width_32)) | (off_is0 & width_64);
            mask[6] = off_is6 | (off_is4 & width_32) | (off_is0 & width_64);

            mask[7] = off_is7 | (off_is6 & width_16) | (off_is4 & width_32) | (off_is0 & width_64);

            generate_ld_byte_mask = mask;
        end
    endfunction : generate_ld_byte_mask

    function automatic [STQ_TAG_WIDTH-1:0] get_next_stq_tag;
        input logic [STQ_TAG_WIDTH-1:0] stq_tag_i;
        begin
            if (stq_tag_i == STQ_ENTRY_COUNT - 1) begin
                get_next_stq_tag = {STQ_TAG_WIDTH{1'b0}};
            end else begin
                get_next_stq_tag = stq_tag_i + 1'b1;
            end
        end
    endfunction : get_next_stq_tag



endpackage : rvh_lsu_pkg
