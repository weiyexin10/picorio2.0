/* 
 **Load Pipeline**
 * ***M0*** : 
   * Calculate Virtual Address
 * ***M1*** :
   * Select instruction between LRQ and AGU. LRQ > AGU
   * Condition(Issue from LRQ) :
        * Condition(IO load) :
            * **IO buffer** : Enqueue IO operation
        * Condition(common load) : 
            * **D-Cache**  : Look up tag RAM and data RAM
   * Condition(Issue from AGU) :
        * Check misalign exception from AGU
        * **DTLB**         : Translate virtual address to physical address
        * **D-Cache**      : Look up tag RAM and data RAM
 * ***M2*** :
    * Exception : Check exception from M1 or DTLB
    * Condition(DTLB miss) :
        * Send commit signal(reactivate) to AGU issue queue
    * Condition(DTLB hit)  : 
        * Send commit signal(deallocate) to AGU issue queue
        * Send translated result and PPN to D-Cache 
        * **RAR Queue**    : Check RAR ordering failure 
        * **Store Queue**  : Check pending store
        * **PMP**          : Check access permission
        * **PMA**          : Check memory attribute
 * ***M3*** :
    * Check exception from M2 or PMP or PMA or RAR Queue
    * Write back result to reorder buffer
    * Condition(pending stores exist / IO region / RAR speculation but RAR queue full / RAW speculation but RAW queue full)
        * Kill D-Cache stage
        * Send instruction to LRQ and mark replay 
    * Condition(others)
        * Allocate RAR queue if RAR speculation
        * Allocate RAW queue if RAW speculation
        * Deallocate LRQ entry 
        
 **Store Pipeline**
 * ***M0*** : 
   * Sync Issue Request
 * ***M1*** :
    * Check misalign exception from AGU
    * **DTLB**         : Translate virtual address to physical address
 * ***M2*** :
    * Exception : Check exception from M1 or DTLB
    * Condition(DTLB miss) :
        * Send commit signal(reactivate) to AGU issue queue
    * Condition(DTLB hit)  : 
        * Send commit signal(deallocate) to AGU issue queue
        * **RAW Queue**    : Check raw ordering failure 
        * **PMP**          : Check access permission
        * **PMA**          : Check memory attribute
 * ***M3*** :
    * Check exception from M2 or PMP or PMA or RAW Queue
    * Condition(No exception)
        * **STQ** : Write back to store queue
 * ***M4*** :
    * Condition(IO store / AMO) :
        * **D-Cacher** : Send request to D$
    * Condition(Common store) :
        * **D-Cache** : Send request to Store buffer
*/
module rvh_ls_pipe
    import rvh_pkg::*;
    import rvh_lsu_pkg::*;
    import rvh_rcu_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
(

    // AGU IQ -> LS Pipe : Issue
    input  logic                        agu_iq_ls_pipe_issue_vld_i,
    input  logic                        agu_iq_ls_pipe_issue_excp_vld_i,
    input  logic [   ROB_TAG_WIDTH-1:0] agu_iq_ls_pipe_issue_rob_tag_i,
    input  logic [   LSQ_TAG_WIDTH-1:0] agu_iq_ls_pipe_issue_lsq_tag_i,
    input  logic [AGU_IQ_TAG_WIDHT-1:0] agu_iq_ls_pipe_issue_entry_tag_i,
    input  logic [  LSU_TYPE_WIDTH-1:0] agu_iq_ls_pipe_issue_fu_type_i,
    input  logic [    LSU_OP_WIDTH-1:0] agu_iq_ls_pipe_issue_opcode_i,
    input  logic [     VADDR_WIDTH-1:0] agu_iq_ls_pipe_issue_vaddr_i,
    input  logic [  PREG_TAG_WIDTH-1:0] agu_iq_ls_pipe_issue_prd_i,
    output logic                        agu_iq_ls_pipe_issue_rdy_o,
    // LRQ -> LS Pipe : Issue
    input  logic                        lrq_ls_pipe_issue_vld_i,
    input  logic                        lrq_ls_pipe_issue_io_i,
    input  logic [   ROB_TAG_WIDTH-1:0] lrq_ls_pipe_issue_rob_tag_i,
    input  logic [   LDQ_TAG_WIDTH-1:0] lrq_ls_pipe_issue_lrq_tag_i,
    input  logic [    LDU_OP_WIDTH-1:0] lrq_ls_pipe_issue_opcode_i,
    input  logic [  PREG_TAG_WIDTH-1:0] lrq_ls_pipe_issue_prd_i,
    input  logic [     PADDR_WIDTH-1:0] lrq_ls_pipe_issue_paddr_i,
    output logic                        lrq_ls_pipe_issue_rdy_o,
    // STQ -> LS Pipe : Issue
    input  logic                        stq_ls_pipe_issue_vld_i,
    input  logic                        stq_ls_pipe_issue_io_i,
    input  logic                        stq_ls_pipe_issue_is_fence_i,
    input  logic [   ROB_TAG_WIDTH-1:0] stq_ls_pipe_issue_rob_tag_i,
    input  logic [    STU_OP_WIDTH-1:0] stq_ls_pipe_issue_opcode_i,
    input  logic [  PREG_TAG_WIDTH-1:0] stq_ls_pipe_issue_prd_i,
    input  logic [     PADDR_WIDTH-1:0] stq_ls_pipe_issue_paddr_i,
    input  logic [            XLEN-1:0] stq_ls_pipe_issue_data_i,
    output logic                        stq_ls_pipe_issue_rdy_o,
    output logic                        stq_ls_pipe_issue_io_rdy_o,
    // LS Pipe -> DTLB : Translation request
    output logic                        ls_pipe_dtlb_req_vld_o,
    output logic [PMA_ACCESS_WIDTH-1:0] ls_pipe_dtlb_req_access_type_o,
    output logic [       VPN_WIDTH-1:0] ls_pipe_dtlb_req_vpn_o,
    input  logic                        ls_pipe_dtlb_req_rdy_i,
    // LS Pipe -> DTLB : Translation response
    input  logic                        ls_pipe_dtlb_resp_vld_i,
    input  logic [       PPN_WIDTH-1:0] ls_pipe_dtlb_resp_ppn_i,
    input  logic                        ls_pipe_dtlb_resp_excp_vld_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0] ls_pipe_dtlb_resp_excp_cause_i,
    input  logic                        ls_pipe_dtlb_resp_hit_i,
    input  logic                        ls_pipe_dtlb_resp_miss_i,
    output logic                        ls_pipe_dtlb_resp_rdy_o,
    // LS Pipe -> AGU IQ : Commit IQ entry
    output logic                        ls_pipe_agu_iq_cmt_vld_o,
    output logic [AGU_IQ_TAG_WIDHT-1:0] ls_pipe_agu_iq_cmt_tag_o,
    output logic                        ls_pipe_agu_iq_cmt_dealloc_o,
    output logic                        ls_pipe_agu_iq_cmt_reactivate_o,
    // LS Pipe -> D$ : Load request
    output logic                        ls_pipe_l1d_ld_req_vld_o,
    output logic                        ls_pipe_l1d_ld_req_io_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_l1d_ld_req_rob_tag_o,
    output logic [  PREG_TAG_WIDTH-1:0] ls_pipe_l1d_ld_req_prd_o,
    output logic [    LDU_OP_WIDTH-1:0] ls_pipe_l1d_ld_req_opcode_o,
    output logic [ L1D_INDEX_WIDTH_CORE-1:0] ls_pipe_l1d_ld_req_index_o,
    output logic [   L1D_TAG_WIDTH_CORE-1:0] ls_pipe_l1d_ld_req_tag_o,
    input  logic                        ls_pipe_l1d_ld_req_rdy_i,
    input  logic                        ls_pipe_l1d_ld_io_req_rdy_i,
    // LS Pipe -> D$ : DTLB response
    output logic                        ls_pipe_l1d_dtlb_resp_vld_o,
    output logic [       PPN_WIDTH-1:0] ls_pipe_l1d_dtlb_resp_ppn_o,
    output logic                        ls_pipe_l1d_dtlb_resp_excp_vld_o,
    output logic                        ls_pipe_l1d_dtlb_resp_hit_o,
    output logic                        ls_pipe_l1d_dtlb_resp_miss_o,
    // LS Pipe -> D$ : Store request
    output logic                        ls_pipe_l1d_st_req_vld_o,
    output logic                        ls_pipe_l1d_st_req_io_o,
    output logic                        ls_pipe_l1d_st_req_is_fence_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_l1d_st_req_rob_tag_o,
    output logic [  PREG_TAG_WIDTH-1:0] ls_pipe_l1d_st_req_prd_o,
    output logic [    STU_OP_WIDTH-1:0] ls_pipe_l1d_st_req_opcode_o,
    output logic [ L1D_INDEX_WIDTH_CORE-1:0] ls_pipe_l1d_st_req_index_o,
    output logic [   L1D_TAG_WIDTH_CORE-1:0] ls_pipe_l1d_st_req_tag_o,
    output logic [            XLEN-1:0] ls_pipe_l1d_st_req_data_o,
    input  logic                        ls_pipe_l1d_st_req_rdy_i,
    input  logic                        ls_pipe_l1d_st_io_req_rdy_i,
    // LS Pipe -> STQ : Check pending store
    output logic                        ls_pipe_stq_check_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_stq_check_rob_tag_o,
    output logic [MEM_OP_LEN_WIDTH-1:0] ls_pipe_stq_check_len_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_stq_check_paddr_o,
    input  logic                        ls_pipe_stq_check_raw_hazard_i,
    // LS Pipe -> RAR : Check RAR Ordering Failure
    output logic                        ls_pipe_rar_check_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_rar_check_rob_tag_o,
    output logic [MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_check_len_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_rar_check_paddr_o,
    input  logic                        ls_pipe_rar_check_fail_i,
    // LS Pipe -> RAW : Check RAW Ordering Failure
    output logic                        ls_pipe_raw_check_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_raw_check_rob_tag_o,
    output logic [MEM_OP_LEN_WIDTH-1:0] ls_pipe_raw_check_len_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_raw_check_paddr_o,
    input  logic                        ls_pipe_raw_check_fail_i,
    // LS Pipe -> LS Pipe : Inter Pipeline Check Master
    output logic                           ls_pipe_inter_check_master_vld_o,
    output logic [      ROB_TAG_WIDTH-1:0] ls_pipe_inter_check_master_rob_tag_o,
    output logic [   MEM_OP_LEN_WIDTH-1:0] ls_pipe_inter_check_master_len_o,
    output logic [        PADDR_WIDTH-1:0] ls_pipe_inter_check_master_paddr_o,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0] ls_pipe_inter_check_master_raw_hazard_i,

    // LS Pipe -> LS Pipe : Inter Pipeline Check Slave
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                          ls_pipe_inter_check_slave_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0]       ls_pipe_inter_check_slave_rob_tag_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0]    ls_pipe_inter_check_slave_len_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0]         ls_pipe_inter_check_slave_paddr_i,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                          ls_pipe_inter_check_slave_raw_hazard_o,

    // L1D -> LS Pipe : D-Cache MSHR Full, Replay load
    input  logic                        l1d_ls_pipe_replay_mshr_full_i,
    // LS Pipe -> L1D : Kill D-Cache Response
    output logic                        ls_pipe_l1d_kill_resp_o,
    // LS Pipe -> RAR : Allocate
    output logic                        ls_pipe_rar_alloc_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_rar_alloc_rob_tag_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_rar_alloc_paddr_o,
    output logic [MEM_OP_LEN_WIDTH-1:0] ls_pipe_rar_alloc_len_o,
    input  logic                        ls_pipe_rar_alloc_rdy_i,
    // LS Pipe -> RAW : Allocate
    output logic                        ls_pipe_raw_alloc_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_raw_alloc_rob_tag_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_raw_alloc_paddr_o,
    output logic [MEM_OP_LEN_WIDTH-1:0] ls_pipe_raw_alloc_len_o,
    input  logic                        ls_pipe_raw_alloc_rdy_i,
    // LS Pipe -> STQ : Write Back
    output logic                        ls_pipe_stq_wb_vld_o,
    output logic [   STQ_TAG_WIDTH-1:0] ls_pipe_stq_wb_stq_tag_o,
    output logic [  PREG_TAG_WIDTH-1:0] ls_pipe_stq_wb_prd_o,
    output logic [    STU_OP_WIDTH-1:0] ls_pipe_stq_wb_opcode_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_stq_wb_paddr_o,
    output logic                        ls_pipe_stq_wb_io_o,
    // LS Pipe -> LRQ : Sleep or Deallocate
    output logic                        ls_pipe_lrq_wb_vld_o,
    output logic                        ls_pipe_lrq_wb_sleep_o,
    output logic                        ls_pipe_lrq_wb_dealloc_o,
    output logic [   LDQ_TAG_WIDTH-1:0] ls_pipe_lrq_wb_lrq_tag_o,
    output logic [  PREG_TAG_WIDTH-1:0] ls_pipe_lrq_wb_prd_o,
    output logic [    LDU_OP_WIDTH-1:0] ls_pipe_lrq_wb_opcode_o,
    output logic [     PADDR_WIDTH-1:0] ls_pipe_lrq_wb_paddr_o,
    output logic                        ls_pipe_lrq_wb_io_o,
    output logic                        ls_pipe_lrq_wb_pending_st_o,
    // LS Pipe -> ROB : Report exception
    output logic                        ls_pipe_rob_report_excp_vld_o,
    output logic [EXCP_CAUSE_WIDTH-1:0] ls_pipe_rob_report_excp_cause_o,
    output logic [ EXCP_TVAL_WIDTH-1:0] ls_pipe_rob_report_excp_tval_o,
    output logic [   ROB_TAG_WIDTH-1:0] ls_pipe_rob_report_excp_rob_tag_o,
    // LDQ Status
    input  logic                        lrq_empty_i,
    input  logic [   ROB_TAG_WIDTH-1:0] oldest_ld_rob_tag_i,
    // STQ Status
    input  logic                        stq_empty_i,
    input  logic [   ROB_TAG_WIDTH-1:0] oldest_st_rob_tag_i,
    // BRQ Status
    input  logic                        brq_empty_i,
    input  logic [   ROB_TAG_WIDTH-1:0] oldest_br_rob_tag_i,

    input logic flush_i,

    input clk,
    input rst
);


    // M0 Stage
    logic m0_agu_clk_en, m0_agu_vld, m0_agu_rdy, m0_agu_fire;

    logic m0_agu_vld_d, m0_agu_vld_q;
    logic m0_agu_excp_vld_d, m0_agu_excp_vld_q;
    logic [ROB_TAG_WIDTH-1:0] m0_agu_rob_tag_d, m0_agu_rob_tag_q;
    logic [LSQ_TAG_WIDTH-1:0] m0_agu_lsq_tag_d, m0_agu_lsq_tag_q;
    logic [AGU_IQ_TAG_WIDHT-1:0] m0_agu_entry_tag_d, m0_agu_entry_tag_q;
    logic [LSU_TYPE_WIDTH-1:0] m0_agu_fu_type_d, m0_agu_fu_type_q;
    logic [LSU_OP_WIDTH-1:0] m0_agu_opcode_d, m0_agu_opcode_q;
    logic [VADDR_WIDTH-1:0] m0_agu_vaddr_d, m0_agu_vaddr_q;
    logic [PREG_TAG_WIDTH-1:0] m0_agu_prd_d, m0_agu_prd_q;

    logic m0_lrq_clk_en, m0_lrq_vld, m0_lrq_rdy, m0_lrq_fire;

    logic m0_lrq_vld_d, m0_lrq_vld_q;
    logic m0_lrq_io_d, m0_lrq_io_q;
    logic [ROB_TAG_WIDTH-1:0] m0_lrq_rob_tag_d, m0_lrq_rob_tag_q;
    logic [LDQ_TAG_WIDTH-1:0] m0_lrq_tag_d, m0_lrq_tag_q;
    logic [LDU_OP_WIDTH-1:0] m0_lrq_opcode_d, m0_lrq_opcode_q;
    logic [PREG_TAG_WIDTH-1:0] m0_lrq_prd_d, m0_lrq_prd_q;
    logic [PADDR_WIDTH-1:0] m0_lrq_paddr_d, m0_lrq_paddr_q;

    // M1 Stage
    logic m1_clk_en, m1_fire;
    logic m1_agu_fire, m1_lrq_fire;
    logic m1_stall_m0_agu, m1_stall_m0_lrq;
    logic m1_sel_lrq;
    logic m1_sel_agu;

    logic m1_vld_d, m1_vld_q;
    logic m1_is_lrq_d, m1_is_lrq_q;
    logic m1_lrq_io_d, m1_lrq_io_q;
    logic m1_excp_vld_d, m1_excp_vld_q;
    logic [ROB_TAG_WIDTH-1:0] m1_rob_tag_d, m1_rob_tag_q;
    logic [LSQ_TAG_WIDTH-1:0] m1_lsq_tag_d, m1_lsq_tag_q;
    logic [AGU_IQ_TAG_WIDHT-1:0] m1_iq_tag_d, m1_iq_tag_q;
    logic [LSU_TYPE_WIDTH-1:0] m1_fu_type_d, m1_fu_type_q;
    logic [LSU_OP_WIDTH-1:0] m1_opcode_d, m1_opcode_q;
    logic [PADDR_WIDTH-1:0] m1_addr_d, m1_addr_q;
    logic [PREG_TAG_WIDTH-1:0] m1_prd_d, m1_prd_q;

    // M2 Stage
    logic m2_clk_en, m2_fire;

    logic m2_is_ld, m2_is_st;
    logic [   ROB_TAG_WIDTH-1:0 ] m2_check_rob_tag;
    logic [MEM_OP_LEN_WIDTH-1:0 ] m2_check_len;
    logic [    PADDR_WIDTH-1:0]  m2_check_paddr;

    logic m2_vld_d, m2_vld_q;
    logic m2_is_lrq_d, m2_is_lrq_q;
    logic m2_excp_vld_d, m2_excp_vld_q;
    logic [EXCP_CAUSE_WIDTH-1:0] m2_excp_cause_d, m2_excp_cause_q;
    logic [ROB_TAG_WIDTH-1:0] m2_rob_tag_d, m2_rob_tag_q;
    logic [LSQ_TAG_WIDTH-1:0] m2_lsq_tag_d, m2_lsq_tag_q;
    logic [LSU_TYPE_WIDTH-1:0] m2_fu_type_d, m2_fu_type_q;
    logic [LSU_OP_WIDTH-1:0] m2_opcode_d, m2_opcode_q;
    logic [VADDR_WIDTH-1:0] m2_vaddr_d, m2_vaddr_q;
    logic [PADDR_WIDTH-1:0] m2_paddr_d, m2_paddr_q;
    logic [PREG_TAG_WIDTH-1:0] m2_prd_d, m2_prd_q;
    logic m2_pma_check_io_d, m2_pma_check_io_q;
    logic m2_rar_check_fail_d, m2_rar_check_fail_q;
    logic m2_raw_check_fail_d, m2_raw_check_fail_q;
    logic m2_pending_st_exist_d, m2_pending_st_exist_q;


    // M3 Stage

    logic m3_is_ld, m3_is_st;
    logic [   ROB_TAG_WIDTH-1:0 ] m3_check_rob_tag;
    logic [MEM_OP_LEN_WIDTH-1:0 ] m3_check_len;
    logic [    PADDR_WIDTH-1:0]  m3_check_paddr;

    logic m3_rar_spec, m3_raw_spec;
    logic m3_rar_resource_hazard, m3_raw_resource_hazard;
    logic m3_replay_ld;
    logic m3_excp_vld;
    logic [EXCP_CAUSE_WIDTH-1:0] m3_excp_cause;
    logic [EXCP_TVAL_WIDTH-1:0] m3_excp_tval;

    // M0 Stage 
    assign agu_iq_ls_pipe_issue_rdy_o = m0_agu_rdy;
    assign lrq_ls_pipe_issue_rdy_o    = m0_lrq_rdy;
    
    assign m0_agu_clk_en = m0_agu_fire;
    assign m0_agu_fire = m0_agu_vld & m0_agu_rdy;
    assign m0_agu_vld = agu_iq_ls_pipe_issue_vld_i;
    assign m0_agu_rdy = ~m1_stall_m0_agu;
    assign m0_agu_vld_d = ~flush_i & (m1_stall_m0_agu ? m0_agu_vld_q : m0_agu_fire);
    assign m0_agu_excp_vld_d = agu_iq_ls_pipe_issue_excp_vld_i;
    assign m0_agu_rob_tag_d = agu_iq_ls_pipe_issue_rob_tag_i;
    assign m0_agu_lsq_tag_d = agu_iq_ls_pipe_issue_lsq_tag_i;
    assign m0_agu_entry_tag_d = agu_iq_ls_pipe_issue_entry_tag_i;
    assign m0_agu_fu_type_d = agu_iq_ls_pipe_issue_fu_type_i;
    assign m0_agu_opcode_d = agu_iq_ls_pipe_issue_opcode_i;
    assign m0_agu_vaddr_d = agu_iq_ls_pipe_issue_vaddr_i;
    assign m0_agu_prd_d = agu_iq_ls_pipe_issue_prd_i;

    assign m0_lrq_clk_en = m0_lrq_fire;
    assign m0_lrq_fire = m0_lrq_vld & m0_lrq_rdy;
    assign m0_lrq_vld = lrq_ls_pipe_issue_vld_i;
    assign m0_lrq_rdy = ~m1_stall_m0_lrq;
    assign m0_lrq_vld_d = ~flush_i & (m1_stall_m0_lrq ? m0_lrq_vld_q : m0_lrq_fire);
    assign m0_lrq_io_d = lrq_ls_pipe_issue_io_i;
    assign m0_lrq_rob_tag_d = lrq_ls_pipe_issue_rob_tag_i;
    assign m0_lrq_tag_d = lrq_ls_pipe_issue_lrq_tag_i;
    assign m0_lrq_opcode_d = lrq_ls_pipe_issue_opcode_i;
    assign m0_lrq_prd_d = lrq_ls_pipe_issue_prd_i;
    assign m0_lrq_paddr_d = lrq_ls_pipe_issue_paddr_i;

    // M1 Stage
    assign m1_stall_m0_agu = m0_agu_vld_q & (m1_sel_lrq | (~m0_agu_excp_vld_q & (~ls_pipe_l1d_ld_req_rdy_i & (m0_agu_fu_type_q == LSU_LD_TYPE) | (~ls_pipe_dtlb_req_rdy_i))));
    assign m1_stall_m0_lrq = m0_lrq_vld_q & (m1_sel_agu | (m0_lrq_io_q ? ~ls_pipe_l1d_ld_io_req_rdy_i : ~ls_pipe_l1d_ld_req_rdy_i));
    assign m1_sel_lrq = m0_lrq_vld_q & (~m0_agu_vld_q | rob_tag_is_older(m0_lrq_rob_tag_q,m0_agu_rob_tag_q));
    assign m1_sel_agu = ~m1_sel_lrq;

    // Access DTLB
    assign ls_pipe_dtlb_req_vld_o = m1_sel_agu & m1_agu_fire & ~m0_agu_excp_vld_q;
    assign ls_pipe_dtlb_req_access_type_o = get_access_type(m0_agu_fu_type_q, m0_agu_opcode_q);
    assign ls_pipe_dtlb_req_vpn_o = m0_agu_vaddr_q[VADDR_WIDTH-1-:VPN_WIDTH];

    // Access D-Cache
    assign ls_pipe_l1d_ld_req_vld_o = m1_sel_lrq ? m0_lrq_vld_q : (m0_agu_vld_q & ~m0_agu_excp_vld_q & (m0_agu_fu_type_q == LSU_LD_TYPE));
    assign ls_pipe_l1d_ld_req_io_o = m1_sel_lrq ? m0_lrq_io_q : 1'b0;
    assign ls_pipe_l1d_ld_req_rob_tag_o = m1_sel_lrq ? m0_lrq_rob_tag_q : m0_agu_rob_tag_q;
    assign ls_pipe_l1d_ld_req_prd_o = m1_sel_lrq ? m0_lrq_prd_q : m0_agu_prd_q;
    assign ls_pipe_l1d_ld_req_opcode_o = m1_sel_lrq ? m0_lrq_opcode_q : m0_agu_opcode_q;
    assign ls_pipe_l1d_ld_req_index_o = m1_sel_lrq ? m0_lrq_paddr_q[L1D_INDEX_WIDTH_CORE-1:0] : m0_agu_vaddr_q[L1D_INDEX_WIDTH_CORE-1:0];
    assign ls_pipe_l1d_ld_req_tag_o = m1_sel_lrq ? m0_lrq_paddr_q[PADDR_WIDTH-1 -: L1D_TAG_WIDTH_CORE] : {L1D_TAG_WIDTH_CORE{1'b0}};

    assign m1_clk_en = m1_fire;
    assign m1_agu_fire = (m0_agu_vld_q & ~m1_stall_m0_agu);
    assign m1_lrq_fire = (m0_lrq_vld_q & ~m1_stall_m0_lrq);
    assign m1_fire = m1_agu_fire | m1_lrq_fire;
    assign m1_vld_d = ~flush_i & m1_fire;
    assign m1_is_lrq_d = m1_sel_lrq;
    assign m1_lrq_io_d = m1_sel_lrq ? m0_lrq_io_q : 1'b0;
    assign m1_excp_vld_d = m1_sel_lrq ? 1'b0 : m0_agu_excp_vld_q;
    assign m1_rob_tag_d = m1_sel_lrq ? m0_lrq_rob_tag_q : m0_agu_rob_tag_q;
    assign m1_lsq_tag_d = m1_sel_lrq ? m0_lrq_tag_q : m0_agu_lsq_tag_q;
    assign m1_iq_tag_d = m1_sel_lrq ? {AGU_IQ_TAG_WIDHT{1'b0}} : m0_agu_entry_tag_q;
    assign m1_fu_type_d = m1_sel_lrq ? LSU_LD_TYPE : m0_agu_fu_type_q;
    assign m1_opcode_d = m1_sel_lrq ? m0_lrq_opcode_q : m0_agu_opcode_q;
    assign m1_addr_d    = m1_sel_lrq ? m0_lrq_paddr_q : {{PADDR_WIDTH-VADDR_WIDTH{1'b0}},m0_agu_vaddr_q};
    assign m1_prd_d = m1_sel_lrq ? m0_lrq_prd_q : m0_agu_prd_q;

    // M2 Stage

    assign m2_is_ld = (m1_fu_type_q == LSU_LD_TYPE);
    assign m2_is_st = ~m2_is_ld;
    assign m2_check_rob_tag = m1_rob_tag_q;
    assign m2_check_len = get_mem_len(m1_fu_type_q, m1_opcode_q);
    assign m2_check_paddr = {ls_pipe_l1d_dtlb_resp_ppn_o, m1_addr_q[PAGE_OFFSET_WIDTH-1:0]};

    assign ls_pipe_l1d_dtlb_resp_vld_o = m1_vld_q & m2_is_ld;
    assign ls_pipe_l1d_dtlb_resp_ppn_o          = m1_is_lrq_q ? m1_addr_q[PADDR_WIDTH-1 -: PPN_WIDTH] : ls_pipe_dtlb_resp_ppn_i;
    assign ls_pipe_l1d_dtlb_resp_excp_vld_o = m1_is_lrq_q ? 1'b0 : ls_pipe_dtlb_resp_excp_vld_i;
    assign ls_pipe_l1d_dtlb_resp_hit_o = m1_is_lrq_q ? 1'b1 : ls_pipe_dtlb_resp_hit_i;
    assign ls_pipe_l1d_dtlb_resp_miss_o = m1_is_lrq_q ? 1'b0 : ls_pipe_dtlb_resp_miss_i;
    // LS Pipe -> STQ : Check pending store
    assign ls_pipe_stq_check_vld_o  = m1_vld_q & ((~m1_excp_vld_q & ~ls_pipe_dtlb_resp_excp_vld_i & ls_pipe_dtlb_resp_hit_i ) | m1_is_lrq_q) & m2_is_ld;
    assign ls_pipe_stq_check_rob_tag_o = m1_rob_tag_q;
    assign ls_pipe_stq_check_len_o = m2_check_len;
    assign ls_pipe_stq_check_paddr_o = m2_check_paddr;
    // LS Pipe -> RAR : Check RAR Ordering Failure
    assign ls_pipe_rar_check_vld_o = ls_pipe_stq_check_vld_o;
    assign ls_pipe_rar_check_rob_tag_o = m1_rob_tag_q;
    assign ls_pipe_rar_check_len_o = m2_check_len;
    assign ls_pipe_rar_check_paddr_o = m2_check_paddr;
    // LS Pipe -> RAW : Check RAW Ordering Failure
    assign ls_pipe_raw_check_vld_o     = m1_vld_q &(~m1_excp_vld_q & ~ls_pipe_dtlb_resp_excp_vld_i & ls_pipe_dtlb_resp_hit_i ) & m2_is_st & (m2_opcode_d != STU_FENCE);
    assign ls_pipe_raw_check_rob_tag_o = m1_rob_tag_q;
    assign ls_pipe_raw_check_len_o     = m2_check_len;
    assign ls_pipe_raw_check_paddr_o   = m2_check_paddr;
    // LS Pipe -> AGU IQ : Commit IQ entry
    assign ls_pipe_agu_iq_cmt_vld_o = m1_vld_q & ~m1_is_lrq_q;
    assign ls_pipe_agu_iq_cmt_tag_o = m1_iq_tag_q;
    assign ls_pipe_agu_iq_cmt_dealloc_o = ls_pipe_dtlb_resp_hit_i;
    assign ls_pipe_agu_iq_cmt_reactivate_o = ls_pipe_dtlb_resp_miss_i;


    assign m2_clk_en = m2_fire;
    assign m2_fire = m1_vld_q & (m1_is_lrq_q ? 1'b1 : (ls_pipe_dtlb_resp_hit_i | m2_excp_vld_d));
    assign m2_vld_d = ~flush_i & m2_fire;
    assign m2_excp_vld_d = m1_excp_vld_q | (ls_pipe_dtlb_resp_vld_i & ls_pipe_dtlb_resp_excp_vld_i);
    assign m2_excp_cause_d                      = m1_excp_vld_q ? (m2_is_ld ? LD_ADDR_MISALIGNED : ST_ADDR_MISALIGNED) :
                                                          (ls_pipe_dtlb_resp_excp_vld_i ? ls_pipe_dtlb_resp_excp_cause_i : {EXCP_CAUSE_WIDTH{1'b0}});
    assign m2_rob_tag_d = m1_rob_tag_q;
    assign m2_lsq_tag_d = m1_lsq_tag_q;
    assign m2_fu_type_d = m1_fu_type_q;
    assign m2_opcode_d = m1_opcode_q;
    assign m2_vaddr_d = m1_addr_q[VADDR_WIDTH-1:0];
    assign m2_paddr_d = ls_pipe_rar_check_paddr_o;
    assign m2_prd_d = m1_prd_q;
    assign m2_is_lrq_d = m1_is_lrq_q;

    assign m2_pma_check_io_d = m1_vld_q & ls_pipe_l1d_dtlb_resp_hit_o & ~ls_pipe_l1d_dtlb_resp_excp_vld_o &
                                ~((ls_pipe_l1d_dtlb_resp_ppn_o >= 'h80000) & (ls_pipe_l1d_dtlb_resp_ppn_o < 'hFF000));
    
    assign m2_rar_check_fail_d = ls_pipe_rar_check_vld_o & ls_pipe_rar_check_fail_i;
    assign m2_raw_check_fail_d = ls_pipe_raw_check_vld_o & ls_pipe_raw_check_fail_i;
    assign m2_pending_st_exist_d = ls_pipe_stq_check_vld_o & ls_pipe_stq_check_raw_hazard_i;

    // M3 Stage

    assign m3_is_ld = (m2_fu_type_q == LSU_LD_TYPE);
    assign m3_is_st = ~m3_is_ld;
    assign m3_check_rob_tag = m2_rob_tag_q;
    assign m3_check_len = get_mem_len(m2_fu_type_q, m2_opcode_q);
    assign m3_check_paddr = m2_paddr_q;

    // Perform inter-Pipeline Check
    // LS Pipe -> LS Pipe : Inter Pipeline Check Master

    assign ls_pipe_inter_check_master_vld_o = m2_vld_q & m3_is_ld &  ~m3_excp_vld & ~m2_excp_vld_q;
    assign ls_pipe_inter_check_master_rob_tag_o = m3_check_rob_tag;
    assign ls_pipe_inter_check_master_len_o = m3_check_len;
    assign ls_pipe_inter_check_master_paddr_o = m3_check_paddr;


    assign m3_rar_spec = ~lrq_empty_i & rob_tag_is_older(oldest_ld_rob_tag_i, m2_rob_tag_q);
    assign m3_raw_spec = ~stq_empty_i & rob_tag_is_older(oldest_st_rob_tag_i, m2_rob_tag_q);
    assign m3_rar_resource_hazard = m3_rar_spec & ~ls_pipe_rar_alloc_rdy_i;
    assign m3_raw_resource_hazard = m3_raw_spec & ~ls_pipe_raw_alloc_rdy_i;
    assign m3_replay_ld                 = m3_rar_resource_hazard | m3_raw_resource_hazard | (m2_pma_check_io_q & ~m2_is_lrq_q) | m2_pending_st_exist_q | (|ls_pipe_inter_check_master_raw_hazard_i) | l1d_ls_pipe_replay_mshr_full_i;
    assign m3_excp_vld                  = m2_rar_check_fail_q | m2_raw_check_fail_q;
    assign m3_excp_cause                = (((m3_is_ld & m2_rar_check_fail_q) | (m3_is_st & m2_raw_check_fail_q))  ? ORDER_FAILURE : {EXCP_CAUSE_WIDTH{1'b0}});

    assign ls_pipe_l1d_kill_resp_o = m2_vld_q & m3_is_ld & (m3_replay_ld | m3_excp_vld);
    // LS Pipe -> RAR : Allocate
    assign ls_pipe_rar_alloc_vld_o      = m2_vld_q & m3_is_ld & m3_rar_spec & ~m3_replay_ld & ~m3_excp_vld & ~m2_excp_vld_q;
    assign ls_pipe_rar_alloc_rob_tag_o = m2_rob_tag_q;
    assign ls_pipe_rar_alloc_paddr_o = m2_paddr_q;
    assign ls_pipe_rar_alloc_len_o = get_mem_len(m2_fu_type_q, m2_opcode_q);
    // LS Pipe -> RAW : Allocate
    assign ls_pipe_raw_alloc_vld_o      = m2_vld_q & m3_is_ld & m3_raw_spec & ~m3_replay_ld & ~m3_excp_vld & ~m2_excp_vld_q;
    assign ls_pipe_raw_alloc_rob_tag_o = ls_pipe_rar_alloc_rob_tag_o;
    assign ls_pipe_raw_alloc_paddr_o = ls_pipe_rar_alloc_paddr_o;
    assign ls_pipe_raw_alloc_len_o = ls_pipe_rar_alloc_len_o;
    // LS Pipe -> STQ : Write Back
    assign ls_pipe_stq_wb_vld_o = m2_vld_q & m3_is_st & ~m3_excp_vld & ~m2_excp_vld_q;
    assign ls_pipe_stq_wb_stq_tag_o = m2_lsq_tag_q[STQ_TAG_WIDTH-1:0];
    assign ls_pipe_stq_wb_prd_o = m2_prd_q;
    assign ls_pipe_stq_wb_opcode_o = m2_opcode_q[STU_OP_WIDTH-1:0];
    assign ls_pipe_stq_wb_paddr_o = m2_paddr_q;
    assign ls_pipe_stq_wb_io_o = m2_pma_check_io_q;
    // LS Pipe -> LRQ : Sleep or Deallocate
    assign ls_pipe_lrq_wb_vld_o         = m2_vld_q & m3_is_ld  & ~m3_excp_vld & ~m2_excp_vld_q;
    assign ls_pipe_lrq_wb_sleep_o = m3_replay_ld;
    assign ls_pipe_lrq_wb_dealloc_o = ~m3_replay_ld;
    assign ls_pipe_lrq_wb_lrq_tag_o = m2_lsq_tag_q[LDQ_TAG_WIDTH-1:0];
    assign ls_pipe_lrq_wb_prd_o = m2_prd_q;
    assign ls_pipe_lrq_wb_opcode_o = m2_opcode_q;
    assign ls_pipe_lrq_wb_paddr_o = m2_paddr_q;
    assign ls_pipe_lrq_wb_io_o = m2_pma_check_io_q;
    assign ls_pipe_lrq_wb_pending_st_o = m2_pending_st_exist_q | (|ls_pipe_inter_check_master_raw_hazard_i);
    // LS Pipe -> ROB : Report exception
    assign ls_pipe_rob_report_excp_vld_o = m2_vld_q & (m2_excp_vld_q | m3_excp_vld);
    assign ls_pipe_rob_report_excp_cause_o      = m2_excp_vld_q ? m2_excp_cause_q : (m3_excp_vld ? m3_excp_cause : {EXCP_CAUSE_WIDTH{1'b0}});
    assign ls_pipe_rob_report_excp_tval_o = m2_vaddr_q;
    assign ls_pipe_rob_report_excp_rob_tag_o = m2_rob_tag_q;

    // M4 Stage
    
    assign ls_pipe_l1d_st_req_vld_o      = stq_ls_pipe_issue_vld_i;
    assign ls_pipe_l1d_st_req_io_o       = stq_ls_pipe_issue_io_i;
    assign ls_pipe_l1d_st_req_is_fence_o = stq_ls_pipe_issue_is_fence_i;
    assign ls_pipe_l1d_st_req_rob_tag_o  = stq_ls_pipe_issue_rob_tag_i;
    assign ls_pipe_l1d_st_req_prd_o      = stq_ls_pipe_issue_prd_i;
    assign ls_pipe_l1d_st_req_opcode_o   = stq_ls_pipe_issue_opcode_i;
    assign ls_pipe_l1d_st_req_index_o    = stq_ls_pipe_issue_paddr_i[L1D_INDEX_WIDTH_CORE-1:0];
    assign ls_pipe_l1d_st_req_tag_o      = stq_ls_pipe_issue_paddr_i[PADDR_WIDTH-1 -: L1D_TAG_WIDTH_CORE];
    assign ls_pipe_l1d_st_req_data_o     = stq_ls_pipe_issue_data_i;
    assign stq_ls_pipe_issue_rdy_o       = ls_pipe_l1d_st_req_rdy_i;
    assign stq_ls_pipe_issue_io_rdy_o    = ls_pipe_l1d_st_io_req_rdy_i;


    // Inter Pipeline Pending Store Check
    logic [LSU_ADDR_PIPE_COUNT-1:0] m2_pending_st_find;
    logic [LSU_ADDR_PIPE_COUNT-1:0] m3_pending_st_find;
    logic [LSU_ADDR_PIPE_COUNT-1:0] m2_addr_overlap;
    logic [LSU_ADDR_PIPE_COUNT-1:0] m3_addr_overlap;


    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            assign m2_pending_st_find[i] = m2_addr_overlap[i] & m2_fire & m2_is_st;
            assign m3_pending_st_find[i] = m3_addr_overlap[i] & ls_pipe_stq_wb_vld_o;
            assign ls_pipe_inter_check_slave_raw_hazard_o[i] = m2_pending_st_find[i] | m3_pending_st_find[i];

            rvh_raw_check u_inter_pipe_m2_rvh_raw_check (
                .src_rob_tag_i(ls_pipe_inter_check_slave_rob_tag_i[i]),
                .src_len_i(ls_pipe_inter_check_slave_len_i[i]),
                .src_paddr_i(ls_pipe_inter_check_slave_paddr_i[i]),
                .des_rob_tag_i(m2_check_rob_tag),
                .des_len_i(m2_check_len),
                .des_paddr_i(m2_check_paddr),
                .overlap_o(m2_addr_overlap[i])
            );
            rvh_raw_check u_inter_pipe_m3_rvh_raw_check (
                .src_rob_tag_i(ls_pipe_inter_check_slave_rob_tag_i[i]),
                .src_len_i(ls_pipe_inter_check_slave_len_i[i]),
                .src_paddr_i(ls_pipe_inter_check_slave_paddr_i[i]),
                .des_rob_tag_i(m3_check_rob_tag),
                .des_len_i(m3_check_len),
                .des_paddr_i(m3_check_paddr),
                .overlap_o(m3_addr_overlap[i])
            );
        end
    endgenerate


    always_ff @(posedge clk) begin : m0_agu_vld_dff
        if (rst) begin
            m0_agu_vld_q <= 1'b0;
        end else begin
            m0_agu_vld_q <= m0_agu_vld_d;
        end
    end

    always_ff @(posedge clk) begin : m0_agu_payload_dff
        if (m0_agu_clk_en) begin
            m0_agu_excp_vld_q  <= m0_agu_excp_vld_d;
            m0_agu_rob_tag_q   <= m0_agu_rob_tag_d;
            m0_agu_lsq_tag_q   <= m0_agu_lsq_tag_d;
            m0_agu_entry_tag_q <= m0_agu_entry_tag_d;
            m0_agu_fu_type_q   <= m0_agu_fu_type_d;
            m0_agu_opcode_q    <= m0_agu_opcode_d;
            m0_agu_vaddr_q     <= m0_agu_vaddr_d;
            m0_agu_prd_q       <= m0_agu_prd_d;
        end
    end

    always_ff @(posedge clk) begin : m0_lrq_vld_dff
        if (rst) begin
            m0_lrq_vld_q <= 1'b0;
        end else begin
            m0_lrq_vld_q <= m0_lrq_vld_d;
        end
    end

    always_ff @(posedge clk) begin : m0_lrq_payload_dff
        if (m0_lrq_clk_en) begin
            m0_lrq_io_q      <= m0_lrq_io_d;
            m0_lrq_rob_tag_q <= m0_lrq_rob_tag_d;
            m0_lrq_tag_q     <= m0_lrq_tag_d;
            m0_lrq_opcode_q  <= m0_lrq_opcode_d;
            m0_lrq_prd_q     <= m0_lrq_prd_d;
            m0_lrq_paddr_q   <= m0_lrq_paddr_d;
        end
    end

    always_ff @(posedge clk) begin : m1_vld_dff
        if (rst) begin
            m1_vld_q <= 1'b0;
        end else begin
            m1_vld_q <= m1_vld_d;
        end
    end

    always_ff @(posedge clk) begin : m1_payload_dff
        if (m1_clk_en) begin
            m1_is_lrq_q <= m1_is_lrq_d;
            m1_lrq_io_q <= m1_lrq_io_d;
            m1_excp_vld_q <= m1_excp_vld_d;
            m1_rob_tag_q <= m1_rob_tag_d;
            m1_lsq_tag_q <= m1_lsq_tag_d;
            m1_iq_tag_q <= m1_iq_tag_d;
            m1_fu_type_q <= m1_fu_type_d;
            m1_opcode_q <= m1_opcode_d;
            m1_addr_q <= m1_addr_d;
            m1_prd_q <= m1_prd_d;
        end
    end


    always_ff @(posedge clk) begin : m2_vld_dff
        if (rst) begin
            m2_vld_q <= 1'b0;
        end else begin
            m2_vld_q <= m2_vld_d;
        end
    end

    always_ff @(posedge clk) begin : m2_payload_dff
        if (m2_clk_en) begin
            m2_is_lrq_q <= m2_is_lrq_d;
            m2_excp_vld_q <= m2_excp_vld_d;
            m2_excp_cause_q <= m2_excp_cause_d;
            m2_rob_tag_q <= m2_rob_tag_d;
            m2_lsq_tag_q <= m2_lsq_tag_d;
            m2_fu_type_q <= m2_fu_type_d;
            m2_opcode_q <= m2_opcode_d;
            m2_vaddr_q <= m2_vaddr_d;
            m2_paddr_q <= m2_paddr_d;
            m2_prd_q <= m2_prd_d;
            m2_pma_check_io_q <= m2_pma_check_io_d;
            m2_rar_check_fail_q <= m2_rar_check_fail_d;
            m2_pending_st_exist_q <= m2_pending_st_exist_d;
            m2_raw_check_fail_q <= m2_raw_check_fail_d;
        end
    end

endmodule : rvh_ls_pipe
