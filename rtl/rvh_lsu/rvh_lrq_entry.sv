module rvh_lrq_entry
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_rcu_pkg::*;
(
    // Allocate
    input logic                     disp_lrq_alloc_vld_i,
    input logic [ROB_TAG_WIDTH-1:0] disp_lrq_alloc_rob_tag_i,

    // Deallocate
    input  logic                      ld_dealloc_vld_i,
    // L1 D$ -> LRQ : Sleep
    input  logic                      ld_sleep_vld_i,
    input  logic [PREG_TAG_WIDTH-1:0] ld_sleep_prd_i,
    input  logic [  LDU_OP_WIDTH-1:0] ld_sleep_opcode_i,
    input  logic [   PADDR_WIDTH-1:0] ld_sleep_paddr_i,
    // Sleep Reason
    input  logic                      ld_sleep_io_region_i,
    input  logic                      ld_sleep_raw_hazard_i,
    // LRQ -> L1 D$
    input  logic                      replay_vld_i,
    output logic                      replay_io_region_o,
    output logic [ ROB_TAG_WIDTH-1:0] replay_rob_tag_o,
    output logic [PREG_TAG_WIDTH-1:0] replay_prd_o,
    output logic [  LDU_OP_WIDTH-1:0] replay_opcode_o,
    output logic [   PADDR_WIDTH-1:0] replay_paddr_o,
    output logic                      replay_rdy_o,

    input logic rar_rdy_i,
    input logic raw_rdy_i,

    // Wake up (LRQ)
    input logic [ROB_TAG_WIDTH-1:0] oldest_ld_rob_tag_i,
    // Wake up (STQ)
    input logic                     stq_empty_i,
    input logic [ROB_TAG_WIDTH-1:0] oldest_st_rob_tag_i,
    // Wake up (BRQ)
    input logic                     brq_empty_i,
    input logic [ROB_TAG_WIDTH-1:0] oldest_br_rob_tag_i,

    input logic flush_i,

    input clk,
    input rst
);

    localparam int unsigned STATUS_WIDTH = 1;
    localparam int unsigned HAZARD_TYPE_WIDTH = 2;

    // Status
    localparam logic [STATUS_WIDTH-1:0] STATUS_INIT = 1'b0;
    localparam logic [STATUS_WIDTH-1:0] STATUS_HAZARD = 1'b1;

    // Hazard Type
    localparam logic [HAZARD_TYPE_WIDTH-1:0] HAZARD_RAW = 2'd0;
    localparam logic [HAZARD_TYPE_WIDTH-1:0] HAZARD_IO = 2'd1;
    localparam logic [HAZARD_TYPE_WIDTH-1:0] HAZARD_RAXQ_FULL = 2'd2;



    // Clock Gating
    logic alloc_clk_en, sleep_clk_en, replay_clk_en, dealloc_clk_en;


    // Change when allocate
    logic [ROB_TAG_WIDTH-1:0]rob_tag_d, rob_tag_q;

    // Change when allocate & deallocate
    logic vld_d, vld_q;

    // Change when allocate & sleep & issue 
    logic [STATUS_WIDTH-1:0] status_d, status_q;


    // Change when sleep
    logic [HAZARD_TYPE_WIDTH-1:0] hazard_d, hazard_q;
    logic [PREG_TAG_WIDTH-1:0] prd_d, prd_q;
    logic [PADDR_WIDTH-1:0] addr_d, addr_q;
    logic [LDU_OP_WIDTH-1:0] opcode_d, opcode_q;

    // Status FSM
    logic init, init2sleep, sleep2init;

    // Hazard Handling
    logic resolve_br_hazard;
    logic resolve_sc_hazard;
    logic exist_rar_hazard;
    logic exist_raw_hazard;

    assign alloc_clk_en = disp_lrq_alloc_vld_i;
    assign sleep_clk_en = ld_sleep_vld_i;
    assign replay_clk_en = replay_vld_i;
    assign dealloc_clk_en = ld_dealloc_vld_i;


    assign exist_rar_hazard = rob_tag_is_older(oldest_ld_rob_tag_i, rob_tag_q);
    assign exist_raw_hazard = ~stq_empty_i & rob_tag_is_older(oldest_st_rob_tag_i, rob_tag_q);
    assign resolve_br_hazard = brq_empty_i | rob_tag_is_older(rob_tag_q, oldest_br_rob_tag_i);
    assign resolve_sc_hazard = ~exist_rar_hazard & ~exist_raw_hazard;


    // Output 
    assign replay_rob_tag_o = rob_tag_q;
    assign replay_prd_o = prd_q;
    assign replay_opcode_o = opcode_q;
    assign replay_paddr_o = addr_q;
    assign replay_io_region_o = (status_q == STATUS_HAZARD) & (hazard_q == HAZARD_IO);
    assign replay_rdy_o = vld_q & (status_q == STATUS_HAZARD) &
        (((hazard_q == HAZARD_RAW) & ~exist_raw_hazard) |
         ((hazard_q == HAZARD_IO) & resolve_br_hazard & resolve_sc_hazard) |
        ((hazard_q == HAZARD_RAXQ_FULL) & (~exist_rar_hazard | rar_rdy_i) & (~exist_raw_hazard | raw_rdy_i))
    );

    assign vld_d = ~flush_i & disp_lrq_alloc_vld_i & ~ld_dealloc_vld_i;
    assign rob_tag_d = disp_lrq_alloc_rob_tag_i;
    assign hazard_d = (HAZARD_RAW & {HAZARD_TYPE_WIDTH{ld_sleep_raw_hazard_i}}) |
        (HAZARD_IO & {HAZARD_TYPE_WIDTH{ld_sleep_io_region_i}}) |
        (HAZARD_RAXQ_FULL & {HAZARD_TYPE_WIDTH{~ld_sleep_raw_hazard_i & ~ld_sleep_io_region_i}});
    assign addr_d = ld_sleep_paddr_i;
    assign opcode_d = ld_sleep_opcode_i;
    assign prd_d = ld_sleep_prd_i;

    always_comb begin : status_fsm
        if (init) begin
            status_d = STATUS_INIT;
        end else if (init2sleep) begin
            status_d = STATUS_HAZARD;
        end else if (sleep2init) begin
            status_d = STATUS_INIT;
        end else begin
            status_d = status_q;
        end
    end

    assign init = disp_lrq_alloc_vld_i;
    assign init2sleep = (status_q == STATUS_INIT) & ld_sleep_vld_i;
    assign sleep2init = (status_q == STATUS_HAZARD) & replay_vld_i;

    // DFFs
    always_ff @(posedge clk) begin : rob_tag_dff
        if (alloc_clk_en) begin
            rob_tag_q <= rob_tag_d;
        end
    end

    always_ff @(posedge clk) begin : vld_dff
        if (rst) begin
            vld_q <= 1'b0;
        end else if (alloc_clk_en | dealloc_clk_en | flush_i) begin
            vld_q <= vld_d;
        end
    end

    always_ff @(posedge clk) begin : status_dff
        if (alloc_clk_en | sleep_clk_en | replay_clk_en) begin
            status_q <= status_d;
        end
    end

    always_ff @(posedge clk) begin : sleep_payload_dff
        if (sleep_clk_en) begin
            hazard_q          <= hazard_d;
            prd_q             <= prd_d;
            addr_q            <= addr_d;
            opcode_q          <= opcode_d;
        end
    end

endmodule : rvh_lrq_entry
