module rvh_lrq
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_rcu_pkg::*;
#(
    localparam int unsigned ENTRY_COUNT = LDQ_ENTRY_COUNT
) (
    // Rename -> LRQ : Allocate
    input  logic [    LSU_ALLOC_WIDTH-1:0]                     disp_lrq_alloc_vld_i,
    input  logic [    LSU_ALLOC_WIDTH-1:0][ ROB_TAG_WIDTH-1:0] disp_lrq_alloc_rob_tag_i,
    output logic [    LSU_ALLOC_WIDTH-1:0][ LDQ_TAG_WIDTH-1:0] disp_lrq_alloc_tag_o,
    output logic [    LSU_ALLOC_WIDTH-1:0]                     disp_lrq_alloc_rdy_o,
    // L1 D$ -> LRQ : Resp
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][ LDQ_TAG_WIDTH-1:0] ld_resp_lrq_tag_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                     ld_resp_dealloc_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                     ld_resp_sleep_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] ld_resp_sleep_prd_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][  LDU_OP_WIDTH-1:0] ld_resp_sleep_opcode_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][   PADDR_WIDTH-1:0] ld_resp_sleep_paddr_i,
    // Sleep Reason
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                     ld_resp_sleep_io_region_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                     ld_resp_sleep_raw_hazard_i,
    // LRQ -> L1 D$
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                     lrq_l1d_replay_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][ LDQ_TAG_WIDTH-1:0] lrq_l1d_replay_ldq_tag_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][ ROB_TAG_WIDTH-1:0] lrq_l1d_replay_rob_tag_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                     lrq_l1d_replay_io_region_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] lrq_l1d_replay_prd_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][  LDU_OP_WIDTH-1:0] lrq_l1d_replay_opcode_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][   PADDR_WIDTH-1:0] lrq_l1d_replay_paddr_o,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                     lrq_l1d_replay_rdy_i,

    input  logic                     rar_rdy_i,
    input  logic                     raw_rdy_i,
    // Wake up
    output logic                     ldq_empty_o,
    output logic [ROB_TAG_WIDTH-1:0] oldest_ld_rob_tag_o,
    // Wake up (STQ)
    input  logic                     stq_empty_i,
    input  logic [ROB_TAG_WIDTH-1:0] oldest_st_rob_tag_i,
    // Wake up (BRQ)
    input  logic                     brq_empty_i,
    input  logic [ROB_TAG_WIDTH-1:0] oldest_br_rob_tag_i,
    input  logic                     flush_i,

    input clk,
    input rst
);

    // Allocate
    logic [LSU_ALLOC_WIDTH-1:0] alloc_fire;
    logic [LSU_ALLOC_WIDTH-1:0][LDQ_TAG_WIDTH-1:0] alloc_ldq_tag;

    logic [ENTRY_COUNT-1:0] disp_lrq_entry_alloc_vld;
    logic [ENTRY_COUNT-1:0][ROB_TAG_WIDTH-1:0] disp_lrq_entry_alloc_rob_tag;

    assign alloc_fire = disp_lrq_alloc_vld_i & disp_lrq_alloc_rdy_o;

    always_comb begin : gen_alloc_entry_signal
        disp_lrq_entry_alloc_vld = {ENTRY_COUNT{1'b0}};
        disp_lrq_entry_alloc_rob_tag = {ENTRY_COUNT * ROB_TAG_WIDTH{1'b0}};
        for (int i = 0; i < LSU_ALLOC_WIDTH; i++) begin
            if (alloc_fire[i]) begin
                disp_lrq_entry_alloc_vld[alloc_ldq_tag[i]] = 1'b1;
                disp_lrq_entry_alloc_rob_tag[alloc_ldq_tag[i]] = disp_lrq_alloc_rob_tag_i[i];
            end
        end
    end

    assign disp_lrq_alloc_tag_o = alloc_ldq_tag;

    // LD Response
    logic [ENTRY_COUNT-1:0]                     ld_entry_dealloc_vld;

    logic [ENTRY_COUNT-1:0]                     ld_entry_sleep_vld;
    logic [ENTRY_COUNT-1:0][PREG_TAG_WIDTH-1:0] ld_entry_sleep_prd;
    logic [ENTRY_COUNT-1:0][  LDU_OP_WIDTH-1:0] ld_entry_sleep_opcode;
    logic [ENTRY_COUNT-1:0][   PADDR_WIDTH-1:0] ld_entry_sleep_paddr;
    logic [ENTRY_COUNT-1:0]                     ld_entry_sleep_io_region;
    logic [ENTRY_COUNT-1:0]                     ld_entry_sleep_raw_hazard;

    always_comb begin : gen_ld_entry_dealloc_vld
        ld_entry_dealloc_vld = {ENTRY_COUNT{1'b0}};
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if (ld_resp_dealloc_vld_i[i]) begin
                ld_entry_dealloc_vld[ld_resp_lrq_tag_i[i]] = 1'b1;
            end
        end
    end

    always_comb begin : gen_ld_entry_sleep_signal
        ld_entry_sleep_vld        = {ENTRY_COUNT{1'b0}};
        ld_entry_sleep_prd        = {ENTRY_COUNT * PREG_TAG_WIDTH{1'b0}};
        ld_entry_sleep_opcode     = {ENTRY_COUNT * LDU_OP_WIDTH{1'b0}};
        ld_entry_sleep_paddr      = {ENTRY_COUNT * PADDR_WIDTH{1'b0}};
        ld_entry_sleep_io_region  = {ENTRY_COUNT{1'b0}};
        ld_entry_sleep_raw_hazard = {ENTRY_COUNT{1'b0}};
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if (ld_resp_sleep_vld_i[i]) begin
                ld_entry_sleep_vld[ld_resp_lrq_tag_i[i]] = 1'b1;
                ld_entry_sleep_prd[ld_resp_lrq_tag_i[i]] = ld_resp_sleep_prd_i[i];
                ld_entry_sleep_opcode[ld_resp_lrq_tag_i[i]] = ld_resp_sleep_opcode_i[i];
                ld_entry_sleep_paddr[ld_resp_lrq_tag_i[i]] = ld_resp_sleep_paddr_i[i];
                ld_entry_sleep_io_region[ld_resp_lrq_tag_i[i]] = ld_resp_sleep_io_region_i[i];
                ld_entry_sleep_raw_hazard[ld_resp_lrq_tag_i[i]] = ld_resp_sleep_raw_hazard_i[i];
            end
        end
    end

    // Replay
    logic [        ENTRY_COUNT-1:0]                     vld_mask;
    logic [        ENTRY_COUNT-1:0]                     oldest_entry_mask;
    logic [        ENTRY_COUNT-1:0][ LDQ_TAG_WIDTH-1:0] entry_tag;
    logic [      LDQ_TAG_WIDTH-1:0]                     oldest_lrq_tag;
    logic [      ROB_TAG_WIDTH-1:0]                     oldest_ld_rob_tag;

    logic [        ENTRY_COUNT-1:0]                     replay_entry_fire;
    logic [        ENTRY_COUNT-1:0]                     replay_entry_io_region;
    logic [        ENTRY_COUNT-1:0][ ROB_TAG_WIDTH-1:0] replay_entry_rob_tag;
    logic [        ENTRY_COUNT-1:0][PREG_TAG_WIDTH-1:0] replay_entry_prd;
    logic [        ENTRY_COUNT-1:0][  LDU_OP_WIDTH-1:0] replay_entry_opcode;
    logic [        ENTRY_COUNT-1:0][   PADDR_WIDTH-1:0] replay_entry_paddr;
    logic [        ENTRY_COUNT-1:0]                     replay_entry_rdy;

    logic [LSU_ADDR_PIPE_COUNT-1:0][   ENTRY_COUNT-1:0] masked_replay_entry_rdy_vec;
    logic [LSU_ADDR_PIPE_COUNT-1:0][   ENTRY_COUNT-1:0] replay_entry_sel_mask;
    logic [LSU_ADDR_PIPE_COUNT-1:0][ LDQ_TAG_WIDTH-1:0] replay_entry_tag;

    logic [LSU_ADDR_PIPE_COUNT-1:0]                     replay_vld;
    logic [LSU_ADDR_PIPE_COUNT-1:0]                     replay_fire;

    logic [LSU_ADDR_PIPE_COUNT-1:0]                     replay_io_region;
    logic [LSU_ADDR_PIPE_COUNT-1:0][ ROB_TAG_WIDTH-1:0] replay_rob_tag;
    logic [LSU_ADDR_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] replay_prd;
    logic [LSU_ADDR_PIPE_COUNT-1:0][  LDU_OP_WIDTH-1:0] replay_opcode;
    logic [LSU_ADDR_PIPE_COUNT-1:0][   PADDR_WIDTH-1:0] replay_paddr;

    always_comb begin : gen_entry_tag
        for (int i = 0; i < ENTRY_COUNT; i++) begin
            entry_tag[i] = i[LDQ_TAG_WIDTH-1:0];
        end
    end

    always_comb begin : gen_masked_replay_entry_rdy_vec
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if (i == 0) begin
                masked_replay_entry_rdy_vec[i] = replay_entry_rdy;
            end else begin
                masked_replay_entry_rdy_vec[i] = ~replay_entry_sel_mask[i-1] &
                    masked_replay_entry_rdy_vec[i-1];
            end
        end
    end

    always_comb begin : gen_replay_vld
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            replay_vld[i] = |replay_entry_sel_mask[i];
        end
    end

    always_comb begin : gen_replay_entry_fire
        replay_entry_fire = {ENTRY_COUNT{1'b0}};
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if (replay_fire[i]) begin
                replay_entry_fire[replay_entry_tag[i]] = 1'b1;
            end
        end
    end

    // OUTPUT 
    assign lrq_l1d_replay_vld_o       = replay_vld;
    assign lrq_l1d_replay_ldq_tag_o   = replay_entry_tag;
    assign lrq_l1d_replay_rob_tag_o   = replay_rob_tag;
    assign lrq_l1d_replay_io_region_o = replay_io_region;
    assign lrq_l1d_replay_prd_o       = replay_prd;
    assign lrq_l1d_replay_opcode_o    = replay_opcode;
    assign lrq_l1d_replay_paddr_o     = replay_paddr;

    assign ldq_empty_o                = ~(|vld_mask);
    assign oldest_ld_rob_tag_o        = oldest_ld_rob_tag;


    assign replay_fire                = lrq_l1d_replay_vld_o & lrq_l1d_replay_rdy_i;


    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_replay_tag_onehot_mux (
        .sel_i (oldest_entry_mask),
        .data_i(replay_entry_rob_tag),
        .data_o(oldest_ld_rob_tag)
    );

    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin : gen_replay_entry_tag
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (LDQ_TAG_WIDTH)
            ) u_replay_tag_onehot_mux (
                .sel_i (replay_entry_sel_mask[i]),
                .data_i(entry_tag),
                .data_o(replay_entry_tag[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (ROB_TAG_WIDTH)
            ) u_rob_tag_onehot_mux (
                .sel_i (replay_entry_sel_mask[i]),
                .data_i(replay_entry_rob_tag),
                .data_o(replay_rob_tag[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (1)
            ) u_replay_io_region_onehot_mux (
                .sel_i (replay_entry_sel_mask[i]),
                .data_i(replay_entry_io_region),
                .data_o(replay_io_region[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (PREG_TAG_WIDTH)
            ) u_replay_prd_onehot_mux (
                .sel_i (replay_entry_sel_mask[i]),
                .data_i(replay_entry_prd),
                .data_o(replay_prd[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (LDU_OP_WIDTH)
            ) u_replay_opcode_onehot_mux (
                .sel_i (replay_entry_sel_mask[i]),
                .data_i(replay_entry_opcode),
                .data_o(replay_opcode[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(ENTRY_COUNT),
                .DATA_WIDTH  (PADDR_WIDTH)
            ) u_replay_paddr_onehot_mux (
                .sel_i (replay_entry_sel_mask[i]),
                .data_i(replay_entry_paddr),
                .data_o(replay_paddr[i])
            );
        end
    endgenerate

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (LDQ_TAG_WIDTH)
    ) u_head_onehot_mux (
        .sel_i (oldest_entry_mask),
        .data_i(entry_tag),
        .data_o(oldest_lrq_tag)
    );

    age_order_selector_with_head #(
        .ENTRY_COUNT(ENTRY_COUNT),
        .ENQ_WIDTH(LSU_ADDR_PIPE_COUNT),
        .DEQ_WIDTH(LSU_ADDR_PIPE_COUNT),
        .SEL_WIDTH(LSU_ADDR_PIPE_COUNT),
        .HEAD_PTR_COUNT(1)
    ) u_age_order_selector_with_head (
        .enq_vld_i(alloc_fire),
        .enq_tag_i(alloc_ldq_tag),
        .deq_vld_i(ld_resp_dealloc_vld_i),
        .deq_tag_i(ld_resp_lrq_tag_i),
        .vld_mask_o(vld_mask),
        .oldest_mask_o(oldest_entry_mask),
        .sel_mask_i(masked_replay_entry_rdy_vec),
        .sel_oldest_mask_o(replay_entry_sel_mask),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    mp_freelist #(
        .ENTRY_COUNT(ENTRY_COUNT),
        .ALLOC_WIDTH(LSU_ADDR_PIPE_COUNT),
        .DEALLOC_WIDTH(LSU_ADDR_PIPE_COUNT),
        .MUST_TAKEN_ALL(1)
    ) u_mp_freelist (
        .alloc_vld_i(disp_lrq_alloc_vld_i),
        .alloc_tag_o(alloc_ldq_tag),
        .alloc_rdy_o(disp_lrq_alloc_rdy_o),
        .dealloc_vld_i(ld_resp_dealloc_vld_i),
        .dealloc_tag_i(ld_resp_lrq_tag_i),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );
    generate
        for (genvar i = 0; i < ENTRY_COUNT; i++) begin
            rvh_lrq_entry u_rvh_lrq_entry (
                .disp_lrq_alloc_vld_i(disp_lrq_entry_alloc_vld[i]),
                .disp_lrq_alloc_rob_tag_i(disp_lrq_entry_alloc_rob_tag[i]),
                .ld_dealloc_vld_i(ld_entry_dealloc_vld[i]),
                .ld_sleep_vld_i(ld_entry_sleep_vld[i]),
                .ld_sleep_prd_i(ld_entry_sleep_prd[i]),
                .ld_sleep_opcode_i(ld_entry_sleep_opcode[i]),
                .ld_sleep_paddr_i(ld_entry_sleep_paddr[i]),
                .ld_sleep_io_region_i(ld_entry_sleep_io_region[i]),
                .ld_sleep_raw_hazard_i(ld_entry_sleep_raw_hazard[i]),
                .replay_vld_i(replay_entry_fire[i]),
                .replay_io_region_o(replay_entry_io_region[i]),
                .replay_rob_tag_o(replay_entry_rob_tag[i]),
                .replay_prd_o(replay_entry_prd[i]),
                .replay_opcode_o(replay_entry_opcode[i]),
                .replay_paddr_o(replay_entry_paddr[i]),
                .replay_rdy_o(replay_entry_rdy[i]),
                .rar_rdy_i(rar_rdy_i),
                .raw_rdy_i(raw_rdy_i),
                .oldest_ld_rob_tag_i(oldest_ld_rob_tag_o),
                .stq_empty_i(stq_empty_i),
                .oldest_st_rob_tag_i(oldest_st_rob_tag_i),
                .brq_empty_i(brq_empty_i),
                .oldest_br_rob_tag_i(oldest_br_rob_tag_i),
                .flush_i(flush_i),
                .clk(clk),
                .rst(rst)
            );
        end
    endgenerate


endmodule : rvh_lrq
