module rvh_stq
    import rvh_pkg::*;
    import rvh_lsu_pkg::*;
    import uop_encoding_pkg::*;
#(
    localparam int unsigned ALLOC_WIDTH   = LSU_ALLOC_WIDTH,
    localparam int unsigned DEALLOC_WIDTH = LSU_DATA_PIPE_COUNT
) (
    // Allocate
    input  logic [        ALLOC_WIDTH-1:0]                       disp_stq_alloc_vld_i,
    input  logic [        ALLOC_WIDTH-1:0]                       disp_stq_alloc_is_fence_i,
    input  logic [        ALLOC_WIDTH-1:0][    STU_OP_WIDTH-1:0] disp_stq_alloc_minor_op_i,
    input  logic [        ALLOC_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] disp_stq_alloc_rob_tag_i,
    output logic [        ALLOC_WIDTH-1:0][   STQ_TAG_WIDTH-1:0] tail_stq_tag_o,
    output logic [        ALLOC_WIDTH-1:0]                       disp_stq_alloc_rdy_o,
    // DTLB -> STQ : Issue Address
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       agu_stq_issue_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       agu_stq_issue_io_region_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][  PREG_TAG_WIDTH-1:0] agu_stq_issue_prd_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][    STU_OP_WIDTH-1:0] agu_stq_issue_opcode_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] agu_stq_issue_paddr_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][   STQ_TAG_WIDTH-1:0] agu_stq_issue_stq_tag_i,
    // SDU -> STQ : Issue Data
    input  logic [LSU_DATA_PIPE_COUNT-1:0]                       sdu_stq_issue_vld_i,
    input  logic [LSU_DATA_PIPE_COUNT-1:0][   STQ_TAG_WIDTH-1:0] sdu_stq_issue_stq_tag_i,
    input  logic [LSU_DATA_PIPE_COUNT-1:0][            XLEN-1:0] sdu_stq_issue_data_i,
    // LDU -> STQ : Check STQ
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       ldu_stq_check_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] ldu_stq_check_rob_tag_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ldu_stq_check_len_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] ldu_stq_check_paddr_i,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       ldu_stq_check_raw_hazard_o,
    // LRQ -> STQ : Wake up
    input  logic                                                 lrq_empty_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       oldest_ld_rob_tag_i,
    // BRQ -> STQ : Wake up
    input  logic                                                 brq_empty_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       oldest_br_rob_tag_i,
    // SB -> STQ 
    input  logic                                                 sb_empty_i,
    // STQ -> L1D : execute
    output logic [LSU_DATA_PIPE_COUNT-1:0]                       stq_l1d_exec_vld_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] stq_l1d_exec_rob_tag_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0]                       stq_l1d_exec_io_region_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0]                       stq_l1d_exec_is_fence_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][  PREG_TAG_WIDTH-1:0] stq_l1d_exec_prd_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][    STU_OP_WIDTH-1:0] stq_l1d_exec_opcode_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] stq_l1d_exec_paddr_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][            XLEN-1:0] stq_l1d_exec_data_o,
    input  logic [LSU_DATA_PIPE_COUNT-1:0]                       stq_l1d_exec_rdy_i,
    input  logic [LSU_DATA_PIPE_COUNT-1:0]                       stq_l1d_exec_io_rdy_i,
    // STQ -> Boardcast
    output logic                                                 stq_empty_o,
    output logic [      ROB_TAG_WIDTH-1:0]                       oldest_st_rob_tag_o,

    input logic flush_i,
    input clk,
    input rst
);
    localparam int unsigned STQ_CNT_WIDTH = $clog2(STQ_ENTRY_COUNT + 1);
    localparam int unsigned ALLOC_CNT_WIDTH = $clog2(ALLOC_WIDTH + 1);
    localparam int unsigned DEALLOC_CNT_WIDTH = $clog2(DEALLOC_WIDTH + 1);

    logic [  STQ_CNT_WIDTH-1:0]                    avail_cnt;
    logic                                          enough_entries;

    logic [    ALLOC_WIDTH-1:0]                    alloc_fire;

    logic [  DEALLOC_WIDTH-1:0][STQ_TAG_WIDTH-1:0] head_stq_tag;
    logic [    ALLOC_WIDTH-1:0][STQ_TAG_WIDTH-1:0] tail_stq_tag;
    // Allocate
    logic [STQ_ENTRY_COUNT-1:0]                    disp_stq_alloc_entry_vld;
    logic [STQ_ENTRY_COUNT-1:0]                    disp_stq_alloc_entry_is_fence;
    logic [STQ_ENTRY_COUNT-1:0][ STU_OP_WIDTH-1:0] disp_stq_alloc_entry_minor_op;
    logic [STQ_ENTRY_COUNT-1:0][ROB_TAG_WIDTH-1:0] disp_stq_alloc_entry_rob_tag;

    assign enough_entries = avail_cnt >= ALLOC_WIDTH;

    assign alloc_fire = disp_stq_alloc_vld_i & disp_stq_alloc_rdy_o;

    always_comb begin : gen_alloc_entries_payload
        disp_stq_alloc_entry_vld          = {STQ_ENTRY_COUNT{1'b0}};
        disp_stq_alloc_entry_is_fence     = {STQ_ENTRY_COUNT{1'b0}};
        disp_stq_alloc_entry_minor_op     = {STQ_ENTRY_COUNT*STU_OP_WIDTH{1'b0}};
        disp_stq_alloc_entry_rob_tag      = {STQ_ENTRY_COUNT * ROB_TAG_WIDTH{1'b0}};
        for (int i = 0; i < ALLOC_WIDTH; i++) begin
            if (alloc_fire[i]) begin
                disp_stq_alloc_entry_vld[tail_stq_tag[i]] = 1'b1;
                disp_stq_alloc_entry_is_fence[tail_stq_tag[i]] = disp_stq_alloc_is_fence_i[i];
                disp_stq_alloc_entry_rob_tag[tail_stq_tag[i]] = disp_stq_alloc_rob_tag_i[i];
                disp_stq_alloc_entry_minor_op[tail_stq_tag[i]] = disp_stq_alloc_minor_op_i[i];
            end
        end
    end
    // Allocate Response
    assign disp_stq_alloc_rdy_o = {ALLOC_WIDTH{enough_entries}};
    assign tail_stq_tag_o = tail_stq_tag;


    // Address Issue
    logic [STQ_ENTRY_COUNT-1:0]                     agu_stq_issue_entry_vld;
    logic [STQ_ENTRY_COUNT-1:0]                     agu_stq_issue_entry_io_region;
    logic [STQ_ENTRY_COUNT-1:0][PREG_TAG_WIDTH-1:0] agu_stq_issue_entry_prd;
    logic [STQ_ENTRY_COUNT-1:0][  STU_OP_WIDTH-1:0] agu_stq_issue_entry_opcode;
    logic [STQ_ENTRY_COUNT-1:0][   PADDR_WIDTH-1:0] agu_stq_issue_entry_paddr;

    always_comb begin : gen_addr_issue_payload
        agu_stq_issue_entry_vld       = {STQ_ENTRY_COUNT{1'b0}};
        agu_stq_issue_entry_io_region = {STQ_ENTRY_COUNT{1'b0}};
        agu_stq_issue_entry_prd       = {STQ_ENTRY_COUNT * PREG_TAG_WIDTH{1'b0}};
        agu_stq_issue_entry_opcode    = {STQ_ENTRY_COUNT * STU_OP_WIDTH{1'b0}};
        agu_stq_issue_entry_paddr     = {STQ_ENTRY_COUNT * PADDR_WIDTH{1'b0}};
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            if (agu_stq_issue_vld_i[i]) begin
                agu_stq_issue_entry_vld[agu_stq_issue_stq_tag_i[i]] = 1'b1;
                agu_stq_issue_entry_io_region[agu_stq_issue_stq_tag_i[i]] =
                    agu_stq_issue_io_region_i[i];
                agu_stq_issue_entry_prd[agu_stq_issue_stq_tag_i[i]] = agu_stq_issue_prd_i[i];
                agu_stq_issue_entry_opcode[agu_stq_issue_stq_tag_i[i]] = agu_stq_issue_opcode_i[i];
                agu_stq_issue_entry_paddr[agu_stq_issue_stq_tag_i[i]] = agu_stq_issue_paddr_i[i];
            end
        end
    end

    // Data Issue
    logic [STQ_ENTRY_COUNT-1:0]           sdu_stq_issue_entry_vld;
    logic [STQ_ENTRY_COUNT-1:0][XLEN-1:0] sdu_stq_issue_entry_data;

    always_comb begin : gen_data_issue_payload
        sdu_stq_issue_entry_vld  = {STQ_ENTRY_COUNT{1'b0}};
        sdu_stq_issue_entry_data = {STQ_ENTRY_COUNT * XLEN{1'b0}};
        for (int i = 0; i < LSU_DATA_PIPE_COUNT; i++) begin
            if (sdu_stq_issue_vld_i[i]) begin
                sdu_stq_issue_entry_vld[sdu_stq_issue_stq_tag_i[i]]  = 1'b1;
                sdu_stq_issue_entry_data[sdu_stq_issue_stq_tag_i[i]] = sdu_stq_issue_data_i[i];
            end
        end
    end

    // RAW Check
    logic [STQ_ENTRY_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0] ldu_stq_check_entry_vld;
    logic [STQ_ENTRY_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0][ROB_TAG_WIDTH-1:0]
        ldu_stq_check_entry_rob_tag;
    logic [STQ_ENTRY_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0]
        ldu_stq_check_entry_len;
    logic [STQ_ENTRY_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0][PADDR_WIDTH-1:0] ldu_stq_check_entry_paddr;
    logic [STQ_ENTRY_COUNT-1:0][LSU_ADDR_PIPE_COUNT-1:0] ldu_stq_check_entry_raw_hazard;

    logic [LSU_ADDR_PIPE_COUNT-1:0][STQ_ENTRY_COUNT-1:0] trans_check_entry_raw_hazard;

    logic [LSU_ADDR_PIPE_COUNT-1:0][STQ_ENTRY_COUNT-1:0] shift_trans_check_entry_raw_hazard;

    logic [LSU_ADDR_PIPE_COUNT-1:0][STQ_ENTRY_COUNT-1:0] oldest_hazard_entry_mask;
    logic [LSU_ADDR_PIPE_COUNT-1:0] raw_hazard_found;


    always_comb begin : gen_raw_check_payload
        for (int i = 0; i < STQ_ENTRY_COUNT; i++) begin
            for (int j = 0; j < LSU_ADDR_PIPE_COUNT; j++) begin
                ldu_stq_check_entry_vld[i][j]     = 1'b0;
                ldu_stq_check_entry_rob_tag[i][j] = {ROB_TAG_WIDTH{1'b0}};
                ldu_stq_check_entry_len[i][j]     = {MEM_OP_LEN_WIDTH{1'b0}};
                ldu_stq_check_entry_paddr[i][j]   = {PADDR_WIDTH{1'b0}};
                if (ldu_stq_check_vld_i[j]) begin
                    ldu_stq_check_entry_vld[i][j]     = 1'b1;
                    ldu_stq_check_entry_rob_tag[i][j] = ldu_stq_check_rob_tag_i[j];
                    ldu_stq_check_entry_len[i][j]     = ldu_stq_check_len_i[j];
                    ldu_stq_check_entry_paddr[i][j]   = ldu_stq_check_paddr_i[j];
                end
            end
        end
    end

    always_comb begin : gen_trans_check_result
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            for (int j = 0; j < STQ_ENTRY_COUNT; j++) begin
                trans_check_entry_raw_hazard[i][j] = ldu_stq_check_entry_raw_hazard[j][i];
            end
        end
    end

    always_comb begin : get_shift_trans_check_result
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            shift_trans_check_entry_raw_hazard[i] = STQ_ENTRY_COUNT'({2{trans_check_entry_raw_hazard[i]}} >> head_stq_tag[0]);
        end
    end


    always_comb begin : gen_raw_hazard_result
        for (int i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            raw_hazard_found[i] = |trans_check_entry_raw_hazard[i];
        end
    end

    assign ldu_stq_check_raw_hazard_o = raw_hazard_found;

    // STQ -> L1 Dcache : Deallocate
    logic [    STQ_ENTRY_COUNT-1:0]                     stq_l1d_exec_entry_vld;
    logic [    STQ_ENTRY_COUNT-1:0][ ROB_TAG_WIDTH-1:0] stq_l1d_exec_entry_rob_tag;
    logic [    STQ_ENTRY_COUNT-1:0]                     stq_l1d_exec_entry_io_region;
    logic [    STQ_ENTRY_COUNT-1:0]                     stq_l1d_exec_entry_is_fence;
    logic [    STQ_ENTRY_COUNT-1:0][PREG_TAG_WIDTH-1:0] stq_l1d_exec_entry_prd;
    logic [    STQ_ENTRY_COUNT-1:0][  STU_OP_WIDTH-1:0] stq_l1d_exec_entry_opcode;
    logic [    STQ_ENTRY_COUNT-1:0][   PADDR_WIDTH-1:0] stq_l1d_exec_entry_paddr;
    logic [    STQ_ENTRY_COUNT-1:0][          XLEN-1:0] stq_l1d_exec_entry_data;
    logic [    STQ_ENTRY_COUNT-1:0]                     stq_l1d_exec_entry_rdy;

    logic [LSU_DATA_PIPE_COUNT-1:0]                     dealloc_fire;

    logic [LSU_DATA_PIPE_COUNT-1:0][ ROB_TAG_WIDTH-1:0] head_entry_rob_tag;
    logic [LSU_DATA_PIPE_COUNT-1:0]                     head_entry_io_region;
    logic [LSU_DATA_PIPE_COUNT-1:0]                     head_entry_is_fence;
    logic [LSU_DATA_PIPE_COUNT-1:0][PREG_TAG_WIDTH-1:0] head_entry_prd;
    logic [LSU_DATA_PIPE_COUNT-1:0][  STU_OP_WIDTH-1:0] head_entry_opcode;
    logic [LSU_DATA_PIPE_COUNT-1:0][   PADDR_WIDTH-1:0] head_entry_paddr;
    logic [LSU_DATA_PIPE_COUNT-1:0][          XLEN-1:0] head_entry_data;
    logic [LSU_DATA_PIPE_COUNT-1:0]                     head_entry_rdy;


    always_comb begin : get_head_entry_payload
        stq_l1d_exec_entry_vld = {STQ_ENTRY_COUNT{1'b0}};
        for (int i = 0; i < LSU_DATA_PIPE_COUNT; i++) begin
            stq_l1d_exec_entry_vld[head_stq_tag[i]] = dealloc_fire[i];
            head_entry_rob_tag[i]                   = stq_l1d_exec_entry_rob_tag[head_stq_tag[i]];
            head_entry_is_fence[i]                  = stq_l1d_exec_entry_is_fence[head_stq_tag[i]];
            head_entry_io_region[i]                 = stq_l1d_exec_entry_io_region[head_stq_tag[i]];
            head_entry_prd[i]                       = stq_l1d_exec_entry_prd[head_stq_tag[i]];
            head_entry_opcode[i]                    = stq_l1d_exec_entry_opcode[head_stq_tag[i]];
            head_entry_paddr[i]                     = stq_l1d_exec_entry_paddr[head_stq_tag[i]];
            head_entry_data[i]                      = stq_l1d_exec_entry_data[head_stq_tag[i]];
            if (i == 0) begin
                head_entry_rdy[i] = stq_l1d_exec_entry_rdy[head_stq_tag[i]];
            end else begin
                head_entry_rdy[i] = stq_l1d_exec_entry_rdy[head_stq_tag[i]] & head_entry_rdy[i-1] & ~head_entry_io_region[i-1];
            end
        end
    end

    // Output 
    assign stq_l1d_exec_vld_o       = head_entry_rdy;
    assign stq_l1d_exec_rob_tag_o   = head_entry_rob_tag;
    assign stq_l1d_exec_io_region_o = head_entry_io_region;
    assign stq_l1d_exec_is_fence_o  = head_entry_is_fence;
    assign stq_l1d_exec_prd_o       = head_entry_prd;
    assign stq_l1d_exec_opcode_o    = head_entry_opcode;
    assign stq_l1d_exec_paddr_o     = head_entry_paddr;
    assign stq_l1d_exec_data_o      = head_entry_data;


    assign stq_empty_o              = (avail_cnt == STQ_ENTRY_COUNT);
    assign oldest_st_rob_tag_o      = head_entry_rob_tag[0];

    always_comb begin
        for (int i = 0; i < LSU_DATA_PIPE_COUNT; i++) begin
            if (i == 0) begin
                dealloc_fire[i] = stq_l1d_exec_vld_o[i] & (stq_l1d_exec_io_region_o[i] ? stq_l1d_exec_io_rdy_i[i] : stq_l1d_exec_rdy_i[i]);
            end
            else begin
                dealloc_fire[i] = dealloc_fire[i - 1] & stq_l1d_exec_vld_o[i] & (stq_l1d_exec_io_region_o[i] ? stq_l1d_exec_io_rdy_i[i] : stq_l1d_exec_rdy_i[i]);
            end
        end
    end


    usage_manager #(
        .ENTRY_COUNT(STQ_ENTRY_COUNT),
        .ENQ_WIDTH(DEALLOC_WIDTH),
        .DEQ_WIDTH(ALLOC_WIDTH),
        .FLAG_EN(0),
        .INIT_IS_FULL(0),
        .COMB_DEQ_EN(0),
        .COMB_ENQ_EN(1)
    ) u_usage_manager (
        .enq_fire_i(alloc_fire),
        .deq_fire_i(dealloc_fire),
        .head_o(head_stq_tag),
        .tail_o(tail_stq_tag),
        .avail_cnt_o(avail_cnt),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            one_hot_priority_encoder #(
                .SEL_WIDTH(STQ_ENTRY_COUNT)
            ) u_one_hot_priority_encoder (
                .sel_i(shift_trans_check_entry_raw_hazard[i]),
                .sel_o(oldest_hazard_entry_mask[i])
            );
        end
    endgenerate


    generate
        for (genvar i = 0; i < STQ_ENTRY_COUNT; i++) begin : gen_stq_entries
            rvh_stq_entry u_rvh_stq_entry (
                .disp_stq_alloc_vld_i(disp_stq_alloc_entry_vld[i]),
                .disp_stq_alloc_is_fence_i(disp_stq_alloc_entry_is_fence[i]),
                .disp_stq_alloc_minor_op_i(disp_stq_alloc_entry_minor_op[i]),
                .disp_stq_alloc_rob_tag_i(disp_stq_alloc_entry_rob_tag[i]),
                .agu_stq_issue_vld_i(agu_stq_issue_entry_vld[i]),
                .agu_stq_issue_io_region_i(agu_stq_issue_entry_io_region[i]),
                .agu_stq_issue_prd_i(agu_stq_issue_entry_prd[i]),
                .agu_stq_issue_opcode_i(agu_stq_issue_entry_opcode[i]),
                .agu_stq_issue_paddr_i(agu_stq_issue_entry_paddr[i]),
                .sdu_stq_issue_vld_i(sdu_stq_issue_entry_vld[i]),
                .sdu_stq_issue_data_i(sdu_stq_issue_entry_data[i]),
                .ldu_stq_check_vld_i(ldu_stq_check_entry_vld[i]),
                .ldu_stq_check_rob_tag_i(ldu_stq_check_entry_rob_tag[i]),
                .ldu_stq_check_len_i(ldu_stq_check_entry_len[i]),
                .ldu_stq_check_paddr_i(ldu_stq_check_entry_paddr[i]),
                .ldu_stq_check_raw_hazard_o(ldu_stq_check_entry_raw_hazard[i]),
                .stq_l1d_exec_vld_i(stq_l1d_exec_entry_vld[i]),
                .stq_l1d_exec_rob_tag_o(stq_l1d_exec_entry_rob_tag[i]),
                .stq_l1d_exec_io_region_o(stq_l1d_exec_entry_io_region[i]),
                .stq_l1d_exec_is_fence_o(stq_l1d_exec_entry_is_fence[i]),
                .stq_l1d_exec_prd_o(stq_l1d_exec_entry_prd[i]),
                .stq_l1d_exec_opcode_o(stq_l1d_exec_entry_opcode[i]),
                .stq_l1d_exec_paddr_o(stq_l1d_exec_entry_paddr[i]),
                .stq_l1d_exec_data_o(stq_l1d_exec_entry_data[i]),
                .stq_l1d_exec_rdy_o(stq_l1d_exec_entry_rdy[i]),
                .lrq_empty_i(lrq_empty_i),
                .oldest_ld_rob_tag_i(oldest_ld_rob_tag_i),
                .brq_empty_i(brq_empty_i),
                .oldest_br_rob_tag_i(oldest_br_rob_tag_i),
                .sb_empty_i(sb_empty_i),
                .flush_i(flush_i),
                .clk(clk),
                .rst(rst)
            );
        end
    endgenerate



endmodule : rvh_stq
