module rvh_stq_entry
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_lsu_pkg::*;
    import rvh_rcu_pkg::*;
(

    // Allocate
    input  logic                                                 disp_stq_alloc_vld_i,
    input  logic                                                 disp_stq_alloc_is_fence_i,
    input  logic [       STU_OP_WIDTH-1:0]                       disp_stq_alloc_minor_op_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       disp_stq_alloc_rob_tag_i,
    // DTLB -> STQ : Issue Address
    input  logic                                                 agu_stq_issue_vld_i,
    input  logic                                                 agu_stq_issue_io_region_i,
    input  logic [     PREG_TAG_WIDTH-1:0]                       agu_stq_issue_prd_i,
    input  logic [       STU_OP_WIDTH-1:0]                       agu_stq_issue_opcode_i,
    input  logic [        PADDR_WIDTH-1:0]                       agu_stq_issue_paddr_i,
    // SDU -> STQ : Issue Data
    input  logic                                                 sdu_stq_issue_vld_i,
    input  logic [               XLEN-1:0]                       sdu_stq_issue_data_i,
    // LDU -> STQ : Check STQ
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       ldu_stq_check_vld_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] ldu_stq_check_rob_tag_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][MEM_OP_LEN_WIDTH-1:0] ldu_stq_check_len_i,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0][     PADDR_WIDTH-1:0] ldu_stq_check_paddr_i,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       ldu_stq_check_raw_hazard_o,
    // LRQ -> STQ : Wake up
    input  logic                                                 lrq_empty_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       oldest_ld_rob_tag_i,
    // BRQ -> STQ : Wake up
    input  logic                                                 brq_empty_i,
    input  logic [      ROB_TAG_WIDTH-1:0]                       oldest_br_rob_tag_i,
    // Store Buffer Status
    input  logic                                                 sb_empty_i,
    // STQ -> SB  : execute
    input  logic                                                 stq_l1d_exec_vld_i,
    output logic [      ROB_TAG_WIDTH-1:0]                       stq_l1d_exec_rob_tag_o,
    output logic                                                 stq_l1d_exec_io_region_o,
    output logic                                                 stq_l1d_exec_is_fence_o,
    output logic [     PREG_TAG_WIDTH-1:0]                       stq_l1d_exec_prd_o,
    output logic [       STU_OP_WIDTH-1:0]                       stq_l1d_exec_opcode_o,
    output logic [        PADDR_WIDTH-1:0]                       stq_l1d_exec_paddr_o,
    output logic [               XLEN-1:0]                       stq_l1d_exec_data_o,
    output logic                                                 stq_l1d_exec_rdy_o,

    input logic flush_i,

    input clk,
    input rst
);


    localparam int unsigned PADDR_TAG_LSB = 3;

    // Clock Gate
    logic alloc_clk_en, dealloc_clk_en, addr_issue_clk_en, data_issue_clk_en;

    // Change when alloc & dealloc
    logic vld_d, vld_q;

    // Change when alloc & addr issue
    logic addr_rdy_d, addr_rdy_q;

    // Change when alloc & data issue
    logic data_rdy_d, data_rdy_q;

    // Change when alloc
    logic [ROB_TAG_WIDTH-1:0] rob_tag_d, rob_tag_q;
    logic is_fence_d, is_fence_q;

    // Change when addr issue
    logic [STU_OP_WIDTH-1:0] opcode_d, opcode_q;
    logic [PADDR_WIDTH-1:0] paddr_d, paddr_q;
    logic [PREG_TAG_WIDTH-1:0] prd_d, prd_q;
    logic io_region_d, io_region_q;
    // Change when data issue
    logic [XLEN-1:0] data_d, data_q;


    assign alloc_clk_en      = disp_stq_alloc_vld_i;
    assign dealloc_clk_en    = stq_l1d_exec_vld_i;
    assign addr_issue_clk_en = agu_stq_issue_vld_i;
    assign data_issue_clk_en = sdu_stq_issue_vld_i;

    // SC Check
    logic resolve_sc_hazard;
    logic resolve_spec_hazard;

    assign resolve_sc_hazard   = lrq_empty_i | rob_tag_is_older(rob_tag_q, oldest_ld_rob_tag_i);
    assign resolve_spec_hazard = brq_empty_i | rob_tag_is_older(rob_tag_q, oldest_br_rob_tag_i);

    // RAW Check
    logic [       STU_OP_WIDTH-1:0]      pending_opcode;
    logic [        PADDR_WIDTH-1:0]      pending_paddr;
    logic                                pending_addr_rdy;

    logic [                    7:0]      check_wr_byte_mask;
    logic [LSU_ADDR_PIPE_COUNT-1:0]      check_wr_is_older;
    logic [LSU_ADDR_PIPE_COUNT-1:0]      check_tag_equal;
    logic [LSU_ADDR_PIPE_COUNT-1:0][7:0] check_rd_byte_mask;
    logic [LSU_ADDR_PIPE_COUNT-1:0][7:0] check_overlap_byte_mask;
    logic [LSU_ADDR_PIPE_COUNT-1:0]      found_raw_hazard;

    // Check incoming store
    assign pending_opcode     = agu_stq_issue_vld_i ? agu_stq_issue_opcode_i : opcode_q;
    assign pending_paddr      = agu_stq_issue_vld_i ? agu_stq_issue_paddr_i : paddr_q;
    assign pending_addr_rdy   = agu_stq_issue_vld_i ? 1'b1 : addr_rdy_q;

    assign check_wr_byte_mask = generate_st_byte_mask(pending_paddr, pending_opcode);

    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            assign check_wr_is_older[i] = rob_tag_is_older(rob_tag_q, ldu_stq_check_rob_tag_i[i]);
            assign check_tag_equal[i] = pending_paddr[PADDR_WIDTH-1:PADDR_TAG_LSB] ==
            ldu_stq_check_paddr_i[i][PADDR_WIDTH-1:PADDR_TAG_LSB];
            assign check_rd_byte_mask[i] = generate_byte_mask_with_len(
                ldu_stq_check_paddr_i[i], ldu_stq_check_len_i[i]
            );
            assign check_overlap_byte_mask[i] = check_wr_byte_mask & check_rd_byte_mask[i];
            assign found_raw_hazard[i] = ldu_stq_check_vld_i[i] & vld_q & check_wr_is_older[i] &
                ((check_tag_equal[i] & pending_addr_rdy & (|check_overlap_byte_mask[i])) | is_fence_q);
        end
    endgenerate



    // output 
    generate
        for (genvar i = 0; i < LSU_ADDR_PIPE_COUNT; i++) begin
            assign ldu_stq_check_raw_hazard_o[i] = found_raw_hazard[i];
        end
    endgenerate

    // Send to L1d Cache
    assign stq_l1d_exec_rob_tag_o = rob_tag_q;
    assign stq_l1d_exec_io_region_o = io_region_q;
    assign stq_l1d_exec_is_fence_o = is_fence_q;
    assign stq_l1d_exec_prd_o = prd_q;
    assign stq_l1d_exec_opcode_o = opcode_q;
    assign stq_l1d_exec_paddr_o = paddr_q;
    assign stq_l1d_exec_data_o = data_q;
    assign stq_l1d_exec_rdy_o = vld_q & resolve_sc_hazard & resolve_spec_hazard & addr_rdy_q & data_rdy_q & (sb_empty_i | ~is_fence_q);


    assign vld_d = ~flush_i & disp_stq_alloc_vld_i & ~stq_l1d_exec_vld_i;
    assign addr_rdy_d = disp_stq_alloc_vld_i ? (disp_stq_alloc_minor_op_i == STU_FENCE) : agu_stq_issue_vld_i;
    assign data_rdy_d = disp_stq_alloc_vld_i ? (disp_stq_alloc_minor_op_i == STU_FENCE) : sdu_stq_issue_vld_i;
    assign rob_tag_d = disp_stq_alloc_rob_tag_i;
    assign is_fence_d = disp_stq_alloc_is_fence_i;
    assign opcode_d = disp_stq_alloc_minor_op_i;
    assign io_region_d = agu_stq_issue_io_region_i;
    assign prd_d = agu_stq_issue_prd_i;
    assign paddr_d = agu_stq_issue_paddr_i;
    assign data_d = sdu_stq_issue_data_i;


    always_ff @(posedge clk) begin : vld_dff
        if (rst) begin
            vld_q <= 1'b0;
        end else begin
            if (alloc_clk_en | dealloc_clk_en | flush_i) begin
                vld_q <= vld_d;
            end
        end
    end

    always_ff @(posedge clk) begin : addr_vld_dff
        if (alloc_clk_en | addr_issue_clk_en) begin
            addr_rdy_q <= addr_rdy_d;
        end
    end

    always_ff @(posedge clk) begin : data_vld_dff
        if (alloc_clk_en | data_issue_clk_en) begin
            data_rdy_q <= data_rdy_d;
        end
    end

    always_ff @(posedge clk) begin : rob_tag_dff
        if (alloc_clk_en) begin
            rob_tag_q <= rob_tag_d;
        end
    end

    always_ff @(posedge clk) begin : is_fence_dff
        if (alloc_clk_en) begin
            is_fence_q <= is_fence_d;
        end
    end

    always_ff @(posedge clk) begin : paddr_dff
        if (addr_issue_clk_en) begin
            paddr_q <= paddr_d;
        end
    end

    always_ff @(posedge clk) begin : opcode_dff
        if (alloc_clk_en) begin
            opcode_q <= opcode_d;
        end
    end

    always_ff @(posedge clk) begin : io_region_dff
        if (rst) begin
            io_region_q <= '0;
        end
        else if (addr_issue_clk_en) begin
            io_region_q <= io_region_d;
        end
    end

    always_ff @(posedge clk) begin : prd_dff
        if (addr_issue_clk_en) begin
            prd_q <= prd_d;
        end
    end

    always_ff @(posedge clk) begin : data_dff
        if (data_issue_clk_en) begin
            data_q <= data_d;
        end
    end

endmodule : rvh_stq_entry
