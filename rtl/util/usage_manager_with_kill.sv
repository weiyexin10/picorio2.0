`ifndef __USAGE_MANAGER_WITH_KILL_SV__
`define __USAGE_MANAGER_WITH_KILL_SV__

module usage_manager_with_kill #(
    parameter int unsigned ENTRY_COUNT = 8,
    parameter int unsigned ENQ_WIDTH = 2,
    parameter int unsigned DEQ_WIDTH = 2,
    parameter int unsigned INIT_IS_FULL = 0,
    parameter int unsigned FLUSH_KILL_DISABLE_ENQ = 0,
    parameter int unsigned COMB_TAIL_WHEN_FLUSH_KILL = 0,
    localparam int unsigned ENTRY_PTR_WIDTH = $clog2(ENTRY_COUNT),
    localparam int unsigned ENTRY_TAG_WIDTH = ENTRY_PTR_WIDTH,
    localparam int unsigned USAGE_CNT_WIDTH = $clog2(ENTRY_COUNT + 1)
) (
    // Enqueue
    input logic [ENQ_WIDTH-1:0]         enq_fire_i,
    // Dequeue
    input logic [DEQ_WIDTH-1:0]         deq_fire_i,
    // kill after
    input logic                         kill_after_valid_i,
    input logic [ENTRY_PTR_WIDTH-1:0]   kill_after_tag_i,

    // Status
    output logic [DEQ_WIDTH-1:0][ENTRY_TAG_WIDTH-1:0] head_o,
    output logic [ENQ_WIDTH-1:0][ENTRY_TAG_WIDTH-1:0] tail_o,
    output logic [ENQ_WIDTH-1:0][ENTRY_TAG_WIDTH-1:0] tail_ff_o,
    output logic [USAGE_CNT_WIDTH-1:0] avail_cnt_o,

    input logic flush_i,

    input clk,
    input rst
);
    // Local Param
    localparam int unsigned ENQ_CNT_WIDTH = $clog2(ENQ_WIDTH + 1);
    localparam int unsigned DEQ_CNT_WIDTH = $clog2(DEQ_WIDTH + 1);
    localparam int unsigned IS_2N = (2 ** ENTRY_PTR_WIDTH == ENTRY_COUNT);
    
    // Function 
    function automatic [ENTRY_TAG_WIDTH-1:0] head_ptr_plus;
        input logic [ENTRY_TAG_WIDTH-1:0] head_ptr_i;
        input logic [DEQ_CNT_WIDTH-1:0] plus_cnt_i;

        logic flag;
        logic [ENTRY_PTR_WIDTH-1:0] index;
        logic [ENTRY_PTR_WIDTH:0] sum;
        logic [ENTRY_PTR_WIDTH:0] reverse_sum;
        begin
            if (IS_2N) begin
                head_ptr_plus = head_ptr_i + plus_cnt_i;
            end else begin
                index = head_ptr_i[ENTRY_PTR_WIDTH-1:0];
                flag = head_ptr_i[ENTRY_TAG_WIDTH-1];
                sum = index + plus_cnt_i;
                reverse_sum = sum - ENTRY_COUNT;
                if (~reverse_sum[ENTRY_PTR_WIDTH]) begin
                    head_ptr_plus = reverse_sum[ENTRY_PTR_WIDTH-1:0];
                end else begin
                    head_ptr_plus = sum[ENTRY_PTR_WIDTH-1:0];
                end
            end
        end
    endfunction : head_ptr_plus


    function automatic [ENTRY_TAG_WIDTH-1:0] ptr_plus_one;
        input logic [ENTRY_TAG_WIDTH-1:0] ptr_i;

        logic flag;
        logic [ENTRY_PTR_WIDTH-1:0] index;
        logic reverse_flag;
        begin
            if (IS_2N) begin
                ptr_plus_one = ptr_i + 1'b1;
            end else begin
                index = ptr_i[ENTRY_PTR_WIDTH-1:0];
                flag  = ptr_i[ENTRY_TAG_WIDTH-1];
                if (index == ENTRY_COUNT - 1) begin
                    index = {ENTRY_PTR_WIDTH{1'b0}};
                    reverse_flag = ~flag;
                end else begin
                    index = ptr_i + 1'b1;
                    reverse_flag = flag;
                end
                ptr_plus_one = index;
            end
        end
    endfunction : ptr_plus_one

    function automatic [ENTRY_TAG_WIDTH-1:0] tail_ptr_plus;
        input logic [ENTRY_TAG_WIDTH-1:0] tail_ptr_i;
        input logic [ENQ_CNT_WIDTH-1:0] plus_cnt_i;
        logic flag;
        logic [ENTRY_PTR_WIDTH-1:0] index;
        logic [ENTRY_PTR_WIDTH:0] sum;
        logic [ENTRY_PTR_WIDTH:0] reverse_sum;
        begin
            if (IS_2N) begin
                tail_ptr_plus = tail_ptr_i + plus_cnt_i;
            end else begin
                index = tail_ptr_i[ENTRY_PTR_WIDTH-1:0];
                flag = tail_ptr_i[ENTRY_TAG_WIDTH-1];
                sum = index + plus_cnt_i;
                reverse_sum = sum - ENTRY_COUNT;
                if (~reverse_sum[ENTRY_PTR_WIDTH]) begin
                    tail_ptr_plus = reverse_sum[ENTRY_PTR_WIDTH-1:0];
                end else begin
                    tail_ptr_plus = sum[ENTRY_PTR_WIDTH-1:0];
                end
            end
        end
    endfunction : tail_ptr_plus


    // Clock Gate
    logic enq_clk_en, deq_clk_en;


    logic [ENQ_CNT_WIDTH-1:0] enq_cnt;
    logic [DEQ_CNT_WIDTH-1:0] deq_cnt;
    logic [USAGE_CNT_WIDTH-1:0] killed_cnt;
    logic [ENTRY_TAG_WIDTH-1:0] head_ptr_d, head_ptr_q;
    logic [ENTRY_TAG_WIDTH-1:0] tail_ptr_d, tail_ptr_q;
    logic [USAGE_CNT_WIDTH-1:0] avail_cnt_d, avail_cnt_q;


    assign enq_clk_en = |enq_fire_i;
    assign deq_clk_en = |deq_fire_i;

    // Output 
    always_comb begin : gen_head
        for (int i = 0; i < DEQ_WIDTH; i++) begin
            if (i == 0) begin
                head_o[i] = head_ptr_q;
            end else begin
                head_o[i] =  ptr_plus_one(head_o[i-1]);
            end
        end
    end

    always_comb begin : gen_tail
        for (int i = 0; i < ENQ_WIDTH; i++) begin
            if (COMB_TAIL_WHEN_FLUSH_KILL) begin
                if (i == 0) begin
                    if (flush_i) begin
                        tail_o[i] = head_ptr_q;
                    end
                    else if (kill_after_valid_i) begin
                        tail_o[i] = tail_ptr_plus(kill_after_tag_i, 1'b1);
                    end
                    else begin
                        tail_o[i] = tail_ptr_q;
                    end
                end 
                else begin
                    tail_o[i] = ptr_plus_one(tail_o[i-1]);
                end
            end
            else begin
                if (i == 0) begin
                    tail_o[i] = tail_ptr_q;
                end else begin
                    tail_o[i] = ptr_plus_one(tail_o[i-1]);
                end
            end
        end
    end

    always_comb begin
        for (int i = 0; i < ENQ_WIDTH; i++) begin
            if (i == 0) begin
                tail_ff_o[i] = tail_ptr_q;
            end else begin
                tail_ff_o[i] = ptr_plus_one(tail_ff_o[i-1]);
            end
        end
    end
    assign avail_cnt_o = avail_cnt_q;

    always_comb begin : head_ptr_update
        head_ptr_d = head_ptr_q;
        if (deq_clk_en) begin
            head_ptr_d = head_ptr_plus(head_ptr_q, deq_cnt);
        end
        if (flush_i) begin
            head_ptr_d = head_ptr_q;
        end
    end

    always_comb begin : tail_ptr_update
        tail_ptr_d = tail_ptr_q;
        if (enq_clk_en & flush_i) begin
            if (FLUSH_KILL_DISABLE_ENQ) begin
                tail_ptr_d = head_ptr_q;
            end
            else begin
                tail_ptr_d = tail_ptr_plus(head_ptr_q, enq_cnt);
            end
        end
        else if (flush_i) begin
            tail_ptr_d = head_ptr_q;
        end
        else if (enq_clk_en & kill_after_valid_i) begin
            if (FLUSH_KILL_DISABLE_ENQ) begin
                tail_ptr_d = tail_ptr_plus(kill_after_tag_i, 1'b1);
            end
            else begin
                tail_ptr_d = tail_ptr_plus(tail_ptr_plus(kill_after_tag_i, 1'b1), enq_cnt);
            end
        end
        else if (enq_clk_en) begin
            tail_ptr_d = tail_ptr_plus(tail_ptr_q, enq_cnt);
        end
        else if (kill_after_valid_i) begin
            tail_ptr_d = tail_ptr_plus(kill_after_tag_i, 1'b1);
        end
    end

    always_comb begin : avail_cnt_update
        avail_cnt_d = avail_cnt_q;
        if (enq_clk_en & flush_i) begin
            if (FLUSH_KILL_DISABLE_ENQ) begin
                if (INIT_IS_FULL) begin
                    avail_cnt_d = {USAGE_CNT_WIDTH{1'b0}};
                end
                else begin
                    avail_cnt_d = ENTRY_COUNT[USAGE_CNT_WIDTH-1:0];
                end
            end
            else begin
                if (~INIT_IS_FULL) begin
                    avail_cnt_d = ENTRY_COUNT[USAGE_CNT_WIDTH-1:0] - enq_cnt;
                end 
                // else begin
                //    // should never reach here, we have assertion
                // end
            end
        end
        else if (flush_i) begin
            if (INIT_IS_FULL) begin
                avail_cnt_d = {USAGE_CNT_WIDTH{1'b0}};
            end else begin
                avail_cnt_d = ENTRY_COUNT[USAGE_CNT_WIDTH-1:0];
            end
        end
        else if (enq_clk_en & deq_clk_en & kill_after_valid_i) begin
            if (FLUSH_KILL_DISABLE_ENQ) begin
                avail_cnt_d = avail_cnt_q + deq_cnt + killed_cnt;
            end
            else begin
                avail_cnt_d = avail_cnt_q + deq_cnt - enq_cnt + killed_cnt;
            end
        end
        else if (enq_clk_en & deq_clk_en) begin
            avail_cnt_d = avail_cnt_q + deq_cnt - enq_cnt;
        end
        else if (enq_clk_en & kill_after_valid_i) begin
            if (FLUSH_KILL_DISABLE_ENQ) begin
                avail_cnt_d = avail_cnt_q + killed_cnt;
            end
            else begin
                avail_cnt_d = avail_cnt_q + killed_cnt - enq_cnt;
            end
        end
        else if (deq_clk_en & kill_after_valid_i) begin
            avail_cnt_d = avail_cnt_q + killed_cnt + deq_cnt;
        end
        else if (enq_clk_en) begin
            avail_cnt_d = avail_cnt_q - enq_cnt;
        end
        else if (deq_clk_en) begin
            avail_cnt_d = avail_cnt_q + deq_cnt;
        end
        else if (kill_after_valid_i) begin
            avail_cnt_d = avail_cnt_q + killed_cnt;
        end
    end

    always_ff @(posedge clk) begin : head_ptr_dff
        if (rst) begin
            head_ptr_q <= {ENTRY_TAG_WIDTH{1'b0}};
        end else begin
            if (deq_clk_en | flush_i) begin
                head_ptr_q <= head_ptr_d;
            end
        end
    end

    always_ff @(posedge clk) begin : tail_ptr_dff
        if (rst) begin
            tail_ptr_q <= {ENTRY_TAG_WIDTH{1'b0}};
        end else begin
            if (enq_clk_en | flush_i | kill_after_valid_i) begin
                tail_ptr_q <= tail_ptr_d;
            end
        end
    end

    always_ff @(posedge clk) begin : avail_cnt_dff
        if (rst) begin
            if (INIT_IS_FULL) begin
                avail_cnt_q <= {USAGE_CNT_WIDTH{1'b0}};
            end else begin
                avail_cnt_q <= ENTRY_COUNT[USAGE_CNT_WIDTH-1:0];
            end
        end else begin
            if (enq_clk_en | deq_clk_en | flush_i | kill_after_valid_i) begin
                avail_cnt_q <= avail_cnt_d;
            end
        end
    end

    one_counter #(
        .DATA_WIDTH(ENQ_WIDTH)
    ) u_enq_one_counter (
        .data_i(enq_fire_i),
        .cnt_o (enq_cnt)
    );

    one_counter #(
        .DATA_WIDTH(DEQ_WIDTH)
    ) u_deq_one_counter (
        .data_i(deq_fire_i),
        .cnt_o (deq_cnt)
    );

    // calculate how many entries will be kill
    always_comb begin
        killed_cnt = 0;
        if (kill_after_valid_i) begin
            if (head_ptr_q == tail_ptr_q) begin
                if (avail_cnt_q == ENTRY_COUNT) begin
                    killed_cnt = 0;
                end
                else if (kill_after_tag_i >= head_ptr_q) begin
                    killed_cnt = ENTRY_COUNT - kill_after_tag_i - 1'b1 + tail_ptr_q;
                end
                else begin
                    killed_cnt = tail_ptr_q - kill_after_tag_i - 1'b1;
                end
            end
            else if (head_ptr_q < tail_ptr_q) begin
                killed_cnt = tail_ptr_q - kill_after_tag_i - 1'b1;
            end
            else begin
                if (kill_after_tag_i >= head_ptr_q) begin
                    killed_cnt = ENTRY_COUNT - kill_after_tag_i - 1'b1 + tail_ptr_q;
                end
                else begin
                    killed_cnt = tail_ptr_q - kill_after_tag_i - 1'b1;
                end
            end
        end
    end

// assert
`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (kill_after_valid_i) begin
            if (head_ptr_q == tail_ptr_q) begin
                if (avail_cnt_q == ENTRY_COUNT) begin
                    if (kill_after_tag_i != head_ptr_q) $fatal("kill invalid tag when head_ptr == tail_ptr");
                end
                else begin
                    if (avail_cnt_q != 0) $fatal("Pointer value does not correspond to available count when queue full");
                end
            end
            else if (head_ptr_q < tail_ptr_q) begin
                if (~((kill_after_tag_i >= head_ptr_q) & (kill_after_tag_i < tail_ptr_q))) $fatal("kill invalid tag when head_ptr < tail_ptr");
            end
            else begin
                if ((kill_after_tag_i >= tail_ptr_q) & (kill_after_tag_i < head_ptr_q)) $fatal("kill invalid tag when head_ptr > tail_ptr");
            end
            if (killed_cnt > (ENTRY_COUNT - avail_cnt_q)) $fatal("Attempt to kill more entries than the total number of entries");
        end
        if (~FLUSH_KILL_DISABLE_ENQ & INIT_IS_FULL) begin
            $fatal("Illegal config! FLUSH_KILL_DISABLE_ENQ must be 1 when INIT_IS_FULL is 1");
        end
    end
`endif
endmodule

`endif