module DCACHE_TAG_RAM
(
  input   logic                     CLK,
  input   logic                     CEB,
  input   logic                     WEB,
  input   logic [2:0]               A,
  input   logic [27:0]             D,
  input   logic                    WE,
  output  logic [27:0]             Q
);

IN22FDX_R1PU_WFUG_W00008B028M02C256 u_sram
    (
`ifdef IVCS_PG
     .VDD           ( ),
     .VBN           ( ),
     .VBP           ( ),
     .VCS           ( ),
     .VSS           ( ),
`endif

     .CLK           (  CLK  ),
     .CEN           (  CEB  ),
     .RDWEN         (  ((~CEB) ? WEB : '1)),
     .DEEPSLEEP     (  '0   ),
     .POWERGATE     (  '0   ),
     .AW            (A[2:1]),
     .AC            (A[0]),
     .D             (D),
     .BW            ({$bits(Q){WE}}),
     .T_LOGIC       ( '0),
     .MA_SAWL       ( '0),
     .MA_WL('0),
     .MA_WRAS('0),
     .MA_WRASD('0),
     .RWE('0),
     .RWFA('0),
     .Q(Q),
     .OBSV_CTL ()

     );

endmodule
