`ifndef __CYCLIC_SHIFTER__
`define __CYCLIC_SHIFTER__

module cyclic_shifter
#(
    parameter int unsigned LENGTH = 8,
    parameter int unsigned SHIFT_LEFT = 1
)
(
    // Enqueue
    input logic[LENGTH-1:0]             data_i,
    input logic[$clog2(LENGTH)-1:0]     shift_bits_i,
    output logic[LENGTH-1:0]            data_o    
);

    logic[LENGTH-1:0][LENGTH-1:0]             data_mat;
    assign data_mat[0] = data_i;

    // if (SHIFT_LEFT == 0) begin
    //     generate for (i = 1; i < LENGTH; i++) begin
    //         assign data_mat[i] = {data_i[LENGTH-i-1:i], data_i[LENGTH-1:LENGTH-i]};
    //     end
    //     endgenerate
    // end
    // else begin
    //     generate for (i = 1; i < LENGTH; i++) begin
    //         assign data_mat[i] = {data_i[i-1:0], data_i[LENGTH-1:i]};
    //     end
    //     endgenerate
    // end

    

    generate for (genvar i = 1; i < LENGTH; i++) begin
            if (SHIFT_LEFT == 0) begin
                assign data_mat[i] = {data_i[LENGTH-i-1:i], data_i[LENGTH-1:LENGTH-i]};
            end
            else begin
                assign data_mat[i] = {data_i[i-1:0], data_i[LENGTH-1:i]};
            end
        end
    endgenerate

    assign data_o = data_mat[shift_bits_i];
    
endmodule : cyclic_shifter

`endif