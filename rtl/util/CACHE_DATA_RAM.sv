module CACHE_DATA_RAM
(
  input   logic                     CLK,
  input   logic                     CEB,
  input   logic                     WEB,
  input   logic [4:0]               A,
  input   logic [127:0]             D,
  input   logic [128/8-1:0]   BYTE_WE,
  output  logic [127:0]             Q
);

logic [$bits(Q)-1:0]             bit_we;

always_comb begin
     for (int i = 0; i < $bits(Q); i++) begin
          if (BYTE_WE[i / 8]) begin
               bit_we[i] = 1;
          end
          else begin
               bit_we[i] = 0;
          end
     end
end


IN22FDX_R1PU_WFUG_W00032B128M02C256 u_sram
    (
`ifdef IVCS_PG
     .VDD           ( ),
     .VBN           ( ),
     .VBP           ( ),
     .VCS           ( ),
     .VSS           ( ),
`endif

     .CLK           (  CLK  ),
     .CEN           (  CEB  ),
     .RDWEN         (  ((~CEB) ? WEB : '1)),
     .DEEPSLEEP     (  '0   ),
     .POWERGATE     (  '0   ),
     .AW(A[4:1]),
     .AC(A[0]),
     .D(D),
     .BW(bit_we),
     .T_LOGIC('0),
     .MA_SAWL       ( '0),
     .MA_WL('0),
     .MA_WRAS('0),
     .MA_WRASD('0),
     .RWE('0),
     .RWFA('0),
     .Q(Q),
     .OBSV_CTL ()

     );

endmodule
