module rvh_backend
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_l1d_pkg::*;
(
    input  logic [            XLEN-1:0]                        hart_id_i,
    input  logic [     VADDR_WIDTH-1:0]                        boot_address_i,
    // CSR Status
    output logic [  PRIV_LVL_WIDTH-1:0]                        priv_lvl_o,
    output logic                                               tvm_o,
    output logic                                               tw_o,
    output logic                                               tsr_o,
    output logic [            XLEN-1:0]                        mstatus_o,
    output logic [            XLEN-1:0]                        satp_o,
    // Decoder -> Rename
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_vld_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_is_rvc_i,
    input  logic [    RENAME_WIDTH-1:0][      VADDR_WIDTH-1:0] dec_rn_inst_pc_i,
    input  logic [    RENAME_WIDTH-1:0][    BTQ_TAG_WIDTH-1:0] dec_rn_inst_btq_tag_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_pred_taken_i,
    input  logic [    RENAME_WIDTH-1:0][      VADDR_WIDTH-1:0] dec_rn_inst_pred_target_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_excp_vld_i,
    input  logic [    RENAME_WIDTH-1:0][   MAJOR_OP_WIDTH-1:0] dec_rn_inst_major_op_i,
    input  logic [    RENAME_WIDTH-1:0][   MINOR_OP_WIDTH-1:0] dec_rn_inst_minor_op_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_use_rs1_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_use_rs2_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_use_rd_i,
    input  logic [    RENAME_WIDTH-1:0][ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rs1_i,
    input  logic [    RENAME_WIDTH-1:0][ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rs2_i,
    input  logic [    RENAME_WIDTH-1:0][ISA_REG_TAG_WIDTH-1:0] dec_rn_inst_rd_i,
    input  logic [    RENAME_WIDTH-1:0][                 31:0] dec_rn_inst_imm_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_use_imm_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_alu_use_pc_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_is_fence_i,
    input  logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_control_flow_i,
    // Exception
    input  logic [ EXCP_TVAL_WIDTH-1:0]                        dec_rn_inst_excp_tval_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                        dec_rn_inst_excp_cause_i,
    output logic [    RENAME_WIDTH-1:0]                        dec_rn_inst_rdy_o,
    // Bru Update
    output logic [       BRU_COUNT-1:0]                        bru_resolve_vld_o,
    output logic [       BRU_COUNT-1:0][      VADDR_WIDTH-1:0] bru_resolve_pc_o,
    output logic [       BRU_COUNT-1:0]                        bru_resolve_taken_o,
    output logic [       BRU_COUNT-1:0]                        bru_resolve_mispred_o,
    output logic [       BRU_COUNT-1:0][    BTQ_TAG_WIDTH-1:0] bru_resolve_btq_tag_o,
    output logic [       BRU_COUNT-1:0][      VADDR_WIDTH-1:0] bru_resolve_target_o,
    // Redirect
    output logic                                               redirect_vld_o,
    output logic [     VADDR_WIDTH-1:0]                        redirect_target_o,
    output logic [     VADDR_WIDTH-1:0]                        redirect_pc_o,
    output logic [   BTQ_TAG_WIDTH-1:0]                        redirect_btq_tag_o,
    output logic                                               redirect_is_taken_o,
    // Redirect Port
    output logic                                               csr_redirect_vld_o,
    output logic [     VADDR_WIDTH-1:0]                        csr_redirect_pc_o,
    output logic [     VADDR_WIDTH-1:0]                        csr_redirect_target_o,
    output logic [   BTQ_TAG_WIDTH-1:0]                        csr_redirect_btq_tag_o,
    // Commit
    output logic [    RETIRE_WIDTH-1:0]                        retire_insn_vld_o,
    output logic [    RETIRE_WIDTH-1:0][      VADDR_WIDTH-1:0] retire_insn_pc_o,
    output logic [    RETIRE_WIDTH-1:0]                        retire_insn_is_br_o,
    output logic [    RETIRE_WIDTH-1:0]                        retire_insn_mispred_o,
    output logic [    RETIRE_WIDTH-1:0][    BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag_o,

    // L1 TLB -> STLB : Look up Request
    output logic l1_tlb_stlb_req_vld_o,
    output logic [DTLB_TRANS_ID_WIDTH-1:0] l1_tlb_stlb_req_trans_id_o,
    output logic [ASID_WIDTH-1:0] l1_tlb_stlb_req_asid_o,
    output logic [PMA_ACCESS_WIDTH-1:0] l1_tlb_stlb_req_access_type_o,
    output logic [VPN_WIDTH-1:0] l1_tlb_stlb_req_vpn_o,
    input logic l1_tlb_stlb_req_rdy_i,

    // STLB -> L1 TLB : Look up Response
    input logic l1_tlb_stlb_resp_vld_i,
    input logic [DTLB_TRANS_ID_WIDTH-1:0] l1_tlb_stlb_resp_trans_id_i,
    input logic [ASID_WIDTH-1:0] l1_tlb_stlb_resp_asid_i,
    input logic [PTE_LVL_WIDTH-1:0] l1_tlb_stlb_resp_pte_lvl_i,
    input logic [XLEN-1:0] l1_tlb_stlb_resp_pte_i,
    input logic [PMA_ACCESS_WIDTH-1:0] l1_tlb_stlb_resp_access_type_i,
    input logic [VPN_WIDTH-1:0] l1_tlb_stlb_resp_vpn_i,
    input logic l1_tlb_stlb_resp_access_fault_i,
    input logic l1_tlb_stlb_resp_page_fault_i,
    output logic l1_tlb_stlb_resp_rdy_o,

    // DTLB Evict
    output dtlb_evict_vld_o,
    output [PTE_WIDTH-1:0] dtlb_evict_pte_o,
    output [PAGE_LVL_WIDTH-1:0] dtlb_evict_page_lvl_o,
    output [VPN_WIDTH-1:0] dtlb_evict_vpn_o,
    output [ASID_WIDTH-1:0] dtlb_evict_asid_o,

    // AR
    output logic             l1d_l2_req_arvalid_o,
    input  logic             l1d_l2_req_arready_i,
    output cache_mem_if_ar_t l1d_l2_req_ar_o,
    // ewrq -> mem bus
    // AW 
    output logic             l1d_l2_req_awvalid_o,
    input  logic             l1d_l2_req_awready_i,
    output cache_mem_if_aw_t l1d_l2_req_aw_o,
    // W 
    output logic             l1d_l2_req_wvalid_o,
    input  logic             l1d_l2_req_wready_i,
    output cache_mem_if_w_t  l1d_l2_req_w_o,
    // L1D -> L2 : Response
    // B
    input  logic             l2_l1d_resp_bvalid_i,
    output logic             l2_l1d_resp_bready_o,
    input  cache_mem_if_b_t  l2_l1d_resp_b_i,
    // mem bus -> mlfb
    // R
    input  logic             l2_l1d_resp_rvalid_i,
    output logic             l2_l1d_resp_rready_o,
    input  cache_mem_if_r_t  l2_l1d_resp_r_i,

    // Interrupt
    input logic interrupt_sw_i,
    input logic interrupt_timer_i,
    input logic interrupt_ext_i,

    // R/W pmp config
    output logic                          pmp_cfg_vld_o,
    output logic [ PMP_CFG_TAG_WIDTH-1:0] pmp_cfg_tag_o,
    output logic [              XLEN-1:0] pmp_cfg_wr_data_o,
    input  logic [              XLEN-1:0] pmp_cfg_rd_data_i,
    // R/W pmp addr
    output logic                          pmp_addr_vld_o,
    output logic [PMP_ADDR_TAG_WIDTH-1:0] pmp_addr_tag_o,
    output logic [              XLEN-1:0] pmp_addr_wr_data_o,
    input  logic [              XLEN-1:0] pmp_addr_rd_data_i,
    // ptw walk request port
    input                                 ptw_walk_req_vld_i,
    input        [      PTW_ID_WIDTH-1:0] ptw_walk_req_id_i,
    input        [       PADDR_WIDTH-1:0] ptw_walk_req_addr_i,
    output                                ptw_walk_req_rdy_o,
    // ptw walk response port
    output                                ptw_walk_resp_vld_o,
    output       [      PTW_ID_WIDTH-1:0] ptw_walk_resp_id_o,
    output       [         PTE_WIDTH-1:0] ptw_walk_resp_pte_o,
    input                                 ptw_walk_resp_rdy_i,

    output logic interrupt_ack_o,
    output logic [EXCP_CAUSE_WIDTH-1:0] interrupt_cause_o,
    // Execute FENCE.I : Flush Icache
    output logic flush_icache_req_o,
    input logic flush_icache_gnt_i,
    // Execute SFENCE.VMA : Flush ITLB
    output logic flush_itlb_req_o,
    output logic flush_itlb_use_vpn_o,
    output logic flush_itlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_itlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_itlb_asid_o,
    input logic flush_itlb_gnt_i,
    // Execute SFENCE.VMA : Flush STLB
    output logic flush_stlb_req_o,
    output logic flush_stlb_use_vpn_o,
    output logic flush_stlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_stlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_stlb_asid_o,
    input logic flush_stlb_gnt_i,

    input clk,
    input rst
);
  logic                                                rob_flush_be_vld;

  // Rename -> INT PRF(Allocate)
  logic [    RENAME_WIDTH-1:0]                         rn_int_prf_alloc_vld;
  logic [    RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_prf_alloc_preg;

  // Rename -> BRQ(Allocate)
  logic [    RENAME_WIDTH-1:0]                         rn_brq_alloc_inst_vld;
  logic [    RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_brq_alloc_inst_rob_tag;
  logic [    RENAME_WIDTH-1:0]                         rn_brq_alloc_inst_pred_taken;
  logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_brq_alloc_inst_pred_target;
  logic [    RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_brq_alloc_inst_brq_tag;
  logic                                                rn_brq_alloc_inst_rdy;
  // Rename -> RCU(Allocate)
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_vld;
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_excp;
  logic [ EXCP_TVAL_WIDTH-1:0]                         rn_rcu_alloc_inst_excp_tval;
  logic [EXCP_CAUSE_WIDTH-1:0]                         rn_rcu_alloc_inst_excp_cause;
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_control_flow;
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_is_rvc;
  logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_rcu_alloc_inst_pc;
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_is_br;
  logic [    RENAME_WIDTH-1:0][     BTQ_TAG_WIDTH-1:0] rn_rcu_alloc_inst_btq_tag;
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_pred_taken;
  logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_rcu_alloc_inst_pred_target;
  logic [    RENAME_WIDTH-1:0]                         rn_rcu_alloc_inst_use_rd;
  logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rd;
  logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rs1;
  logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rs2;
  logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rs1;
  logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rs2;
  logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rd;
  logic [    RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_rcu_alloc_inst_rob_tag;
  logic [    RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_rcu_alloc_inst_brq_tag;
  logic                                                rn_rcu_alloc_rcu_rdy;
  // Rename -> Dispatch Buffer(INT)
  logic [    RENAME_WIDTH-1:0]                         rn_int_disp_vld;
`ifdef SIMULATION
  logic [RENAME_WIDTH-1:0][VADDR_WIDTH-1:0] rn_int_disp_pc;
`endif
  logic [RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_int_disp_rob_tag;
  logic [RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_int_disp_brq_tag;
  logic [RENAME_WIDTH-1:0][    INT_TYPE_WIDTH-1:0] rn_int_disp_major_op;
  logic [RENAME_WIDTH-1:0][      INT_OP_WIDTH-1:0] rn_int_disp_minor_op;
  logic [RENAME_WIDTH-1:0]                         rn_int_disp_use_rs1;
  logic [RENAME_WIDTH-1:0]                         rn_int_disp_use_rs2;
  logic [RENAME_WIDTH-1:0]                         rn_int_disp_use_rd;
  logic [RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs1;
  logic [RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs2;
  logic [RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rd;
  logic [RENAME_WIDTH-1:0][                  31:0] rn_int_disp_imm;
  logic [RENAME_WIDTH-1:0]                         rn_int_disp_use_imm;
  logic [RENAME_WIDTH-1:0]                         rn_int_disp_alu_use_pc;
  logic                                            rn_int_disp_rdy;
  // Rename -> Dispatch Buffer(LS)
  logic [RENAME_WIDTH-1:0]                         rn_ls_disp_vld;
`ifdef SIMULATION
  logic [RENAME_WIDTH-1:0][VADDR_WIDTH-1:0] rn_ls_disp_pc;
`endif
  logic [      RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_ls_disp_rob_tag;
  logic [      RENAME_WIDTH-1:0][    LSU_TYPE_WIDTH-1:0] rn_ls_disp_major_op;
  logic [      RENAME_WIDTH-1:0][      LSU_OP_WIDTH-1:0] rn_ls_disp_minor_op;
  logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs1;
  logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs2;
  logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rd;
  logic [      RENAME_WIDTH-1:0][                  11:0] rn_ls_disp_imm;
  logic [      RENAME_WIDTH-1:0]                         rn_ls_disp_is_fence;
  logic                                                  rn_ls_disp_rdy;
  // Rename -> CSR Issue Register
  logic                                                  rn_csr_ir_disp_vld;
  logic [       VADDR_WIDTH-1:0]                         rn_csr_ir_disp_pc;
  logic [     ROB_TAG_WIDTH-1:0]                         rn_csr_ir_disp_rob_tag;
  logic [      CSR_OP_WIDTH-1:0]                         rn_csr_ir_disp_minor_op;
  logic                                                  rn_csr_ir_disp_use_imm;
  logic [                   4:0]                         rn_csr_ir_disp_imm;
  logic                                                  rn_csr_ir_disp_use_rs1;
  logic [INT_PREG_TAG_WIDTH-1:0]                         rn_csr_ir_disp_phy_rs1;
  logic                                                  rn_csr_ir_disp_use_rs2;
  logic [INT_PREG_TAG_WIDTH-1:0]                         rn_csr_ir_disp_phy_rs2;
  logic [INT_PREG_TAG_WIDTH-1:0]                         rn_csr_ir_disp_phy_rd;
  logic [                  11:0]                         rn_csr_ir_disp_csr_addr;

  // LS Router -> AGU IQ0
  logic                                                  disp_agu0_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_agu0_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_agu0_iq_rob_tag;
  logic [     LSQ_TAG_WIDTH-1:0] disp_agu0_iq_lsq_tag;
  logic [    LSU_TYPE_WIDTH-1:0] disp_agu0_iq_fu_type;
  logic [      LSU_OP_WIDTH-1:0] disp_agu0_iq_opcode;
  logic                          disp_agu0_iq_rs1_rdy;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu0_iq_phy_rs1;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu0_iq_phy_rd;
  logic [                  11:0] disp_agu0_iq_imm;
  logic                          disp_agu0_iq_rdy;
  // LS Router -> SDU IQ0
  logic                          disp_sdu0_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_sdu0_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_sdu0_iq_rob_tag;
  logic [     STQ_TAG_WIDTH-1:0] disp_sdu0_iq_stq_tag;
  logic                          disp_sdu0_iq_rs2_rdy;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_sdu0_iq_phy_rs2;
  logic                          disp_sdu0_iq_rdy;
  // LS Router -> AGU IQ1
  logic                          disp_agu1_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_agu1_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_agu1_iq_rob_tag;
  logic [     LSQ_TAG_WIDTH-1:0] disp_agu1_iq_lsq_tag;
  logic [    LSU_TYPE_WIDTH-1:0] disp_agu1_iq_fu_type;
  logic [      LSU_OP_WIDTH-1:0] disp_agu1_iq_opcode;
  logic                          disp_agu1_iq_rs1_rdy;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu1_iq_phy_rs1;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu1_iq_phy_rd;
  logic [                  11:0] disp_agu1_iq_imm;
  logic                          disp_agu1_iq_rdy;
  // LS Router -> SDU IQ1
  logic                          disp_sdu1_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_sdu1_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_sdu1_iq_rob_tag;
  logic [     STQ_TAG_WIDTH-1:0] disp_sdu1_iq_stq_tag;
  logic                          disp_sdu1_iq_rs2_rdy;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_sdu1_iq_phy_rs2;
  logic                          disp_sdu1_iq_rdy;
  // Dispatch Buffer(INT) -> IQ(BR)
  logic                          disp_br_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_br_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_br_iq_rob_tag;
  logic [     BRQ_TAG_WIDTH-1:0] disp_br_iq_brq_tag;
  logic [      BRU_OP_WIDTH-1:0] disp_br_iq_opcode;
  logic                          disp_br_iq_rs1_rdy;
  logic                          disp_br_iq_rs2_rdy;
  logic                          disp_br_iq_use_rs1;
  logic                          disp_br_iq_use_rs2;
  logic                          disp_br_iq_use_rd;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_br_iq_phy_rs1;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_br_iq_phy_rs2;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_br_iq_phy_rd;
  logic [                  31:0] disp_br_iq_imm;
  logic                          disp_br_iq_rdy;
  // Dispatch Buffer(INT) -> IQ(ALU0)
  logic                          disp_alu0_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_alu0_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_alu0_iq_rob_tag;
  logic [      ALU_OP_WIDTH-1:0] disp_alu0_iq_opcode;
  logic                          disp_alu0_iq_rs1_rdy;
  logic                          disp_alu0_iq_rs2_rdy;
  logic                          disp_alu0_iq_use_rs1;
  logic                          disp_alu0_iq_use_rs2;
  logic                          disp_alu0_iq_use_rd;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_alu0_iq_phy_rs1;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_alu0_iq_phy_rs2;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_alu0_iq_phy_rd;
  logic [                  31:0] disp_alu0_iq_imm;
  logic                          disp_alu0_iq_use_imm;
  logic                          disp_alu0_iq_use_pc;
  logic                          disp_alu0_iq_rdy;
  // Dispatch Buffer(INT) -> IQ(ALU1)
  logic                          disp_alu1_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_alu1_iq_pc;
`endif
  logic [     ROB_TAG_WIDTH-1:0] disp_alu1_iq_rob_tag;
  logic [      ALU_OP_WIDTH-1:0] disp_alu1_iq_opcode;
  logic                          disp_alu1_iq_rs1_rdy;
  logic                          disp_alu1_iq_rs2_rdy;
  logic                          disp_alu1_iq_use_rs1;
  logic                          disp_alu1_iq_use_rs2;
  logic                          disp_alu1_iq_use_rd;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_alu1_iq_phy_rs1;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_alu1_iq_phy_rs2;
  logic [INT_PREG_TAG_WIDTH-1:0] disp_alu1_iq_phy_rd;
  logic [                  31:0] disp_alu1_iq_imm;
  logic                          disp_alu1_iq_use_imm;
  logic                          disp_alu1_iq_use_pc;
  logic                          disp_alu1_iq_rdy;
  // Dispatch Buffer(INT) -> IQ(Complex ALU2)  
  logic                          disp_alu2_iq_vld;
`ifdef SIMULATION
  logic [VADDR_WIDTH-1:0] disp_alu2_iq_pc;
`endif
  logic [          ROB_TAG_WIDTH-1:0]                        disp_alu2_iq_rob_tag;
  logic [                        1:0]                        disp_alu2_iq_fu_type;
  logic [           ALU_OP_WIDTH-1:0]                        disp_alu2_iq_opcode;
  logic                                                      disp_alu2_iq_rs1_rdy;
  logic                                                      disp_alu2_iq_rs2_rdy;
  logic                                                      disp_alu2_iq_use_rs1;
  logic                                                      disp_alu2_iq_use_rs2;
  logic                                                      disp_alu2_iq_use_rd;
  logic [     INT_PREG_TAG_WIDTH-1:0]                        disp_alu2_iq_phy_rs1;
  logic [     INT_PREG_TAG_WIDTH-1:0]                        disp_alu2_iq_phy_rs2;
  logic [     INT_PREG_TAG_WIDTH-1:0]                        disp_alu2_iq_phy_rd;
  logic [                       31:0]                        disp_alu2_iq_imm;
  logic                                                      disp_alu2_iq_use_imm;
  logic                                                      disp_alu2_iq_use_pc;
  logic                                                      disp_alu2_iq_rdy;
  // disp -> INT PRF
  logic [INT_PRF_DISP_PORT_COUNT-1:0][ISA_REG_TAG_WIDTH-1:0] disp_int_prf_phy_rs1;
  logic [INT_PRF_DISP_PORT_COUNT-1:0][ISA_REG_TAG_WIDTH-1:0] disp_int_prf_phy_rs2;
  logic [INT_PRF_DISP_PORT_COUNT-1:0]                        disp_int_prf_rs1_rdy;
  logic [INT_PRF_DISP_PORT_COUNT-1:0]                        disp_int_prf_rs2_rdy;
  // disp -> LSQ
  logic [         LSU_DISP_WIDTH-1:0]                        disp_lsq_alloc_vld;
  logic [         LSU_DISP_WIDTH-1:0][   LSU_TYPE_WIDTH-1:0] disp_lsq_alloc_fu_type;
  logic [         LSU_DISP_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] disp_lsq_alloc_rob_tag;
  logic [         LSU_DISP_WIDTH-1:0]                        disp_lsq_alloc_is_fence;
  logic [         LSU_DISP_WIDTH-1:0][     LSU_OP_WIDTH-1:0] disp_lsq_alloc_minor_op;
  logic [         LSU_DISP_WIDTH-1:0][    LSQ_TAG_WIDTH-1:0] disp_lsq_alloc_lsq_tag;
  logic [         LSU_DISP_WIDTH-1:0]                        disp_lsq_alloc_rdy;

  logic [              ALU_COUNT-1:0][    ROB_TAG_WIDTH-1:0] alu_issue_rob_tag;
  logic [              ALU_COUNT-1:0][      VADDR_WIDTH-1:0] alu_issue_pc;
  logic [              BRU_COUNT-1:0][    ROB_TAG_WIDTH-1:0] bru_issue_rob_tag;
  logic [              BRU_COUNT-1:0][      VADDR_WIDTH-1:0] bru_issue_pc;
  logic [              BRU_COUNT-1:0]                        bru_issue_is_rvc;
  logic [              BRU_COUNT-1:0][    BTQ_TAG_WIDTH-1:0] bru_issue_btq_tag;
  logic [              BRU_COUNT-1:0][    BRQ_TAG_WIDTH-1:0] bru_issue_brq_tag;
  logic [              BRU_COUNT-1:0]                        bru_issue_pred_taken;
  logic [              BRU_COUNT-1:0][      VADDR_WIDTH-1:0] bru_issue_pred_target;

  // Write Back : Clear Busy
  logic [           ROB_CB_WIDTH-1:0]                        wb_rob_cb_vld;
  logic [           ROB_CB_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] wb_rob_cb_tag;
  // Write Back : Report Exception
  logic [           ROB_RE_WIDTH-1:0]                        wb_rob_re_vld;
  logic [           ROB_RE_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] wb_rob_re_tag;
  logic [           ROB_RE_WIDTH-1:0][ EXCP_CAUSE_WIDTH-1:0] wb_rob_re_cause;
  logic [           ROB_RE_WIDTH-1:0][  EXCP_TVAL_WIDTH-1:0] wb_rob_re_tval;
  // Write Back : Branch Resolve
  logic [           ROB_BR_WIDTH-1:0]                        wb_rob_br_vld;
  logic [           ROB_BR_WIDTH-1:0]                        wb_rob_br_mispred;
  logic [           ROB_BR_WIDTH-1:0]                        wb_rob_br_is_taken;
  logic [           ROB_BR_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] wb_rob_br_rob_tag;
  logic [           ROB_BR_WIDTH-1:0][    BRQ_TAG_WIDTH-1:0] wb_rob_br_brq_tag;
  logic [           ROB_BR_WIDTH-1:0][      VADDR_WIDTH-1:0] wb_rob_br_target;

  logic [              BRU_COUNT-1:0]                        bru_resolve_vld;
  logic [              BRU_COUNT-1:0][      VADDR_WIDTH-1:0] bru_resolve_pc;
  logic [              BRU_COUNT-1:0]                        bru_resolve_taken;
  logic [              BRU_COUNT-1:0]                        bru_resolve_mispred;
  logic [              BRU_COUNT-1:0][    ROB_TAG_WIDTH-1:0] bru_resolve_rob_tag;
  logic [              BRU_COUNT-1:0][    BRQ_TAG_WIDTH-1:0] bru_resolve_brq_tag;
  logic [              BRU_COUNT-1:0][    BTQ_TAG_WIDTH-1:0] bru_resolve_btq_tag;
  logic [              BRU_COUNT-1:0][      VADDR_WIDTH-1:0] bru_resolve_target;

  logic                                                      csr_insn_resume;

  logic                                                      rob_empty;
  logic [          ROB_TAG_WIDTH-1:0]                        oldest_insn_rob_tag;
  logic                                                      brq_empty;
  logic [          ROB_TAG_WIDTH-1:0]                        oldest_br_rob_tag;

  logic                                                      retire_excp_vld;
  logic [            VADDR_WIDTH-1:0]                        retire_excp_pc;
  logic                                                      retire_excp_is_rvc;
  logic [        EXCP_TVAL_WIDTH-1:0]                        retire_excp_tval;
  logic [       EXCP_CAUSE_WIDTH-1:0]                        retire_excp_cause;
  logic [          BTQ_TAG_WIDTH-1:0]                        retire_excp_btq_tag;

  logic                                                      retire_excp_vld_q;
  logic [            VADDR_WIDTH-1:0]                        retire_excp_pc_q;
  logic [            VADDR_WIDTH-1:0]                        retire_excp_btq_tag_q;

  // Allocate Port
  logic [         LSU_DISP_WIDTH-1:0]                        rn_lsu_alloc_vld;
  logic [         LSU_DISP_WIDTH-1:0]                        rn_lsu_alloc_is_fence;
  logic [         LSU_DISP_WIDTH-1:0][   LSU_TYPE_WIDTH-1:0] rn_lsu_alloc_fu_type;
  logic [         LSU_DISP_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] rn_lsu_alloc_rob_tag;
  logic [         LSU_DISP_WIDTH-1:0][    LSQ_TAG_WIDTH-1:0] rn_lsu_alloc_lsq_tag;
  logic [         LSU_DISP_WIDTH-1:0]                        rn_lsu_alloc_rdy;
  // AGU -> LSU : Issue
  logic [    LSU_ADDR_PIPE_COUNT-1:0]                        agu_iq_lsu_issue_vld;
  logic [    LSU_ADDR_PIPE_COUNT-1:0]                        agu_iq_lsu_issue_excp_vld;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][    ROB_TAG_WIDTH-1:0] agu_iq_lsu_issue_rob_tag;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][    LSQ_TAG_WIDTH-1:0] agu_iq_lsu_issue_lsq_tag;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][ AGU_IQ_TAG_WIDHT-1:0] agu_iq_lsu_issue_entry_tag;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][   LSU_TYPE_WIDTH-1:0] agu_iq_lsu_issue_fu_type;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][     LSU_OP_WIDTH-1:0] agu_iq_lsu_issue_opcode;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][      VADDR_WIDTH-1:0] agu_iq_lsu_issue_vaddr;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][   PREG_TAG_WIDTH-1:0] agu_iq_lsu_issue_prd;
  logic [    LSU_ADDR_PIPE_COUNT-1:0]                        agu_iq_lsu_issue_rdy;
  // SDU -> LSU(STQ) : Issue
  logic [    LSU_DATA_PIPE_COUNT-1:0]                        sdu_iq_lsu_issue_vld;
  logic [    LSU_DATA_PIPE_COUNT-1:0][    STQ_TAG_WIDTH-1:0] sdu_iq_lsu_issue_stq_tag;
  logic [    LSU_DATA_PIPE_COUNT-1:0][             XLEN-1:0] sdu_iq_lsu_issue_data;
  // LS Pipe -> AGU IQ : Commit IQ entry
  logic [         LSU_DISP_WIDTH-1:0]                        ls_pipe_agu_iq_cmt_vld;
  logic [         LSU_DISP_WIDTH-1:0][ AGU_IQ_TAG_WIDHT-1:0] ls_pipe_agu_iq_cmt_tag;
  logic [         LSU_DISP_WIDTH-1:0]                        ls_pipe_agu_iq_cmt_dealloc;
  logic [         LSU_DISP_WIDTH-1:0]                        ls_pipe_agu_iq_cmt_reactivate;
  // LSU -> CDB : Wake Up
  logic [    LSU_ADDR_PIPE_COUNT-1:0]                        lsu_cdb_wakeup_vld;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][   PREG_TAG_WIDTH-1:0] lsu_cdb_wakeup_ptag;
  // LSU -> ROB : Clear Busy
  logic [     LSU_COMPLETE_WIDTH-1:0]                        lsu_rob_cb_vld;
  logic [     LSU_COMPLETE_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] lsu_rob_cb_tag;
  // LSU -> PRF : Write back
  logic [    LSU_ADDR_PIPE_COUNT-1:0]                        lsu_prf_wr_vld;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][   PREG_TAG_WIDTH-1:0] lsu_prf_wr_ptag;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][             XLEN-1:0] lsu_prf_wr_data;
  // LS Pipe -> ROB : Report exception
  logic [    LSU_ADDR_PIPE_COUNT-1:0]                        ls_pipe_rob_report_excp_vld;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][ EXCP_CAUSE_WIDTH-1:0] ls_pipe_rob_report_excp_cause;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][  EXCP_TVAL_WIDTH-1:0] ls_pipe_rob_report_excp_tval;
  logic [    LSU_ADDR_PIPE_COUNT-1:0][    ROB_TAG_WIDTH-1:0] ls_pipe_rob_report_excp_rob_tag;
  // INT -> ROB : Clear Busy
  logic [     INT_COMPLETE_WIDTH-1:0]                        int_rob_cb_vld;
  logic [     INT_COMPLETE_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] int_rob_cb_tag;
  // CSR -> ROB : Clear Busy
  logic                                                      csr_rob_cb_vld;
  logic [          ROB_TAG_WIDTH-1:0]                        csr_rob_cb_tag;
  // CSR -> ROB : Report exception
  logic                                                      csr_rob_re_vld;
  logic [       EXCP_CAUSE_WIDTH-1:0]                        csr_rob_re_cause;
  logic [        EXCP_TVAL_WIDTH-1:0]                        csr_rob_re_tval;
  logic [          ROB_TAG_WIDTH-1:0]                        csr_rob_re_rob_tag;
  // Execute FENCE.I : Flush Dcache
  logic                                                      flush_dcache_req;
  logic                                                      flush_dcache_gnt;
  // Execute SFENCE.VMA : Flush DTLB
  logic                                                      flush_dtlb_req;
  logic                                                      flush_dtlb_use_vpn;
  logic                                                      flush_dtlb_use_asid;
  logic [              VPN_WIDTH-1:0]                        flush_dtlb_vpn;
  logic [              ASID_WIDTH-1:0]                       flush_dtlb_asid;
  logic                                                      flush_dtlb_gnt;
  // Commit 
  logic [    RETIRE_WIDTH-1:0]                        retire_insn_vld;
  logic [    RETIRE_WIDTH-1:0][      VADDR_WIDTH-1:0] retire_insn_pc;
  logic [    RETIRE_WIDTH-1:0]                        retire_insn_is_br;
  logic [    RETIRE_WIDTH-1:0]                        retire_insn_mispred;
  logic [    RETIRE_WIDTH-1:0][    BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag;

  always_comb begin
      retire_insn_vld_o = retire_insn_vld;
      retire_insn_pc_o  = retire_insn_pc;
      retire_insn_is_br_o = retire_insn_is_br;
      retire_insn_mispred_o = retire_insn_mispred;
      retire_insn_btq_tag_o = retire_insn_btq_tag;
      if(retire_excp_vld_q) begin
            retire_insn_vld_o[0] = 1'b1;
            retire_insn_pc_o[0] = retire_excp_pc_q;
            retire_insn_btq_tag_o[0] = retire_excp_btq_tag_q;
            retire_insn_is_br_o[0] = 1'b0;
            retire_insn_mispred_o[0] = 1'b0;
      end
  end


  assign bru_resolve_vld_o     = bru_resolve_vld;
  assign bru_resolve_pc_o      = bru_resolve_pc;
  assign bru_resolve_taken_o   = bru_resolve_taken;
  assign bru_resolve_mispred_o = bru_resolve_mispred;
  assign bru_resolve_btq_tag_o = bru_resolve_btq_tag;
  assign bru_resolve_target_o  = bru_resolve_target;

  assign wb_rob_br_vld         = bru_resolve_vld;
  assign wb_rob_br_mispred     = bru_resolve_mispred;
  assign wb_rob_br_is_taken    = bru_resolve_taken;
  assign wb_rob_br_rob_tag     = bru_resolve_rob_tag;
  assign wb_rob_br_brq_tag     = bru_resolve_brq_tag;
  assign wb_rob_br_target      = bru_resolve_target;
  
  assign csr_redirect_btq_tag_o = retire_excp_btq_tag;

  generate
    for (genvar i = 0; i < INT_COMPLETE_WIDTH; i++) begin
      assign wb_rob_cb_vld[i] = int_rob_cb_vld[i];
      assign wb_rob_cb_tag[i] = int_rob_cb_tag[i];
    end
    for (genvar i = 0; i < LSU_COMPLETE_WIDTH; i++) begin
      if (i == 0) begin
        assign wb_rob_cb_vld[i+INT_COMPLETE_WIDTH] = csr_rob_cb_vld ? 1'b1 : lsu_rob_cb_vld[i];
        assign wb_rob_cb_tag[i+INT_COMPLETE_WIDTH] = csr_rob_cb_vld ? csr_rob_cb_tag : lsu_rob_cb_tag[i];
      end else begin
        assign wb_rob_cb_vld[i+INT_COMPLETE_WIDTH] = lsu_rob_cb_vld[i];
        assign wb_rob_cb_tag[i+INT_COMPLETE_WIDTH] = lsu_rob_cb_tag[i];
      end
    end
    for (genvar i = 0; i < ROB_RE_WIDTH; i++) begin
      if (i == 0) begin
        assign wb_rob_re_vld[i] = csr_rob_re_vld ? 1'b1 : ls_pipe_rob_report_excp_vld[i];
        assign wb_rob_re_tag[i] = csr_rob_re_vld ? csr_rob_re_rob_tag : ls_pipe_rob_report_excp_rob_tag[i];
        assign wb_rob_re_cause[i] = csr_rob_re_vld ? csr_rob_re_cause : ls_pipe_rob_report_excp_cause[i];
        assign wb_rob_re_tval[i] = csr_rob_re_vld ? csr_rob_re_tval : ls_pipe_rob_report_excp_tval[i];
      end else begin
        assign wb_rob_re_vld[i]   = ls_pipe_rob_report_excp_vld[i];
        assign wb_rob_re_tag[i]   = ls_pipe_rob_report_excp_rob_tag[i];
        assign wb_rob_re_cause[i] = ls_pipe_rob_report_excp_cause[i];
        assign wb_rob_re_tval[i]  = ls_pipe_rob_report_excp_tval[i];
      end
    end
  endgenerate

  always_ff@(posedge clk) begin
      if(rst) begin
            retire_excp_vld_q <= 1'b0;
      end else begin
            retire_excp_vld_q <= retire_excp_vld;
            if(retire_excp_vld) begin
                  retire_excp_pc_q <= retire_excp_pc;
                  retire_excp_btq_tag_q <= retire_excp_btq_tag;
            end
      end
  end

  rvh_rename u_rvh_rename (
      .dec_rn_inst_vld_i(dec_rn_inst_vld_i),
      .dec_rn_inst_is_rvc_i(dec_rn_inst_is_rvc_i),
      .dec_rn_inst_pc_i(dec_rn_inst_pc_i),
      .dec_rn_inst_btq_tag_i(dec_rn_inst_btq_tag_i),
      .dec_rn_inst_pred_taken_i(dec_rn_inst_pred_taken_i),
      .dec_rn_inst_pred_target_i(dec_rn_inst_pred_target_i),
      .dec_rn_inst_excp_vld_i(dec_rn_inst_excp_vld_i),
      .dec_rn_inst_major_op_i(dec_rn_inst_major_op_i),
      .dec_rn_inst_minor_op_i(dec_rn_inst_minor_op_i),
      .dec_rn_inst_use_rs1_i(dec_rn_inst_use_rs1_i),
      .dec_rn_inst_use_rs2_i(dec_rn_inst_use_rs2_i),
      .dec_rn_inst_use_rd_i(dec_rn_inst_use_rd_i),
      .dec_rn_inst_rs1_i(dec_rn_inst_rs1_i),
      .dec_rn_inst_rs2_i(dec_rn_inst_rs2_i),
      .dec_rn_inst_rd_i(dec_rn_inst_rd_i),
      .dec_rn_inst_imm_i(dec_rn_inst_imm_i),
      .dec_rn_inst_use_imm_i(dec_rn_inst_use_imm_i),
      .dec_rn_inst_alu_use_pc_i(dec_rn_inst_alu_use_pc_i),
      .dec_rn_inst_is_fence_i(dec_rn_inst_is_fence_i),
      .dec_rn_inst_control_flow_i(dec_rn_inst_control_flow_i),
      .dec_rn_inst_excp_tval_i(dec_rn_inst_excp_tval_i),
      .dec_rn_inst_excp_cause_i(dec_rn_inst_excp_cause_i),
      .dec_rn_inst_rdy_o(dec_rn_inst_rdy_o),
      .rn_int_prf_alloc_vld_o(rn_int_prf_alloc_vld),
      .rn_int_prf_alloc_preg_o(rn_int_prf_alloc_preg),
      .rn_rcu_alloc_inst_vld_o(rn_rcu_alloc_inst_vld),
      .rn_rcu_alloc_inst_excp_o(rn_rcu_alloc_inst_excp),
      .rn_rcu_alloc_inst_excp_tval_o(rn_rcu_alloc_inst_excp_tval),
      .rn_rcu_alloc_inst_excp_cause_o(rn_rcu_alloc_inst_excp_cause),
      .rn_rcu_alloc_inst_control_flow_o(rn_rcu_alloc_inst_control_flow),
      .rn_rcu_alloc_inst_is_rvc_o(rn_rcu_alloc_inst_is_rvc),
      .rn_rcu_alloc_inst_pc_o(rn_rcu_alloc_inst_pc),
      .rn_rcu_alloc_inst_is_br_o(rn_rcu_alloc_inst_is_br),
      .rn_rcu_alloc_inst_btq_tag_o(rn_rcu_alloc_inst_btq_tag),
      .rn_rcu_alloc_inst_pred_taken_o(rn_rcu_alloc_inst_pred_taken),
      .rn_rcu_alloc_inst_pred_target_o(rn_rcu_alloc_inst_pred_target),
      .rn_rcu_alloc_inst_use_rd_o(rn_rcu_alloc_inst_use_rd),
      .rn_rcu_alloc_inst_isa_rd_o(rn_rcu_alloc_inst_isa_rd),
      .rn_rcu_alloc_inst_isa_rs1_o(rn_rcu_alloc_inst_isa_rs1),
      .rn_rcu_alloc_inst_isa_rs2_o(rn_rcu_alloc_inst_isa_rs2),
      .rn_rcu_alloc_inst_phy_rs1_i(rn_rcu_alloc_inst_phy_rs1),
      .rn_rcu_alloc_inst_phy_rs2_i(rn_rcu_alloc_inst_phy_rs2),
      .rn_rcu_alloc_inst_phy_rd_i(rn_rcu_alloc_inst_phy_rd),
      .rn_rcu_alloc_inst_rob_tag_i(rn_rcu_alloc_inst_rob_tag),
      .rn_rcu_alloc_inst_brq_tag_i(rn_rcu_alloc_inst_brq_tag),
      .rn_rcu_alloc_rcu_rdy_i(rn_rcu_alloc_rcu_rdy),
      .rn_int_disp_vld_o(rn_int_disp_vld),
`ifdef SIMULATION
      .rn_int_disp_pc_o(rn_int_disp_pc),
`endif
      .rn_int_disp_rob_tag_o(rn_int_disp_rob_tag),
      .rn_int_disp_brq_tag_o(rn_int_disp_brq_tag),
      .rn_int_disp_major_op_o(rn_int_disp_major_op),
      .rn_int_disp_minor_op_o(rn_int_disp_minor_op),
      .rn_int_disp_use_rs1_o(rn_int_disp_use_rs1),
      .rn_int_disp_use_rs2_o(rn_int_disp_use_rs2),
      .rn_int_disp_use_rd_o(rn_int_disp_use_rd),
      .rn_int_disp_phy_rs1_o(rn_int_disp_phy_rs1),
      .rn_int_disp_phy_rs2_o(rn_int_disp_phy_rs2),
      .rn_int_disp_phy_rd_o(rn_int_disp_phy_rd),
      .rn_int_disp_imm_o(rn_int_disp_imm),
      .rn_int_disp_use_imm_o(rn_int_disp_use_imm),
      .rn_int_disp_alu_use_pc_o(rn_int_disp_alu_use_pc),
      .rn_int_disp_rdy_i(rn_int_disp_rdy),
      .rn_ls_disp_vld_o(rn_ls_disp_vld),
`ifdef SIMULATION
      .rn_ls_disp_pc_o(rn_ls_disp_pc),
`endif
      .rn_ls_disp_rob_tag_o(rn_ls_disp_rob_tag),
      .rn_ls_disp_major_op_o(rn_ls_disp_major_op),
      .rn_ls_disp_minor_op_o(rn_ls_disp_minor_op),
      .rn_ls_disp_phy_rs1_o(rn_ls_disp_phy_rs1),
      .rn_ls_disp_phy_rs2_o(rn_ls_disp_phy_rs2),
      .rn_ls_disp_phy_rd_o(rn_ls_disp_phy_rd),
      .rn_ls_disp_imm_o(rn_ls_disp_imm),
      .rn_ls_disp_is_fence_o(rn_ls_disp_is_fence),
      .rn_ls_disp_rdy_i(rn_ls_disp_rdy),
      .rn_csr_ir_disp_vld_o(rn_csr_ir_disp_vld),
      .rn_csr_ir_disp_pc_o(rn_csr_ir_disp_pc),
      .rn_csr_ir_disp_rob_tag_o(rn_csr_ir_disp_rob_tag),
      .rn_csr_ir_disp_minor_op_o(rn_csr_ir_disp_minor_op),
      .rn_csr_ir_disp_use_imm_o(rn_csr_ir_disp_use_imm),
      .rn_csr_ir_disp_imm_o(rn_csr_ir_disp_imm),
      .rn_csr_ir_disp_use_rs1_o(rn_csr_ir_disp_use_rs1),
      .rn_csr_ir_disp_phy_rs1_o(rn_csr_ir_disp_phy_rs1),
      .rn_csr_ir_disp_use_rs2_o(rn_csr_ir_disp_use_rs2),
      .rn_csr_ir_disp_phy_rs2_o(rn_csr_ir_disp_phy_rs2),
      .rn_csr_ir_disp_phy_rd_o(rn_csr_ir_disp_phy_rd),
      .rn_csr_ir_disp_csr_addr_o(rn_csr_ir_disp_csr_addr),
      .flush_i(rob_flush_be_vld),
      .clk(clk),
      .rst(rst)
  );

  rvh_rcu u_rvh_rcu (
      .rn_rcu_alloc_inst_vld_i(rn_rcu_alloc_inst_vld),
      .rn_rcu_alloc_inst_excp_i(rn_rcu_alloc_inst_excp),
      .rn_rcu_alloc_inst_excp_tval_i(rn_rcu_alloc_inst_excp_tval),
      .rn_rcu_alloc_inst_excp_cause_i(rn_rcu_alloc_inst_excp_cause),
      .rn_rcu_alloc_inst_control_flow_i(rn_rcu_alloc_inst_control_flow),
      .rn_rcu_alloc_inst_is_rvc_i(rn_rcu_alloc_inst_is_rvc),
      .rn_rcu_alloc_inst_pc_i(rn_rcu_alloc_inst_pc),
      .rn_rcu_alloc_inst_is_br_i(rn_rcu_alloc_inst_is_br),
      .rn_rcu_alloc_inst_btq_tag_i(rn_rcu_alloc_inst_btq_tag),
      .rn_rcu_alloc_inst_pred_taken_i(rn_rcu_alloc_inst_pred_taken),
      .rn_rcu_alloc_inst_pred_target_i(rn_rcu_alloc_inst_pred_target),
      .rn_rcu_alloc_inst_use_rd_i(rn_rcu_alloc_inst_use_rd),
      .rn_rcu_alloc_inst_isa_rd_i(rn_rcu_alloc_inst_isa_rd),
      .rn_rcu_alloc_inst_isa_rs1_i(rn_rcu_alloc_inst_isa_rs1),
      .rn_rcu_alloc_inst_isa_rs2_i(rn_rcu_alloc_inst_isa_rs2),
      .rn_rcu_alloc_inst_phy_rs1_o(rn_rcu_alloc_inst_phy_rs1),
      .rn_rcu_alloc_inst_phy_rs2_o(rn_rcu_alloc_inst_phy_rs2),
      .rn_rcu_alloc_inst_phy_rd_o(rn_rcu_alloc_inst_phy_rd),
      .rn_rcu_alloc_inst_rob_tag_o(rn_rcu_alloc_inst_rob_tag),
      .rn_rcu_alloc_inst_brq_tag_o(rn_rcu_alloc_inst_brq_tag),
      .rn_rcu_alloc_rcu_rdy_o(rn_rcu_alloc_rcu_rdy),
      .alu_issue_rob_tag_i(alu_issue_rob_tag),
      .alu_issue_pc_o(alu_issue_pc),
      .bru_issue_rob_tag_i(bru_issue_rob_tag),
      .bru_issue_pc_o(bru_issue_pc),
      .bru_issue_is_rvc_o(bru_issue_is_rvc),
      .bru_issue_btq_tag_o(bru_issue_btq_tag),
      .bru_issue_brq_tag_i(bru_issue_brq_tag),
      .bru_issue_pred_taken_o(bru_issue_pred_taken),
      .bru_issue_pred_target_o(bru_issue_pred_target),
      .wb_rob_cb_vld_i(wb_rob_cb_vld),
      .wb_rob_cb_tag_i(wb_rob_cb_tag),
      .wb_rob_br_vld_i(wb_rob_br_vld),
      .wb_rob_br_mispred_i(wb_rob_br_mispred),
      .wb_rob_br_is_taken_i(wb_rob_br_is_taken),
      .wb_rob_br_rob_tag_i(wb_rob_br_rob_tag),
      .wb_rob_br_brq_tag_i(wb_rob_br_brq_tag),
      .wb_rob_br_target_i(wb_rob_br_target),
      .wb_rob_re_vld_i(wb_rob_re_vld),
      .wb_rob_re_tag_i(wb_rob_re_tag),
      .wb_rob_re_cause_i(wb_rob_re_cause),
      .wb_rob_re_tval_i(wb_rob_re_tval),
      .redirect_vld_o(redirect_vld_o),
      .redirect_target_o(redirect_target_o),
      .redirect_pc_o(redirect_pc_o),
      .redirect_btq_tag_o(redirect_btq_tag_o),
      .redirect_is_taken_o(redirect_is_taken_o),
      .retire_insn_vld_o(retire_insn_vld),
      .retire_insn_pc_o(retire_insn_pc),
      .retire_insn_is_br_o(retire_insn_is_br),
      .retire_insn_mispred_o(retire_insn_mispred),
      .retire_insn_btq_tag_o(retire_insn_btq_tag),
      .retire_excp_vld_o(retire_excp_vld),
      .retire_excp_pc_o(retire_excp_pc),
      .retire_excp_is_rvc_o(retire_excp_is_rvc),
      .retire_excp_tval_o(retire_excp_tval),
      .retire_excp_cause_o(retire_excp_cause),
      .retire_excp_btq_tag_o(retire_excp_btq_tag),
      .rob_empty_o(rob_empty),
      .oldest_insn_rob_tag_o(oldest_insn_rob_tag),
      .brq_empty_o(brq_empty),
      .oldest_br_rob_tag_o(oldest_br_rob_tag),
      .flush_be_vld_o(rob_flush_be_vld),
      .clk(clk),
      .rst(rst)
  );

  rvh_dispatch u_rvh_dispatch (
      .rn_int_disp_vld_i(rn_int_disp_vld),
`ifdef SIMULATION
      .rn_int_disp_pc_i(rn_int_disp_pc),
`endif
      .rn_int_disp_rob_tag_i(rn_int_disp_rob_tag),
      .rn_int_disp_brq_tag_i(rn_int_disp_brq_tag),
      .rn_int_disp_major_op_i(rn_int_disp_major_op),
      .rn_int_disp_minor_op_i(rn_int_disp_minor_op),
      .rn_int_disp_use_rs1_i(rn_int_disp_use_rs1),
      .rn_int_disp_use_rs2_i(rn_int_disp_use_rs2),
      .rn_int_disp_use_rd_i(rn_int_disp_use_rd),
      .rn_int_disp_phy_rs1_i(rn_int_disp_phy_rs1),
      .rn_int_disp_phy_rs2_i(rn_int_disp_phy_rs2),
      .rn_int_disp_phy_rd_i(rn_int_disp_phy_rd),
      .rn_int_disp_imm_i(rn_int_disp_imm),
      .rn_int_disp_use_imm_i(rn_int_disp_use_imm),
      .rn_int_disp_use_pc_i(rn_int_disp_alu_use_pc),
      .rn_int_disp_rdy_o(rn_int_disp_rdy),
      .rn_ls_disp_vld_i(rn_ls_disp_vld),
`ifdef SIMULATION
      .rn_ls_disp_pc_i(rn_ls_disp_pc),
`endif
      .rn_ls_disp_rob_tag_i(rn_ls_disp_rob_tag),
      .rn_ls_disp_major_op_i(rn_ls_disp_major_op),
      .rn_ls_disp_minor_op_i(rn_ls_disp_minor_op),
      .rn_ls_disp_phy_rs1_i(rn_ls_disp_phy_rs1),
      .rn_ls_disp_phy_rs2_i(rn_ls_disp_phy_rs2),
      .rn_ls_disp_phy_rd_i(rn_ls_disp_phy_rd),
      .rn_ls_disp_imm_i(rn_ls_disp_imm),
      .rn_ls_disp_is_fence_i(rn_ls_disp_is_fence),
      .rn_ls_disp_rdy_o(rn_ls_disp_rdy),
      .disp_agu0_iq_vld_o(disp_agu0_iq_vld),
`ifdef SIMULATION
      .disp_agu0_iq_pc_o(disp_agu0_iq_pc),
`endif
      .disp_agu0_iq_rob_tag_o(disp_agu0_iq_rob_tag),
      .disp_agu0_iq_lsq_tag_o(disp_agu0_iq_lsq_tag),
      .disp_agu0_iq_fu_type_o(disp_agu0_iq_fu_type),
      .disp_agu0_iq_opcode_o(disp_agu0_iq_opcode),
      .disp_agu0_iq_phy_rs1_o(disp_agu0_iq_phy_rs1),
      .disp_agu0_iq_phy_rd_o(disp_agu0_iq_phy_rd),
      .disp_agu0_iq_imm_o(disp_agu0_iq_imm),
      .disp_agu0_iq_rdy_i(disp_agu0_iq_rdy),
      .disp_sdu0_iq_vld_o(disp_sdu0_iq_vld),
`ifdef SIMULATION
      .disp_sdu0_iq_pc_o(disp_sdu0_iq_pc),
`endif
      .disp_sdu0_iq_rob_tag_o(disp_sdu0_iq_rob_tag),
      .disp_sdu0_iq_stq_tag_o(disp_sdu0_iq_stq_tag),
      .disp_sdu0_iq_phy_rs2_o(disp_sdu0_iq_phy_rs2),
      .disp_sdu0_iq_rdy_i(disp_sdu0_iq_rdy),
      .disp_agu1_iq_vld_o(disp_agu1_iq_vld),
`ifdef SIMULATION
      .disp_agu1_iq_pc_o(disp_agu1_iq_pc),
`endif
      .disp_agu1_iq_rob_tag_o(disp_agu1_iq_rob_tag),
      .disp_agu1_iq_lsq_tag_o(disp_agu1_iq_lsq_tag),
      .disp_agu1_iq_fu_type_o(disp_agu1_iq_fu_type),
      .disp_agu1_iq_opcode_o(disp_agu1_iq_opcode),
      .disp_agu1_iq_phy_rs1_o(disp_agu1_iq_phy_rs1),
      .disp_agu1_iq_phy_rd_o(disp_agu1_iq_phy_rd),
      .disp_agu1_iq_imm_o(disp_agu1_iq_imm),
      .disp_agu1_iq_rdy_i(disp_agu1_iq_rdy),
      .disp_sdu1_iq_vld_o(disp_sdu1_iq_vld),
`ifdef SIMULATION
      .disp_sdu1_iq_pc_o(disp_sdu1_iq_pc),
`endif
      .disp_sdu1_iq_rob_tag_o(disp_sdu1_iq_rob_tag),
      .disp_sdu1_iq_stq_tag_o(disp_sdu1_iq_stq_tag),
      .disp_sdu1_iq_phy_rs2_o(disp_sdu1_iq_phy_rs2),
      .disp_sdu1_iq_rdy_i(disp_sdu1_iq_rdy),
      .disp_br_iq_vld_o(disp_br_iq_vld),
`ifdef SIMULATION
      .disp_br_iq_pc_o(disp_br_iq_pc),
`endif
      .disp_br_iq_rob_tag_o(disp_br_iq_rob_tag),
      .disp_br_iq_brq_tag_o(disp_br_iq_brq_tag),
      .disp_br_iq_opcode_o(disp_br_iq_opcode),
      .disp_br_iq_use_rs1_o(disp_br_iq_use_rs1),
      .disp_br_iq_use_rs2_o(disp_br_iq_use_rs2),
      .disp_br_iq_use_rd_o(disp_br_iq_use_rd),
      .disp_br_iq_phy_rs1_o(disp_br_iq_phy_rs1),
      .disp_br_iq_phy_rs2_o(disp_br_iq_phy_rs2),
      .disp_br_iq_phy_rd_o(disp_br_iq_phy_rd),
      .disp_br_iq_imm_o(disp_br_iq_imm),
      .disp_br_iq_rdy_i(disp_br_iq_rdy),
      .disp_alu0_iq_vld_o(disp_alu0_iq_vld),
`ifdef SIMULATION
      .disp_alu0_iq_pc_o(disp_alu0_iq_pc),
`endif
      .disp_alu0_iq_rob_tag_o(disp_alu0_iq_rob_tag),
      .disp_alu0_iq_opcode_o(disp_alu0_iq_opcode),
      .disp_alu0_iq_use_rs1_o(disp_alu0_iq_use_rs1),
      .disp_alu0_iq_use_rs2_o(disp_alu0_iq_use_rs2),
      .disp_alu0_iq_use_rd_o(disp_alu0_iq_use_rd),
      .disp_alu0_iq_phy_rs1_o(disp_alu0_iq_phy_rs1),
      .disp_alu0_iq_phy_rs2_o(disp_alu0_iq_phy_rs2),
      .disp_alu0_iq_phy_rd_o(disp_alu0_iq_phy_rd),
      .disp_alu0_iq_imm_o(disp_alu0_iq_imm),
      .disp_alu0_iq_use_imm_o(disp_alu0_iq_use_imm),
      .disp_alu0_iq_use_pc_o(disp_alu0_iq_use_pc),
      .disp_alu0_iq_rdy_i(disp_alu0_iq_rdy),
      .disp_alu1_iq_vld_o(disp_alu1_iq_vld),
`ifdef SIMULATION
      .disp_alu1_iq_pc_o(disp_alu1_iq_pc),
`endif
      .disp_alu1_iq_rob_tag_o(disp_alu1_iq_rob_tag),
      .disp_alu1_iq_opcode_o(disp_alu1_iq_opcode),
      .disp_alu1_iq_use_rs1_o(disp_alu1_iq_use_rs1),
      .disp_alu1_iq_use_rs2_o(disp_alu1_iq_use_rs2),
      .disp_alu1_iq_use_rd_o(disp_alu1_iq_use_rd),
      .disp_alu1_iq_phy_rs1_o(disp_alu1_iq_phy_rs1),
      .disp_alu1_iq_phy_rs2_o(disp_alu1_iq_phy_rs2),
      .disp_alu1_iq_phy_rd_o(disp_alu1_iq_phy_rd),
      .disp_alu1_iq_imm_o(disp_alu1_iq_imm),
      .disp_alu1_iq_use_imm_o(disp_alu1_iq_use_imm),
      .disp_alu1_iq_use_pc_o(disp_alu1_iq_use_pc),
      .disp_alu1_iq_rdy_i(disp_alu1_iq_rdy),
      .disp_alu2_iq_vld_o(disp_alu2_iq_vld),
`ifdef SIMULATION
      .disp_alu2_iq_pc_o(disp_alu2_iq_pc),
`endif
      .disp_alu2_iq_rob_tag_o(disp_alu2_iq_rob_tag),
      .disp_alu2_iq_fu_type_o(disp_alu2_iq_fu_type),
      .disp_alu2_iq_opcode_o(disp_alu2_iq_opcode),
      .disp_alu2_iq_use_rs1_o(disp_alu2_iq_use_rs1),
      .disp_alu2_iq_use_rs2_o(disp_alu2_iq_use_rs2),
      .disp_alu2_iq_use_rd_o(disp_alu2_iq_use_rd),
      .disp_alu2_iq_phy_rs1_o(disp_alu2_iq_phy_rs1),
      .disp_alu2_iq_phy_rs2_o(disp_alu2_iq_phy_rs2),
      .disp_alu2_iq_phy_rd_o(disp_alu2_iq_phy_rd),
      .disp_alu2_iq_imm_o(disp_alu2_iq_imm),
      .disp_alu2_iq_use_imm_o(disp_alu2_iq_use_imm),
      .disp_alu2_iq_use_pc_o(disp_alu2_iq_use_pc),
      .disp_alu2_iq_rdy_i(disp_alu2_iq_rdy),
      .disp_lsq_alloc_vld_o(disp_lsq_alloc_vld),
      .disp_lsq_alloc_fu_type_o(disp_lsq_alloc_fu_type),
      .disp_lsq_alloc_rob_tag_o(disp_lsq_alloc_rob_tag),
      .disp_lsq_alloc_is_fence_o(disp_lsq_alloc_is_fence),
      .disp_lsq_alloc_minor_op_o(disp_lsq_alloc_minor_op),
      .disp_lsq_alloc_lsq_tag_i(disp_lsq_alloc_lsq_tag),
      .disp_lsq_alloc_rdy_i(disp_lsq_alloc_rdy),
      .flush_i(rob_flush_be_vld),
      .clk(clk),
      .rst(rst)
  );

  rvh_iew u_rvh_iew (
      .hart_id_i(hart_id_i),
      .boot_address_i(boot_address_i),
      .priv_lvl_o(priv_lvl_o),
      .tvm_o(tvm_o),
      .tw_o(tw_o),
      .tsr_o(tsr_o),
      .mstatus_o(mstatus_o),
      .satp_o(satp_o),
      .fs_o(),
      .fflags_o(),
      .frm_o(),
      .fprec_o(),
      .rn_int_prf_alloc_vld_i(rn_int_prf_alloc_vld),
      .rn_int_prf_alloc_tag_i(rn_int_prf_alloc_preg),
      .rn_csr_ir_disp_vld_i(rn_csr_ir_disp_vld),
      .rn_csr_ir_disp_pc_i(rn_csr_ir_disp_pc),
      .rn_csr_ir_disp_rob_tag_i(rn_csr_ir_disp_rob_tag),
      .rn_csr_ir_disp_minor_op_i(rn_csr_ir_disp_minor_op),
      .rn_csr_ir_disp_use_imm_i(rn_csr_ir_disp_use_imm),
      .rn_csr_ir_disp_imm_i(rn_csr_ir_disp_imm),
      .rn_csr_ir_disp_use_rs1_i(rn_csr_ir_disp_use_rs1),
      .rn_csr_ir_disp_phy_rs1_i(rn_csr_ir_disp_phy_rs1),
      .rn_csr_ir_disp_use_rs2_i(rn_csr_ir_disp_use_rs2),
      .rn_csr_ir_disp_phy_rs2_i(rn_csr_ir_disp_phy_rs2),
      .rn_csr_ir_disp_phy_rd_i(rn_csr_ir_disp_phy_rd),
      .rn_csr_ir_disp_csr_addr_i(rn_csr_ir_disp_csr_addr),
      .disp_int0_iq_vld_i(disp_alu0_iq_vld),
`ifdef SIMULATION
      .disp_int0_iq_pc_i(disp_alu0_iq_pc),
`endif
      .disp_int0_iq_rob_tag_i(disp_alu0_iq_rob_tag),
      .disp_int0_iq_opcode_i(disp_alu0_iq_opcode),
      .disp_int0_iq_use_rs1_i(disp_alu0_iq_use_rs1),
      .disp_int0_iq_use_rs2_i(disp_alu0_iq_use_rs2),
      .disp_int0_iq_use_rd_i(disp_alu0_iq_use_rd),
      .disp_int0_iq_phy_rs1_i(disp_alu0_iq_phy_rs1),
      .disp_int0_iq_phy_rs2_i(disp_alu0_iq_phy_rs2),
      .disp_int0_iq_phy_rd_i(disp_alu0_iq_phy_rd),
      .disp_int0_iq_imm_i(disp_alu0_iq_imm),
      .disp_int0_iq_use_imm_i(disp_alu0_iq_use_imm),
      .disp_int0_iq_use_pc_i(disp_alu0_iq_use_pc),
      .disp_int0_iq_rdy_o(disp_alu0_iq_rdy),
      .disp_int1_iq_vld_i(disp_alu1_iq_vld),
`ifdef SIMULATION
      .disp_int1_iq_pc_i(disp_alu1_iq_pc),
`endif
      .disp_int1_iq_rob_tag_i(disp_alu1_iq_rob_tag),
      .disp_int1_iq_opcode_i(disp_alu1_iq_opcode),
      .disp_int1_iq_use_rs1_i(disp_alu1_iq_use_rs1),
      .disp_int1_iq_use_rs2_i(disp_alu1_iq_use_rs2),
      .disp_int1_iq_use_rd_i(disp_alu1_iq_use_rd),
      .disp_int1_iq_phy_rs1_i(disp_alu1_iq_phy_rs1),
      .disp_int1_iq_phy_rs2_i(disp_alu1_iq_phy_rs2),
      .disp_int1_iq_phy_rd_i(disp_alu1_iq_phy_rd),
      .disp_int1_iq_imm_i(disp_alu1_iq_imm),
      .disp_int1_iq_use_imm_i(disp_alu1_iq_use_imm),
      .disp_int1_iq_use_pc_i(disp_alu1_iq_use_pc),
      .disp_int1_iq_rdy_o(disp_alu1_iq_rdy),
      .disp_complex_int0_iq_vld_i(disp_alu2_iq_vld),
`ifdef SIMULATION
      .disp_complex_int0_iq_pc_i(disp_alu2_iq_pc),
`endif
      .disp_complex_int0_iq_fu_type_i(disp_alu2_iq_fu_type),
      .disp_complex_int0_iq_rob_tag_i(disp_alu2_iq_rob_tag),
      .disp_complex_int0_iq_opcode_i(disp_alu2_iq_opcode),
      .disp_complex_int0_iq_use_rs1_i(disp_alu2_iq_use_rs1),
      .disp_complex_int0_iq_use_rs2_i(disp_alu2_iq_use_rs2),
      .disp_complex_int0_iq_use_rd_i(disp_alu2_iq_use_rd),
      .disp_complex_int0_iq_phy_rs1_i(disp_alu2_iq_phy_rs1),
      .disp_complex_int0_iq_phy_rs2_i(disp_alu2_iq_phy_rs2),
      .disp_complex_int0_iq_phy_rd_i(disp_alu2_iq_phy_rd),
      .disp_complex_int0_iq_imm_i(disp_alu2_iq_imm),
      .disp_complex_int0_iq_use_imm_i(disp_alu2_iq_use_imm),
      .disp_complex_int0_iq_use_pc_i(disp_alu2_iq_use_pc),
      .disp_complex_int0_iq_rdy_o(disp_alu2_iq_rdy),
      .disp_br0_iq_vld_i(disp_br_iq_vld),
`ifdef SIMULATION
      .disp_br0_iq_pc_i(disp_br_iq_pc),
`endif
      .disp_br0_iq_rob_tag_i(disp_br_iq_rob_tag),
      .disp_br0_iq_brq_tag_i(disp_br_iq_brq_tag),
      .disp_br0_iq_opcode_i(disp_br_iq_opcode),
      .disp_br0_iq_use_rs1_i(disp_br_iq_use_rs1),
      .disp_br0_iq_use_rs2_i(disp_br_iq_use_rs2),
      .disp_br0_iq_use_rd_i(disp_br_iq_use_rd),
      .disp_br0_iq_phy_rs1_i(disp_br_iq_phy_rs1),
      .disp_br0_iq_phy_rs2_i(disp_br_iq_phy_rs2),
      .disp_br0_iq_phy_rd_i(disp_br_iq_phy_rd),
      .disp_br0_iq_imm_i(disp_br_iq_imm),
      .disp_br0_iq_rdy_o(disp_br_iq_rdy),
      .disp_agu0_iq_vld_i(disp_agu0_iq_vld),
`ifdef SIMULATION
      .disp_agu0_iq_pc_i(disp_agu0_iq_pc),
`endif
      .disp_agu0_iq_rob_tag_i(disp_agu0_iq_rob_tag),
      .disp_agu0_iq_lsq_tag_i(disp_agu0_iq_lsq_tag),
      .disp_agu0_iq_fu_type_i(disp_agu0_iq_fu_type),
      .disp_agu0_iq_opcode_i(disp_agu0_iq_opcode),
      .disp_agu0_iq_phy_rs1_i(disp_agu0_iq_phy_rs1),
      .disp_agu0_iq_phy_rd_i(disp_agu0_iq_phy_rd),
      .disp_agu0_iq_imm_i(disp_agu0_iq_imm),
      .disp_agu0_iq_rdy_o(disp_agu0_iq_rdy),
      .disp_agu1_iq_vld_i(disp_agu1_iq_vld),
`ifdef SIMULATION
      .disp_agu1_iq_pc_i(disp_agu1_iq_pc),
`endif
      .disp_agu1_iq_rob_tag_i(disp_agu1_iq_rob_tag),
      .disp_agu1_iq_lsq_tag_i(disp_agu1_iq_lsq_tag),
      .disp_agu1_iq_fu_type_i(disp_agu1_iq_fu_type),
      .disp_agu1_iq_opcode_i(disp_agu1_iq_opcode),
      .disp_agu1_iq_phy_rs1_i(disp_agu1_iq_phy_rs1),
      .disp_agu1_iq_phy_rd_i(disp_agu1_iq_phy_rd),
      .disp_agu1_iq_imm_i(disp_agu1_iq_imm),
      .disp_agu1_iq_rdy_o(disp_agu1_iq_rdy),
      .disp_sdu0_iq_vld_i(disp_sdu0_iq_vld),
`ifdef SIMULATION
      .disp_sdu0_iq_pc_i(disp_sdu0_iq_pc),
`endif
      .disp_sdu0_iq_rob_tag_i(disp_sdu0_iq_rob_tag),
      .disp_sdu0_iq_stq_tag_i(disp_sdu0_iq_stq_tag),
      .disp_sdu0_iq_phy_rs2_i(disp_sdu0_iq_phy_rs2),
      .disp_sdu0_iq_rdy_o(disp_sdu0_iq_rdy),
      .disp_sdu1_iq_vld_i(disp_sdu1_iq_vld),
`ifdef SIMULATION
      .disp_sdu1_iq_pc_i(disp_sdu1_iq_pc),
`endif
      .disp_sdu1_iq_rob_tag_i(disp_sdu1_iq_rob_tag),
      .disp_sdu1_iq_stq_tag_i(disp_sdu1_iq_stq_tag),
      .disp_sdu1_iq_phy_rs2_i(disp_sdu1_iq_phy_rs2),
      .disp_sdu1_iq_rdy_o(disp_sdu1_iq_rdy),
      .alu_issue_rob_tag_o(alu_issue_rob_tag),
      .alu_issue_pc_i(alu_issue_pc),
      .bru_issue_rob_tag_o(bru_issue_rob_tag),
      .bru_issue_brq_tag_o(bru_issue_brq_tag),
      .bru_issue_pc_i(bru_issue_pc),
      .bru_issue_is_rvc_i(bru_issue_is_rvc),
      .bru_issue_btq_tag_i(bru_issue_btq_tag),
      .bru_issue_pred_taken_i(bru_issue_pred_taken),
      .bru_issue_pred_target_i(bru_issue_pred_target),
      .agu_lsu_issue_vld_o(agu_iq_lsu_issue_vld),
      .agu_lsu_issue_excp_vld_o(agu_iq_lsu_issue_excp_vld),
      .agu_lsu_issue_fu_type_o(agu_iq_lsu_issue_fu_type),
      .agu_lsu_issue_opcode_o(agu_iq_lsu_issue_opcode),
      .agu_lsu_issue_rob_tag_o(agu_iq_lsu_issue_rob_tag),
      .agu_lsu_issue_lsq_tag_o(agu_iq_lsu_issue_lsq_tag),
      .agu_lsu_issue_iq_tag_o(agu_iq_lsu_issue_entry_tag),
      .agu_lsu_issue_vaddr_o(agu_iq_lsu_issue_vaddr),
      .agu_lsu_issue_phy_rd_o(agu_iq_lsu_issue_prd),
      .agu_lsu_issue_rdy_i(agu_iq_lsu_issue_rdy),
      .sdu_lsu_issue_vld_o(sdu_iq_lsu_issue_vld),
      .sdu_lsu_issue_stq_tag_o(sdu_iq_lsu_issue_stq_tag),
      .sdu_lsu_issue_data_o(sdu_iq_lsu_issue_data),
      .sdu_lsu_issue_rdy_i({LSU_DATA_PIPE_COUNT{1'b1}}),
      .lsu_iq_commit_vld_i(ls_pipe_agu_iq_cmt_vld),
      .lsu_iq_commit_reactivate_i(ls_pipe_agu_iq_cmt_reactivate),
      .lsu_iq_commit_tag_i(ls_pipe_agu_iq_cmt_tag),
      .lsu_cdb_wakeup_vld_i(lsu_cdb_wakeup_vld),
      .lsu_cdb_wakeup_ptag_i(lsu_cdb_wakeup_ptag),
      .lsu_prf_wr_vld_i(lsu_prf_wr_vld),
      .lsu_prf_wr_ptag_i(lsu_prf_wr_ptag),
      .lsu_prf_wr_data_i(lsu_prf_wr_data),
      .rob_complete_vld_o(int_rob_cb_vld),
      .rob_complete_tag_o(int_rob_cb_tag),
      .bru_resolve_vld_o(bru_resolve_vld),
      .bru_resolve_pc_o(bru_resolve_pc),
      .bru_resolve_taken_o(bru_resolve_taken),
      .bru_resolve_mispred_o(bru_resolve_mispred),
      .bru_resolve_rob_tag_o(bru_resolve_rob_tag),
      .bru_resolve_brq_tag_o(bru_resolve_brq_tag),
      .bru_resolve_btq_tag_o(bru_resolve_btq_tag),
      .bru_resolve_target_o(bru_resolve_target),
      .retire_insn_vld_i(retire_insn_vld_o),
      .retire_insn_pc_i(retire_insn_pc_o),
      .retire_insn_is_br_i(retire_insn_is_br_o),
      .retire_insn_mispred_i(retire_insn_mispred_o),
      .retire_excp_vld_i(retire_excp_vld),
      .retire_excp_pc_i(retire_excp_pc),
      .retire_excp_is_rvc_i(retire_excp_is_rvc),
      .retire_excp_tval_i(retire_excp_tval),
      .retire_excp_cause_i(retire_excp_cause),
      .csr_redirect_vld_o(csr_redirect_vld_o),
      .csr_redirect_pc_o(csr_redirect_pc_o),
      .csr_redirect_target_o(csr_redirect_target_o),
      .csr_insn_cb_vld_o(csr_rob_cb_vld),
      .csr_insn_cb_rob_tag_o(csr_rob_cb_tag),
      .csr_insn_re_vld_o(csr_rob_re_vld),
      .csr_insn_re_rob_tag_o(csr_rob_re_rob_tag),
      .csr_insn_re_cause_o(csr_rob_re_cause),
      .csr_insn_re_tval_o(csr_rob_re_tval),
      .rob_empty_i(rob_empty),
      .oldest_insn_rob_tag_i(oldest_insn_rob_tag),
      .interrupt_sw_i(interrupt_sw_i),
      .interrupt_timer_i(interrupt_timer_i),
      .interrupt_ext_i(interrupt_ext_i),
      .pmp_cfg_vld_o(pmp_cfg_vld_o),
      .pmp_cfg_tag_o(pmp_cfg_tag_o),
      .pmp_cfg_wr_data_o(pmp_cfg_wr_data_o),
      .pmp_cfg_rd_data_i(pmp_cfg_rd_data_i),
      .pmp_addr_vld_o(pmp_addr_vld_o),
      .pmp_addr_tag_o(pmp_addr_tag_o),
      .pmp_addr_wr_data_o(pmp_addr_wr_data_o),
      .pmp_addr_rd_data_i(pmp_addr_rd_data_i),
      .flush_icache_req_o(flush_icache_req_o),
      .flush_icache_gnt_i(flush_icache_gnt_i),
      .flush_dcache_req_o(flush_dcache_req),
      .flush_dcache_gnt_i(flush_dcache_gnt),
      .flush_itlb_req_o(flush_itlb_req_o),
      .flush_itlb_use_vpn_o(flush_itlb_use_vpn_o),
      .flush_itlb_use_asid_o(flush_itlb_use_asid_o),
      .flush_itlb_vpn_o(flush_itlb_vpn_o),
      .flush_itlb_asid_o(flush_itlb_asid_o),
      .flush_itlb_gnt_i(flush_itlb_gnt_i),
      .flush_dtlb_req_o(flush_dtlb_req),
      .flush_dtlb_use_vpn_o(flush_dtlb_use_vpn),
      .flush_dtlb_use_asid_o(flush_dtlb_use_asid),
      .flush_dtlb_vpn_o(flush_dtlb_vpn),
      .flush_dtlb_asid_o(flush_dtlb_asid),
      .flush_dtlb_gnt_i(flush_dtlb_gnt),
      .flush_stlb_req_o(flush_stlb_req_o),
      .flush_stlb_use_vpn_o(flush_stlb_use_vpn_o),
      .flush_stlb_use_asid_o(flush_stlb_use_asid_o),
      .flush_stlb_vpn_o(flush_stlb_vpn_o),
      .flush_stlb_asid_o(flush_stlb_asid_o),
      .flush_stlb_gnt_i(flush_stlb_gnt_i),
      .interrupt_ack_o(interrupt_ack_o),
      .interrupt_cause_o(interrupt_cause_o),
      .flush_i(rob_flush_be_vld),
      .clk(clk),
      .rst(rst)

  );

  rvh_lsu u_rvh_lsu (
      .misc_priv_lvl_i                  (priv_lvl_o),
      .misc_mstatus_i                   (mstatus_o),
      .misc_satp_i                      (satp_o),
      .rn_lsu_alloc_vld_i               (disp_lsq_alloc_vld),
      .rn_lsu_alloc_is_fence_i          (disp_lsq_alloc_is_fence),
      .rn_lsu_alloc_minor_op_i          (disp_lsq_alloc_minor_op),
      .rn_lsu_alloc_fu_type_i           (disp_lsq_alloc_fu_type),
      .rn_lsu_alloc_rob_tag_i           (disp_lsq_alloc_rob_tag),
      .rn_lsu_alloc_lsq_tag_o           (disp_lsq_alloc_lsq_tag),
      .rn_lsu_alloc_rdy_o               (disp_lsq_alloc_rdy),
      .agu_iq_lsu_issue_vld_i           (agu_iq_lsu_issue_vld),
      .agu_iq_lsu_issue_excp_vld_i      (agu_iq_lsu_issue_excp_vld),
      .agu_iq_lsu_issue_rob_tag_i       (agu_iq_lsu_issue_rob_tag),
      .agu_iq_lsu_issue_lsq_tag_i       (agu_iq_lsu_issue_lsq_tag),
      .agu_iq_lsu_issue_entry_tag_i     (agu_iq_lsu_issue_entry_tag),
      .agu_iq_lsu_issue_fu_type_i       (agu_iq_lsu_issue_fu_type),
      .agu_iq_lsu_issue_opcode_i        (agu_iq_lsu_issue_opcode),
      .agu_iq_lsu_issue_vaddr_i         (agu_iq_lsu_issue_vaddr),
      .agu_iq_lsu_issue_prd_i           (agu_iq_lsu_issue_prd),
      .agu_iq_lsu_issue_rdy_o           (agu_iq_lsu_issue_rdy),
      .sdu_iq_lsu_issue_vld_i           (sdu_iq_lsu_issue_vld),
      .sdu_iq_lsu_issue_stq_tag_i       (sdu_iq_lsu_issue_stq_tag),
      .sdu_iq_lsu_issue_data_i          (sdu_iq_lsu_issue_data),
      .ls_pipe_agu_iq_cmt_vld_o         (ls_pipe_agu_iq_cmt_vld),
      .ls_pipe_agu_iq_cmt_tag_o         (ls_pipe_agu_iq_cmt_tag),
      .ls_pipe_agu_iq_cmt_dealloc_o     (ls_pipe_agu_iq_cmt_dealloc),
      .ls_pipe_agu_iq_cmt_reactivate_o  (ls_pipe_agu_iq_cmt_reactivate),
      .l1d_rob_wb_vld_o                 (lsu_rob_cb_vld),
      .l1d_rob_wb_rob_tag_o             (lsu_rob_cb_tag),
      .lsu_cdb_wakeup_vld_o             (lsu_cdb_wakeup_vld),
      .lsu_cdb_wakeup_ptag_o            (lsu_cdb_wakeup_ptag),
      .lsu_prf_wr_vld_o                 (lsu_prf_wr_vld),
      .lsu_prf_wr_ptag_o                (lsu_prf_wr_ptag),
      .lsu_prf_wr_data_o                (lsu_prf_wr_data),
      .ls_pipe_rob_report_excp_vld_o    (ls_pipe_rob_report_excp_vld),
      .ls_pipe_rob_report_excp_cause_o  (ls_pipe_rob_report_excp_cause),
      .ls_pipe_rob_report_excp_tval_o   (ls_pipe_rob_report_excp_tval),
      .ls_pipe_rob_report_excp_rob_tag_o(ls_pipe_rob_report_excp_rob_tag),
      .l1_tlb_stlb_req_vld_o            (l1_tlb_stlb_req_vld_o),
      .l1_tlb_stlb_req_trans_id_o       (l1_tlb_stlb_req_trans_id_o),
      .l1_tlb_stlb_req_asid_o           (l1_tlb_stlb_req_asid_o),
      .l1_tlb_stlb_req_access_type_o    (l1_tlb_stlb_req_access_type_o),
      .l1_tlb_stlb_req_vpn_o            (l1_tlb_stlb_req_vpn_o),
      .l1_tlb_stlb_req_rdy_i            (l1_tlb_stlb_req_rdy_i),
      .l1_tlb_stlb_resp_vld_i           (l1_tlb_stlb_resp_vld_i),
      .l1_tlb_stlb_resp_trans_id_i      (l1_tlb_stlb_resp_trans_id_i),
      .l1_tlb_stlb_resp_asid_i          (l1_tlb_stlb_resp_asid_i),
      .l1_tlb_stlb_resp_pte_lvl_i       (l1_tlb_stlb_resp_pte_lvl_i),
      .l1_tlb_stlb_resp_pte_i           (l1_tlb_stlb_resp_pte_i),
      .l1_tlb_stlb_resp_vpn_i           (l1_tlb_stlb_resp_vpn_i),
      .l1_tlb_stlb_resp_access_type_i   (l1_tlb_stlb_resp_access_type_i),
      .l1_tlb_stlb_resp_access_fault_i  (l1_tlb_stlb_resp_access_fault_i),
      .l1_tlb_stlb_resp_page_fault_i    (l1_tlb_stlb_resp_page_fault_i),
      .l1_tlb_stlb_resp_rdy_o           (l1_tlb_stlb_resp_rdy_o),
      .dtlb_evict_vld_o                 (dtlb_evict_vld_o),
      .dtlb_evict_pte_o                 (dtlb_evict_pte_o),
      .dtlb_evict_page_lvl_o            (dtlb_evict_page_lvl_o),
      .dtlb_evict_vpn_o                 (dtlb_evict_vpn_o),
      .dtlb_evict_asid_o                (dtlb_evict_asid_o),
      .l1d_l2_req_arvalid_o             (l1d_l2_req_arvalid_o),
      .l1d_l2_req_arready_i             (l1d_l2_req_arready_i),
      .l1d_l2_req_ar_o                  (l1d_l2_req_ar_o),
      .l1d_l2_req_awvalid_o             (l1d_l2_req_awvalid_o),
      .l1d_l2_req_awready_i             (l1d_l2_req_awready_i),
      .l1d_l2_req_aw_o                  (l1d_l2_req_aw_o),
      .l1d_l2_req_wvalid_o              (l1d_l2_req_wvalid_o),
      .l1d_l2_req_wready_i              (l1d_l2_req_wready_i),
      .l1d_l2_req_w_o                   (l1d_l2_req_w_o),
      .l2_l1d_resp_bvalid_i             (l2_l1d_resp_bvalid_i),
      .l2_l1d_resp_bready_o             (l2_l1d_resp_bready_o),
      .l2_l1d_resp_b_i                  (l2_l1d_resp_b_i),
      .l2_l1d_resp_rvalid_i             (l2_l1d_resp_rvalid_i),
      .l2_l1d_resp_rready_o             (l2_l1d_resp_rready_o),
      .l2_l1d_resp_r_i                  (l2_l1d_resp_r_i),
      .ptw_walk_req_vld_i               (ptw_walk_req_vld_i),
      .ptw_walk_req_id_i                (ptw_walk_req_id_i),
      .ptw_walk_req_addr_i              (ptw_walk_req_addr_i),
      .ptw_walk_req_rdy_o               (ptw_walk_req_rdy_o),
      .ptw_walk_resp_vld_o              (ptw_walk_resp_vld_o),
      .ptw_walk_resp_id_o               (ptw_walk_resp_id_o),
      .ptw_walk_resp_pte_o              (ptw_walk_resp_pte_o),
      .ptw_walk_resp_rdy_i              (ptw_walk_resp_rdy_i),
      .brq_empty_i                      (brq_empty),
      .oldest_br_rob_tag_i              (oldest_br_rob_tag),
      .flush_dcache_req_i               (flush_dcache_req),
      .flush_dcache_gnt_o               (flush_dcache_gnt),
      .flush_dtlb_req_i                 (flush_dtlb_req),
      .flush_dtlb_use_vpn_i             (flush_dtlb_use_vpn),
      .flush_dtlb_use_asid_i            (flush_dtlb_use_asid),
      .flush_dtlb_vpn_i                 (flush_dtlb_vpn),
      .flush_dtlb_asid_i                (flush_dtlb_asid),
      .flush_dtlb_gnt_o                 (flush_dtlb_gnt),
      .flush_i                          (rob_flush_be_vld),
      .clk                              (clk),
      .rst                              (rst)
  );


endmodule : rvh_backend
