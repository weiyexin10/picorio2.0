module rvh_decode
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
(
    input  logic [  PRIV_LVL_WIDTH-1:0]                    priv_lvl_i,
    input  logic                                           debug_mode_i,
    input  logic                                           tvm_i,
    input  logic                                           tw_i,
    input  logic                                           tsr_i,
    // Interrupt
    input  logic                                           interrupt_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                    interrupt_cause_i,
    // IFU -> Decode
    input  logic [    DECODE_WIDTH-1:0]                    ifu_dec_inst_vld_i,
    input  logic [    DECODE_WIDTH-1:0][             31:0] ifu_dec_inst_i,
    input  logic [    DECODE_WIDTH-1:0][  VADDR_WIDTH-1:0] ifu_dec_inst_pc_i,
    input  logic [    DECODE_WIDTH-1:0][BTQ_TAG_WIDTH-1:0] ifu_dec_inst_btq_tag_i,
    input  logic [    DECODE_WIDTH-1:0]                    ifu_dec_inst_pred_taken_i,
    input  logic [    DECODE_WIDTH-1:0][  VADDR_WIDTH-1:0] ifu_dec_inst_pred_target_i,
    input  logic [ EXCP_TVAL_WIDTH-1:0]                    ifu_dec_inst_excp_tval_i,
    input  logic                                           ifu_dec_inst_excp_vld_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                    ifu_dec_inst_excp_cause_i,
    output logic [    DECODE_WIDTH-1:0]                    ifu_dec_inst_rdy_o,

    // Decode -> Rename
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_vld_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_excp_vld_o,
    output logic [    DECODE_WIDTH-1:0][   VADDR_WIDTH-1:0] dec_rn_inst_pc_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_is_rvc_o,
    output logic [    DECODE_WIDTH-1:0][ BTQ_TAG_WIDTH-1:0] dec_rn_inst_btq_tag_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_pred_taken_o,
    output logic [    DECODE_WIDTH-1:0][   VADDR_WIDTH-1:0] dec_rn_inst_pred_target_o,
    output logic [    DECODE_WIDTH-1:0][MAJOR_OP_WIDTH-1:0] dec_rn_inst_major_op_o,
    output logic [    DECODE_WIDTH-1:0][MINOR_OP_WIDTH-1:0] dec_rn_inst_minor_op_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_use_rs1_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_use_rs2_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_use_rd_o,
    output logic [    DECODE_WIDTH-1:0][               4:0] dec_rn_inst_rs1_o,
    output logic [    DECODE_WIDTH-1:0][               4:0] dec_rn_inst_rs2_o,
    output logic [    DECODE_WIDTH-1:0][               4:0] dec_rn_inst_rd_o,
    output logic [    DECODE_WIDTH-1:0][              31:0] dec_rn_inst_imm_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_use_imm_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_alu_use_pc_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_is_fence_o,
    output logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_control_flow_o,
    output logic [ EXCP_TVAL_WIDTH-1:0]                     dec_rn_inst_excp_tval_o,
    output logic [EXCP_CAUSE_WIDTH-1:0]                     dec_rn_inst_excp_cause_o,
    input  logic [    DECODE_WIDTH-1:0]                     dec_rn_inst_rdy_i,

    input logic flush_i,

    input clk,
    input rst
);

  // RVC Expender
  logic [DECODE_WIDTH-1:0][                31:0] rvc_expend_inst;
  logic [DECODE_WIDTH-1:0]                       rvc_expend_illegal;
  logic [DECODE_WIDTH-1:0]                       rvc_expend_is_rvc;

  // RISCV Decoder
  logic [DECODE_WIDTH-1:0]                       dec_illegal;

  logic [DECODE_WIDTH-1:0]                       inst_is_illegal;
  logic [DECODE_WIDTH-1:0]                       inst_is_ecall;
  logic [DECODE_WIDTH-1:0]                       inst_is_ebreak;
  logic [DECODE_WIDTH-1:0]                       inst_excp_vld;
  logic [DECODE_WIDTH-1:0][ EXCP_TVAL_WIDTH-1:0] inst_excp_tval;
  logic [DECODE_WIDTH-1:0][EXCP_CAUSE_WIDTH-1:0] inst_excp_cause;

  logic [DECODE_WIDTH-1:0] inst_vld_r, inst_vld_nxt;
  logic [DECODE_WIDTH-1:0] inst_excp_vld_r, inst_excp_vld_nxt;
  logic [DECODE_WIDTH-1:0][BTQ_TAG_WIDTH-1:0] inst_btq_tag_r, inst_btq_tag_nxt;
  logic [DECODE_WIDTH-1:0] inst_pred_taken_r, inst_pred_taken_nxt;
  logic [DECODE_WIDTH-1:0][VADDR_WIDTH-1:0] inst_pred_target_r, inst_pred_target_nxt;
  logic [DECODE_WIDTH-1:0][VADDR_WIDTH-1:0] inst_pc_r, inst_pc_nxt;
  logic [DECODE_WIDTH-1:0] inst_is_rvc_r, inst_is_rvc_nxt;
  logic [DECODE_WIDTH-1:0][MAJOR_OP_WIDTH-1:0] inst_major_op_r, inst_major_op_nxt;
  logic [DECODE_WIDTH-1:0][MINOR_OP_WIDTH-1:0] inst_minor_op_r, inst_minor_op_nxt;
  logic [DECODE_WIDTH-1:0] inst_use_rs1_r, inst_use_rs1_nxt;
  logic [DECODE_WIDTH-1:0] inst_use_rs2_r, inst_use_rs2_nxt;
  logic [DECODE_WIDTH-1:0] inst_use_rd_r, inst_use_rd_nxt;
  logic [DECODE_WIDTH-1:0][4:0] inst_rs1_r, inst_rs1_nxt;
  logic [DECODE_WIDTH-1:0][4:0] inst_rs2_r, inst_rs2_nxt;
  logic [DECODE_WIDTH-1:0][4:0] inst_rd_r, inst_rd_nxt;
  logic [DECODE_WIDTH-1:0][31:0] inst_imm_r, inst_imm_nxt;
  logic [DECODE_WIDTH-1:0] inst_use_imm_r, inst_use_imm_nxt;
  logic [DECODE_WIDTH-1:0] inst_alu_use_pc_r, inst_alu_use_pc_nxt;
  logic [DECODE_WIDTH-1:0] inst_is_fence_r, inst_is_fence_nxt;
  logic [DECODE_WIDTH-1:0] inst_control_flow_r, inst_control_flow_nxt;
  logic [EXCP_TVAL_WIDTH-1:0] inst_excp_tval_r, inst_excp_tval_nxt;
  logic [EXCP_CAUSE_WIDTH-1:0] inst_excp_cause_r, inst_excp_cause_nxt;

  logic ifu_dec_inst_fire;

  logic [DECODE_WIDTH-1:0] dec_rn_inst_fire;
  logic dec_rn_fire, dec_rn_stall;

  always_comb begin
    for (int i = 0; i < DECODE_WIDTH; i++) begin
      inst_excp_vld[i]   = inst_is_illegal[i] | inst_is_ecall[i] | inst_is_ebreak[i] | interrupt_i;
      inst_excp_tval[i] = {EXCP_TVAL_WIDTH{inst_is_illegal[i]}} & ifu_dec_inst_i[i];
      inst_excp_cause[i] = {EXCP_CAUSE_WIDTH{1'b0}};
      if (inst_is_illegal[i]) begin
        inst_excp_cause[i] = riscv_pkg::ILLEGAL_INSTR;
      end else if (inst_is_ecall[i]) begin
        if (priv_lvl_i == riscv_pkg::PRIV_M) begin
          inst_excp_cause[i] = riscv_pkg::ENV_CALL_MMODE;
        end else if (priv_lvl_i == riscv_pkg::PRIV_S) begin
          inst_excp_cause[i] = riscv_pkg::ENV_CALL_SMODE;
        end else if (priv_lvl_i == riscv_pkg::PRIV_U) begin
          inst_excp_cause[i] = riscv_pkg::ENV_CALL_UMODE;
        end
      end else if (inst_is_ebreak[i]) begin
        inst_excp_cause[i] = riscv_pkg::BREAKPOINT;
      end else if (interrupt_i) begin
        inst_excp_cause[i] = interrupt_cause_i;
      end
    end
  end


  always_comb begin
    inst_vld_nxt = ifu_dec_inst_vld_i;
    inst_excp_vld_nxt = {DECODE_WIDTH{ifu_dec_inst_excp_vld_i}};
    inst_excp_tval_nxt = ifu_dec_inst_excp_tval_i;
    inst_excp_cause_nxt = ifu_dec_inst_excp_cause_i;
    if (~ifu_dec_inst_excp_vld_i) begin
      for (int i = DECODE_WIDTH - 1; i >= 0; i = i - 1) begin
        if (inst_excp_vld[i]) begin
          inst_excp_vld_nxt[i]= 1'b1;
          inst_excp_tval_nxt  = inst_excp_tval[i];
          inst_excp_cause_nxt = inst_excp_cause[i];
        end
      end
    end
  end

  assign inst_is_illegal = (rvc_expend_is_rvc & rvc_expend_illegal) |
        (~rvc_expend_is_rvc & dec_illegal);

  assign dec_rn_inst_fire = dec_rn_inst_vld_o & dec_rn_inst_rdy_i;
  assign dec_rn_fire = |dec_rn_inst_fire;
  assign dec_rn_stall = dec_rn_inst_fire != dec_rn_inst_vld_o;

  assign ifu_dec_inst_fire = |(ifu_dec_inst_vld_i & ifu_dec_inst_rdy_o);

  assign inst_pc_nxt = ifu_dec_inst_pc_i;
  assign inst_is_rvc_nxt = rvc_expend_is_rvc;
  assign inst_btq_tag_nxt = ifu_dec_inst_btq_tag_i;
  assign inst_pred_taken_nxt = ifu_dec_inst_pred_taken_i;
  assign inst_pred_target_nxt = ifu_dec_inst_pred_target_i;

  assign ifu_dec_inst_rdy_o = {DECODE_WIDTH{~dec_rn_stall}};

  always_ff @(posedge clk) begin
    if (rst) begin
      inst_vld_r <= {(DECODE_WIDTH) {1'b0}};
    end else begin
      if (flush_i) begin
        inst_vld_r <= {(DECODE_WIDTH) {1'b0}};
      end else begin
        if (dec_rn_stall) begin
          inst_vld_r <= inst_vld_r & ~dec_rn_inst_fire;
        end else begin
          inst_vld_r <= inst_vld_nxt;
        end
      end
    end
  end

  always_ff @(posedge clk) begin
    if (ifu_dec_inst_fire) begin
      inst_pc_r           <= inst_pc_nxt;
      inst_is_rvc_r       <= inst_is_rvc_nxt;
      inst_btq_tag_r      <= inst_btq_tag_nxt;
      inst_pred_taken_r   <= inst_pred_taken_nxt;
      inst_pred_target_r  <= inst_pred_target_nxt;
      inst_major_op_r     <= inst_major_op_nxt;
      inst_minor_op_r     <= inst_minor_op_nxt;
      inst_use_rs1_r      <= inst_use_rs1_nxt;
      inst_use_rs2_r      <= inst_use_rs2_nxt;
      inst_use_rd_r       <= inst_use_rd_nxt;
      inst_rs1_r          <= inst_rs1_nxt;
      inst_rs2_r          <= inst_rs2_nxt;
      inst_rd_r           <= inst_rd_nxt;
      inst_imm_r          <= inst_imm_nxt;
      inst_use_imm_r      <= inst_use_imm_nxt;
      inst_alu_use_pc_r   <= inst_alu_use_pc_nxt;
      inst_is_fence_r     <= inst_is_fence_nxt;
      inst_control_flow_r <= inst_control_flow_nxt;
      inst_excp_vld_r     <= inst_excp_vld_nxt;
      inst_excp_tval_r    <= inst_excp_tval_nxt;
      inst_excp_cause_r   <= inst_excp_cause_nxt;
    end
  end

  assign dec_rn_inst_vld_o          = inst_vld_r;
  assign dec_rn_inst_is_rvc_o       = inst_is_rvc_r;
  assign dec_rn_inst_pc_o           = inst_pc_r;
  assign dec_rn_inst_excp_vld_o     = inst_excp_vld_r;
  assign dec_rn_inst_btq_tag_o      = inst_btq_tag_r;
  assign dec_rn_inst_pred_taken_o   = inst_pred_taken_r;
  assign dec_rn_inst_pred_target_o  = inst_pred_target_r;
  assign dec_rn_inst_major_op_o     = inst_major_op_r;
  assign dec_rn_inst_minor_op_o     = inst_minor_op_r;
  assign dec_rn_inst_use_rs1_o      = inst_use_rs1_r;
  assign dec_rn_inst_use_rs2_o      = inst_use_rs2_r;
  assign dec_rn_inst_use_rd_o       = inst_use_rd_r;
  assign dec_rn_inst_rs1_o          = inst_rs1_r;
  assign dec_rn_inst_rs2_o          = inst_rs2_r;
  assign dec_rn_inst_rd_o           = inst_rd_r;
  assign dec_rn_inst_imm_o          = inst_imm_r;
  assign dec_rn_inst_use_imm_o      = inst_use_imm_r;
  assign dec_rn_inst_alu_use_pc_o   = inst_alu_use_pc_r;
  assign dec_rn_inst_is_fence_o     = inst_is_fence_r;
  assign dec_rn_inst_control_flow_o = inst_control_flow_r;
  assign dec_rn_inst_excp_tval_o    = inst_excp_tval_r;
  assign dec_rn_inst_excp_cause_o   = inst_excp_cause_r;


  generate
    for (genvar i = 0; i < DECODE_WIDTH; i++) begin
      rvc_expender rvc_expender_u (
          .instr_i(ifu_dec_inst_i[i]),
          .instr_o(rvc_expend_inst[i]),
          .illegal_instr_o(rvc_expend_illegal[i]),
          .is_rvc_o(rvc_expend_is_rvc[i])
      );
      riscv_decoder #(
          .IS_XLEN64  (1'b1),
          .SUPPORT_RVI(1'b1),
          .SUPPORT_RVM(1'b1),
          .SUPPORT_RVA(1'b1),
          .SUPPORT_WFI(1'b0)
      ) riscv_decoder_u (
          .uncompressed_inst_i(rvc_expend_inst[i]),
          .priv_lvl_i(priv_lvl_i),
          .debug_mode_i(debug_mode_i),
          .tvm_i(tvm_i),
          .tw_i(tw_i),
          .tsr_i(tsr_i),
          .inst_illegal_o(dec_illegal[i]),
          .inst_ecall_o(inst_is_ecall[i]),
          .inst_ebreak_o(inst_is_ebreak[i]),
          .inst_major_op_o(inst_major_op_nxt[i]),
          .inst_minor_op_o(inst_minor_op_nxt[i]),
          .inst_use_rs1_o(inst_use_rs1_nxt[i]),
          .inst_use_rs2_o(inst_use_rs2_nxt[i]),
          .inst_use_rd_o(inst_use_rd_nxt[i]),
          .inst_rs1_o(inst_rs1_nxt[i]),
          .inst_rs2_o(inst_rs2_nxt[i]),
          .inst_rd_o(inst_rd_nxt[i]),
          .inst_imm_o(inst_imm_nxt[i]),
          .inst_use_imm_o(inst_use_imm_nxt[i]),
          .inst_alu_use_pc_o(inst_alu_use_pc_nxt[i]),
          .inst_is_fence_o(inst_is_fence_nxt[i]),
          .inst_control_flow_o(inst_control_flow_nxt[i])
      );
    end
  endgenerate






endmodule : rvh_decode

