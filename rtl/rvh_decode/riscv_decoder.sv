module riscv_decoder
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
#(
    parameter IS_XLEN64   = 1,
    parameter SUPPORT_RVI = 1,
    parameter SUPPORT_RVM = 1,
    parameter SUPPORT_RVA = 1,
    parameter SUPPORT_WFI = 1
) (
    input logic [31:0] uncompressed_inst_i,

    input logic [1:0] priv_lvl_i,
    input logic       debug_mode_i,
    input logic       tvm_i,
    input logic       tw_i,
    input logic       tsr_i,

    output logic inst_illegal_o,
    output logic inst_ecall_o,
    output logic inst_ebreak_o,

    output logic [MAJOR_OP_WIDTH-1:0] inst_major_op_o,
    output logic [MINOR_OP_WIDTH-1:0] inst_minor_op_o,
    output logic                      inst_use_rs1_o,
    output logic                      inst_use_rs2_o,
    output logic                      inst_use_rd_o,
    output logic [               4:0] inst_rs1_o,
    output logic [               4:0] inst_rs2_o,
    output logic [               4:0] inst_rd_o,
    output logic [              31:0] inst_imm_o,
    output logic                      inst_use_imm_o,
    output logic                      inst_alu_use_pc_o,
    output logic                      inst_is_fence_o,
    output logic                      inst_control_flow_o
);

  logic      [  6:0] inst_op;
  logic      [  2:0] inst_func3;
  logic      [  6:0] inst_func7;
  logic      [ 11:0] inst_func12;
  imm_type_t         inst_imm_type;

  logic      [11:10] csr_access_permission;
  logic      [  9:8] csr_access_priv;
  logic      [ 11:0] csr_address;
  logic              csr_read_only;
  logic              csr_priv_check_fail;

  logic      [ 31:0] imm_i_type;
  logic      [ 31:0] imm_s_type;
  logic      [ 31:0] imm_sb_type;
  logic      [ 31:0] imm_u_type;
  logic      [ 31:0] imm_uj_type;
  logic      [ 31:0] imm_zimm_type;

  always_comb begin
    inst_illegal_o      = 1'b0;
    inst_control_flow_o = 1'b0;
    inst_ecall_o        = 1'b0;
    inst_ebreak_o       = 1'b0;
    inst_major_op_o     = OP_ALU;
    inst_minor_op_o     = ALU_ADD;
    inst_use_rs1_o      = 1'b0;
    inst_use_rs2_o      = 1'b0;
    inst_use_rd_o       = 1'b0;
    inst_imm_type       = IIMM;
    inst_use_imm_o      = 1'b0;
    inst_alu_use_pc_o   = 1'b0;
    inst_is_fence_o     = 1'b0;
    unique case (inst_op)
      OpcodeSystem: begin
        inst_major_op_o = OP_CSR;
        inst_control_flow_o = 1'b1;
        inst_imm_type = ZIMM;
        unique case (inst_func3)
          3'b000: begin
            if (uncompressed_inst_i[19:15] != 5'b0 || uncompressed_inst_i[11:7] != 5'b0) begin
              inst_illegal_o = 1'b1;
            end
            unique case (inst_func12)
              12'b0000_0000_0000: begin
                inst_ecall_o = 1'b1;
              end
              12'b0000_0000_0001: begin
                inst_ebreak_o = 1'b1;
              end
              12'b0011_0000_0010: begin
                inst_minor_op_o = CSR_MRET;
                if (priv_lvl_i != riscv_pkg::PRIV_M) begin
                  inst_illegal_o = 1'b1;
                end
              end
              12'b0001_0000_0010: begin
                inst_minor_op_o = CSR_SRET;
                if ((priv_lvl_i == riscv_pkg::PRIV_S && tsr_i) ||
                                    priv_lvl_i == riscv_pkg::PRIV_U) begin
                  inst_illegal_o = 1'b1;
                end
              end
              12'b0111_1011_0010: begin
                inst_minor_op_o = CSR_DRET;
                if (debug_mode_i) begin
                  inst_illegal_o = 1'b1;
                end
              end
              12'b0001_0000_0101: begin
                if (SUPPORT_WFI) begin
                  inst_minor_op_o = CSR_WFI;
                end
                if ((priv_lvl_i == riscv_pkg::PRIV_S && tw_i) ||
                                    priv_lvl_i == riscv_pkg::PRIV_U) begin
                  inst_illegal_o = 1'b1;
                end
              end
              default: begin
                if (inst_func7 == 7'b000_1001) begin
                  inst_minor_op_o = CSR_SFENCE_VMA;
                  inst_use_rs1_o = 1'b1;
                  inst_use_rs2_o = 1'b1;
                  inst_illegal_o  = (uncompressed_inst_i[11:7] != 5'b0) | (tvm_i & (priv_lvl_i == riscv_pkg::PRIV_S));
                end else begin
                  inst_illegal_o = 1'b1;
                end
              end
            endcase
          end
          3'b001: begin
            inst_minor_op_o = CSR_CSRRW;
            inst_use_rs1_o  = 1'b1;
            inst_use_rd_o   = 1'b1;
            if (csr_read_only | csr_priv_check_fail) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b010: begin
            inst_use_rd_o = 1'b1;
            if (uncompressed_inst_i[19:15] == 5'b0) begin
              inst_minor_op_o = CSR_CSRR;
            end else begin
              inst_minor_op_o = CSR_CSRRS;
              inst_use_rs1_o  = 1'b1;
              if (csr_read_only) begin
                inst_illegal_o = 1'b1;
              end
            end
            if (csr_priv_check_fail) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b011: begin
            inst_use_rd_o = 1'b1;
            if (uncompressed_inst_i[19:15] == 5'b0) begin
              inst_minor_op_o = CSR_CSRR;
            end else begin
              inst_minor_op_o = CSR_CSRRC;
              inst_use_rs1_o  = 1'b1;
              if (csr_read_only) begin
                inst_illegal_o = 1'b1;
              end
            end
            if (csr_priv_check_fail) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b101: begin
            inst_minor_op_o = CSR_CSRRW;
            inst_use_rd_o   = 1'b1;
            inst_use_imm_o  = 1'b1;
            if (csr_read_only | csr_priv_check_fail) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b110: begin
            inst_use_rd_o  = 1'b1;
            inst_use_imm_o = 1'b1;
            if (uncompressed_inst_i[19:15] == 5'b0) begin
              inst_minor_op_o = CSR_CSRR;
            end else begin
              inst_minor_op_o = CSR_CSRRS;
              if (csr_read_only) begin
                inst_illegal_o = 1'b1;
              end
            end
            if (csr_priv_check_fail) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b111: begin
            inst_use_rd_o  = 1'b1;
            inst_use_imm_o = 1'b1;
            if (uncompressed_inst_i[19:15] == 5'b0) begin
              inst_minor_op_o = CSR_CSRR;
            end else begin
              inst_minor_op_o = CSR_CSRRC;
              if (csr_read_only) begin
                inst_illegal_o = 1'b1;
              end
            end
            if (csr_priv_check_fail) begin
              inst_illegal_o = 1'b1;
            end
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeMiscMem: begin
        inst_imm_type = IIMM;
        unique case (inst_func3)
          3'b000: begin
            inst_major_op_o = OP_STU;
            inst_minor_op_o = STU_FENCE;
            inst_is_fence_o = 1'b1;
          end
          3'b001: begin
            inst_major_op_o = OP_CSR;
            inst_minor_op_o = CSR_FENCEI;
            inst_control_flow_o = 1'b1;
            if (inst_func12 != 12'b0) begin
              inst_illegal_o = 1'b1;
            end
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
        if (uncompressed_inst_i[19:15] != 5'b0 || uncompressed_inst_i[11:7] != 5'b0) begin
          inst_illegal_o = 1'b1;
        end
      end
      OpcodeLoad: begin
        inst_major_op_o = OP_LDU;
        inst_imm_type   = IIMM;
        inst_use_rs1_o  = 1'b1;
        inst_use_rd_o   = 1'b1;
        unique case (inst_func3)
          3'b000: begin
            inst_minor_op_o = LDU_LB;
          end
          3'b001: begin
            inst_minor_op_o = LDU_LH;
          end
          3'b010: begin
            inst_minor_op_o = LDU_LW;
          end
          3'b011: begin
            if (IS_XLEN64) begin
              inst_minor_op_o = LDU_LD;
            end else begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b100: begin
            inst_minor_op_o = LDU_LBU;
          end
          3'b101: begin
            inst_minor_op_o = LDU_LHU;
          end
          3'b110: begin
            if (IS_XLEN64) begin
              inst_minor_op_o = LDU_LWU;
            end else begin
              inst_illegal_o = 1'b1;
            end
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeAmo: begin
        inst_major_op_o = OP_STU;
        inst_imm_type   = NONE;
        inst_use_rs1_o  = 1'b1;
        inst_use_rs2_o  = 1'b1;
        inst_use_rd_o   = 1'b1;
        inst_is_fence_o = 1'b1;
        unique case (inst_func7[6:2])
          5'b00010: begin
            inst_use_rs2_o = 1'b0;
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_LRW;
              end
              3'b011: begin
                inst_minor_op_o = STU_LRD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b00011: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_SCW;
              end
              3'b011: begin
                inst_minor_op_o = STU_SCD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b00001: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOSWAPW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOSWAPD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b00000: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOADDW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOADDD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b00100: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOXORW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOXORD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b01100: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOANDW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOANDD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b01000: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOORW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOORD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b10000: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOMINW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOMIND;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b10100: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOMAXW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOMAXD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b11000: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOMINUW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOMINUD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          5'b11100: begin
            unique case (inst_func3)
              3'b010: begin
                inst_minor_op_o = STU_AMOMAXUW;
              end
              3'b011: begin
                inst_minor_op_o = STU_AMOMAXUD;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeStore: begin
        inst_major_op_o = OP_STU;
        inst_imm_type   = SIMM;
        inst_use_rs1_o  = 1'b1;
        inst_use_rs2_o  = 1'b1;
        unique case (inst_func3)
          3'b000: begin
            inst_minor_op_o = STU_SB;
          end
          3'b001: begin
            inst_minor_op_o = STU_SH;
          end
          3'b010: begin
            inst_minor_op_o = STU_SW;
          end
          3'b011: begin
            if (IS_XLEN64) begin
              inst_minor_op_o = STU_SD;
            end else begin
              inst_illegal_o = 1'b1;
            end
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeBranch: begin
        inst_major_op_o = OP_BRU;
        inst_imm_type   = SBIMM;
        inst_use_rs1_o  = 1'b1;
        inst_use_rs2_o  = 1'b1;
        unique case (inst_func3)
          3'b000: begin
            inst_minor_op_o = BRU_BEQ;
          end
          3'b001: begin
            inst_minor_op_o = BRU_BNE;
          end
          3'b100: begin
            inst_minor_op_o = BRU_BLT;
          end
          3'b101: begin
            inst_minor_op_o = BRU_BGE;
          end
          3'b110: begin
            inst_minor_op_o = BRU_BLTU;
          end
          3'b111: begin
            inst_minor_op_o = BRU_BGEU;
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeJal: begin
        inst_major_op_o = OP_BRU;
        inst_imm_type   = UJIMM;
        inst_use_rd_o   = 1'b1;
        inst_minor_op_o = BRU_JAL;
      end
      OpcodeJalr: begin
        inst_major_op_o = OP_BRU;
        inst_imm_type   = IIMM;
        inst_use_rs1_o  = 1'b1;
        inst_use_rd_o   = 1'b1;
        inst_minor_op_o = BRU_JALR;
        if (inst_func3 != 3'b000) begin
          inst_illegal_o = 1'b1;
        end
      end
      OpcodeOp: begin
        inst_major_op_o = (inst_func7 == 7'b0000001) ? ((inst_func3[2] ? OP_DIV : OP_MUL)) : OP_ALU;
        inst_use_rs1_o = 1'b1;
        inst_use_rs2_o = 1'b1;
        inst_use_rd_o = 1'b1;
        unique case ({
          inst_func7, inst_func3
        })
          {
            7'b0000000, 3'b000
          } : begin
            inst_minor_op_o = ALU_ADD;
          end
          {
            7'b0100000, 3'b000
          } : begin
            inst_minor_op_o = ALU_SUB;
          end
          {
            7'b0000000, 3'b001
          } : begin
            inst_minor_op_o = ALU_SLL;
          end
          {
            7'b0000000, 3'b010
          } : begin
            inst_minor_op_o = ALU_SLT;
          end
          {
            7'b0000000, 3'b011
          } : begin
            inst_minor_op_o = ALU_SLTU;
          end
          {
            7'b0000000, 3'b100
          } : begin
            inst_minor_op_o = ALU_XOR;
          end
          {
            7'b0000000, 3'b101
          } : begin
            inst_minor_op_o = ALU_SRL;
          end
          {
            7'b0100000, 3'b101
          } : begin
            inst_minor_op_o = ALU_SRA;
          end
          {
            7'b0000000, 3'b110
          } : begin
            inst_minor_op_o = ALU_OR;
          end
          {
            7'b0000000, 3'b111
          } : begin
            inst_minor_op_o = ALU_AND;
          end
          {
            7'b0000001, 3'b000
          } : begin
            inst_minor_op_o = MUL_MUL;
          end
          {
            7'b0000001, 3'b001
          } : begin
            inst_minor_op_o = MUL_MULH;
          end
          {
            7'b0000001, 3'b010
          } : begin
            inst_minor_op_o = MUL_MULHSU;
          end
          {
            7'b0000001, 3'b011
          } : begin
            inst_minor_op_o = MUL_MULHU;
          end
          {
            7'b0000001, 3'b100
          } : begin
            inst_minor_op_o = DIV_DIV;
          end
          {
            7'b0000001, 3'b101
          } : begin
            inst_minor_op_o = DIV_DIVU;
          end
          {
            7'b0000001, 3'b110
          } : begin
            inst_minor_op_o = DIV_REM;
          end
          {
            7'b0000001, 3'b111
          } : begin
            inst_minor_op_o = DIV_REMU;
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeOp32: begin
        if (IS_XLEN64) begin
          inst_major_op_o = (inst_func7 == 7'b0000001) ?
                        ((inst_func3[2] ? OP_DIV : OP_MUL)) : OP_ALU;
          inst_use_rs1_o = 1'b1;
          inst_use_rs2_o = 1'b1;
          inst_use_rd_o = 1'b1;
          unique case ({
            inst_func7, inst_func3
          })
            {
              7'b000_0000, 3'b000
            } : begin
              inst_minor_op_o = ALU_ADDW;
            end
            {
              7'b010_0000, 3'b000
            } : begin
              inst_minor_op_o = ALU_SUBW;
            end
            {
              7'b000_0000, 3'b001
            } : begin
              inst_minor_op_o = ALU_SLLW;
            end
            {
              7'b000_0000, 3'b101
            } : begin
              inst_minor_op_o = ALU_SRLW;
            end
            {
              7'b010_0000, 3'b101
            } : begin
              inst_minor_op_o = ALU_SRAW;
            end
            {
              7'b000_0001, 3'b000
            } : begin
              inst_minor_op_o = MUL_MULW;
            end
            {
              7'b000_0001, 3'b100
            } : begin
              inst_minor_op_o = DIV_DIVW;
            end
            {
              7'b000_0001, 3'b101
            } : begin
              inst_minor_op_o = DIV_DIVUW;
            end
            {
              7'b000_0001, 3'b110
            } : begin
              inst_minor_op_o = DIV_REMW;
            end
            {
              7'b000_0001, 3'b111
            } : begin
              inst_minor_op_o = DIV_REMUW;
            end
            default: begin
              inst_illegal_o = 1'b1;
            end
          endcase
        end else begin
          inst_illegal_o = 1'b1;
        end
      end
      OpcodeOpImm: begin
        inst_major_op_o = OP_ALU;
        inst_imm_type   = IIMM;
        inst_use_rs1_o  = 1'b1;
        inst_use_rd_o   = 1'b1;
        inst_use_imm_o  = 1'b1;
        unique case (inst_func3)
          3'b000: begin
            inst_minor_op_o = ALU_ADD;
          end
          3'b001: begin
            inst_minor_op_o = ALU_SLL;
            if ((inst_func7[6:1] != 6'b0) || (!IS_XLEN64 && inst_func7[0])) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b010: begin
            inst_minor_op_o = ALU_SLT;
          end
          3'b011: begin
            inst_minor_op_o = ALU_SLTU;
          end
          3'b100: begin
            inst_minor_op_o = ALU_XOR;
          end
          3'b101: begin
            unique case (inst_func7[6:1])
              6'b000000: begin
                inst_minor_op_o = ALU_SRL;
              end
              6'b010000: begin
                inst_minor_op_o = ALU_SRA;
              end
              default: begin
                inst_illegal_o = 1'b1;
              end
            endcase
            if (!IS_XLEN64 && inst_func7[0]) begin
              inst_illegal_o = 1'b1;
            end
          end
          3'b110: begin
            inst_minor_op_o = ALU_OR;
          end
          3'b111: begin
            inst_minor_op_o = ALU_AND;
          end
          default: begin
            inst_illegal_o = 1'b1;
          end
        endcase
      end
      OpcodeOpImm32: begin
        if (IS_XLEN64) begin
          inst_major_op_o = OP_ALU;
          inst_imm_type   = IIMM;
          inst_use_rs1_o  = 1'b1;
          inst_use_rd_o   = 1'b1;
          inst_use_imm_o  = 1'b1;
          unique case (inst_func3)
            3'b000: begin
              inst_minor_op_o = ALU_ADDW;
            end
            3'b001: begin
              inst_minor_op_o = ALU_SLLW;
              if (inst_func7 != 7'b0) begin
                inst_illegal_o = 1'b1;
              end
            end
            3'b101: begin
              unique case (inst_func7)
                7'b0000000: begin
                  inst_minor_op_o = ALU_SRLW;
                end
                7'b0100000: begin
                  inst_minor_op_o = ALU_SRAW;
                end
                default: begin
                  inst_illegal_o = 1'b1;
                end
              endcase
            end
            default: begin
              inst_illegal_o = 1'b1;
            end
          endcase
        end else begin
          inst_illegal_o = 1'b1;
        end
      end
      OpcodeAuipc: begin
        inst_major_op_o = OP_ALU;
        inst_imm_type = UIMM;
        inst_use_rd_o = 1'b1;
        inst_minor_op_o = ALU_ADD;
        inst_alu_use_pc_o = 1'b1;
        inst_use_imm_o = 1'b1;
      end
      OpcodeLui: begin
        inst_major_op_o = OP_ALU;
        inst_imm_type   = UIMM;
        inst_use_rd_o   = 1'b1;
        inst_minor_op_o = ALU_ADDW;
        inst_use_imm_o  = 1'b1;
      end
      default: begin
        inst_illegal_o = 1'b1;
      end
    endcase
    if (inst_illegal_o | inst_ecall_o | inst_ebreak_o) begin
      inst_major_op_o = OP_ALU;
      inst_minor_op_o = ALU_ADD;
      inst_use_rs1_o  = 1'b0;
      inst_use_rs2_o  = 1'b0;
      inst_use_rd_o   = 1'b0;
    end
  end


  always_comb begin : imm_select_u
    imm_i_type = {{32 - 12{uncompressed_inst_i[31]}}, uncompressed_inst_i[31:20]};
    imm_s_type = {
      {32 - 12{uncompressed_inst_i[31]}}, uncompressed_inst_i[31:25], uncompressed_inst_i[11:7]
    };
    imm_sb_type = {
      {32 - 13{uncompressed_inst_i[31]}},
      uncompressed_inst_i[31],
      uncompressed_inst_i[7],
      uncompressed_inst_i[30:25],
      uncompressed_inst_i[11:8],
      1'b0
    };
    imm_u_type = {
      {32 - 32{uncompressed_inst_i[31]}}, uncompressed_inst_i[31:12], 12'b0
    };  // JAL, AUIPC, sign extended to 64 bit
    imm_uj_type = {
      {32 - 20{uncompressed_inst_i[31]}},
      uncompressed_inst_i[19:12],
      uncompressed_inst_i[20],
      uncompressed_inst_i[30:21],
      1'b0
    };
    imm_zimm_type = {
       15'b0, uncompressed_inst_i[31:15]
      }; // CSR
    case (inst_imm_type)
      IIMM: begin
        inst_imm_o = imm_i_type;
      end
      SIMM: begin
        inst_imm_o = imm_s_type;
      end
      SBIMM: begin
        inst_imm_o = imm_sb_type;
      end
      UIMM: begin
        inst_imm_o = imm_u_type;
      end
      UJIMM: begin
        inst_imm_o = imm_uj_type;
      end
      ZIMM: begin
        inst_imm_o = imm_zimm_type;
      end
      default: begin
        inst_imm_o = {32{1'b0}};
      end
    endcase

  end

  assign inst_op               = uncompressed_inst_i[6:0];
  assign inst_func3            = uncompressed_inst_i[14:12];
  assign inst_func7            = uncompressed_inst_i[31:25];

  assign inst_rs1_o            = inst_use_rs1_o ? uncompressed_inst_i[19:15] : 5'b0;
  assign inst_rs2_o            = inst_use_rs2_o ? uncompressed_inst_i[24:20] : 5'b0;
  assign inst_rd_o             = inst_use_rd_o ? uncompressed_inst_i[11:7] : 5'b0;

  assign inst_func12           = uncompressed_inst_i[31:20];

  assign csr_address           = uncompressed_inst_i[31:20];
  assign csr_access_permission = csr_address[11:10];
  assign csr_access_priv       = csr_address[9:8];

  assign csr_read_only         = csr_access_permission == 2'b11;
  assign csr_priv_check_fail   = priv_lvl_i < csr_access_priv;



endmodule
