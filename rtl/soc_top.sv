`include "typedef.svh"
`include "assign.svh"

module soc_top
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_l1d_pkg::*;
(
    input logic                   uart_rx_i,
    output logic                  uart_tx_o,
    
    input logic                   clk,
    input logic                   rst
);

  logic                             soft_int;
  logic                             timer_int;
  logic                             uart_int;
////////////////////////////////
  logic               [2-1:0]       l1_l2_arvalid;
  logic               [2-1:0]       l1_l2_arready;
  cache_mem_if_ar_t   [2-1:0]       l1_l2_ar;

  logic               [2-1:0]       l1_l2_rvalid;
  logic               [2-1:0]       l1_l2_rready;
  cache_mem_if_r_t    [2-1:0]       l1_l2_r;

  logic               [2-1:0]       l1_l2_awvalid;
  logic               [2-1:0]       l1_l2_awready;
  cache_mem_if_aw_t   [2-1:0]       l1_l2_aw;

  logic               [2-1:0]       l1_l2_wvalid;
  logic               [2-1:0]       l1_l2_wready;
  cache_mem_if_w_t    [2-1:0]       l1_l2_w;

  logic               [2-1:0]       l1_l2_bvalid;
  logic               [2-1:0]       l1_l2_bready;
  cache_mem_if_b_t    [2-1:0]       l1_l2_b;

////////////////////////////////
  logic              axi_mem_arvalid;
  logic              axi_mem_arready;
  cache_mem_if_ar_t  axi_mem_ar;
  logic              axi_mem_ar_extra_id_bit;

  logic              axi_mem_rvalid;
  logic              axi_mem_rready;
  cache_mem_if_r_t   axi_mem_r;
  logic              axi_mem_r_extra_id_bit;

  logic              axi_mem_awvalid;
  logic              axi_mem_awready;
  cache_mem_if_aw_t  axi_mem_aw;
  logic              axi_mem_aw_extra_id_bit;

  logic              axi_mem_wvalid;
  logic              axi_mem_wready;
  cache_mem_if_w_t   axi_mem_w;

  logic              axi_mem_bvalid;
  logic              axi_mem_bready;
  cache_mem_if_b_t   axi_mem_b;
  logic              axi_mem_b_extra_id_bit;

///////////////////////////////

  logic              peri_arvalid;
  logic              peri_arready;
  cache_mem_if_ar_t  peri_ar;
  logic              peri_ar_extra_id_bit;

  logic              peri_rvalid;
  logic              peri_rready;
  cache_mem_if_r_t   peri_r;
  logic              peri_r_extra_id_bit;

  logic              peri_awvalid;
  logic              peri_awready;
  cache_mem_if_aw_t  peri_aw;
  logic              peri_aw_extra_id_bit;

  logic              peri_wvalid;
  logic              peri_wready;
  cache_mem_if_w_t   peri_w;

  logic              peri_bvalid;
  logic              peri_bready;
  cache_mem_if_b_t   peri_b;
  logic              peri_b_extra_id_bit;

///////////////////////////////

  logic              clint_arvalid;
  logic              clint_arready;
  cache_mem_if_ar_t  clint_ar;
  logic              clint_ar_extra_id_bit;

  logic              clint_rvalid;
  logic              clint_rready;
  cache_mem_if_r_t   clint_r;
  logic              clint_r_extra_id_bit;

  logic              clint_awvalid;
  logic              clint_awready;
  cache_mem_if_aw_t  clint_aw;
  logic              clint_aw_extra_id_bit;

  logic              clint_wvalid;
  logic              clint_wready;
  cache_mem_if_w_t   clint_w;

  logic              clint_bvalid;
  logic              clint_bready;
  cache_mem_if_b_t   clint_b;
  logic              clint_b_extra_id_bit;

//////////////////////////////////////////////////////////////////////////////////////////////
// core
//////////////////////////////////////////////////////////////////////////////////////////////
  rvh_core u_rvh_core (
      .hart_id_i          (0),
      .boot_address_i     (39'h80000000),
      .interrupt_sw_i     (soft_int),
      .interrupt_timer_i  (timer_int),
      .interrupt_ext_i    (uart_int),
      // AR
      .l1_l2_req_arvalid_o(l1_l2_arvalid),
      .l1_l2_req_arready_i(l1_l2_arready),
      .l1_l2_req_ar_o     (l1_l2_ar),
      // ewrq -> mem bus
      // AW
      .l1_l2_req_awvalid_o(l1_l2_awvalid),
      .l1_l2_req_awready_i(l1_l2_awready),
      .l1_l2_req_aw_o     (l1_l2_aw),
      // W
      .l1_l2_req_wvalid_o (l1_l2_wvalid),
      .l1_l2_req_wready_i (l1_l2_wready),
      .l1_l2_req_w_o      (l1_l2_w),
      // L1D -> L2 : Response
      // B
      .l2_l1_resp_bvalid_i(l1_l2_bvalid),
      .l2_l1_resp_bready_o(l1_l2_bready),
      .l2_l1_resp_b_i     (l1_l2_b),
      // mem bus -> mlfb
      // R
      .l2_l1_resp_rvalid_i(l1_l2_rvalid),
      .l2_l1_resp_rready_o(l1_l2_rready),
      .l2_l1_resp_r_i     (l1_l2_r),

      .clk                (clk),
      .rst                (rst)
  );
  assign l1_l2_r[0].mesi_sta = EXCLUSIVE;
  assign l1_l2_r[0].err      = 1'b0;
  assign l1_l2_r[1].mesi_sta = EXCLUSIVE;
  assign l1_l2_r[1].err      = 1'b0;

//////////////////////////////////////////////////////////////////////////////////////////////
// axi4 crossbar config & definition
//////////////////////////////////////////////////////////////////////////////////////////////

  localparam axi_pkg::xbar_rule_64_t [axi_pkg::xbar_cfg.NoAddrRules-1:0] AddrMap = axi_pkg::addr_map_gen();

  AXI_BUS #(
    .AXI_ADDR_WIDTH ( axi_pkg::xbar_cfg.AxiAddrWidth      ),
    .AXI_DATA_WIDTH ( axi_pkg::xbar_cfg.AxiDataWidth      ),
    .AXI_ID_WIDTH   ( axi_pkg::xbar_cfg.AxiIdWidthSlvPorts),
    .AXI_USER_WIDTH ( 1 )
  ) master [axi_pkg::xbar_cfg.NoSlvPorts-1:0] ();

  AXI_BUS #(
    .AXI_ADDR_WIDTH ( axi_pkg::xbar_cfg.AxiAddrWidth     ),
    .AXI_DATA_WIDTH ( axi_pkg::xbar_cfg.AxiDataWidth     ),
    .AXI_ID_WIDTH   ( axi_pkg::xbar_cfg.AxiIdWidthSlvPorts + $clog2(axi_pkg::xbar_cfg.NoSlvPorts)),
    .AXI_USER_WIDTH ( 1     )
  ) slave [axi_pkg::xbar_cfg.NoMstPorts-1:0] ();


//////////////////////////////////////////////////////////////////////////////////////////////
// master port connection
//////////////////////////////////////////////////////////////////////////////////////////////
  for (genvar i = 0; i < axi_pkg::xbar_cfg.NoSlvPorts; i++) begin : gen_master_sig
    assign master[i].aw_id          = l1_l2_aw[i].awid;
    assign master[i].aw_addr        = l1_l2_aw[i].awaddr;
    assign master[i].aw_len         = l1_l2_aw[i].awlen;
    assign master[i].aw_size        = l1_l2_aw[i].awsize;
    assign master[i].aw_burst       = l1_l2_aw[i].awburst;
    assign master[i].aw_lock        = '0;
    assign master[i].aw_cache       = '0;
    assign master[i].aw_prot        = '0;
    assign master[i].aw_qos         = '0;
    assign master[i].aw_region      = '0;
    assign master[i].aw_atop        = '0;
    assign master[i].aw_user        = '0;
    assign master[i].aw_valid       = l1_l2_awvalid[i];
    assign l1_l2_awready[i]         = master[i].aw_ready;

    assign master[i].w_data         = l1_l2_w[i].wdata;
    assign master[i].w_strb         = l1_l2_w[i].wstrb;
    assign master[i].w_last         = l1_l2_w[i].wlast;
    assign master[i].w_user         = '0;  // l1_l2_w[0].wid
    assign master[i].w_valid        = l1_l2_wvalid[i];
    assign l1_l2_wready[i]          = master[i].w_ready;

    assign l1_l2_b[i].bid           = master[i].b_id;
    assign l1_l2_b[i].bresp         = master[i].b_resp;
    assign l1_l2_bvalid[i]          = master[i].b_valid;
    assign master[i].b_ready        = l1_l2_bready[i];

    assign master[i].ar_id          = l1_l2_ar[i].arid;
    assign master[i].ar_addr        = l1_l2_ar[i].araddr;
    assign master[i].ar_len         = l1_l2_ar[i].arlen;
    assign master[i].ar_size        = l1_l2_ar[i].arsize;
    assign master[i].ar_burst       = l1_l2_ar[i].arburst;
    assign master[i].ar_lock        = '0;
    assign master[i].ar_cache       = '0;
    assign master[i].ar_prot        = '0;
    assign master[i].ar_qos         = '0;
    assign master[i].ar_region      = '0;
    assign master[i].ar_user        = '0;
    assign master[i].ar_valid       = l1_l2_arvalid[i];
    assign l1_l2_arready[i]         = master[i].ar_ready;

    assign l1_l2_r[i].rid           = master[i].r_id;
    assign l1_l2_r[i].dat           = master[i].r_data;
    assign l1_l2_r[i].rresp         = master[i].r_resp;
    assign l1_l2_r[i].rlast         = master[i].r_last;
    // assign master[i].r_user         = '0;
    assign l1_l2_rvalid[i]          = master[i].r_valid;
    assign master[i].r_ready        = l1_l2_rready[i];
  end

//////////////////////////////////////////////////////////////////////////////////////////////
// slave port connection
//////////////////////////////////////////////////////////////////////////////////////////////
  assign {axi_mem_aw.awid, axi_mem_aw_extra_id_bit}     = slave[0].aw_id;
  assign axi_mem_aw.awaddr                              = slave[0].aw_addr;
  assign axi_mem_aw.awlen                               = slave[0].aw_len;
  assign axi_mem_aw.awsize                              = slave[0].aw_size;
  assign axi_mem_aw.awburst                             = slave[0].aw_burst;
  assign axi_mem_awvalid                                = slave[0].aw_valid;
  assign slave[0].aw_ready                              = axi_mem_awready;

  assign axi_mem_w.wdata                                = slave[0].w_data;
  assign axi_mem_w.wstrb                                = slave[0].w_strb;
  assign axi_mem_w.wlast                                = slave[0].w_last;
  assign axi_mem_wvalid                                 = slave[0].w_valid;
  assign slave[0].w_ready                               = axi_mem_wready;

  assign slave[0].b_id                                  = {axi_mem_b.bid, axi_mem_b_extra_id_bit};
  assign slave[0].b_resp                                = axi_mem_b.bresp;
  assign slave[0].b_user                                = '0;
  assign slave[0].b_valid                               = axi_mem_bvalid;
  assign axi_mem_bready                                 = slave[0].b_ready;

  assign {axi_mem_ar.arid, axi_mem_ar_extra_id_bit}     = slave[0].ar_id;
  assign axi_mem_ar.araddr                              = slave[0].ar_addr;
  assign axi_mem_ar.arlen                               = slave[0].ar_len;
  assign axi_mem_ar.arsize                              = slave[0].ar_size;
  assign axi_mem_ar.arburst                             = slave[0].ar_burst;
  assign axi_mem_arvalid                                = slave[0].ar_valid;
  assign slave[0].ar_ready                              = axi_mem_arready;

  assign slave[0].r_id                                  = {axi_mem_r.rid, axi_mem_r_extra_id_bit};
  assign slave[0].r_data                                = axi_mem_r.dat;
  assign slave[0].r_resp                                = axi_mem_r.rresp;
  assign slave[0].r_last                                = axi_mem_r.rlast;
  assign slave[0].r_user                                = '0;
  assign slave[0].r_valid                               = axi_mem_rvalid;
  assign axi_mem_rready                                 = slave[0].r_ready;

///////////////////////////////////////////////

  assign {peri_aw.awid, peri_aw_extra_id_bit}     = slave[1].aw_id;
  assign peri_aw.awaddr                           = slave[1].aw_addr;
  assign peri_aw.awlen                            = slave[1].aw_len;
  assign peri_aw.awsize                           = slave[1].aw_size;
  assign peri_aw.awburst                          = slave[1].aw_burst;
  assign peri_awvalid                             = slave[1].aw_valid;
  assign slave[1].aw_ready                        = peri_awready;

  assign peri_w.wdata                             = slave[1].w_data;
  assign peri_w.wstrb                             = slave[1].w_strb;
  assign peri_w.wlast                             = slave[1].w_last;
  assign peri_wvalid                              = slave[1].w_valid;
  assign slave[1].w_ready                         = peri_wready;

  assign slave[1].b_id                            = {peri_b.bid, peri_b_extra_id_bit};
  assign slave[1].b_resp                          = peri_b.bresp;
  assign slave[1].b_user                          = '0;
  assign slave[1].b_valid                         = peri_bvalid;
  assign peri_bready                              = slave[1].b_ready;

  assign {peri_ar.arid, peri_ar_extra_id_bit}     = slave[1].ar_id;
  assign peri_ar.araddr                           = slave[1].ar_addr;
  assign peri_ar.arlen                            = slave[1].ar_len;
  assign peri_ar.arsize                           = slave[1].ar_size;
  assign peri_ar.arburst                          = slave[1].ar_burst;
  assign peri_arvalid                             = slave[1].ar_valid;
  assign slave[1].ar_ready                        = peri_arready;

  assign slave[1].r_id                            = {peri_r.rid, peri_r_extra_id_bit};
  assign slave[1].r_data                          = peri_r.dat;
  assign slave[1].r_resp                          = peri_r.rresp;
  assign slave[1].r_last                          = peri_r.rlast;
  assign slave[1].r_user                          = '0;
  assign slave[1].r_valid                         = peri_rvalid;
  assign peri_rready                              = slave[1].r_ready;


///////////////////////////////////////////////
  assign {clint_aw.awid, clint_aw_extra_id_bit}       = slave[2].aw_id;
  assign clint_aw.awaddr                              = slave[2].aw_addr;
  assign clint_aw.awlen                               = slave[2].aw_len;
  assign clint_aw.awsize                              = slave[2].aw_size;
  assign clint_aw.awburst                             = slave[2].aw_burst;
  assign clint_awvalid                                = slave[2].aw_valid;
  assign slave[2].aw_ready                            = clint_awready;

  assign clint_w.wdata                                = slave[2].w_data;
  assign clint_w.wstrb                                = slave[2].w_strb;
  assign clint_w.wlast                                = slave[2].w_last;
  assign clint_wvalid                                 = slave[2].w_valid;
  assign slave[2].w_ready                             = clint_wready;

  assign slave[2].b_id                                  = {clint_b.bid, clint_b_extra_id_bit};
  assign slave[2].b_resp                                = clint_b.bresp;
  assign slave[2].b_user                                = '0;
  assign slave[2].b_valid                               = clint_bvalid;
  assign clint_bready                                   = slave[2].b_ready;

  assign {clint_ar.arid, clint_ar_extra_id_bit}     = slave[2].ar_id;
  assign clint_ar.araddr                              = slave[2].ar_addr;
  assign clint_ar.arlen                               = slave[2].ar_len;
  assign clint_ar.arsize                              = slave[2].ar_size;
  assign clint_ar.arburst                             = slave[2].ar_burst;
  assign clint_arvalid                                = slave[2].ar_valid;
  assign slave[2].ar_ready                              = clint_arready;

  assign slave[2].r_id                                  = {clint_r.rid, clint_r_extra_id_bit};
  assign slave[2].r_data                                = clint_r.dat;
  assign slave[2].r_resp                                = clint_r.rresp;
  assign slave[2].r_last                                = clint_r.rlast;
  assign slave[2].r_user                                = '0;
  assign slave[2].r_valid                               = clint_rvalid;
  assign clint_rready                                 = slave[2].r_ready;

///////////////////////////////////////////////
  for (genvar i = 3; i < axi_pkg::xbar_cfg.NoMstPorts; i++) begin : gen_slave_sig
    assign slave[i].aw_ready  = '0;
    assign slave[i].w_ready   = '0;
    assign slave[i].b_id      = '0;
    assign slave[i].b_resp    = '0;
    assign slave[i].b_user    = '0;
    assign slave[i].b_valid   = '0;
    assign slave[i].ar_ready  = '0;
    assign slave[i].r_id      = '0;
    assign slave[i].r_data    = '0;
    assign slave[i].r_resp    = '0;
    assign slave[i].r_last    = '0;
    assign slave[i].r_user    = '0;
    assign slave[i].r_valid   = '0;
  end
//////////////////////////////////////////////////////////////////////////////////////////////
// axi crossbar
//////////////////////////////////////////////////////////////////////////////////////////////
  axi_xbar_intf #(
    .AXI_USER_WIDTH ( 1  ),
    .Cfg            ( axi_pkg::xbar_cfg        ),
    .rule_t         ( axi_pkg::xbar_rule_64_t )
  ) i_xbar_dut (
    .clk_i                  ( clk     ),
    .rst_ni                 ( ~rst    ),
    .test_i                 ( 1'b0    ),
    .slv_ports              ( master  ),
    .mst_ports              ( slave   ),
    .addr_map_i             ( AddrMap ),
    .en_default_mst_port_i  ( '0      ),
    .default_mst_port_i     ( '0      )
  );
//////////////////////////////////////////////////////////////////////////////////////////////
// clint
//////////////////////////////////////////////////////////////////////////////////////////////
rvh_clint #(
    .ID_WIDTH       ($bits(mem_tid_t) + 1),
    .AXI_ADDR_WIDTH (64),
    .AXI_DATA_WIDTH (64),
    .AXI_USER_WIDTH (1),
    .BASE_ADDR      (64'h2000000)
)
(
    .clk(clk),
    .rstn(~rst),
    
    // aw
    .slave_aw_id      ({clint_aw.awid, clint_aw_extra_id_bit}),
    .slave_aw_addr    (clint_aw.awaddr),
    .slave_aw_len     (clint_aw.awlen),
    .slave_aw_size    (clint_aw.awsize),
    .slave_aw_burst   (clint_aw.awburst),
    .slave_aw_valid   (clint_awvalid),
    .slave_aw_ready   (clint_awready),
    
    // AR
    .slave_ar_id      ({clint_ar.arid, clint_ar_extra_id_bit}),
    .slave_ar_addr    (clint_ar.araddr),
    .slave_ar_len     (clint_ar.arlen),
    .slave_ar_size    (clint_ar.arsize),
    .slave_ar_burst   (clint_ar.arburst),
    .slave_ar_valid   (clint_arvalid),
    .slave_ar_ready   (clint_arready),

    // W
    .slave_w_data     (clint_w.wdata),
    .slave_w_strb     (clint_w.wstrb),
    .slave_w_last     (clint_w.wlast),
    .slave_w_valid    (clint_wvalid),
    .slave_w_ready    (clint_wready),
    // B
    .slave_b_id       ({clint_b.bid, clint_b_extra_id_bit}),
    .slave_b_resp     (clint_b.bresp),
    .slave_b_valid    (clint_bvalid),
    .slave_b_ready    (clint_bready),
    // R
    .slave_r_id       ({clint_r.rid, clint_r_extra_id_bit}),
    .slave_r_data     (clint_r.dat),
    .slave_r_resp     (clint_r.rresp),
    .slave_r_last     (clint_r.rlast),
    .slave_r_valid    (clint_rvalid),
    .slave_r_ready    (clint_rready),

    .timer_int_o      (timer_int),
    .soft_int_o       (soft_int)
);

//////////////////////////////////////////////////////////////////////////////////////////////
// peripheral
//////////////////////////////////////////////////////////////////////////////////////////////
rvh_peri #(
    .AXI4_ADDRESS_WIDTH (64),
    .AXI4_RDATA_WIDTH   (64),
    .AXI4_WDATA_WIDTH   (64),
    .AXI4_ID_WIDTH      ($bits(mem_tid_t) + 1),
    .AXI4_USER_WIDTH    (1),

    .BUFF_DEPTH_SLAVE   (2),
    .APB_NUM_SLAVES     (1),
    .APB_ADDR_WIDTH     (64)
)(

    //AXI write address bus 
    .AWID_i     ({peri_aw.awid, peri_aw_extra_id_bit}),
    .AWADDR_i   (peri_aw.awaddr),
    .AWLEN_i    (peri_aw.awlen),
    .AWSIZE_i   (peri_aw.awsize),
    .AWBURST_i  (peri_aw.awburst),
    .AWLOCK_i   ('0),
    .AWCACHE_i  ('0),
    .AWPROT_i   ('0),
    .AWREGION_i ('0),
    .AWUSER_i   ('0),
    .AWQOS_i    ('0),
    .AWVALID_i  (peri_awvalid),
    .AWREADY_o  (peri_awready),

    //AXI write data bus
    .WDATA_i    (peri_w.wdata),
    .WSTRB_i    (peri_w.wstrb),
    .WLAST_i    (peri_w.wlast),
    .WUSER_i    ('0),
    .WVALID_i   (peri_wvalid),
    .WREADY_o   (peri_wready),

    //AXI write response bus
    .BID_o      ({peri_b.bid, peri_b_extra_id_bit}),
    .BRESP_o    (peri_b.bresp),
    .BVALID_o   (peri_bvalid),
    .BUSER_o    ('0),
    .BREADY_i   (peri_bready),

    //AXI read address bus
    .ARID_i     ({peri_ar.arid, peri_ar_extra_id_bit}),
    .ARADDR_i   (peri_ar.araddr),
    .ARLEN_i    (peri_ar.arlen),
    .ARSIZE_i   (peri_ar.arsize),
    .ARBURST_i  (peri_ar.arburst),
    .ARLOCK_i   ('0),
    .ARCACHE_i  ('0),
    .ARPROT_i   ('0),
    .ARREGION_i ('0),
    .ARUSER_i   ('0),
    .ARQOS_i    ('0),
    .ARVALID_i  (peri_arvalid),
    .ARREADY_o  (peri_arready),

    //AXI read data bus
    .RID_o      ({peri_r.rid, peri_r_extra_id_bit}),
    .RDATA_o    (peri_r.dat),
    .RRESP_o    (peri_r.rresp),
    .RLAST_o    (peri_r.rlast),
    .RUSER_o    ('0),
    .RVALID_o   (peri_rvalid),
    .RREADY_i   (peri_rready),

    .uart_rx_i  (uart_rx_i),      // Receiver input
    .uart_tx_o  (uart_tx_o),      // Transmitter output
    .uart_int_o (uart_int), 

    .clk        (clk),
    .rst        (rst)
);


`ifndef SYNTHESIS
  logic [63:0] 	         i_wstrb;
  always_comb begin
    for (int i = 0; i < 64; i++) begin
        i_wstrb[i] = axi_mem_w.wstrb[i / 8];
    end
  end

  axi_mem #(
      .ID_WIDTH($bits(mem_tid_t) + 1),
      .MEM_SIZE(1 << 29),  //byte 512MB
      .mem_clear(0),
      .mem_simple_seq(0),
      .READ_DELAY_CYCLE(1 << 4),
      .READ_DELAY_CYCLE_RANDOMLIZE(0),
      .READ_DELAY_CYCLE_RANDOMLIZE_UPDATE_CYCLE(1 << 10),
      .AXI_DATA_WIDTH(MEM_DATA_WIDTH)  // bit
  ) u_axi_mem (
        .clk      (clk),
        .rst_n    (~rst)
      //AW
      , .i_awid   ({axi_mem_aw.awid, axi_mem_aw_extra_id_bit}),
        .i_awaddr (axi_mem_aw.awaddr),
        .i_awlen  (axi_mem_aw.awlen),
        .i_awsize (axi_mem_aw.awsize),
        .i_awburst(axi_mem_aw.awburst)   // INCR mode
      , .i_awvalid(axi_mem_awvalid),
        .o_awready(axi_mem_awready)
      //AR
      , .i_arid   ({axi_mem_ar.arid, axi_mem_ar_extra_id_bit}),
        .i_araddr (axi_mem_ar.araddr),
        .i_arlen  (axi_mem_ar.arlen),
        .i_arsize (axi_mem_ar.arsize),
        .i_arburst(axi_mem_ar.arburst),
        .i_arvalid(axi_mem_arvalid),
        .o_arready(axi_mem_arready)
      //W
      , .i_wdata  (axi_mem_w.wdata),
        .i_wstrb  (i_wstrb),
        .i_wlast  (axi_mem_w.wlast),
        .i_wvalid (axi_mem_wvalid),
        .o_wready (axi_mem_wready)
      //B
      , .o_bid    ({axi_mem_b.bid, axi_mem_b_extra_id_bit}),
        .o_bresp  (axi_mem_b.bresp),
        .o_bvalid (axi_mem_bvalid),
        .i_bready (axi_mem_bready)
      //R
      , .o_rid    ({axi_mem_r.rid, axi_mem_r_extra_id_bit}),
        .o_rdata  (axi_mem_r.dat),
        .o_rresp  (axi_mem_r.rresp),
        .o_rlast  (axi_mem_r.rlast),
        .o_rvalid (axi_mem_rvalid),
        .i_rready (axi_mem_rready)

  );

  
`endif

endmodule : testbench
