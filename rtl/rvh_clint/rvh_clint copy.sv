//      Memory map 
//------------------------------------------------------------------------------------------------------------
//      Reg         |   Offset Address          | Description
//------------------|---------------------------|-------------------------------------------------------------
//      msip(n)     |   0x0000 + 0x4 * n        | (n inside [0:N_CPU-1]
//      mtimecmp(n) |   0x4000 + 0x8 * n        | (n inside [0:N_CPU-1]
//      timer_en    |   0xbff0                  | timer enable signal ,default is 1'b0, means default is off.
//      mtime lo    |   0xbff8                  | 64-bit mtime low  32-bit,counting up if timer_en is 1'b1, will keep when timer_en is 1'b0
//      mtime hi    |   0xbffc                  | 64-bit mtime high 32-bit,counting up if timer_en is 1'b1, will keep when timer_en is 1'b0
//      gpio        |   0xc000                  | 32-bit gpio
//==========================================================================================================================================

module rvh_clint
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter               N_TIMERCMP              = 1,
    parameter               N_GPIO_BITS             = 32,
    parameter int unsigned  ID_WIDTH          = 0,
    parameter int unsigned  AXI_ADDR_WIDTH    = 0,
    parameter int unsigned  AXI_DATA_WIDTH    = 0,
    parameter int unsigned  AXI_USER_WIDTH    = 0,
    parameter logic [AXI_ADDR_WIDTH-1:0]  BASE_ADDR    = 0
)
(
    input                                       clk,
    input                                       rstn,
    
    // aw
    input wire [ID_WIDTH-1:0]                   slave_aw_id,
    input wire [AXI_ADDR_WIDTH-1:0] 	        slave_aw_addr,
    input wire [7:0] 	                        slave_aw_len,
    input wire [2:0] 	                        slave_aw_size,
    input wire [1:0] 	                        slave_aw_burst,
    input wire     		                        slave_aw_valid,
    output reg 	     	                        slave_aw_ready,
    // R
    input wire [ID_WIDTH-1:0]                   slave_ar_id,
    input wire [AXI_ADDR_WIDTH-1:0] 	        slave_ar_addr,
    input wire [7:0] 	                        slave_ar_len,
    input wire [2:0] 	                        slave_ar_size,
    input wire [1:0] 	                        slave_ar_burst,
    input wire 		                            slave_ar_valid,
    output reg 		                            slave_ar_ready,
    // W
    input wire [AXI_DATA_WIDTH-1:0] 	        slave_w_data,
    input wire [AXI_DATA_WIDTH/8-1:0] 	        slave_w_strb,
    input wire 		                            slave_w_last,
    input wire 		                            slave_w_valid,
    output reg 		                            slave_w_ready,
    // B
    output reg [ID_WIDTH-1:0]                   slave_b_id,
    output reg [1:0] 	                        slave_b_resp,
    output reg 		                            slave_b_valid,
    input wire 		                            slave_b_ready,
    // R
    output reg [ID_WIDTH-1:0]                   slave_r_id,
    output reg [AXI_DATA_WIDTH-1:0] 	        slave_r_data,
    output reg [1:0] 	                        slave_r_resp,
    output reg 		                            slave_r_last,
    output reg 		                            slave_r_valid,
    input wire 		                            slave_r_ready,


    output  [N_TIMERCMP-1:0]                    timer_int_o,
    output  [N_TIMERCMP-1:0]                    soft_int_o,
    // GPIO
    input   [N_GPIO_BITS-1:0]                   gpio_in, // N_GPIO_BITS <= DEBUG_DATA
    output  [N_GPIO_BITS-1:0]                   gpio_out
);

    localparam   SOFT_INT_OFFSET            = 32'h0000;//soft interrupt
    localparam   MTIMECMP_OFFSET            = 32'h4000;

    localparam   TIMER_EN_OFFSET            = 32'hbff0;
    localparam   MTIME_OFFSET               = 32'hbff8;


    logic                           ce;
    logic                           we;
    logic [AXI_DATA_WIDTH/8-1:0]    be;
    logic [AXI_DATA_WIDTH-1:0]      wdata;
    logic [AXI_DATA_WIDTH-1:0]      rdata;
    logic [AXI_ADDR_WIDTH-1:0]      addr;

    logic [AXI_DATA_WIDTH/8:0][15:0] addr_per_byte;

    logic [N_TIMERCMP-1:0]          soft_int_sel;
    logic [N_TIMERCMP-1:0]          soft_int_wdata;
    logic [N_TIMERCMP-1:0]          soft_int_q;

    logic [N_TIMERCMP-1:0][63:0]    mtimecmp_q;
    logic                           timer_en_q;
    logic [63:0]                    mtimer;

    

    logic [AXI_DATA_WIDTH-1:0] 	 w_biten;
    always_comb begin
        for (int i = 0; i < 64; i++) begin
            w_biten[i] = slave_w_strb[i / 8];
        end
    end

    axi2mem #(
        .ID_WIDTH          (ID_WIDTH      ),
        .AXI_ADDR_WIDTH    (AXI_ADDR_WIDTH),
        .AXI_DATA_WIDTH    (AXI_DATA_WIDTH),
        .AXI_USER_WIDTH    (AXI_USER_WIDTH)
    )axi2mem_u(
        .clk_i               (clk),   
        .rst_ni              (rstn),  
    //AW
        .slave_aw_id         (slave_aw_id   ),
        .slave_aw_addr       (slave_aw_addr ),
        .slave_aw_len        (slave_aw_len  ),
        .slave_aw_size       (slave_aw_size ),
        .slave_aw_burst      (slave_aw_burst),
        .slave_aw_valid      (slave_aw_valid),
        .slave_aw_ready      (slave_aw_ready),
    //AR
        .slave_ar_id         (slave_ar_id   ),
        .slave_ar_addr       (slave_ar_addr ),
        .slave_ar_len        (slave_ar_len  ),
        .slave_ar_size       (slave_ar_size ),
        .slave_ar_burst      (slave_ar_burst),
        .slave_ar_valid      (slave_ar_valid),
        .slave_ar_ready      (slave_ar_ready),
    //W
        .slave_w_data        (slave_w_data),
        .slave_w_strb        (w_biten),
        .slave_w_last        (slave_w_last ),
        .slave_w_valid       (slave_w_valid),
        .slave_w_ready       (slave_w_ready),
    //B
        .slave_b_id          (slave_b_id   ),
        .slave_b_resp        (slave_b_resp ),
        .slave_b_valid       (slave_b_valid),
        .slave_b_ready       (slave_b_ready),
    //R
        .slave_r_id          (slave_r_id   ),
        .slave_r_data        (slave_r_data ),
        .slave_r_resp        (slave_r_resp ),
        .slave_r_last        (slave_r_last ),
        .slave_r_valid       (slave_r_valid),
        .slave_r_ready       (slave_r_ready),

        .req_o               (ce),
        .we_o                (we),
        .addr_o              (addr),
        .be_o                (be),
        .data_o              (wdata),
        .data_i              (rdata)
    );

    always_comb begin
        for (int i = 0; i < AXI_DATA_WIDTH/8; i++) begin
            addr_per_byte[i] = ((addr - BASE_ADDR) & 0xffff) + i;
        end
    end



    always_ff @(posedge clk) begin
        if (rst) begin
            soft_int_q <= '0;
        end
        else if (ce & we) begin
            for (int i = 0; i < N_TIMERCMP; i++) begin
                for (int j = 0; j < AXI_DATA_WIDTH/8; j++) begin
                    if ((addr_per_byte[j] == (i * 4)) & be[j]) begin
                        soft_int_q[i] <= wdata[j*8];
                    end
                end
            end
        end
    end

    // timercmp
    always_ff @(posedge clk) begin
        if (rst) begin
            mtimecmp_q <= '0;
        end
        else if (ce & we) begin
            for (int i = 0; i < N_TIMERCMP; i++) begin
                for (int j = 0; j < AXI_DATA_WIDTH/8; j++) begin
                    if ((addr_per_byte[j] >= ('h4000 + i * 8)) & (addr_per_byte[j] < ('h4000 + i * 8 + 8)) & be[j]) begin
                        mtimecmp_q[i]
                    end

                end
                if (addr_internal == ) begin
                     <= wdata;
                end
            end
        end
    end

    // timer_en_q
    always_ff @(posedge clk) begin
        if (rst) begin
            timer_en_q <= '0;
        end
        else if (ce & we) begin
            if (addr_internal == 0xbff0) begin
                timer_en_q <= wdata[0];
            end
        end
    end

    // timer_en_q
    always_ff @(posedge clk) begin
        if (rst) begin
            timer_en_q <= '0;
        end
        else if (ce & we) begin
            if (addr_internal == 0xbff0) begin
                timer_en_q <= wdata[0];
            end
        end
    end


    always_comb begin
            for (int i = 0; i < N_TIMERCMP; i++) begin
                if (addr_internal == (i *4)) begin
                    soft_int_q[i] <= wdata[0];
                end
            end
    end





assign  soft_int_o          = rff_soft_int;
assign  gpio_out            = rff_gpio_out;
assign  debug_req_ready     = ~rff_debug_req_busy;
assign  debug_resp_valid    = rff_debug_req_busy;
assign  debug_resp      = dff_debug_resp;
assign  debug_req_taken = debug_req_valid & debug_req_ready;
assign  wr_byte_vld     = debug_req.req_paddr[2] ? {debug_req.req_mask,4'b0} : {4'b0,debug_req.req_mask};       // timer reg width = 64 but debug data width is 32.
// as in addr, 1 addr bit save 1 byte data; so for 64 bit data,hi 32 bit data addr[2]=1, and lo 32 bit data addr[2]=0.
assign  wr_data         = debug_req.req_paddr[2] ? {debug_req.req_data,32'h0} : {32'h0,debug_req.req_data};
assign  wr_byte_vld_gpio= debug_req.req_mask;


assign  debug_req_is_wr = (debug_req.req_type == REQ_WRITE);
assign  timer_en_en     = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == TIMER_EN_OFFSET);
assign  timer_en_rd_en  = timer_en_en & ~debug_req_is_wr;
assign  timer_en_wr_en  = timer_en_en & debug_req_is_wr;
assign  mtime_lo_en     = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == TIMER_MTIME_LO_OFFSET);
assign  mtime_lo_rd_en  = mtime_lo_en & ~debug_req_is_wr;
assign  mtime_lo_wr_en  = mtime_lo_en & debug_req_is_wr;
assign  mtime_hi_en     = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == TIMER_MTIME_HI_OFFSET);
assign  mtime_hi_rd_en  = mtime_hi_en & ~debug_req_is_wr;
assign  mtime_hi_wr_en  = mtime_hi_en & debug_req_is_wr;

assign  gpio_in_rd_en   = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == GPIO_IN_OFFSET)  & ~debug_req_is_wr;
assign  gpio_out_rd_en  = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == GPIO_OUT_OFFSET) & ~debug_req_is_wr;
assign  gpio_out_wr_en  = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == (GPIO_OUT_OFFSET)) & debug_req_is_wr;


// debug read data generate
assign  debug_rd_en[0]  = timer_en_rd_en;
assign  debug_rd_en[1]  = mtime_lo_rd_en;
assign  debug_rd_en[2]  = mtime_hi_rd_en;
assign  debug_rd_en[3]  = gpio_in_rd_en;
assign  debug_rd_en[4]  = gpio_out_rd_en;
assign  debug_rdata[0]  = rff_timer_en;
assign  debug_rdata[1]  = rff_mtime[0+:32];
assign  debug_rdata[2]  = rff_mtime[32+:32];
assign  debug_rdata[3]  = rff_gpio_in;
assign  debug_rdata[4]  = rff_gpio_out;
generate
for(i=0;i<(TIMER_WIDTH/8);i++)begin : GEN_WR_BIT_VLD
    assign  wr_bit_vld[8*i+:8]  = {8{wr_byte_vld[i]}};
end
for(i=0;i<(N_GPIO_BITS/8);i++)begin : GEN_WR_BIT_VLD_GPIO
    assign  wr_bit_vld_gpio[8*i+:8]  = {8{wr_byte_vld_gpio[i]}};
end

for(i=0;i<N_TIMERCMP;i++)begin : GEN_MTIMECMP
    assign  soft_int_en[i]      = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == i*4+SOFT_INT_OFFSET);
    assign  soft_int_rd_en[i]   = soft_int_en[i]& ~debug_req_is_wr;
    assign  soft_int_wr_en[i]   = soft_int_en[i]&  debug_req_is_wr;
    assign  mtimecmp_lo_en[i]   = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == i*8 + TIMER_MTIMECMP_LO_OFFSET);
    assign  mtimecmp_lo_rd_en[i]= mtimecmp_lo_en[i] & ~debug_req_is_wr;
    assign  mtimecmp_lo_wr_en[i]= mtimecmp_lo_en[i] & debug_req_is_wr;
    assign  mtimecmp_hi_en[i]   = debug_req_taken & ({debug_req.req_paddr[EFF_ADDR_MSB:EFF_ADDR_LSB],2'b0} == i*8 + TIMER_MTIMECMP_HI_OFFSET);
    assign  mtimecmp_hi_rd_en[i]= mtimecmp_hi_en[i] & ~debug_req_is_wr;
    assign  mtimecmp_hi_wr_en[i]= mtimecmp_hi_en[i] & debug_req_is_wr;
    assign  debug_rd_en[5+3*i]  = soft_int_rd_en[i];
    assign  debug_rd_en[5+3*i+1]= mtimecmp_lo_rd_en[i];
    assign  debug_rd_en[5+3*i+2]= mtimecmp_hi_rd_en[i];
    assign  debug_rdata[5+3*i]  = {31'h0,rff_soft_int[i]};
    assign  debug_rdata[5+3*i+1]= rff_mtimecmp[i][0+:32];
    assign  debug_rdata[5+3*i+2]= rff_mtimecmp[i][32+:32];
end

// rdata select & generate
for(i=0;i<DEBUG_RDATA_NUM;i++)begin : GEN_DEBUG_RDATA
    if(i==0)begin
        assign  debug_rdata_history[i]  = {DEBUG_DATA_WIDTH{debug_rd_en[i]}} & debug_rdata[i];
    end
    else begin
        assign  debug_rdata_history[i]  = {DEBUG_DATA_WIDTH{debug_rd_en[i]}} & debug_rdata[i] | debug_rdata_history[i-1];
    end
end
endgenerate
assign  rdata_mux   = debug_rdata_history[DEBUG_RDATA_NUM-1];

always_ff@(posedge clk)begin
    if(debug_req_taken)begin
        dff_debug_resp.resp_type<= debug_req.req_type;
        dff_debug_resp.resp_tid <= debug_req.req_tid;
        dff_debug_resp.resp_data<= rdata_mux;
    end
end

//memory maped register
always_ff @(posedge clk or negedge rstn) begin
  if (~rstn) begin
    rff_gpio_in <= '0;
  end else if (rff_gpio_in != gpio_in) begin
    rff_gpio_in <= gpio_in;
  end
end

always_ff @(posedge clk or negedge rstn) begin
  if (~rstn) begin
    rff_gpio_out <= '0;
  end else if (gpio_out_wr_en) begin
    rff_gpio_out <= (rff_gpio_out & ~wr_bit_vld_gpio) | (debug_req.req_data & wr_bit_vld_gpio);
  end
end

always_ff@(posedge clk or negedge rstn)begin
    if(~rstn)begin
        rff_debug_req_busy  <= 1'b0;
    end
    else if(debug_req_valid & debug_req_ready)begin
        rff_debug_req_busy  <= 1'b1;
    end
    else if(debug_resp_valid & debug_resp_ready)begin
        rff_debug_req_busy  <= 1'b0;
    end
end
//memory map register 
always_ff @(posedge clk or negedge rstn)begin
    if(~rstn)begin
        rff_timer_en    <= 1'b0;
    end
    else if(timer_en_wr_en)begin
        rff_timer_en    <= debug_req.req_data[0];
    end
end

always_ff @(posedge clk or negedge rstn)begin
    if(~rstn)begin
        rff_mtime   <= '0;
    end
    else if(mtime_hi_wr_en | mtime_lo_wr_en)begin
        rff_mtime   <= rff_mtime & ~wr_bit_vld | wr_data & wr_bit_vld;
    end
    else if(rff_timer_en)begin
        rff_mtime   <= mtime_out;
    end
end
generate
for(i=0;i<N_TIMERCMP;i++)begin : GEN_RFFTIMERCMP
    //soft interrupt, only lsb is valid
    always_ff @(posedge clk or negedge rstn)begin
        if(~rstn)begin
            rff_soft_int[i]    <= '0;
        end
        else if(soft_int_wr_en[i])begin
            rff_soft_int[i]    <= debug_req.req_data[0];
        end
    end
    always_ff @(posedge clk or negedge rstn)begin
        if(~rstn)begin
            rff_mtimecmp[i]    <= '1;
        end
        else if(mtimecmp_hi_wr_en[i] | mtimecmp_lo_wr_en[i])begin
            rff_mtimecmp[i]    <= rff_mtimecmp[i] & ~wr_bit_vld | wr_data & wr_bit_vld;
        end
    end
end
endgenerate

assign mtime_out = rff_mtime + {{(TIMER_WIDTH-1){1'b0}}, 1'b1};

generate
for (genvar i=0; i<N_TIMERCMP; i++) begin: TIMER_INTS
    assign timer_int_internal[i] = (unsigned'(rff_mtime) >= unsigned'(rff_mtimecmp[i]));
end
endgenerate

std_dffr #( N_TIMERCMP ) ff_timer_int_u(.clk( clk ),.rstn( rstn ),.d( timer_int_internal ),.q( timer_int_o ));

endmodule
