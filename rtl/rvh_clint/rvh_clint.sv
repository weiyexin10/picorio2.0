//      Memory map 
//------------------------------------------------------------------------------------------------------------
//      Reg         |   Offset Address          | Description
//------------------|---------------------------|-------------------------------------------------------------
//      msip(n)     |   0x0000 + 0x4 * n        | (n inside [0:N_CPU-1]
//      mtimecmp(n) |   0x4000 + 0x8 * n        | (n inside [0:N_CPU-1]
//      timer_en    |   0xbff0                  | timer enable signal ,default is 1'b0, means default is off.
//      mtime lo    |   0xbff8                  | 64-bit mtime low  32-bit,counting up if timer_en is 1'b1, will keep when timer_en is 1'b0
//      mtime hi    |   0xbffc                  | 64-bit mtime high 32-bit,counting up if timer_en is 1'b1, will keep when timer_en is 1'b0
//==========================================================================================================================================

module rvh_clint
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned  ID_WIDTH          = 0,
    parameter int unsigned  AXI_ADDR_WIDTH    = 0,
    parameter int unsigned  AXI_DATA_WIDTH    = 0,
    parameter int unsigned  AXI_USER_WIDTH    = 0,
    parameter logic [AXI_ADDR_WIDTH-1:0]  BASE_ADDR    = 0
)
(
    input                                       clk,
    input                                       rstn,
    
    // aw
    input  logic [ID_WIDTH-1:0]                   slave_aw_id,
    input  logic [AXI_ADDR_WIDTH-1:0] 	        slave_aw_addr,
    input  logic [7:0] 	                        slave_aw_len,
    input  logic [2:0] 	                        slave_aw_size,
    input  logic [1:0] 	                        slave_aw_burst,
    input  logic     		                        slave_aw_valid,
    output logic 	     	                        slave_aw_ready,
    // R
    input  logic [ID_WIDTH-1:0]                   slave_ar_id,
    input  logic [AXI_ADDR_WIDTH-1:0] 	        slave_ar_addr,
    input  logic [7:0] 	                        slave_ar_len,
    input  logic [2:0] 	                        slave_ar_size,
    input  logic [1:0] 	                        slave_ar_burst,
    input  logic 		                            slave_ar_valid,
    output logic 		                            slave_ar_ready,
    // W
    input  logic [AXI_DATA_WIDTH-1:0] 	        slave_w_data,
    input  logic [AXI_DATA_WIDTH/8-1:0] 	        slave_w_strb,
    input  logic 		                            slave_w_last,
    input  logic 		                            slave_w_valid,
    output logic 		                            slave_w_ready,
    // B
    output logic [ID_WIDTH-1:0]                   slave_b_id,
    output logic [1:0] 	                        slave_b_resp,
    output logic 		                            slave_b_valid,
    input  logic 		                            slave_b_ready,
    // R
    output logic [ID_WIDTH-1:0]                   slave_r_id,
    output logic [AXI_DATA_WIDTH-1:0] 	        slave_r_data,
    output logic [1:0] 	                        slave_r_resp,
    output logic 		                            slave_r_last,
    output logic 		                            slave_r_valid,
    input  logic 		                            slave_r_ready,


    output logic                                timer_int_o,
    output logic                                soft_int_o
);

    logic                           ce;
    logic                           we;
    logic [AXI_DATA_WIDTH/8-1:0]    be;
    logic [AXI_DATA_WIDTH-1:0]      wdata;
    logic [AXI_DATA_WIDTH-1:0]      rdata;
    logic [AXI_ADDR_WIDTH-1:0]      addr;
    logic [15:0]                    offset;

    logic                           soft_int_q;
    logic [63:0]                    mtimecmp_q;
    // logic                           timer_en_q;
    logic [63:0]                    mtimer_q;

    

    logic [AXI_DATA_WIDTH-1:0] 	 w_biten;
    always_comb begin
        for (int i = 0; i < 64; i++) begin
            w_biten[i] = slave_w_strb[i / 8];
        end
    end

    axi2mem #(
        .ID_WIDTH          (ID_WIDTH      ),
        .AXI_ADDR_WIDTH    (AXI_ADDR_WIDTH),
        .AXI_DATA_WIDTH    (AXI_DATA_WIDTH),
        .AXI_USER_WIDTH    (AXI_USER_WIDTH)
    )axi2mem_u(
        .clk_i               (clk),   
        .rst_ni              (rstn),  
    //AW
        .slave_aw_id         (slave_aw_id   ),
        .slave_aw_addr       (slave_aw_addr ),
        .slave_aw_len        (slave_aw_len  ),
        .slave_aw_size       (slave_aw_size ),
        .slave_aw_burst      (slave_aw_burst),
        .slave_aw_valid      (slave_aw_valid),
        .slave_aw_ready      (slave_aw_ready),
    //AR
        .slave_ar_id         (slave_ar_id   ),
        .slave_ar_addr       (slave_ar_addr ),
        .slave_ar_len        (slave_ar_len  ),
        .slave_ar_size       (slave_ar_size ),
        .slave_ar_burst      (slave_ar_burst),
        .slave_ar_valid      (slave_ar_valid),
        .slave_ar_ready      (slave_ar_ready),
    //W
        .slave_w_data        (slave_w_data),
        .slave_w_strb        (w_biten),
        .slave_w_last        (slave_w_last ),
        .slave_w_valid       (slave_w_valid),
        .slave_w_ready       (slave_w_ready),
    //B
        .slave_b_id          (slave_b_id   ),
        .slave_b_resp        (slave_b_resp ),
        .slave_b_valid       (slave_b_valid),
        .slave_b_ready       (slave_b_ready),
    //R
        .slave_r_id          (slave_r_id   ),
        .slave_r_data        (slave_r_data ),
        .slave_r_resp        (slave_r_resp ),
        .slave_r_last        (slave_r_last ),
        .slave_r_valid       (slave_r_valid),
        .slave_r_ready       (slave_r_ready),

        .req_o               (ce),
        .we_o                (we),
        .addr_o              (addr),
        .be_o                (be),
        .data_o              (wdata),
        .data_i              (rdata)
    );

    assign offset = ((addr - BASE_ADDR) & 'hffff);

    always_ff @(posedge clk) begin
        if (~rstn) begin
            soft_int_q <= '0;
        end
        else if (ce & we) begin
            if (offset == 0) begin
                soft_int_q <= wdata[0];
            end
        end
    end

    // timercmp
    always_ff @(posedge clk) begin
        if (~rstn) begin
            mtimecmp_q <= 64'hffffffff_ffffffff;
        end
        else if (ce & we) begin
            if (offset == 'h4000) begin
                for (int i = 0; i < 8; i++) begin
                    if (be[i]) begin
                        mtimecmp_q[i*8+:8] <= wdata[i*8+:8];
                    end
                end
            end
        end
    end

    // // timer_en_q
    // always_ff @(posedge clk) begin
    //     if (~rstn) begin
    //         timer_en_q <= '0;
    //     end
    //     else if (ce & we) begin
    //         if (offset == 'hbff0) begin
    //             timer_en_q <= wdata[0];
    //         end
    //     end
    // end

    // mtime
    always_ff @(posedge clk) begin
        if (~rstn) begin
            mtimer_q <= '0;
        end
        else if (ce & we) begin
            if (offset == 'hbff8) begin
                for (int i = 0; i < 8; i++) begin
                    if (be[i]) begin
                        mtimer_q[i*8+:8] <= wdata[i*8+:8];
                    end
                end
            end
        end
        // else if (timer_en_q) begin
        else begin
            mtimer_q <= mtimer_q + 1'b1;
        end
    end

// read
    always_comb begin
        if ((offset & ~('b111)) == 0) begin
            rdata = soft_int_q;
        end
        else if ((offset & ~('b111)) == 'h4000) begin
            rdata = mtimecmp_q;
        end
        // else if (offset == 'hbff0) begin
        //     rdata = timer_en_q;
        // end
        else if ((offset & ~('b111)) == 'hbff8) begin
            rdata = mtimer_q;
        end
        else begin
            rdata = 0;
        end
    end

    always_ff @(posedge clk) begin
        if (~rstn) begin
            timer_int_o <= '0;
        end
        // else if (timer_en_q) begin
        else begin
            timer_int_o <= (mtimer_q >= mtimecmp_q);
        end
    end 

    assign soft_int_o = soft_int_q;



endmodule
