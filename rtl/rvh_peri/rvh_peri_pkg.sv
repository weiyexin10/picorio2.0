`ifndef __RVH_PERI_PKG_SV__
`define __RVH_PERI_PKG_SV__
package rvh_peri_pkg;
import rvh_pkg::*;
import uop_encoding_pkg::*;

  typedef struct packed {
    logic[63:0]     paddr;   // same as AXI4-Lite
    logic[2:0]      pprot;   // same as AXI4-Lite, specification is the same
    logic           psel;    // one request line per connected APB4 slave
    logic           penable; // enable signal shows second APB4 cycle
    logic           pwrite;  // write enable
    logic[31:0]     pwdata;  // write data, comes from W channel
    logic[3:0]      pstrb;   // write strb, comes from W channel
  } apb_req_t;

  typedef struct packed {
    logic           pready;   // slave signals that it is ready
    logic [31:0]    prdata;   // read data, connects to R channel
    logic           pslverr;  // gets translated into either `axi_pkg::RESP_OK` or `axi_pkg::RESP_SLVERR`
  } apb_resp_t;


endpackage

`endif 