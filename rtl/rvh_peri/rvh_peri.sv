module rvh_peri
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_l1d_pkg::*;
  import rvh_peri_pkg::*;
#(
    parameter int unsigned AXI4_ADDRESS_WIDTH = 64,
    parameter int unsigned AXI4_RDATA_WIDTH   = 64,
    parameter int unsigned AXI4_WDATA_WIDTH   = 64,
    parameter int unsigned AXI4_ID_WIDTH      = 0,
    parameter int unsigned AXI4_USER_WIDTH    = 0,
    parameter int unsigned AXI_NUMBYTES       = AXI4_WDATA_WIDTH/8,

    parameter int unsigned BUFF_DEPTH_SLAVE   = 4,
    parameter int unsigned APB_NUM_SLAVES     = 1,
    parameter int unsigned APB_ADDR_WIDTH     = 64
)(
    // ---------------------------------------------------------
    // AXI TARG Port Declarations ------------------------------
    // ---------------------------------------------------------
    //AXI write address bus -------------- // USED// -----------
    input  logic [AXI4_ID_WIDTH-1:0]       AWID_i     ,
    input  logic [AXI4_ADDRESS_WIDTH-1:0]  AWADDR_i   ,
    input  logic [ 7:0]                    AWLEN_i    ,
    input  logic [ 2:0]                    AWSIZE_i   ,
    input  logic [ 1:0]                    AWBURST_i  ,
    input  logic                           AWLOCK_i   ,
    input  logic [ 3:0]                    AWCACHE_i  ,
    input  logic [ 2:0]                    AWPROT_i   ,
    input  logic [ 3:0]                    AWREGION_i ,
    input  logic [ AXI4_USER_WIDTH-1:0]    AWUSER_i   ,
    input  logic [ 3:0]                    AWQOS_i    ,
    input  logic                           AWVALID_i  ,
    output logic                           AWREADY_o  ,
    // ---------------------------------------------------------

    //AXI write data bus -------------- // USED// --------------
    input  logic [AXI_NUMBYTES-1:0][7:0]   WDATA_i    ,
    input  logic [AXI_NUMBYTES-1:0]        WSTRB_i    ,
    input  logic                           WLAST_i    ,
    input  logic [AXI4_USER_WIDTH-1:0]     WUSER_i    ,
    input  logic                           WVALID_i   ,
    output logic                           WREADY_o   ,
    // ---------------------------------------------------------

    //AXI write response bus -------------- // USED// ----------
    output logic   [AXI4_ID_WIDTH-1:0]     BID_o      ,
    output logic   [ 1:0]                  BRESP_o    ,
    output logic                           BVALID_o   ,
    output logic   [AXI4_USER_WIDTH-1:0]   BUSER_o    ,
    input  logic                           BREADY_i   ,
    // ---------------------------------------------------------

    //AXI read address bus -------------------------------------
    input  logic [AXI4_ID_WIDTH-1:0]       ARID_i     ,
    input  logic [AXI4_ADDRESS_WIDTH-1:0]  ARADDR_i   ,
    input  logic [ 7:0]                    ARLEN_i    ,
    input  logic [ 2:0]                    ARSIZE_i   ,
    input  logic [ 1:0]                    ARBURST_i  ,
    input  logic                           ARLOCK_i   ,
    input  logic [ 3:0]                    ARCACHE_i  ,
    input  logic [ 2:0]                    ARPROT_i   ,
    input  logic [ 3:0]                    ARREGION_i ,
    input  logic [ AXI4_USER_WIDTH-1:0]    ARUSER_i   ,
    input  logic [ 3:0]                    ARQOS_i    ,
    input  logic                           ARVALID_i  ,
    output logic                           ARREADY_o  ,


    //AXI read data bus
    output  logic [AXI4_ID_WIDTH-1:0]      RID_o      ,
    output  logic [AXI4_RDATA_WIDTH-1:0]   RDATA_o    ,
    output  logic [ 1:0]                   RRESP_o    ,
    output  logic                          RLAST_o    ,
    output  logic [AXI4_USER_WIDTH-1:0]    RUSER_o    ,
    output  logic                          RVALID_o   ,
    input   logic                          RREADY_i   ,

//  peripheral io
    input logic                             uart_rx_i,      // Receiver input
    output logic                            uart_tx_o,      // Transmitter output
    output logic                            uart_int_o,

    input clk,
    input rst
);

typedef struct packed {
    addr_t          paddr;   // same as AXI4-Lite
    logic[2:0]      pprot;   // same as AXI4-Lite, specification is the same
    logic           psel;    // one request line per connected APB4 slave
    logic           penable; // enable signal shows second APB4 cycle
    logic           pwrite;  // write enable
    logic[31:0]     pwdata;  // write data, comes from W channel
    logic[3:0]      pstrb;   // write strb, comes from W channel
  } apb_req_t;

  typedef struct packed {
    logic           pready;   // slave signals that it is ready
    logic [31:0]    prdata;   // read data, connects to R channel
    logic           pslverr;  // gets translated into either `axi_pkg::RESP_OK` or `axi_pkg::RESP_SLVERR`
  } apb_resp_t;

    apb_req_t   apb_req;
    apb_resp_t  apb_resp;

rvh_axi2apb #(
    .AXI4_ADDRESS_WIDTH (AXI4_ADDRESS_WIDTH),
    .AXI4_RDATA_WIDTH   (AXI4_RDATA_WIDTH),
    .AXI4_WDATA_WIDTH   (AXI4_WDATA_WIDTH),
    .AXI4_ID_WIDTH      (AXI4_ID_WIDTH),
    .AXI4_USER_WIDTH    (AXI4_USER_WIDTH),

    .BUFF_DEPTH_SLAVE   (BUFF_DEPTH_SLAVE),
    .APB_NUM_SLAVES     (APB_NUM_SLAVES),
    .APB_ADDR_WIDTH     (APB_ADDR_WIDTH)
)
(
    .ACLK                       (clk),
    .ARESETn                    (~rst),
    .test_en_i                  ('0),
 
    .AWID_i                     (AWID_i    ),
    .AWADDR_i                   (AWADDR_i  ),
    .AWLEN_i                    (AWLEN_i   ),
    .AWSIZE_i                   (AWSIZE_i  ),
    .AWBURST_i                  (AWBURST_i ),
    .AWLOCK_i                   (AWLOCK_i  ),
    .AWCACHE_i                  (AWCACHE_i ),
    .AWPROT_i                   (AWPROT_i  ),
    .AWREGION_i                 (AWREGION_i),
    .AWUSER_i                   (AWUSER_i  ),
    .AWQOS_i                    (AWQOS_i   ),
    .AWVALID_i                  (AWVALID_i ),
    .AWREADY_o                  (AWREADY_o ),
    // ---------------------------------------------------------

    //AXI write data bus -------------- // USED// --------------
    .WDATA_i                    (WDATA_i ),
    .WSTRB_i                    (WSTRB_i ),
    .WLAST_i                    (WLAST_i ),
    .WUSER_i                    (WUSER_i ),
    .WVALID_i                   (WVALID_i),
    .WREADY_o                   (WREADY_o),
    // ---------------------------------------------------------

    //AXI write response bus -------------- // USED// ----------
    .BID_o                      (BID_o   ),
    .BRESP_o                    (BRESP_o ),
    .BVALID_o                   (BVALID_o),
    .BUSER_o                    (BUSER_o ),
    .BREADY_i                   (BREADY_i),
    // ---------------------------------------------------------

    //AXI read address bus -------------------------------------
    .ARID_i                     (ARID_i   ),
    .ARADDR_i                   (ARADDR_i ),
    .ARLEN_i                    (ARLEN_i  ),
    .ARSIZE_i                   (ARSIZE_i ),
    .ARBURST_i                  (ARBURST_i),
    .ARLOCK_i                   (ARLOCK_i ),
    .ARCACHE_i                  (ARCACHE_i),
    .ARPROT_i                   (ARPROT_i ),
    .ARREGION_i                 (ARREGION_),
    .ARUSER_i                   (ARUSER_i ),
    .ARQOS_i                    (ARQOS_i  ),
    .ARVALID_i                  (ARVALID_i),
    .ARREADY_o                  (ARREADY_o),
    // ---------------------------------------------------------

    //AXI read data bus ----------------------------------------
    .RID_o                      (RID_o   ),
    .RDATA_o                    (RDATA_o ),
    .RRESP_o                    (RRESP_o ),
    .RLAST_o                    (RLAST_o ),
    .RUSER_o                    (RUSER_o ),
    .RVALID_o                   (RVALID_o),
    .RREADY_i                   (RREADY_i),
    // ---------------------------------------------------------

    .PENABLE                    (apb_req.penable),
    .PWRITE                     (apb_req.pwrite),
    .PADDR                      (apb_req.paddr),
    .PSEL                       ( apb_req.psel        ),
    .PWDATA                     ( apb_req.pwdata      ),
    .PRDATA                     ( apb_resp.prdata      ),
    .PREADY                     ( apb_resp.pready      ),
    .PSLVERR                    ( apb_resp.pslverr     )
);


    apb_uart_sv #(
    .APB_ADDR_WIDTH(APB_ADDR_WIDTH)  //APB slaves are 4KB by default
    )
    (
        .CLK        (clk),
        .RSTN       (~rst),

        .PADDR      (apb_req.paddr),

        .PWDATA     (apb_req.pwdata),
        .PWRITE     (apb_req.pwrite),
        .PSEL       (apb_req.psel),
        .PENABLE    (apb_req.penable),
        .PRDATA     (apb_resp.prdata),
        .PREADY     (apb_resp.pready),
        .PSLVERR    (apb_resp.pslverr),

        .rx_i       (uart_rx_i),      // Receiver input
        .tx_o       (uart_tx_o),      // Transmitter output

        .event_o    (uart_int_o)    // interrupt/event output
    );





endmodule
