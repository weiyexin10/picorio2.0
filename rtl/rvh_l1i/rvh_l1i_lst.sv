module rvh_l1i_lst
    import rvh_pkg::*;
    import rvh_l1i_pkg::*;
#(
     parameter entry_num=32
    ,parameter entry_idx=$clog2(entry_num)
    ,parameter way_num=4
    ,parameter way_idx=$clog2(way_num)
)
(
     input logic  clk
    ,input logic  rstn
    
    ,input logic                                    lst_mesi_wr_en_s0_req
    ,input logic [L1I_BANK_SET_INDEX_WIDTH-1:0]     lst_mesi_wr_set_idx_s0_req
    ,input logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]     lst_mesi_wr_way_idx_s0_req
    ,input rrv64_mesi_type_e                        lst_mesi_wr_dat_s0_req
    
    ,input logic  [L1I_BANK_SET_INDEX_WIDTH-1:0]    lst_rd_idx_s0_req
    ,output rrv64_l1i_lst_t                         lst_rd_dat_s0_req
    
    ,input logic  [L1I_BANK_SET_INDEX_WIDTH-1:0]    lst_rd_idx_mlfb_peek
    ,input logic  [entry_idx-1:0]                   lst_avail_way_rd_idx_mlfb_peek
    ,output rrv64_l1i_lst_t                         lst_rd_dat_mlfb_peek
    ,output logic [way_idx-1:0]                     lst_avail_way_rd_dat_mlfb_peek

    ,input  logic                                   fencei_flush_i // fencei flush, invalidate all cache line
);

rrv64_l1i_lst_t[entry_num-1:0]                                              lst_entry; // a lst_entry_mesi_way wrapper
logic[entry_num-1:0][L1I_BANK_WAY_NUM-1:0][$bits(rrv64_mesi_type_e)-1:0]    lst_entry_mesi_way; // lst entity

logic[entry_num -1:0][L1I_BANK_WAY_NUM-1:0]                                 mesi_wr_set_en;
rrv64_mesi_type_e[entry_num-1:0][L1I_BANK_WAY_NUM-1:0]                      mesi_wr_dat;
logic[way_num -1:0]                                                         set_avail_bit_list_mlfb_peek;

logic clk_lst, clk_lst_en;
assign clk_lst_en = lst_mesi_wr_en_s0_req | fencei_flush_i;
rrv64_cell_clkgate U_ICG_LST (.clk_i(clk) ,.clk_enable_i(clk_lst_en) ,.clk_senable_i('0),.clk_gated_o(clk_lst));

genvar ii,jj;
generate
for(ii=0; ii<entry_num; ii++)begin:GEN_RRV64_L1I_LST_SET
    //wr
    for(jj=0; jj<L1I_BANK_WAY_NUM; jj++)begin:GEN_RRV64_L1I_LST_WAY
        //mesi
        assign mesi_wr_set_en[ii][jj] = lst_mesi_wr_en_s0_req & (lst_mesi_wr_set_idx_s0_req ==ii) & (lst_mesi_wr_way_idx_s0_req==jj) | fencei_flush_i;
        assign mesi_wr_dat[ii][jj] = fencei_flush_i ? INVALID : lst_mesi_wr_dat_s0_req;
        std_dffre #(.WIDTH($bits(rrv64_mesi_type_e))) U_RRV64_L1I_LST_MESI_WAY (.clk(clk_lst), .rstn(rstn), .en(mesi_wr_set_en[ii][jj]), .d(mesi_wr_dat[ii][jj]), .q(lst_entry_mesi_way[ii][jj]));
        assign lst_entry[ii].mesi_sta[jj] = {lst_entry_mesi_way[ii][jj]};
    end
end
endgenerate

assign lst_rd_dat_s0_req    = lst_entry[lst_rd_idx_s0_req];
assign lst_rd_dat_mlfb_peek = lst_entry[lst_rd_idx_mlfb_peek];

//peek avail entry
genvar kk;
generate
    for(kk=0; kk<way_num; kk++)begin:GEN_VALID_BIT
        assign set_avail_bit_list_mlfb_peek[kk]=(lst_entry[lst_avail_way_rd_idx_mlfb_peek].mesi_sta[kk] == INVALID);
    end
endgenerate


always_comb begin
    lst_avail_way_rd_dat_mlfb_peek = '0;
    for(int i = 0; i < way_num; i++) begin
        if(set_avail_bit_list_mlfb_peek[i] == 1'b1) begin
            lst_avail_way_rd_dat_mlfb_peek = i[way_idx-1:0];
        end
    end
end

endmodule