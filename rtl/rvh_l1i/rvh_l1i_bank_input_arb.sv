module rvh_l1i_bank_input_arb
    import riscv_pkg::*;
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_l1i_pkg::*;
#(
  parameter int unsigned        N_ARB_IN_PORT  = 2,
  parameter int unsigned    FETCH_PORT_IDX  = 0,
  parameter int unsigned    PREFETCH_PORT_IDX  = FETCH_PORT_IDX == 0 ? 1 : 0,
  parameter int unsigned    N_ARB_OUT_PORT = L1I_BANK_ID_NUM
)
(
// input from lsu
    // LS Pipe -> D$ : Load request
    input  logic [N_ARB_IN_PORT-1:0]                         if_l1i_if_req_vld_i,
    input  logic [N_ARB_IN_PORT-1:0][FTQ_TAG_LEN-1:0]        if_l1i_if_req_if_tag_i,
`ifdef RUBY
    input  logic [N_ARB_IN_PORT-1:0][RRV64_LSU_ID_WIDTH -1:0] if_l1i_if_req_lsu_tag_i,
`endif
    // input  logic [N_ARB_IN_PORT-1:0][   L1I_INDEX_WIDTH-1:0] if_l1i_if_req_index_i,
    input  logic [N_ARB_IN_PORT-1:0][   L1I_INDEX_WIDTH-1:0] if_l1i_if_req_idx_i, //
    input  logic [N_ARB_IN_PORT-1:0][  L1I_OFFSET_WIDTH-1:0] if_l1i_if_req_offset_i, //
    input  logic [N_ARB_IN_PORT-1:0][     L1I_TAG_WIDTH-1:0] if_l1i_if_req_vtag_i, // vtag
    output logic [N_ARB_IN_PORT-1:0]                         if_l1i_if_req_rdy_o,
    output logic [N_ARB_IN_PORT-1:0][  L1I_BANK_ID_INDEX_WIDTH-1:0] if_l1i_if_req_hit_bank_id_o,

// output to l1i cache banks
  // FETCH -> I$ : IFETCH Request
    output logic [N_ARB_OUT_PORT-1:0]                          l1i_bank_if_req_vld_o,
    output logic [N_ARB_OUT_PORT-1:0][ FTQ_TAG_LEN-1:0]        l1i_bank_if_req_if_tag_o,
`ifdef RUBY
    output logic [N_ARB_OUT_PORT-1:0][RRV64_LSU_ID_WIDTH -1:0] l1i_bank_if_req_lsu_tag_o,
`endif

    output logic [N_ARB_OUT_PORT-1:0][L1I_BANK_SET_INDEX_WIDTH-1:0 ]  l1i_bank_if_req_idx_o,
    output logic [N_ARB_OUT_PORT-1:0][L1I_BANK_OFFSET_WIDTH-1:0]      l1i_bank_if_req_offset_o,
    output logic [N_ARB_OUT_PORT-1:0][L1I_BANK_TAG_WIDTH-1:0]         l1i_bank_if_req_vtag_o,
    output logic [N_ARB_OUT_PORT-1:0]                                 l1i_bank_if_req_is_prefetch_o,

    input  logic [N_ARB_OUT_PORT-1:0]                                 l1i_bank_if_req_fetch_rdy_i,
    input  logic [N_ARB_OUT_PORT-1:0]                                 l1i_bank_if_req_prefetch_rdy_i,
  
  // PFB -> fetch
    input  logic [N_ARB_OUT_PORT-1:0]                                 fetch_l1i_if_prefetch_hit_i,
    input  logic [N_ARB_OUT_PORT-1:0]                                 fetch_l1i_if_fetch_inflight_i,
    input  logic [N_ARB_OUT_PORT-1:0]                                 fetch_l1i_if_fetch_hit_when_req_i,
    input  logic [N_ARB_OUT_PORT-1:0]                                 fetch_l1i_if_fetch_hit_when_fill_i,

    output  logic [N_ARB_IN_PORT-1:0]                                 fetch_l1i_if_prefetch_hit_o,
    output  logic [N_ARB_IN_PORT-1:0]                                 fetch_l1i_if_fetch_hit_o,
    output  logic [N_ARB_IN_PORT-1:0]                                 fetch_l1i_if_fetch_inflight_o,
  // ITLB -> I$
    // LS Pipe -> D$ : DTLB response
    input  logic [N_ARB_IN_PORT-1:0]                                  if_l1i_itlb_resp_vld_i,
    input  logic [N_ARB_IN_PORT-1:0][         PPN_WIDTH-1:0]          if_l1i_itlb_resp_ppn_i,
    input  logic [N_ARB_IN_PORT-1:0]                                  if_l1i_itlb_resp_excp_vld_i,
    input  logic [N_ARB_IN_PORT-1:0]                                  if_l1i_itlb_resp_hit_i,

    output logic [N_ARB_OUT_PORT-1:0]                                 itlb_l1i_resp_vld_o,
    output logic [N_ARB_OUT_PORT-1:0]                                 itlb_l1i_resp_excp_vld_o, // s1 kill
    output logic [N_ARB_OUT_PORT-1:0]                                 itlb_l1i_resp_hit_o,      // s1 kill
    output logic [N_ARB_OUT_PORT-1:0][       PPN_WIDTH-1:0]           itlb_l1i_resp_ppn_o, // VIPT, get at s1 if tlb hit

    input clk,
    input rst
);
  
    genvar i, j;
    // 1. divide the lsu req to cache bank req by bank id
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0]                         if_l1i_input_arb_if_req_vld;
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0][     FTQ_TAG_LEN-1:0]   if_l1i_input_arb_if_req_if_tag;
`ifdef RUBY
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0][RRV64_LSU_ID_WIDTH -1:0] if_l1i_input_arb_if_req_lsu_tag;
`endif
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0][   L1I_INDEX_WIDTH-1:0] if_l1i_input_arb_if_req_idx;
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0][  L1I_OFFSET_WIDTH-1:0] if_l1i_input_arb_if_req_offset;
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0][     L1I_TAG_WIDTH-1:0] if_l1i_input_arb_if_req_vtag;

    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0]                         if_l1i_input_arb_if_req_rdy;

    logic [N_ARB_IN_PORT-1:0][L1I_BANK_ID_INDEX_WIDTH-1:0]                if_req_bank_id, if_req_bank_id_ff;
    logic [N_ARB_IN_PORT-1:0]                                             lsu_pipe_if_req_hsk;

    generate for(i = 0; i < N_ARB_IN_PORT; i++) begin
            assign if_req_bank_id[i] = if_l1i_if_req_idx_i[i][L1I_BANK_ID_INDEX_WIDTH-1:0];
            assign lsu_pipe_if_req_hsk[i] = if_l1i_if_req_vld_i[i] & if_l1i_if_req_rdy_o[i];
        end
    endgenerate

    always_ff @(posedge clk) begin
        if(rst) begin
            if_req_bank_id_ff  <= '0;
        end 
        else begin
            for(int i = 0; i < N_ARB_IN_PORT; i++) begin
                if(lsu_pipe_if_req_hsk[i]) begin
                    if_req_bank_id_ff[i] <= if_req_bank_id[i];
                end
            end
        end
    end

    always_comb begin : lsu_if_req_to_cache_bank_router
        if_l1i_input_arb_if_req_vld = '0;
        if_l1i_input_arb_if_req_if_tag  = '0;
        if_l1i_input_arb_if_req_idx      = '0;
        if_l1i_input_arb_if_req_offset   = '0;
        if_l1i_input_arb_if_req_vtag     = '0;
        for (int i = 0; i < N_ARB_OUT_PORT; i++) begin
            for (int j = 0; j < N_ARB_IN_PORT; j++) begin
                if (if_req_bank_id[j] == i[L1I_BANK_ID_INDEX_WIDTH-1:0]) begin 
                    if_l1i_input_arb_if_req_vld    [i][j]  = if_l1i_if_req_vld_i    [j];
                    if_l1i_input_arb_if_req_if_tag [i][j]  = if_l1i_if_req_if_tag_i [j]; 
`ifdef RUBY
                    if_l1i_input_arb_if_req_lsu_tag[i][j]  = if_l1i_if_req_lsu_tag_i[j];
`endif
                    if_l1i_input_arb_if_req_idx    [i][j]  = if_l1i_if_req_idx_i     [j];
                    if_l1i_input_arb_if_req_offset [i][j]  = if_l1i_if_req_offset_i  [j];
                    if_l1i_input_arb_if_req_vtag   [i][j]  = if_l1i_if_req_vtag_i    [j];
                end
            end
        end
    end

    // 2. cache bank input arbiter
    // 2.1 ld req input check if bank conflict
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0]           l1i_bank_if_req_grt, l1i_bank_if_req_grt_rr;
    logic [N_ARB_OUT_PORT-1:0][$clog2(N_ARB_IN_PORT)-1:0]   l1i_bank_if_req_grt_idx, l1i_bank_if_req_grt_idx_rr;
    logic [N_ARB_OUT_PORT-1:0][N_ARB_IN_PORT-1:0]           l1i_bank_if_req_hsk, l1i_bank_if_req_hsk_ff;

    // cache bank input rr arbiter gen
    always_comb begin
        l1i_bank_if_req_grt = 0;
        l1i_bank_if_req_grt_idx = 0;
        if (&l1i_bank_if_req_fetch_rdy_i) begin
            for (int i = 0; i < N_ARB_OUT_PORT; i++) begin
                if (&if_l1i_input_arb_if_req_vld[i]) begin
                    // 1. all inports are valid, fetch takes precedence
                    l1i_bank_if_req_grt[i] = 1 << FETCH_PORT_IDX;
                    l1i_bank_if_req_grt_idx[i] = FETCH_PORT_IDX;
                end
                else if (if_l1i_input_arb_if_req_vld[i][FETCH_PORT_IDX]) begin
                    // 2. as long as fetch import is ready, it takes precedence
                    l1i_bank_if_req_grt[i] = 1 << FETCH_PORT_IDX;
                    l1i_bank_if_req_grt_idx[i] = FETCH_PORT_IDX;
                end
                else if (|if_l1i_input_arb_if_req_vld[i]) begin
                    // 3. only prefetch import is ready
                    l1i_bank_if_req_grt[i] = 1 << PREFETCH_PORT_IDX;
                    l1i_bank_if_req_grt_idx[i] = PREFETCH_PORT_IDX;
                end
            end
        end
        else begin
            for (int i = 0; i < N_ARB_OUT_PORT; i++) begin
                // if there is any fetch stalled, we always select prefetch req
                if (if_l1i_input_arb_if_req_vld[i][PREFETCH_PORT_IDX]) begin
                    // 2. as long as fetch import is ready, it takes precedence
                    l1i_bank_if_req_grt[i] = 1 << PREFETCH_PORT_IDX;
                    l1i_bank_if_req_grt_idx[i] = PREFETCH_PORT_IDX;
                end
            end
        end
    end

    // lsu ld input hsk with cache bank
    always_comb begin
        for(int i = 0; i < N_ARB_OUT_PORT; i++) begin
            if (l1i_bank_if_req_grt_idx[i] == FETCH_PORT_IDX) begin
                l1i_bank_if_req_hsk[i] = l1i_bank_if_req_grt[i] & {N_ARB_IN_PORT{l1i_bank_if_req_fetch_rdy_i[i]}};
            end
            else begin
                l1i_bank_if_req_hsk[i] = l1i_bank_if_req_grt[i] & {N_ARB_IN_PORT{l1i_bank_if_req_prefetch_rdy_i[i]}};
            end
        end
    end

    always_ff@(posedge clk) begin
        if(rst) begin
            l1i_bank_if_req_hsk_ff <= '0;
        end 
        else begin
            l1i_bank_if_req_hsk_ff <= l1i_bank_if_req_hsk; 
        end
    end

    always_comb begin
        if_l1i_if_req_rdy_o = '0;
        if_l1i_if_req_hit_bank_id_o = '0;
        for(int i = 0; i < N_ARB_OUT_PORT; i++) begin
            for(int j = 0; j < N_ARB_IN_PORT; j++) begin
                if(l1i_bank_if_req_grt[i][j]) begin // if the bank grt one of the ld req
                    if (j == FETCH_PORT_IDX) begin
                        if_l1i_if_req_rdy_o[j] = &l1i_bank_if_req_fetch_rdy_i; // the lsu ld pipe rdy is assigned to the ld rdy of the cache bank
                    end
                    else begin
                        if_l1i_if_req_rdy_o[j] = l1i_bank_if_req_prefetch_rdy_i[i]; // the lsu ld pipe rdy is assigned to the ld rdy of the cache bank
                    end
                    if_l1i_if_req_hit_bank_id_o[j] = i[  L1I_BANK_ID_INDEX_WIDTH-1:0];
                end
            end
        end
    end

    // 3. req signals to each cache bank
    always_comb begin
        for(int i = 0; i < N_ARB_OUT_PORT; i++) begin
            // FETCH -> I$ : IFETCH Request
            l1i_bank_if_req_vld_o      [i]  = if_l1i_input_arb_if_req_vld    [i][l1i_bank_if_req_grt_idx[i]];
            l1i_bank_if_req_if_tag_o  [i]  = if_l1i_input_arb_if_req_if_tag[i][l1i_bank_if_req_grt_idx[i]];
`ifdef RUBY
            l1i_bank_if_req_lsu_tag_o  [i]  = if_l1i_input_arb_if_req_lsu_tag[i][l1i_bank_if_req_grt_idx[i]];
`endif
            l1i_bank_if_req_idx_o      [i]  = if_l1i_input_arb_if_req_idx    [i][l1i_bank_if_req_grt_idx[i]][L1I_INDEX_WIDTH-1-:L1I_BANK_SET_INDEX_WIDTH];
            l1i_bank_if_req_offset_o   [i]  = if_l1i_input_arb_if_req_offset [i][l1i_bank_if_req_grt_idx[i]][L1I_BANK_OFFSET_WIDTH-1:0];
            l1i_bank_if_req_vtag_o     [i]  = if_l1i_input_arb_if_req_vtag   [i][l1i_bank_if_req_grt_idx[i]][L1I_BANK_TAG_WIDTH-1:0];
            l1i_bank_if_req_is_prefetch_o[i] = l1i_bank_if_req_grt_idx[i] != FETCH_PORT_IDX;
        end
    end

    // 4. tlb resp, 1cycle later than req
    always_comb begin
        itlb_l1i_resp_vld_o = '0;

        itlb_l1i_resp_excp_vld_o  = '0;
        itlb_l1i_resp_hit_o       = '0;
        itlb_l1i_resp_ppn_o       = '0;
        
        for(int i = 0; i < N_ARB_OUT_PORT; i++) begin
            for(int j = 0; j < N_ARB_IN_PORT; j++) begin
                if((if_req_bank_id_ff[j] == i[L1I_BANK_ID_INDEX_WIDTH-1:0]) & l1i_bank_if_req_hsk_ff[i][j]) begin
                    itlb_l1i_resp_vld_o     [i] = if_l1i_itlb_resp_vld_i     [j];
                    itlb_l1i_resp_excp_vld_o[i] = if_l1i_itlb_resp_excp_vld_i[j];     // s1 kill
                    itlb_l1i_resp_hit_o     [i] = if_l1i_itlb_resp_hit_i     [j];     // s1 kill
                    itlb_l1i_resp_ppn_o     [i] = if_l1i_itlb_resp_ppn_i     [j];     // VIPT, get at s1 if tlb hit
                end
            end
        end
    end

    assign fetch_l1i_if_prefetch_hit_o   = '0;
    assign fetch_l1i_if_fetch_inflight_o = '0;

    always_comb begin
        fetch_l1i_if_fetch_hit_o      = '0;
        for(int i = 0; i < N_ARB_OUT_PORT; i++) begin
            if((if_req_bank_id_ff[FETCH_PORT_IDX] == i[L1I_BANK_ID_INDEX_WIDTH-1:0]) & l1i_bank_if_req_hsk_ff[i][FETCH_PORT_IDX]) begin
                fetch_l1i_if_fetch_hit_o[FETCH_PORT_IDX] = fetch_l1i_if_fetch_hit_when_req_i[i];
            end
            else if (|fetch_l1i_if_fetch_hit_when_fill_i) begin
                fetch_l1i_if_fetch_hit_o[FETCH_PORT_IDX] = 1;
            end
        end
    end

// assertion
`ifndef SYNTHESIS
    logic [N_ARB_OUT_PORT-1:0]                                 fetch_busy_banks;
    assign fetch_busy_banks = ~l1i_bank_if_req_fetch_rdy_i;
    always_ff @(posedge clk) begin
        if (~rst) begin
            // if ((fetch_busy_banks & (fetch_busy_banks - 1'b1)) != 0) begin
            //     $fatal("more than one banks report fetch busy");
            // end
            if (N_ARB_IN_PORT != 2) begin
                $fatal("the number of I$ inport must be 2");
            end
            if ((fetch_l1i_if_fetch_hit_when_fill_i & (fetch_l1i_if_fetch_hit_when_fill_i - 1'b1)) != 0) begin
                $fatal("more than one bank report fetch hit/prehit");
            end
        end
    end
`endif

endmodule