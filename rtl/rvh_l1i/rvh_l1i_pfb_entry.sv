module rvh_l1i_pfb_entry
    import rvh_pkg::*;
    import rvh_l1i_pkg::*;
#() (
// alloc
    input logic                                         alloc_valid_i,
    input logic                                         alloc_is_fetch_i,
    input logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]          alloc_ptag_i,
    input logic [L1I_BANK_SET_INDEX_WIDTH-1:0]          alloc_bank_index_i,
    input logic [L1I_BANK_OFFSET_WIDTH-1:0]             alloc_offset_i,
    input logic [FTQ_TAG_LEN-1:0]                       alloc_ftq_tag_i,
// req
    input logic                                         req_fire_i,
// fill     
    input logic                                         fill_valid_i,
    input logic [MEM_DATA_WIDTH-1:0]                    fill_data_i,
    input logic [$clog2(BURST_SIZE)-1:0]                fill_idx_i,
    input logic                                         fill_err_i,
    input logic[$bits(rrv64_mesi_type_e)-1:0]           fill_mesi_sta_i,
    input logic                                         fill_last_seg_i,
// prefetch 
    input logic                                         prefetch_hit_i,
// fetch 
    input logic                                         fetch_hit_i,
// peek
    input logic                                         peek_done_i,
    input logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]          peek_lru_victim_way_i,
    input logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]          peek_lst_avail_way_i,
    input logic                                         peek_lst_all_way_occupied_i,

// dealloc      
    input logic                                         dealloc_valid_i,
// state        
    output logic                                        valid_o,
    output logic                                        active_o,
    output logic                                        requested_o,
    output logic                                        clear_bit_o,
    output logic                                        filled_o,
    output logic                                        need_refill_o,
    output logic                                        peek_done_o,
    output logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]         peek_lru_victim_way_o,
    output logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]         peek_lst_avail_way_o,
    output logic                                        peek_lst_all_way_occupied_o,
    output logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]         ptag_o,
    output logic [L1I_BANK_SET_INDEX_WIDTH-1:0]         bank_index_o,
    output logic [L1I_BANK_OFFSET_WIDTH-1:0]            offset_o,
    output logic [BURST_SIZE-1:0][MEM_DATA_WIDTH-1:0]   cacheline_o,
    output logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]     boundary_o,
    output logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]     rvc_mask_o,

    output logic [FTQ_TAG_LEN-1:0]                      ftq_tag_o,
    output logic                                        err_o,
    output logic[$bits(rrv64_mesi_type_e)-1:0]          mesi_sta_o,
// backend redirect
    input logic                                         flush_i,
    input logic                                         inactivate_i,
    input logic                                         clk,
    input logic                                         rst
);
    logic                                       valid_q;
    logic                                       clear_bit_q;
    logic                                       active_q;
    logic                                       requested_q;
    logic [BURST_SIZE-1:0][MEM_DATA_WIDTH-1:0]  cacheline_q;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]    boundary_q;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]    rvc_mask_q;
    logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]        ptag_q;
    logic [L1I_BANK_SET_INDEX_WIDTH-1:0]        bank_index_q;
    logic [L1I_BANK_OFFSET_WIDTH-1:0]           offset_q;
    logic [FTQ_TAG_LEN-1:0]                     ftq_tag_q;
    logic [BURST_SIZE-1:0]                      filled_mask_q;
    logic                                       err_q;
    logic                                       need_refill_q;
    logic[$bits(rrv64_mesi_type_e)-1:0]         mesi_sta_q;
    logic                                       peek_done_q;
    logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]        peek_lru_victim_way_q;
    logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]        peek_lst_avail_way_q;
    logic                                       peek_lst_all_way_occupied_q;

    logic [1:0]                                 has_inner_misalign_q;
    logic [1:0][MEM_DATA_WIDTH/16-1:0]          inst_border_mask;
    logic [1:0][MEM_DATA_WIDTH/16-1:0]          inst_rvc_mask;

    always_ff @(posedge clk) begin
        if (rst) begin
            valid_q <= 0;
            requested_q <= 0;
            filled_mask_q <= 0;
            need_refill_q <= 0;
            clear_bit_q <= 1;
            active_q <= 0;
        end
        else if (flush_i) begin
            valid_q <= 0;
            requested_q <= 0;
            filled_mask_q <= 0;
            need_refill_q <= 0;
            clear_bit_q <= 1;
            active_q <= 0;
        end 
        else begin
            if (alloc_valid_i) begin
                valid_q <= 1;
                active_q <= 1;
                clear_bit_q <= 0;
                requested_q <= 0;
                filled_mask_q <= 0;
                ptag_q <= alloc_ptag_i;
                bank_index_q <= alloc_bank_index_i;
                offset_q <= alloc_offset_i;
                ftq_tag_q <= alloc_ftq_tag_i;
                need_refill_q <= alloc_is_fetch_i;
                peek_done_q <= 0;
            end
            if (fill_valid_i) begin
`ifndef SYNTHESIS
                if (filled_mask_q[fill_idx_i] != 0) $fatal("wants to fill a filled entry");
                if (FETCH_WIDTH / MEM_DATA_WIDTH != 2) $fatal("Current code requires FETCH_ WIDTH / MEM_ DATA_ WIDTH==2, otherwise you need to modify the pre decoding logic");
`endif
                filled_mask_q[fill_idx_i] <= 1;
                cacheline_q[fill_idx_i] <= fill_data_i;
                err_q <= fill_err_i;
                mesi_sta_q <= fill_mesi_sta_i;

                if (fill_idx_i[0]) begin
                    boundary_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+MEM_DATA_WIDTH/16+:MEM_DATA_WIDTH/16] = inst_border_mask[0];
                    boundary_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+FETCH_WIDTH/16+MEM_DATA_WIDTH/16+:MEM_DATA_WIDTH/16] = inst_border_mask[1];
                    rvc_mask_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+MEM_DATA_WIDTH/16+:MEM_DATA_WIDTH/16] = inst_rvc_mask[0];
                    rvc_mask_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+FETCH_WIDTH/16+MEM_DATA_WIDTH/16+:MEM_DATA_WIDTH/16] = inst_rvc_mask[1];
                end
                else begin
                    boundary_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+:MEM_DATA_WIDTH/16] = inst_border_mask[0];
                    boundary_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+FETCH_WIDTH/16+:MEM_DATA_WIDTH/16] = inst_border_mask[1];
                    rvc_mask_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+:MEM_DATA_WIDTH/16] = inst_rvc_mask[0];
                    rvc_mask_q[fill_idx_i[$clog2(BURST_SIZE)-1:1]*FETCH_WIDTH/16*2+FETCH_WIDTH/16+:MEM_DATA_WIDTH/16] = inst_rvc_mask[1];
                end
                if (~active_q & fill_last_seg_i) begin
                    clear_bit_q <= 1;
                end
            end
            if (dealloc_valid_i) begin
                filled_mask_q <= 0;
                valid_q <= 0;
                active_q <= 0;
                clear_bit_q <= 1;
                requested_q <= 0;
                need_refill_q <= 0;
                peek_done_q <= 0;
            end
            if (req_fire_i) begin
                requested_q <= 1;
            end
            if (fetch_hit_i) begin
                ftq_tag_q <= alloc_ftq_tag_i;
                offset_q <= alloc_offset_i;
                need_refill_q <= 1;
                clear_bit_q <= 0;
                active_q <= 1;
            end
            // if (prefetch_hit_i) begin
            //     clear_bit_q <= 0;
            // end
            if (peek_done_i) begin
                peek_done_q <= 1;
                peek_lru_victim_way_q <= peek_lru_victim_way_i;
                peek_lst_avail_way_q <= peek_lst_avail_way_i;
                peek_lst_all_way_occupied_q <= peek_lst_all_way_occupied_i;
            end
            if (inactivate_i) begin
                active_q <= 0;
            end
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            has_inner_misalign_q <= 0;
        end
        else if (fill_valid_i) begin
            if (fill_idx_i[0]) begin
                has_inner_misalign_q <= 0;
            end
            else begin
                if (inst_border_mask[0][MEM_DATA_WIDTH/16-1] & (fill_data_i[MEM_DATA_WIDTH-15:MEM_DATA_WIDTH-16] == 2'b11)) begin
                    has_inner_misalign_q[0] <= 1;
                end
                else begin
                    has_inner_misalign_q[0] <= 0;
                end
                if (inst_border_mask[1][MEM_DATA_WIDTH/16-1] & (fill_data_i[MEM_DATA_WIDTH-15:MEM_DATA_WIDTH-16] == 2'b11)) begin
                    has_inner_misalign_q[1] <= 1;
                end
                else begin
                    has_inner_misalign_q[1] <= 0;
                end
            end
        end
    end

    always_comb begin
        if (fill_idx_i[0]) begin
            if (has_inner_misalign_q[0]) begin
                inst_border_mask[0] = 0;
            end
            else begin
                inst_border_mask[0] = 1;
            end
            if (has_inner_misalign_q[1]) begin
                inst_border_mask[1] = 0;
            end
            else begin
                inst_border_mask[1] = 1;
            end
            for (int i = 1; i < MEM_DATA_WIDTH / 16; i++) begin
                inst_border_mask[0][i] = inst_border_mask[0][i - 1'b1] == 0 ? 1 : fill_data_i[(i - 1'b1) * 16 +: 2] != 2'b11;
                inst_border_mask[1][i] = inst_border_mask[1][i - 1'b1] == 0 ? 1 : fill_data_i[(i - 1'b1) * 16 +: 2] != 2'b11;
            end
        end
        else begin
            inst_border_mask[0] = 0;
            inst_border_mask[1] = 1;
            for (int i = 1; i < MEM_DATA_WIDTH / 16; i++) begin
                inst_border_mask[0][i] = inst_border_mask[0][i - 1'b1] == 0 ? 1 : fill_data_i[(i - 1'b1) * 16 +: 2] != 2'b11;
                inst_border_mask[1][i] = inst_border_mask[1][i - 1'b1] == 0 ? 1 : fill_data_i[(i - 1'b1) * 16 +: 2] != 2'b11;
            end
        end
    end

    always_comb begin
        for (int i = 0; i < MEM_DATA_WIDTH / 16; i++) begin
            inst_rvc_mask[0][i] = inst_border_mask[0][i] & (fill_data_i[i*16+:2] != 2'b11);
        end
        for (int i = 0; i < MEM_DATA_WIDTH / 16; i++) begin
            inst_rvc_mask[1][i] = inst_border_mask[1][i] & (fill_data_i[i*16+:2] != 2'b11);
        end
    end

    assign valid_o = valid_q;
    assign active_o = active_q;
    assign clear_bit_o = clear_bit_q;
    assign requested_o = requested_q;
    assign cacheline_o = cacheline_q;
    assign boundary_o = boundary_q;
    assign rvc_mask_o = rvc_mask_q;
    assign ftq_tag_o = ftq_tag_q;
    assign ptag_o = ptag_q;
    assign bank_index_o = bank_index_q;
    assign offset_o = offset_q;
    assign filled_o = &filled_mask_q;
    assign err_o = err_q;
    assign mesi_sta_o = mesi_sta_q;
    assign need_refill_o = need_refill_q;
    assign peek_done_o = peek_done_q;
    assign peek_lru_victim_way_o = peek_lru_victim_way_q;
    assign peek_lst_avail_way_o = peek_lst_avail_way_q;
    assign peek_lst_all_way_occupied_o = peek_lst_all_way_occupied_q;

endmodule
