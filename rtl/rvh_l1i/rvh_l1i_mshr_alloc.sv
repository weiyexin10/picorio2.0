module rvh_l1i_mshr_alloc
  import rvh_l1i_pkg::*;
  (
    input  logic [N_MSHR-1:0]           mshr_bank_valid_i,
    output logic [N_MSHR_W-1:0]         mshr_id_o,
    output logic                        has_free_mshr_o,
    output logic [N_MSHR_W-1+1:0]       free_mshr_num_o
  );
  
  logic[N_MSHR-1:0] mshr_bank_invalid;
  assign mshr_bank_invalid = ~mshr_bank_valid_i;
  
  priority_encoder
  #(
    .SEL_WIDTH(N_MSHR)
  )
  new_mshr_id_sel
  (
    .sel_i  (mshr_bank_invalid    ),
    .id_vld_o   (has_free_mshr_o  ),
    .id_o   (mshr_id_o            )
  );

  
  one_counter 
  #(
    .DATA_WIDTH(N_MSHR)
  ) 
  free_mshr_counter_u 
  (
      .data_i(mshr_bank_invalid),
      .cnt_o(free_mshr_num_o)
  );
  
//  always_comb begin
//    mshr_sel = '0;
//    mshr_sel[mshr_id_o] = 1'b1;
//  end

endmodule
