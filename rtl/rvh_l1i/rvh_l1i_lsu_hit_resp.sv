module rvh_l1i_lsu_hit_resp
    import rvh_pkg::*;
    import rvh_l1i_pkg::*;
(
    input logic                                           resp_valid_i,
    input logic                                           pfb_hit_i,
    input logic [L1I_BANK_WAY_NUM-1:0]                    tag_compare_hit_per_way_i,
    input logic                                           ld_tlb_hit_i,
    input logic [L1I_BANK_LINE_DATA_SIZE-1:0]             pfb_data_i,
    input logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]        pfb_boundary_i,
    input logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]        pfb_rvc_mask_i,
    input logic [2-1:0][L1I_BANK_LINE_DATA_SIZE-1:0]      lsu_ld_dat_i,
    input logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0] lsu_ld_boundary_i,
    input logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0] lsu_ld_rvc_mask_i,
    input logic [FTQ_TAG_LEN-1:0]                         fetch_l1i_if_req_if_tag_i,
    input logic [L1I_BANK_OFFSET_WIDTH-1:0]               fetch_l1i_if_req_offset_i, 

    // I$ -> FETCH : resp
    output logic                                          l1i_fetch_if_resp_vld_o,
    output logic [FTQ_TAG_LEN-1:0]                        l1i_fetch_if_resp_tag_o,
    output logic [L1I_FETCH_WIDTH-1:0]                    l1i_fetch_if_resp_data_o,
    output logic [L1I_FETCH_WIDTH/16*2-1:0]               l1i_fetch_if_resp_boundary_o,
    output logic [L1I_FETCH_WIDTH/16*2-1:0]               l1i_fetch_if_resp_rvc_mask_o
);

// output data select
logic [L1I_FETCH_WIDTH-1:0] lsu_ld_hit_dat;
logic [L1I_FETCH_WIDTH/16*2-1:0] lsu_ld_hit_boundary;
logic [L1I_FETCH_WIDTH/16*2-1:0] lsu_ld_hit_rvc_mask;
logic [L1I_FETCH_WIDTH-1:0] lsu_ld_refill_dat;
logic [L1I_FETCH_WIDTH/16*2-1:0] lsu_ld_refill_boundary;
logic [L1I_FETCH_WIDTH/16*2-1:0] lsu_ld_refill_rvc_mask;

assign l1i_fetch_if_resp_data_o = pfb_hit_i ? lsu_ld_refill_dat : lsu_ld_hit_dat;
assign l1i_fetch_if_resp_boundary_o = pfb_hit_i ? lsu_ld_refill_boundary : lsu_ld_hit_boundary;
assign l1i_fetch_if_resp_rvc_mask_o = pfb_hit_i ? lsu_ld_refill_rvc_mask : lsu_ld_hit_rvc_mask;

assign l1i_fetch_if_resp_vld_o  = resp_valid_i & (ld_tlb_hit_i | pfb_hit_i);
assign l1i_fetch_if_resp_tag_o  = fetch_l1i_if_req_if_tag_i;

// get lsu_ld_refill_dat
logic [L1I_FETCH_WIDTH-1:0]    lsu_ld_refill_dat_sel_by_offset;
logic [L1I_FETCH_WIDTH/16*2-1:0] lsu_ld_refill_boundary_sel_by_offset;
logic [L1I_FETCH_WIDTH/16*2-1:0] lsu_ld_refill_rvc_mask_sel_by_offset;

assign lsu_ld_refill_dat_sel_by_offset = pfb_data_i[fetch_l1i_if_req_offset_i[L1I_BANK_OFFSET_WIDTH-1:4]*FETCH_WIDTH +: L1I_FETCH_WIDTH];
assign lsu_ld_refill_dat = lsu_ld_refill_dat_sel_by_offset;

assign lsu_ld_refill_boundary_sel_by_offset = pfb_boundary_i[fetch_l1i_if_req_offset_i[L1I_BANK_OFFSET_WIDTH-1:4]*FETCH_WIDTH/16*2 +: L1I_FETCH_WIDTH/16*2];
assign lsu_ld_refill_boundary = lsu_ld_refill_boundary_sel_by_offset;

assign lsu_ld_refill_rvc_mask_sel_by_offset = pfb_rvc_mask_i[fetch_l1i_if_req_offset_i[L1I_BANK_OFFSET_WIDTH-1:4]*FETCH_WIDTH/16*2 +: L1I_FETCH_WIDTH/16*2];
assign lsu_ld_refill_rvc_mask = lsu_ld_refill_rvc_mask_sel_by_offset;

// get lsu_ld_hit_dat
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0]         lsu_ld_hit_dat_per_way_seg;
logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0]                               lsu_ld_hit_dat_sel_way_seg;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM-1:0]    lsu_ld_hit_boundary_per_way_seg;
logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM-1:0]                          lsu_ld_hit_boundary_sel_way_seg;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM-1:0]    lsu_ld_hit_rvc_mask_per_way_seg;
logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM-1:0]                          lsu_ld_hit_rvc_mask_sel_way_seg;

always_comb begin: select_way_data
    for(int i = 0; i < 2; i++) begin
        for(int waynum = 0; waynum < L1I_BANK_WAY_NUM; waynum++) begin
            lsu_ld_hit_dat_per_way_seg[i][waynum] = lsu_ld_dat_i[i][(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)*waynum+:(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]; //data ram output 128bit, if input 128bit, use offset[4] to diff
            lsu_ld_hit_boundary_per_way_seg[i][waynum] = lsu_ld_boundary_i[i][(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)*waynum+:(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)];
            lsu_ld_hit_rvc_mask_per_way_seg[i][waynum] = lsu_ld_rvc_mask_i[i][(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)*waynum+:(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)];
        end
    end
end

assign lsu_ld_hit_dat = fetch_l1i_if_req_offset_i[4] ? {lsu_ld_hit_dat_sel_way_seg[0], lsu_ld_hit_dat_sel_way_seg[1]} :
                                                       {lsu_ld_hit_dat_sel_way_seg[1], lsu_ld_hit_dat_sel_way_seg[0]};
assign lsu_ld_hit_boundary = fetch_l1i_if_req_offset_i[4] ? {lsu_ld_hit_boundary_sel_way_seg[0], lsu_ld_hit_boundary_sel_way_seg[1]} :
                                                       {lsu_ld_hit_boundary_sel_way_seg[1], lsu_ld_hit_boundary_sel_way_seg[0]};
assign lsu_ld_hit_rvc_mask = fetch_l1i_if_req_offset_i[4] ? {lsu_ld_hit_rvc_mask_sel_way_seg[0], lsu_ld_hit_rvc_mask_sel_way_seg[1]} :
                                                       {lsu_ld_hit_rvc_mask_sel_way_seg[1], lsu_ld_hit_rvc_mask_sel_way_seg[0]};
generate
  for(genvar i = 0; i < 2; i++) begin: gen_lsu_ld_hit_dat_sel_way_seg
    onehot_mux
    #(
      .SOURCE_COUNT(L1I_BANK_WAY_NUM),
      .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)
    )
    L1I_RESP_DAT_OH_MUX
    (
      .sel_i (tag_compare_hit_per_way_i),
      .data_i(lsu_ld_hit_dat_per_way_seg[i]),
      .data_o(lsu_ld_hit_dat_sel_way_seg[i])
    );
    onehot_mux
    #(
      .SOURCE_COUNT(L1I_BANK_WAY_NUM),
      .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)
    )
    L1I_RESP_BOUNDARY_OH_MUX
    (
      .sel_i (tag_compare_hit_per_way_i),
      .data_i(lsu_ld_hit_boundary_per_way_seg[i]),
      .data_o(lsu_ld_hit_boundary_sel_way_seg[i])
    );
    onehot_mux
    #(
      .SOURCE_COUNT(L1I_BANK_WAY_NUM),
      .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)
    )
    L1I_RESP_RVC_MASK_OH_MUX
    (
      .sel_i (tag_compare_hit_per_way_i),
      .data_i(lsu_ld_hit_rvc_mask_per_way_seg[i]),
      .data_o(lsu_ld_hit_rvc_mask_sel_way_seg[i])
    );
  end
endgenerate

endmodule