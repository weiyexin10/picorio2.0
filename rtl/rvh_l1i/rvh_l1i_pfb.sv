module rvh_l1i_pfb
    import rvh_pkg::*;
    import rvh_l1i_pkg::*;
#(
    parameter ENTRY_COUNT=8,
    parameter ENTRY_IDX= $clog2(ENTRY_COUNT),
    parameter BANK_ID=0
)(
// fetch
    input logic                                         valid_i,
    input logic                                         is_prefetch_i,
    input logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]          ptag_i,
    input logic [L1I_BANK_SET_INDEX_WIDTH-1:0]          bank_index_i,
    input logic [L1I_BANK_OFFSET_WIDTH-1:0]             offset_i,
    input logic [FTQ_TAG_LEN-1:0]                       ftq_tag_i,
    input logic                                         ic_hit_i,

    output logic                                        fetch_hit_when_req_o,
    output logic                                        fetch_hit_when_fill_o,
    output logic                                        fetch_stall_o,
    output logic [L1I_BANK_LINE_DATA_SIZE-1:0]          fetch_cacheline_o,
    output logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]     fetch_boundary_o,
    output logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]     fetch_rvc_mask_o,
    output logic [FTQ_TAG_LEN-1:0]                      fetch_ftq_tag_o,
    output logic [L1I_BANK_OFFSET_WIDTH-1:0]            fetch_offset_o,
    output logic                                        prefetch_hit_o,
    output logic [ENTRY_IDX-1:0]                        prefetch_pfb_tag_o,
    output logic                                        fetch_ready_o,
    output logic                                        prefetch_ready_o,
// req to l2
    output logic                                        l2_req_if_arvalid_o,
    input  logic                                        l2_req_if_arready_i,
    output cache_mem_if_ar_t                            l2_req_if_ar_o,
    output logic                                        l2_req_if_is_fetch_o,
// resp from l2
    input logic                                         l1i_l2_rd_resp_valid_i,
    output logic                                        l1i_l2_rd_resp_ready_o,
    input cache_mem_if_r_t                              l1i_l2_rd_resp_i,

// when data from L2 are all arrived,
// search lru for victim way in case all way are full
    output                                              lru_peek_valid_o,
    output[L1I_BANK_SET_INDEX_WIDTH-1:0]                lru_peek_set_idx_o,
    input[L1I_BANK_WAY_INDEX_WIDTH-1:0]                 lru_peek_victim_way_i,
//  search lst for INVALID line and we can finalize the way we will evict 
    output[L1I_BANK_SET_INDEX_WIDTH-1:0]                lst_peek_set_idx_o,
    input rrv64_l1i_lst_t                               lst_peek_dat_i,
    input [L1I_BANK_WAY_INDEX_WIDTH-1:0]                lst_peek_avail_way_idx_i,

// do refill        
    output logic                                        cache_refill_req_valid_o,
    input logic                                         cache_refill_req_ready_i,
    output rrv64_l1i_refill_req_t                       cache_refill_req_o,
// state
    output logic                                        empty_o,

    input                                               s1_valid_i,
    input logic[PADDR_WIDTH-1:0]                        s1_paddr_i,
    input                                               s2_valid_i,
    input logic[PADDR_WIDTH-1:0]                        s2_paddr_i,

    input logic                                         flush_i,

    input  logic                                        fencei_flush_vld_or_pending_i,
    output logic                                        fencei_flush_grant_o,

    input logic                                         rst,
    input logic                                         clk
);
    enum logic[2:0]{
        IDLE,
        WAIT_FOR_L2_REQ_FIRE,
        FETCH_PENDING
    } pfb_state, next_pfb_state;

    logic [ENTRY_COUNT-1:0]                                       entry_valid;
    logic [ENTRY_COUNT-1:0]                                       entry_active;
    logic [ENTRY_COUNT-1:0]                                       entry_requested;
    logic [ENTRY_COUNT-1:0]                                       entry_clear_bit;
    logic [ENTRY_COUNT-1:0]                                       entry_evict_bit;
    logic [ENTRY_COUNT-1:0]                                       entry_filled;
    logic [ENTRY_COUNT-1:0][L1I_BANK_PADDR_TAG_WIDTH-1:0]         entry_ptag;
    logic [ENTRY_COUNT-1:0][L1I_BANK_SET_INDEX_WIDTH-1:0]         entry_bank_index;
    logic [ENTRY_COUNT-1:0][L1I_BANK_OFFSET_WIDTH-1:0]            entry_offset;
    logic [ENTRY_COUNT-1:0][BURST_SIZE-1:0][MEM_DATA_WIDTH-1:0]   entry_cacheline;
    logic [ENTRY_COUNT-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0]     entry_boundary;
    logic [ENTRY_COUNT-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0]     entry_rvc_mask;
    logic [ENTRY_COUNT-1:0][FTQ_TAG_LEN-1:0]                      entry_ftq_tag;
    logic [ENTRY_COUNT-1:0]                                       entry_err;
    logic [ENTRY_COUNT-1:0][$bits(rrv64_mesi_type_e)-1:0]         entry_mesi_sta;
    logic [ENTRY_COUNT-1:0]                                       entry_need_refill;
    logic [ENTRY_COUNT-1:0]                                       entry_peek_done;
    logic [ENTRY_COUNT-1:0][L1I_BANK_WAY_INDEX_WIDTH-1:0]         entry_lru_victim_way;
    logic [ENTRY_COUNT-1:0][L1I_BANK_WAY_INDEX_WIDTH-1:0]         entry_lst_avail_way;
    logic [ENTRY_COUNT-1:0]                                       entry_all_way_occupied;
    logic [ENTRY_COUNT-1:0]                                       entry_flush;
    logic [ENTRY_COUNT-1:0]                                       entry_inactivate;

    logic [ENTRY_COUNT-1:0]                                       entry_wait_for_peek_one_hot;
    logic [ENTRY_COUNT-1:0]                                       entry_wait_for_refill_one_hot;

    logic [$clog2(ENTRY_COUNT)-1:0]                               pending_fetch_idx;
    logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]                          pending_fetch_ptag;
    logic [L1I_BANK_SET_INDEX_WIDTH-1:0]                          pending_fetch_bank_index;

    logic                                                       fetch_inflight;
    logic                                                       fetch_miss;
    logic                                                       fetch_hit_when_req, fetch_hit_when_fill;
    logic [L1I_BANK_LINE_DATA_SIZE-1:0]                         fetch_cacheline_when_req_hit, fetch_cacheline_when_fill_hit;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]                    fetch_boundary_when_req_hit, fetch_boundary_when_fill_hit;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]                    fetch_rvc_mask_when_req_hit, fetch_rvc_mask_when_fill_hit;
    logic                                                       need_alloc;
    logic                                                       alloc_fire;
    logic                                                       l2_req_fire;
    logic  [ENTRY_IDX-1:0]                                      avail_entry_idx;
    logic  [ENTRY_IDX-1:0]                                      evict_entry_idx;
    logic  [ENTRY_IDX-1:0]                                      replace_entry_idx;
    logic                                                       has_pending_req;
    logic  [ENTRY_IDX-1:0]                                      pending_req_idx;
    logic                                                       is_fetch_req;

    logic[$clog2(BURST_SIZE) -1:0]                              fill_seg_wr_cnt;
    logic[ENTRY_IDX-1:0]                                        fill_pfb_idx;
    logic                                                       fill_err;
    logic[$bits(rrv64_mesi_type_e)-1:0]                         fill_mesi_sta;
    logic                                                       fill_last_seg;

    logic [ENTRY_COUNT-1:0]                                       req_hit_mask;
    logic [ENTRY_COUNT-1:0]                                       fill_hit_mask;
    logic                                                       pending_fetch_done;
    logic [ENTRY_COUNT-1:0]                                       inflight_mask;
    logic [$clog2(ENTRY_COUNT)-1:0]                                inflight_idx;

    logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]                        peek_ptag;
    logic [L1I_BANK_SET_INDEX_WIDTH-1:0]                        peek_bank_index;
    logic [L1I_BANK_OFFSET_WIDTH-1:0]                           peek_offset;
    logic [PADDR_WIDTH-1:0]                                     peek_paddr;
    logic [L1I_BANK_WAY_NUM-1:0]                                peek_way_avail_mask;
    logic                                                       peek_all_way_occupied;

    logic [BURST_SIZE-1:0][MEM_DATA_WIDTH-1:0]                  refill_cacheline;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]                    refill_boundary;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]                    refill_rvc_mask;
    logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]                        refill_lru_victim_way;
    logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]                        refill_lst_avail_way;
    logic                                                       refill_all_way_occupied;
    logic [$bits(rrv64_mesi_type_e)-1:0]                        refill_mesi_sta;
    logic [FTQ_TAG_LEN-1:0]                                     refill_ftq_tag;
    logic                                                       refill_done;
    logic [PADDR_WIDTH-1:0]                                     refill_paddr;
    logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]                        refill_ptag;
    logic [L1I_BANK_SET_INDEX_WIDTH-1:0]                        refill_bank_index;
    logic [L1I_BANK_OFFSET_WIDTH-1:0]                           refill_offset;

// request to l2
    assign l2_req_if_arvalid_o = has_pending_req;
    assign l2_req_fire = l2_req_if_arvalid_o & l2_req_if_arready_i;
    assign l2_req_if_ar_o.arid.is_io = '0;
    assign l2_req_if_ar_o.arid.master_id = 1;
    assign l2_req_if_ar_o.arid.tid = pending_req_idx;
    assign l2_req_if_ar_o.arid.bid = {1'b1, BANK_ID[MEMNOC_TID_MASTERID_SIZE-1-1:0]}; // msb 1 represents i$
    assign l2_req_if_is_fetch_o = is_fetch_req;
    generate
        if(L1I_BANK_ID_NUM > 1) begin  
            assign l2_req_if_ar_o.araddr  = {entry_ptag[pending_req_idx], entry_bank_index[pending_req_idx], BANK_ID[L1I_BANK_ID_INDEX_WIDTH-1:0], {L1I_BANK_OFFSET_WIDTH{1'b0}}};
        end else begin
            assign l2_req_if_ar_o.araddr  = {entry_ptag[pending_req_idx], entry_bank_index[pending_req_idx], {L1I_BANK_OFFSET_WIDTH{1'b0}}};
        end
    endgenerate
    // read a full burst from memory(2'b11)
    assign l2_req_if_ar_o.arlen   = BURST_SIZE-1;
    assign l2_req_if_ar_o.arsize  = AXI_SIZE;
    assign l2_req_if_ar_o.arburst = 2'b01; // INCR mode

    priority_encoder #(
        .SEL_WIDTH(ENTRY_COUNT)
    )u_avail_entry_priority_encoder (
        .sel_i(~entry_valid),
        .id_vld_o(),
        .id_o(avail_entry_idx)
    );

    priority_encoder #(
        .SEL_WIDTH(ENTRY_COUNT)
    )u_evict_entry_priority_encoder (
        .sel_i(entry_evict_bit),
        .id_vld_o(),
        .id_o(evict_entry_idx)
    );

    assign entry_evict_bit = |entry_clear_bit ? entry_clear_bit : ~entry_need_refill & entry_filled;
    assign replace_entry_idx = &entry_valid ? evict_entry_idx : avail_entry_idx;

    mp_queue
    #(
        .payload_t          (logic[ENTRY_IDX-1:0]   ),
        .ENQUEUE_WIDTH      (1                      ),
        .DEQUEUE_WIDTH      (1                      ),
        .DEPTH              (ENTRY_COUNT              ),
        .MUST_TAKEN_ALL     (1                      )
    ) u_req_queue (
        // Enqueue
        .enqueue_vld_i                  (alloc_fire          ),
        .enqueue_push_front_i           (~is_prefetch_i      ),
        .enqueue_payload_i              (replace_entry_idx   ),
        .enqueue_rdy_o                  (                    ),
        // Dequeue      
        .dequeue_vld_o                  (has_pending_req     ),
        .dequeue_payload_o              (pending_req_idx     ),
        .dequeue_pushed_from_front_o    (is_fetch_req        ),
        .dequeue_rdy_i                  (l2_req_fire         ),

        .flush_i                        (flush_i | fencei_flush_vld_or_pending_i),

        .clk                            (clk                 ),
        .rst                            (rst                 )
    );

// resp from l2
    always @(posedge clk) begin
        if (rst) begin
            fill_seg_wr_cnt <= 0;
        end
        else if (l1i_l2_rd_resp_valid_i) begin
            fill_seg_wr_cnt <= fill_seg_wr_cnt + 1;
        end
    end

    assign fill_pfb_idx    = l1i_l2_rd_resp_i.rid.tid;
    assign fill_err         = l1i_l2_rd_resp_i.err;
    assign fill_mesi_sta    = l1i_l2_rd_resp_i.mesi_sta;
    assign fill_last_seg    = l1i_l2_rd_resp_i.rlast;

    generate for (genvar i = 0; i < ENTRY_COUNT; i++) begin
        assign req_hit_mask[i] = entry_valid[i] & entry_filled[i] & ({entry_ptag[i], entry_bank_index[i]} == {ptag_i, bank_index_i});
        assign inflight_mask[i] = entry_valid[i] & ~entry_filled[i] & ({entry_ptag[i], entry_bank_index[i]} == {ptag_i, bank_index_i});
    end
    endgenerate

    assign need_alloc = valid_i & ~ic_hit_i & ((req_hit_mask | inflight_mask) == 0) & ~flush_i;    // fetch / prefetch miss
    assign alloc_fire = need_alloc & (fetch_ready_o | prefetch_ready_o);
// prefetch
    assign prefetch_hit_o = valid_i & is_prefetch_i & |(req_hit_mask | inflight_mask);
// fetch

    generate for (genvar i = 0; i < ENTRY_COUNT; i++) begin
        assign fill_hit_mask[i] = pending_fetch_done & (i == pending_fetch_idx);
    end
    endgenerate

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE)
    ) u_data_out_in_hit_mux
    (
        .sel_i(req_hit_mask),
        .data_i(entry_cacheline),
        .data_o(fetch_cacheline_when_req_hit)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE)
    ) u_data_out_in_fill_mux
    (
        .sel_i(fill_hit_mask),
        .data_i(entry_cacheline),
        .data_o(fetch_cacheline_when_fill_hit)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/16*2)
    ) u_boundary_out_in_hit_mux
    (
        .sel_i(req_hit_mask),
        .data_i(entry_boundary),
        .data_o(fetch_boundary_when_req_hit)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/16*2)
    ) u_boundary_out_in_fill_mux
    (
        .sel_i(fill_hit_mask),
        .data_i(entry_boundary),
        .data_o(fetch_boundary_when_fill_hit)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/16*2)
    ) u_rvc_mask_out_in_hit_mux
    (
        .sel_i(req_hit_mask),
        .data_i(entry_rvc_mask),
        .data_o(fetch_rvc_mask_when_req_hit)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH(L1I_BANK_LINE_DATA_SIZE/16*2)
    ) u_rvc_mask_out_in_fill_mux
    (
        .sel_i(fill_hit_mask),
        .data_i(entry_rvc_mask),
        .data_o(fetch_rvc_mask_when_fill_hit)
    );

    
    assign pending_fetch_done = (pfb_state == FETCH_PENDING) & entry_valid[pending_fetch_idx] & entry_filled[pending_fetch_idx];
    assign fetch_hit_when_req = valid_i & ~is_prefetch_i & |req_hit_mask;
    assign fetch_hit_when_fill = pending_fetch_done & entry_active[pending_fetch_idx];
    assign fetch_hit_when_req_o = fetch_hit_when_req;
    assign fetch_hit_when_fill_o = fetch_hit_when_fill;
    assign fetch_ftq_tag_o = entry_ftq_tag[pending_fetch_idx];
    assign fetch_offset_o = entry_offset[pending_fetch_idx];
    assign fetch_cacheline_o = fetch_hit_when_req ? fetch_cacheline_when_req_hit : fetch_cacheline_when_fill_hit;
    assign fetch_boundary_o = fetch_hit_when_req ? fetch_boundary_when_req_hit : fetch_boundary_when_fill_hit;
    assign fetch_rvc_mask_o = fetch_hit_when_req ? fetch_rvc_mask_when_req_hit : fetch_rvc_mask_when_fill_hit;
    assign fetch_stall_o = (pfb_state != IDLE) | (next_pfb_state != IDLE);
    assign fetch_inflight = valid_i & ~is_prefetch_i & |inflight_mask ;
    assign fetch_miss = valid_i & ~is_prefetch_i & ~ic_hit_i & (req_hit_mask == 0) & (inflight_mask == 0);

    always_comb begin
        next_pfb_state = pfb_state;
        case (pfb_state)
            IDLE: begin
                if ((fetch_ready_o | prefetch_ready_o) & ~flush_i) begin
                    if (fetch_miss) begin
                        next_pfb_state = WAIT_FOR_L2_REQ_FIRE;
                    end
                    else if (fetch_inflight) begin
                        next_pfb_state = FETCH_PENDING;
                    end
                end
            end
            WAIT_FOR_L2_REQ_FIRE: begin
                if (l2_req_fire) begin
                    next_pfb_state = FETCH_PENDING;
                end
                else if (flush_i) begin
                    next_pfb_state = IDLE;
                end
            end
            FETCH_PENDING: begin
                if (pending_fetch_done) begin
                    next_pfb_state = IDLE;
                end
                else if (flush_i & ~(l2_req_fire & (l2_req_if_ar_o.arid.tid == pending_fetch_idx)) & ~entry_requested[pending_fetch_idx]) begin
                    next_pfb_state = IDLE;
                end
            end
            default:;
        endcase
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            pfb_state <= IDLE;
        end
        else begin
            pfb_state <= next_pfb_state;
        end
    end

    priority_encoder #(
        .SEL_WIDTH(ENTRY_COUNT)
    )u_inflight_priority_encoder (
        .sel_i(inflight_mask),
        .id_vld_o(),
        .id_o(inflight_idx)
    );

    always_ff @(posedge clk) begin
        if ((pfb_state == IDLE) & (next_pfb_state != IDLE)) begin
            pending_fetch_idx <= fetch_inflight ? inflight_idx : replace_entry_idx;
            pending_fetch_ptag <= ptag_i;
            pending_fetch_bank_index <= bank_index_i;
        end
    end

`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (!rst) begin
            if (valid_i & ((req_hit_mask & (req_hit_mask - 1'b1)) != 0)) begin
                $fatal("req_hit_mask is not one hot!");
            end
            if (valid_i & ((inflight_mask & (inflight_mask - 1'b1)) != 0)) begin
                $fatal("inflight_mask is not one hot!");
            end
        end
    end
    
`endif

// generate entry flush signal, only flush the entries that has not send req to l2
// we don't flush the entries that has filled or waiting for l2's resp, instead we inactive them
always_comb begin
    entry_flush = 0;
    for (int i = 0; i < ENTRY_COUNT; i++) begin
        if (flush_i) begin
            entry_flush[i] = entry_valid[i] & ~entry_requested[i] & ~((l2_req_fire & (i == pending_req_idx)));
        end
    end
end

always_comb begin
    entry_inactivate = 0;
    for (int i = 0; i < ENTRY_COUNT; i++) begin
        if (flush_i) begin
            entry_inactivate[i] = entry_valid[i] & (entry_requested[i] | (l2_req_fire & (i == pending_req_idx))) & ~entry_filled[i];
        end
    end
end

// 0. select a line for peek
    one_hot_priority_encoder #(
        .SEL_WIDTH(ENTRY_COUNT)
    ) u_wait_for_peek_sel_one_hot_priority_encoder (
        .sel_i(entry_need_refill & entry_filled & ~(entry_peek_done)),
        .sel_o(entry_wait_for_peek_one_hot)
    );
// 0.1. generate paddr for peek
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_PADDR_TAG_WIDTH)
    ) u_wait_for_peek_tag_onehot_mux (
        .sel_i (entry_wait_for_peek_one_hot),
        .data_i(entry_ptag),
        .data_o(peek_ptag)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_SET_INDEX_WIDTH)
    ) u_wait_for_peek_bank_index_onehot_mux (
        .sel_i (entry_wait_for_peek_one_hot),
        .data_i(entry_bank_index),
        .data_o(peek_bank_index)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_OFFSET_WIDTH)
    ) u_wait_for_peek_bank_offset_onehot_mux (
        .sel_i (entry_wait_for_peek_one_hot),
        .data_i(entry_offset),
        .data_o(peek_offset)
    );
    generate if(L1I_BANK_ID_NUM > 1) begin  
            assign peek_paddr = {peek_ptag, peek_bank_index, BANK_ID[L1I_BANK_ID_INDEX_WIDTH-1:0], peek_offset};
        end else begin
            assign peek_paddr = {peek_ptag, peek_bank_index, peek_offset};
        end
    endgenerate
// 0.2. do peek: search lru for victim way in case all way are full
    assign lru_peek_valid_o = |entry_wait_for_peek_one_hot;
    assign lru_peek_set_idx_o = peek_paddr[L1I_BANK_SET_INDEX_WIDTH+L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH-1:L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH];
// 0.3. do peek: search lst for INVALID line and we can finalize the way we will evict 
    assign lst_peek_set_idx_o = peek_paddr[L1I_BANK_SET_INDEX_WIDTH+L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH-1:L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH];
    generate for(genvar i=0; i<L1I_BANK_WAY_NUM; i++) begin
        assign peek_way_avail_mask[i] = (lst_peek_dat_i.mesi_sta[i]!=INVALID);
    end
    endgenerate
    assign peek_all_way_occupied = &peek_way_avail_mask;

// 2. select a line for refill
    one_hot_priority_encoder #(
        .SEL_WIDTH(ENTRY_COUNT)
    ) u_refill_one_hot_priority_encoder (
        .sel_i(entry_need_refill & entry_filled & entry_peek_done),
        .sel_o(entry_wait_for_refill_one_hot)
    );
// 2.1. generate paddr for refill
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_PADDR_TAG_WIDTH)
    ) u_refill_tag_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_ptag),
        .data_o(refill_ptag)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_SET_INDEX_WIDTH)
    ) u_refill_bank_index_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_bank_index),
        .data_o(refill_bank_index)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_OFFSET_WIDTH)
    ) u_refill_offset_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_offset),
        .data_o(refill_offset)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (1)
    ) u_refill_occupied_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_all_way_occupied),
        .data_o(refill_all_way_occupied)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_WAY_INDEX_WIDTH)
    ) u_refill_victim_way_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_lru_victim_way),
        .data_o(refill_lru_victim_way)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_WAY_INDEX_WIDTH)
    ) u_refill_avail_way_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_lst_avail_way),
        .data_o(refill_lst_avail_way)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_LINE_DATA_SIZE)
    ) u_refill_cacheline_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_cacheline),
        .data_o(refill_cacheline)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_LINE_DATA_SIZE/16*2)
    ) u_refill_boundary_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_boundary),
        .data_o(refill_boundary)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (L1I_BANK_LINE_DATA_SIZE/16*2)
    ) u_refill_rvc_mask_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_rvc_mask),
        .data_o(refill_rvc_mask)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  ($bits(rrv64_mesi_type_e))
    ) u_refill_mesi_sta_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_mesi_sta),
        .data_o(refill_mesi_sta)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (FTQ_TAG_LEN)
    ) u_ftq_tag_onehot_mux (
        .sel_i (entry_wait_for_refill_one_hot),
        .data_i(entry_ftq_tag),
        .data_o(refill_ftq_tag)
    );
    generate if(L1I_BANK_ID_NUM > 1) begin  
            assign refill_paddr = {refill_ptag, refill_bank_index, BANK_ID[L1I_BANK_ID_INDEX_WIDTH-1:0], refill_offset};
        end else begin
            assign refill_paddr = {refill_ptag, refill_bank_index, refill_offset};
        end
    endgenerate
// 2.2. fire refill req
    assign cache_refill_req_valid_o = |entry_wait_for_refill_one_hot;
    assign refill_done = cache_refill_req_valid_o & cache_refill_req_ready_i;



    assign cache_refill_req_o.set_idx = refill_paddr[L1I_BANK_SET_INDEX_WIDTH+L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH-1:L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH];
    assign cache_refill_req_o.way_idx = refill_all_way_occupied ? refill_lru_victim_way : refill_lst_avail_way;
    assign cache_refill_req_o.tag     = refill_ptag;
    assign cache_refill_req_o.dat     = refill_cacheline;
    assign cache_refill_req_o.mesi_sta= refill_mesi_sta; // TODO: it is only for single core
    assign cache_refill_req_o.is_lr   = 0;
    assign cache_refill_req_o.is_ld   = 1;
    assign cache_refill_req_o.offset  = refill_offset;
    assign cache_refill_req_o.req_type_dec = 0;
    assign cache_refill_req_o.ld_no_resp = 0;
    assign cache_refill_req_o.boundary     = refill_boundary;
    assign cache_refill_req_o.rvc_mask     = refill_rvc_mask;

    generate for (genvar i = 0; i < ENTRY_COUNT; i++) begin
            rvh_l1i_pfb_entry u_pfb_entry(
            // alloc
                .alloc_valid_i(alloc_fire & (i == replace_entry_idx)),
                .alloc_is_fetch_i(~is_prefetch_i),
                .alloc_ptag_i(ptag_i),
                .alloc_bank_index_i(bank_index_i),
                .alloc_offset_i(offset_i),
                .alloc_ftq_tag_i(ftq_tag_i),
            // request
                .req_fire_i(l2_req_fire & (pending_req_idx == i)),
            // fill     
                .fill_valid_i(l1i_l2_rd_resp_valid_i & (fill_pfb_idx == i)),
                .fill_data_i(l1i_l2_rd_resp_i.dat),
                .fill_idx_i(fill_seg_wr_cnt),
                .fill_err_i(fill_err),
                .fill_mesi_sta_i(fill_mesi_sta),
                .fill_last_seg_i(fill_last_seg),
            // prefetch
                .prefetch_hit_i(valid_i & is_prefetch_i & (req_hit_mask[i] | inflight_mask[i])),
            // fetch
                .fetch_hit_i(valid_i & ~is_prefetch_i & (req_hit_mask[i] | inflight_mask[i])),
            // peek
                .peek_done_i(lru_peek_valid_o & entry_wait_for_peek_one_hot[i]),
                .peek_lru_victim_way_i(lru_peek_victim_way_i),
                .peek_lst_avail_way_i(lst_peek_avail_way_idx_i),
                .peek_lst_all_way_occupied_i(peek_all_way_occupied),
            // dealloc      
                .dealloc_valid_i(refill_done & entry_wait_for_refill_one_hot[i]),
            // state        
                .valid_o(entry_valid[i]),
                .active_o(entry_active[i]),
                .requested_o(entry_requested[i]),
                .clear_bit_o(entry_clear_bit[i]),
                .filled_o(entry_filled[i]),
                .need_refill_o(entry_need_refill[i]),
                .peek_done_o(entry_peek_done[i]),
                .peek_lru_victim_way_o(entry_lru_victim_way[i]),
                .peek_lst_avail_way_o(entry_lst_avail_way[i]),
                .peek_lst_all_way_occupied_o(entry_all_way_occupied[i]),
                .ptag_o(entry_ptag[i]),
                .bank_index_o(entry_bank_index[i]),
                .offset_o(entry_offset[i]),
                .cacheline_o(entry_cacheline[i]),
                .boundary_o(entry_boundary[i]),
                .rvc_mask_o(entry_rvc_mask[i]),
                .ftq_tag_o(entry_ftq_tag[i]),
                .err_o(entry_err[i]),
                .mesi_sta_o(entry_mesi_sta[i]),
            // backend redirect
                .flush_i(entry_flush[i] | fencei_flush_grant_o),
                .inactivate_i(entry_inactivate[i]),
                .clk(clk),
                .rst(rst)
            );
        end
    endgenerate

    assign empty_o = |entry_valid == 0;
    assign l1i_l2_rd_resp_ready_o = 1;
    assign prefetch_pfb_tag_o = replace_entry_idx;

    always_comb begin
        if (&entry_valid == 0) begin
            fetch_ready_o = 1;
            prefetch_ready_o = 1;
        end
        else if (|entry_clear_bit) begin
            fetch_ready_o = 1;
            prefetch_ready_o = 1;
        end
        else if (|entry_evict_bit) begin
            fetch_ready_o = 1;
            prefetch_ready_o = 0;
        end
        else begin
            fetch_ready_o = 0;
            prefetch_ready_o = 0;
        end
    end

// fencei
    assign fencei_flush_grant_o = fencei_flush_vld_or_pending_i & ~l2_req_fire & ((entry_valid & entry_requested & ~entry_filled) == 0);

// assertion
`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (~rst) begin
            if (pending_fetch_done & ({entry_ptag[pending_fetch_idx], entry_bank_index[pending_fetch_idx]} != {pending_fetch_ptag, pending_fetch_bank_index})) begin
                $fatal("pending fetch tag in entry is not equal to pending_fetch_ptag in pipeline");
            end
            if (fetch_hit_when_req & fetch_hit_when_fill) begin
                $fatal("fetch_hit_when_req & fetch_hit_when_fill can not be true at the same time");
            end
        end
    end
`endif

endmodule
