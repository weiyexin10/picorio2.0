module rvh_l1i_bank_data_ram_access
    import rvh_pkg::*;
    import rvh_l1i_pkg::*;
(
  input  logic                                                                                  cache_wr_valid_dram_i,
  input  logic                                                                                  cache_rd_valid_dram_i,
  input  logic [L1I_BANK_SET_INDEX_WIDTH-1:0 ]                                                  cache_idx_dram_i,
  input  logic [L1I_BANK_OFFSET_WIDTH-1:0]                                                      cache_offset_dram_i,
  input  logic [L1I_BANK_WAY_INDEX_WIDTH-1:0]                                                   cache_way_idx_dram_i,
  input  logic [L1I_BANK_OFFSET_WIDTH-1:0   ]                                                   cache_offset_dram_s1_i,
  input  logic                                                                                  refill_hsk_i,
  input  rrv64_l1i_refill_req_t                                                                 refill_req_i,

  // access data ram
  output logic [L1I_BANK_WAY_NUM-1:0]                                                           dram_cs_o ,
  output logic [L1I_BANK_WAY_NUM-1:0]                                                           dram_wen_o,
  output logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH-1:0]  dram_addr_ram_o,
  output logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0]             dram_wdat_ram_o,
  output logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0]        dram_w_boundary_ram_o,
  output logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0]        dram_w_rvc_mask_ram_o,
  output logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8-1:0]           dram_wen_biten_o, // byte enable
  output logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2/8-1:0]      dram_boundary_wen_biten_o, // byte enable

  input  logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0]             dram_rdat_i,
  input  logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM-1:0]        dram_r_boundary_i,
  input  logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM-1:0]        dram_r_rvc_mask_i,

  // reform data ram output
  output logic [L1I_BANK_LINE_DATA_SIZE-1:0]                                                    dram_rdat_all_o,
  output logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]                                               dram_r_boundary_all_o,
  output logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]                                               dram_r_rvc_mask_all_o

);

logic[L1I_BANK_WAY_NUM -1:0] cache_way_en_dram;
logic[L1I_BANK_WAY_NUM -1:0] cache_way_en_dram_seg;
logic[L1I_BANK_WAY_NUM -1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8-1:0] cache_way_byte_en_dram_seg;
logic [XLEN/8-1:0] st_dat_biten; // byte enable
logic [L1I_BANK_WAY_NUM-1:0] dram_wen_way;
logic [L1I_BANK_SET_INDEX_WIDTH-1:0] dram_addr;
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH-1:0]dram_addr_by_way;
logic [L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH-1:0] dram_addr_by_offset;
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0] dram_wdat_way;
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0] dram_w_boundary_way;
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0] dram_w_rvc_mask_way;

logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all_way_seg_realigned;
logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all_way_seg_realigned_offset0;
logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all_way_seg_realigned_offset1;
logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all_way_seg_realigned_offset2;
logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all_way_seg_realigned_offset3;

logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_boundary_all_way_seg_realigned;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_boundary_all_way_seg_realigned_offset0;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_boundary_all_way_seg_realigned_offset1;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_boundary_all_way_seg_realigned_offset2;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_boundary_all_way_seg_realigned_offset3;

logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_rvc_mask_all_way_seg_realigned;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_rvc_mask_all_way_seg_realigned_offset0;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_rvc_mask_all_way_seg_realigned_offset1;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_rvc_mask_all_way_seg_realigned_offset2;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_rvc_mask_all_way_seg_realigned_offset3;

logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_wdat;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_w_boundary;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_w_rvc_mask;

// --- data --- //
  // to data ram
// separate even and odd line_seg to achieve one block ahead access
// even bank
//{set_idx,line_seg=0} : way0_seg0 way1_seg0 way2_seg0 way3_seg0
//{set_idx,line_seg=2} : way2_seg2 way3_seg2 way0_seg2 way1_seg2

// odd bank
//{set_idx,line_seg=l} : way3_seg1 way0_seg1 way1_seg1 way2_seg1
//{set_idx,line seg=3} : way1_seg3 way2_seg3 way3_seg3 way0_seg3

//{set_idx,line_seg=0} : way0_seg0 way1_seg0 way2_seg0 way3_seg0
//{set_idx,line_seg=l} : way3_seg1 way0_seg1 way1_seg1 way2_seg1
//{set_idx,line_seg=2} : way2_seg2 way3_seg2 way0_seg2 way1_seg2
//{set_idx,line seg=3} : way1_seg3 way2_seg3 way3_seg3 way0_seg3

// wr byte enable gen
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8-1:0] fetch_l1i_st_req_data_byte_mask_per_seg;

logic [L1I_BANK_LINE_DATA_SIZE/8-1:0] fetch_l1i_st_req_data_byte_mask_whole_line;

always_comb begin:fetch_l1i_st_req_data_byte_mask_whole_line_gen
    fetch_l1i_st_req_data_byte_mask_whole_line = '0;
    for(int i = 0; i < L1I_BANK_WAY_NUM; i++) begin
        fetch_l1i_st_req_data_byte_mask_per_seg[i] = fetch_l1i_st_req_data_byte_mask_whole_line[(i*(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8))+:(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8)];
    end
end



// data ram access
assign dram_cs_o = (cache_way_en_dram & {L1I_BANK_WAY_NUM{cache_wr_valid_dram_i }}) | {L1I_BANK_WAY_NUM{cache_rd_valid_dram_i }};
assign dram_wen_o = dram_wen_way; // lsu st wr a single way
assign dram_addr = cache_idx_dram_i;
assign dram_addr_by_offset = {dram_addr,cache_offset_dram_i[5:4]};

assign dram_wdat    = refill_req_i.dat;
assign dram_w_boundary = refill_req_i.boundary;
assign dram_w_rvc_mask = refill_req_i.rvc_mask;
assign st_dat_biten = '0;
//assign st_dat_biten   = cur.s1.req_type_dec.op_b  ? {{(XLEN/8-1){1'b0}}, 1'b1}  :
//                        cur.s1.req_type_dec.op_hw ? {{(XLEN/8-2){1'b0}}, 2'b11} :
//                        cur.s1.req_type_dec.op_w  ? {{(XLEN/8-4){1'b0}}, 4'b1111} :
//                        cur.s1.req_type_dec.op_dw ? {{(XLEN/8-8){1'b0}}, 8'b11111111} : '0;
// assign dram_wen_biten_tmp = refill_hsk_i ? '1 : (st_dat_biten << cur.s1.fetch_l1i_req_offset[$clog2(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8)-1:0]);
// assign dram line seg idx=cache_line_seg dram;
always_comb begin
  for(int jj=0; jj<L1I_BANK_WAY_NUM; jj++) begin
    for (int i = 0; i < $bits(dram_boundary_wen_biten_o[0]); i++) begin
      dram_boundary_wen_biten_o[jj][i] = |dram_wen_biten_o[jj][i*8+:8];
    end
  end
end

generate
  genvar jj;
  for(jj=0; jj<L1I_BANK_WAY_NUM; jj++) begin:DATA_RAM_SIGNAL_GEN
    assign cache_way_en_dram[jj] = refill_hsk_i ? '1 : cache_way_en_dram_seg[jj];
    assign dram_wen_way[jj] = cache_wr_valid_dram_i & cache_way_en_dram[jj];
    assign dram_wen_biten_o[jj] = refill_hsk_i ? '1 : cache_way_byte_en_dram_seg[jj];
//    assign dram_wen_all[jj] = cache_wr_valid_dram_i ;
    if(jj==0) begin
      assign cache_way_byte_en_dram_seg[jj] = {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b00)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[0]) |
                                              {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b11)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[1]) |
                                              {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b10)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[2]) |         
                                              {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b01)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[3]) ;

      assign dram_wdat_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                 (cache_way_idx_dram_i == 2'b01) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                 (cache_way_idx_dram_i == 2'b10) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                                                 dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)];
      
      assign dram_w_boundary_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                       (cache_way_idx_dram_i == 2'b01) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                       (cache_way_idx_dram_i == 2'b10) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                 dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      
      assign dram_w_rvc_mask_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                       (cache_way_idx_dram_i == 2'b01) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                       (cache_way_idx_dram_i == 2'b10) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                 dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1 +: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      
      assign dram_addr_by_way [jj]= {(L1I_BANK_SET_INDEX_WIDTH+2){(cache_way_idx_dram_i == 2'b00)}} & {dram_addr,2'b00} |
                                    {(L1I_BANK_SET_INDEX_WIDTH+2){(cache_way_idx_dram_i == 2'b01)}} & {dram_addr,2'b11} |
                                    {(L1I_BANK_SET_INDEX_WIDTH+2){(cache_way_idx_dram_i == 2'b10)}} & {dram_addr,2'b10} |
                                    {(L1I_BANK_SET_INDEX_WIDTH+2){(cache_way_idx_dram_i == 2'b11)}} & {dram_addr,2'b01} ; 
    end
    if(jj==1) begin
      assign cache_way_byte_en_dram_seg[jj]  = {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b00)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[1]) |
                                          {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b11)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[2]) |
                                          {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b10)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[3]) | 
                                          {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b01)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[0]) ;
      assign dram_wdat_way[jj]  = (cache_way_idx_dram_i == 2'b00) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                  (cache_way_idx_dram_i == 2'b01) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                  (cache_way_idx_dram_i == 2'b10) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                                                  dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)];
      
      assign dram_w_boundary_way[jj]  = (cache_way_idx_dram_i == 2'b00) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                        (cache_way_idx_dram_i == 2'b01) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                        (cache_way_idx_dram_i == 2'b10) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                  dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      assign dram_w_rvc_mask_way[jj]  = (cache_way_idx_dram_i == 2'b00) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                        (cache_way_idx_dram_i == 2'b01) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                        (cache_way_idx_dram_i == 2'b10) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                  dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];

      assign dram_addr_by_way [jj]= {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b00)}} & {dram_addr,2'b01} |
                                    {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b01)}} & {dram_addr,2'b00} |
                                    {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b10)}} & {dram_addr,2'b11} |
                                    {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b11)}} & {dram_addr,2'b10};
    end
    if(jj==2) begin
      assign cache_way_byte_en_dram_seg[jj]  = {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b00)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[2]) |
                                          {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b11)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[3]) |
                                          {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b10)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[0]) |
                                          {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b01)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[1]);
      assign dram_wdat_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                 (cache_way_idx_dram_i == 2'b01) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                 (cache_way_idx_dram_i == 2'b10) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                                                 dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)];
      assign dram_w_boundary_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b01) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b10) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                 dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      assign dram_w_rvc_mask_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b01) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b10) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                 dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      assign dram_addr_by_way [jj]= {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b00)}} & {dram_addr,2'b10} |
                                    {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b01)}} & {dram_addr,2'b01} |
                                    {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b10)}} & {dram_addr,2'b00} |
                                    {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b11)}} & {dram_addr,2'b11} ;
    end
    if(jj==3) begin
      assign cache_way_byte_en_dram_seg[jj] = {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b00)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[3]) |
                                         {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b11)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[0]) |
                                         {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b10)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[1]) |
                                         {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8){(cache_way_idx_dram_i == 2'b01)}} & (fetch_l1i_st_req_data_byte_mask_per_seg[2]);
      assign dram_wdat_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                 (cache_way_idx_dram_i == 2'b01) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                 (cache_way_idx_dram_i == 2'b10) ? dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)]:
                                                                 dram_wdat[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)];
      
      assign dram_w_boundary_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b01) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b10) ? dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                 dram_w_boundary[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      assign dram_w_rvc_mask_way[jj] = (cache_way_idx_dram_i == 2'b00) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b01) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                      (cache_way_idx_dram_i == 2'b10) ? dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)]:
                                                                 dram_w_rvc_mask[(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2)];
      assign dram_addr_by_way [jj] = {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b00)}} & {dram_addr,2'b11} |
                                     {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b01)}} & {dram_addr,2'b10} |
                                     {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b10)}} & {dram_addr,2'b01} |
                                     {(L1I_BANK_SET_INDEX_WIDTH + 2){(cache_way_idx_dram_i == 2'b11)}} & {dram_addr,2'b00} ;
    end

    assign dram_addr_ram_o[jj]  = cache_rd_valid_dram_i ? dram_addr_by_offset : dram_addr_by_way[jj];
    assign dram_wdat_ram_o[jj]  = dram_wdat_way[jj];
    assign dram_w_boundary_ram_o[jj]  = dram_w_boundary_way[jj];
    assign dram_w_rvc_mask_ram_o[jj]  = dram_w_rvc_mask_way[jj];
    assign cache_way_en_dram_seg[jj] = |cache_way_byte_en_dram_seg[jj];
//    assign dram_wen_biten_o[jj] = {(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM){dram_wen_o[jj]}};


    if(jj==0) begin
      assign dram_rdat_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
    end
    if(jj==1) begin
      assign dram_rdat_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
    end
    if(jj==2) begin
      assign dram_rdat_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
    end
    if(jj==3) begin
      assign dram_rdat_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
      assign dram_rdat_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM)] =dram_rdat_i [jj];
    end


    if(jj==0) begin
      assign dram_r_boundary_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
    end
    if(jj==1) begin
      assign dram_r_boundary_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
    end
    if(jj==2) begin
      assign dram_r_boundary_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
    end
    if(jj==3) begin
      assign dram_r_boundary_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
      assign dram_r_boundary_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_boundary_i [jj];
    end

    if(jj==0) begin
      assign dram_r_rvc_mask_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
    end
    if(jj==1) begin
      assign dram_r_rvc_mask_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
    end
    if(jj==2) begin
      assign dram_r_rvc_mask_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
    end
    if(jj==3) begin
      assign dram_r_rvc_mask_all_way_seg_realigned_offset0 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 3+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset1 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 2+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset2 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 1+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
      assign dram_r_rvc_mask_all_way_seg_realigned_offset3 [(L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM) * 0+: (L1I_BANK_LINE_DATA_SIZE/16*2/L1I_BANK_WAY_NUM)] =dram_r_rvc_mask_i [jj];
    end

  end
endgenerate

//cache result sel
assign dram_rdat_all_way_seg_realigned  = {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b00)}}& dram_rdat_all_way_seg_realigned_offset0 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b01)}}& dram_rdat_all_way_seg_realigned_offset1 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b10)}}& dram_rdat_all_way_seg_realigned_offset2 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b11)}}& dram_rdat_all_way_seg_realigned_offset3 ;
assign dram_rdat_all_o = dram_rdat_all_way_seg_realigned;

assign dram_r_boundary_all_way_seg_realigned  = {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b00)}}& dram_r_boundary_all_way_seg_realigned_offset0 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b01)}}& dram_r_boundary_all_way_seg_realigned_offset1 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b10)}}& dram_r_boundary_all_way_seg_realigned_offset2 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b11)}}& dram_r_boundary_all_way_seg_realigned_offset3 ;
assign dram_r_boundary_all_o = dram_r_boundary_all_way_seg_realigned;

assign dram_r_rvc_mask_all_way_seg_realigned  = {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b00)}}& dram_r_rvc_mask_all_way_seg_realigned_offset0 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b01)}}& dram_r_rvc_mask_all_way_seg_realigned_offset1 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b10)}}& dram_r_rvc_mask_all_way_seg_realigned_offset2 |
                                          {(L1I_BANK_LINE_DATA_SIZE){(cache_offset_dram_s1_i[5:4] == 2'b11)}}& dram_r_rvc_mask_all_way_seg_realigned_offset3 ;
assign dram_r_rvc_mask_all_o = dram_r_rvc_mask_all_way_seg_realigned;

endmodule