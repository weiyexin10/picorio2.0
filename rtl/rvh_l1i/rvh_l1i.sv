module rvh_l1i
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
    // import rvh_lsu_pkg::*;
    import rvh_l1i_pkg::*;
(
    // FETCH -> I$ : IFETCH request
    input  logic                          fetch_l1i_if_req_vld_i,
    input  logic [ FTQ_TAG_LEN-1:0]       fetch_l1i_if_req_if_tag_i,
    input  logic [   L1I_INDEX_WIDTH-1:0] fetch_l1i_if_req_index_i,
    input  logic [  L1I_OFFSET_WIDTH-1:0] fetch_l1i_if_req_offset_i,
    input  logic [     L1I_TAG_WIDTH-1:0] fetch_l1i_if_req_vtag_i, // vtag
    output logic                          fetch_l1i_if_req_rdy_o,

    // PREFETCH -> I$: tag checking
    input  logic                          prefetch_l1i_if_req_vld_i,
    input  logic [ FTQ_TAG_LEN-1:0]       prefetch_l1i_if_req_if_tag_i,
    input  logic [   L1I_INDEX_WIDTH-1:0] prefetch_l1i_if_req_index_i,
    input  logic [  L1I_OFFSET_WIDTH-1:0] prefetch_l1i_if_req_offset_i,
    input  logic [     L1I_TAG_WIDTH-1:0] prefetch_l1i_if_req_vtag_i, // vtag
    output logic                          prefetch_l1i_if_req_rdy_o,

    // FETCH -> I$ : ITLB response
    input  logic                          fetch_l1i_if_itlb_resp_vld_i,
    input  logic [         PPN_WIDTH-1:0] fetch_l1i_if_itlb_resp_ppn_i,
    input  logic                          fetch_l1i_if_itlb_resp_excp_vld_i,
    input  logic                          fetch_l1i_if_itlb_resp_hit_i,

    // FETCH -> I$ : ITLB response
    input  logic                          prefetch_l1i_if_itlb_resp_vld_i,
    input  logic [         PPN_WIDTH-1:0] prefetch_l1i_if_itlb_resp_ppn_i,
    input  logic                          prefetch_l1i_if_itlb_resp_excp_vld_i,
    input  logic                          prefetch_l1i_if_itlb_resp_hit_i,

    // I$ -> FETCH : resp: s2 cycle
    output logic                           l1i_fetch_if_resp_vld_o,
    output logic [FTQ_TAG_LEN-1:0]         l1i_fetch_if_resp_if_tag_o,
    output logic [L1I_FETCH_WIDTH-1:0]     l1i_fetch_if_resp_data_o,
    output logic [L1I_FETCH_WIDTH/16*2-1:0]l1i_fetch_if_resp_boundary_o,
    output logic [L1I_FETCH_WIDTH/16*2-1:0]l1i_fetch_if_resp_rvc_mask_o,
    output logic                           l1i_fetch_if_resp_two_blks_o,
    // I$ -> FETCH : resp: s1 cycle
    output  logic                          fetch_l1i_if_hit_o,
    output  logic                          fetch_l1i_if_inflight_o,
    output  logic                          prefetch_l1i_if_hit_o,

    // L1I -> L2 : Request
      // mshr -> mem bus
      // AR
    output logic              l1i_l2_req_arvalid_o,
    input  logic              l1i_l2_req_arready_i,
    output cache_mem_if_ar_t  l1i_l2_req_ar_o,
      // ewrq -> mem bus
      // AW 
    output logic              l1i_l2_req_awvalid_o,
    input  logic              l1i_l2_req_awready_i,
    output cache_mem_if_aw_t  l1i_l2_req_aw_o,
      // W 
    output logic              l1i_l2_req_wvalid_o,
    input  logic              l1i_l2_req_wready_i,
    output cache_mem_if_w_t   l1i_l2_req_w_o,
    // L1I -> L2 : Response
      // B
    input  logic              l2_l1i_resp_bvalid_i,
    output logic              l2_l1i_resp_bready_o,
    input  cache_mem_if_b_t   l2_l1i_resp_b_i,
      // mem bus -> mlfb
      // R
    input  logic              l2_l1i_resp_rvalid_i,
    output logic              l2_l1i_resp_rready_o,
    input cache_mem_if_r_t    l2_l1i_resp_r_i,


    input logic flush_i,

    input  logic fencei_flush_vld_i,
    output logic fencei_flush_grant_o,

    input clk,
    input rst
);

genvar i, j, k;

// l1i arb -> l1i banks
// Request
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_req_vld;                                      
logic [L1I_BANK_ID_NUM-1:0][ FTQ_TAG_LEN-1:0]               l1i_arb_bank_req_if_tag;
logic [L1I_BANK_ID_NUM-1:0][PREG_TAG_WIDTH-1:0]             l1i_arb_bank_req_prd;                                      
logic [L1I_BANK_ID_NUM-1:0][  LDU_OP_WIDTH-1:0]             l1i_arb_bank_req_opcode;                                                                                                                                 
logic [L1I_BANK_ID_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH-1:0]   l1i_arb_bank_req_idx;                                      
logic [L1I_BANK_ID_NUM-1:0][L1I_BANK_OFFSET_WIDTH-1:0]      l1i_arb_bank_req_offset; 
logic [L1I_BANK_ID_NUM-1:0][L1I_BANK_TAG_WIDTH-1:0]         l1i_arb_bank_req_vtag;                          
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_req_is_prefetch;
logic [2-1:0][  L1I_BANK_ID_INDEX_WIDTH-1:0]                l1i_arb_hit_bank_id;   
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_fetch_rdy;
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_prefetch_rdy;
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_prefetch_hit;
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_fetch_inflight;
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_fetch_hit_when_req;
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_fetch_hit_when_fill;

// ITLB -> I$                                                                                           
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_itlb_resp_vld;                              
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_itlb_resp_excp_vld; // s1 kill              
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_itlb_resp_hit;      // s1 kill              
logic [L1I_BANK_ID_NUM-1:0][     PPN_WIDTH-1:0]             l1i_arb_bank_itlb_resp_ppn; // VIPT, get at s1 if tlb hit

// I$ --> fetch
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_resp_vld;
logic [L1I_BANK_ID_NUM-1:0][FTQ_TAG_LEN-1:0]                l1i_arb_bank_resp_if_tag;
logic [L1I_BANK_ID_NUM-1:0][L1I_FETCH_WIDTH-1:0]            l1i_arb_bank_resp_data;
logic [L1I_BANK_ID_NUM-1:0][L1I_FETCH_WIDTH/16*2-1:0]       l1i_arb_bank_resp_boundary;
logic [L1I_BANK_ID_NUM-1:0][L1I_FETCH_WIDTH/16*2-1:0]       l1i_arb_bank_resp_rvc_mask;
logic [L1I_BANK_ID_NUM-1:0]                                 l1i_arb_bank_resp_two_blks;

// L1I banks -> axi arb
  // AR
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_arvalid;
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_arready;
cache_mem_if_ar_t [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_ar;
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_is_fetch;
  // AW                   
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_awvalid;
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_awready;
cache_mem_if_aw_t [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_aw;
  // W                    
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_wvalid;
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_wready;
cache_mem_if_w_t  [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_w;
  // B                    
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_bvalid;
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_bready;
cache_mem_if_b_t  [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_b;
  // R                    
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_rvalid;
logic             [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_rready;
cache_mem_if_r_t  [L1I_BANK_ID_NUM-1:0]                     l1i_bank_axi_arb_r;

logic                                                       fetch_l1i_if_req_rdy;
logic                                                       prefetch_l1i_if_req_rdy;
l1i_fencei_state_t                                          fencei_state, next_fencei_state;
logic [L1I_BANK_ID_NUM-1:0]                                 fencei_flush_grant_per_bank_ff;
logic [L1I_BANK_ID_NUM-1:0]                                 fencei_flush_grant_per_bank;

logic                          place_holder0, place_holder1, place_holder2;
rvh_l1i_bank_input_arb u_l1i_input_arb
(
    .if_l1i_if_req_vld_i                ({prefetch_l1i_if_req_vld_i, fetch_l1i_if_req_vld_i}),
    .if_l1i_if_req_if_tag_i             ({prefetch_l1i_if_req_if_tag_i, fetch_l1i_if_req_if_tag_i}),
    .if_l1i_if_req_idx_i                ({prefetch_l1i_if_req_index_i, fetch_l1i_if_req_index_i}), //
    .if_l1i_if_req_offset_i             ({prefetch_l1i_if_req_offset_i, fetch_l1i_if_req_offset_i}), //
    .if_l1i_if_req_vtag_i               ({prefetch_l1i_if_req_vtag_i, fetch_l1i_if_req_vtag_i}), // vtag
    .if_l1i_if_req_rdy_o                ({prefetch_l1i_if_req_rdy, fetch_l1i_if_req_rdy}),
    .if_l1i_if_req_hit_bank_id_o        (l1i_arb_hit_bank_id),

  // FETCH -> I$ : IFETCH Request
    .l1i_bank_if_req_vld_o              (l1i_arb_bank_req_vld),
    .l1i_bank_if_req_if_tag_o           (l1i_arb_bank_req_if_tag),
    .l1i_bank_if_req_idx_o              (l1i_arb_bank_req_idx),
    .l1i_bank_if_req_offset_o           (l1i_arb_bank_req_offset),
    .l1i_bank_if_req_vtag_o             (l1i_arb_bank_req_vtag),
    .l1i_bank_if_req_is_prefetch_o      (l1i_arb_bank_req_is_prefetch),
    .l1i_bank_if_req_fetch_rdy_i        (l1i_arb_bank_fetch_rdy),
    .l1i_bank_if_req_prefetch_rdy_i     (l1i_arb_bank_prefetch_rdy),

    .fetch_l1i_if_prefetch_hit_o        ({prefetch_l1i_if_hit_o, place_holder0}),
    .fetch_l1i_if_fetch_hit_o           ({place_holder1, fetch_l1i_if_hit_o}),
    .fetch_l1i_if_fetch_inflight_o      ({place_holder2, fetch_l1i_if_inflight_o}),

    .fetch_l1i_if_prefetch_hit_i        (l1i_arb_bank_prefetch_hit),
    .fetch_l1i_if_fetch_inflight_i      (l1i_arb_bank_fetch_inflight),
    .fetch_l1i_if_fetch_hit_when_req_i  (l1i_arb_bank_fetch_hit_when_req),
    .fetch_l1i_if_fetch_hit_when_fill_i (l1i_arb_bank_fetch_hit_when_fill),

    .if_l1i_itlb_resp_vld_i             ({prefetch_l1i_if_itlb_resp_vld_i, fetch_l1i_if_itlb_resp_vld_i}),
    .if_l1i_itlb_resp_ppn_i             ({prefetch_l1i_if_itlb_resp_ppn_i, fetch_l1i_if_itlb_resp_ppn_i}),
    .if_l1i_itlb_resp_excp_vld_i        ({prefetch_l1i_if_itlb_resp_excp_vld_i, fetch_l1i_if_itlb_resp_excp_vld_i}),
    .if_l1i_itlb_resp_hit_i             ({prefetch_l1i_if_itlb_resp_hit_i, fetch_l1i_if_itlb_resp_hit_i}),

     // ITLB -> I$
    .itlb_l1i_resp_vld_o                (l1i_arb_bank_itlb_resp_vld),
    .itlb_l1i_resp_excp_vld_o           (l1i_arb_bank_itlb_resp_excp_vld), // s1 kill
    .itlb_l1i_resp_hit_o                (l1i_arb_bank_itlb_resp_hit),      // s1 kill
    .itlb_l1i_resp_ppn_o                (l1i_arb_bank_itlb_resp_ppn), // VIPT, get at s1 if tlb hit

    .clk                                (clk),
    .rst                                (rst)
);

// cache bank gen
generate
  for(i = 0; i < L1I_BANK_ID_NUM; i++) begin: gen_l1i_bank
    rvh_l1i_bank
    #(
      .BANK_ID (i)
    ) L1I_CACHE_BANK_U (
        // LS_PIPE -> D$ : LD Request
        .fetch_l1i_if_req_vld_i                 (l1i_arb_bank_req_vld[i]),
        .fetch_l1i_if_req_if_tag_i              (l1i_arb_bank_req_if_tag[i]),
        .fetch_l1i_if_req_prd_i                 ({PREG_TAG_WIDTH{1'b0}}),
        .fetch_l1i_if_req_opcode_i              (LDU_LD),
        .fetch_l1i_if_req_prefetch_i            (l1i_arb_bank_req_is_prefetch[i]),

        .fetch_l1i_if_req_idx_i                 (l1i_arb_bank_req_idx[i]),
        .fetch_l1i_if_req_offset_i              (l1i_arb_bank_req_offset[i]),
        .fetch_l1i_if_req_vtag_i                (l1i_arb_bank_req_vtag[i]),
        .fetch_l1i_if_prefetch_hit_o            (l1i_arb_bank_prefetch_hit[i]),
        .fetch_l1i_if_prefetch_pfb_tag_o        ( ),
        .fetch_l1i_if_fetch_hit_when_req_o      (l1i_arb_bank_fetch_hit_when_req[i]),
        .fetch_l1i_if_fetch_hit_when_fill_o     (l1i_arb_bank_fetch_hit_when_fill[i]),

        .fetch_l1i_if_fetch_inflight_o          (l1i_arb_bank_fetch_inflight[i]),
        .fetch_l1i_if_fetch_req_rdy_o           (l1i_arb_bank_fetch_rdy[i]),
        .fetch_l1i_if_prefetch_req_rdy_o        (l1i_arb_bank_prefetch_rdy[i]),

        // DTLB -> D$ 
        .dtlb_l1i_resp_vld_i                    (l1i_arb_bank_itlb_resp_vld[i]     ),
        .dtlb_l1i_resp_excp_vld_i               (l1i_arb_bank_itlb_resp_excp_vld[i]), // s1 kill
        .dtlb_l1i_resp_hit_i                    (l1i_arb_bank_itlb_resp_hit[i]     ),      // s1 kill
        .dtlb_l1i_resp_ppn_i                    (l1i_arb_bank_itlb_resp_ppn[i]     ), // VIPT, get at s1 if tlb hit
        .dtlb_l1i_resp_rdy_o                    ( ),

        // I$ -> FETCH : resp 
        .l1i_fetch_if_resp_vld_o                (l1i_arb_bank_resp_vld[i]),
        .l1i_fetch_if_resp_if_tag_o             (l1i_arb_bank_resp_if_tag[i]),
        .l1i_fetch_if_resp_data_o               (l1i_arb_bank_resp_data[i]),
        .l1i_fetch_if_resp_two_blks_o           (l1i_arb_bank_resp_two_blks[i]),
        .l1i_fetch_if_resp_boundary_o           (l1i_arb_bank_resp_boundary[i]),
        .l1i_fetch_if_resp_rvc_mask_o           (l1i_arb_bank_resp_rvc_mask[i]),
        
        // L1I -> L2 : Request
        // mshr -> mem bus
        // AR
        .l2_req_if_arvalid                      (l1i_bank_axi_arb_arvalid[i]),
        .l2_req_if_arready                      (l1i_bank_axi_arb_arready[i]),
        .l2_req_if_ar                           (l1i_bank_axi_arb_ar     [i]),
        .l2_req_if_ar_is_fetch                  (l1i_bank_axi_arb_is_fetch[i]),
          // ewrq -> mem bus                                             
          // AW                                                          
        .l2_req_if_awvalid                      (l1i_bank_axi_arb_awvalid[i]),
        .l2_req_if_awready                      (l1i_bank_axi_arb_awready[i]),
        .l2_req_if_aw                           (l1i_bank_axi_arb_aw     [i]),                       
          // W                                                          
        .l2_req_if_wvalid                       (l1i_bank_axi_arb_wvalid [i]),
        .l2_req_if_wready                       (l1i_bank_axi_arb_wready [i]),
        .l2_req_if_w                            (l1i_bank_axi_arb_w      [i]),                       
          // B                                                         
        .l2_resp_if_bvalid                      (l1i_bank_axi_arb_bvalid [i]),
        .l2_resp_if_bready                      (l1i_bank_axi_arb_bready [i]),
        .l2_resp_if_b                           (l1i_bank_axi_arb_b      [i]),                        
          // mem bus -> mlfb
          // R              
        .l2_resp_if_rvalid                      (l1i_bank_axi_arb_rvalid [i]),
        .l2_resp_if_rready                      (l1i_bank_axi_arb_rready [i]),
        .l2_resp_if_r                           (l1i_bank_axi_arb_r      [i]),
        
        // L1I -> L2 : Response
        .flush_i                                (flush_i              ),

        // make all valid line invalid
        .fencei_flush_vld_i                     (fencei_flush_vld_i  ),
        .fencei_flush_grant_o                   (fencei_flush_grant_per_bank[i]),

        .clk                                    (clk                  ),
        .rst                                    (rst                  )
    );
  end
endgenerate

rvh_l1i_bank_axi_arb 
#(
  .INPUT_PORT_NUM (L1I_BANK_ID_NUM)
) L1I_CACHE_BANK_AXI_ARB_U (
    // L1D banks -> axi arb
      // AR
    .l1i_bank_axi_arb_is_fetch_i                (l1i_bank_axi_arb_is_fetch ),
    .l1i_bank_axi_arb_arvalid_i                 (l1i_bank_axi_arb_arvalid  ),
    .l1i_bank_axi_arb_arready_o                 (l1i_bank_axi_arb_arready  ),
    .l1i_bank_axi_arb_ar_i                      (l1i_bank_axi_arb_ar       ),
      // AW                                                           
    .l1i_bank_axi_arb_awvalid_i                 (l1i_bank_axi_arb_awvalid  ),                     
    .l1i_bank_axi_arb_awready_o                 (l1i_bank_axi_arb_awready  ),
    .l1i_bank_axi_arb_aw_i                      (l1i_bank_axi_arb_aw       ),
      // W                                                             
    .l1i_bank_axi_arb_wvalid_i                  (l1i_bank_axi_arb_wvalid   ),                      
    .l1i_bank_axi_arb_wready_o                  (l1i_bank_axi_arb_wready   ),
    .l1i_bank_axi_arb_w_i                       (l1i_bank_axi_arb_w        ),
      // B                                                             
    .l1i_bank_axi_arb_bvalid_o                  (l1i_bank_axi_arb_bvalid   ),                     
    .l1i_bank_axi_arb_bready_i                  (l1i_bank_axi_arb_bready   ),
    .l1i_bank_axi_arb_b_o                       (l1i_bank_axi_arb_b        ),
      // R
    .l1i_bank_axi_arb_rvalid_o                  (l1i_bank_axi_arb_rvalid   ),                     
    .l1i_bank_axi_arb_rready_i                  (l1i_bank_axi_arb_rready   ),                     
    .l1i_bank_axi_arb_r_o                       (l1i_bank_axi_arb_r        ),
                                               
    // axi arb -> L2                           
      // AR                                   
    .axi_arb_l2_arvalid_o                       (l1i_l2_req_arvalid_o      ),
    .axi_arb_l2_arready_i                       (l1i_l2_req_arready_i      ),
    .axi_arb_l2_ar_o                            (l1i_l2_req_ar_o           ),             
      // AW
    .axi_arb_l2_awvalid_o                       (l1i_l2_req_awvalid_o      ),
    .axi_arb_l2_awready_i                       (l1i_l2_req_awready_i      ),
    .axi_arb_l2_aw_o                            (l1i_l2_req_aw_o           ),
      // W
    .axi_arb_l2_wvalid_o                        (l1i_l2_req_wvalid_o       ),
    .axi_arb_l2_wready_i                        (l1i_l2_req_wready_i       ),
    .axi_arb_l2_w_o                             (l1i_l2_req_w_o            ),
      // B
    .axi_arb_l2_bvalid_i                        (l2_l1i_resp_bvalid_i      ),            
    .axi_arb_l2_bready_o                        (l2_l1i_resp_bready_o      ),
    .axi_arb_l2_b_i                             (l2_l1i_resp_b_i           ),
      // R
    .axi_arb_l2_rvalid_i                        (l2_l1i_resp_rvalid_i      ),             
    .axi_arb_l2_rready_o                        (l2_l1i_resp_rready_o      ),             
    .axi_arb_l2_r_i                             (l2_l1i_resp_r_i           ),
                                              
    .clk    (clk),                                     
    .rst    (rst)
    
);

// generate output 
assign l1i_fetch_if_resp_vld_o = |l1i_arb_bank_resp_vld;

onehot_mux
#(
    .SOURCE_COUNT(L1I_BANK_ID_NUM),
    .DATA_WIDTH(FTQ_TAG_LEN)
) if_tag_one_hot_mux(
    .sel_i(l1i_arb_bank_resp_vld),
    .data_i(l1i_arb_bank_resp_if_tag),
    .data_o(l1i_fetch_if_resp_if_tag_o)
);

onehot_mux
#(
    .SOURCE_COUNT(L1I_BANK_ID_NUM),
    .DATA_WIDTH(L1I_FETCH_WIDTH)
) resp_data_one_hot_mux(
    .sel_i(l1i_arb_bank_resp_vld),
    .data_i(l1i_arb_bank_resp_data),
    .data_o(l1i_fetch_if_resp_data_o)
);

onehot_mux
#(
    .SOURCE_COUNT(L1I_BANK_ID_NUM),
    .DATA_WIDTH(L1I_FETCH_WIDTH/16*2)
) resp_boundary_one_hot_mux(
    .sel_i(l1i_arb_bank_resp_vld),
    .data_i(l1i_arb_bank_resp_boundary),
    .data_o(l1i_fetch_if_resp_boundary_o)
);

onehot_mux
#(
    .SOURCE_COUNT(L1I_BANK_ID_NUM),
    .DATA_WIDTH(L1I_FETCH_WIDTH/16*2)
) resp_rvc_mask_one_hot_mux(
    .sel_i(l1i_arb_bank_resp_vld),
    .data_i(l1i_arb_bank_resp_rvc_mask),
    .data_o(l1i_fetch_if_resp_rvc_mask_o)
);

onehot_mux
#(
    .SOURCE_COUNT(L1I_BANK_ID_NUM),
    .DATA_WIDTH(1)
) resp_two_blks_one_hot_mux(
    .sel_i(l1i_arb_bank_resp_vld),
    .data_i(l1i_arb_bank_resp_two_blks),
    .data_o(l1i_fetch_if_resp_two_blks_o)
);

// fencei
always @(posedge clk) begin
  if (rst) begin
    fencei_state <= FENCEI_IDLE;
  end
  else begin
    fencei_state <= next_fencei_state;
  end
end

always_comb begin
  next_fencei_state = fencei_state;
  unique case (fencei_state) 
    FENCEI_IDLE: begin
      if (fencei_flush_vld_i & ~(&fencei_flush_grant_per_bank)) begin
        next_fencei_state = FENCEI_WAITING_FOR_BANK_GRANT;
      end
    end
    FENCEI_WAITING_FOR_BANK_GRANT: begin
      if (&fencei_flush_grant_per_bank_ff) begin
        next_fencei_state = FENCEI_IDLE;
      end
    end
    default:;
  endcase
end

always_ff @(posedge clk) begin
  if (rst) begin
    fencei_flush_grant_per_bank_ff <= 0;
  end
  else if ((fencei_state == FENCEI_WAITING_FOR_BANK_GRANT) | (next_fencei_state == FENCEI_WAITING_FOR_BANK_GRANT)) begin
    fencei_flush_grant_per_bank_ff <= fencei_flush_grant_per_bank_ff | fencei_flush_grant_per_bank;
  end
  else begin
    fencei_flush_grant_per_bank_ff <= 0;
  end
end

assign fetch_l1i_if_req_rdy_o = fetch_l1i_if_req_rdy & ((fencei_state == FENCEI_IDLE) & (next_fencei_state == FENCEI_IDLE));
assign prefetch_l1i_if_req_rdy_o = prefetch_l1i_if_req_rdy & ((fencei_state == FENCEI_IDLE) & (next_fencei_state == FENCEI_IDLE));
assign fencei_flush_grant_o = (fencei_flush_vld_i & (next_fencei_state == FENCEI_IDLE)) | ((fencei_state == FENCEI_WAITING_FOR_BANK_GRANT) & (next_fencei_state == FENCEI_IDLE));

`ifndef SYNTHESIS
int ic_debug_print;
initial begin
  ic_debug_print = 0;
  $value$plusargs("ic_debug_print=%d",ic_debug_print);
end

always_ff @(posedge clk) begin
  if (!rst) begin
    if (l1i_arb_bank_resp_vld & (l1i_arb_bank_resp_vld - 1'b1) != 0) begin
      $fatal("more than one bank return valid data");
    end
  end
end

logic [63:0] cycle;
always_ff @(posedge clk or negedge rst) begin
  if(~rst) begin
    cycle <= '0;
  end else begin
    cycle <= cycle + 1;
  end
end

always_ff @(posedge clk) begin
  if(ic_debug_print) begin
    for(int i = 0; i < 1; i++) begin
      if(fetch_l1i_if_req_vld_i & fetch_l1i_if_req_rdy_o) begin
        $display("\n\n====================");
        $display("l1i @ cycle = %d, load req[port %d, l1i bank %d] handshake", cycle, i[$clog2(LSU_ADDR_PIPE_COUNT)-1:0], 0);
        // $display("lsu_id    = 0x%x", lsu_l1i_if_reqif_req.lsu_id);
        $display("if_tag    = 0x%x", fetch_l1i_if_req_if_tag_i);
        // $display("opcode    = 0x%x", fetch_l1i_if_req_opcode_i);
        $display("vaddr     = 0x%x", {fetch_l1i_if_req_vtag_i, fetch_l1i_if_req_index_i, fetch_l1i_if_req_offset_i});
        // $display("prd_tag   = 0x%x", fetch_l1i_if_req_prd_i);
        $display("====================");
      end
    end

    for(int i = 0; i < 1; i++) begin
      if(l1i_fetch_if_resp_vld_o) begin
        $display("\n\n====================");
        $display("l1i @ cycle = %d, load resp[port %d] handshake", cycle, i[$clog2(LSU_ADDR_PIPE_COUNT)-1:0]);
        $display("if_tag   = 0x%x", l1i_fetch_if_resp_if_tag_o);
        $write("if_data    = 0x");
        for(int j = L1I_FETCH_WIDTH/64-1; j >=0; j--) begin
          $write("%h", l1i_fetch_if_resp_data_o[j*64+:64]);
        end
        $display("\n====================");
      end
    end

    if(l1i_l2_req_awvalid_o & l1i_l2_req_awready_i) begin
      $display("\n\n====================");
      $display("l1i @ cycle = %d, write back aw req handshake", cycle);
      $display("awaddr    = 0x%x", l1i_l2_req_aw_o.awaddr);
      $display("awlen     = 0x%x", l1i_l2_req_aw_o.awlen);
      $display("awsize    = 0x%x", l1i_l2_req_aw_o.awsize);
      $display("awid      = 0x%x", l1i_l2_req_aw_o.awid);
      $display("awburst   = 0x%x", l1i_l2_req_aw_o.awburst);
      $display("====================");
    end
    if(l1i_l2_req_wvalid_o & l1i_l2_req_wready_i) begin
      $display("\n\n====================");
      $display("l1i @ cycle = %d, write back w req handshake", cycle);
      $display("wlast     = 0x%x", l1i_l2_req_w_o.wlast);
      $display("wid       = 0x%x", l1i_l2_req_w_o.wid);
      $write("wdata     = 0x");
      for(int i = MEM_DATA_WIDTH/64-1; i >=0; i--) begin
        $write("%h", l1i_l2_req_w_o.wdata[i*64+:64]);
      end
      $display("\n====================");
    end
    if(l1i_l2_req_arvalid_o & l1i_l2_req_arready_i) begin
      $display("\n\n====================");
      $display("l1i @ cycle = %d, l1i miss ar req handshake", cycle);
      $display("araddr    = 0x%x", l1i_l2_req_ar_o.araddr);
      $display("arlen     = 0x%x", l1i_l2_req_ar_o.arlen);
      $display("arsize    = 0x%x", l1i_l2_req_ar_o.arsize);
      $display("arid      = 0x%x", l1i_l2_req_ar_o.arid);
      $display("arburst   = 0x%x", l1i_l2_req_ar_o.arburst);
      $display("====================");
    end
    if(l2_l1i_resp_rvalid_i & l2_l1i_resp_rready_o) begin
      $display("\n\n====================");
      $display("l1i @ cycle = %d, l1i miss r resp handshake", cycle);
      $display("mesi_sta  = 0x%x", l2_l1i_resp_r_i.mesi_sta);
      $display("rresp     = 0x%x", l2_l1i_resp_r_i.rresp);
      $display("rlast     = 0x%x", l2_l1i_resp_r_i.rlast);
      $display("rid       = 0x%x", l2_l1i_resp_r_i.rid);
      $write("rdata     = 0x");
      for(int i = MEM_DATA_WIDTH/64-1; i >=0; i--) begin
        $write("%h", l2_l1i_resp_r_i.dat[i*64+:64]);
      end
      $display("\n====================");
    end
  end
end
`endif

endmodule : rvh_l1i
