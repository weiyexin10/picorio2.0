module rvh_l1i_dec
    import uop_encoding_pkg::*;
    import rvh_l1i_pkg::*;
(
    input  logic                          is_if_req_vld_i,
    input  logic                          is_st_req_vld_i,
    input  logic [      LDU_OP_WIDTH-1:0] fetch_l1i_if_req_opcode_i,
    input  logic [      STU_OP_WIDTH-1:0] fetch_l1i_st_req_opcode_i,
    output rrv64_l1i_req_type_dec_t req_type_dec_o
);

assign req_type_dec_o.is_ld = is_if_req_vld_i & (
                            (fetch_l1i_if_req_opcode_i == LDU_LB)    |
                            (fetch_l1i_if_req_opcode_i == LDU_LBU)   |
                            (fetch_l1i_if_req_opcode_i == LDU_LH)    |
                            (fetch_l1i_if_req_opcode_i == LDU_LHU)   |
                            (fetch_l1i_if_req_opcode_i == LDU_LW)    |
                            (fetch_l1i_if_req_opcode_i == LDU_LWU)   |
                            (fetch_l1i_if_req_opcode_i == LDU_LD ));//    |
                            // (fetch_l1i_if_req_opcode_i == LSU_FLW)   |
                            // (fetch_l1i_if_req_opcode_i == LSU_FLD)) ;
assign req_type_dec_o.is_st = is_st_req_vld_i & (
                            (fetch_l1i_st_req_opcode_i == STU_SB)    |
                            (fetch_l1i_st_req_opcode_i == STU_SH)    |
                            (fetch_l1i_st_req_opcode_i == STU_SW)    |
                            (fetch_l1i_st_req_opcode_i == STU_SD));//    |
                            // (fetch_l1i_st_req_opcode_i == LSU_FSW)   |
                            // (fetch_l1i_st_req_opcode_i == LSU_FSD)) ;

assign req_type_dec_o.is_lr = 1'b0;

assign req_type_dec_o.is_sc = 1'b0;
assign req_type_dec_o.is_amo = is_st_req_vld_i & (
                             (fetch_l1i_st_req_opcode_i == STU_AMOSWAPW) |
                             (fetch_l1i_st_req_opcode_i == STU_AMOSWAPD) |
                             (fetch_l1i_st_req_opcode_i == STU_AMOADDW)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOADDD)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOANDW)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOANDD)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOORW)   |
                             (fetch_l1i_st_req_opcode_i == STU_AMOORD)   |
                             (fetch_l1i_st_req_opcode_i == STU_AMOXORW)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOXORD)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMAXW)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMAXD)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMAXUW) |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMAXUD) |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMINW)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMIND)  |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMINUW) |
                             (fetch_l1i_st_req_opcode_i == STU_AMOMINUD));


assign req_type_dec_o.op_b = is_if_req_vld_i & ( 
                           (fetch_l1i_if_req_opcode_i ==LDU_LB) |
                           (fetch_l1i_if_req_opcode_i == LDU_LBU)) |
                           is_st_req_vld_i &
                           (fetch_l1i_st_req_opcode_i == STU_SB ) ;
assign req_type_dec_o.op_hw = is_if_req_vld_i & (
                            (fetch_l1i_if_req_opcode_i == LDU_LH )|
                            (fetch_l1i_if_req_opcode_i == LDU_LHU)) |
                            is_st_req_vld_i &
                            (fetch_l1i_st_req_opcode_i == STU_SH );
assign req_type_dec_o.op_w =  is_if_req_vld_i & (
                            (fetch_l1i_if_req_opcode_i == LDU_LW)    |
                            (fetch_l1i_if_req_opcode_i == LDU_LWU))   |
                            // (fetch_l1i_if_req_opcode_i == LSU_FLW))   |
                            is_st_req_vld_i & (
                            (fetch_l1i_st_req_opcode_i == STU_SW)    |
                            // (fetch_l1i_st_req_opcode_i == LSU_FSW)   |
                            (fetch_l1i_st_req_opcode_i == STU_AMOSWAPW) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOADDW ) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOANDW ) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOORW )  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOXORW ) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMAXW ) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMAXUW) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMINW ) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMINUW));
assign req_type_dec_o.op_dw = is_if_req_vld_i & (
                            (fetch_l1i_if_req_opcode_i == LDU_LD ))   |
                            // (fetch_l1i_if_req_opcode_i == LSU_FLD))   |
                            is_st_req_vld_i & (
                            (fetch_l1i_st_req_opcode_i == STU_SD)    |
                            // (fetch_l1i_st_req_opcode_i == LSU_FSD)   |
                            (fetch_l1i_st_req_opcode_i == STU_AMOSWAPD)  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOADDD)   |
                            (fetch_l1i_st_req_opcode_i == STU_AMOANDD)   |
                            (fetch_l1i_st_req_opcode_i == STU_AMOORD)    |
                            (fetch_l1i_st_req_opcode_i == STU_AMOXORD )  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMAXD )  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMAXUD ) |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMIND )  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMINUD));
assign req_type_dec_o.ld_u =  is_if_req_vld_i & (
                            (fetch_l1i_if_req_opcode_i == LDU_LBU )  |
                            (fetch_l1i_if_req_opcode_i == LDU_LHU )  |
                            (fetch_l1i_if_req_opcode_i == LDU_LWU ))  |
                            is_st_req_vld_i & (
                            (fetch_l1i_st_req_opcode_i == STU_AMOMAXUW)  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMAXUD)  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMINUW)  |
                            (fetch_l1i_st_req_opcode_i == STU_AMOMINUD));

assign req_type_dec_o.amo_type =  ((fetch_l1i_st_req_opcode_i == STU_AMOSWAPW) |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOSWAPD)) ? AMOSWAP :
                                ((fetch_l1i_st_req_opcode_i == STU_AMOADDW)  |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOADDD)) ? AMOADD :
                                ((fetch_l1i_st_req_opcode_i == STU_AMOANDW)  |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOANDD)) ? AMOAND :
                                ((fetch_l1i_st_req_opcode_i == STU_AMOORW)   |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOORD)) ? AMOOR :
                                ((fetch_l1i_st_req_opcode_i == STU_AMOXORW) |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOXORD))? AMOXOR :
                                ((fetch_l1i_st_req_opcode_i == STU_AMOMAXW ) |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOMAXD ) |  
                                 (fetch_l1i_st_req_opcode_i == STU_AMOMAXUW) |
                                 (fetch_l1i_st_req_opcode_i == STU_AMOMAXUD)
                                ) ? AMOMAX : AMOMIN;
                                
endmodule