module rvh_l1i_bank
    import riscv_pkg::*;
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_l1i_pkg::*;
#(
  parameter BANK_ID = 0
)
(

    // LS_PIPE -> D$ : LD Request
    input  logic                                  fetch_l1i_if_req_vld_i,
    input  logic [     FTQ_TAG_LEN-1:0]           fetch_l1i_if_req_if_tag_i,
    input  logic [    PREG_TAG_WIDTH-1:0]         fetch_l1i_if_req_prd_i,
    input  logic [      LDU_OP_WIDTH-1:0]         fetch_l1i_if_req_opcode_i,
    input  logic                                  fetch_l1i_if_req_prefetch_i,
`ifdef RUBY
    input  logic [RRV64_LSU_ID_WIDTH-1:0]         fetch_l1i_if_req_lsu_tag_i,
`endif

    input  logic [L1I_BANK_SET_INDEX_WIDTH-1:0]   fetch_l1i_if_req_idx_i,
    input  logic [L1I_BANK_OFFSET_WIDTH-1:0   ]   fetch_l1i_if_req_offset_i,
    input  logic [L1I_BANK_TAG_WIDTH-1:0      ]   fetch_l1i_if_req_vtag_i,
    output logic                                  fetch_l1i_if_fetch_inflight_o,
    output logic                                  fetch_l1i_if_prefetch_hit_o,
    output logic                                  fetch_l1i_if_fetch_hit_when_req_o,
    output logic                                  fetch_l1i_if_fetch_hit_when_fill_o,
    output logic [PFB_TAG_LEN-1:0]                fetch_l1i_if_prefetch_pfb_tag_o,
    output logic                                  fetch_l1i_if_fetch_req_rdy_o,
    output logic                                  fetch_l1i_if_prefetch_req_rdy_o,

    // DTLB -> D$
    input  logic                                  dtlb_l1i_resp_vld_i,
    input  logic                                  dtlb_l1i_resp_excp_vld_i, // s1 kill
    input  logic                                  dtlb_l1i_resp_hit_i,      // s1 kill
    input  logic [       PPN_WIDTH-1:0]           dtlb_l1i_resp_ppn_i, // VIPT, get at s1 if tlb hit
    output logic                                  dtlb_l1i_resp_rdy_o,

    // I$ -> FETCH : resp
    output logic                                  l1i_fetch_if_resp_vld_o,
    output logic [FTQ_TAG_LEN-1:0]                l1i_fetch_if_resp_if_tag_o,
    output logic [L1I_FETCH_WIDTH-1:0]            l1i_fetch_if_resp_data_o,
    output logic [L1I_FETCH_WIDTH/16*2-1:0]       l1i_fetch_if_resp_boundary_o,
    output logic [L1I_FETCH_WIDTH/16*2-1:0]       l1i_fetch_if_resp_rvc_mask_o,
    output logic                                  l1i_fetch_if_resp_two_blks_o,

    // PTW -> D$ : Request

    // PTW -> D$ : Response

    // L1I -> L2 : Request
      // mshr -> mem bus
      // AR
    output logic                                  l2_req_if_arvalid,
    input  logic                                  l2_req_if_arready,
    output cache_mem_if_ar_t                      l2_req_if_ar,
    output logic                                  l2_req_if_ar_is_fetch,
      // ewrq -> mem bus                    
      // AW                     
    output logic                                  l2_req_if_awvalid,
    input  logic                                  l2_req_if_awready,
    output cache_mem_if_aw_t                      l2_req_if_aw,
      // W                    
    output logic                                  l2_req_if_wvalid,
    input  logic                                  l2_req_if_wready,
    output cache_mem_if_w_t                       l2_req_if_w,
      // B                    
    input  logic                                  l2_resp_if_bvalid,
    output logic                                  l2_resp_if_bready,
    input  cache_mem_if_b_t                       l2_resp_if_b,
      // mem bus -> mlfb                    
      // R                    
    input  logic                                  l2_resp_if_rvalid,
    output logic                                  l2_resp_if_rready,
    input cache_mem_if_r_t                        l2_resp_if_r,
    
    // L1I -> L2 : Response
    input logic                                   flush_i, // kill all the load in pipeline, and mark all the load miss in mshr "no resp"

    // make all dirty line into clean, write back dirty line
    input  logic                                  fencei_flush_vld_i,
    output logic                                  fencei_flush_grant_o,

    input logic                                   clk,
    input logic                                   rst
);

// pipe reg
l1i_pipe_reg_t                cur, nxt;

// pipe valid
logic                         s1_valid, s2_valid;
logic[PADDR_WIDTH-1:0]        s1_paddr, s2_paddr;
logic                         s1_nxt_ena,s2_nxt_ena;
logic                         s1_valid_nxt,s2_valid_nxt;

//decode
rrv64_l1i_req_type_dec_t req_type_dec;

// tag ram access
logic cache_rd_valid_tram;
logic cache_wr_valid_tram;
logic[L1I_BANK_WAY_NUM-1:0] cache_rd_tram_way_en;
logic[L1I_BANK_WAY_NUM-1:0] cache_wr_tram_way_en;
logic[L1I_BANK_SET_INDEX_WIDTH-1:0 ] cache_idx_tram;
logic[L1I_BANK_OFFSET_WIDTH-1:0] cache_offset_tram;

logic[L1I_BANK_WAY_NUM-1:0] tram_cs ;
logic[L1I_BANK_WAY_NUM-1:0] tram_wen ;
// logic[L1I_BANK_WAY_NUM-1:0] tram_wmask;
logic[L1I_BANK_WAY_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH-1:0] tram_addr;
logic[L1I_BANK_TAG_RAM_WORD_WIDTH-1:0] cache_wr_dat_tram;
logic[L1I_BANK_WAY_NUM-1:0][L1I_BANK_TAG_RAM_WORD_WIDTH-1:0] tram_wdat; // +1 for valid bit
logic[L1I_BANK_WAY_NUM-1:0][L1I_BANK_TAG_RAM_WORD_WIDTH-1:0] tram_rdat;


// data ram access
logic cache_rd_valid_dram;
logic cache_wr_valid_dram;
logic[L1I_BANK_SET_INDEX_WIDTH-1:0 ] cache_idx_dram;
// logic[L1I_BANK_OFFSET_WIDTH-1:0] cache_offset_dram;
logic[2-1:0][L1I_BANK_OFFSET_WIDTH-1:0] cache_offset_dram;
logic[2-1:0][L1I_BANK_OFFSET_WIDTH-1:0] fetch_l1i_if_req_offset;
logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] cache_way_idx_dram;
logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] cache_way_idx_dram_s1;
logic[2-1:0][L1I_BANK_OFFSET_WIDTH-1:0   ] cache_offset_dram_s1;
logic[L1I_BANK_WAY_NUM -1:0] cache_way_en_dram_raw;
logic[L1I_BANK_WAY_NUM -1:0] cache_way_en_dram;
logic[L1I_BANK_WAY_NUM -1:0] cache_way_en_dram_seg;
logic[L1I_BANK_WAY_NUM -1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8-1:0] cache_way_byte_en_dram_seg;
logic[XLEN -1:0] cache_wr_dat_dram;


logic [2-1:0][L1I_BANK_WAY_NUM-1:0] dram_cs ;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/8-1:0] dram_wen_biten; // byte enable
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2/8-1:0] dram_boundary_wen_biten;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0] dram_wen;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0] dram_wen_tmp;
logic [L1I_BANK_SET_INDEX_WIDTH-1:0] dram_addr;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH/2-1:0] dram_addr_ram;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH-1:0] dram_addr_ram_tmp;
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH-1:0]dram_addr_by_way;
logic [L1I_BANK_SET_INDEX_WIDTH + L1I_BANK_WAY_INDEX_WIDTH-1:0] dram_addr_by_offset;

logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_wdat;
logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_wdat_tmp;
logic [L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0] dram_wdat_way;

logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0] dram_rdat;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0] dram_r_boundary;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0] dram_r_rvc_mask;
logic [2-1:0][L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all;
logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_boundary_all;
logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0] dram_r_rvc_mask_all;
logic [L1I_BANK_LINE_DATA_SIZE-1:0] dram_rdat_all_way_selected;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM-1:0] dram_wdat_ram;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0] dram_w_boundary_ram;
logic [2-1:0][L1I_BANK_WAY_NUM-1:0][L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2-1:0] dram_w_rvc_mask_ram;

logic [L1I_BANK_LINE_DATA_SIZE-1:0] pfb_rdat_all;

logic [L1I_BANK_SET_INDEX_WIDTH-1:0] fetch_l1i_st_req_idx;
logic [L1I_BANK_OFFSET_WIDTH-1:0   ] fetch_l1i_st_req_offset;

//MLFB
logic plru_rd_en_mlfb_peek;
logic[L1I_BANK_SET_INDEX_WIDTH-1:0] plru_rd_idx_mlfb_peek;
logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] plru_rd_dat_mlfb_peek;
logic[L1I_BANK_SET_INDEX_WIDTH-1:0] lst_rd_idx_mlfb_peek;
logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] lst_rd_avail_way_mlfb_peek;
rrv64_l1i_lst_t lst_rd_dat_mlfb_peek;

logic[L1I_BANK_SET_INDEX_WIDTH-1:0] lst_rd_idx_s0_req;
rrv64_l1i_lst_t lst_rd_dat_s0_req;

// logic mlfb_evict_valid;
// logic mlfb_evict_ready;
// logic mlfb_evict_hsk;
// rrv64_l1i_evict_req_t mlfb_evict_req;

// logic evict_valid_cache;
// logic evict_ready_cache;
// logic evict_hsk_cache;
// rrv64_l1i_evict_req_t evict_req;

logic refill_valid;
logic refill_ready;               // include bank pipe hazard and output arbiter hazard
logic refill_bank_internal_ready; // include bank pipe hazard
logic refill_hsk;                 // include bank pipe hazard and output arbiter hazard
logic refill_bank_internal_hsk;   // include bank pipe hazard
rrv64_l1i_refill_req_t refill_req;

// s1 tag compare
logic[L1I_BANK_PADDR_TAG_WIDTH-1:0] tag_used_to_compare;
logic[L1I_BANK_WAY_NUM-1:0] s1_tag_compare_hit_per_way;
logic s1_tag_compare_hit;

// tag compare
logic ld_tlb_hit;

// lsu ld gets tag for tlb at s1
logic[L1I_BANK_PADDR_TAG_WIDTH-1:0] paddr_tag_from_tlb;

// lst
logic lst_mesi_wr_en_s0_req_vld;
logic[L1I_BANK_SET_INDEX_WIDTH-1:0] lst_mesi_wr_set_idx_s0_req;
logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] lst_mesi_wr_way_idx_s0_req;
rrv64_mesi_type_e lst_mesi_wr_dat_s0_req;

logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] s1_tag_compare_hit_idx;

// s3 ld resp to lsu
logic l1i_lsu_resp_valid;
logic l1i_lsu_resp_is_refill;
logic [L1I_BANK_OFFSET_WIDTH-1:0]   l1i_lsu_resp_offset;
logic [FTQ_TAG_LEN-1:0]  l1i_lsu_resp_if_tag;
logic [PREG_TAG_WIDTH-1:0] l1i_lsu_resp_prd;

// hand shake
logic fetch_l1i_if_req_hsk;
logic dtlb_l1i_resp_hsk;
logic s1_st_req_tag_hit;
logic s1_if_req_tag_hit;
logic s1_st_req_tag_miss;
logic s1_if_req_tag_miss;
logic s1_req_tag_miss;
logic s2_req_tag_miss;

// fencei flush fsm
// make all dirty line into clean, write back dirty line
logic fencei_flush_hsk;
logic fencei_flush_rdy_internal;
l1i_bank_fencei_flush_state_t l1i_bank_fencei_flush_state_d, l1i_bank_fencei_flush_state_q;
logic l1i_bank_fencei_flush_state_d_ena;
logic in_fencei_flush;
logic in_fencei_flush_pending;
logic in_fencei_flush_write_lst;
logic in_fencei_finish_flush;

/******************************************************************************
  prefetch buffer
******************************************************************************/
logic                                       pfb_req_valid;
logic                                       pfb_prefetch_hit;
logic                                       pfb_fetch_hit_when_req, pfb_fetch_hit_when_fill;
logic                                       pfb_fetch_stall;
logic                                       pfb_fetch_ready;
logic                                       pfb_prefetch_ready;
logic [$clog2(PFB_ENTRY_COUNT_PER_BANK)-1:0]prefetch_pfb_tag;
logic [L1I_BANK_LINE_DATA_SIZE-1:0]         pfb_fetch_cacheline;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]    pfb_fetch_boundary;
logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]    pfb_fetch_rvc_mask;
logic                                       pfb_empty;
logic [FTQ_TAG_LEN-1:0]                     pfb_ftq_tag_when_fill;
logic [L1I_BANK_OFFSET_WIDTH-1:0]           pfb_offset_when_fill;
logic                                       pfb_fencei_flush_grant;

assign pfb_req_valid = s1_valid & ld_tlb_hit;
// to pfb
rvh_l1i_pfb #(
    .ENTRY_COUNT(PFB_ENTRY_COUNT_PER_BANK),
    .BANK_ID(BANK_ID)
) u_pfb (
// fetch
    .valid_i(pfb_req_valid),
    .is_prefetch_i(cur.s1.is_prefetch),
    .ptag_i(paddr_tag_from_tlb),
    .bank_index_i(cur.s1.fetch_l1i_req_idx),
    .offset_i(cur.s1.fetch_l1i_req_offset),
    .ftq_tag_i(cur.s1.fetch_l1i_req_if_tag),
    .ic_hit_i(s1_if_req_tag_hit),
    .fetch_hit_when_req_o(pfb_fetch_hit_when_req),
    .fetch_hit_when_fill_o(pfb_fetch_hit_when_fill),
    .fetch_cacheline_o(pfb_fetch_cacheline),
    .fetch_boundary_o(pfb_fetch_boundary),
    .fetch_rvc_mask_o(pfb_fetch_rvc_mask),
    .fetch_stall_o(pfb_fetch_stall),
    .fetch_ftq_tag_o(pfb_ftq_tag_when_fill),
    .fetch_offset_o(pfb_offset_when_fill),
    .prefetch_hit_o(pfb_prefetch_hit),
    .prefetch_pfb_tag_o(prefetch_pfb_tag),
    .fetch_ready_o(pfb_fetch_ready),
    .prefetch_ready_o(pfb_prefetch_ready),

// req to l2
    .l2_req_if_arvalid_o (l2_req_if_arvalid      ),
    .l2_req_if_arready_i (l2_req_if_arready      ),
    .l2_req_if_ar_o      (l2_req_if_ar           ),
    .l2_req_if_is_fetch_o(l2_req_if_ar_is_fetch  ),

// resp from l2
    .l1i_l2_rd_resp_valid_i (l2_resp_if_rvalid),
    .l1i_l2_rd_resp_ready_o (l2_resp_if_rready),
    .l1i_l2_rd_resp_i (l2_resp_if_r),

// when data from L2 are all arrived,
// search lru for victim way in case all way are full
    .lru_peek_valid_o (plru_rd_en_mlfb_peek),
    .lru_peek_set_idx_o (plru_rd_idx_mlfb_peek),
    .lru_peek_victim_way_i (plru_rd_dat_mlfb_peek),
//  search lst for INVALID line and we can finalize the way we will evict 
    .lst_peek_set_idx_o (lst_rd_idx_mlfb_peek),
    .lst_peek_dat_i (lst_rd_dat_mlfb_peek),
    .lst_peek_avail_way_idx_i (lst_rd_avail_way_mlfb_peek),

// do refill        
    .cache_refill_req_valid_o(refill_valid),
    .cache_refill_req_ready_i(refill_ready),
    .cache_refill_req_o(refill_req),
// state
    .empty_o(pfb_empty),

    .s1_valid_i (s1_valid),
    .s1_paddr_i (s1_paddr),
    .s2_valid_i (s2_valid),
    .s2_paddr_i (s2_paddr),

    .fencei_flush_vld_or_pending_i(fencei_flush_vld_i | in_fencei_flush_pending),
    .fencei_flush_grant_o(pfb_fencei_flush_grant),

    .flush_i(flush_i),
    
    .rst(rst),
    .clk(clk)
);

assign l2_req_if_awvalid = '0;
assign l2_req_if_wvalid = '0;

 // to tag ram
genvar waynum;
generate
  for(waynum = 0; waynum < L1I_BANK_WAY_NUM; waynum++) begin
    assign tram_cs[waynum] = cache_rd_tram_way_en[waynum] | cache_wr_tram_way_en[waynum];
    assign tram_wen[waynum] = cache_wr_tram_way_en[waynum];
    assign tram_addr[waynum] = cache_idx_tram;
    assign tram_wdat[waynum] = cache_wr_dat_tram; // TODO:
//    assign tram_wen_biten[waynum] = '1; // TODO:
  end
endgenerate

// --- tag --- //
// tag ram generate
generate
  for(waynum = 0; waynum < L1I_BANK_WAY_NUM; waynum++) begin: TAG_RAM_GEN
    generic_spram
    #(
      .w(L1I_BANK_TAG_RAM_WORD_WIDTH),
      .p(L1I_BANK_TAG_RAM_WORD_WIDTH),
      .d(L1I_BANK_SET_NUM),
      .log2d($clog2(L1I_BANK_SET_NUM)),
      .id(1),

      .RAM_LATENCY(1),
      .RESET      (1),
      .RESET_HIGH (0)
    )
    U_TAG_RAM
    (
      .clk (clk),//clock
      .ce (tram_cs[waynum]),//chip enable,low active
      .we (tram_wen[waynum]),//write enable,low active
      .addr (tram_addr[waynum]),//address
      .din (tram_wdat[waynum]),//data in
      .dout (tram_rdat[waynum]),//data out
      .biten ('1)
//      .biten (tram_wen_biten[waynum])
    );

    // ICACHE_TAG_RAM U_TAG_RAM
    // (
    //   .CLK(clk),
    //   .CEB(~tram_cs[waynum]),
    //   .WEB(~tram_wen[waynum]),
    //   .A(tram_addr[waynum]),
    //   .D(tram_wdat[waynum]),
    //   .WE(1'b1),
    //   .Q(tram_rdat[waynum])
    // );




  end
endgenerate

// --- data --- //
  // to data ram
// separate even and odd line_seg to achieve one block ahead access
// even bank
//{set_idx,line_seg=0} : way0_seg0 way1_seg0 way2_seg0 way3_seg0
//{set_idx,line_seg=2} : way2_seg2 way3_seg2 way0_seg2 way1_seg2

// odd bank
//{set_idx,line_seg=l} : way3_seg1 way0_seg1 way1_seg1 way2_seg1
//{set_idx,line seg=3} : way1_seg3 way2_seg3 way3_seg3 way0_seg3

//{set_idx,line_seg=0} : way0_seg0 way1_seg0 way2_seg0 way3_seg0
//{set_idx,line_seg=l} : way3_seg1 way0_seg1 way1_seg1 way2_seg1
//{set_idx,line_seg=2} : way2_seg2 way3_seg2 way0_seg2 way1_seg2
//{set_idx,line seg=3} : way1_seg3 way2_seg3 way3_seg3 way0_seg3

// data ram generate
always_comb begin
  dram_wen = '0;
  unique case(cache_way_idx_dram)
    2'b00,
    2'b10: begin
      dram_wen[0] = dram_wen_tmp[0] & 4'b0101;
      dram_wen[1] = dram_wen_tmp[1] & 4'b1010;
    end
    2'b01,
    2'b11: begin
      dram_wen[0] = dram_wen_tmp[0] & 4'b1010;
      dram_wen[1] = dram_wen_tmp[1] & 4'b0101;
    end
    default:begin
    end
  endcase
end

generate
  for(genvar i = 0; i < 2; i++) begin: gen_even_odd_data_ram_bank
    rvh_l1i_bank_data_ram_access
    DATA_RAM_ACCESS_EVEN_BANK_U (
      .cache_wr_valid_dram_i  (cache_wr_valid_dram),
      .cache_rd_valid_dram_i  (cache_rd_valid_dram),
      .cache_idx_dram_i       (cache_idx_dram),
      .cache_offset_dram_i    (cache_offset_dram[i]),
      .cache_way_idx_dram_i   (cache_way_idx_dram),
      .cache_offset_dram_s1_i (cache_offset_dram_s1[i] ),

      .refill_hsk_i           (refill_hsk),
      .refill_req_i           (refill_req),

      .dram_rdat_i            (dram_rdat [i]),
      .dram_r_boundary_i      (dram_r_boundary[i]),
      .dram_r_rvc_mask_i      (dram_r_rvc_mask[i]),

      .dram_cs_o              (dram_cs  [i]),
      .dram_wen_o             (dram_wen_tmp [i] ),
      .dram_addr_ram_o        (dram_addr_ram_tmp[i] ),
      .dram_wdat_ram_o        (dram_wdat_ram[i] ),
      .dram_w_boundary_ram_o  (dram_w_boundary_ram[i] ),
      .dram_w_rvc_mask_ram_o  (dram_w_rvc_mask_ram[i] ),
      .dram_wen_biten_o       (dram_wen_biten[i]),
      .dram_boundary_wen_biten_o(dram_boundary_wen_biten[i]),


      .dram_rdat_all_o        (dram_rdat_all [i]),
      .dram_r_boundary_all_o  (dram_r_boundary_all [i]),
      .dram_r_rvc_mask_all_o  (dram_r_rvc_mask_all [i])
    );

    for(waynum = 0; waynum < L1I_BANK_WAY_NUM; waynum++) begin: DATA_RAM_GEN
      assign dram_addr_ram[i][waynum] = dram_addr_ram_tmp[i][waynum] >> 1;
      generic_spram
      #(
        .w(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM),//128
        .p(8),
        .d(L1I_BANK_SET_NUM*L1I_BANK_WAY_NUM/2), // 128
        .log2d($clog2(L1I_BANK_SET_NUM*L1I_BANK_WAY_NUM/2)), // 4
        .id(0),

        .RAM_LATENCY(1),
        .RESET      (1),
        .RESET_HIGH (0)
      )
      U_DATA_RAM
      (
        .clk (clk),//clock
        .ce (dram_cs[i][waynum]),//chip enable,low active
        .we (dram_wen[i][waynum]),//write enable,low active
        .addr (dram_addr_ram[i][waynum]),//address
        .din (dram_wdat_ram[i][waynum]),//data in
        .dout (dram_rdat[i][waynum]),//data out
        .biten (dram_wen_biten[i][waynum]) // byte enable
      );

      // CACHE_DATA_RAM U_DATA_RAM
      // (
      //   .CLK(clk),
      //   .CEB(~dram_cs[i][waynum]),
      //   .WEB(~dram_wen[i][waynum]),
      //   .A(dram_addr_ram[i][waynum]),
      //   .D(dram_wdat_ram[i][waynum]),
      //   .BYTE_WE(dram_wen_biten[i][waynum]),
      //   .Q(dram_rdat[i][waynum])
      // );


      generic_spram
      #(
        .w(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2),//8*2
        .p(8),
        .d(L1I_BANK_SET_NUM*L1I_BANK_WAY_NUM/2), // 32
        .log2d($clog2(L1I_BANK_SET_NUM*L1I_BANK_WAY_NUM/2)), // 5
        .id(0),

        .RAM_LATENCY(1),
        .RESET      (1),
        .RESET_HIGH (0)
      )
      U_INST_BOUNDARY_RAM
      (
        .clk (clk),//clock
        .ce (dram_cs[i][waynum]),//chip enable,low active
        .we (dram_wen[i][waynum]),//write enable,low active
        .addr (dram_addr_ram[i][waynum]),//address
        .din (dram_w_boundary_ram[i][waynum]),//data in
        .dout (dram_r_boundary[i][waynum]),//data out
        .biten (dram_boundary_wen_biten[i][waynum]) // byte enable
      );
      generic_spram
      #(
        .w(L1I_BANK_LINE_DATA_SIZE/L1I_BANK_WAY_NUM/16*2),//8*2
        .p(8),
        .d(L1I_BANK_SET_NUM*L1I_BANK_WAY_NUM/2), // 32
        .log2d($clog2(L1I_BANK_SET_NUM*L1I_BANK_WAY_NUM/2)), // 5
        .id(0),

        .RAM_LATENCY(1),
        .RESET      (1),
        .RESET_HIGH (0)
      )
      U_INST_RVC_MASK_RAM
      (
        .clk (clk),//clock
        .ce (dram_cs[i][waynum]),//chip enable,low active
        .we (dram_wen[i][waynum]),//write enable,low active
        .addr (dram_addr_ram[i][waynum]),//address
        .din (dram_w_rvc_mask_ram[i][waynum]),//data in
        .dout (dram_r_rvc_mask[i][waynum]),//data out
        .biten (dram_boundary_wen_biten[i][waynum]) // byte enable
      );
    end
  end
endgenerate


// ------- pipe logic --------- // 

//--- stage 0 ---//
// 1. load : data ram read:
//                given idx and offset, read all way with certain offset
//           tag ram read:
//                given idx, read all way
//           lst reg read:
//                given idx, read all way state
// 2. store: tag ram read:
//                given idx, read all way
//           lst reg read:
//                given idx, read all way state
// 3. evict: tag ram read:
//                given idx and way id, read single way
//           data ram read:
//                given idx and way id, read single way


// req decode
assign req_type_dec.is_ld     = 1'b1;
assign req_type_dec.is_st     = 1'b0;
assign req_type_dec.is_amo    = 1'b0;
assign req_type_dec.amo_type  = '0;
assign req_type_dec.is_lr     = 1'b0;
assign req_type_dec.is_sc     = 1'b0;
assign req_type_dec.op_b      = 1'b0;
assign req_type_dec.op_hw     = 1'b0;
assign req_type_dec.op_w      = 1'b0;
assign req_type_dec.op_dw     = 1'b1; // TODO: 128 bit
assign req_type_dec.ld_u      = 1'b0;

logic [L1I_BANK_SET_INDEX_WIDTH-1:0] fetch_l1i_req_idx;
assign fetch_l1i_req_idx = fetch_l1i_if_req_idx_i;

// lsu ld
 // s0 ld need to rd tag, data ram
assign fetch_l1i_if_fetch_req_rdy_o = ~refill_valid & pfb_fetch_ready & ~pfb_fetch_stall & ~in_fencei_flush;
assign fetch_l1i_if_prefetch_req_rdy_o = ~refill_valid & pfb_prefetch_ready & ~in_fencei_flush;
assign fetch_l1i_if_prefetch_pfb_tag_o = {BANK_ID, prefetch_pfb_tag};
// fetch hit: 1.ic hit 2. pfb hit in req 3. pfb hit in fill
assign fetch_l1i_if_fetch_hit_when_req_o = pfb_fetch_hit_when_req | s1_if_req_tag_hit;
assign fetch_l1i_if_fetch_hit_when_fill_o = pfb_fetch_hit_when_fill;
assign fetch_l1i_if_fetch_inflight_o = 1'b0;
assign fetch_l1i_if_prefetch_hit_o = s1_if_req_tag_hit | pfb_prefetch_hit;
// assign fetch_l1i_if_req_rdy_o = ~refill_valid &                     // refill has highest priority
//                                   ~fetch_l1i_req_idx_haz_in_pipe & // same idx miss at s1 and s2
//                                   ~fetch_l1i_req_idx_haz_in_mshr & // same idx in mshr
//                                   ~(  ~has_free_mshr | 
//                                       ((free_mshr_num == 1) & (s1_req_tag_miss | s2_req_tag_miss)) | 
//                                       ((free_mshr_num == 2) & (s1_req_tag_miss & s2_req_tag_miss)) // if no mshr or there is going to be no mshr, stall
//                                   ) &
//                                   ~flush_i &                        // rob flush, kill load req in pipeline
//                                   ~in_fencei_flush;                 // fencei flush, wait until done
assign fetch_l1i_if_req_hsk   = fetch_l1i_if_req_vld_i & ((fetch_l1i_if_req_prefetch_i & fetch_l1i_if_prefetch_req_rdy_o) | (~fetch_l1i_if_req_prefetch_i & fetch_l1i_if_fetch_req_rdy_o));

// mlfb refill
assign refill_ready               = ~s1_valid;
assign refill_hsk                 = refill_valid & refill_ready;

// fencei flush
  // can start fencei flush when the cache bank has no on fly req
assign fencei_flush_rdy_internal  = ~s1_valid & ~s2_valid & pfb_fencei_flush_grant;
assign fencei_flush_hsk           = (fencei_flush_vld_i | in_fencei_flush_pending) & fencei_flush_rdy_internal;

// lst reg access
  // lsu ld,st rd
assign lst_rd_idx_s0_req = cache_idx_tram; // for s0 load/store lst access, idx is he same as tag ram access

// tag ram access
  // rd
assign cache_idx_tram      = refill_hsk ? refill_req.set_idx : fetch_l1i_if_req_idx_i;
assign cache_offset_tram   = fetch_l1i_if_req_offset_i;
assign cache_rd_tram_way_en = {L1I_BANK_WAY_NUM{fetch_l1i_if_req_hsk}};

  //wr
assign cache_wr_valid_tram = refill_hsk;
generate
    for(waynum = 0; waynum < L1I_BANK_WAY_NUM; waynum++) begin
        assign cache_wr_tram_way_en[waynum] = refill_hsk ? (waynum == refill_req.way_idx) : '0;
    end
endgenerate
assign cache_wr_dat_tram = refill_req.tag;

// data ram access
  // rd
assign cache_rd_valid_dram = fetch_l1i_if_req_hsk;  // lsu ld s0 rd data
assign cache_idx_dram      = refill_hsk ? refill_req.set_idx : fetch_l1i_if_req_idx_i;
assign cache_wr_valid_dram = refill_hsk;  // stb st s1 wr data;

// for oba access, read 2 16-byte block at the same time
always_comb begin
  fetch_l1i_if_req_offset = '0;
  unique case(fetch_l1i_if_req_offset_i[5:4])
    2'b00: begin
      fetch_l1i_if_req_offset[0] = 6'b000000;
      fetch_l1i_if_req_offset[1] = 6'b010000;
    end
    2'b01: begin
      fetch_l1i_if_req_offset[0] = 6'b100000;
      fetch_l1i_if_req_offset[1] = 6'b010000;
    end
    2'b10: begin
      fetch_l1i_if_req_offset[0] = 6'b100000;
      fetch_l1i_if_req_offset[1] = 6'b110000;
    end
    2'b11: begin
      fetch_l1i_if_req_offset[1] = 6'b110000;
    end
    default: begin
    end
  endcase
end

always_comb begin
  for(int i = 0; i < 2 ; i++) begin
    cache_offset_dram[i] = refill_hsk ? '0 : fetch_l1i_if_req_offset[i];
  end
end

assign cache_way_idx_dram  = refill_hsk ? refill_req.way_idx : s1_tag_compare_hit_idx;

//--- stage 1 ---//
// 1. lsu load:
//   1.1 tlb hit: 
//     1.1.1 compare tag ram output with tlb physical tag
//       1.1.1.1 if hit: resp to lsu at s1
//       1.1.1.2 if miss: miss req goes into mshr
//   1.2 tlb miss:
//     1.2.1 kill the lsu ld req


assign dtlb_l1i_resp_rdy_o  = '1; // TODO: not used
assign dtlb_l1i_resp_hsk    = dtlb_l1i_resp_vld_i & dtlb_l1i_resp_rdy_o;

generate
  if(L1I_BANK_SET_INDEX_WIDTH+L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH >= 12) begin
    assign paddr_tag_from_tlb = dtlb_l1i_resp_ppn_i[PPN_WIDTH-1:((L1I_BANK_SET_INDEX_WIDTH+L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH)-12)];
  end else begin
    assign paddr_tag_from_tlb = {dtlb_l1i_resp_ppn_i, cur.s1.fetch_l1i_req_vtag[(12-(L1I_BANK_SET_INDEX_WIDTH+L1I_BANK_ID_INDEX_WIDTH+L1I_BANK_OFFSET_WIDTH)):0]};
  end
endgenerate

assign ld_tlb_hit = dtlb_l1i_resp_hsk & dtlb_l1i_resp_hit_i & ~dtlb_l1i_resp_excp_vld_i;
    
    // compare tag from load:from tlb
    // mux between ld tlb tag and st tag
assign tag_used_to_compare = paddr_tag_from_tlb;

generate
  for(waynum = 0; waynum < L1I_BANK_WAY_NUM; waynum++) begin
    assign s1_tag_compare_hit_per_way[waynum] = (tram_rdat[waynum] == tag_used_to_compare) // tag match
                                              && ((cur.s1.lst_dat.mesi_sta[waynum] == EXCLUSIVE)||(cur.s1.lst_dat.mesi_sta[waynum] == MODIFIED)); // valid bit set
  end
endgenerate

assign s1_tag_compare_hit = ld_tlb_hit & (|s1_tag_compare_hit_per_way);

assign s1_if_req_tag_hit = s1_valid & s1_tag_compare_hit; 
assign s1_if_req_tag_miss = s1_valid & ~s1_tag_compare_hit; 
assign s1_req_tag_miss = s1_valid & ~s1_tag_compare_hit;
assign s2_req_tag_miss = s2_valid & ~cur.s2.tag_compare_hit;

// for oba access, read 2 16-byte block at the same time
always_comb begin
  cache_offset_dram_s1 = '0;
  unique case(cur.s1.fetch_l1i_req_offset[5:4])
    2'b00: begin
      cache_offset_dram_s1[0] = 6'b000000;
      cache_offset_dram_s1[1] = 6'b010000;
    end
    2'b01: begin
      cache_offset_dram_s1[0] = 6'b100000;
      cache_offset_dram_s1[1] = 6'b010000;
    end
    2'b10: begin
      cache_offset_dram_s1[0] = 6'b100000;
      cache_offset_dram_s1[1] = 6'b110000;
    end
    2'b11: begin
      cache_offset_dram_s1[1] = 6'b110000;
    end
    default: begin
    end
  endcase
end

// --- lsu resp --- //
// s2 ld resp to lsu
assign l1i_lsu_resp_valid = s2_valid & (cur.s2.tag_compare_hit | cur.s2.pfb_hit);
assign l1i_lsu_resp_offset = cur.s2.fetch_l1i_req_offset;
assign l1i_lsu_resp_if_tag = cur.s2.fetch_l1i_req_if_tag;
assign l1i_lsu_resp_prd     = cur.s2.fetch_l1i_req_prd;
assign l1i_fetch_if_resp_two_blks_o = (l1i_lsu_resp_offset[L1I_BANK_OFFSET_WIDTH-1:4] * FETCH_WIDTH) < (L1I_BANK_LINE_DATA_SIZE - FETCH_WIDTH);

// assign l1i_lsu_req_type_dec_o = (refill_bank_internal_hsk & refill_req.is_ld) ? refill_req.req_type_dec : cur.s2.req_type_dec;
rvh_l1i_lsu_hit_resp rvh1_l1i_lsu_hit_resp_u
(
  .resp_valid_i                 (l1i_lsu_resp_valid             ),
  .pfb_hit_i                    (cur.s2.pfb_hit                 ),
  .tag_compare_hit_per_way_i    (cur.s2.tag_compare_hit_per_way ),
  .ld_tlb_hit_i                 (cur.s2.ld_tlb_hit              ),
  .pfb_data_i                   (cur.s2.pfb_data                ),
  .pfb_boundary_i               (cur.s2.pfb_boundary            ),
  .pfb_rvc_mask_i               (cur.s2.pfb_rvc_mask            ),
  .lsu_ld_dat_i                 (cur.s2.line_data               ),
  .lsu_ld_boundary_i            (cur.s2.line_boundary           ),
  .lsu_ld_rvc_mask_i            (cur.s2.line_rvc_mask           ),

  .fetch_l1i_if_req_if_tag_i    (l1i_lsu_resp_if_tag            ),
  .fetch_l1i_if_req_offset_i    (l1i_lsu_resp_offset            ),

  .l1i_fetch_if_resp_vld_o      (l1i_fetch_if_resp_vld_o        ),
  .l1i_fetch_if_resp_tag_o      (l1i_fetch_if_resp_if_tag_o     ),
  .l1i_fetch_if_resp_data_o     (l1i_fetch_if_resp_data_o       ),
  .l1i_fetch_if_resp_boundary_o (l1i_fetch_if_resp_boundary_o   ),
  .l1i_fetch_if_resp_rvc_mask_o (l1i_fetch_if_resp_rvc_mask_o   )

);



//----- mlfb ----------//
// after receiving a whole cache line, do one step each cycle:
// 1. head_buf_valid_nxt = true
// 2. read lru reg & read valid & dirty reg => get victim or available
// 3. set reserve bit in lst
// 4. if need evict, send evict(flush) req to s0
// 5. after evict(flush) req go though pipeline(read tag and data at s0, get into ewrq at s1), line refill(write tag and data ram, update valid, dirty and lru)
// 6. if step 5 done, deallocate mlfb and mshr entry
  generate
    if(L1I_BANK_ID_NUM > 1) begin  
      assign s1_paddr = {paddr_tag_from_tlb, cur.s1.fetch_l1i_req_idx, BANK_ID[L1I_BANK_ID_INDEX_WIDTH-1:0], cur.s1.fetch_l1i_req_offset};
      assign s2_paddr = {cur.s2.fetch_l1i_req_tag, cur.s2.fetch_l1i_req_idx, BANK_ID[L1I_BANK_ID_INDEX_WIDTH-1:0], cur.s2.fetch_l1i_req_offset};
    end else begin
      assign s1_paddr = {paddr_tag_from_tlb, cur.s1.fetch_l1i_req_idx, cur.s1.fetch_l1i_req_offset};
      assign s2_paddr = {cur.s2.fetch_l1i_req_tag, cur.s2.fetch_l1i_req_idx, cur.s2.fetch_l1i_req_offset};
    end
  endgenerate

// --- lst --- //
//LST req, ordered by priority
// write req
//  port 1. mlfb refill: given idx & way_id, write state & check_bit
//       1. lsu store s1: given idx & way_id, write state(dirty bit)
//       2. mlfb evict check: given idx & way_id, write check_bit
// read req: (as reg, all read can performed parallelly)
//       3. mlfb evict peek: given idx, read all way state
//       4. lsu load s0: given idx, read all way state
//       4. lsu store s0: given idx, read all way state

always_comb begin
  s1_tag_compare_hit_idx = '0;
  for(int i = 0; i < L1I_BANK_WAY_NUM; i++) begin
      if(s1_tag_compare_hit_per_way[i] == 1'b1) begin
          s1_tag_compare_hit_idx = i;
      end
  end
end

assign lst_mesi_wr_en_s0_req_vld  =  refill_hsk;
assign lst_mesi_wr_set_idx_s0_req =  refill_hsk ? refill_req.set_idx : cur.s1.fetch_l1i_req_idx;
assign lst_mesi_wr_way_idx_s0_req =  refill_hsk ? refill_req.way_idx : s1_tag_compare_hit_idx;
assign lst_mesi_wr_dat_s0_req     =  refill_hsk ? refill_req.mesi_sta : MODIFIED;

rvh_l1i_lst
#(
 .entry_num(L1I_BANK_SET_NUM)
 ,.way_num(L1I_BANK_WAY_NUM)
) U_L1I_LST(
     .clk (clk)
    ,.rstn (~rst )
    ,.lst_mesi_wr_en_s0_req (lst_mesi_wr_en_s0_req_vld ) // 1
    ,.lst_mesi_wr_set_idx_s0_req (lst_mesi_wr_set_idx_s0_req)
    ,.lst_mesi_wr_way_idx_s0_req (lst_mesi_wr_way_idx_s0_req)
    ,.lst_mesi_wr_dat_s0_req     (lst_mesi_wr_dat_s0_req)
    
    ,.lst_rd_idx_s0_req (lst_rd_idx_s0_req)  // 4
    ,.lst_rd_dat_s0_req (lst_rd_dat_s0_req)  
    
    ,.lst_rd_idx_mlfb_peek (lst_rd_idx_mlfb_peek) // 3
    ,.lst_rd_dat_mlfb_peek (lst_rd_dat_mlfb_peek)
    ,.lst_avail_way_rd_idx_mlfb_peek (lst_rd_idx_mlfb_peek)
    ,.lst_avail_way_rd_dat_mlfb_peek (lst_rd_avail_way_mlfb_peek)

    // fencei flush, invalidate alla cache line
    ,.fencei_flush_i  (in_fencei_flush_write_lst )
);

// --- plru ---//
//PLRU req, ordered by priority
// write req
//  port 1. load/store hit: given idx & way_id, update plru
// read req: (as reg, all read can performed parallelly)
//       2. mlfb evict peek: given idx, get victim way_id

logic plru_upd_en_s1_cache_hit;
logic[L1I_BANK_SET_INDEX_WIDTH-1:0] plru_set_idx_s1_cache_hit; 
logic[L1I_BANK_WAY_INDEX_WIDTH-1:0] plru_way_idx_s1_cache_hit;

assign plru_upd_en_s1_cache_hit  = s1_valid & s1_if_req_tag_hit & ~cur.s1.is_prefetch;
assign plru_set_idx_s1_cache_hit = cur.s1.fetch_l1i_req_idx;
assign plru_way_idx_s1_cache_hit = s1_tag_compare_hit_idx;

rvh_l1i_plru
#(
.entry_num(L1I_BANK_SET_NUM)
,.way_num(L1I_BANK_WAY_NUM)
)
U_RRV64_L1I_PLRU(
.clk (clk)
,.rstn (~rst)
,.upd_en_hit (plru_upd_en_s1_cache_hit)
,.upd_set_idx_hit (plru_set_idx_s1_cache_hit)
,.upd_way_idx_hit (plru_way_idx_s1_cache_hit)

,.rd_en_refill (plru_rd_en_mlfb_peek)
,.rd_idx_refill (plru_rd_idx_mlfb_peek)
,.rd_dat_refill (plru_rd_dat_mlfb_peek) 
);


// fencei flush fsm
// make all line invalid
  // next fencei flush state

always_comb begin: case_l1i_bank_fencei_flush_state_d
  l1i_bank_fencei_flush_state_d = FLUSH_IDLE;
  l1i_bank_fencei_flush_state_d_ena = 1'b0;

  unique case(l1i_bank_fencei_flush_state_q)
    FLUSH_IDLE: begin
      if(fencei_flush_hsk) begin // fencei req comes and hsk success
        l1i_bank_fencei_flush_state_d     = FLUSH_WRITE_LST;
        l1i_bank_fencei_flush_state_d_ena = 1'b1;
      end 
      else if(fencei_flush_vld_i) begin // fencei req comes and hsk fail
        l1i_bank_fencei_flush_state_d     = FLUSH_PENDING;
        l1i_bank_fencei_flush_state_d_ena = 1'b1;
      end
    end
    FLUSH_PENDING: begin
      if(fencei_flush_hsk) begin // fencei req comes and hsk success
        l1i_bank_fencei_flush_state_d     = FLUSH_WRITE_LST;
        l1i_bank_fencei_flush_state_d_ena = 1'b1;
      end 
      else begin // fencei req comes and hsk fail
        l1i_bank_fencei_flush_state_d     = FLUSH_PENDING;
        l1i_bank_fencei_flush_state_d_ena = 1'b0;
      end
    end
    // write lst all line invalid
    FLUSH_WRITE_LST: begin
        l1i_bank_fencei_flush_state_d     = FLUSH_FINISH;
        l1i_bank_fencei_flush_state_d_ena = 1'b1;
    end
    FLUSH_FINISH: begin
      l1i_bank_fencei_flush_state_d     = FLUSH_IDLE;
      l1i_bank_fencei_flush_state_d_ena = 1'b1;
    end
    default: begin
      l1i_bank_fencei_flush_state_d     = FLUSH_IDLE;
      l1i_bank_fencei_flush_state_d_ena = 1'b1;
    end
  endcase
end

  // output of fsm
    // states
assign in_fencei_flush_pending    = (l1i_bank_fencei_flush_state_q == FLUSH_PENDING);
assign in_fencei_flush_write_lst  = (l1i_bank_fencei_flush_state_q == FLUSH_WRITE_LST);
assign in_fencei_flush            = in_fencei_flush_pending | in_fencei_flush_write_lst;
assign in_fencei_finish_flush     = (l1i_bank_fencei_flush_state_q == FLUSH_FINISH);

  // fencei flush finish signal
assign fencei_flush_grant_o = in_fencei_finish_flush;

  // fsm registers
std_dffrve
#(.WIDTH($bits(l1i_bank_fencei_flush_state_t)))
U_L1I_BANK_FENCEI_STATE_REG
(
  .clk(clk),
  .rstn(~rst),
  .rst_val(FLUSH_IDLE),
  .en(l1i_bank_fencei_flush_state_d_ena),
  .d(l1i_bank_fencei_flush_state_d),
  .q(l1i_bank_fencei_flush_state_q)
);

// stage reg
assign s1_valid_nxt = fetch_l1i_if_req_hsk & ~flush_i;
assign s1_nxt_ena = pfb_prefetch_ready | pfb_fetch_ready;// s1_valid_nxt;
assign nxt.s1.fetch_l1i_req_if_tag  = fetch_l1i_if_req_if_tag_i;
assign nxt.s1.fetch_l1i_req_prd      = fetch_l1i_if_req_prd_i;
assign nxt.s1.fetch_l1i_req_idx      = fetch_l1i_if_req_idx_i;
assign nxt.s1.fetch_l1i_req_offset   = fetch_l1i_if_req_offset_i;
assign nxt.s1.fetch_l1i_req_vtag     = fetch_l1i_if_req_vtag_i;
assign nxt.s1.is_prefetch            = fetch_l1i_if_req_prefetch_i;
 //TODO: separate enable signal

assign nxt.s1.lst_dat                  = lst_rd_dat_s0_req;
assign nxt.s1.req_type_dec             = req_type_dec;

std_dffre#(.WIDTH(1)) U_STG_VALID_REG_S1 (.clk(clk),.rstn(~rst),.en(s1_nxt_ena),.d(s1_valid_nxt),.q(s1_valid));
std_dffe #(.WIDTH($bits(l1i_pipe_s1_t))) U_STG_DAT_REG_S1 (.clk(clk),.en(s1_nxt_ena),.d(nxt.s1),.q(cur.s1));

  // s1-s2
assign s2_valid_nxt = (((s1_if_req_tag_hit | pfb_fetch_hit_when_req) & ~cur.s1.is_prefetch) | pfb_fetch_hit_when_fill) &     // ld hit need go into s2, and resp to lsu at s2
                        ~flush_i;            // rob flush, kill load req in pipeline
                          // prefetch can never go to s2, only fetch can

assign s2_nxt_ena = s2_valid_nxt;

assign nxt.s2.tag_compare_hit               = s1_tag_compare_hit;
assign nxt.s2.tag_compare_hit_per_way       = s1_tag_compare_hit_per_way;
assign nxt.s2.ld_tlb_hit                    = ld_tlb_hit;
assign nxt.s2.line_data[0]                  = dram_rdat_all[0];
assign nxt.s2.line_data[1]                  = dram_rdat_all[1];
assign nxt.s2.line_boundary[0]              = dram_r_boundary_all[0];
assign nxt.s2.line_boundary[1]              = dram_r_boundary_all[1];
assign nxt.s2.line_rvc_mask[0]              = dram_r_rvc_mask_all[0];
assign nxt.s2.line_rvc_mask[1]              = dram_r_rvc_mask_all[1];
assign nxt.s2.is_prefetch                   = cur.s1.is_prefetch;
assign nxt.s2.fetch_l1i_req_prd           = cur.s1.fetch_l1i_req_prd;
assign nxt.s2.req_type_dec                  = cur.s1.req_type_dec;
assign nxt.s2.fetch_l1i_req_tag           = paddr_tag_from_tlb;
assign nxt.s2.fetch_l1i_req_idx           = cur.s1.fetch_l1i_req_idx;
assign nxt.s2.fetch_l1i_req_if_tag       = pfb_fetch_hit_when_fill ? pfb_ftq_tag_when_fill : cur.s1.fetch_l1i_req_if_tag;
assign nxt.s2.fetch_l1i_req_offset        = pfb_fetch_hit_when_fill ? pfb_offset_when_fill : cur.s1.fetch_l1i_req_offset;
assign nxt.s2.pfb_data                    = pfb_fetch_cacheline;
assign nxt.s2.pfb_boundary                = pfb_fetch_boundary;
assign nxt.s2.pfb_rvc_mask                = pfb_fetch_rvc_mask;
assign nxt.s2.pfb_hit                     = pfb_fetch_hit_when_req | pfb_fetch_hit_when_fill;

std_dffr #(.WIDTH(1)) U_STG_VALID_REG_S2 (.clk(clk),.rstn(~rst),.d(s2_valid_nxt),.q(s2_valid));
std_dffe #(.WIDTH($bits(l1i_pipe_s2_t))) U_STG_DAT_REG_S2 (.clk(clk),.en(s2_nxt_ena),.d(nxt.s2),.q(cur.s2));


endmodule : rvh_l1i_bank
