`ifndef __RVH_L1I_PKG_SV__
`define __RVH_L1I_PKG_SV__
package rvh_l1i_pkg;
import rvh_pkg::*;
import uop_encoding_pkg::*;

localparam L1I_BANK_LINE_DATA_SIZE = 512; // bits

localparam L1I_BANK_SET_NUM = 16; // sets
localparam L1I_BANK_WAY_NUM = 4;
localparam L1I_BANK_ID_NUM = 4;

localparam L1I_INDEX_WIDTH  = $clog2(L1I_BANK_SET_NUM*L1I_BANK_ID_NUM);
localparam L1I_OFFSET_WIDTH = $clog2(L1I_BANK_LINE_DATA_SIZE/8);
localparam L1I_BIT_OFFSET_WIDTH = $clog2(L1I_BANK_LINE_DATA_SIZE);
localparam L1I_TAG_WIDTH    = PADDR_WIDTH-L1I_INDEX_WIDTH-L1I_OFFSET_WIDTH;

localparam L1I_BANK_SET_INDEX_WIDTH = $clog2(L1I_BANK_SET_NUM);
localparam L1I_BANK_ID_INDEX_WIDTH  = $clog2(L1I_BANK_ID_NUM);
localparam L1I_BANK_OFFSET_WIDTH  = L1I_OFFSET_WIDTH;
localparam L1I_BANK_TAG_WIDTH     = L1I_TAG_WIDTH;
localparam L1I_BANK_WAY_INDEX_WIDTH = $clog2(L1I_BANK_WAY_NUM);

localparam PFB_ENTRY_COUNT_PER_BANK = 4;
localparam PFB_TAG_LEN = $clog2(PFB_ENTRY_COUNT_PER_BANK) + L1I_BANK_ID_INDEX_WIDTH;

localparam L1I_BANK_LINE_ADDR_SIZE = PADDR_WIDTH-L1I_OFFSET_WIDTH-L1I_BANK_ID_INDEX_WIDTH;
localparam L1I_BANK_PADDR_TAG_WIDTH = PADDR_WIDTH-L1I_BANK_SET_INDEX_WIDTH-L1I_BANK_ID_INDEX_WIDTH-L1I_BANK_OFFSET_WIDTH;
localparam L1I_BANK_TAG_RAM_WORD_WIDTH = L1I_BANK_PADDR_TAG_WIDTH; 


// s_axi_awsize    width(byte)
// 3'b000          1
// 3'b001          2
// 3'b010          4
// 3'b011          8
// 3'b100          16
// 3'b101          32
// 3'b110          64
// 3'b111          128
localparam MEM_DATA_WIDTH = 64;
localparam BURST_SIZE = L1I_BANK_LINE_DATA_SIZE/MEM_DATA_WIDTH;//8
localparam AXI_SIZE = $clog2(MEM_DATA_WIDTH/8);
// localparam PPN_WIDTH = 44;


localparam MEMNOC_TID_MASTERID_SIZE  = 4;
localparam MEMNOC_TID_TID_SIZE       = 4;


  typedef enum logic [1:0] {
    INVALID,
    SHARED,
    EXCLUSIVE,
    MODIFIED
  } rrv64_mesi_type_e;

  typedef struct packed {
    logic [MEMNOC_TID_MASTERID_SIZE-1:0] bid; // cache bank id
    logic [$clog2(PFB_ENTRY_COUNT_PER_BANK)-1:0] tid;      // pfb id in ar and r
    logic                                is_io;
    logic                                master_id;
  } mem_tid_t;
  typedef enum logic[1:0] {
    AXI_RESP_OKAY = 2'b00,
    AXI_RESP_EXOKAY = 2'b01,
    AXI_RESP_SLVERR = 2'b10,
    AXI_RESP_DECERR = 2'b11
  } axi4_resp_t;

  typedef struct packed {
    mem_tid_t awid;
    logic [PADDR_WIDTH-1:0]  awaddr;
    logic [7 : 0] awlen;
    
    logic [2 : 0] awsize; // TODO: 
    logic [1 : 0] awburst; // TODO:
    
} cache_mem_if_aw_t;

  typedef struct packed {
    logic [MEM_DATA_WIDTH-1:0]  wdata;
    logic wlast;
    mem_tid_t wid;
    logic [MEM_DATA_WIDTH/8-1:0]  wstrb;
} cache_mem_if_w_t;

  typedef struct packed {
    mem_tid_t arid;
    logic [7  : 0] arlen;
    
    logic [2 : 0] arsize; // TODO: 
    logic [1 : 0] arburst; // TODO:
    
    logic [PADDR_WIDTH-1:0]  araddr;
  } cache_mem_if_ar_t;

  // typedef struct packed {
  //   mem_tid_t rid;
  //   logic [MEM_DATA_WIDTH-1:0]  rdata;
  //   axi4_resp_t rresp;
  //   logic rlast;
  // } cache_mem_if_r_t;

  typedef struct packed {
    mem_tid_t                        rid;
    logic [MEM_DATA_WIDTH-1:0]       dat;
    logic                            err;
    rrv64_mesi_type_e                mesi_sta;
    // logic [RRV64_SCU_SST_IDX_W-1:0]  sst_idx;
    axi4_resp_t rresp; // TODO: 
    logic rlast;  // TODO: 
  //    logic                            l2_hit;
  } cache_mem_if_r_t;

  typedef struct packed {
    mem_tid_t bid;
    axi4_resp_t bresp;
  } cache_mem_if_b_t;


  typedef enum logic [2:0] {
    AMOSWAP,
    AMOADD,
    AMOAND,
    AMOOR,
    AMOXOR,
    AMOMAX,
    AMOMIN
  } l1i_amo_type_e;

  typedef struct packed {
    logic          is_ld;
    logic          is_st;
    logic          is_amo;
    l1i_amo_type_e amo_type;
    logic          is_lr;
    logic          is_sc;
    logic          op_b;
    logic          op_hw;
    logic          op_w;
    logic          op_dw;
    logic          ld_u;
  } rrv64_l1i_req_type_dec_t;


typedef struct packed {
    rrv64_mesi_type_e [L1I_BANK_WAY_NUM-1:0] mesi_sta;
} rrv64_l1i_lst_t;

typedef struct packed {
  // stage 1
  logic [FTQ_TAG_LEN-1:0]               fetch_l1i_req_if_tag;

  logic [L1I_BANK_SET_INDEX_WIDTH-1:0]  fetch_l1i_req_idx;
  logic [L1I_BANK_OFFSET_WIDTH-1:0   ]  fetch_l1i_req_offset;
  logic [L1I_BANK_TAG_WIDTH-1:0      ]  fetch_l1i_req_vtag; // for ld gen ptag when idx+offset<12
  
  rrv64_l1i_lst_t                       lst_dat; // mesi, cover valid & dirty bit

  logic [    PREG_TAG_WIDTH-1:0]        fetch_l1i_req_prd;
  rrv64_l1i_req_type_dec_t              req_type_dec;
  logic                                 is_prefetch;
  } l1i_pipe_s1_t;

typedef struct packed {
  // stage 2
  logic [FTQ_TAG_LEN-1:0]                         fetch_l1i_req_if_tag;

  logic                                           tag_compare_hit;
  logic [L1I_BANK_WAY_NUM-1:0]                    tag_compare_hit_per_way;
  logic                                           ld_tlb_hit;
  logic [2-1:0][L1I_BANK_LINE_DATA_SIZE-1:0]      line_data;  // 1. for ld req: lsu_ld_hit_dat; 2. for st req: fetch_l1i_st_req_dat
  logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0] line_boundary;  // 1. for ld req: lsu_ld_hit_dat; 2. for st req: fetch_l1i_st_req_dat
  logic [2-1:0][L1I_BANK_LINE_DATA_SIZE/16*2-1:0] line_rvc_mask;  // 1. for ld req: lsu_ld_hit_dat; 2. for st req: fetch_l1i_st_req_dat
  
  logic [    L1I_BANK_PADDR_TAG_WIDTH-1:0]        fetch_l1i_req_tag;
  logic [    L1I_BANK_SET_INDEX_WIDTH-1:0]        fetch_l1i_req_idx;
  logic [       L1I_BANK_OFFSET_WIDTH-1:0]        fetch_l1i_req_offset;

  logic [    PREG_TAG_WIDTH-1:0]                  fetch_l1i_req_prd;
  rrv64_l1i_req_type_dec_t                        req_type_dec;
  logic                                           is_prefetch;

  logic [L1I_BANK_LINE_DATA_SIZE-1:0]             pfb_data;
  logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]        pfb_boundary;
  logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0]        pfb_rvc_mask;
  logic                                           pfb_hit;

} l1i_pipe_s2_t;

typedef struct packed {
  // stage 1
  l1i_pipe_s1_t s1;
  // stage 2
  l1i_pipe_s2_t s2;
} l1i_pipe_reg_t;

// fencei flush fsm
typedef enum logic [2:0] {
  FLUSH_IDLE,
  FLUSH_PENDING,
  FLUSH_WRITE_LST,                // write lst all line invalid
  FLUSH_FINISH
} l1i_bank_fencei_flush_state_t;

// eviction fsm
typedef enum logic [1:0] {
  IDLE,
  IN_AGE_EVICT,     // stb full; 
  SELECTED_EVICT,   // load partial hit; coherence snoop hit;
  FLUSH             // stb flush (evict all);
} l1i_stb_evict_state_t;

  //------------------------------------------------------
  // MSHR definition
  typedef enum logic [2:0] {
    MSHR_WAIT_WRITE_BACK,
    MSHR_WRITE_BACK,
    MSHR_WAIT_ALLOCATE
  } mshr_state_t;

  typedef struct packed {
    // axi bus status
    logic                   waddr;
    logic                   wdata;
  } mshr_mem_state_t;

typedef enum logic [2:0] {
  FENCEI_IDLE,
  FENCEI_WAITING_FOR_BANK_GRANT
} l1i_fencei_state_t;

  typedef struct packed {
    // cache data write pipeline
    logic                                   wvalid;
    // wdata_pipe_t                            wdata_pipe;
    logic [$clog2(BURST_SIZE)-1:0]          wdata_offset;
    //mshr_mem_state_t [N_MSHR-1:0]           mem_state;
    //mem_offset_t rdata_offset;

    // data output pipeline
  //  cpu_resp_t  rdata_pipe;
    // logic       rdata_pipe_valid;
  } mem_fsm_reg_t;

    
// ------ mlfb ------// 
  typedef struct packed {
    logic [L1I_BANK_SET_INDEX_WIDTH-1:0] set_idx;
    logic [L1I_BANK_WAY_NUM-1:0] way_idx;
} rrv64_l1i_evict_req_t;

  typedef struct packed {
    logic [L1I_BANK_PADDR_TAG_WIDTH-1:0]      tag;
    logic [L1I_BANK_LINE_DATA_SIZE-1:0]    dat;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] boundary;
    logic [L1I_BANK_LINE_DATA_SIZE/16*2-1:0] rvc_mask;
    logic [L1I_BANK_SET_INDEX_WIDTH-1:0]  set_idx;
    logic [L1I_BANK_WAY_NUM-1:0]  way_idx;
    rrv64_mesi_type_e                mesi_sta;
    logic                            is_lr;
    logic                            is_ld;

    logic [L1I_BANK_OFFSET_WIDTH-1:0]         offset;  
// `ifdef RUBY
//     logic [RRV64_LSU_ID_WIDTH -1:0] lsu_tag;
// `endif
    rrv64_l1i_req_type_dec_t        req_type_dec;
    logic                             ld_no_resp;
  } rrv64_l1i_refill_req_t;


endpackage

`endif 