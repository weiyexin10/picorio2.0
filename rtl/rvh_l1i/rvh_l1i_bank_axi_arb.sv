module rvh_l1i_bank_axi_arb
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_l1i_pkg::*;
#(
  parameter INPUT_PORT_NUM = L1I_BANK_ID_NUM,
  parameter INPUT_PORT_NUM_INDEX_WIDTH = $clog2(INPUT_PORT_NUM),
  parameter RESP_PORT_SELECT_BID_LSB = 0
)
(
    // l1i banks -> axi arb
      // AR
    input  logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_is_fetch_i,
    input  logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_arvalid_i,
    output logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_arready_o,
    input  cache_mem_if_ar_t [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_ar_i,
      // AW
    input  logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_awvalid_i,
    output logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_awready_o,
    input  cache_mem_if_aw_t [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_aw_i,
      // W
    input  logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_wvalid_i,
    output logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_wready_o,
    input  cache_mem_if_w_t  [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_w_i,
      // B
    output logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_bvalid_o,
    input  logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_bready_i,
    output cache_mem_if_b_t  [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_b_o,
      // R
    output logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_rvalid_o,
    input  logic             [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_rready_i,
    output cache_mem_if_r_t  [INPUT_PORT_NUM-1:0] l1i_bank_axi_arb_r_o,
    
    // axi arb -> L2
      // AR
    output logic              axi_arb_l2_arvalid_o,
    input  logic              axi_arb_l2_arready_i,
    output cache_mem_if_ar_t  axi_arb_l2_ar_o,
      // AW 
    output logic              axi_arb_l2_awvalid_o,
    input  logic              axi_arb_l2_awready_i,
    output cache_mem_if_aw_t  axi_arb_l2_aw_o,
      // W 
    output logic              axi_arb_l2_wvalid_o,
    input  logic              axi_arb_l2_wready_i,
    output cache_mem_if_w_t   axi_arb_l2_w_o,
      // B
    input  logic              axi_arb_l2_bvalid_i,
    output logic              axi_arb_l2_bready_o,
    input  cache_mem_if_b_t   axi_arb_l2_b_i,
      // R
    input  logic              axi_arb_l2_rvalid_i,
    output logic              axi_arb_l2_rready_o,
    input cache_mem_if_r_t    axi_arb_l2_r_i,
    
    input logic clk,
    input logic rst
    
);
	logic [INPUT_PORT_NUM-1:0]            	ar_grt, ar_rr_grt, ar_fetch_grt;
	logic [INPUT_PORT_NUM_INDEX_WIDTH-1:0]  ar_grt_idx, ar_rr_grt_idx, ar_fetch_grt_idx;
	logic 									update_rr_arb;

// cache bank input arbiter
	assign ar_fetch_grt = l1i_bank_axi_arb_is_fetch_i & l1i_bank_axi_arb_arvalid_i;

	one_hot_rr_arb #(
		.N_INPUT      (INPUT_PORT_NUM) 
	) ar_rr_arb_u (
		.req_i        (l1i_bank_axi_arb_arvalid_i ),
		.update_i     (update_rr_arb   	 		  ),
		.grt_o        (ar_rr_grt                  ),
		.grt_idx_o    (ar_rr_grt_idx              ),
		.rstn         (~rst                       ),
		.clk          (clk                        )
	);

	priority_encoder #(
		.SEL_WIDTH(INPUT_PORT_NUM)
	) ar_fetch_grt_idx_encoder (
		.sel_i(ar_fetch_grt),
		.id_vld_o(),
		.id_o(ar_fetch_grt_idx)
	);
	
	always_comb begin
        ar_grt = 0;
        ar_grt_idx = 0;
		update_rr_arb = 0;
		if (ar_fetch_grt != 0) begin
			// 1. has fetch req, select fetch req
			ar_grt = ar_fetch_grt;
			ar_grt_idx = ar_fetch_grt_idx;
		end
		else if (l1i_bank_axi_arb_arvalid_i != 0) begin
			// 2. no fetch req but has prefetch req, use rr arb
			ar_grt = ar_rr_grt;
			ar_grt_idx = ar_rr_grt_idx;
			update_rr_arb = |(ar_rr_grt & l1i_bank_axi_arb_arready_o);
		end
    end

// ar channel output
	always_comb begin
		l1i_bank_axi_arb_arready_o = '0;
		axi_arb_l2_arvalid_o = '0;
		for(int i = 0; i < INPUT_PORT_NUM; i++) begin
			if(ar_grt[i]) begin
				l1i_bank_axi_arb_arready_o[i] = axi_arb_l2_arready_i;
				axi_arb_l2_arvalid_o = l1i_bank_axi_arb_arvalid_i[i];
			end
		end
	end

	assign axi_arb_l2_ar_o = l1i_bank_axi_arb_ar_i[ar_grt_idx];

// r channel respond
	always_comb begin
		axi_arb_l2_rready_o = '0;
		l1i_bank_axi_arb_rvalid_o = '0;
		for(int i = 0; i < INPUT_PORT_NUM; i++) begin
			if(axi_arb_l2_r_i.rid.bid[RESP_PORT_SELECT_BID_LSB+:INPUT_PORT_NUM_INDEX_WIDTH] == i[INPUT_PORT_NUM_INDEX_WIDTH-1:0]) begin
				axi_arb_l2_rready_o = l1i_bank_axi_arb_rready_i[i];
				l1i_bank_axi_arb_rvalid_o [i] = axi_arb_l2_rvalid_i;
			end
		end
	end

	always_comb begin
		for (int i = 0; i < INPUT_PORT_NUM; i++) begin
			l1i_bank_axi_arb_r_o[i] = axi_arb_l2_r_i;
		end
	end
	
	assign l1i_bank_axi_arb_awready_o = 0;
	assign l1i_bank_axi_arb_wready_o = 0;
	assign l1i_bank_axi_arb_bvalid_o = 0;
	assign l1i_bank_axi_arb_b_o = 0;

	assign axi_arb_l2_awvalid_o = 0;
	assign axi_arb_l2_aw_o = 0;
	assign axi_arb_l2_wvalid_o = 0;
	assign axi_arb_l2_w_o = 0;
	assign axi_arb_l2_bready_o = 0;

// assertion
`ifndef SYNTHESIS
	always_ff @(posedge clk) begin
		if ((ar_fetch_grt & (ar_fetch_grt - 1'b1)) != 0) begin
			$fatal("l1i_bank_axi_arb_is_fetch_i is not one hot!");
		end
	end
`endif

endmodule