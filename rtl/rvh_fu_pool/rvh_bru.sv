module rvh_bru
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
(
    // Issue
    input  logic                          issue_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i,
    input  logic [     BRQ_TAG_WIDTH-1:0] issue_brq_tag_i,
    input  logic [     BTQ_TAG_WIDTH-1:0] issue_btq_tag_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i,
    input  logic [      BRU_OP_WIDTH-1:0] issue_opcode_i,
    input  logic                          issue_is_rvc_i,
    input  logic [              XLEN-1:0] issue_operand0_i,
    input  logic [              XLEN-1:0] issue_operand1_i,
    input  logic [       VADDR_WIDTH-1:0] issue_pc_i,
    input  logic [                  31:0] issue_imm_i,
    input  logic                          issue_pred_taken_i,
    input  logic [       VADDR_WIDTH-1:0] issue_pred_target_i,
    output logic                          issue_rdy_o,

    // Write Back
    output logic                          wb_vld_o,
    output logic [       VADDR_WIDTH-1:0] wb_pc_o,
    output logic [     ROB_TAG_WIDTH-1:0] wb_rob_tag_o,
    output logic [     BRQ_TAG_WIDTH-1:0] wb_brq_tag_o,
    output logic [     BTQ_TAG_WIDTH-1:0] wb_btq_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o,
    output logic [              XLEN-1:0] wb_data_o,
    output logic                          wb_taken_o,
    output logic                          wb_mispred_o,
    output logic [       VADDR_WIDTH-1:0] wb_target_o,
    input  logic                          wb_rdy_i,

    input logic flush_i,

    input clk,
    input rst
);

    logic is_jalr_inst, is_jal_inst, is_branch_inst;
    logic            is_cmp_u;
    logic [XLEN-1:0] jalr_imm;
    logic [XLEN-1:0] jal_imm;
    logic [XLEN-1:0] br_imm;
    logic [XLEN-1:0] pc_adder_src1;
    logic [XLEN-1:0] pc_adder_src2;
    logic [XLEN-1:0] pc_adder_out;
    logic [  XLEN:0] alu_adder_src1;
    logic [  XLEN:0] alu_adder_src2;
    logic [  XLEN:0] alu_adder_out;
    logic            alu_adder_out_msb;
    logic            alu_adder_out_is_zero;
    logic            is_branch_taken;
    logic            pc_mismatch;
    logic            br_pc_mismatch;
    logic            taken_mismatch;
    logic            br_predict_miss;
    logic            jump_predict_miss;




    assign is_jalr_inst = issue_opcode_i == BRU_JALR;
    assign is_jal_inst = issue_opcode_i == BRU_JAL;
    assign is_branch_inst = ~(is_jalr_inst | is_jal_inst);

    // pc adder.
    // for jal/jalr/branch instruction, it's the target pc
    //--------------------------------------------------------
    assign pc_adder_src1 = is_jalr_inst ? issue_operand0_i : issue_pc_i;
    assign pc_adder_src2 = {{XLEN-32{issue_imm_i[31]}},issue_imm_i};
    assign pc_adder_out = pc_adder_src1 + pc_adder_src2;

    // alu adder
    // for branch, is a substractor
    // for jal/jalr, it's destination register value
    assign alu_adder_src1 = is_branch_inst ?
        {issue_operand0_i[XLEN-1] & ~is_cmp_u, issue_operand0_i} : {{{XLEN-VADDR_WIDTH{issue_pc_i[VADDR_WIDTH-1]}},issue_pc_i}};
    assign alu_adder_src2 = is_branch_inst ?
        (~{issue_operand1_i[XLEN-1] & ~is_cmp_u, issue_operand1_i}) :
        (issue_is_rvc_i ? 65'd2 : 65'd4);
    assign alu_adder_out = alu_adder_src1 + alu_adder_src2 + {{XLEN{1'b0}}, is_branch_inst};

    assign alu_adder_out_msb = alu_adder_out[XLEN];
    assign alu_adder_out_is_zero = alu_adder_out == '0;
    assign is_branch_taken = (issue_opcode_i == BRU_BGE | issue_opcode_i == BRU_BGEU) &
        (~alu_adder_out_msb | alu_adder_out_is_zero) |
        (issue_opcode_i == BRU_BLT | issue_opcode_i == BRU_BLTU) & (alu_adder_out_msb) |
        (issue_opcode_i == BRU_BEQ) & alu_adder_out_is_zero |
        (issue_opcode_i == BRU_BNE) & (~alu_adder_out_is_zero);

    assign is_cmp_u = (issue_opcode_i == BRU_BLTU) |  (issue_opcode_i == BRU_BGEU);

    // Branch
    // there are serveral kinds of miss
    // 1. actual taken, predict miss
    // 2. actual not taken, predict taken
    // 3. actual taken, predict taken,pc mismatch
    assign pc_mismatch = ({pc_adder_out[1+:VADDR_WIDTH-1], 1'b0} != issue_pred_target_i);

    //branch
    assign taken_mismatch = is_branch_taken ^ issue_pred_taken_i;
    assign br_pc_mismatch = is_branch_taken & issue_pred_taken_i & pc_mismatch;
    assign br_predict_miss = (taken_mismatch | br_pc_mismatch) & is_branch_inst;
    assign jump_predict_miss = (is_jalr_inst | is_jal_inst) & (pc_mismatch | (~issue_pred_taken_i));


    // output
    assign wb_vld_o = issue_vld_i & ~flush_i;
    assign wb_pc_o = issue_pc_i;
    assign wb_rob_tag_o = issue_rob_tag_i;
    assign wb_brq_tag_o = issue_brq_tag_i;
    assign wb_btq_tag_o = issue_btq_tag_i;
    assign wb_phy_rd_o = issue_phy_rd_i;
    assign wb_data_o = alu_adder_out[XLEN-1:0];
    assign wb_taken_o = is_branch_taken | is_jalr_inst | is_jal_inst;
    assign wb_mispred_o = issue_vld_i & (br_predict_miss | jump_predict_miss);
    assign wb_target_o = (is_branch_inst & ~is_branch_taken) ?
        (issue_pc_i + (issue_is_rvc_i ? 64'd2 : 64'd4)) : {pc_adder_out[1+:XLEN-1], 1'b0};

    assign issue_rdy_o = wb_rdy_i;





endmodule : rvh_bru
