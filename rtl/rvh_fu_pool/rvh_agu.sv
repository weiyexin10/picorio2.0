module rvh_agu
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
(
    // Issue
    input  logic                          issue_vld_i,
    input  logic                          issue_fu_type_i,
    input  logic [      LSU_OP_WIDTH-1:0] issue_opcode_i,
    input  logic [              XLEN-1:0] issue_operand0_i,
    input  logic [                  11:0] issue_imm_i,
    output logic                          issue_rdy_o,

    // Write Back
    output logic                          wb_vld_o,
    output logic [       VADDR_WIDTH-1:0] wb_address_o,
    output logic                          wb_excp_vld_o,
    input  logic                          wb_rdy_i,

    input logic flush_i,

    input clk,
    input rst
);

    logic [XLEN-1:0] imm_ext;
    logic [XLEN-1:0] adder_rslt;
    logic            misalign;
    logic ls_width_16, ls_width_32, ls_width_64;
    logic is_ld, is_st;

    assign is_ld = issue_fu_type_i == LSU_LD_TYPE;
    assign is_st = ~is_ld;

    assign imm_ext = 64'({{64{issue_imm_i[11]}}, issue_imm_i[11:0]});
    assign adder_rslt = issue_operand0_i + imm_ext;

    assign ls_width_16 = (is_ld & (issue_opcode_i == LDU_LH | issue_opcode_i == LDU_LHU)) |
        (is_st & issue_opcode_i == STU_SH);
    assign ls_width_32 = (is_ld & (issue_opcode_i == LDU_LW | issue_opcode_i == LDU_LWU)) |
        (is_st & issue_opcode_i == STU_SW);
    assign ls_width_64 = (is_ld & issue_opcode_i == LDU_LD) | (is_st & issue_opcode_i == STU_SD);

    assign misalign = (ls_width_16 & (|adder_rslt[0:0])) | (ls_width_32 & (|adder_rslt[1:0])) |
        (ls_width_64 & (|adder_rslt[2:0]));
    // output
    assign wb_vld_o = issue_vld_i & ~flush_i;
    assign wb_address_o = adder_rslt;
    assign wb_excp_vld_o = misalign;

    assign issue_rdy_o = wb_rdy_i;


endmodule : rvh_agu
