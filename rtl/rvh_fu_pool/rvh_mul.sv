module rvh_mul
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
(
    // Issue
    input  logic                          issue_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i,
    input  logic [      MUL_OP_WIDTH-1:0] issue_opcode_i,
    input  logic [              XLEN-1:0] issue_operand0_i,
    input  logic [              XLEN-1:0] issue_operand1_i,
    output logic                          issue_rdy_o,

    // Write Back
    output logic                          wb_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0] wb_rob_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o,
    output logic [              XLEN-1:0] wb_data_o,
    input  logic                          wb_rdy_i,

    input logic flush_i,

    input clk,
    input rst
);

    logic                          src1_is_signed;
    logic                          src2_is_signed;
    logic [                  32:0] mul_lo_src1;
    logic [                  32:0] mul_hi_src1;
    logic [                  32:0] mul_lo_src2;
    logic [                  32:0] mul_hi_src2;
    logic [                  65:0] mul_ll_rslt;  //mul_lo_src1 x mul_lo_src2
    logic [                  65:0] mul_lh_rslt;  //mul_lo_src1 x mul_hi_src2
    logic [                  65:0] mul_hl_rslt;  //mul_hi_src1 x mul_lo_src2
    logic [                  65:0] mul_hh_rslt;  //mul_hi_src1 x mul_hi_src2

    logic [                  65:0] mul_ll_rslt_ex2;  //mul_lo_src1 x mul_lo_src2
    logic [                  65:0] mul_lh_rslt_ex2;  //mul_lo_src1 x mul_hi_src2
    logic [                  65:0] mul_hl_rslt_ex2;  //mul_hi_src1 x mul_lo_src2
    logic [                  65:0] mul_hh_rslt_ex2;  //mul_hi_src1 x mul_hi_src2

    logic                          use_hi_part;
    logic                          use_hi_part_ex2;
    logic                          rslt_word_type;
    logic                          rslt_word_type_ex2;
    logic [     ROB_TAG_WIDTH-1:0] rob_tag_ex2;
    logic [INT_PREG_TAG_WIDTH-1:0] phy_rd_ex2;
    logic                          mul_stg2_rdy;
    logic                          mul_stg1_vld;
    logic                          mul_stg2_vld;

    logic [                 129:0] mul_ll_rslt_ext_ex2;
    logic [                 129:0] mul_lh_rslt_ext_ex2;
    logic [                 129:0] mul_hl_rslt_ext_ex2;
    logic [                 129:0] mul_hh_rslt_ext_ex2;
    logic [                 129:0] mul_rslt_stg2_ex2;

    logic                          int_reg_wr_ready_fin;


    assign src1_is_signed = issue_opcode_i == MUL_MUL | issue_opcode_i == MUL_MULH |
        issue_opcode_i == MUL_MULHSU;

    assign src2_is_signed = issue_opcode_i == MUL_MUL | issue_opcode_i == MUL_MULH;

    assign use_hi_part = issue_opcode_i == MUL_MULH | issue_opcode_i == MUL_MULHSU |
        issue_opcode_i == MUL_MULHU;

    assign rslt_word_type = issue_opcode_i == MUL_MULW;

    assign mul_lo_src1 = {1'b0, issue_operand0_i[31:0]};
    assign mul_hi_src1 = {src1_is_signed & issue_operand0_i[63], issue_operand0_i[63:32]};
    assign mul_lo_src2 = {1'b0, issue_operand1_i[31:0]};
    assign mul_hi_src2 = {src2_is_signed & issue_operand1_i[63], issue_operand1_i[63:32]};

    assign mul_ll_rslt = mul_lo_src1 * mul_lo_src2;
    assign mul_lh_rslt = $signed(mul_lo_src1) * $signed(mul_hi_src2);
    assign mul_hl_rslt = $signed(mul_hi_src1) * $signed(mul_lo_src2);
    assign mul_hh_rslt = $signed(mul_hi_src1) * $signed(mul_hi_src2);

    assign mul_stg1_vld = issue_vld_i & (~flush_i);
    assign mul_stg2_rdy = ~mul_stg2_vld | (wb_rdy_i | rob_tag_ex2 == '0);


    always_ff @(posedge clk) begin
        if (rst) begin
            mul_stg2_vld <= '0;
        end else if (mul_stg2_rdy) begin
            mul_stg2_vld <= mul_stg1_vld;
        end
    end

    always_ff @(posedge clk) begin
        if (mul_stg2_rdy) begin
            use_hi_part_ex2    <= use_hi_part;
            rslt_word_type_ex2 <= rslt_word_type;
            rob_tag_ex2        <= issue_rob_tag_i;
            phy_rd_ex2         <= issue_phy_rd_i;
            mul_ll_rslt_ex2    <= mul_ll_rslt;
            mul_lh_rslt_ex2    <= mul_lh_rslt;
            mul_hl_rslt_ex2    <= mul_hl_rslt;
            mul_hh_rslt_ex2    <= mul_hh_rslt;
        end
    end

    assign mul_ll_rslt_ext_ex2 = {{130 - 66{1'b0}}, mul_ll_rslt_ex2};
    assign mul_lh_rslt_ext_ex2 = {{32{mul_lh_rslt_ex2[65]}}, mul_lh_rslt_ex2, 32'h0};
    assign mul_hl_rslt_ext_ex2 = {{32{mul_hl_rslt_ex2[65]}}, mul_hl_rslt_ex2, 32'h0};
    assign mul_hh_rslt_ext_ex2 = {mul_hh_rslt_ex2, 64'h0};

    assign mul_rslt_stg2_ex2 = $signed(
        mul_ll_rslt_ext_ex2
    ) + $signed(
        mul_lh_rslt_ext_ex2
    ) + $signed(
        mul_hl_rslt_ext_ex2
    ) + $signed(
        mul_hh_rslt_ext_ex2
    );

    // output
    assign wb_vld_o = mul_stg2_vld & ~flush_i;
    assign wb_rob_tag_o = rob_tag_ex2;
    assign wb_phy_rd_o = phy_rd_ex2;
    assign wb_data_o = {64{use_hi_part_ex2}} & mul_rslt_stg2_ex2[64+:64] |
        {64{~use_hi_part_ex2 & ~rslt_word_type_ex2}} & mul_rslt_stg2_ex2[0+:64] |
        {64{~use_hi_part_ex2 & rslt_word_type_ex2}} &
        {{32{mul_rslt_stg2_ex2[31]}}, mul_rslt_stg2_ex2[0+:32]};
    assign issue_rdy_o = ~mul_stg1_vld | mul_stg2_rdy;




endmodule : rvh_mul
