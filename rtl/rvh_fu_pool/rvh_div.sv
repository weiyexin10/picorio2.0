`ifndef FPGA

module rvh_div
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
#(
    parameter  int unsigned LATENCY   = 4,
    localparam int unsigned CNT_WIDTH = $clog2(LATENCY + 1)
) (
    // Issue
    input  logic                          issue_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i,
    input  logic [      DIV_OP_WIDTH-1:0] issue_opcode_i,
    input  logic [              XLEN-1:0] issue_operand0_i,
    input  logic [              XLEN-1:0] issue_operand1_i,
    output logic                          issue_rdy_o,

    // Write Back
    output logic                          wb_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0] wb_rob_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o,
    output logic [              XLEN-1:0] wb_data_o,
    input  logic                          wb_rdy_i,

    output logic [CNT_WIDTH-1:0] complete_cnt_o,

    input logic flush_i,

    input clk,
    input rst
);

  parameter a_width = 65;
  parameter b_width = 65;
  parameter tc_mode = 1;
  parameter num_cyc = 8;
  parameter rst_mode = 1;
  parameter input_mode = 0;
  parameter output_mode = 0;
  parameter early_start = 0;

  enum logic[2:0] {
    IDLE,
    BUSY,
    DIV_0_BUSY,
    OVERFLOW_BUSY
  } state, next_state;

  logic is_div_0, is_overflow;
  logic [XLEN:0] a, b, q, r;
  logic divider_complete, complete;
  logic [XLEN-1:0] q_64, q_32;
  logic [XLEN-1:0] r_64, r_32;
  logic [XLEN-1:0] rdr, rdq;

  logic                          issue_fire;
  logic [      DIV_OP_WIDTH-1:0] opcode_ff;
  logic [     ROB_TAG_WIDTH-1:0] rob_tag_ff;
  logic [INT_PREG_TAG_WIDTH-1:0] phy_rd_ff;
  logic [              XLEN-1:0] operand0_ff;
  logic [              XLEN-1:0] operand1_ff;
  logic [         CNT_WIDTH-1:0] counter;

  always_ff @(posedge clk) begin
    if (rst) begin
      counter <= LATENCY;
    end else begin
      if (state == IDLE & next_state != IDLE) begin
        counter <= LATENCY;
      end else begin
        if ((state == BUSY) & (divider_complete)) begin
          counter <= counter - 1'b1;
        end
        else if (state == DIV_0_BUSY) begin
          counter <= counter - 1'b1;
        end
        else if (state == OVERFLOW_BUSY) begin
          counter <= counter - 1'b1;
        end
      end
    end
  end

  assign complete_cnt_o = counter;

  always_ff @(posedge clk) begin
    if (issue_fire) begin
      rob_tag_ff <= issue_rob_tag_i;
      phy_rd_ff  <= issue_phy_rd_i;
    end
  end
  
  assign opcode_ff   = issue_opcode_i;
  assign operand0_ff = issue_operand0_i;
  assign operand1_ff = issue_operand1_i;

  always_comb
    case (opcode_ff)
      DIV_DIVU, DIV_REMU: begin
        a = {1'b0,operand0_ff};
        b = {1'b0,operand1_ff};
        is_div_0 = operand1_ff == 64'b0;
        is_overflow = 1'b0;
      end
      DIV_DIVW, DIV_REMW: begin
        a = {{33{operand0_ff[31]}}, operand0_ff[31:0]};
        b = {{33{operand1_ff[31]}}, operand1_ff[31:0]};
        is_div_0 = operand1_ff[31:0] == 32'b0;
        is_overflow = (operand0_ff[31:0] == {{1'b1},{31{1'b0}}}) & (operand1_ff[31:0] == {32{1'b1}});
      end
      DIV_DIVUW, DIV_REMUW: begin
        a = {{33'b0}, operand0_ff[31:0]};
        b = {{33'b0}, operand1_ff[31:0]};
        is_div_0 = operand1_ff[31:0] == 32'b0;
        is_overflow = 1'b0;
      end
      DIV_DIV, DIV_REM: begin
        a = {operand0_ff[XLEN-1],operand0_ff};
        b = {operand1_ff[XLEN-1],operand1_ff};
        is_div_0 = operand1_ff == 64'b0;
        is_overflow = (operand0_ff == {{1'b1},{63{1'b0}}})& (operand1_ff == {64{1'b1}});
      end
      default:;
    endcase

  DW_div_seq 
    #(a_width, b_width, tc_mode, num_cyc, rst_mode, input_mode, output_mode, early_start) 
    DW_DIV_SEQ(
      .clk        (clk),
      .rst_n      (~rst),
      .hold       (1'b0),
      .start      (issue_fire & ~is_div_0 & ~is_overflow),
      .a          (a),
      .b          (b),
      .complete   (divider_complete),
      .divide_by_0(),
      .quotient   (q),
      .remainder  (r)
  );

  assign complete = (state != IDLE) & (counter == 0);

  assign q_64 = q[63:0];
  assign q_32 = {{32{q[31]}}, q[31:0]};

  always_comb
    case (opcode_ff)
      DIV_DIVW:
      unique case (1'b1)
        is_div_0:    rdq = {64{1'b1}};
        is_overflow: rdq = {{32{operand0_ff[31]}},operand0_ff[31:0]};
        default:     rdq = q_32;
      endcase
      DIV_DIVUW:
      if (is_div_0) rdq = {64{1'b1}};
      else rdq = q_32;
      DIV_DIV:
      unique case (1'b1)
        is_div_0:    rdq = {64{1'b1}};
        is_overflow: rdq = operand0_ff[63:0];
        default:     rdq = q_64;
      endcase
      DIV_DIVU:
      if (is_div_0) rdq = {64{1'b1}};
      else rdq = q_64;
      default: rdq = q_64;
    endcase

  assign r_64 = r[63:0];
  assign r_32 = {{32{r[31]}}, r[31:0]};

  always_comb
    case (opcode_ff)
      DIV_REMW:
      unique case (1'b1)
        is_div_0:    rdr = {{32{operand0_ff[31]}},operand0_ff[31:0]};
        is_overflow: rdr = 64'b0;
        default:     rdr = r_32;
      endcase
      DIV_REMUW:
      if (is_div_0)  rdr = {{32{operand0_ff[31]}},operand0_ff[31:0]};
      else rdr = r_32;
      DIV_REM:
      unique case (1'b1)
        is_div_0:    rdr = operand0_ff[63:0];
        is_overflow: rdr = 64'b0;
        default:     rdr = r_64;
      endcase
      DIV_REMU:
      if (is_div_0) rdr = operand0_ff[63:0];
      else rdr = r_64;
      default: rdr = r[63:0];
    endcase


  // control

  always @(posedge clk) begin
    if (rst) begin
      state <= IDLE;
    end else begin
      state <= next_state;
    end
  end

  always_comb begin
    next_state = state;
    case (state)
      IDLE: begin
        if (issue_fire) begin
          if (is_div_0) begin
            next_state = DIV_0_BUSY;
          end
          else if (is_overflow) begin
            next_state = OVERFLOW_BUSY;
          end
          else begin
            next_state = BUSY;
          end
        end
      end
      DIV_0_BUSY, OVERFLOW_BUSY, BUSY: begin
        if (complete & ~issue_fire) begin
          next_state = IDLE;
        end
      end
    endcase
    if(flush_i) begin
        next_state = IDLE;
    end
  end

  // output
  assign wb_vld_o     = complete & ~(flush_i);
  assign wb_rob_tag_o = rob_tag_ff;
  assign wb_phy_rd_o  = phy_rd_ff;
  always_comb
    case (opcode_ff)
      DIV_DIVU, DIV_DIVW, DIV_DIVUW, DIV_DIV: begin
        wb_data_o = rdq;
      end
      default: begin  //DIV_REMU, DIV_REMW, DIV_REMUW, DIV_REM: begin
        wb_data_o = rdr;
      end
    endcase


  // assign issue_rdy_o = (state == IDLE) | ((state == BUSY) & (complete & wb_rdy_i));
  assign issue_rdy_o = state == IDLE;
  assign issue_fire = issue_vld_i & issue_rdy_o & ~flush_i;


endmodule : rvh_div

`else

module rvh_div
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
#(
    parameter  int unsigned LATENCY   = 4,
    localparam int unsigned CNT_WIDTH = $clog2(LATENCY + 1)
) (
    // Issue
    input  logic                          issue_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_i,
    input  logic [      DIV_OP_WIDTH-1:0] issue_opcode_i,
    input  logic [              XLEN-1:0] issue_operand0_i,
    input  logic [              XLEN-1:0] issue_operand1_i,
    output logic                          issue_rdy_o,

    // Write Back
    output logic                          wb_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0] wb_rob_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] wb_phy_rd_o,
    output logic [              XLEN-1:0] wb_data_o,
    input  logic                          wb_rdy_i,

    output logic [CNT_WIDTH-1:0] complete_cnt_o,

    input logic flush_i,

    input clk,
    input rst
);



  parameter a_width = 65;
  parameter b_width = 65;
  parameter tc_mode = 1;
  parameter num_cyc = 8;
  parameter rst_mode = 1;
  parameter input_mode = 0;
  parameter output_mode = 0;
  parameter early_start = 0;

  enum logic[2:0] {
    IDLE,
    SIGNED_BUSY,
    UNSIGNED_BUSY,
    DIV_0_BUSY,
    OVERFLOW_BUSY
  } state, next_state;
  
  logic             is_div_0, is_overflow;
  logic [XLEN:0]    a, b;
  logic [XLEN-1:0]  q, r;
  logic [XLEN-1:0]  signed_q, signed_r;
  logic [XLEN-1:0]  unsigned_q, unsigned_r;
  logic             complete;
  logic [XLEN-1:0]  q_64, q_32;
  logic [XLEN-1:0]  r_64, r_32;
  logic [XLEN-1:0]  rdr, rdq;

  logic                          signed_div_ready, signed_divisor_ready, signed_dividend_ready;
  logic                          signed_div_done;
  logic                          unsigned_div_ready, unsigned_divisor_ready, unsigned_dividend_ready;
  logic                          unsigned_div_done;

  logic                          issue_fire;
  logic                          is_signed;
  logic [      DIV_OP_WIDTH-1:0] opcode_ff;
  logic [     ROB_TAG_WIDTH-1:0] rob_tag_ff;
  logic [INT_PREG_TAG_WIDTH-1:0] phy_rd_ff;
  logic [              XLEN-1:0] operand0_ff;
  logic [              XLEN-1:0] operand1_ff;
  
  logic [         CNT_WIDTH-1:0] counter;

  always_ff @(posedge clk) begin
    if (rst) begin
      counter <= LATENCY;
    end else begin
      if ((state == IDLE) & (next_state != IDLE)) begin
        counter <= LATENCY;
      end else begin
        if ((state == SIGNED_BUSY) & (signed_div_done)) begin
          counter <= counter - 1'b1;
        end
        else if ((state == UNSIGNED_BUSY) & (unsigned_div_done)) begin
          counter <= counter - 1'b1;
        end
        else if (state == DIV_0_BUSY) begin
          counter <= counter - 1'b1;
        end
        else if (state == OVERFLOW_BUSY) begin
          counter <= counter - 1'b1;
        end
      end
    end
  end

  assign complete_cnt_o = counter;

  always_ff @(posedge clk) begin
    if (issue_fire) begin
      rob_tag_ff <= issue_rob_tag_i;
      phy_rd_ff  <= issue_phy_rd_i;
    end
  end
  
  assign opcode_ff   = issue_opcode_i;
  assign operand0_ff = issue_operand0_i;
  assign operand1_ff = issue_operand1_i;

  always_comb
    case (opcode_ff)
      DIV_DIVU, DIV_REMU: begin
        a = {1'b0,operand0_ff};
        b = {1'b0,operand1_ff};
        is_div_0 = operand1_ff == 64'b0;
        is_overflow = 1'b0;
      end
      DIV_DIVW, DIV_REMW: begin
        a = {{33{operand0_ff[31]}}, operand0_ff[31:0]};
        b = {{33{operand1_ff[31]}}, operand1_ff[31:0]};
        is_div_0 = operand1_ff[31:0] == 32'b0;
        is_overflow = (operand0_ff[31:0] == {{1'b1},{31{1'b0}}}) & (operand1_ff[31:0] == {32{1'b1}});
      end
      DIV_DIVUW, DIV_REMUW: begin
        a = {{33'b0}, operand0_ff[31:0]};
        b = {{33'b0}, operand1_ff[31:0]};
        is_div_0 = operand1_ff[31:0] == 32'b0;
        is_overflow = 1'b0;
      end
      DIV_DIV, DIV_REM: begin
        a = {operand0_ff[XLEN-1],operand0_ff};
        b = {operand1_ff[XLEN-1],operand1_ff};
        is_div_0 = operand1_ff == 64'b0;
        is_overflow = (operand0_ff == {{1'b1},{63{1'b0}}})& (operand1_ff == {64{1'b1}});
      end
      default:;
    endcase

  signed_div u_signed_div (
    .aclk                   (clk), 
    .aresetn                (~rst),
    .s_axis_divisor_tvalid  (issue_fire & ~is_div_0 & ~is_overflow), 
    .s_axis_divisor_tready  (signed_divisor_ready), 
    .s_axis_divisor_tdata   (b[63:0]), 
    .s_axis_dividend_tvalid (issue_fire & ~is_div_0 & ~is_overflow), 
    .s_axis_dividend_tready (signed_dividend_ready), 
    .s_axis_dividend_tdata  (a[63:0]), 
    
    .m_axis_dout_tvalid     (signed_div_done), 
    .m_axis_dout_tdata      ({signed_q, signed_r})
  );

  assign signed_div_ready = signed_divisor_ready & signed_dividend_ready;


  unsigned_div u_unsigned_div (
    .aclk                   (clk), 
    .aresetn                (~rst),
    .s_axis_divisor_tvalid  (issue_fire & ~is_div_0 & ~is_overflow), 
    .s_axis_divisor_tready  (unsigned_divisor_ready), 
    .s_axis_divisor_tdata   (b[63:0]), 
    .s_axis_dividend_tvalid (issue_fire & ~is_div_0 & ~is_overflow), 
    .s_axis_dividend_tready (unsigned_dividend_ready), 
    .s_axis_dividend_tdata  (a[63:0]), 

    .m_axis_dout_tvalid     (unsigned_div_done), 
    .m_axis_dout_tdata      ({unsigned_q, unsigned_r})
  );
  
  assign unsigned_div_ready = unsigned_divisor_ready & unsigned_dividend_ready;

  // DW_div_seq 
  //   #(a_width, b_width, tc_mode, num_cyc, rst_mode, input_mode, output_mode, early_start) 
  //   DW_DIV_SEQ(
  //     .clk        (clk),
  //     .rst_n      (~rst),
  //     .hold       (1'b0),
  //     .start      (issue_fire & ~is_div_0 & ~is_overflow),
  //     .a          (a),
  //     .b          (b),
  //     .complete   (),
  //     .divide_by_0(),
  //     .quotient   (q),
  //     .remainder  (r)
  // );

  assign complete = (state != IDLE) & (counter == 0);

  assign q_64 = q[63:0];
  assign q_32 = {{32{q[31]}}, q[31:0]};

  always_comb
    case (opcode_ff)
      DIV_DIVW:
        unique case (1'b1)
          is_div_0:    rdq = {64{1'b1}};
          is_overflow: rdq = {{32{operand0_ff[31]}},operand0_ff[31:0]};
          default:     rdq = q_32;
        endcase
      DIV_DIVUW:
        if (is_div_0) rdq = {64{1'b1}};
        else rdq = q_32;
      DIV_DIV:
        unique case (1'b1)
          is_div_0:    rdq = {64{1'b1}};
          is_overflow: rdq = operand0_ff[63:0];
          default:     rdq = q_64;
        endcase
      DIV_DIVU:
        if (is_div_0) rdq = {64{1'b1}};
        else rdq = q_64;
      default: rdq = q_64;
    endcase

  assign r_64 = r[63:0];
  assign r_32 = {{32{r[31]}}, r[31:0]};

  always_comb
    case (opcode_ff)
      DIV_REMW:
        unique case (1'b1)
          is_div_0:    rdr = {{32{operand0_ff[31]}},operand0_ff[31:0]};
          is_overflow: rdr = 64'b0;
          default:     rdr = r_32;
        endcase
      DIV_REMUW:
        if (is_div_0)  rdr = {{32{operand0_ff[31]}},operand0_ff[31:0]};
        else rdr = r_32;
      DIV_REM:
        unique case (1'b1)
          is_div_0:    rdr = operand0_ff[63:0];
          is_overflow: rdr = 64'b0;
          default:     rdr = r_64;
        endcase
      DIV_REMU:
        if (is_div_0) rdr = operand0_ff[63:0];
        else rdr = r_64;
      default: rdr = r[63:0];
    endcase


  // control
  assign is_signed = (opcode_ff == DIV_DIV) | (opcode_ff == DIV_REM) | (opcode_ff == DIV_DIVW) | (opcode_ff == DIV_REMW);

  always @(posedge clk) begin
    if (rst) begin
      state <= IDLE;
    end else begin
      state <= next_state;
    end
  end

  always_comb begin
    next_state = state;
    case (state)
      IDLE: begin
        if (issue_fire) begin
          if (is_div_0) begin
            next_state = DIV_0_BUSY;
          end
          else if (is_overflow) begin
            next_state = OVERFLOW_BUSY;
          end
          else begin
            next_state = is_signed ? SIGNED_BUSY : UNSIGNED_BUSY;
          end
        end 
      end
      SIGNED_BUSY, UNSIGNED_BUSY, DIV_0_BUSY, OVERFLOW_BUSY: begin
        if (complete & ~issue_fire) begin
          next_state = IDLE;
        end
      end
    endcase
    if(flush_i) begin
        next_state = IDLE;
    end
  end

  // output
  assign wb_vld_o     = complete & ~(flush_i);
  assign wb_rob_tag_o = rob_tag_ff;
  assign wb_phy_rd_o  = phy_rd_ff;
  always_comb
    case (opcode_ff)
      DIV_DIVU, DIV_DIVW, DIV_DIVUW, DIV_DIV: begin
        wb_data_o = rdq;
      end
      default: begin  //DIV_REMU, DIV_REMW, DIV_REMUW, DIV_REM: begin
        wb_data_o = rdr;
      end
    endcase

  always_comb begin
    issue_rdy_o = '0;
    if (state == IDLE) begin
      if (is_signed) begin
        issue_rdy_o = signed_div_ready;
      end
      else begin
        issue_rdy_o = unsigned_div_ready;
      end
    end
  end


  // assign issue_rdy_o = (state == IDLE) | ((state == BUSY) & (complete & wb_rdy_i));
  assign issue_fire = issue_vld_i & issue_rdy_o & ~flush_i;


endmodule : rvh_div

`endif
