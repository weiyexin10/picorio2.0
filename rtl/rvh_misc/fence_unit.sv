module fence_unit
  import riscv_pkg::*;
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
(
    input logic [ROB_TAG_WIDTH-1:0] fence_req_rob_tag_i,
    input logic [VADDR_WIDTH-1:0] fence_req_pc_i,
    // FENCE.I
    input logic fence_i_req_vld_i,
    // SFENCE.VMA
    input logic sfence_vma_req_vld_i,
    input logic sfence_vma_req_use_asid_i,
    input logic sfence_vma_req_use_vpn_i,
    input logic [VPN_WIDTH-1:0] sfence_vma_req_vpn_i,
    input logic [ASID_WIDTH-1:0] sfence_vma_req_asid_i,
    // Execute FENCE.I : Flush Icache
    output logic flush_icache_req_o,
    input logic flush_icache_gnt_i,
    // Execute FENCE.I : Flush Dcache
    output logic flush_dcache_req_o,
    input logic flush_dcache_gnt_i,
    // Execute SFENCE.VMA : Flush ITLB
    output logic flush_itlb_req_o,
    output logic flush_itlb_use_vpn_o,
    output logic flush_itlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_itlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_itlb_asid_o,
    input  logic flush_itlb_gnt_i,
    // Execute SFENCE.VMA : Flush DTLB
    output logic flush_dtlb_req_o,
    output logic flush_dtlb_use_vpn_o,
    output logic flush_dtlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_dtlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_dtlb_asid_o,
    input  logic flush_dtlb_gnt_i,
    // Execute SFENCE.VMA : Flush STLB
    output logic flush_stlb_req_o,
    output logic flush_stlb_use_vpn_o,
    output logic flush_stlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_stlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_stlb_asid_o,
    input  logic flush_stlb_gnt_i,
    // Clear Busy
    output logic rob_cb_vld_o,
    output logic [ROB_TAG_WIDTH-1:0] rob_cb_tag_o,
    // Redirect 
    output logic redirect_vld_o,
    output logic [VADDR_WIDTH-1:0] redirect_pc_o,
    output logic [VADDR_WIDTH-1:0] redirect_target_o,

    input logic clk,
    input logic rst
);
  // FSM State
  localparam STATE_WIDTH = 2;
  localparam STATE_IDLE = 2'b00;
  localparam STATE_FLUSH_DC = 2'b01;
  localparam STATE_FLUSH_IC = 2'b11;
  localparam STATE_FLUSH_TLB = 2'b10;

  logic idle2flushdc;
  logic flushdc2flushic;
  logic flushic2idle;
  logic idle2flushtlb;
  logic flushtlb2idle;

  // fence status
  logic fence_state_clk_en;
  logic [STATE_WIDTH-1:0]fence_state_d, fence_state_q;
  // pending flush
  logic flush_l1i_pending_clk_en;
  logic flush_l1i_pending_set;
  logic flush_l1i_pending_clean;
  logic flush_l1i_pending_d, flush_l1i_pending_q;
  logic flush_l1d_pending_clk_en;
  logic flush_l1d_pending_set;
  logic flush_l1d_pending_clean;
  logic flush_l1d_pending_d, flush_l1d_pending_q;
  logic flush_l2_pending_clk_en;
  logic flush_l2_pending_set;
  logic flush_l2_pending_clean;
  logic flush_l2_pending_d, flush_l2_pending_q;

  assign fence_state_clk_en = idle2flushdc | flushdc2flushic | flushic2idle | idle2flushtlb | flushtlb2idle;
  assign fence_state_d = ({STATE_WIDTH{idle2flushdc}} & STATE_FLUSH_DC) |
    ({STATE_WIDTH{flushdc2flushic}} & STATE_FLUSH_IC) |
    ({STATE_WIDTH{idle2flushtlb}} & STATE_FLUSH_TLB) |
    ({STATE_WIDTH{flushtlb2idle | flushic2idle}} & STATE_IDLE);

  assign idle2flushdc = fence_state_q == STATE_IDLE & fence_i_req_vld_i;
  assign flushdc2flushic = fence_state_q == STATE_FLUSH_DC & flush_dcache_gnt_i;
  assign flushic2idle = fence_state_q == STATE_FLUSH_IC & flush_icache_gnt_i;
  assign idle2flushtlb = fence_state_q == STATE_IDLE & sfence_vma_req_vld_i;
  assign flushtlb2idle = fence_state_q == STATE_FLUSH_TLB &
    (flush_l1i_pending_q | flush_itlb_gnt_i) &
    (flush_l1d_pending_q | flush_dtlb_gnt_i);

  assign flush_l1i_pending_clk_en = flush_l1i_pending_set | flush_l1i_pending_clean;
  assign flush_l1i_pending_set = idle2flushtlb;
  assign flush_l1i_pending_clean = (fence_state_q == STATE_FLUSH_TLB) & flush_itlb_gnt_i;
  assign flush_l1i_pending_d = flush_l1i_pending_set & ~flush_l1i_pending_clean;

  assign flush_l1d_pending_clk_en = flush_l1d_pending_set | flush_l1d_pending_clean;
  assign flush_l1d_pending_set = idle2flushtlb;
  assign flush_l1d_pending_clean = (fence_state_q == STATE_FLUSH_TLB) & flush_dtlb_gnt_i;
  assign flush_l1d_pending_d = flush_l1d_pending_set & ~flush_l1d_pending_clean;

  assign flush_l2_pending_clk_en = flush_l2_pending_set | flush_l2_pending_clean;
  assign flush_l2_pending_set = idle2flushtlb;
  assign flush_l2_pending_clean = (fence_state_q == STATE_FLUSH_TLB) & flush_stlb_gnt_i;
  assign flush_l2_pending_d = flush_l2_pending_set & ~flush_l2_pending_clean;

  assign rob_cb_vld_o = flushtlb2idle | flushic2idle;
  assign rob_cb_tag_o = fence_req_rob_tag_i;

  assign redirect_vld_o = rob_cb_vld_o;
  assign redirect_pc_o = fence_req_pc_i;
  assign redirect_target_o = fence_req_pc_i + 3'd4;

  assign flush_icache_req_o = flushdc2flushic;
  assign flush_dcache_req_o = idle2flushdc;

  assign flush_itlb_req_o      = idle2flushtlb;
  assign flush_itlb_use_vpn_o  = sfence_vma_req_use_vpn_i;
  assign flush_itlb_use_asid_o = sfence_vma_req_use_asid_i;
  assign flush_itlb_vpn_o      = sfence_vma_req_vpn_i;
  assign flush_itlb_asid_o     = sfence_vma_req_asid_i;

  assign flush_dtlb_req_o      = idle2flushtlb;
  assign flush_dtlb_use_vpn_o  = sfence_vma_req_use_vpn_i;
  assign flush_dtlb_use_asid_o = sfence_vma_req_use_asid_i;
  assign flush_dtlb_vpn_o      = sfence_vma_req_vpn_i;
  assign flush_dtlb_asid_o     = sfence_vma_req_asid_i;


  assign flush_stlb_req_o      = idle2flushtlb;
  assign flush_stlb_use_vpn_o  = sfence_vma_req_use_vpn_i;
  assign flush_stlb_use_asid_o = sfence_vma_req_use_asid_i;
  assign flush_stlb_vpn_o      = sfence_vma_req_vpn_i;
  assign flush_stlb_asid_o     = sfence_vma_req_asid_i;

  DFFRE #(
      .Width(STATE_WIDTH)
  ) u_fence_state_DFFRE (
      .CLK(clk),
      .RSTN(~rst),
      .EN(fence_state_clk_en),
      .DRST(STATE_IDLE),
      .D(fence_state_d),
      .Q(fence_state_q)
  );

  DFFE #(
      .Width(1)
  ) u_l1i_flush_pending_DFFE (
      .CLK(clk),
      .EN (flush_l1i_pending_clk_en),
      .D  (flush_l1i_pending_d),
      .Q  (flush_l1i_pending_q)
  );
  DFFE #(
      .Width(1)
  ) u_l1d_flush_pending_DFFE (
      .CLK(clk),
      .EN (flush_l1d_pending_clk_en),
      .D  (flush_l1d_pending_d),
      .Q  (flush_l1d_pending_q)
  );
  DFFE #(
      .Width(1)
  ) u_l2_flush_pending_DFFE (
      .CLK(clk),
      .EN (flush_l2_pending_clk_en),
      .D  (flush_l2_pending_d),
      .Q  (flush_l2_pending_q)
  );

endmodule
