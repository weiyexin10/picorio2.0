module csr_regfile
  import riscv_pkg::*;
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
(
    input  logic [              XLEN-1:0] hart_id_i,
    input  logic [       VADDR_WIDTH-1:0] boot_address_i,
    // CSR Status
    output logic [    PRIV_LVL_WIDTH-1:0] priv_lvl_o,
    output logic                          tvm_o,
    output logic                          tw_o,
    output logic                          tsr_o,
    output logic [              XLEN-1:0] mstatus_o,
    output logic [              XLEN-1:0] satp_o,
    // Float point
    output logic [                   1:0] fs_o,
    output logic [                   4:0] fflags_o,
    output logic [                   2:0] frm_o,
    output logic [                   6:0] fprec_o,
    // CSR Execute
    input  logic                          csr_insn_vld_i,
    input  logic [       VADDR_WIDTH-1:0] csr_insn_pc_i,
    input  logic [     ROB_TAG_WIDTH-1:0] csr_insn_rob_tag_i,
    input  logic [      CSR_OP_WIDTH-1:0] csr_insn_op_i,
    input  logic [    CSR_ADDR_WIDTH-1:0] csr_insn_addr_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] csr_insn_prd_i,
    input  logic [              XLEN-1:0] csr_insn_operand0_i,

    output logic                          csr_insn_wb_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] csr_insn_wb_prd_o,
    output logic [              XLEN-1:0] csr_insn_wb_data_o,

    output logic                     csr_insn_cb_vld_o,
    output logic [ROB_TAG_WIDTH-1:0] csr_insn_cb_rob_tag_o,

    output logic                        csr_insn_re_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] csr_insn_re_rob_tag_o,
    output logic [EXCP_CAUSE_WIDTH-1:0] csr_insn_re_cause_o,
    output logic [ EXCP_TVAL_WIDTH-1:0] csr_insn_re_tval_o,

    // Commit Port
    input  logic [      RETIRE_WIDTH-1:0]                  retire_insn_vld_i,
    input  logic [      RETIRE_WIDTH-1:0][VADDR_WIDTH-1:0] retire_insn_pc_i,
    input  logic [      RETIRE_WIDTH-1:0]                  retire_insn_is_br_i,
    input  logic [      RETIRE_WIDTH-1:0]                  retire_insn_mispred_i,
    // Exception Port
    input  logic                                           retire_excp_vld_i,
    input  logic [       VADDR_WIDTH-1:0]                  retire_excp_pc_i,
    input  logic                                           retire_excp_is_rvc_i,
    input  logic [   EXCP_TVAL_WIDTH-1:0]                  retire_excp_tval_i,
    input  logic [  EXCP_CAUSE_WIDTH-1:0]                  retire_excp_cause_i,
    // Redirect Port
    output logic                                           csr_redirect_vld_o,
    output logic [       VADDR_WIDTH-1:0]                  csr_redirect_pc_o,
    output logic [       VADDR_WIDTH-1:0]                  csr_redirect_target_o,
    // Interrupt
    input  logic                                           interrupt_sw_i,
    input  logic                                           interrupt_timer_i,
    input  logic                                           interrupt_ext_i,
    // R/W pmp config
    output logic                                           pmp_cfg_vld_o,
    output logic [ PMP_CFG_TAG_WIDTH-1:0]                  pmp_cfg_tag_o,
    output logic [              XLEN-1:0]                  pmp_cfg_wr_data_o,
    input  logic [              XLEN-1:0]                  pmp_cfg_rd_data_i,
    // R/W pmp addr
    output logic                                           pmp_addr_vld_o,
    output logic [PMP_ADDR_TAG_WIDTH-1:0]                  pmp_addr_tag_o,
    output logic [              XLEN-1:0]                  pmp_addr_wr_data_o,
    input  logic [              XLEN-1:0]                  pmp_addr_rd_data_i,
    // Ack Interrupt
    output logic                                           interrupt_ack_o,
    output logic [  EXCP_CAUSE_WIDTH-1:0]                  interrupt_cause_o,

    input clk,
    input rst
);

  priv_lvl_t priv_lvl_d, priv_lvl_q;
  //Unprivileged Counter/Timers
  logic [XLEN-1:0] cycle_d, cycle_q;
  logic [XLEN-1:0] instret_d, instret_q;
  /* Machine Level CSR */
  // Machine Information Registers
  logic [XLEN-1:0] mvendorid_d, mvendorid_q;
  logic [XLEN-1:0] marchid_d, marchid_q;
  logic [XLEN-1:0] mimpid_d, mimpid_q;
  logic [XLEN-1:0] mhartid_d, mhartid_q;
  logic [XLEN-1:0] mconfigptr_d, mconfigptr_q;
  // Machine Trap Setup
  mstatus_t mstatus_d, mstatus_q;
  logic [XLEN-1:0] misa_d, misa_q;
  logic [XLEN-1:0] medeleg_d, medeleg_q;
  logic [XLEN-1:0] mideleg_d, mideleg_q;
  logic    mie_we;
  csr_ie_t mie_d, mie_q, mie_mask;
  logic [XLEN-1:0] mtvec_d, mtvec_q;
  logic [XLEN-1:0] mcounteren_d, mcounteren_q;
  // Machine Trap Handling
  logic [XLEN-1:0] mscratch_d, mscratch_q;
  logic [XLEN-1:0] mepc_d, mepc_q;
  logic [XLEN-1:0] mcause_d, mcause_q;
  logic [XLEN-1:0] mtval_d, mtval_q;
  logic    mip_we;
  csr_ip_t mip_d, mip_q, mip_mask;
  logic [XLEN-1:0] mtinst_d, mtinst_q;
  logic [XLEN-1:0] mtval2_d, mtval2_q;
  // Machine Configuration
  logic [XLEN-1:0] menvcfg_d, menvcfg_q;
  logic [XLEN-1:0] mseccfg_d, mseccfg_q;
  /* Supervisor Level CSR  */
  // Supervisor Trap Setup
  csr_ie_t sie, sie_mask;
  logic [XLEN-1:0] stvec_d, stvec_q;
  logic [XLEN-1:0] scounteren_d, scounteren_q;
  // Supervisor Configuration
  logic [XLEN-1:0] senvcfg_d, senvcfg_q;
  // Supervisor Trap Handling
  logic [XLEN-1:0] sscratch_d, sscratch_q;
  logic [XLEN-1:0] sepc_d, sepc_q;
  logic [XLEN-1:0] scause_d, scause_q;
  logic [XLEN-1:0] stval_d, stval_q;
  csr_ip_t sip, sip_mask;
  // Supervisor Protection and Translation
  satp_t satp_d, satp_q;

// interrupt
  csr_ip_t                          m_masked_int;
  csr_ip_t                          s_masked_int;
  csr_ip_t                          m_masked_int_trapable;
  csr_ip_t                          s_masked_int_trapable;
  csr_ip_t                          masked_int_trapable;
  csr_ip_t                          masked_int;

  logic                             m_int_en;
  logic                             s_int_en;
  logic[VADDR_WIDTH-1:0]            m_int_target_pc;
  logic[VADDR_WIDTH-1:0]            s_int_target_pc;
  logic                             int_pending_d, int_pending_q;
  logic                             int_trapable_d, int_trapable_q;
  logic [  EXCP_CAUSE_WIDTH-1:0]    int_trap_cause_d, int_trap_cause_q;

  // CSR Operation Decode
  logic [11:0] csr_op_addr;

  logic rd_csr_en, wr_csr_en;
  logic [XLEN-1:0] csr_rd_data;
  logic [XLEN-1:0] csr_wr_data;
  logic is_mret, is_sret, is_fencei, is_sfence_vma;

  logic      [$clog2(RETIRE_WIDTH+1)-1:0] retire_cnt;

  logic                                   wr_side_effect;
  logic                                   sel_unknown_csr;

  logic                                   sel_pmp_cfg;
  logic                                   sel_pmp_addr;

  logic                                   sel_mcycle;
  logic                                   sel_minstret;
  logic                                   sel_mconfigptr;
  logic                                   sel_mstatus;
  logic                                   sel_misa;
  logic                                   sel_medeleg;
  logic                                   sel_mideleg;
  logic                                   sel_mie;
  logic                                   sel_mtvec;
  logic                                   sel_mcounteren;
  logic                                   sel_mscratch;
  logic                                   sel_mepc;
  logic                                   sel_mcause;
  logic                                   sel_mtval;
  logic                                   sel_mip;
  logic                                   sel_mtinst;
  logic                                   sel_mtval2;
  logic                                   sel_menvcfg;
  logic                                   sel_mseccfg;
  logic                                   sel_mhpmcounter;
  logic                                   sel_mcountinhibit;
  logic                                   sel_mhpmevent;
  logic                                   sel_sie;
  logic                                   sel_stvec;
  logic                                   sel_scounteren;
  logic                                   sel_senvcfg;
  logic                                   sel_sscratch;
  logic                                   sel_sepc;
  logic                                   sel_scause;
  logic                                   sel_stval;
  logic                                   sel_sip;
  logic                                   sel_satp;

  /* Trap Handle */
  priv_lvl_t                              trap_to_priv_lvl;
  logic                                   retire_excp_vld;
  logic      [           VADDR_WIDTH-1:0] retire_excp_pc;
  logic      [       EXCP_TVAL_WIDTH-1:0] retire_excp_tval;
  logic      [      EXCP_CAUSE_WIDTH-1:0] retire_excp_cause;
  logic      [                  XLEN-1:0] m_trap_target;
  logic      [                  XLEN-1:0] s_trap_target;
  logic      [                  XLEN-1:0] trap_target;
  logic                                   order_failure;
  // TODO 
  // assign interrupt_ack_o = 1'b0;
  // assign interrupt_cause_o = '0;

  // Output
  assign priv_lvl_o = priv_lvl_q;
  assign tvm_o = mstatus_q.tvm;
  assign tw_o = mstatus_q.tw;
  assign tsr_o = mstatus_q.tsr;
  assign mstatus_o = mstatus_q;
  assign satp_o = satp_q;

  assign csr_insn_wb_vld_o = rd_csr_en & ~sel_unknown_csr;
  assign csr_insn_wb_prd_o = csr_insn_prd_i;
  assign csr_insn_wb_data_o = csr_rd_data;

  assign csr_insn_cb_vld_o = csr_insn_vld_i & ~sel_unknown_csr;
  assign csr_insn_cb_rob_tag_o = csr_insn_rob_tag_i;

  assign csr_insn_re_vld_o = sel_unknown_csr;
  assign csr_insn_re_rob_tag_o = csr_insn_rob_tag_i;
  assign csr_insn_re_cause_o = ILLEGAL_INSTR;
  assign csr_insn_re_tval_o = {EXCP_TVAL_WIDTH{1'b0}};

  always_comb begin
    case ({mtvec_q[1:0],retire_excp_cause[XLEN-1]})
      {2'b01,1'b1} : m_trap_target = {mtvec_q[XLEN-1:2],2'b0} + {retire_excp_cause[3:0],2'b0};
      default: m_trap_target = {mtvec_q[XLEN-1:2],2'b0};
    endcase
  end

  always_comb begin
    case ({stvec_q[1:0],retire_excp_cause[XLEN-1]})
      {2'b01,1'b1} : s_trap_target = {stvec_q[XLEN-1:2],2'b0} + {retire_excp_cause[3:0],2'b0};
      default: s_trap_target = {stvec_q[XLEN-1:2],2'b0};
    endcase
  end
  assign trap_target = ({XLEN{(trap_to_priv_lvl == PRIV_S)}} & s_trap_target) | ({XLEN{(trap_to_priv_lvl == PRIV_M)}} & m_trap_target);
  assign csr_redirect_vld_o = (wr_side_effect & wr_csr_en) | retire_excp_vld_i | is_mret | is_sret | order_failure;
  assign csr_redirect_target_o = ({XLEN{is_sret}} & {sepc_q[XLEN-1:1],1'b0}) |
                                   ({XLEN{is_mret}} & {mepc_q[XLEN-1:1],1'b0}) |
                                   ({XLEN{(wr_side_effect & wr_csr_en)}} & (csr_insn_pc_i + 3'd4)) |
                                   ({XLEN{retire_excp_vld}} & trap_target) |
                                   ({XLEN{order_failure}} & retire_excp_pc) 
                                   ;

  assign csr_redirect_pc_o = retire_excp_vld_i ? retire_excp_pc_i : csr_insn_pc_i;

  assign order_failure = retire_excp_vld_i & (retire_excp_cause_i == ORDER_FAILURE);
  assign retire_excp_vld = retire_excp_vld_i & (retire_excp_cause_i != ORDER_FAILURE);
  assign retire_excp_pc = retire_excp_pc_i;
  assign retire_excp_tval = retire_excp_tval_i;
  assign retire_excp_cause = retire_excp_cause_i;

  assign csr_op_addr = csr_insn_addr_i;

  always_comb begin : csr_op_decode
    rd_csr_en   = 1'b0;
    wr_csr_en   = 1'b0;
    is_mret     = 1'b0;
    is_sret     = 1'b0;
    csr_wr_data = {XLEN{1'b0}};
    if (csr_insn_vld_i) begin
      case (csr_insn_op_i)
        CSR_CSRRW: begin
          rd_csr_en   = 1'b1;
          wr_csr_en   = 1'b1;
          csr_wr_data = csr_insn_operand0_i;
        end
        CSR_CSRRS: begin
          rd_csr_en   = 1'b1;
          wr_csr_en   = 1'b1;
          csr_wr_data = csr_insn_operand0_i | csr_rd_data;
        end
        CSR_CSRRC: begin
          rd_csr_en   = 1'b1;
          wr_csr_en   = 1'b1;
          csr_wr_data = ~csr_insn_operand0_i & csr_rd_data;
        end
        CSR_CSRR: begin
          rd_csr_en = 1'b1;
        end
        CSR_MRET: begin
          is_mret = 1'b1;
        end
        CSR_SRET: begin
          is_sret = 1'b1;
        end
        default: ;
      endcase
    end
  end


  assign pmp_cfg_vld_o = sel_pmp_cfg & wr_csr_en;
  assign pmp_cfg_tag_o = csr_op_addr[3:1];
  assign pmp_cfg_wr_data_o = csr_wr_data;

  assign pmp_addr_vld_o = sel_pmp_addr & wr_csr_en;
  assign pmp_addr_tag_o = csr_op_addr[5:0];
  assign pmp_addr_wr_data_o = csr_wr_data;

  always_comb begin : read_csr

    wr_side_effect = 1'b0;

    sel_mcycle = 1'b0;
    sel_minstret = 1'b0;

    sel_pmp_cfg = 1'b0;
    sel_pmp_addr = 1'b0;
    sel_unknown_csr = 1'b0;
    sel_mconfigptr = 1'b0;
    sel_mstatus = 1'b0;
    sel_misa = 1'b0;
    sel_medeleg = 1'b0;
    sel_mideleg = 1'b0;
    sel_mie = 1'b0;
    sel_mtvec = 1'b0;
    sel_mcounteren = 1'b0;
    sel_mscratch = 1'b0;
    sel_mepc = 1'b0;
    sel_mcause = 1'b0;
    sel_mtval = 1'b0;
    sel_mip = 1'b0;
    sel_mtinst = 1'b0;
    sel_mtval2 = 1'b0;
    sel_menvcfg = 1'b0;
    sel_mseccfg = 1'b0;
    sel_mhpmcounter = 1'b0;
    sel_mcountinhibit = 1'b0;
    sel_mhpmevent = 1'b0;
    sel_sie = 1'b0;
    sel_stvec = 1'b0;
    sel_scounteren = 1'b0;
    sel_senvcfg = 1'b0;
    sel_sscratch = 1'b0;
    sel_sepc = 1'b0;
    sel_scause = 1'b0;
    sel_stval = 1'b0;
    sel_sip = 1'b0;
    sel_satp = 1'b0;

    csr_rd_data = {XLEN{1'b0}};
    if (rd_csr_en) begin
      case (csr_op_addr)
        // Unprivileged Count/Timers
        CSR_CYCLE: csr_rd_data = cycle_q;
        CSR_TIME: ;  // TODO
        CSR_INSTRET: csr_rd_data = instret_q;
        CSR_HPMCOUNTER3,
                CSR_HPMCOUNTER4,
                CSR_HPMCOUNTER5,
                CSR_HPMCOUNTER6,
                CSR_HPMCOUNTER7,
                CSR_HPMCOUNTER8,
                CSR_HPMCOUNTER9,
                CSR_HPMCOUNTER10,
                CSR_HPMCOUNTER11,
                CSR_HPMCOUNTER12,
                CSR_HPMCOUNTER13,
                CSR_HPMCOUNTER14,
                CSR_HPMCOUNTER15,
                CSR_HPMCOUNTER16,
                CSR_HPMCOUNTER17,
                CSR_HPMCOUNTER18,
                CSR_HPMCOUNTER19,
                CSR_HPMCOUNTER20,
                CSR_HPMCOUNTER21,
                CSR_HPMCOUNTER22,
                CSR_HPMCOUNTER23,
                CSR_HPMCOUNTER24,
                CSR_HPMCOUNTER25,
                CSR_HPMCOUNTER26,
                CSR_HPMCOUNTER27,
                CSR_HPMCOUNTER28,
                CSR_HPMCOUNTER29,
                CSR_HPMCOUNTER30,
                CSR_HPMCOUNTER31 :
        ;  //TODO
        // Machine Information Registers
        CSR_MVENDORID: csr_rd_data = mvendorid_q;
        CSR_MARCHID: csr_rd_data = marchid_q;
        CSR_MIMPID: csr_rd_data = mimpid_q;
        CSR_MHARTID: csr_rd_data = mhartid_q;
        CSR_MCONFIGPTR: csr_rd_data = mconfigptr_q;
        // Machine Trap Setup
        CSR_MSTATUS: begin
          csr_rd_data = mstatus_q;
          csr_rd_data[33:32] = 2'b10;
          sel_mstatus = 1'b1;
          wr_side_effect = 1'b1;
          wr_side_effect = wr_csr_en;
        end
        CSR_MISA: begin
          csr_rd_data = misa_q;
          sel_misa = 1'b1;
        end
        CSR_MEDELEG: begin
          csr_rd_data = medeleg_q;
          sel_medeleg = 1'b1;
        end
        CSR_MIDELEG: begin
          csr_rd_data = mideleg_q;
          sel_mideleg = 1'b1;
        end
        CSR_MIE: begin
          csr_rd_data = mie_q;
          sel_mie = 1'b1;
        end
        CSR_MTVEC: begin
          csr_rd_data = mtvec_q;
          sel_mtvec   = 1'b1;
        end
        CSR_MCOUNTEREN: begin
          csr_rd_data = mcounteren_q;
          sel_mcounteren = 1'b1;
        end
        // Machine Trap Handling
        CSR_MSCRATCH: begin
          csr_rd_data  = mscratch_q;
          sel_mscratch = 1'b1;
        end
        CSR_MEPC: begin
          csr_rd_data = mepc_q;
          sel_mepc = 1'b1;
        end
        CSR_MCAUSE: begin
          csr_rd_data = mcause_q;
          sel_mcause  = 1'b1;
        end
        CSR_MTVAL: begin
          csr_rd_data = mtval_q;
          sel_mtval   = 1'b1;
        end
        CSR_MIP: begin
          csr_rd_data = mip_q;
          sel_mip = 1'b1;
        end
        CSR_MTINST: begin
          csr_rd_data = CSR_MTINST;
          sel_mtinst  = 1'b1;
        end
        CSR_MTVAL2: begin
          csr_rd_data = CSR_MTVAL2;
          sel_mtval2  = 1'b1;
        end
        // Machine Configuration
        CSR_MENVCFG: begin
          csr_rd_data = menvcfg_q;
          sel_menvcfg = 1'b1;
        end
        CSR_MSECCFG: begin
          csr_rd_data = mseccfg_q;
          sel_mseccfg = 1'b1;
        end
        // Machine Memory Protection
        CSR_PMPCFG0,
                CSR_PMPCFG2,
                CSR_PMPCFG4,
                CSR_PMPCFG6,
                CSR_PMPCFG8,
                CSR_PMPCFG10,
                CSR_PMPCFG12,
                CSR_PMPCFG14 : begin
          sel_pmp_cfg = 1'b1;
          csr_rd_data = pmp_cfg_rd_data_i;
          wr_side_effect = wr_csr_en;
        end
        CSR_PMPADDR0,
                CSR_PMPADDR1,
                CSR_PMPADDR2,
                CSR_PMPADDR3,
                CSR_PMPADDR4,
                CSR_PMPADDR5,
                CSR_PMPADDR6,
                CSR_PMPADDR7,
                CSR_PMPADDR8,
                CSR_PMPADDR9,
                CSR_PMPADDR10,
                CSR_PMPADDR11,
                CSR_PMPADDR12,
                CSR_PMPADDR13,
                CSR_PMPADDR14,
                CSR_PMPADDR15,
                CSR_PMPADDR16,
                CSR_PMPADDR17,
                CSR_PMPADDR18,
                CSR_PMPADDR19,
                CSR_PMPADDR20,
                CSR_PMPADDR21,
                CSR_PMPADDR22,
                CSR_PMPADDR23,
                CSR_PMPADDR24,
                CSR_PMPADDR25,
                CSR_PMPADDR26,
                CSR_PMPADDR27,
                CSR_PMPADDR28,
                CSR_PMPADDR29,
                CSR_PMPADDR30,
                CSR_PMPADDR31,
                CSR_PMPADDR32,
                CSR_PMPADDR33,
                CSR_PMPADDR34,
                CSR_PMPADDR35,
                CSR_PMPADDR36,
                CSR_PMPADDR37,
                CSR_PMPADDR38,
                CSR_PMPADDR39,
                CSR_PMPADDR40,
                CSR_PMPADDR41,
                CSR_PMPADDR42,
                CSR_PMPADDR43,
                CSR_PMPADDR44,
                CSR_PMPADDR45,
                CSR_PMPADDR46,
                CSR_PMPADDR47,
                CSR_PMPADDR48,
                CSR_PMPADDR49,
                CSR_PMPADDR50,
                CSR_PMPADDR51,
                CSR_PMPADDR52,
                CSR_PMPADDR53,
                CSR_PMPADDR54,
                CSR_PMPADDR55,
                CSR_PMPADDR56,
                CSR_PMPADDR57,
                CSR_PMPADDR58,
                CSR_PMPADDR59,
                CSR_PMPADDR60,
                CSR_PMPADDR61,
                CSR_PMPADDR62,
                CSR_PMPADDR63 : begin
          sel_pmp_addr = 1'b1;
          csr_rd_data = pmp_addr_rd_data_i;
          wr_side_effect = wr_csr_en;
        end
        // Machine Counter/Timers
        CSR_MCYCLE: begin
          csr_rd_data = cycle_q;
          sel_mcycle  = 1'b1;
        end
        CSR_MINSTRET: begin
          csr_rd_data  = instret_q;
          sel_minstret = 1'b1;
        end
        CSR_MHPMCOUNTER3,
                CSR_MHPMCOUNTER4,
                CSR_MHPMCOUNTER5,
                CSR_MHPMCOUNTER6,
                CSR_MHPMCOUNTER7,
                CSR_MHPMCOUNTER8,
                CSR_MHPMCOUNTER9,
                CSR_MHPMCOUNTER10,
                CSR_MHPMCOUNTER11,
                CSR_MHPMCOUNTER12,
                CSR_MHPMCOUNTER13,
                CSR_MHPMCOUNTER14,
                CSR_MHPMCOUNTER15,
                CSR_MHPMCOUNTER16,
                CSR_MHPMCOUNTER17,
                CSR_MHPMCOUNTER18,
                CSR_MHPMCOUNTER19,
                CSR_MHPMCOUNTER20,
                CSR_MHPMCOUNTER21,
                CSR_MHPMCOUNTER22,
                CSR_MHPMCOUNTER23,
                CSR_MHPMCOUNTER24,
                CSR_MHPMCOUNTER25,
                CSR_MHPMCOUNTER26,
                CSR_MHPMCOUNTER27,
                CSR_MHPMCOUNTER28,
                CSR_MHPMCOUNTER29,
                CSR_MHPMCOUNTER30,
                CSR_MHPMCOUNTER31 :
        ;  //TODO
        // Machine Counter Setup
        CSR_MCOUNTINHIBIT: ;  //TODO
        CSR_MHPMEVENT3,
                CSR_MHPMEVENT4,
                CSR_MHPMEVENT5,
                CSR_MHPMEVENT6,
                CSR_MHPMEVENT7,
                CSR_MHPMEVENT8,
                CSR_MHPMEVENT9,
                CSR_MHPMEVENT10,
                CSR_MHPMEVENT11,
                CSR_MHPMEVENT12,
                CSR_MHPMEVENT13,
                CSR_MHPMEVENT14,
                CSR_MHPMEVENT15,
                CSR_MHPMEVENT16,
                CSR_MHPMEVENT17,
                CSR_MHPMEVENT18,
                CSR_MHPMEVENT19,
                CSR_MHPMEVENT20,
                CSR_MHPMEVENT21,
                CSR_MHPMEVENT22,
                CSR_MHPMEVENT23,
                CSR_MHPMEVENT24,
                CSR_MHPMEVENT25,
                CSR_MHPMEVENT26,
                CSR_MHPMEVENT27,
                CSR_MHPMEVENT28,
                CSR_MHPMEVENT29,
                CSR_MHPMEVENT30,
                CSR_MHPMEVENT31 :
        ;  //TODO
        // Supervisor Trap Setup
        CSR_SSTATUS: begin
          csr_rd_data = mstatus_q;
          csr_rd_data[33:32] = 2'b10;
          sel_mstatus = 1'b1;
          wr_side_effect = wr_csr_en;
        end
        CSR_SIE: begin
          csr_rd_data = sie;
          sel_sie = 1'b1;
        end
        CSR_STVEC: begin
          csr_rd_data = stvec_q;
          sel_stvec   = 1'b1;
        end
        CSR_SCOUNTEREN: begin
          csr_rd_data = scounteren_q;
          sel_scounteren = 1'b1;
        end
        // Supervisor Configuration
        CSR_SENVCFG: begin
          csr_rd_data = senvcfg_q;
          sel_senvcfg = 1'b1;
        end
        CSR_SSCRATCH: begin
          csr_rd_data  = sscratch_q;
          sel_sscratch = 1'b1;
        end
        CSR_SEPC: begin
          csr_rd_data = sepc_q;
          sel_sepc = 1'b1;
        end
        CSR_SCAUSE: begin
          csr_rd_data = scause_q;
          sel_scause  = 1'b1;
        end
        CSR_STVAL: begin
          csr_rd_data = stval_q;
          sel_stval   = 1'b1;
        end
        CSR_SIP: begin
          csr_rd_data = sip;
          sel_sip = 1'b1;
        end
        // Supervisor Protection and Translation
        CSR_SATP: begin
          csr_rd_data = satp_q;
          sel_satp = 1'b1;
          wr_side_effect = wr_csr_en;
        end
        default: sel_unknown_csr = 1'b1;
      endcase
    end
  end

  assign cycle_d = (wr_csr_en & sel_mcycle) ? csr_wr_data : (cycle_q + 1'b1);
  assign instret_d       = (wr_csr_en & sel_minstret) ? csr_wr_data : (instret_q + retire_cnt + (csr_insn_vld_i & ~sel_unknown_csr));
  assign mconfigptr_d = csr_wr_data;
  assign misa_d = csr_wr_data;
  assign medeleg_d = csr_wr_data;
  assign mideleg_d = csr_wr_data;
  assign mtvec_d = csr_wr_data;
  assign mcounteren_d = csr_wr_data;
  assign mscratch_d = csr_wr_data;
  assign mtinst_d = csr_wr_data;
  assign mtval2_d = csr_wr_data;
  assign menvcfg_d = csr_wr_data;
  assign mseccfg_d = csr_wr_data;
  assign scounteren_d = csr_wr_data;
  assign senvcfg_d = csr_wr_data;
  assign sscratch_d = csr_wr_data;
  assign satp_d = csr_wr_data;
  assign stvec_d = csr_wr_data;

// interrupt ip
  always_comb begin
      mip_mask        = '0;
      mip_mask.SSIP   = '1;
      mip_mask.STIP   = '1;
      mip_mask.SEIP   = '1;
  end

  always_comb begin
      sip_mask        = '0;
      sip_mask.SEIP   = '1;
      sip_mask.STIP   = '1;
      sip_mask.SSIP   = '1;
  end

  always_comb begin : interrupt_handling
    mip_d = mip_q;
    if(wr_csr_en & sel_mip)begin
        mip_d = csr_ip_t'(csr_wr_data) & mip_mask;
    end
    else if(wr_csr_en & sel_sip)begin
        mip_d = mip_q & ~sip_mask | sip_mask & csr_wr_data;
    end
    mip_d.MEIP   = interrupt_ext_i;
    mip_d.MTIP   = interrupt_timer_i;
    mip_d.MSIP   = interrupt_sw_i;
  end

  assign mip_we = wr_csr_en & sel_mip |
                wr_csr_en & sel_sip |
                (mip_q.MEIP ^ interrupt_ext_i)   |
                (mip_q.MTIP ^ interrupt_timer_i) |
                (mip_q.MSIP ^ interrupt_sw_i) ;

  always_ff @(posedge clk) begin
    if (rst) begin
      mip_q <= 0;
    end
    else if (mip_we) begin
      mip_q <= mip_d;
    end
  end

  assign sip = mip_q & sip_mask;

// interrupt ie
  always_comb begin
      sie_mask        = '0;
      sie_mask.SEIE   = '1;
      sie_mask.STIE   = '1;
      sie_mask.SSIE   = '1;
  end

  always_comb begin
      mie_mask        = '0;
      mie_mask.SSIE   = '1;
      mie_mask.MSIE   = '1;
      mie_mask.STIE   = '1;
      mie_mask.MTIE   = '1;
      mie_mask.SEIE   = '1;
      mie_mask.MEIE   = '1;
  end

  always_comb begin
    mie_d = mie_q;
    if(wr_csr_en & sel_mie)begin
        mie_d = csr_wr_data & mie_mask;
    end
    else if(wr_csr_en & sel_sie)begin
        mie_d = mie_q & ~sie_mask | csr_wr_data & sie_mask;
    end
  end

  assign mie_we = wr_csr_en & (sel_mie | sel_sie);

  always_ff @(posedge clk) begin
    if (rst) begin
      mie_q <= 0;
    end
    else if (mie_we) begin
      mie_q <= mie_d;
    end
  end
  assign sie  = mie_q & sie_mask;

//interrupt
  assign m_masked_int           = mip_q & mie_q & ~mideleg_q;
  assign s_masked_int           = mip_q & mie_q & mideleg_q;
  assign m_int_en             = (priv_lvl_q==PRIV_M) & mstatus_q.mie | (priv_lvl_q==PRIV_S) | (priv_lvl_q==PRIV_U);
  assign s_int_en             = (priv_lvl_q==PRIV_S) & mstatus_q.sie | (priv_lvl_q==PRIV_U);
  assign m_masked_int_trapable  = m_masked_int & {$bits(csr_ip_t){m_int_en}};
  assign s_masked_int_trapable  = s_masked_int & {$bits(csr_ip_t){s_int_en}};
  assign masked_int             = (|m_masked_int) ? m_masked_int : s_masked_int;
  assign masked_int_trapable    = (|m_masked_int_trapable) ? m_masked_int_trapable : s_masked_int_trapable;
  assign int_pending_d        = masked_int.MEIP | masked_int.MTIP | masked_int.MSIP |
                                masked_int.SEIP | masked_int.STIP | masked_int.SSIP;
  assign int_trapable_d       = (|masked_int_trapable);
  assign int_trap_cause_d     = masked_int_trapable.MEIP ? IRQ_M_EXT : 
                                masked_int_trapable.MSIP ? IRQ_M_SOFT :
                                masked_int_trapable.MTIP ? IRQ_M_TIMER:
                                masked_int_trapable.SEIP ? IRQ_S_EXT :
                                masked_int_trapable.SSIP ? IRQ_S_SOFT : 
                                                          IRQ_S_TIMER;
  // assign trap_int_level       = mideleg[6'(int_trap_cause)] ? PRIV_S : PRIV_M;
  // assign m_int_target_pc      = (mtvec_q[1:0] == CSR_TVEC_DIRECT) ? RRV64_VPC_W'({mtvec_q[XLEN-1:2],2'b0}) : RRV64_VPC_W'({mtvec_q[XLEN-1:2],2'b0} + (trap_int_cause <<2));
  // assign s_int_target_pc      = (mtvec_q[1:0] == CSR_TVEC_DIRECT) ? RRV64_VPC_W'({stvec_q[XLEN-1:2],2'b0}) : RRV64_VPC_W'({stvec_q[XLEN-1:2],2'b0} + (trap_int_cause <<2));
  // assign csr_int_target_pc    = (trap_int_level == PRIV_S) ? s_int_target_pc : m_int_target_pc;

  always_ff @(posedge clk) begin
    if (rst) begin
      int_pending_q <= 0;
      int_trapable_q <= 0;
      int_trap_cause_q <= 0;
    end
    else begin
      int_pending_q <= int_pending_d;
      int_trapable_q <= int_trapable_d;
      int_trap_cause_q <= int_trap_cause_d;
    end
  end

  assign interrupt_ack_o = int_trapable_q;
  assign interrupt_cause_o = int_trap_cause_q;

// exception
  always_comb begin : exception_handling
    priv_lvl_d = priv_lvl_q;
    mstatus_d  = mstatus_q;
    mepc_d     = mepc_q;
    mcause_d   = mcause_q;
    mtval_d    = mtval_q;
    sepc_d     = sepc_q;
    scause_d   = scause_q;
    stval_d    = stval_q;
    if (wr_csr_en & ~sel_unknown_csr) begin
      if (sel_mstatus) mstatus_d = csr_wr_data;
      if (sel_mepc) mepc_d = csr_wr_data;
      if (sel_mcause) mcause_d = csr_wr_data;
      if (sel_mtval) mtval_d = csr_wr_data;
      if (sel_sepc) sepc_d = csr_wr_data;
      if (sel_scause) scause_d = csr_wr_data;
      if (sel_stval) stval_d = csr_wr_data;
    end
    trap_to_priv_lvl = PRIV_M;
    if (retire_excp_vld) begin
      if ((retire_excp_cause && mideleg_q[retire_excp_cause[$clog2(
              XLEN
          )-1:0]]) | (~retire_excp_cause && medeleg_q[retire_excp_cause[$clog2(
              XLEN
          )-1:0]])) begin
        trap_to_priv_lvl = priv_lvl_q == PRIV_M ? PRIV_M : PRIV_S;
      end
      if (trap_to_priv_lvl == PRIV_S) begin
        mstatus_d.sie = 1'b0;
        mstatus_d.spie = mstatus_q.sie;
        mstatus_d.spp = priv_lvl_q[0];
        scause_d = retire_excp_cause;
        sepc_d = {{XLEN - VADDR_WIDTH{retire_excp_pc[VADDR_WIDTH-1]}}, retire_excp_pc};
        stval_d = retire_excp_tval;
      end else begin
        mstatus_d.mie = 1'b0;
        mstatus_d.mpie = mstatus_q.mie;
        mstatus_d.mpp = priv_lvl_q;
        mcause_d = retire_excp_cause;
        mepc_d = {{XLEN - VADDR_WIDTH{retire_excp_pc[VADDR_WIDTH-1]}}, retire_excp_pc};
        mtval_d = retire_excp_tval;
      end
      priv_lvl_d = trap_to_priv_lvl;
    end
    if (is_mret) begin
      mstatus_d.mie = mstatus_q.mpie;
      priv_lvl_d = mstatus_q.mpp;
      mstatus_d.mpp = PRIV_U;
      mstatus_d.mpie = 1'b1;
    end
    if (is_sret) begin
      mstatus_d.mie = mstatus_q.spie;
      priv_lvl_d = priv_lvl_t'(mstatus_q.spp);
      mstatus_d.spp = PRIV_U;
      mstatus_d.spie = 1'b1;
    end
  end

  always_ff @(posedge clk) begin : status_dff
    if (rst) begin
      mstatus_q <= {XLEN{1'b0}};
    end else begin
      mstatus_q <= mstatus_d;
    end
  end

  always_ff @(posedge clk) begin : excp_handle_dff
    if (rst) begin
      mepc_q   <= {XLEN{1'b0}};
      mcause_q <= {XLEN{1'b0}};
      mtval_q  <= {XLEN{1'b0}};
      sepc_q   <= {XLEN{1'b0}};
      scause_q <= {XLEN{1'b0}};
      stval_q  <= {XLEN{1'b0}};
    end else begin
      mepc_q   <= mepc_d;
      mcause_q <= mcause_d;
      mtval_q  <= mtval_d;
      sepc_q   <= sepc_d;
      scause_q <= scause_d;
      stval_q  <= stval_d;
    end
  end

  always_ff @(posedge clk) begin : csr_rf_dff
    if (rst) begin
      priv_lvl_q   <= PRIV_M;
      //Unprivileged Counter/Timers
      cycle_q      <= {XLEN{1'b0}};
      instret_q    <= {XLEN{1'b0}};
      /* Machine Level CSR */
      // Machine Information Registers
      mvendorid_q  <= {XLEN{1'b0}};
      marchid_q    <= {XLEN{1'b0}};
      mimpid_q     <= {XLEN{1'b0}};
      mhartid_q    <= hart_id_i[XLEN-1:0];
      mconfigptr_q <= {XLEN{1'b0}};
      // Machine Trap Setup
      misa_q       <= {2'b10, 62'b0};
      mtvec_q      <= {XLEN{1'b0}};
      medeleg_q    <= {XLEN{1'b0}};
      mideleg_q    <= {XLEN{1'b0}};
      mcounteren_q <= {XLEN{1'b0}};
      // Machine Trap Handling
      mscratch_q   <= {XLEN{1'b0}};
      mtinst_q     <= {XLEN{1'b0}};
      mtval2_q     <= {XLEN{1'b0}};
      // Machine Configuration
      menvcfg_q    <= {XLEN{1'b0}};
      mseccfg_q    <= {XLEN{1'b0}};
      /* Supervisor Level CSR  */
      // Supervisor Trap Setup
      stvec_q      <= {XLEN{1'b0}};
      scounteren_q <= {XLEN{1'b0}};
      // Supervisor Configuration
      senvcfg_q    <= {XLEN{1'b0}};
      // Supervisor Trap Handling
      sscratch_q   <= {XLEN{1'b0}};
      // Supervisor Protection and Translation
      satp_q       <= {XLEN{1'b0}};
    end else begin
      priv_lvl_q <= priv_lvl_d;
      cycle_q    <= cycle_d;
      instret_q  <= instret_d;
      if (sel_misa & wr_csr_en) misa_q <= misa_d;
      if (sel_medeleg & wr_csr_en) medeleg_q <= medeleg_d;
      if (sel_mideleg & wr_csr_en) mideleg_q <= mideleg_d;
      if (sel_mtvec & wr_csr_en) mtvec_q <= mtvec_d;
      if (sel_mcounteren & wr_csr_en) mcounteren_q <= mcounteren_d;
      if (sel_mscratch & wr_csr_en) mscratch_q <= mscratch_d;
      if (sel_mtinst & wr_csr_en) mtinst_q <= mtinst_d;
      if (sel_mtval2 & wr_csr_en) mtval2_q <= mtval2_d;
      if (sel_menvcfg & wr_csr_en) menvcfg_q <= menvcfg_d;
      if (sel_mseccfg & wr_csr_en) mseccfg_q <= mseccfg_d;
      if (sel_stvec & wr_csr_en) stvec_q <= stvec_d;
      if (sel_scounteren & wr_csr_en) scounteren_q <= scounteren_d;
      if (sel_senvcfg & wr_csr_en) senvcfg_q <= senvcfg_d;
      if (sel_sscratch & wr_csr_en) sscratch_q <= sscratch_d;
      if (sel_satp & wr_csr_en) satp_q <= satp_d;
    end
  end

  one_counter #(
      .DATA_WIDTH(RETIRE_WIDTH)
  ) u_retire_insn_counter (
      .data_i(retire_insn_vld_i),
      .cnt_o (retire_cnt)
  );


endmodule
