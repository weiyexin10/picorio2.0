module br_ctrl_pipe
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned IQ_ENTRY_COUNT = 4
) (
    // Disp -> IQ(BRU)
    input  logic                          disp_br_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] disp_br_iq_rob_tag_i,
    input  logic [     BRQ_TAG_WIDTH-1:0] disp_br_iq_brq_tag_i,
    input  logic [      BRU_OP_WIDTH-1:0] disp_br_iq_opcode_i,
    input  logic                          disp_br_iq_rs1_rdy_i,
    input  logic                          disp_br_iq_rs2_rdy_i,
    input  logic                          disp_br_iq_use_rs1_i,
    input  logic                          disp_br_iq_use_rs2_i,
    input  logic                          disp_br_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_br_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_br_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_br_iq_phy_rd_i,
    input  logic [                  31:0] disp_br_iq_imm_i,
    output logic                          disp_br_iq_rdy_o,

    // IQ -> Issue Ctrl : Issue Request
    output logic                               issue_rd_prf_vld_o,
    output logic [1:0]                         issue_rd_prf_mask_o,
    output logic [1:0][INT_PREG_TAG_WIDTH-1:0] issue_rd_prf_tag_o,
    input  logic                               issue_rd_prf_rdy_i,

    // IQ -> ROB : Read Pc
    output logic [ROB_TAG_WIDTH-1:0] rob_tag_o,
    output logic [BRQ_TAG_WIDTH-1:0] brq_tag_o,
    input logic is_rvc_i,
    input logic [VADDR_WIDTH-1:0] inst_pc_i,
    input logic [BTQ_TAG_WIDTH-1:0] btq_tag_i,
    input logic pred_taken_i,
    input logic [VADDR_WIDTH-1:0] pred_target_i,

    // PRF -> IQ : Read Operand
    input logic [1:0][XLEN-1:0] prf_data_i,

    // FU -> PRF : Writeback
    output logic wr_prf_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] wr_prf_tag_o,
    output logic [XLEN-1:0] wr_prf_data_o,
    input logic wr_prf_rdy_i,

    // FU -> ROB : Clear Busy
    output logic wb_rob_vld_o,
    output logic [ROB_TAG_WIDTH-1:0] wb_rob_tag_o,

    // FU -> BoardCast : Redirect
    output logic bru_resolve_vld_o,
    output logic [VADDR_WIDTH-1:0] bru_resolve_pc_o,
    output logic [ROB_TAG_WIDTH-1:0] bru_resolve_rob_tag_o,
    output logic [BRQ_TAG_WIDTH-1:0] bru_resolve_brq_tag_o,
    output logic [BTQ_TAG_WIDTH-1:0] bru_resolve_btq_tag_o,
    output logic bru_resolve_taken_o,
    output logic bru_resolve_mispred_o,
    output logic [VADDR_WIDTH-1:0] bru_resolve_target_o,

    // IEW -> Forwarding : Wakeup
    output logic forward_wakeup_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] forward_wakeup_prf_tag_o,
    // IEW -> Forwarding : Forward Data 
    output logic forward_data_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] forward_data_prf_tag_o,
    output logic [XLEN-1:0] forward_data_o,

    // Fowarding Path -> IQ : Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0]                         cdb_wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_wakeup_ptag_i,

    // Fowarding Path -> IQ : Forward Ctrl
    input logic [INT_CDB_BUS_WIDTH-1:0]                         cdb_forward_ctrl_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ctrl_ptag_i,

    // Fowarding Path -> IQ : Forward Data
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0]                         cdb_forward_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ptag_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][              XLEN-1:0] cdb_forward_data_i,


    input logic flush_i,
    input clk,
    input rst
);

    logic i1_clk_en, i1_vld, i1_rdy, i1_fire, i1_stall;
    logic i2_clk_en, i2_vld, i2_rdy, i2_fire, i2_stall;
    logic e0_vld, e0_rdy, e0_fire, e0_stall;

    /**************** I0 *****************/  // Dispatch Insn & Enqueue

    /**************** I1 *****************/  // Issue select & Dequeue
    assign i1_rdy    = ~i2_stall;
    assign i1_fire   = i1_vld & i1_rdy;
    assign i1_stall  = i1_vld & ~i1_rdy;
    assign i1_clk_en = i1_fire;

    logic i1_vld_d, i1_vld_q;
    logic [ROB_TAG_WIDTH-1:0] i1_rob_tag_d, i1_rob_tag_q;
    logic [BRQ_TAG_WIDTH-1:0] i1_brq_tag_d, i1_brq_tag_q;
    logic [BRU_OP_WIDTH-1:0] i1_opcode_d, i1_opcode_q;
    logic [INT_CDB_ID_WIDTH-1:0] i1_rs1_cdb_id_d, i1_rs1_cdb_id_q;
    logic [INT_CDB_ID_WIDTH-1:0] i1_rs2_cdb_id_d, i1_rs2_cdb_id_q;
    logic i1_use_rs1_d, i1_use_rs1_q;
    logic i1_use_rs2_d, i1_use_rs2_q;
    logic i1_use_rd_d, i1_use_rd_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rs1_d, i1_phy_rs1_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rs2_d, i1_phy_rs2_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rd_d, i1_phy_rd_q;
    logic [31:0] i1_imm_d, i1_imm_q;

    assign i1_vld_d = flush_i ? 1'b0 : (i2_stall ? i1_vld_q : i1_fire);

    always_ff @(posedge clk) begin : i1_vld_dff
        if (rst) begin
            i1_vld_q <= 1'b0;
        end else begin
            i1_vld_q <= i1_vld_d;
        end
    end

    always_ff @(posedge clk) begin : i1_payload_dff
        if (i1_clk_en) begin
            i1_rob_tag_q    <= i1_rob_tag_d;
            i1_brq_tag_q    <= i1_brq_tag_d;
            i1_opcode_q     <= i1_opcode_d;
            i1_rs1_cdb_id_q <= i1_rs1_cdb_id_d;
            i1_rs2_cdb_id_q <= i1_rs2_cdb_id_d;
            i1_use_rs1_q    <= i1_use_rs1_d;
            i1_use_rs2_q    <= i1_use_rs2_d;
            i1_use_rd_q     <= i1_use_rd_d;
            i1_phy_rs1_q    <= i1_phy_rs1_d;
            i1_phy_rs2_q    <= i1_phy_rs2_d;
            i1_phy_rd_q     <= i1_phy_rd_d;
            i1_imm_q        <= i1_imm_d;
        end
    end


    /**************** I2 *****************/  // Read Operand

    logic rd_prf_rejected;
    assign rd_prf_rejected = issue_rd_prf_vld_o & ~issue_rd_prf_rdy_i;

    assign i2_vld    = i1_vld_q;
    assign i2_rdy    = ~e0_stall & ~rd_prf_rejected;
    assign i2_stall  = i2_vld & ~i2_rdy;
    assign i2_fire   = i2_vld & i2_rdy;
    assign i2_clk_en = i2_fire;


    logic i2_vld_d, i2_vld_q;
    logic [ROB_TAG_WIDTH-1:0] i2_rob_tag_d, i2_rob_tag_q;
    logic [BRQ_TAG_WIDTH-1:0] i2_brq_tag_d, i2_brq_tag_q;
    logic [BTQ_TAG_WIDTH-1:0] i2_btq_tag_d, i2_btq_tag_q;
    logic [BRU_OP_WIDTH-1:0] i2_opcode_d, i2_opcode_q;
    logic i2_use_rd_d, i2_use_rd_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i2_phy_rd_d, i2_phy_rd_q;
    logic [XLEN-1:0] i2_operand0_d, i2_operand0_q;
    logic [XLEN-1:0] i2_operand1_d, i2_operand1_q;
    logic [31:0] i2_imm_d, i2_imm_q;
    logic [VADDR_WIDTH-1:0] i2_pc_d, i2_pc_q;
    logic i2_is_rvc_d, i2_is_rvc_q;
    logic i2_pred_taken_d, i2_pred_taken_q;
    logic [VADDR_WIDTH-1:0] i2_pred_target_d, i2_pred_target_q;

    logic rs1_is_zero, rs2_is_zero;
    logic [INT_CDB_DEPTH-1:0] rs1_forward_mask, rs2_forward_mask;
    logic [INT_CDB_DEPTH-1:0] rs1_forward_hit_mask, rs2_forward_hit_mask;
    logic rs1_forward_hit, rs2_forward_hit;
    logic [XLEN-1:0] rs1_forward_data, rs2_forward_data;
  
    generate
      for (genvar i = 0; i < INT_CDB_DEPTH; i++) begin
        assign rs1_forward_mask[i] = (i1_phy_rs1_q == cdb_forward_ptag_i[i1_rs1_cdb_id_q][i]) &
                  cdb_forward_vld_i[i1_rs1_cdb_id_q][i];
        assign rs2_forward_mask[i] = (i1_phy_rs2_q == cdb_forward_ptag_i[i1_rs2_cdb_id_q][i]) &
                  cdb_forward_vld_i[i1_rs2_cdb_id_q][i];
      end
    endgenerate
  
    assign rs1_forward_hit_mask = rs1_forward_mask & ~(rs1_forward_mask - 1'b1);
    assign rs2_forward_hit_mask = rs2_forward_mask & ~(rs2_forward_mask - 1'b1);
  
    assign rs1_forward_hit = |rs1_forward_mask;
    assign rs2_forward_hit = |rs2_forward_mask;
  
    onehot_mux #(
        .SOURCE_COUNT(INT_CDB_DEPTH),
        .DATA_WIDTH  (XLEN)
    ) u_rs1_forward_data_onehot_mux (
        .sel_i (rs1_forward_hit_mask),
        .data_i(cdb_forward_data_i[i1_rs1_cdb_id_q]),
        .data_o(rs1_forward_data)
    );
  
    onehot_mux #(
        .SOURCE_COUNT(INT_CDB_DEPTH),
        .DATA_WIDTH  (XLEN)
    ) rs2_forward_data_onehot_mux (
        .sel_i (rs2_forward_hit_mask),
        .data_i(cdb_forward_data_i[i1_rs2_cdb_id_q]),
        .data_o(rs2_forward_data)
    );
  

    assign rs1_is_zero = i1_phy_rs1_q == 0;
    assign rs2_is_zero = i1_phy_rs2_q == 0;

    assign rob_tag_o = i1_rob_tag_q;
    assign brq_tag_o = i1_brq_tag_q;

    assign issue_rd_prf_vld_o = i1_vld_q;
    assign issue_rd_prf_mask_o = {
        i1_use_rs2_q & ~rs2_is_zero & ~rs2_forward_hit,
        i1_use_rs1_q & ~rs1_is_zero & ~rs1_forward_hit
    };
    assign issue_rd_prf_tag_o = {i1_phy_rs2_q, i1_phy_rs1_q};


    always_comb begin : i2_operand0_sel
        casez ({
            i1_use_rs1_q, rs1_is_zero, rs1_forward_hit
        })
            3'b100:  i2_operand0_d = prf_data_i[0];
            3'b11?:  i2_operand0_d = {XLEN{1'b0}};
            3'b101:  i2_operand0_d = rs1_forward_data;
            default: i2_operand0_d = {XLEN{1'b0}};
        endcase
    end

    always_comb begin : i2_operand1_sel
        casez ({
            i1_use_rs2_q, rs2_is_zero, rs2_forward_hit
        })
            3'b100:  i2_operand1_d = prf_data_i[1];
            3'b11?:  i2_operand1_d = {XLEN{1'b0}};
            3'b101:  i2_operand1_d = rs2_forward_data;
            default: i2_operand1_d = {XLEN{1'b0}};
        endcase
    end

    assign i2_rob_tag_d     = i1_rob_tag_q;
    assign i2_brq_tag_d     = i1_brq_tag_q;
    assign i2_btq_tag_d     = btq_tag_i;
    assign i2_opcode_d      = i1_opcode_q;
    assign i2_use_rd_d      = i1_use_rd_q;
    assign i2_phy_rd_d      = i1_phy_rd_q;
    assign i2_pc_d          = inst_pc_i;
    assign i2_is_rvc_d      = is_rvc_i;
    assign i2_pred_taken_d  = pred_taken_i;
    assign i2_pred_target_d = pred_target_i;
    assign i2_imm_d         = i1_imm_q;

    assign i2_vld_d         = flush_i ? 1'b0 : (e0_stall ? i2_vld_q : i2_fire);


    always_ff @(posedge clk) begin : i2_vld_dff
        if (rst) begin
            i2_vld_q <= 1'b0;
        end else begin
            i2_vld_q <= i2_vld_d;
        end
    end

    always_ff @(posedge clk) begin : i2_payload_dff
        if (i2_clk_en) begin
            i2_rob_tag_q     <= i2_rob_tag_d;
            i2_brq_tag_q     <= i2_brq_tag_d;
            i2_btq_tag_q     <= i2_btq_tag_d;
            i2_opcode_q      <= i2_opcode_d;
            i2_use_rd_q      <= i2_use_rd_d;
            i2_phy_rd_q      <= i2_phy_rd_d;
            i2_pc_q          <= i2_pc_d;
            i2_is_rvc_q      <= i2_is_rvc_d;
            i2_imm_q         <= i2_imm_d;
            i2_pred_taken_q  <= i2_pred_taken_d;
            i2_pred_target_q <= i2_pred_target_d;
            i2_operand0_q    <= i2_operand0_d;
            i2_operand1_q    <= i2_operand1_d;
        end
    end
    /**************** E0 *****************/  // Execute & Wakeup

    assign e0_vld   = i2_vld_q & ~flush_i;
    assign e0_fire  = e0_vld & e0_rdy;
    assign e0_stall = e0_vld & ~e0_rdy;

    logic [     ROB_TAG_WIDTH-1:0] e0_rob_tag;
    logic [     BRQ_TAG_WIDTH-1:0] e0_brq_tag;
    logic [     BTQ_TAG_WIDTH-1:0] e0_btq_tag;
    logic [      BRU_OP_WIDTH-1:0] e0_opcode;
    logic [INT_PREG_TAG_WIDTH-1:0] e0_phy_rd;
    logic [              XLEN-1:0] e0_operand0;
    logic [              XLEN-1:0] e0_operand1;
    logic [       VADDR_WIDTH-1:0] e0_pc;
    logic                          e0_is_rvc;
    logic [                  31:0] e0_imm;
    logic                          e0_pred_taken;
    logic [       VADDR_WIDTH-1:0] e0_pred_target;

    assign e0_rob_tag               = i2_rob_tag_q;
    assign e0_brq_tag               = i2_brq_tag_q;
    assign e0_btq_tag               = i2_btq_tag_q;
    assign e0_opcode                = i2_opcode_q;
    assign e0_phy_rd                = i2_phy_rd_q;
    assign e0_operand0              = i2_operand0_q;
    assign e0_operand1              = i2_operand1_q;
    assign e0_pc                    = i2_pc_q;
    assign e0_is_rvc                = i2_is_rvc_q;
    assign e0_imm                   = i2_imm_q;
    assign e0_pred_taken            = i2_pred_taken_q;
    assign e0_pred_target           = i2_pred_target_q;


    assign forward_wakeup_vld_o     = i2_fire;
    assign forward_wakeup_prf_tag_o = i1_phy_rd_q;

    /**************** W0 *****************/  // WriteBack & Forwarding
    logic                          w0_vld;
    logic                          w0_rdy;
    logic                          w0_fire;
    logic [       VADDR_WIDTH-1:0] w0_pc;
    logic [     ROB_TAG_WIDTH-1:0] w0_rob_tag;
    logic [     BRQ_TAG_WIDTH-1:0] w0_brq_tag;
    logic [     BTQ_TAG_WIDTH-1:0] w0_btq_tag;
    logic [INT_PREG_TAG_WIDTH-1:0] w0_phy_rd;
    logic [              XLEN-1:0] w0_result;
    
    logic                          w0_taken;
    logic                          w0_mispred;
    logic [       VADDR_WIDTH-1:0] w0_target;

    assign w0_rdy                 = wr_prf_rdy_i;
    assign w0_fire                = w0_vld & w0_rdy;

    assign wr_prf_vld_o           = w0_fire;
    assign wr_prf_tag_o           = w0_phy_rd;
    assign wr_prf_data_o          = w0_result;

    assign wb_rob_vld_o           = w0_fire;
    assign wb_rob_tag_o           = w0_rob_tag;

    assign bru_resolve_vld_o      = w0_fire;
    assign bru_resolve_pc_o       = w0_pc;
    assign bru_resolve_taken_o    = w0_taken;
    assign bru_resolve_mispred_o  = w0_mispred;
    assign bru_resolve_target_o   = w0_target;
    assign bru_resolve_rob_tag_o  = w0_rob_tag;
    assign bru_resolve_brq_tag_o  = w0_brq_tag;
    assign bru_resolve_btq_tag_o  = w0_btq_tag;

    assign forward_data_vld_o     = w0_vld;
    assign forward_data_prf_tag_o = w0_phy_rd;
    assign forward_data_o         = w0_result;

    br_iq #(
        .ENTRY_COUNT(IQ_ENTRY_COUNT)
    ) u_br_iq (
        .disp_br_iq_vld_i(disp_br_iq_vld_i),
        .disp_br_iq_rob_tag_i(disp_br_iq_rob_tag_i),
        .disp_br_iq_brq_tag_i(disp_br_iq_brq_tag_i),
        .disp_br_iq_opcode_i(disp_br_iq_opcode_i),
        .disp_br_iq_rs1_rdy_i(disp_br_iq_rs1_rdy_i),
        .disp_br_iq_rs2_rdy_i(disp_br_iq_rs2_rdy_i),
        .disp_br_iq_use_rs1_i(disp_br_iq_use_rs1_i),
        .disp_br_iq_use_rs2_i(disp_br_iq_use_rs2_i),
        .disp_br_iq_use_rd_i(disp_br_iq_use_rd_i),
        .disp_br_iq_phy_rs1_i(disp_br_iq_phy_rs1_i),
        .disp_br_iq_phy_rs2_i(disp_br_iq_phy_rs2_i),
        .disp_br_iq_phy_rd_i(disp_br_iq_phy_rd_i),
        .disp_br_iq_imm_i(disp_br_iq_imm_i),
        .disp_br_iq_rdy_o(disp_br_iq_rdy_o),
        .br_iq_issue_vld_o(i1_vld),
        .br_iq_issue_rob_tag_o(i1_rob_tag_d),
        .br_iq_issue_brq_tag_o(i1_brq_tag_d),
        .br_iq_issue_opcode_o(i1_opcode_d),
        .br_iq_issue_rs1_cdb_id_o(i1_rs1_cdb_id_d),
        .br_iq_issue_rs2_cdb_id_o(i1_rs2_cdb_id_d),
        .br_iq_issue_use_rs1_o(i1_use_rs1_d),
        .br_iq_issue_use_rs2_o(i1_use_rs2_d),
        .br_iq_issue_use_rd_o(i1_use_rd_d),
        .br_iq_issue_phy_rs1_o(i1_phy_rs1_d),
        .br_iq_issue_phy_rs2_o(i1_phy_rs2_d),
        .br_iq_issue_phy_rd_o(i1_phy_rd_d),
        .br_iq_issue_imm_o(i1_imm_d),
        .br_iq_issue_rdy_i(i1_rdy),
        .br_iq_bru_issue_rdy_i(e0_rdy),
        .wakeup_vld_i(cdb_wakeup_vld_i),
        .wakeup_ptag_i(cdb_wakeup_ptag_i),
        .forwarding_vld_i(cdb_forward_ctrl_vld_i),
        .forwarding_ptag_i(cdb_forward_ctrl_ptag_i),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    rvh_bru u_rvh_bru (
        .issue_vld_i(e0_vld),
        .issue_rob_tag_i(e0_rob_tag),
        .issue_brq_tag_i(e0_brq_tag),
        .issue_btq_tag_i(e0_btq_tag),
        .issue_phy_rd_i(e0_phy_rd),
        .issue_opcode_i(e0_opcode),
        .issue_operand0_i(e0_operand0),
        .issue_operand1_i(e0_operand1),
        .issue_pc_i(e0_pc),
        .issue_is_rvc_i(e0_is_rvc),
        .issue_imm_i(e0_imm),
        .issue_pred_taken_i(e0_pred_taken),
        .issue_pred_target_i(e0_pred_target),
        .issue_rdy_o(e0_rdy),
        .wb_vld_o(w0_vld),
        .wb_pc_o(w0_pc),
        .wb_rob_tag_o(w0_rob_tag),
        .wb_brq_tag_o(w0_brq_tag),
        .wb_btq_tag_o(w0_btq_tag),
        .wb_phy_rd_o(w0_phy_rd),
        .wb_data_o(w0_result),
        .wb_taken_o(w0_taken),
        .wb_mispred_o(w0_mispred),
        .wb_target_o(w0_target),
        .wb_rdy_i(w0_rdy),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );




endmodule : br_ctrl_pipe
