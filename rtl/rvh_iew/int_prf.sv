module int_prf #(
    parameter int unsigned XLEN = 64,
    parameter int unsigned REGISTER_COUNT = 128,
    parameter int unsigned ALLOC_PORT_COUNT = 4,
    parameter int unsigned BUSY_TABLE_READ_PORT = 12,
    parameter int unsigned READ_PORT_COUNT = 6,
    parameter int unsigned WRITE_PORT_COUNT = 9,
    parameter int unsigned OUTPUT_LATENCY = 0,
    localparam int unsigned TAG_WIDTH = $clog2(REGISTER_COUNT)
) (
    // Allocate Port
    input  logic [    ALLOC_PORT_COUNT-1:0]                alloc_prf_vld_i,
    input  logic [    ALLOC_PORT_COUNT-1:0][TAG_WIDTH-1:0] alloc_prf_tag_i,
    // Busy Table Read Port
    input  logic [BUSY_TABLE_READ_PORT-1:0][TAG_WIDTH-1:0] disp_inst_src_ptag_i,
    output logic [BUSY_TABLE_READ_PORT-1:0]                disp_inst_src_rdy_o,
    // Read Port
    input  logic [     READ_PORT_COUNT-1:0]                rd_prf_vld_i,
    input  logic [     READ_PORT_COUNT-1:0][TAG_WIDTH-1:0] rd_prf_tag_i,
    output logic [     READ_PORT_COUNT-1:0][     XLEN-1:0] rd_prf_data_o,
    // Write Port
    input  logic [    WRITE_PORT_COUNT-1:0]                wr_prf_vld_i,
    input  logic [    WRITE_PORT_COUNT-1:0][TAG_WIDTH-1:0] wr_prf_tag_i,
    input  logic [    WRITE_PORT_COUNT-1:0][     XLEN-1:0] wr_prf_data_i,

    input clk,
    input rst
);

    logic [REGISTER_COUNT-1:0] prf_clk_en;
    logic [REGISTER_COUNT-1:0] alloc_clk_en;
    logic [REGISTER_COUNT-1:0][XLEN-1:0] prf_d, prf_q;
    logic [REGISTER_COUNT-1:0] busy_table_d, busy_table_q;

    logic [READ_PORT_COUNT-1:0][XLEN-1:0] rd_data;
    
    // Init Src Status
    always_comb begin : init_src_status_logic    
        for(int i = 0 ; i < BUSY_TABLE_READ_PORT; i++) begin
            disp_inst_src_rdy_o[i] = busy_table_q[disp_inst_src_ptag_i[i]];
        end
        // Read After Write
        for(int i = 0 ; i < BUSY_TABLE_READ_PORT; i++) begin
            for(int j = 0 ; j < WRITE_PORT_COUNT; j++) begin
                if(wr_prf_vld_i[j] & (wr_prf_tag_i[j] == disp_inst_src_ptag_i[i])) begin
                    disp_inst_src_rdy_o[i] = 1'b1;
                end
            end
        end
    end
    /* Read Logic */
    generate
        if (OUTPUT_LATENCY == 0) begin
            for (genvar i = 0; i < READ_PORT_COUNT; i++) begin
                assign rd_data[i] = prf_q[rd_prf_tag_i[i]];
            end
        end else begin
            always_ff @(posedge clk) begin
                for (int i = 0; i < READ_PORT_COUNT; i++) begin
                    if (rd_prf_vld_i[i]) begin
                        rd_data[i] <= prf_q[rd_prf_tag_i[i]];
                    end
                end
            end
        end
    endgenerate

    assign rd_prf_data_o = rd_data;


    /* Write Logic */
    always_comb begin : prf_clk_gate_logic
        prf_clk_en = {REGISTER_COUNT{1'b0}};
        for (int i = 0; i < WRITE_PORT_COUNT; i++) begin
            if(wr_prf_vld_i[i]) begin
                prf_clk_en[wr_prf_tag_i[i]] = 1'b1;
            end
        end
        prf_clk_en[0] = 1'b0;
    end
    
    always_comb begin : alloc_clk_gate_logic
        alloc_clk_en = {REGISTER_COUNT{1'b0}};
        for(int i = 0 ; i < ALLOC_PORT_COUNT; i++) begin
            if(alloc_prf_vld_i[i]) begin
                alloc_clk_en[alloc_prf_tag_i[i]] = 1'b1;
            end
        end
        alloc_clk_en[0] = 1'b0;
    end

    always_comb begin : busy_table_update_logic
        busy_table_d = busy_table_q;
        for(int i = 0 ; i < WRITE_PORT_COUNT; i++) begin
            if(wr_prf_vld_i[i]) begin
                busy_table_d[wr_prf_tag_i[i]] = 1'b1;
            end
        end
        for(int i = 0 ; i < ALLOC_PORT_COUNT; i++) begin
            if(alloc_prf_vld_i[i]) begin
                busy_table_d[alloc_prf_tag_i[i]] = 1'b0;
            end
        end
        busy_table_d[0] = 1'b1;
    end
    
    always_comb begin : prf_update_logic
        prf_d = prf_q;
        for (int i = 0; i < WRITE_PORT_COUNT; i++) begin
            if(wr_prf_vld_i[i]) begin
                prf_d[wr_prf_tag_i[i]] = wr_prf_data_i[i];
            end
        end
        prf_d[0] = {XLEN{1'b0}};
    end


    /* DFF */
    always_ff @(posedge clk) begin : busy_table_dff
        if (rst) begin
            for(int i = 0 ; i < REGISTER_COUNT; i++) begin
                if( i < 32 ) begin
                    busy_table_q[i] <= 1'b1;
                end else begin
                    busy_table_q[i] <= 1'b0;
                end
            end
        end else begin
            for(int i = 0 ; i < REGISTER_COUNT; i++) begin
                if(prf_clk_en[i] | alloc_clk_en[i]) begin
                    busy_table_q[i] <= busy_table_d[i];
                end
            end
        end
    end

    always_ff @(posedge clk) begin : prf_dff
        if (rst) begin
            for(int i = 0 ; i < 32 ; i++) begin
                prf_q[i] <= {XLEN{1'b0}};
            end
        end else begin
            for (int i = 0; i < REGISTER_COUNT; i++) begin
                if (prf_clk_en[i]) begin
                    prf_q[i] <= prf_d[i];
                end
            end
        end
    end

endmodule : int_prf
