module sdu_ctrl_pipe
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned IQ_ENTRY_COUNT = 4
) (
    // Disp -> IQ(SDU)
    input  logic                          disp_sdu_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] disp_sdu_iq_rob_tag_i,
    input  logic [     STQ_TAG_WIDTH-1:0] disp_sdu_iq_stq_tag_i,
    input  logic                          disp_sdu_iq_rs2_rdy_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_sdu_iq_phy_rs2_i,
    output logic                          disp_sdu_iq_rdy_o,

    // IQ -> Issue Ctrl : Issue Request
    output logic                          issue_rd_prf_vld_o,
    output logic                          issue_rd_prf_mask_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] issue_rd_prf_tag_o,
    input  logic                          issue_rd_prf_rdy_i,

    // PRF -> IQ : Read Operand
    input logic [XLEN-1:0] prf_data_i,

    // SDU -> SDQ : Issue Data
    output logic store_data_vld_o,
    output logic [STQ_TAG_WIDTH-1:0] store_data_stq_tag_o,
    output logic [XLEN-1:0] store_data_o,
    input logic store_data_rdy_i,

    // Fowarding Path -> IQ : Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0]                         cdb_wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_wakeup_ptag_i,

    // Fowarding Path -> IQ : Forward Ctrl
    input logic [INT_CDB_BUS_WIDTH-1:0]                         cdb_forward_ctrl_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ctrl_ptag_i,

    // Fowarding Path -> IQ : Forward Data
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0]                         cdb_forward_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ptag_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][              XLEN-1:0] cdb_forward_data_i,


    input logic flush_i,
    input clk,
    input rst
);
    localparam bit LD_TYPE = 1'b0;
    localparam bit ST_TYPE = 1'b1;


    logic i1_clk_en, i1_vld, i1_rdy, i1_fire, i1_stall;
    logic i2_clk_en, i2_vld, i2_rdy, i2_fire, i2_stall;
    logic m0_vld, m0_rdy, m0_fire, m0_stall;

    /**************** I0 *****************/  // Dispatch Insn & Enqueue

    /**************** I1 *****************/  // Issue select & Dequeue
    assign i1_rdy    = ~i2_stall;
    assign i1_fire   = i1_vld & i1_rdy;
    assign i1_stall  = i1_vld & ~i1_rdy;
    assign i1_clk_en = i1_fire;

    logic i1_vld_d, i1_vld_q;
    logic [ROB_TAG_WIDTH-1:0] i1_rob_tag_d, i1_rob_tag_q;
    logic [STQ_TAG_WIDTH-1:0] i1_stq_tag_d, i1_stq_tag_q;
    logic [INT_CDB_ID_WIDTH-1:0] i1_rs2_cdb_id_d, i1_rs2_cdb_id_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rs2_d, i1_phy_rs2_q;

    assign i1_vld_d = flush_i ? 1'b0 : (i2_stall ? i1_vld_q : i1_fire);

    always_ff @(posedge clk) begin : i1_vld_dff
        if (rst) begin
            i1_vld_q <= 1'b0;
        end else begin
            i1_vld_q <= i1_vld_d;
        end
    end

    always_ff @(posedge clk) begin : i1_payload_dff
        if (i1_clk_en) begin
            i1_rob_tag_q    <= i1_rob_tag_d;
            i1_stq_tag_q    <= i1_stq_tag_d;
            i1_rs2_cdb_id_q <= i1_rs2_cdb_id_d;
            i1_phy_rs2_q    <= i1_phy_rs2_d;
        end
    end

    /**************** I2 *****************/  // Read Operand

    logic rd_prf_rejected;
    assign rd_prf_rejected = issue_rd_prf_vld_o & ~issue_rd_prf_rdy_i;

    assign i2_vld    = i1_vld_q;
    assign i2_rdy    = ~m0_stall & ~rd_prf_rejected;
    assign i2_stall  = i2_vld & ~i2_rdy;
    assign i2_fire   = i2_vld & i2_rdy;
    assign i2_clk_en = i2_fire;


    logic i2_vld_d, i2_vld_q;
    logic [ROB_TAG_WIDTH-1:0] i2_rob_tag_d, i2_rob_tag_q;
    logic [STQ_TAG_WIDTH-1:0] i2_stq_tag_d, i2_stq_tag_q;
    logic [XLEN-1:0] i2_operand0_d, i2_operand0_q;

    logic rs2_is_zero;
    logic [INT_CDB_DEPTH-1:0] rs2_forward_mask;
    logic [INT_CDB_DEPTH-1:0] rs2_forward_hit_mask;
    logic rs2_forward_hit;
    logic [XLEN-1:0] rs2_forward_data;
  
    generate
      for (genvar i = 0; i < INT_CDB_DEPTH; i++) begin
        assign rs2_forward_mask[i] = (i1_phy_rs2_q == cdb_forward_ptag_i[i1_rs2_cdb_id_q][i]) &
                  cdb_forward_vld_i[i1_rs2_cdb_id_q][i];
      end
    endgenerate
  
    assign rs2_forward_hit_mask = rs2_forward_mask & ~(rs2_forward_mask - 1'b1);
  
    assign rs2_forward_hit = |rs2_forward_mask;
  
    onehot_mux #(
        .SOURCE_COUNT(INT_CDB_DEPTH),
        .DATA_WIDTH  (XLEN)
    ) rs2_forward_data_onehot_mux (
        .sel_i (rs2_forward_hit_mask),
        .data_i(cdb_forward_data_i[i1_rs2_cdb_id_q]),
        .data_o(rs2_forward_data)
    );

    assign rs2_is_zero = i1_phy_rs2_q == 0;

    assign issue_rd_prf_vld_o = i1_vld_q;
    assign issue_rd_prf_mask_o = ~rs2_is_zero & ~rs2_forward_hit;
    assign issue_rd_prf_tag_o = i1_phy_rs2_q;

    always_comb begin : i2_operand0_sel
        casez ({
            rs2_is_zero, rs2_forward_hit
        })
            2'b00:   i2_operand0_d = prf_data_i;
            2'b1?:   i2_operand0_d = {XLEN{1'b0}};
            2'b01:   i2_operand0_d = rs2_forward_data;
            default: i2_operand0_d = {XLEN{1'b0}};
        endcase
    end

    assign i2_rob_tag_d = i1_rob_tag_q;
    assign i2_stq_tag_d = i1_stq_tag_q;

    assign i2_vld_d = flush_i ? 1'b0 : (m0_stall ? i2_vld_q : i2_fire);

    always_ff @(posedge clk) begin : i2_vld_dff
        if (rst) begin
            i2_vld_q <= 1'b0;
        end else begin
            i2_vld_q <= i2_vld_d;
        end
    end

    always_ff @(posedge clk) begin : i2_payload_dff
        if (i2_clk_en) begin
            i2_rob_tag_q  <= i2_rob_tag_d;
            i2_stq_tag_q  <= i2_stq_tag_d;
            i2_operand0_q <= i2_operand0_d;
        end
    end

    /**************** m0 *****************/  // Send to LSU
    logic [ROB_TAG_WIDTH-1:0] m0_rob_tag;
    logic [STQ_TAG_WIDTH-1:0] m0_stq_tag;
    logic [         XLEN-1:0] m0_data;

    assign m0_vld               = i2_vld_q & ~flush_i;
    assign m0_fire              = m0_vld & m0_rdy;
    assign m0_stall             = m0_vld & ~m0_rdy;
    assign m0_rdy               = store_data_rdy_i;

    assign m0_rob_tag           = i2_rob_tag_q;
    assign m0_stq_tag           = i2_stq_tag_q;
    assign m0_data              = i2_operand0_q;

    assign store_data_vld_o     = m0_vld;
    assign store_data_stq_tag_o = m0_stq_tag;
    assign store_data_o         = m0_data;


    sdu_iq #(
        .ENTRY_COUNT(IQ_ENTRY_COUNT)
    ) u_sdu_iq (
        .disp_sdu_iq_vld_i(disp_sdu_iq_vld_i),
        .disp_sdu_iq_rob_tag_i(disp_sdu_iq_rob_tag_i),
        .disp_sdu_iq_stq_tag_i(disp_sdu_iq_stq_tag_i),
        .disp_sdu_iq_rs2_rdy_i(disp_sdu_iq_rs2_rdy_i),
        .disp_sdu_iq_phy_rs2_i(disp_sdu_iq_phy_rs2_i),
        .disp_sdu_iq_rdy_o(disp_sdu_iq_rdy_o),
        .sdu_iq_issue_vld_o(i1_vld),
        .sdu_iq_issue_rob_tag_o(i1_rob_tag_d),
        .sdu_iq_issue_stq_tag_o(i1_stq_tag_d),
        .sdu_iq_issue_rs2_cdb_id_o(i1_rs2_cdb_id_d),
        .sdu_iq_issue_phy_rs2_o(i1_phy_rs2_d),
        .sdu_iq_sdu_issue_rdy_i(1'b1),
        .sdu_iq_issue_rdy_i(i1_rdy),
        .wakeup_vld_i(cdb_wakeup_vld_i),
        .wakeup_ptag_i(cdb_wakeup_ptag_i),
        .forwarding_vld_i(cdb_forward_ctrl_vld_i),
        .forwarding_ptag_i(cdb_forward_ctrl_ptag_i),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

endmodule : sdu_ctrl_pipe
