module rvh_cdb
  import rvh_pkg::*;
(
    // FU -> CDB : Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0] fu_cdb_wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] fu_cdb_wakeup_prf_tag_i,

    // FU -> CDB : Forward
    input logic [INT_CDB_BUS_WIDTH-1:0] fu_cdb_forward_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] fu_cdb_forward_prf_tag_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][XLEN-1:0] fu_cdb_forward_data_i,

    // CDB -> IQ : Wakeup
    output logic [INT_CDB_BUS_WIDTH-1:0] cdb_iq_wakeup_vld_o,
    output logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_iq_wakeup_prf_tag_o,

    // CDB -> IQ : Forward Ctrl Path
    output logic [INT_CDB_BUS_WIDTH-1:0] cdb_iq_forward_ctrl_vld_o,
    output logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_iq_forward_ctrl_prf_tag_o,

    // CDB -> IQ : Forward Data Network
    output logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0] cdb_iq_forward_data_vld_o,
    output logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_iq_forward_data_prf_tag_o,
    output logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][XLEN-1:0] cdb_iq_forward_data_o,

    input logic flush_i,

    input clk,
    input rst
);

  // Output 
  assign cdb_iq_wakeup_vld_o           = fu_cdb_wakeup_vld_i;
  assign cdb_iq_wakeup_prf_tag_o       = fu_cdb_wakeup_prf_tag_i;

  assign cdb_iq_forward_ctrl_vld_o     = fu_cdb_forward_vld_i;
  assign cdb_iq_forward_ctrl_prf_tag_o = fu_cdb_forward_prf_tag_i;

  generate
    if (INT_CDB_DEPTH == 1) begin
      for (genvar i = 0; i < INT_CDB_BUS_WIDTH; i++) begin : gen_forward_path
        assign cdb_iq_forward_data_vld_o[i][0] = fu_cdb_forward_vld_i[i];
        assign cdb_iq_forward_data_prf_tag_o[i][0] = fu_cdb_forward_prf_tag_i[i];
        assign cdb_iq_forward_data_o[i][0] = fu_cdb_forward_data_i[i];
      end
    end else begin
      logic [INT_CDB_BUS_WIDTH-1:0] lane_clk_en;
      logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:1] forward_vld_d, forward_vld_q;
      logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:1][INT_PREG_TAG_WIDTH-1:0]
          forward_prf_tag_d, forward_prf_tag_q;
      logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:1][XLEN-1:0]
          forward_data_d, forward_data_q;


      for (genvar i = 0; i < INT_CDB_BUS_WIDTH; i++) begin : gen_forward_network
        assign cdb_iq_forward_data_vld_o[i][0] = fu_cdb_forward_vld_i[i];
        assign cdb_iq_forward_data_prf_tag_o[i][0] = fu_cdb_forward_prf_tag_i[i];
        assign cdb_iq_forward_data_o[i][0] = fu_cdb_forward_data_i[i];
        assign cdb_iq_forward_data_vld_o[i][INT_CDB_DEPTH-1:1] = forward_vld_q[i][INT_CDB_DEPTH-1:1];
        assign cdb_iq_forward_data_prf_tag_o[i][INT_CDB_DEPTH-1:1] = forward_prf_tag_q[i][INT_CDB_DEPTH-1:1];
        assign cdb_iq_forward_data_o[i][INT_CDB_DEPTH-1:1] = forward_data_q[i][INT_CDB_DEPTH-1:1];
      end

      for (genvar i = 0; i < INT_CDB_BUS_WIDTH; i++) begin
        assign lane_clk_en[i] = fu_cdb_forward_vld_i[i];
        for (genvar j = 1; j < INT_CDB_DEPTH; j++) begin
          if (j == 1) begin
            assign forward_vld_d[i][j] = fu_cdb_forward_vld_i[i];
            assign forward_prf_tag_d[i][j] = fu_cdb_forward_prf_tag_i[i];
            assign forward_data_d[i][j] = fu_cdb_forward_data_i[i];
          end else begin
            assign forward_vld_d[i][j] = forward_vld_q[i][j-1];
            assign forward_prf_tag_d[i][j] = forward_prf_tag_q[i][j-1];
            assign forward_data_d[i][j] = forward_data_q[i][j-1];
          end
        end
      end

      for (genvar i = 0; i < INT_CDB_BUS_WIDTH; i++) begin
        always_ff @(posedge clk) begin
          if (rst) begin
            forward_vld_q[i] <= {INT_CDB_DEPTH - 1{1'b0}};
          end else begin
            forward_vld_q[i] <= forward_vld_d[i];
          end
        end
        always_ff @(posedge clk) begin
          if (lane_clk_en[i]) begin
            forward_prf_tag_q[i] <= forward_prf_tag_d[i];
            forward_data_q[i] <= forward_data_d[i];
          end
        end
      end
    end
  endgenerate

endmodule : rvh_cdb
