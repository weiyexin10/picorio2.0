module sdu_iq
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned ENTRY_COUNT = 4
) (
    // Disp -> IQ(sdu)
    input  logic                          disp_sdu_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] disp_sdu_iq_rob_tag_i,
    input  logic [     STQ_TAG_WIDTH-1:0] disp_sdu_iq_stq_tag_i,
    input  logic                          disp_sdu_iq_rs2_rdy_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_sdu_iq_phy_rs2_i,
    output logic                          disp_sdu_iq_rdy_o,

    // IQ -> Fu : Issue
    output logic                          sdu_iq_issue_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0] sdu_iq_issue_rob_tag_o,
    output logic [     STQ_TAG_WIDTH-1:0] sdu_iq_issue_stq_tag_o,
    output logic [  INT_CDB_ID_WIDTH-1:0] sdu_iq_issue_rs2_cdb_id_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] sdu_iq_issue_phy_rs2_o,
    input  logic                          sdu_iq_sdu_issue_rdy_i,
    input  logic                          sdu_iq_issue_rdy_i,

    // Early Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0]                         wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] wakeup_ptag_i,

    // Fu -> IQ : Forwarding
    input logic [INT_CDB_BUS_WIDTH-1:0]                         forwarding_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] forwarding_ptag_i,

    input logic flush_i,

    input clk,
    input rst
);

    localparam int unsigned ENTRY_TAG_WIDTH = (ENTRY_COUNT > 1) ? $clog2(ENTRY_COUNT) : 1;


    // Dispatch 
    logic disp_vld;
    logic disp_rdy;
    logic disp_fire;
    logic [ENTRY_COUNT-1:0] disp_vld_mask;
    logic [ENTRY_TAG_WIDTH-1:0] disp_tag;

    always_comb begin : gen_disp_vld_mask
        for (int i = 0; i < ENTRY_COUNT; i++) begin
            disp_vld_mask[i] = disp_fire & (disp_tag == i);
        end
    end
    assign disp_vld = disp_sdu_iq_vld_i;
    assign disp_fire = disp_vld & disp_rdy;
    assign disp_sdu_iq_rdy_o = disp_rdy;

    // Issue Select
    logic                                                  issue_vld;
    logic                                                  issue_rdy;
    logic                                                  issue_fire;
    logic [       ENTRY_COUNT-1:0]                         issue_rdy_mask;
    logic [       ENTRY_COUNT-1:0]                         issue_vld_mask;
    logic [       ENTRY_COUNT-1:0][   ENTRY_TAG_WIDTH-1:0] issue_sel_tags;
    logic [   ENTRY_TAG_WIDTH-1:0]                         issue_tag;

    logic [       ENTRY_COUNT-1:0][     ROB_TAG_WIDTH-1:0] issue_rob_tag_mux_in;
    logic [       ENTRY_COUNT-1:0][     STQ_TAG_WIDTH-1:0] issue_stq_tag_mux_in;
    logic [       ENTRY_COUNT-1:0][  INT_CDB_ID_WIDTH-1:0] issue_rs2_cdb_id_mux_in;
    logic [       ENTRY_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] issue_phy_rs2_mux_in;

    logic [     ROB_TAG_WIDTH-1:0]                         issue_rob_tag_mux_out;
    logic [     STQ_TAG_WIDTH-1:0]                         issue_stq_tag_mux_out;
    logic [  INT_CDB_ID_WIDTH-1:0]                         issue_rs2_cdb_id_mux_out;
    logic [INT_PREG_TAG_WIDTH-1:0]                         issue_phy_rs2_mux_out;

    assign issue_vld                 = |issue_rdy_mask;
    assign issue_rdy                 = sdu_iq_issue_rdy_i;
    assign issue_fire                = issue_vld & issue_rdy;

    // Output 
    assign sdu_iq_issue_vld_o        = issue_fire;
    assign sdu_iq_issue_rob_tag_o    = issue_rob_tag_mux_out;
    assign sdu_iq_issue_stq_tag_o    = issue_stq_tag_mux_out;
    assign sdu_iq_issue_rs2_cdb_id_o = issue_rs2_cdb_id_mux_out;
    assign sdu_iq_issue_phy_rs2_o    = issue_phy_rs2_mux_out;


    always_comb begin : gen_entry_tag
        for(int i = 0 ; i < ENTRY_COUNT; i++) begin
            issue_sel_tags[i] = i[ENTRY_TAG_WIDTH-1:0];
        end
    end

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ENTRY_TAG_WIDTH)
    ) u_issue_entry_tag_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_sel_tags),
        .data_o(issue_tag)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_issue_rob_tag_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_rob_tag_mux_in),
        .data_o(issue_rob_tag_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (STQ_TAG_WIDTH)
    ) u_issue_opcode_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_stq_tag_mux_in),
        .data_o(issue_stq_tag_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_CDB_ID_WIDTH)
    ) u_issue_rs2_cdb_id_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_rs2_cdb_id_mux_in),
        .data_o(issue_rs2_cdb_id_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
    ) u_issue_phy_rs2_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_phy_rs2_mux_in),
        .data_o(issue_phy_rs2_mux_out)
    );

    freelist #(
        .ENTRY_COUNT(ENTRY_COUNT)
    ) u_freelist (
        .enq_vld_i(issue_fire),
        .enq_tag_i(issue_tag),
        .deq_vld_i(disp_vld),
        .deq_tag_o(disp_tag),
        .deq_rdy_o(disp_rdy),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    age_order_selector #(
        .ENTRY_COUNT(ENTRY_COUNT),
        .ENQ_WIDTH(1),
        .DEQ_WIDTH(1),
        .SEL_WIDTH(1)
    ) u_issue_selector (
        .enq_vld_i(disp_fire),
        .enq_tag_i(disp_tag),
        .deq_vld_i(issue_fire),
        .deq_tag_i(issue_tag),
        .vld_mask_o(),
        .sel_mask_i(issue_rdy_mask),
        .sel_oldest_mask_o(issue_vld_mask),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    generate
        for (genvar i = 0; i < ENTRY_COUNT; i++) begin
            sdu_iq_entry u_sdu_iq_entry (
                .disp_vld_i(disp_vld_mask[i]),
                .disp_rob_tag_i(disp_sdu_iq_rob_tag_i),
                .disp_stq_tag_i(disp_sdu_iq_stq_tag_i),
                .disp_rs2_rdy_i(disp_sdu_iq_rs2_rdy_i),
                .disp_phy_rs2_i(disp_sdu_iq_phy_rs2_i),
                .sdu_is_rdy_i(sdu_iq_sdu_issue_rdy_i),
                .is_rdy_o(issue_rdy_mask[i]),
                .issue_vld_i(issue_vld_mask[i]& issue_rdy),
                .issue_rob_tag_o(issue_rob_tag_mux_in[i]),
                .issue_stq_tag_o(issue_stq_tag_mux_in[i]),
                .issue_rs2_cdb_id_o(issue_rs2_cdb_id_mux_in[i]),
                .issue_phy_rs2_o(issue_phy_rs2_mux_in[i]),
                .wakeup_vld_i(wakeup_vld_i),
                .wakeup_ptag_i(wakeup_ptag_i),
                .forwarding_vld_i(forwarding_vld_i),
                .forwarding_ptag_i(forwarding_ptag_i),
                .flush_i(flush_i),
                .clk(clk),
                .rst(rst)
            );

        end
    endgenerate


endmodule : sdu_iq
