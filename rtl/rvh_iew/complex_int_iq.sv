module complex_int_iq
    import rvh_pkg::*;
    import riscv_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned ENTRY_COUNT = 4
) (
    // Disp -> IQ(INT)
    input  logic                          disp_int_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] disp_int_iq_rob_tag_i,
    input  logic [                   1:0] disp_int_iq_fu_type_i,
    input  logic [      INT_OP_WIDTH-1:0] disp_int_iq_opcode_i,
    input  logic                          disp_int_iq_rs1_rdy_i,
    input  logic                          disp_int_iq_rs2_rdy_i,
    input  logic                          disp_int_iq_use_rs1_i,
    input  logic                          disp_int_iq_use_rs2_i,
    input  logic                          disp_int_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int_iq_phy_rd_i,
    input  logic [                  31:0] disp_int_iq_imm_i,
    input  logic                          disp_int_iq_use_imm_i,
    input  logic                          disp_int_iq_use_pc_i,
    output logic                          disp_int_iq_rdy_o,

    // IQ -> Fu : Issue
    output logic                          int_iq_issue_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0] int_iq_issue_rob_tag_o,
    output logic [                   1:0] int_iq_issue_fu_type_o,
    output logic [      INT_OP_WIDTH-1:0] int_iq_issue_opcode_o,
    output logic [  INT_CDB_ID_WIDTH-1:0] int_iq_issue_rs1_cdb_id_o,
    output logic [  INT_CDB_ID_WIDTH-1:0] int_iq_issue_rs2_cdb_id_o,
    output logic                          int_iq_issue_use_rs1_o,
    output logic                          int_iq_issue_use_rs2_o,
    output logic                          int_iq_issue_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] int_iq_issue_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] int_iq_issue_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] int_iq_issue_phy_rd_o,
    output logic [                  31:0] int_iq_issue_imm_o,
    output logic                          int_iq_issue_use_imm_o,
    output logic                          int_iq_issue_use_pc_o,
    input  logic                          int_iq_issue_rdy_i,

    input logic int_iq_alu_issue_rdy_i,
    input logic int_iq_mul_issue_rdy_i,
    input logic int_iq_div_issue_rdy_i,

    // Early Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0]                         wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] wakeup_ptag_i,

    // Fu -> IQ : Forwarding
    input logic [INT_CDB_BUS_WIDTH-1:0]                         forwarding_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] forwarding_ptag_i,

    input logic flush_i,

    input clk,
    input rst
);

    localparam int unsigned ENTRY_TAG_WIDTH = (ENTRY_COUNT > 1) ? $clog2(ENTRY_COUNT) : 1;


    // Dispatch 
    logic disp_vld;
    logic disp_rdy;
    logic disp_fire;
    logic [ENTRY_COUNT-1:0] disp_vld_mask;
    logic [ENTRY_TAG_WIDTH-1:0] disp_tag;

    always_comb begin : gen_disp_vld_mask
        for (int i = 0; i < ENTRY_COUNT; i++) begin
            disp_vld_mask[i] = disp_fire & (disp_tag == i);
        end
    end
    assign disp_vld = disp_int_iq_vld_i;
    assign disp_fire = disp_vld & disp_rdy;
    assign disp_int_iq_rdy_o = disp_rdy;

    // Issue Select
    logic                                                  issue_vld;
    logic                                                  issue_rdy;
    logic                                                  issue_fire;
    logic [       ENTRY_COUNT-1:0]                         issue_rdy_mask;
    logic [       ENTRY_COUNT-1:0]                         issue_vld_mask;
    logic [       ENTRY_COUNT-1:0][   ENTRY_TAG_WIDTH-1:0] issue_sel_tags;
    logic [   ENTRY_TAG_WIDTH-1:0]                         issue_tag;

    logic [       ENTRY_COUNT-1:0][     ROB_TAG_WIDTH-1:0] issue_rob_tag_mux_in;
    logic [       ENTRY_COUNT-1:0][                   1:0] issue_fu_type_mux_in;
    logic [       ENTRY_COUNT-1:0][      INT_OP_WIDTH-1:0] issue_opcode_mux_in;
    logic [       ENTRY_COUNT-1:0][  INT_CDB_ID_WIDTH-1:0] issue_rs1_cdb_id_mux_in;
    logic [       ENTRY_COUNT-1:0][  INT_CDB_ID_WIDTH-1:0] issue_rs2_cdb_id_mux_in;
    logic [       ENTRY_COUNT-1:0]                         issue_use_rs1_mux_in;
    logic [       ENTRY_COUNT-1:0]                         issue_use_rs2_mux_in;
    logic [       ENTRY_COUNT-1:0]                         issue_use_rd_mux_in;
    logic [       ENTRY_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] issue_phy_rs1_mux_in;
    logic [       ENTRY_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] issue_phy_rs2_mux_in;
    logic [       ENTRY_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_mux_in;
    logic [       ENTRY_COUNT-1:0][                  31:0] issue_imm_mux_in;
    logic [       ENTRY_COUNT-1:0]                         issue_use_imm_mux_in;
    logic [       ENTRY_COUNT-1:0]                         issue_use_pc_mux_in;

    logic [     ROB_TAG_WIDTH-1:0]                         issue_rob_tag_mux_out;
    logic [                   1:0]                         issue_fu_type_mux_out;
    logic [      INT_OP_WIDTH-1:0]                         issue_opcode_mux_out;
    logic [  INT_CDB_ID_WIDTH-1:0]                         issue_rs1_cdb_id_mux_out;
    logic [  INT_CDB_ID_WIDTH-1:0]                         issue_rs2_cdb_id_mux_out;
    logic                                                  issue_use_rs1_mux_out;
    logic                                                  issue_use_rs2_mux_out;
    logic                                                  issue_use_rd_mux_out;
    logic [INT_PREG_TAG_WIDTH-1:0]                         issue_phy_rs1_mux_out;
    logic [INT_PREG_TAG_WIDTH-1:0]                         issue_phy_rs2_mux_out;
    logic [INT_PREG_TAG_WIDTH-1:0]                         issue_phy_rd_mux_out;
    logic [                  31:0]                         issue_imm_mux_out;
    logic                                                  issue_use_imm_mux_out;
    logic                                                  issue_use_pc_mux_out;

    assign issue_vld                 = |issue_rdy_mask;
    assign issue_rdy                 = int_iq_issue_rdy_i;
    assign issue_fire                = issue_vld & issue_rdy;

    // Output 
    assign int_iq_issue_vld_o        = issue_fire;
    assign int_iq_issue_rob_tag_o    = issue_rob_tag_mux_out;
    assign int_iq_issue_fu_type_o    = issue_fu_type_mux_out;
    assign int_iq_issue_opcode_o     = issue_opcode_mux_out;
    assign int_iq_issue_rs1_cdb_id_o = issue_rs1_cdb_id_mux_out;
    assign int_iq_issue_rs2_cdb_id_o = issue_rs2_cdb_id_mux_out;
    assign int_iq_issue_use_rs1_o    = issue_use_rs1_mux_out;
    assign int_iq_issue_use_rs2_o    = issue_use_rs2_mux_out;
    assign int_iq_issue_use_rd_o     = issue_use_rd_mux_out;
    assign int_iq_issue_phy_rs1_o    = issue_phy_rs1_mux_out;
    assign int_iq_issue_phy_rs2_o    = issue_phy_rs2_mux_out;
    assign int_iq_issue_phy_rd_o     = issue_phy_rd_mux_out;
    assign int_iq_issue_imm_o        = issue_imm_mux_out;
    assign int_iq_issue_use_imm_o    = issue_use_imm_mux_out;
    assign int_iq_issue_use_pc_o     = issue_use_pc_mux_out;

    always_comb begin : gen_entry_tag
        for(int i = 0 ; i < ENTRY_COUNT; i++) begin
            issue_sel_tags[i] = i[ENTRY_TAG_WIDTH-1:0];
        end
    end

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ENTRY_TAG_WIDTH)
    ) u_issue_entry_tag_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_sel_tags),
        .data_o(issue_tag)
    );
    
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_issue_rob_tag_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_rob_tag_mux_in),
        .data_o(issue_rob_tag_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_TYPE_WIDTH)
    ) u_issue_fu_type_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_fu_type_mux_in),
        .data_o(issue_fu_type_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_OP_WIDTH)
    ) u_issue_opcode_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_opcode_mux_in),
        .data_o(issue_opcode_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_CDB_ID_WIDTH)
    ) u_issue_rs1_cdb_id_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_rs1_cdb_id_mux_in),
        .data_o(issue_rs1_cdb_id_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_CDB_ID_WIDTH)
    ) u_issue_rs2_cdb_id_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_rs2_cdb_id_mux_in),
        .data_o(issue_rs2_cdb_id_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (1)
    ) u_issue_use_rs1_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_use_rs1_mux_in),
        .data_o(issue_use_rs1_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (1)
    ) u_issue_use_rs2_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_use_rs2_mux_in),
        .data_o(issue_use_rs2_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (1)
    ) u_issue_use_rd_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_use_rd_mux_in),
        .data_o(issue_use_rd_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
    ) u_issue_phy_rs1_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_phy_rs1_mux_in),
        .data_o(issue_phy_rs1_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
    ) u_issue_phy_rs2_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_phy_rs2_mux_in),
        .data_o(issue_phy_rs2_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
    ) u_issue_phy_rd_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_phy_rd_mux_in),
        .data_o(issue_phy_rd_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (32)
    ) u_issue_imm_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_imm_mux_in),
        .data_o(issue_imm_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (1)
    ) u_issue_use_imm_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_use_imm_mux_in),
        .data_o(issue_use_imm_mux_out)
    );
    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (1)
    ) u_issue_use_pc_onehot_mux (
        .sel_i (issue_vld_mask),
        .data_i(issue_use_pc_mux_in),
        .data_o(issue_use_pc_mux_out)
    );

    freelist #(
        .ENTRY_COUNT(ENTRY_COUNT)
    ) u_freelist (
        .enq_vld_i(issue_fire),
        .enq_tag_i(issue_tag),
        .deq_vld_i(disp_vld),
        .deq_tag_o(disp_tag),
        .deq_rdy_o(disp_rdy),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    age_order_selector #(
        .ENTRY_COUNT(ENTRY_COUNT),
        .ENQ_WIDTH(1),
        .DEQ_WIDTH(1),
        .SEL_WIDTH(1)
    ) u_issue_selector (
        .enq_vld_i(disp_fire),
        .enq_tag_i(disp_tag),
        .deq_vld_i(issue_fire),
        .deq_tag_i(issue_tag),
        .vld_mask_o(),
        .sel_mask_i(issue_rdy_mask),
        .sel_oldest_mask_o(issue_vld_mask),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    generate
        for (genvar i = 0; i < ENTRY_COUNT; i++) begin
            complex_int_iq_entry u_complex_int_iq_entry (
                .disp_vld_i(disp_vld_mask[i]),
                .disp_rob_tag_i(disp_int_iq_rob_tag_i),
                .disp_fu_type_i(disp_int_iq_fu_type_i),
                .disp_opcode_i(disp_int_iq_opcode_i),
                .disp_rs1_rdy_i(disp_int_iq_rs1_rdy_i),
                .disp_rs2_rdy_i(disp_int_iq_rs2_rdy_i),
                .disp_use_rs1_i(disp_int_iq_use_rs1_i),
                .disp_use_rs2_i(disp_int_iq_use_rs2_i),
                .disp_use_rd_i(disp_int_iq_use_rd_i),
                .disp_phy_rs1_i(disp_int_iq_phy_rs1_i),
                .disp_phy_rs2_i(disp_int_iq_phy_rs2_i),
                .disp_phy_rd_i(disp_int_iq_phy_rd_i),
                .disp_imm_i(disp_int_iq_imm_i),
                .disp_use_imm_i(disp_int_iq_use_imm_i),
                .disp_use_pc_i(disp_int_iq_use_pc_i),
                .is_rdy_o(issue_rdy_mask[i]),
                .alu_is_rdy_i(int_iq_alu_issue_rdy_i),
                .mul_is_rdy_i(int_iq_mul_issue_rdy_i),
                .div_is_rdy_i(int_iq_div_issue_rdy_i),
                .issue_vld_i(issue_vld_mask[i] & issue_rdy),
                .issue_rob_tag_o(issue_rob_tag_mux_in[i]),
                .issue_fu_type_o(issue_fu_type_mux_in[i]),
                .issue_opcode_o(issue_opcode_mux_in[i]),
                .issue_rs1_cdb_id_o(issue_rs1_cdb_id_mux_in[i]),
                .issue_rs2_cdb_id_o(issue_rs2_cdb_id_mux_in[i]),
                .issue_use_rs1_o(issue_use_rs1_mux_in[i]),
                .issue_use_rs2_o(issue_use_rs2_mux_in[i]),
                .issue_use_rd_o(issue_use_rd_mux_in[i]),
                .issue_phy_rs1_o(issue_phy_rs1_mux_in[i]),
                .issue_phy_rs2_o(issue_phy_rs2_mux_in[i]),
                .issue_phy_rd_o(issue_phy_rd_mux_in[i]),
                .issue_imm_o(issue_imm_mux_in[i]),
                .issue_use_imm_o(issue_use_imm_mux_in[i]),
                .issue_use_pc_o(issue_use_pc_mux_in[i]),
                .wakeup_vld_i(wakeup_vld_i),
                .wakeup_ptag_i(wakeup_ptag_i),
                .forwarding_vld_i(forwarding_vld_i),
                .forwarding_ptag_i(forwarding_ptag_i),
                .flush_i(flush_i),
                .clk(clk),
                .rst(rst)
            );

        end
    endgenerate




endmodule : complex_int_iq
