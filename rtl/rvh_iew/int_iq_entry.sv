module int_iq_entry
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
(
    // Dispatch
    input logic                          disp_vld_i,
    input logic [     ROB_TAG_WIDTH-1:0] disp_rob_tag_i,
    input logic [      ALU_OP_WIDTH-1:0] disp_opcode_i,
    input logic                          disp_rs1_rdy_i,
    input logic                          disp_rs2_rdy_i,
    input logic                          disp_use_rs1_i,
    input logic                          disp_use_rs2_i,
    input logic                          disp_use_rd_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] disp_phy_rs1_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] disp_phy_rs2_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] disp_phy_rd_i,
    input logic                          disp_use_imm_i,
    input logic                          disp_use_pc_i,
    input logic [                  31:0] disp_imm_i,


    // Issue Select
    input  logic alu_is_rdy_i,
    output logic is_rdy_o,

    // Issue 
    input  logic                          issue_vld_i,
    output logic [     ROB_TAG_WIDTH-1:0] issue_rob_tag_o,
    output logic [      ALU_OP_WIDTH-1:0] issue_opcode_o,
    output logic [  INT_CDB_ID_WIDTH-1:0] issue_rs1_cdb_id_o,
    output logic [  INT_CDB_ID_WIDTH-1:0] issue_rs2_cdb_id_o,
    output logic                          issue_use_rs1_o,
    output logic                          issue_use_rs2_o,
    output logic                          issue_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] issue_phy_rd_o,
    output logic                          issue_use_imm_o,
    output logic                          issue_use_pc_o,
    output logic [                  31:0] issue_imm_o,

    // Early Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0]                         wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] wakeup_ptag_i,
    
    // Forward
    input logic [INT_CDB_BUS_WIDTH-1:0]                         forwarding_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] forwarding_ptag_i,

    input logic flush_i,

    input clk,
    input rst
);

    logic dispatch_clk_en;
    logic issue_clk_en;
    logic forwarding_clk_en;

    // Change when dispatch & issue
    logic vld_d, vld_q;

    // Change when dispatch & forwarding 
    //    logic [WAKEUP_TIMER_WIDTH-1:0] hold_timer_d, hold_timer_q;    //TODO

    // Change when dispatch & forwarding
    logic rs1_rdy_d, rs1_rdy_q;
    logic rs2_rdy_d, rs2_rdy_q;
    logic [INT_CDB_ID_WIDTH-1:0]rs1_cdb_id_d, rs1_cdb_id_q;
    logic [INT_CDB_ID_WIDTH-1:0]rs2_cdb_id_d, rs2_cdb_id_q;

    // Change when dispatch    
    logic [INT_PREG_TAG_WIDTH-1:0] phy_rs1_d, phy_rs1_q;
    logic [INT_PREG_TAG_WIDTH-1:0] phy_rs2_d, phy_rs2_q;
    logic use_rs1_d, use_rs1_q;
    logic use_rs2_d, use_rs2_q;
    logic use_rd_d, use_rd_q;
    logic [ROB_TAG_WIDTH-1:0] rob_tag_d, rob_tag_q;
    logic [ALU_OP_WIDTH-1:0] opcode_d, opcode_q;
    logic [INT_PREG_TAG_WIDTH-1:0] phy_rd_d, phy_rd_q;
    logic [31:0] imm_d, imm_q;
    logic use_imm_d, use_imm_q;
    logic use_pc_d, use_pc_q;


    assign is_rdy_o = vld_q & (~use_rs1_q | rs1_rdy_d) & (~use_rs2_q | rs2_rdy_d) & alu_is_rdy_i;

    assign dispatch_clk_en = disp_vld_i;
    assign issue_clk_en = issue_vld_i;
    assign forwarding_clk_en = (|forwarding_vld_i) | (|wakeup_vld_i);

    always_comb begin : vld_update_logic
        vld_d = vld_q;
        if (issue_vld_i) begin
            vld_d = 1'b0;
        end
        if (disp_vld_i) begin
            vld_d = 1'b1;
        end
        if (flush_i) begin
            vld_d = 1'b0;
        end
    end

    always_comb begin : rs_rdy_update_logic
        rs1_rdy_d = rs1_rdy_q;
        rs2_rdy_d = rs2_rdy_q;
        rs1_cdb_id_d = rs1_cdb_id_q;
        rs2_cdb_id_d = rs2_cdb_id_q;
        for (int i = 0; i < INT_CDB_BUS_WIDTH; i++) begin
            if (wakeup_vld_i[i] & (wakeup_ptag_i[i] == phy_rs1_q)) begin
                rs1_rdy_d = 1'b1;
                rs1_cdb_id_d = i[INT_CDB_ID_WIDTH-1:0];
            end
            if (wakeup_vld_i[i] & (wakeup_ptag_i[i] == phy_rs2_q)) begin
                rs2_rdy_d = 1'b1;
                rs2_cdb_id_d = i[INT_CDB_ID_WIDTH-1:0];
            end
            if (forwarding_vld_i[i] & (forwarding_ptag_i[i] == phy_rs1_q)) begin
                rs1_rdy_d = 1'b1;
                rs1_cdb_id_d = i[INT_CDB_ID_WIDTH-1:0];
            end
            if (forwarding_vld_i[i] & (forwarding_ptag_i[i] == phy_rs2_q)) begin
                rs2_rdy_d = 1'b1;
                rs2_cdb_id_d = i[INT_CDB_ID_WIDTH-1:0];
            end
        end
        if (disp_vld_i) begin
            rs1_rdy_d = disp_rs1_rdy_i;
            rs2_rdy_d = disp_rs2_rdy_i;
            rs1_cdb_id_d = {INT_CDB_ID_WIDTH{1'b0}};
            rs2_cdb_id_d = {INT_CDB_ID_WIDTH{1'b0}};
        end
    end

    always_comb begin : disp_update_logic
        phy_rs1_d = disp_phy_rs1_i;
        phy_rs2_d = disp_phy_rs2_i;
        use_rs1_d = disp_use_rs1_i;
        use_rs2_d = disp_use_rs2_i;
        use_rd_d  = disp_use_rd_i;
        rob_tag_d = disp_rob_tag_i;
        opcode_d  = disp_opcode_i;
        phy_rd_d  = disp_phy_rd_i;
        imm_d     = disp_imm_i;
        use_imm_d = disp_use_imm_i;
        use_pc_d  = disp_use_pc_i;
    end

    // Output 
    assign issue_rob_tag_o = rob_tag_q;
    assign issue_opcode_o  = opcode_q;
    assign issue_rs1_cdb_id_o = rs1_cdb_id_d;
    assign issue_rs2_cdb_id_o = rs2_cdb_id_d;
    assign issue_use_rs1_o = use_rs1_q;
    assign issue_use_rs2_o = use_rs2_q;
    assign issue_use_rd_o  = use_rd_q;
    assign issue_phy_rs1_o = phy_rs1_q;
    assign issue_phy_rs2_o = phy_rs2_q;
    assign issue_phy_rd_o  = phy_rd_q;
    assign issue_imm_o     = imm_q;
    assign issue_use_imm_o = use_imm_q;
    assign issue_use_pc_o  = use_pc_q;

    // DFF
    always_ff @(posedge clk) begin
        if (rst) begin
            vld_q <= 1'b0;
        end else begin
            if (dispatch_clk_en | issue_clk_en | flush_i) begin
                vld_q <= vld_d;
            end
        end
    end

    always_ff @(posedge clk) begin
        if (dispatch_clk_en | forwarding_clk_en) begin
            rs1_rdy_q <= rs1_rdy_d;
            rs2_rdy_q <= rs2_rdy_d;
            rs1_cdb_id_q <= rs1_cdb_id_d;
            rs2_cdb_id_q <= rs2_cdb_id_d;
        end
    end


    always_ff @(posedge clk) begin
        if (dispatch_clk_en) begin
            phy_rs1_q <= phy_rs1_d;
            phy_rs2_q <= phy_rs2_d;
            use_rs1_q <= use_rs1_d;
            use_rs2_q <= use_rs2_d;
            use_rd_q <= use_rd_d;
            rob_tag_q <= rob_tag_d;
            opcode_q <= opcode_d;
            phy_rd_q <= phy_rd_d;
            imm_q <= imm_d;
            use_imm_q <= use_imm_d;
            use_pc_q <= use_pc_d;
        end
    end

endmodule : int_iq_entry
