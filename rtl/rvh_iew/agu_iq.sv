module agu_iq
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter int unsigned ENTRY_COUNT = 4,
    localparam int unsigned ENTRY_TAG_WIDTH = (ENTRY_COUNT > 1) ? $clog2(ENTRY_COUNT) : 1
) (

    // Disp -> IQ(agu)
    input  logic                                                  disp_agu_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0]                         disp_agu_iq_rob_tag_i,
    input  logic [     LSQ_TAG_WIDTH-1:0]                         disp_agu_iq_lsq_tag_i,
    input  logic                                                  disp_agu_iq_fu_type_i,
    input  logic [      LSU_OP_WIDTH-1:0]                         disp_agu_iq_opcode_i,
    input  logic                                                  disp_agu_iq_rs1_rdy_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu_iq_phy_rd_i,
    input  logic [                  11:0]                         disp_agu_iq_imm_i,
    output logic                                                  disp_agu_iq_rdy_o,
    // IQ -> Fu : Issue
    output logic                                                  agu_iq_issue_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0]                         agu_iq_issue_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                         agu_iq_issue_lsq_tag_o,
    output logic [   ENTRY_TAG_WIDTH-1:0]                         agu_iq_issue_tag_o,
    output logic                                                  agu_iq_issue_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                         agu_iq_issue_opcode_o,
    output logic [  INT_CDB_ID_WIDTH-1:0]                         agu_iq_issue_rs1_cdb_id_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         agu_iq_issue_phy_rs1_o,
    output logic [    PREG_TAG_WIDTH-1:0]                         agu_iq_issue_phy_rd_o,
    output logic [                  11:0]                         agu_iq_issue_imm_o,
    input  logic                                                  agu_iq_issue_rdy_i,
    // LSU -> IQ : Commit
    input  logic                                                  lsu_iq_commit_vld_i,
    input  logic                                                  lsu_iq_commit_reactivate_i,
    input  logic [   ENTRY_TAG_WIDTH-1:0]                         lsu_iq_commit_tag_i,
    // Early Wakeup
    input  logic [ INT_CDB_BUS_WIDTH-1:0]                         wakeup_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] wakeup_ptag_i,
    // Fu -> IQ : Forwarding
    input  logic [ INT_CDB_BUS_WIDTH-1:0]                         forwarding_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] forwarding_ptag_i,

    input logic flush_i,

    input clk,
    input rst
);

    // Entry Status
    logic [       ENTRY_COUNT-1:0]                         entry_vld;
    logic [       ENTRY_COUNT-1:0][   ENTRY_TAG_WIDTH-1:0] entry_tag;
    logic [       ENTRY_COUNT-1:0]                         unused_entry_mask;

    // Enqueue Sel
    logic [       ENTRY_COUNT-1:0]                         enq_entry_mask;
    logic                                                  enq_fire;
    logic [   ENTRY_TAG_WIDTH-1:0]                         enq_entry_tag;

    // Issue Sel
    logic [       ENTRY_COUNT-1:0]                         entry_issue_rdy_mask;
    logic [       ENTRY_COUNT-1:0]                         entry_issue_mask;

    logic [       ENTRY_COUNT-1:0][     ROB_TAG_WIDTH-1:0] issue_rob_tag_mux_in;
    logic [       ENTRY_COUNT-1:0][     LSQ_TAG_WIDTH-1:0] issue_lsq_tag_mux_in;
    logic [       ENTRY_COUNT-1:0][   ENTRY_TAG_WIDTH-1:0] issue_iq_tag_mux_in;
    logic [       ENTRY_COUNT-1:0][    LSU_TYPE_WIDTH-1:0] issue_fu_type_mux_in;
    logic [       ENTRY_COUNT-1:0][      LSU_OP_WIDTH-1:0] issue_opcode_mux_in;
    logic [       ENTRY_COUNT-1:0][  INT_CDB_ID_WIDTH-1:0] issue_rs1_cdb_id_mux_in;
    logic [       ENTRY_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] issue_phy_rs1_mux_in;
    logic [       ENTRY_COUNT-1:0][    PREG_TAG_WIDTH-1:0] issue_phy_rd_mux_in;
    logic [       ENTRY_COUNT-1:0][                  11:0] issue_imm_mux_in;

    logic [     ROB_TAG_WIDTH-1:0]                         issue_rob_tag_mux_out;
    logic [     LSQ_TAG_WIDTH-1:0]                         issue_lsq_tag_mux_out;
    logic [   ENTRY_TAG_WIDTH-1:0]                         issue_iq_tag_mux_out;
    logic [    LSU_TYPE_WIDTH-1:0]                         issue_fu_type_mux_out;
    logic [      LSU_OP_WIDTH-1:0]                         issue_opcode_mux_out;
    logic [  INT_CDB_ID_WIDTH-1:0]                         issue_rs1_cdb_id_mux_out;
    logic [INT_PREG_TAG_WIDTH-1:0]                         issue_phy_rs1_mux_out;
    logic [    PREG_TAG_WIDTH-1:0]                         issue_phy_rd_mux_out;
    logic [                  11:0]                         issue_imm_mux_out;


    // deq
    logic                                                  deq_fire;
    logic [   ENTRY_TAG_WIDTH-1:0]                         deq_entry_tag;


    assign unused_entry_mask         = ~entry_vld;

    // Enqueue
    assign disp_agu_iq_rdy_o         = |unused_entry_mask;
    assign enq_fire                  = disp_agu_iq_vld_i & disp_agu_iq_rdy_o;

    // issue
    assign agu_iq_issue_vld_o        = |entry_issue_rdy_mask;
    assign agu_iq_issue_rob_tag_o    = issue_rob_tag_mux_out;
    assign agu_iq_issue_lsq_tag_o    = issue_lsq_tag_mux_out;
    assign agu_iq_issue_tag_o        = issue_iq_tag_mux_out;
    assign agu_iq_issue_fu_type_o    = issue_fu_type_mux_out;
    assign agu_iq_issue_opcode_o     = issue_opcode_mux_out;
    assign agu_iq_issue_rs1_cdb_id_o = issue_rs1_cdb_id_mux_out;
    assign agu_iq_issue_phy_rs1_o    = issue_phy_rs1_mux_out;
    assign agu_iq_issue_phy_rd_o     = issue_phy_rd_mux_out;
    assign agu_iq_issue_imm_o        = issue_imm_mux_out;

    // Dequeue
    assign deq_fire                  = lsu_iq_commit_vld_i & ~lsu_iq_commit_reactivate_i;
    assign deq_entry_tag             = lsu_iq_commit_tag_i;

    generate
        for (genvar i = 0; i < ENTRY_COUNT; i++) begin : gen_agu_iq_entry

            logic entry_disp_vld;
            logic entry_issue_vld;
            logic entry_commit_hit;
            logic entry_commit_vld;
            logic entry_commit_reactivate;

            assign entry_tag[i] = i[ENTRY_TAG_WIDTH-1:0];
            assign entry_disp_vld = disp_agu_iq_vld_i & enq_entry_mask[i];
            assign entry_issue_vld = entry_issue_mask[i] & agu_iq_issue_rdy_i;

            assign entry_commit_hit = i[ENTRY_TAG_WIDTH-1:0] == lsu_iq_commit_tag_i;
            assign entry_commit_vld = lsu_iq_commit_vld_i & entry_commit_hit;
            assign entry_commit_reactivate = lsu_iq_commit_reactivate_i;

            agu_iq_entry #(
                .ENTRY_TAG_WIDTH(ENTRY_TAG_WIDTH),
                .LOCAL_TAG(i)
            ) u_agu_iq_entry (
                .disp_vld_i(entry_disp_vld),
                .disp_rob_tag_i(disp_agu_iq_rob_tag_i),
                .disp_lsq_tag_i(disp_agu_iq_lsq_tag_i),
                .disp_fu_type_i(disp_agu_iq_fu_type_i),
                .disp_opcode_i(disp_agu_iq_opcode_i),
                .disp_rs1_rdy_i(disp_agu_iq_rs1_rdy_i),
                .disp_phy_rs1_i(disp_agu_iq_phy_rs1_i),
                .disp_phy_rd_i(disp_agu_iq_phy_rd_i),
                .disp_imm_i(disp_agu_iq_imm_i),
                .issue_vld_i(entry_issue_vld),
                .issue_rob_tag_o(issue_rob_tag_mux_in[i]),
                .issue_lsq_tag_o(issue_lsq_tag_mux_in[i]),
                .issue_iq_tag_o(issue_iq_tag_mux_in[i]),
                .issue_fu_type_o(issue_fu_type_mux_in[i]),
                .issue_opcode_o(issue_opcode_mux_in[i]),
                .issue_rs1_cdb_id_o(issue_rs1_cdb_id_mux_in[i]),
                .issue_phy_rs1_o(issue_phy_rs1_mux_in[i]),
                .issue_phy_rd_o(issue_phy_rd_mux_in[i]),
                .issue_imm_o(issue_imm_mux_in[i]),
                .issue_rdy_o(entry_issue_rdy_mask[i]),
                .entry_vld_i(entry_vld[i]),
                .commit_vld_i(entry_commit_vld),
                .commit_reactivate_i(entry_commit_reactivate),
                .wakeup_vld_i(wakeup_vld_i),
                .wakeup_ptag_i(wakeup_ptag_i),
                .forward_vld_i(forwarding_vld_i),
                .forward_ptag_i(forwarding_ptag_i),
                .flush_i(flush_i),
                .clk(clk),
                .rst(rst)
            );

        end
    endgenerate

    one_hot_priority_encoder #(
        .SEL_WIDTH(ENTRY_COUNT)
    ) u_enq_sel_one_hot_priority_encoder (
        .sel_i(unused_entry_mask),
        .sel_o(enq_entry_mask)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ENTRY_TAG_WIDTH)
    ) u_enq_tag_onehot_mux (
        .sel_i (enq_entry_mask),
        .data_i(entry_tag),
        .data_o(enq_entry_tag)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_issue_rob_tag_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_rob_tag_mux_in),
        .data_o(issue_rob_tag_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (LSQ_TAG_WIDTH)
    ) u_issue_lsq_tag_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_lsq_tag_mux_in),
        .data_o(issue_lsq_tag_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (ENTRY_TAG_WIDTH)
    ) u_issue_entry_tag_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_iq_tag_mux_in),
        .data_o(issue_iq_tag_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (LSU_TYPE_WIDTH)
    ) u_issue_fu_type_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_fu_type_mux_in),
        .data_o(issue_fu_type_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (LSU_OP_WIDTH)
    ) u_issue_opcode_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_opcode_mux_in),
        .data_o(issue_opcode_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_CDB_ID_WIDTH)
    ) u_issue_rs1_cdb_id_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_rs1_cdb_id_mux_in),
        .data_o(issue_rs1_cdb_id_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
    ) u_issue_phy_rs1_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_phy_rs1_mux_in),
        .data_o(issue_phy_rs1_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (PREG_TAG_WIDTH)
    ) u_issue_phy_rd_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_phy_rd_mux_in),
        .data_o(issue_phy_rd_mux_out)
    );

    onehot_mux #(
        .SOURCE_COUNT(ENTRY_COUNT),
        .DATA_WIDTH  (12)
    ) u_issue_imm_onehot_mux (
        .sel_i (entry_issue_mask),
        .data_i(issue_imm_mux_in),
        .data_o(issue_imm_mux_out)
    );

    age_order_selector #(
        .ENTRY_COUNT(ENTRY_COUNT),
        .ENQ_WIDTH  (1),
        .DEQ_WIDTH  (1),
        .SEL_WIDTH  (1)
    ) u_age_order_selector (
        .enq_vld_i(enq_fire),
        .enq_tag_i(enq_entry_tag),
        .deq_vld_i(deq_fire),
        .deq_tag_i(deq_entry_tag),
        .vld_mask_o(entry_vld),
        .sel_mask_i(entry_issue_rdy_mask),
        .sel_oldest_mask_o(entry_issue_mask),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

endmodule : agu_iq
