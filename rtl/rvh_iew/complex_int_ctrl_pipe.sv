module complex_int_ctrl_pipe
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
  import rvh_rcu_pkg::*;
#(
    parameter int unsigned IQ_ENTRY_COUNT = 4
) (
    // Disp -> IQ(INT)
    input  logic                          disp_int_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0] disp_int_iq_rob_tag_i,
    input  logic [                   1:0] disp_int_iq_fu_type_i,
    input  logic [      INT_OP_WIDTH-1:0] disp_int_iq_opcode_i,
    input  logic                          disp_int_iq_rs1_rdy_i,
    input  logic                          disp_int_iq_rs2_rdy_i,
    input  logic                          disp_int_iq_use_rs1_i,
    input  logic                          disp_int_iq_use_rs2_i,
    input  logic                          disp_int_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int_iq_phy_rd_i,
    input  logic [                  31:0] disp_int_iq_imm_i,
    input  logic                          disp_int_iq_use_imm_i,
    input  logic                          disp_int_iq_use_pc_i,
    output logic                          disp_int_iq_rdy_o,

    // IQ -> Issue Ctrl : Issue Request
    output logic                               issue_rd_prf_vld_o,
    output logic [1:0]                         issue_rd_prf_mask_o,
    output logic [1:0][INT_PREG_TAG_WIDTH-1:0] issue_rd_prf_tag_o,
    input  logic                               issue_rd_prf_rdy_i,

    // IQ -> ROB : Read Pc
    output logic [ROB_TAG_WIDTH-1:0] rob_tag_o,
    input  logic [  VADDR_WIDTH-1:0] rob_inst_pc_i,

    // PRF -> IQ : Read Operand
    input logic [1:0][XLEN-1:0] prf_data_i,

    // FU -> PRF : Writeback
    output logic wr_prf_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] wr_prf_tag_o,
    output logic [XLEN-1:0] wr_prf_data_o,
    input logic wr_prf_rdy_i,

    // FU -> ROB : Writeback
    output logic wb_rob_vld_o,
    output logic [ROB_TAG_WIDTH-1:0] wb_rob_tag_o,

    // IEW -> Forwarding : Wakeup
    output logic alu_forward_wakeup_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] alu_forward_wakeup_prf_tag_o,
    output logic mul_forward_wakeup_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] mul_forward_wakeup_prf_tag_o,
    output logic div_forward_wakeup_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] div_forward_wakeup_prf_tag_o,
    // IEW -> Forwarding : Forward Data 
    output logic alu_forward_data_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] alu_forward_data_prf_tag_o,
    output logic [XLEN-1:0] alu_forward_data_o,
    output logic mul_forward_data_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] mul_forward_data_prf_tag_o,
    output logic [XLEN-1:0] mul_forward_data_o,
    output logic div_forward_data_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] div_forward_data_prf_tag_o,
    output logic [XLEN-1:0] div_forward_data_o,

    // Fowarding Path -> IQ : Wakeup
    input logic [INT_CDB_BUS_WIDTH-1:0]                         cdb_wakeup_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_wakeup_ptag_i,


    // Fowarding Path -> IQ : Forward Ctrl
    input logic [INT_CDB_BUS_WIDTH-1:0]                         cdb_forward_ctrl_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ctrl_ptag_i,

    // Fowarding Path -> IQ : Forward Data
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0] cdb_forward_vld_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0] [INT_PREG_TAG_WIDTH-1:0] cdb_forward_ptag_i,
    input logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][XLEN-1:0] cdb_forward_data_i,


    input logic flush_i,
    input clk,
    input rst
);

  logic i1_clk_en, i1_vld, i1_rdy, i1_fire, i1_stall;
  logic i2_clk_en, i2_vld, i2_rdy, i2_fire, i2_stall;

  logic e0_stall;
  logic e0_alu_vld, e0_alu_rdy, e0_alu_fire, e0_alu_stall;
  logic e0_mul_vld, e0_mul_rdy, e0_mul_fire, e0_mul_stall;
  logic e0_div_vld, e0_div_rdy, e0_div_fire, e0_div_stall;
  logic div_issue_rdy, mul_issue_rdy, alu_issue_rdy;

  /**************** I0 *****************/  // Dispatch Insn & Enqueue

  /**************** I1 *****************/  // Issue select & Dequeue
  assign i1_rdy    = ~i2_stall;
  assign i1_fire   = i1_vld & i1_rdy;
  assign i1_stall  = i1_vld & ~i1_rdy;
  assign i1_clk_en = i1_fire;

  logic i1_vld_d, i1_vld_q;
  logic [ROB_TAG_WIDTH-1:0] i1_rob_tag_d, i1_rob_tag_q;
  logic [1:0] i1_fu_type_d, i1_fu_type_q;
  logic [INT_OP_WIDTH-1:0] i1_opcode_d, i1_opcode_q;
  logic [INT_CDB_ID_WIDTH-1:0] i1_rs1_cdb_id_d, i1_rs1_cdb_id_q;
  logic [INT_CDB_ID_WIDTH-1:0] i1_rs2_cdb_id_d, i1_rs2_cdb_id_q;
  logic i1_use_rs1_d, i1_use_rs1_q;
  logic i1_use_rs2_d, i1_use_rs2_q;
  logic i1_use_rd_d, i1_use_rd_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rs1_d, i1_phy_rs1_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rs2_d, i1_phy_rs2_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rd_d, i1_phy_rd_q;
  logic [31:0] i1_imm_d, i1_imm_q;
  logic i1_use_imm_d, i1_use_imm_q;
  logic i1_use_pc_d, i1_use_pc_q;


  assign i1_vld_d = flush_i ? 1'b0 : (i2_stall ? i1_vld_q : i1_fire);

  always_ff @(posedge clk) begin : i1_vld_dff
    if (rst) begin
      i1_vld_q <= 1'b0;
    end else begin
      i1_vld_q <= i1_vld_d;
    end
  end

  always_ff @(posedge clk) begin : i1_payload_dff
    if (i1_clk_en) begin
      i1_rob_tag_q    <= i1_rob_tag_d;
      i1_fu_type_q    <= i1_fu_type_d;
      i1_opcode_q     <= i1_opcode_d;
      i1_rs1_cdb_id_q <= i1_rs1_cdb_id_d;
      i1_rs2_cdb_id_q <= i1_rs2_cdb_id_d;
      i1_use_rs1_q    <= i1_use_rs1_d;
      i1_use_rs2_q    <= i1_use_rs2_d;
      i1_use_rd_q     <= i1_use_rd_d;
      i1_phy_rs1_q    <= i1_phy_rs1_d;
      i1_phy_rs2_q    <= i1_phy_rs2_d;
      i1_phy_rd_q     <= i1_phy_rd_d;
      i1_imm_q        <= i1_imm_d;
      i1_use_imm_q    <= i1_use_imm_d;
      i1_use_pc_q     <= i1_use_pc_d;
    end
  end


  /**************** I2 *****************/  // Read Operand

  logic rd_prf_rejected;
  assign rd_prf_rejected = issue_rd_prf_vld_o & ~issue_rd_prf_rdy_i;

  assign i2_vld    = i1_vld_q;
  assign i2_rdy    = ~e0_stall & ~rd_prf_rejected;
  assign i2_stall  = i2_vld & ~i2_rdy;
  assign i2_fire   = i2_vld & i2_rdy;
  assign i2_clk_en = i2_fire;


  logic i2_vld_d;
  logic [ROB_TAG_WIDTH-1:0] i2_rob_tag_d;
  logic [INT_OP_WIDTH-1:0] i2_opcode_d;
  logic i2_use_rd_d;
  logic [INT_PREG_TAG_WIDTH-1:0] i2_phy_rd_d;
  logic [XLEN-1:0] i2_operand0_d;
  logic [XLEN-1:0] i2_operand1_d;

  logic i2_is_alu, i2_is_mul, i2_is_div;

  logic rs1_is_zero, rs2_is_zero;
  logic [INT_CDB_DEPTH-1:0] rs1_forward_mask, rs2_forward_mask;
  logic [INT_CDB_DEPTH-1:0] rs1_forward_hit_mask, rs2_forward_hit_mask;
  logic rs1_forward_hit, rs2_forward_hit;
  logic [XLEN-1:0] rs1_forward_data, rs2_forward_data;

  generate
    for (genvar i = 0; i < INT_CDB_DEPTH; i++) begin
      assign rs1_forward_mask[i] = (i1_phy_rs1_q == cdb_forward_ptag_i[i1_rs1_cdb_id_q][i]) &
                cdb_forward_vld_i[i1_rs1_cdb_id_q][i];
      assign rs2_forward_mask[i] = (i1_phy_rs2_q == cdb_forward_ptag_i[i1_rs2_cdb_id_q][i]) &
                cdb_forward_vld_i[i1_rs2_cdb_id_q][i];
    end
  endgenerate

  assign rs1_forward_hit_mask = rs1_forward_mask & ~(rs1_forward_mask - 1'b1);
  assign rs2_forward_hit_mask = rs2_forward_mask & ~(rs2_forward_mask - 1'b1);

  assign rs1_forward_hit = |rs1_forward_mask;
  assign rs2_forward_hit = |rs2_forward_mask;

  onehot_mux #(
      .SOURCE_COUNT(INT_CDB_DEPTH),
      .DATA_WIDTH  (XLEN)
  ) u_rs1_forward_data_onehot_mux (
      .sel_i (rs1_forward_hit_mask),
      .data_i(cdb_forward_data_i[i1_rs1_cdb_id_q]),
      .data_o(rs1_forward_data)
  );

  onehot_mux #(
      .SOURCE_COUNT(INT_CDB_DEPTH),
      .DATA_WIDTH  (XLEN)
  ) rs2_forward_data_onehot_mux (
      .sel_i (rs2_forward_hit_mask),
      .data_i(cdb_forward_data_i[i1_rs2_cdb_id_q]),
      .data_o(rs2_forward_data)
  );

  assign rs1_is_zero = i1_phy_rs1_q == 0;
  assign rs2_is_zero = i1_phy_rs2_q == 0;

  assign issue_rd_prf_vld_o = i1_vld_q;
  assign issue_rd_prf_mask_o = {
    i1_use_rs2_q & ~rs2_is_zero & ~rs2_forward_hit, i1_use_rs1_q & ~rs1_is_zero & ~rs1_forward_hit
  };
  assign issue_rd_prf_tag_o = {i1_phy_rs2_q, i1_phy_rs1_q};
  assign rob_tag_o = i1_rob_tag_q;

  always_comb begin : i2_operand0_sel
    casez ({
      i1_use_rs1_q, rs1_is_zero, rs1_forward_hit, i1_use_pc_q
    })
      4'b1000: i2_operand0_d = prf_data_i[0];
      4'b11?0: i2_operand0_d = {XLEN{1'b0}};
      4'b1010: i2_operand0_d = rs1_forward_data;
      4'b0??1:
      i2_operand0_d = {{(XLEN - VADDR_WIDTH) {rob_inst_pc_i[VADDR_WIDTH-1]}}, rob_inst_pc_i};
      default: i2_operand0_d = {XLEN{1'b0}};
    endcase
  end

  always_comb begin : i2_operand1_sel
    casez ({
      i1_use_rs2_q, rs2_is_zero, rs2_forward_hit, i1_use_imm_q
    })
      4'b1000: i2_operand1_d = prf_data_i[1];
      4'b11?0: i2_operand1_d = {XLEN{1'b0}};
      4'b1010: i2_operand1_d = rs2_forward_data;
      4'b0??1: i2_operand1_d = {{XLEN - 32{i1_imm_q[31]}}, i1_imm_q};
      default: i2_operand1_d = {XLEN{1'b0}};
    endcase
  end

  assign i2_rob_tag_d = i1_rob_tag_q;
  assign i2_opcode_d = i1_opcode_q;
  assign i2_use_rd_d = i1_use_rd_q;
  assign i2_phy_rd_d = i1_phy_rd_q;

  assign i2_is_alu = i1_fu_type_q == INT_ALU_TYPE;
  assign i2_is_mul = i1_fu_type_q == INT_MUL_TYPE;
  assign i2_is_div = i1_fu_type_q == INT_DIV_TYPE;

  assign i2_vld_d = flush_i ? 1'b0 : i2_fire;

  // Fu Issue Router
  logic i2_alu_vld_q, i2_alu_vld_d;
  logic [ROB_TAG_WIDTH-1:0] i2_alu_rob_tag_q;
  logic [INT_OP_WIDTH-1:0] i2_alu_opcode_q;
  logic i2_alu_use_rd_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i2_alu_phy_rd_q;
  logic [XLEN-1:0] i2_alu_operand0_q;
  logic [XLEN-1:0] i2_alu_operand1_q;

  assign i2_alu_vld_d = e0_alu_stall ? i2_alu_vld_q : (i2_vld_d & i2_is_alu);

  always_ff @(posedge clk) begin : i2_alu_vld_dff
    if (rst) begin
      i2_alu_vld_q <= 1'b0;
    end else begin
      i2_alu_vld_q <= i2_alu_vld_d;
    end
  end

  always_ff @(posedge clk) begin : i2_alu_payload_dff
    if (i2_clk_en) begin
      i2_alu_rob_tag_q  <= i2_rob_tag_d;
      i2_alu_opcode_q   <= i2_opcode_d;
      i2_alu_use_rd_q   <= i2_use_rd_d;
      i2_alu_phy_rd_q   <= i2_phy_rd_d;
      i2_alu_operand0_q <= i2_operand0_d;
      i2_alu_operand1_q <= i2_operand1_d;
    end
  end
  // MUL
  logic i2_mul_vld_q, i2_mul_vld_d;
  logic [ROB_TAG_WIDTH-1:0] i2_mul_rob_tag_q;
  logic [MUL_OP_WIDTH-1:0] i2_mul_opcode_q;
  logic i2_mul_use_rd_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i2_mul_phy_rd_q;
  logic [XLEN-1:0] i2_mul_operand0_q;
  logic [XLEN-1:0] i2_mul_operand1_q;

  assign i2_mul_vld_d = e0_mul_stall ? i2_mul_vld_q : (i2_vld_d & i2_is_mul);

  always_ff @(posedge clk) begin : i2_mul_vld_dff
    if (rst) begin
      i2_mul_vld_q <= 1'b0;
    end else begin
      i2_mul_vld_q <= i2_mul_vld_d;
    end
  end

  always_ff @(posedge clk) begin : i2_mul_payload_dff
    if (i2_clk_en & i2_is_mul) begin
      i2_mul_rob_tag_q  <= i2_rob_tag_d;
      i2_mul_opcode_q   <= i2_opcode_d;
      i2_mul_use_rd_q   <= i2_use_rd_d;
      i2_mul_phy_rd_q   <= i2_phy_rd_d;
      i2_mul_operand0_q <= i2_operand0_d;
      i2_mul_operand1_q <= i2_operand1_d;
    end
  end

  // DIV
  logic i2_div_vld_q, i2_div_vld_d;
  logic [ROB_TAG_WIDTH-1:0] i2_div_rob_tag_q;
  logic [DIV_OP_WIDTH-1:0] i2_div_opcode_q;
  logic i2_div_use_rd_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i2_div_phy_rd_q;
  logic [XLEN-1:0] i2_div_operand0_q;
  logic [XLEN-1:0] i2_div_operand1_q;

  assign i2_div_vld_d = e0_div_stall ? i2_div_vld_q : (i2_vld_d & i2_is_div);

  always_ff @(posedge clk) begin : i2_div_vld_dff
    if (rst) begin
      i2_div_vld_q <= 1'b0;
    end else begin
      i2_div_vld_q <= i2_div_vld_d;
    end
  end

  always_ff @(posedge clk) begin : i2_div_payload_dff
    if (i2_clk_en & i2_is_div) begin
      i2_div_rob_tag_q  <= i2_rob_tag_d;
      i2_div_opcode_q   <= i2_opcode_d;
      i2_div_use_rd_q   <= i2_use_rd_d;
      i2_div_phy_rd_q   <= i2_phy_rd_d;
      i2_div_operand0_q <= i2_operand0_d;
      i2_div_operand1_q <= i2_operand1_d;
    end
  end
  /**************** E0 *****************/  // Execute & Wakeup
  assign e0_stall = e0_alu_stall | e0_mul_stall | e0_div_stall;

  logic [     ROB_TAG_WIDTH-1:0] e0_alu_rob_tag;
  logic [      INT_OP_WIDTH-1:0] e0_alu_opcode;
  logic [INT_PREG_TAG_WIDTH-1:0] e0_alu_phy_rd;
  logic [              XLEN-1:0] e0_alu_operand0;
  logic [              XLEN-1:0] e0_alu_operand1;

  assign e0_alu_rob_tag = i2_alu_rob_tag_q;
  assign e0_alu_opcode = i2_alu_opcode_q;
  assign e0_alu_phy_rd = i2_alu_phy_rd_q;
  assign e0_alu_operand0 = i2_alu_operand0_q;
  assign e0_alu_operand1 = i2_alu_operand1_q;

  assign e0_alu_vld = i2_alu_vld_q & ~flush_i;
  assign e0_alu_fire = e0_alu_vld & e0_alu_rdy;
  assign e0_alu_stall = e0_alu_vld & ~e0_alu_rdy;

  logic [     ROB_TAG_WIDTH-1:0] e0_mul_rob_tag;
  logic [      MUL_OP_WIDTH-1:0] e0_mul_opcode;
  logic [INT_PREG_TAG_WIDTH-1:0] e0_mul_phy_rd;
  logic [              XLEN-1:0] e0_mul_operand0;
  logic [              XLEN-1:0] e0_mul_operand1;

  assign e0_mul_rob_tag = i2_mul_rob_tag_q;
  assign e0_mul_opcode = i2_mul_opcode_q;
  assign e0_mul_phy_rd = i2_mul_phy_rd_q;
  assign e0_mul_operand0 = i2_mul_operand0_q;
  assign e0_mul_operand1 = i2_mul_operand1_q;

  assign e0_mul_vld = i2_mul_vld_q & ~flush_i;
  assign e0_mul_fire = e0_mul_vld & e0_mul_rdy;
  assign e0_mul_stall = e0_mul_vld & ~e0_mul_rdy;

  localparam int unsigned DIV_LATENCY = 4;
  localparam int unsigned DIV_CNT_WIDTH = $clog2(DIV_LATENCY + 1);

  logic [     ROB_TAG_WIDTH-1:0] e0_div_rob_tag;
  logic [      DIV_OP_WIDTH-1:0] e0_div_opcode;
  logic [INT_PREG_TAG_WIDTH-1:0] e0_div_phy_rd;
  logic [              XLEN-1:0] e0_div_operand0;
  logic [              XLEN-1:0] e0_div_operand1;
  logic [     DIV_CNT_WIDTH-1:0] div_complete_cnt;


  assign e0_div_rob_tag = i2_div_rob_tag_q;
  assign e0_div_opcode = i2_div_opcode_q;
  assign e0_div_phy_rd = i2_div_phy_rd_q;
  assign e0_div_operand0 = i2_div_operand0_q;
  assign e0_div_operand1 = i2_div_operand1_q;

  assign e0_div_vld = i2_div_vld_q & ~flush_i;
  assign e0_div_fire = e0_div_vld & e0_div_rdy;
  assign e0_div_stall = e0_div_vld & ~e0_div_rdy;




  /**************** W0 *****************/  // WriteBack & Forwarding
  logic                          w0_alu_vld;
  logic                          w0_alu_rdy;
  logic                          w0_alu_fire;
  logic [     ROB_TAG_WIDTH-1:0] w0_alu_rob_tag;
  logic [INT_PREG_TAG_WIDTH-1:0] w0_alu_phy_rd;
  logic [              XLEN-1:0] w0_alu_result;

  logic                          w0_mul_vld;
  logic                          w0_mul_rdy;
  logic                          w0_mul_fire;
  logic [     ROB_TAG_WIDTH-1:0] w0_mul_rob_tag;
  logic [INT_PREG_TAG_WIDTH-1:0] w0_mul_phy_rd;
  logic [              XLEN-1:0] w0_mul_result;

  logic                          w0_div_vld;
  logic                          w0_div_rdy;
  logic                          w0_div_fire;
  logic [     ROB_TAG_WIDTH-1:0] w0_div_rob_tag;
  logic [INT_PREG_TAG_WIDTH-1:0] w0_div_phy_rd;
  logic [              XLEN-1:0] w0_div_result;

  logic                          alu_inst_older_than_mul;
  logic                          mul_alu_sel_alu;
  logic w0_sel_alu, w0_sel_mul, w0_sel_div;


  assign w0_alu_rdy                 = w0_sel_alu & wr_prf_rdy_i;
  assign w0_alu_fire                = w0_alu_vld & w0_alu_rdy;

  assign alu_forward_data_vld_o     = w0_alu_vld;
  assign alu_forward_data_prf_tag_o = w0_alu_phy_rd;
  assign alu_forward_data_o         = w0_alu_result;


  assign w0_mul_rdy                 = w0_sel_mul & wr_prf_rdy_i;
  assign w0_mul_fire                = w0_mul_vld & w0_mul_rdy;

  assign mul_forward_data_vld_o     = w0_mul_vld;
  assign mul_forward_data_prf_tag_o = w0_mul_phy_rd;
  assign mul_forward_data_o         = w0_mul_result;


  assign w0_div_rdy                 = w0_sel_div & wr_prf_rdy_i;
  assign w0_div_fire                = w0_div_vld & w0_div_rdy;

  assign div_forward_data_vld_o     = w0_div_vld;
  assign div_forward_data_prf_tag_o = w0_div_phy_rd;
  assign div_forward_data_o         = w0_div_result;




  // Write back arbiter

  assign alu_inst_older_than_mul    = rob_tag_is_older(w0_alu_rob_tag, w0_mul_rob_tag);

  always_comb begin : alu_mul_arbitrary
    casez ({
      w0_alu_vld, w0_mul_vld, alu_inst_older_than_mul
    })
      3'b10?:  mul_alu_sel_alu = 1'b1;
      3'b01?:  mul_alu_sel_alu = 1'b0;
      3'b110:  mul_alu_sel_alu = 1'b0;
      3'b111:  mul_alu_sel_alu = 1'b1;
      default: mul_alu_sel_alu = 1'b1;
    endcase
  end

  assign w0_sel_alu   = ~w0_div_vld & mul_alu_sel_alu;
  assign w0_sel_mul   = ~w0_div_vld & ~mul_alu_sel_alu;
  assign w0_sel_div   = w0_div_vld;

  assign wr_prf_vld_o = w0_alu_vld | w0_mul_vld | w0_div_vld;
  assign wb_rob_vld_o = wr_prf_vld_o & wr_prf_rdy_i;

  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_wb_prf_tag_onehot_mux (
      .sel_i ({w0_sel_alu, w0_sel_mul, w0_sel_div}),
      .data_i({w0_alu_phy_rd, w0_mul_phy_rd, w0_div_phy_rd}),
      .data_o(wr_prf_tag_o)
  );
  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (XLEN)
  ) u_wb_data_onehot_mux (
      .sel_i ({w0_sel_alu, w0_sel_mul, w0_sel_div}),
      .data_i({w0_alu_result, w0_mul_result, w0_div_result}),
      .data_o(wr_prf_data_o)
  );
  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (ROB_TAG_WIDTH)
  ) u_wb_rob_tag_onehot_mux (
      .sel_i ({w0_sel_alu, w0_sel_mul, w0_sel_div}),
      .data_i({w0_alu_rob_tag, w0_mul_rob_tag, w0_div_rob_tag}),
      .data_o(wb_rob_tag_o)
  );

  // Back Pressure
  logic div_completing_in_2nd_cycle;
  logic div_completing_in_3rd_cycle;
  logic mul_completing_in_2nd_cycle;

  assign div_completing_in_3rd_cycle = (~e0_div_rdy & (div_complete_cnt == 3));
  assign div_completing_in_2nd_cycle = (~e0_div_rdy & (div_complete_cnt == 2));
  assign mul_completing_in_2nd_cycle = (i2_fire & i2_is_mul);

  assign alu_issue_rdy = e0_alu_rdy & ~mul_completing_in_2nd_cycle & ~div_completing_in_2nd_cycle;
  assign mul_issue_rdy = e0_mul_rdy & ~div_completing_in_3rd_cycle;
  assign div_issue_rdy = (e0_div_rdy) & (~(i1_vld_q & i2_is_div)) & (~i2_div_vld_q);

  // Early Wakeup
  logic alu_completing_in_1nd_cycle;
  logic mul_completing_in_1nd_cycle;
  logic div_completing_in_1nd_cycle;

  assign alu_completing_in_1nd_cycle = (i2_fire & i2_is_alu);
  assign mul_completing_in_1nd_cycle = e0_mul_fire;
  assign div_completing_in_1nd_cycle = (~e0_div_rdy & (div_complete_cnt == 1));

  assign alu_forward_wakeup_vld_o = alu_completing_in_1nd_cycle;
  assign alu_forward_wakeup_prf_tag_o = i1_phy_rd_q;

  assign mul_forward_wakeup_vld_o = mul_completing_in_1nd_cycle;
  assign mul_forward_wakeup_prf_tag_o = e0_mul_phy_rd;

  assign div_forward_wakeup_vld_o = div_completing_in_1nd_cycle;
  assign div_forward_wakeup_prf_tag_o = w0_div_phy_rd;

  complex_int_iq #(
      .ENTRY_COUNT(IQ_ENTRY_COUNT)
  ) u_complex_int_iq (
      .disp_int_iq_vld_i(disp_int_iq_vld_i),
      .disp_int_iq_rob_tag_i(disp_int_iq_rob_tag_i),
      .disp_int_iq_fu_type_i(disp_int_iq_fu_type_i),
      .disp_int_iq_opcode_i(disp_int_iq_opcode_i),
      .disp_int_iq_rs1_rdy_i(disp_int_iq_rs1_rdy_i),
      .disp_int_iq_rs2_rdy_i(disp_int_iq_rs2_rdy_i),
      .disp_int_iq_use_rs1_i(disp_int_iq_use_rs1_i),
      .disp_int_iq_use_rs2_i(disp_int_iq_use_rs2_i),
      .disp_int_iq_use_rd_i(disp_int_iq_use_rd_i),
      .disp_int_iq_phy_rs1_i(disp_int_iq_phy_rs1_i),
      .disp_int_iq_phy_rs2_i(disp_int_iq_phy_rs2_i),
      .disp_int_iq_phy_rd_i(disp_int_iq_phy_rd_i),
      .disp_int_iq_imm_i(disp_int_iq_imm_i),
      .disp_int_iq_use_imm_i(disp_int_iq_use_imm_i),
      .disp_int_iq_use_pc_i(disp_int_iq_use_pc_i),
      .disp_int_iq_rdy_o(disp_int_iq_rdy_o),
      .int_iq_issue_vld_o(i1_vld),
      .int_iq_issue_rob_tag_o(i1_rob_tag_d),
      .int_iq_issue_fu_type_o(i1_fu_type_d),
      .int_iq_issue_opcode_o(i1_opcode_d),
      .int_iq_issue_rs1_cdb_id_o(i1_rs1_cdb_id_d),
      .int_iq_issue_rs2_cdb_id_o(i1_rs2_cdb_id_d),
      .int_iq_issue_use_rs1_o(i1_use_rs1_d),
      .int_iq_issue_use_rs2_o(i1_use_rs2_d),
      .int_iq_issue_use_rd_o(i1_use_rd_d),
      .int_iq_issue_phy_rs1_o(i1_phy_rs1_d),
      .int_iq_issue_phy_rs2_o(i1_phy_rs2_d),
      .int_iq_issue_phy_rd_o(i1_phy_rd_d),
      .int_iq_issue_imm_o(i1_imm_d),
      .int_iq_issue_use_imm_o(i1_use_imm_d),
      .int_iq_issue_use_pc_o(i1_use_pc_d),
      .int_iq_alu_issue_rdy_i(alu_issue_rdy),
      .int_iq_mul_issue_rdy_i(mul_issue_rdy),
      .int_iq_div_issue_rdy_i(div_issue_rdy),
      .int_iq_issue_rdy_i(i1_rdy),
      .wakeup_vld_i(cdb_wakeup_vld_i),
      .wakeup_ptag_i(cdb_wakeup_ptag_i),
      .forwarding_vld_i(cdb_forward_ctrl_vld_i),
      .forwarding_ptag_i(cdb_forward_ctrl_ptag_i),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_alu u_rvh_alu (
      .issue_vld_i(e0_alu_vld),
      .issue_rob_tag_i(e0_alu_rob_tag),
      .issue_phy_rd_i(e0_alu_phy_rd),
      .issue_opcode_i(e0_alu_opcode),
      .issue_operand0_i(e0_alu_operand0),
      .issue_operand1_i(e0_alu_operand1),
      .issue_rdy_o(e0_alu_rdy),
      .wb_vld_o(w0_alu_vld),
      .wb_rob_tag_o(w0_alu_rob_tag),
      .wb_phy_rd_o(w0_alu_phy_rd),
      .wb_data_o(w0_alu_result),
      .wb_rdy_i(w0_alu_rdy),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_mul u_rvh_mul (
      .issue_vld_i(e0_mul_vld),
      .issue_rob_tag_i(e0_mul_rob_tag),
      .issue_phy_rd_i(e0_mul_phy_rd),
      .issue_opcode_i(e0_mul_opcode),
      .issue_operand0_i(e0_mul_operand0),
      .issue_operand1_i(e0_mul_operand1),
      .issue_rdy_o(e0_mul_rdy),
      .wb_vld_o(w0_mul_vld),
      .wb_rob_tag_o(w0_mul_rob_tag),
      .wb_phy_rd_o(w0_mul_phy_rd),
      .wb_data_o(w0_mul_result),
      .wb_rdy_i(w0_mul_rdy),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  rvh_div #(
      .LATENCY(DIV_LATENCY)
  ) u_rvh_div (
      .issue_vld_i(e0_div_vld),
      .issue_rob_tag_i(e0_div_rob_tag),
      .issue_phy_rd_i(e0_div_phy_rd),
      .issue_opcode_i(e0_div_opcode),
      .issue_operand0_i(e0_div_operand0),
      .issue_operand1_i(e0_div_operand1),
      .issue_rdy_o(e0_div_rdy),
      .wb_vld_o(w0_div_vld),
      .wb_rob_tag_o(w0_div_rob_tag),
      .wb_phy_rd_o(w0_div_phy_rd),
      .wb_data_o(w0_div_result),
      .wb_rdy_i(w0_div_rdy),
      .complete_cnt_o(div_complete_cnt),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

endmodule : complex_int_ctrl_pipe
