module agu_ctrl_pipe
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
#(
    parameter  int unsigned IQ_ENTRY_COUNT = 4,
    localparam int unsigned IQ_ENTRY_TAG   = $clog2(IQ_ENTRY_COUNT)
) (
    // Disp -> IQ(AGU)
    input  logic                                                  disp_agu_iq_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0]                         disp_agu_iq_rob_tag_i,
    input  logic [     LSQ_TAG_WIDTH-1:0]                         disp_agu_iq_lsq_tag_i,
    input  logic                                                  disp_agu_iq_fu_type_i,
    input  logic [      LSU_OP_WIDTH-1:0]                         disp_agu_iq_opcode_i,
    input  logic                                                  disp_agu_iq_rs1_rdy_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu_iq_phy_rd_i,
    input  logic [                  11:0]                         disp_agu_iq_imm_i,
    output logic                                                  disp_agu_iq_rdy_o,
    // IQ -> Issue Ctrl : Issue Request
    output logic                                                  issue_rd_prf_vld_o,
    output logic                                                  issue_rd_prf_mask_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         issue_rd_prf_tag_o,
    input  logic                                                  issue_rd_prf_rdy_i,
    // PRF -> IQ : Read Operand
    input  logic [              XLEN-1:0]                         prf_data_i,
    // AGU -> LSU : Issue Address
    output logic                                                  agu_iq_issue_vld_o,
    output logic                                                  agu_iq_issue_excp_vld_o,
    output logic [     ROB_TAG_WIDTH-1:0]                         agu_iq_issue_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                         agu_iq_issue_lsq_tag_o,
    output logic [      IQ_ENTRY_TAG-1:0]                         agu_iq_issue_iq_tag_o,
    output logic                                                  agu_iq_issue_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                         agu_iq_issue_opcode_o,
    output logic [       VADDR_WIDTH-1:0]                         agu_iq_issue_vaddr_o,
    output logic [    PREG_TAG_WIDTH-1:0]                         agu_iq_issue_phy_rd_o,
    input  logic                                                  agu_iq_issue_rdy_i,
    // LSU -> IQ : Commit
    input  logic                                                  lsu_iq_commit_vld_i,
    input  logic                                                  lsu_iq_commit_reactivate_i,
    input  logic [      IQ_ENTRY_TAG-1:0]                         lsu_iq_commit_tag_i,
    // Fowarding Path -> IQ : Wakeup
    input  logic [ INT_CDB_BUS_WIDTH-1:0]                         cdb_wakeup_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_wakeup_ptag_i,
    // Fowarding Path -> IQ : Forward Ctrl
    input  logic [ INT_CDB_BUS_WIDTH-1:0]                         cdb_forward_ctrl_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ctrl_ptag_i,
    // Fowarding Path -> IQ : Forward Data
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0]                         cdb_forward_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_forward_ptag_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][              XLEN-1:0] cdb_forward_data_i,



    input logic flush_i,
    input clk,
    input rst
);
    localparam bit LD_TYPE = 1'b0;
    localparam bit ST_TYPE = 1'b1;


    logic i1_clk_en, i1_vld, i1_rdy, i1_fire, i1_stall;
    logic i2_clk_en, i2_vld, i2_rdy, i2_fire, i2_stall;
    logic e0_vld, e0_rdy, e0_fire, e0_stall;

    /**************** I0 *****************/  // Dispatch Insn & Enqueue

    /**************** I1 *****************/  // Issue select & Dequeue
    assign i1_rdy    = ~i2_stall;
    assign i1_fire   = i1_vld & i1_rdy;
    assign i1_stall  = i1_vld & ~i1_rdy;
    assign i1_clk_en = i1_fire;

    logic i1_vld_d, i1_vld_q;
    logic [ROB_TAG_WIDTH-1:0] i1_rob_tag_d, i1_rob_tag_q;
    logic [LSQ_TAG_WIDTH-1:0] i1_lsq_tag_d, i1_lsq_tag_q;
    logic [IQ_ENTRY_TAG-1:0] i1_iq_tag_d, i1_iq_tag_q;
    logic i1_fu_type_d, i1_fu_type_q;
    logic [LSU_OP_WIDTH-1:0] i1_opcode_d, i1_opcode_q;
    logic [INT_CDB_ID_WIDTH-1:0] i1_rs1_cdb_id_d, i1_rs1_cdb_id_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rs1_d, i1_phy_rs1_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rd_d, i1_phy_rd_q;
    logic [11:0] i1_imm_d, i1_imm_q;

    assign i1_vld_d = flush_i ? 1'b0 : (i2_stall ? i1_vld_q : i1_fire);

    always_ff @(posedge clk) begin : i1_vld_dff
        if (rst) begin
            i1_vld_q <= 1'b0;
        end else begin
            i1_vld_q <= i1_vld_d;
        end
    end

    always_ff @(posedge clk) begin : i1_payload_dff
        if (i1_clk_en) begin
            i1_rob_tag_q    <= i1_rob_tag_d;
            i1_lsq_tag_q    <= i1_lsq_tag_d;
            i1_iq_tag_q     <= i1_iq_tag_d;
            i1_fu_type_q    <= i1_fu_type_d;
            i1_opcode_q     <= i1_opcode_d;
            i1_rs1_cdb_id_q <= i1_rs1_cdb_id_d;
            i1_phy_rs1_q    <= i1_phy_rs1_d;
            i1_phy_rd_q     <= i1_phy_rd_d;
            i1_imm_q        <= i1_imm_d;
        end
    end

    /**************** I2 *****************/  // Read Operand

    logic rd_prf_rejected;
    logic agu_rdy;
    assign rd_prf_rejected = issue_rd_prf_vld_o & ~issue_rd_prf_rdy_i;

    assign i2_vld    = i1_vld_q;
    assign i2_rdy    = ~e0_stall & ~rd_prf_rejected & agu_rdy;
    assign i2_stall  = i2_vld & ~i2_rdy;
    assign i2_fire   = i2_vld & i2_rdy;
    assign i2_clk_en = i2_fire;

    logic i2_vld_d, i2_vld_q;
    logic [ROB_TAG_WIDTH-1:0] i2_rob_tag_d, i2_rob_tag_q;
    logic [LSQ_TAG_WIDTH-1:0] i2_lsq_tag_d, i2_lsq_tag_q;
    logic [IQ_ENTRY_TAG-1:0] i2_iq_tag_d, i2_iq_tag_q;
    logic [LSU_TYPE_WIDTH-1:0] i2_fu_type_d, i2_fu_type_q;
    logic [LSU_OP_WIDTH-1:0] i2_opcode_d, i2_opcode_q;
    logic [INT_PREG_TAG_WIDTH-1:0] i2_phy_rd_d, i2_phy_rd_q;
    logic [XLEN-1:0] i2_operand0_d, i2_operand0_q;
    logic [11:0] i2_imm_d, i2_imm_q;

    logic rs1_is_zero;
    logic [INT_CDB_DEPTH-1:0] rs1_forward_mask;
    logic [INT_CDB_DEPTH-1:0] rs1_forward_hit_mask;
    logic rs1_forward_hit;
    logic [XLEN-1:0] rs1_forward_data;
  
    generate
      for (genvar i = 0; i < INT_CDB_DEPTH; i++) begin
        assign rs1_forward_mask[i] = (i1_phy_rs1_q == cdb_forward_ptag_i[i1_rs1_cdb_id_q][i]) &
                  cdb_forward_vld_i[i1_rs1_cdb_id_q][i];
      end
    endgenerate
  
    assign rs1_forward_hit_mask = rs1_forward_mask & ~(rs1_forward_mask - 1'b1);
  
    assign rs1_forward_hit = |rs1_forward_mask;
  
    onehot_mux #(
        .SOURCE_COUNT(INT_CDB_DEPTH),
        .DATA_WIDTH  (XLEN)
    ) u_rs1_forward_data_onehot_mux (
        .sel_i (rs1_forward_hit_mask),
        .data_i(cdb_forward_data_i[i1_rs1_cdb_id_q]),
        .data_o(rs1_forward_data)
    );
  
    assign rs1_is_zero = i1_phy_rs1_q == 0;


    assign issue_rd_prf_vld_o = i1_vld_q;
    assign issue_rd_prf_mask_o = ~rs1_is_zero & ~rs1_forward_hit;
    assign issue_rd_prf_tag_o = i1_phy_rs1_q;

    always_comb begin : i2_operand0_sel
        casez ({
            rs1_is_zero, rs1_forward_hit
        })
            2'b00:   i2_operand0_d = prf_data_i;
            2'b1?:   i2_operand0_d = {XLEN{1'b0}};
            2'b01:   i2_operand0_d = rs1_forward_data;
            default: i2_operand0_d = {XLEN{1'b0}};
        endcase
    end

    assign i2_imm_d     = {{XLEN - 32{1'b0}}, i1_imm_q};
    assign i2_rob_tag_d = i1_rob_tag_q;
    assign i2_lsq_tag_d = i1_lsq_tag_q;
    assign i2_iq_tag_d  = i1_iq_tag_q;
    assign i2_fu_type_d = i1_fu_type_q;
    assign i2_opcode_d  = i1_opcode_q;
    assign i2_phy_rd_d  = i1_phy_rd_q;

    assign i2_vld_d     = flush_i ? 1'b0 : (e0_stall ? i2_vld_q : i2_fire);

    always_ff @(posedge clk) begin : i2_vld_dff
        if (rst) begin
            i2_vld_q <= 1'b0;
        end else begin
            i2_vld_q <= i2_vld_d;
        end
    end

    always_ff @(posedge clk) begin : i2_payload_dff
        if (i2_clk_en) begin
            i2_rob_tag_q  <= i2_rob_tag_d;
            i2_lsq_tag_q  <= i2_lsq_tag_d;
            i2_iq_tag_q   <= i2_iq_tag_d;
            i2_fu_type_q  <= i2_fu_type_d;
            i2_opcode_q   <= i2_opcode_d;
            i2_phy_rd_q   <= i2_phy_rd_d;
            i2_operand0_q <= i2_operand0_d;
            i2_imm_q      <= i2_imm_d;
        end
    end

    // Output
    assign agu_iq_issue_vld_o = e0_vld;
    assign agu_iq_issue_rob_tag_o = i2_rob_tag_q;
    assign agu_iq_issue_lsq_tag_o = i2_lsq_tag_q;
    assign agu_iq_issue_iq_tag_o = i2_iq_tag_q;
    assign agu_iq_issue_fu_type_o = i2_fu_type_q;
    assign agu_iq_issue_opcode_o = i2_opcode_q;
    assign agu_iq_issue_phy_rd_o = i2_phy_rd_q;
    assign e0_rdy = agu_iq_issue_rdy_i;

    assign e0_fire = e0_vld & e0_rdy;
    assign e0_stall = e0_vld & ~e0_rdy;

    rvh_agu u_rvh_agu (
        .issue_vld_i(i2_vld_q),
        .issue_fu_type_i(i2_fu_type_q),
        .issue_opcode_i(i2_opcode_q),
        .issue_operand0_i(i2_operand0_q),
        .issue_imm_i(i2_imm_q),
        .issue_rdy_o(agu_rdy),
        .wb_vld_o(e0_vld),
        .wb_address_o(agu_iq_issue_vaddr_o),
        .wb_excp_vld_o(agu_iq_issue_excp_vld_o),
        .wb_rdy_i(e0_rdy),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    agu_iq #(
        .ENTRY_COUNT(IQ_ENTRY_COUNT)
    ) u_agu_iq (
        .disp_agu_iq_vld_i(disp_agu_iq_vld_i),
        .disp_agu_iq_rob_tag_i(disp_agu_iq_rob_tag_i),
        .disp_agu_iq_lsq_tag_i(disp_agu_iq_lsq_tag_i),
        .disp_agu_iq_fu_type_i(disp_agu_iq_fu_type_i),
        .disp_agu_iq_opcode_i(disp_agu_iq_opcode_i),
        .disp_agu_iq_rs1_rdy_i(disp_agu_iq_rs1_rdy_i),
        .disp_agu_iq_phy_rs1_i(disp_agu_iq_phy_rs1_i),
        .disp_agu_iq_phy_rd_i(disp_agu_iq_phy_rd_i),
        .disp_agu_iq_imm_i(disp_agu_iq_imm_i),
        .disp_agu_iq_rdy_o(disp_agu_iq_rdy_o),
        .agu_iq_issue_vld_o(i1_vld),
        .agu_iq_issue_rob_tag_o(i1_rob_tag_d),
        .agu_iq_issue_lsq_tag_o(i1_lsq_tag_d),
        .agu_iq_issue_tag_o(i1_iq_tag_d),
        .agu_iq_issue_fu_type_o(i1_fu_type_d),
        .agu_iq_issue_opcode_o(i1_opcode_d),
        .agu_iq_issue_rs1_cdb_id_o(i1_rs1_cdb_id_d),
        .agu_iq_issue_phy_rs1_o(i1_phy_rs1_d),
        .agu_iq_issue_phy_rd_o(i1_phy_rd_d),
        .agu_iq_issue_imm_o(i1_imm_d),
        .agu_iq_issue_rdy_i(i1_rdy),
        .lsu_iq_commit_vld_i(lsu_iq_commit_vld_i),
        .lsu_iq_commit_reactivate_i(lsu_iq_commit_reactivate_i),
        .lsu_iq_commit_tag_i(lsu_iq_commit_tag_i),
        .wakeup_vld_i(cdb_wakeup_vld_i),
        .wakeup_ptag_i(cdb_wakeup_ptag_i),
        .forwarding_vld_i(cdb_forward_ctrl_vld_i),
        .forwarding_ptag_i(cdb_forward_ctrl_ptag_i),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

endmodule : agu_ctrl_pipe
