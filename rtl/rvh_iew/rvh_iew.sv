module rvh_iew
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
(
    input  logic [          XLEN-1:0] hart_id_i,
    input  logic [   VADDR_WIDTH-1:0] boot_address_i,
    // CSR Status
    output logic [PRIV_LVL_WIDTH-1:0] priv_lvl_o,
    output logic                      tvm_o,
    output logic                      tw_o,
    output logic                      tsr_o,
    output logic [          XLEN-1:0] mstatus_o,
    output logic [          XLEN-1:0] satp_o,
    // Float point
    output logic [               1:0] fs_o,
    output logic [               4:0] fflags_o,
    output logic [               2:0] frm_o,
    output logic [               6:0] fprec_o,

    /* Rename Port */
    input logic [INT_PRF_ALLOC_PORT_COUNT-1:0] rn_int_prf_alloc_vld_i,
    input logic [INT_PRF_ALLOC_PORT_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_prf_alloc_tag_i,

    /* Dispatch Port */
    // Rename -> CSR Issue Register
    input logic                          rn_csr_ir_disp_vld_i,
    input logic [       VADDR_WIDTH-1:0] rn_csr_ir_disp_pc_i,
    input logic [     ROB_TAG_WIDTH-1:0] rn_csr_ir_disp_rob_tag_i,
    input logic                          rn_csr_ir_disp_use_imm_i,
    input logic [                   4:0] rn_csr_ir_disp_imm_i,
    input logic [      CSR_OP_WIDTH-1:0] rn_csr_ir_disp_minor_op_i,
    input logic                          rn_csr_ir_disp_use_rs1_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] rn_csr_ir_disp_phy_rs1_i,
    input logic                          rn_csr_ir_disp_use_rs2_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] rn_csr_ir_disp_phy_rs2_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] rn_csr_ir_disp_phy_rd_i,
    input logic [                  11:0] rn_csr_ir_disp_csr_addr_i,

    // Disp -> IQ(INT0)
    input  logic                          disp_int0_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_int0_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_int0_iq_rob_tag_i,
    input  logic [      ALU_OP_WIDTH-1:0] disp_int0_iq_opcode_i,
    input  logic                          disp_int0_iq_use_rs1_i,
    input  logic                          disp_int0_iq_use_rs2_i,
    input  logic                          disp_int0_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int0_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int0_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int0_iq_phy_rd_i,
    input  logic [                  31:0] disp_int0_iq_imm_i,
    input  logic                          disp_int0_iq_use_imm_i,
    input  logic                          disp_int0_iq_use_pc_i,
    output logic                          disp_int0_iq_rdy_o,
    // Disp -> IQ(INT1)
    input  logic                          disp_int1_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_int1_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_int1_iq_rob_tag_i,
    input  logic [      ALU_OP_WIDTH-1:0] disp_int1_iq_opcode_i,
    input  logic                          disp_int1_iq_use_rs1_i,
    input  logic                          disp_int1_iq_use_rs2_i,
    input  logic                          disp_int1_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int1_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int1_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_int1_iq_phy_rd_i,
    input  logic [                  31:0] disp_int1_iq_imm_i,
    input  logic                          disp_int1_iq_use_imm_i,
    input  logic                          disp_int1_iq_use_pc_i,
    output logic                          disp_int1_iq_rdy_o,
    // Disp -> IQ(Complex Int0)
    input  logic                          disp_complex_int0_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_complex_int0_iq_pc_i,
`endif
    input  logic [                   1:0] disp_complex_int0_iq_fu_type_i,
    input  logic [     ROB_TAG_WIDTH-1:0] disp_complex_int0_iq_rob_tag_i,
    input  logic [      ALU_OP_WIDTH-1:0] disp_complex_int0_iq_opcode_i,
    input  logic                          disp_complex_int0_iq_use_rs1_i,
    input  logic                          disp_complex_int0_iq_use_rs2_i,
    input  logic                          disp_complex_int0_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_complex_int0_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_complex_int0_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_complex_int0_iq_phy_rd_i,
    input  logic [                  31:0] disp_complex_int0_iq_imm_i,
    input  logic                          disp_complex_int0_iq_use_imm_i,
    input  logic                          disp_complex_int0_iq_use_pc_i,
    output logic                          disp_complex_int0_iq_rdy_o,
    // Disp -> IQ(BRU)
    input  logic                          disp_br0_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_br0_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_br0_iq_rob_tag_i,
    input  logic [     BRQ_TAG_WIDTH-1:0] disp_br0_iq_brq_tag_i,
    input  logic [      BRU_OP_WIDTH-1:0] disp_br0_iq_opcode_i,
    input  logic                          disp_br0_iq_use_rs1_i,
    input  logic                          disp_br0_iq_use_rs2_i,
    input  logic                          disp_br0_iq_use_rd_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_br0_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_br0_iq_phy_rs2_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_br0_iq_phy_rd_i,
    input  logic [                  31:0] disp_br0_iq_imm_i,
    output logic                          disp_br0_iq_rdy_o,
    // Disp -> IQ(AGU)
    input  logic                          disp_agu0_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_agu0_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_agu0_iq_rob_tag_i,
    input  logic [     LSQ_TAG_WIDTH-1:0] disp_agu0_iq_lsq_tag_i,
    input  logic                          disp_agu0_iq_fu_type_i,
    input  logic [      LSU_OP_WIDTH-1:0] disp_agu0_iq_opcode_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu0_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu0_iq_phy_rd_i,
    input  logic [                  11:0] disp_agu0_iq_imm_i,
    output logic                          disp_agu0_iq_rdy_o,
    // Disp -> IQ(AGU)
    input  logic                          disp_agu1_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_agu1_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_agu1_iq_rob_tag_i,
    input  logic [     LSQ_TAG_WIDTH-1:0] disp_agu1_iq_lsq_tag_i,
    input  logic                          disp_agu1_iq_fu_type_i,
    input  logic [      LSU_OP_WIDTH-1:0] disp_agu1_iq_opcode_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu1_iq_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_agu1_iq_phy_rd_i,
    input  logic [                  11:0] disp_agu1_iq_imm_i,
    output logic                          disp_agu1_iq_rdy_o,
    // Disp -> IQ(SDU)
    input  logic                          disp_sdu0_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_sdu0_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_sdu0_iq_rob_tag_i,
    input  logic [     STQ_TAG_WIDTH-1:0] disp_sdu0_iq_stq_tag_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_sdu0_iq_phy_rs2_i,
    output logic                          disp_sdu0_iq_rdy_o,
    // Disp -> IQ(SDU)
    input  logic                          disp_sdu1_iq_vld_i,
`ifdef SIMULATION
    input  logic [       VADDR_WIDTH-1:0] disp_sdu1_iq_pc_i,
`endif
    input  logic [     ROB_TAG_WIDTH-1:0] disp_sdu1_iq_rob_tag_i,
    input  logic [     STQ_TAG_WIDTH-1:0] disp_sdu1_iq_stq_tag_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0] disp_sdu1_iq_phy_rs2_i,
    output logic                          disp_sdu1_iq_rdy_o,

    /* Issue */
    output logic [ALU_COUNT-1:0][ROB_TAG_WIDTH-1:0] alu_issue_rob_tag_o,
    input  logic [ALU_COUNT-1:0][  VADDR_WIDTH-1:0] alu_issue_pc_i,

    output logic [BRU_COUNT-1:0][ROB_TAG_WIDTH-1:0] bru_issue_rob_tag_o,
    output logic [BRU_COUNT-1:0][BRQ_TAG_WIDTH-1:0] bru_issue_brq_tag_o,
    input  logic [BRU_COUNT-1:0][  VADDR_WIDTH-1:0] bru_issue_pc_i,
    input  logic [BRU_COUNT-1:0]                    bru_issue_is_rvc_i,
    input  logic [BRU_COUNT-1:0][BTQ_TAG_WIDTH-1:0] bru_issue_btq_tag_i,
    input  logic [BRU_COUNT-1:0]                    bru_issue_pred_taken_i,
    input  logic [BRU_COUNT-1:0][  VADDR_WIDTH-1:0] bru_issue_pred_target_i,


    // AGU -> LSU
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       agu_lsu_issue_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       agu_lsu_issue_excp_vld_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0]                       agu_lsu_issue_fu_type_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][    LSU_OP_WIDTH-1:0] agu_lsu_issue_opcode_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][   ROB_TAG_WIDTH-1:0] agu_lsu_issue_rob_tag_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][   LSQ_TAG_WIDTH-1:0] agu_lsu_issue_lsq_tag_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][AGU_IQ_TAG_WIDHT-1:0] agu_lsu_issue_iq_tag_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][     VADDR_WIDTH-1:0] agu_lsu_issue_vaddr_o,
    output logic [LSU_ADDR_PIPE_COUNT-1:0][  PREG_TAG_WIDTH-1:0] agu_lsu_issue_phy_rd_o,
    input  logic [LSU_ADDR_PIPE_COUNT-1:0]                       agu_lsu_issue_rdy_i,

    // SDU -> LSU(SDQ)
    output logic [LSU_DATA_PIPE_COUNT-1:0]                    sdu_lsu_issue_vld_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][STQ_TAG_WIDTH-1:0] sdu_lsu_issue_stq_tag_o,
    output logic [LSU_DATA_PIPE_COUNT-1:0][         XLEN-1:0] sdu_lsu_issue_data_o,
    input  logic [LSU_DATA_PIPE_COUNT-1:0]                    sdu_lsu_issue_rdy_i,


    // LSU -> IQ : Commit
    input logic [LSU_ADDR_PIPE_COUNT-1:0]                       lsu_iq_commit_vld_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0]                       lsu_iq_commit_reactivate_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][AGU_IQ_TAG_WIDHT-1:0] lsu_iq_commit_tag_i,

    // LSU -> IQ
    input logic [LSU_ADDR_PIPE_COUNT-1:0] lsu_cdb_wakeup_vld_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] lsu_cdb_wakeup_ptag_i,

    // LSU -> PRF
    input logic [LSU_ADDR_PIPE_COUNT-1:0] lsu_prf_wr_vld_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] lsu_prf_wr_ptag_i,
    input logic [LSU_ADDR_PIPE_COUNT-1:0][XLEN-1:0] lsu_prf_wr_data_i,

    /* Write Back */
    output logic [INT_COMPLETE_WIDTH-1:0] rob_complete_vld_o,
    output logic [INT_COMPLETE_WIDTH-1:0][ROB_TAG_WIDTH-1:0] rob_complete_tag_o,

    /* Bru Resolve Boardcast */
    output logic [BRU_COUNT-1:0]                    bru_resolve_vld_o,
    output logic [BRU_COUNT-1:0][  VADDR_WIDTH-1:0] bru_resolve_pc_o,
    output logic [BRU_COUNT-1:0]                    bru_resolve_taken_o,
    output logic [BRU_COUNT-1:0]                    bru_resolve_mispred_o,
    output logic [BRU_COUNT-1:0][ROB_TAG_WIDTH-1:0] bru_resolve_rob_tag_o,
    output logic [BRU_COUNT-1:0][BRQ_TAG_WIDTH-1:0] bru_resolve_brq_tag_o,
    output logic [BRU_COUNT-1:0][BTQ_TAG_WIDTH-1:0] bru_resolve_btq_tag_o,
    output logic [BRU_COUNT-1:0][  VADDR_WIDTH-1:0] bru_resolve_target_o,

    // Commit Port
    input  logic [    RETIRE_WIDTH-1:0]                  retire_insn_vld_i,
    input  logic [    RETIRE_WIDTH-1:0][VADDR_WIDTH-1:0] retire_insn_pc_i,
    input  logic [    RETIRE_WIDTH-1:0]                  retire_insn_is_br_i,
    input  logic [    RETIRE_WIDTH-1:0]                  retire_insn_mispred_i,
    // Exception Port
    input  logic                                         retire_excp_vld_i,
    input  logic [     VADDR_WIDTH-1:0]                  retire_excp_pc_i,
    input  logic                                         retire_excp_is_rvc_i,
    input  logic [ EXCP_TVAL_WIDTH-1:0]                  retire_excp_tval_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                  retire_excp_cause_i,
    // Redirect Port
    output logic                                         csr_redirect_vld_o,
    output logic [     VADDR_WIDTH-1:0]                  csr_redirect_pc_o,
    output logic [     VADDR_WIDTH-1:0]                  csr_redirect_target_o,
    // CSR Report Exception
    output logic                                         csr_insn_re_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0]                  csr_insn_re_rob_tag_o,
    output logic [EXCP_CAUSE_WIDTH-1:0]                  csr_insn_re_cause_o,
    output logic [EXCP_CAUSE_WIDTH-1:0]                  csr_insn_re_tval_o,
    // CSR Clear Busy
    output logic                                         csr_insn_cb_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0]                  csr_insn_cb_rob_tag_o,
    /* Wake up*/
    input  logic                                         rob_empty_i,
    input  logic [   ROB_TAG_WIDTH-1:0]                  oldest_insn_rob_tag_i,

    // Interrupt
    input logic interrupt_sw_i,
    input logic interrupt_timer_i,
    input logic interrupt_ext_i,

    // R/W pmp config
    output logic                          pmp_cfg_vld_o,
    output logic [ PMP_CFG_TAG_WIDTH-1:0] pmp_cfg_tag_o,
    output logic [              XLEN-1:0] pmp_cfg_wr_data_o,
    input  logic [              XLEN-1:0] pmp_cfg_rd_data_i,
    // R/W pmp addr
    output logic                          pmp_addr_vld_o,
    output logic [PMP_ADDR_TAG_WIDTH-1:0] pmp_addr_tag_o,
    output logic [              XLEN-1:0] pmp_addr_wr_data_o,
    input  logic [              XLEN-1:0] pmp_addr_rd_data_i,

    output logic interrupt_ack_o,
    output logic [EXCP_CAUSE_WIDTH-1:0] interrupt_cause_o,

    // Execute FENCE.I : Flush Icache
    output logic flush_icache_req_o,
    input logic flush_icache_gnt_i,
    // Execute FENCE.I : Flush Dcache
    output logic flush_dcache_req_o,
    input logic flush_dcache_gnt_i,
    // Execute SFENCE.VMA : Flush ITLB
    output logic flush_itlb_req_o,
    output logic flush_itlb_use_vpn_o,
    output logic flush_itlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_itlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_itlb_asid_o,
    input logic flush_itlb_gnt_i,
    // Execute SFENCE.VMA : Flush DTLB
    output logic flush_dtlb_req_o,
    output logic flush_dtlb_use_vpn_o,
    output logic flush_dtlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_dtlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_dtlb_asid_o,
    input logic flush_dtlb_gnt_i,
    // Execute SFENCE.VMA : Flush STLB
    output logic flush_stlb_req_o,
    output logic flush_stlb_use_vpn_o,
    output logic flush_stlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_stlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_stlb_asid_o,
    input logic flush_stlb_gnt_i,

    input logic flush_i,
    input clk,
    input rst
);

  /* Dispatch */
  logic [INT_PRF_DISP_PORT_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] disp_bt_init_src_ptag;
  logic [INT_PRF_DISP_PORT_COUNT-1:0]                         disp_bt_init_src_rdy;

  logic                                                       disp_int0_iq_rs1_rdy;
  logic                                                       disp_int0_iq_rs2_rdy;

  logic                                                       disp_int1_iq_rs1_rdy;
  logic                                                       disp_int1_iq_rs2_rdy;

  logic                                                       disp_complex_int0_iq_rs1_rdy;
  logic                                                       disp_complex_int0_iq_rs2_rdy;

  logic                                                       disp_br0_iq_rs1_rdy;
  logic                                                       disp_br0_iq_rs2_rdy;


  logic                                                       disp_agu0_iq_rs1_rdy;
  logic                                                       disp_agu1_iq_rs1_rdy;

  logic                                                       disp_sdu0_iq_rs2_rdy;
  logic                                                       disp_sdu1_iq_rs2_rdy;



  assign disp_bt_init_src_ptag[0]     = disp_int0_iq_phy_rs1_i;
  assign disp_bt_init_src_ptag[1]     = disp_int0_iq_phy_rs2_i;
  assign disp_int0_iq_rs1_rdy         = disp_bt_init_src_rdy[0];
  assign disp_int0_iq_rs2_rdy         = disp_bt_init_src_rdy[1];

  assign disp_bt_init_src_ptag[2]     = disp_int1_iq_phy_rs1_i;
  assign disp_bt_init_src_ptag[3]     = disp_int1_iq_phy_rs2_i;
  assign disp_int1_iq_rs1_rdy         = disp_bt_init_src_rdy[2];
  assign disp_int1_iq_rs2_rdy         = disp_bt_init_src_rdy[3];

  assign disp_bt_init_src_ptag[4]     = disp_complex_int0_iq_phy_rs1_i;
  assign disp_bt_init_src_ptag[5]     = disp_complex_int0_iq_phy_rs2_i;
  assign disp_complex_int0_iq_rs1_rdy = disp_bt_init_src_rdy[4];
  assign disp_complex_int0_iq_rs2_rdy = disp_bt_init_src_rdy[5];

  assign disp_bt_init_src_ptag[6]     = disp_br0_iq_phy_rs1_i;
  assign disp_bt_init_src_ptag[7]     = disp_br0_iq_phy_rs2_i;
  assign disp_br0_iq_rs1_rdy          = disp_bt_init_src_rdy[6];
  assign disp_br0_iq_rs2_rdy          = disp_bt_init_src_rdy[7];

  assign disp_bt_init_src_ptag[8]     = disp_agu0_iq_phy_rs1_i;
  assign disp_bt_init_src_ptag[9]     = disp_agu1_iq_phy_rs1_i;
  assign disp_agu0_iq_rs1_rdy         = disp_bt_init_src_rdy[8];
  assign disp_agu1_iq_rs1_rdy         = disp_bt_init_src_rdy[9];

  assign disp_bt_init_src_ptag[10]    = disp_sdu0_iq_phy_rs2_i;
  assign disp_bt_init_src_ptag[11]    = disp_sdu1_iq_phy_rs2_i;
  assign disp_sdu0_iq_rs2_rdy         = disp_bt_init_src_rdy[10];
  assign disp_sdu1_iq_rs2_rdy         = disp_bt_init_src_rdy[11];

  /* Common Data Bus */
  /*  0 : INT0-ALU
     *  1 : INT1-ALU
     *  2 : COMPLEX-INT0-ALU
     *  3 : COMPLEX-INT0-MUL
     *  4 : COMPLEX-INT0-DIV
     *  5 : BR-BRU
     *  6 : LSU0
     *  7 : LSU1
     */

  // FU -> CDB : Wakeup
  logic [INT_CDB_BUS_WIDTH-1:0] fu_cdb_wakeup_vld;
  logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] fu_cdb_wakeup_prf_tag;

  // FU -> CDB : Forward
  logic [INT_CDB_BUS_WIDTH-1:0] fu_cdb_forward_vld;
  logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] fu_cdb_forward_prf_tag;
  logic [INT_CDB_BUS_WIDTH-1:0][XLEN-1:0] fu_cdb_forward_data;

  // CDB -> IQ : Wakeup
  logic [INT_CDB_BUS_WIDTH-1:0] cdb_iq_wakeup_vld;
  logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_iq_wakeup_prf_tag;

  // CDB -> IQ : Forward Ctrl Path
  logic [INT_CDB_BUS_WIDTH-1:0] cdb_iq_forward_ctrl_vld;
  logic [INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_iq_forward_ctrl_prf_tag;

  // CDB -> IQ : Forward Data Network
  logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0] cdb_iq_forward_data_vld;
  logic [ INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][INT_PREG_TAG_WIDTH-1:0] cdb_iq_forward_data_prf_tag;
  logic [INT_CDB_BUS_WIDTH-1:0][INT_CDB_DEPTH-1:0][XLEN-1:0] cdb_iq_forward_data;

  /* Read PRF Arbiter */
  // Port0  : INT0-Operand0          -    INT0-Operand1           -   SDU0-Operand1
  // Port1  : AGU0-Operand0          -    SDU0-Operand1           -   INT0-Operand1

  // Port2  : INT1-Operand0          -    INT1-Operand1           -   SDU1-Operand1
  // Port3  : AGU1-Operand0          -    SDU1-Operand1           -   INT1-Operand1

  // Port4  : COMPLEX_INT0-Operand0  -    COMPLEX_INT0-Operand1   -   BRU0-Operand1
  // Port5  : BRU0-Operand0          -    BRU0-Operand1           -   COMPLEX_INT-Operand1    

  // Group 0

  logic int0_rd_prf_vld;
  logic [1:0] int0_rd_prf_mask;
  logic [1:0][INT_PREG_TAG_WIDTH-1:0] int0_rd_prf_tag;
  logic [1:0][XLEN-1:0] int0_rd_prf_data;
  logic int0_rd_prf_rdy;

  logic agu0_rd_prf_vld;
  logic agu0_rd_prf_mask;
  logic [INT_PREG_TAG_WIDTH-1:0] agu0_rd_prf_tag;
  logic [XLEN-1:0] agu0_rd_prf_data;
  logic agu0_rd_prf_rdy;

  logic sdu0_rd_prf_vld;
  logic sdu0_rd_prf_mask;
  logic [INT_PREG_TAG_WIDTH-1:0] sdu0_rd_prf_tag;
  logic [XLEN-1:0] sdu0_rd_prf_data;
  logic sdu0_rd_prf_rdy;

  // Group 1
  logic int1_rd_prf_vld;
  logic [1:0] int1_rd_prf_mask;
  logic [1:0][INT_PREG_TAG_WIDTH-1:0] int1_rd_prf_tag;
  logic [1:0][XLEN-1:0] int1_rd_prf_data;
  logic int1_rd_prf_rdy;

  logic agu1_rd_prf_vld;
  logic agu1_rd_prf_mask;
  logic [INT_PREG_TAG_WIDTH-1:0] agu1_rd_prf_tag;
  logic [XLEN-1:0] agu1_rd_prf_data;
  logic agu1_rd_prf_rdy;

  logic sdu1_rd_prf_vld;
  logic sdu1_rd_prf_mask;
  logic [INT_PREG_TAG_WIDTH-1:0] sdu1_rd_prf_tag;
  logic [XLEN-1:0] sdu1_rd_prf_data;
  logic sdu1_rd_prf_rdy;

  // Group 2
  logic complex_int0_rd_prf_vld;
  logic [1:0] complex_int0_rd_prf_mask;
  logic [1:0][INT_PREG_TAG_WIDTH-1:0] complex_int0_rd_prf_tag;
  logic [1:0][XLEN-1:0] complex_int0_rd_prf_data;
  logic complex_int0_rd_prf_rdy;

  logic br0_rd_prf_vld;
  logic [1:0] br0_rd_prf_mask;
  logic [1:0][INT_PREG_TAG_WIDTH-1:0] br0_rd_prf_tag;
  logic [1:0][XLEN-1:0] br0_rd_prf_data;
  logic br0_rd_prf_rdy;

  logic csr_rd_prf_vld;
  logic [1:0] csr_rd_prf_mask;
  logic [1:0][INT_PREG_TAG_WIDTH-1:0] csr_rd_prf_tag;
  logic [1:0][XLEN-1:0] csr_rd_prf_data;


  // Group 0
  logic g0_robin_q;

  logic int0_op0_ask;
  logic int0_op1_ask;
  logic int0_ask0, int0_ask1, int0_ask2;

  logic agu1_op0_ask;
  logic agu1_ask0, agu1_ask1;

  logic sdu1_op1_ask;
  logic sdu1_ask0, sdu1_ask1;

  logic g0_conflict;
  logic g0_sel_int0;
  logic g0_sel_ls1;

  assign int0_op0_ask = int0_rd_prf_vld & int0_rd_prf_mask[0];
  assign int0_op1_ask = int0_rd_prf_vld & int0_rd_prf_mask[1];
  assign int0_ask0 = ~int0_op1_ask & ~int0_op1_ask;
  assign int0_ask1 = int0_op0_ask ^ int0_op1_ask;
  assign int0_ask2 = int0_op0_ask & int0_op1_ask;

  assign agu1_op0_ask = agu1_rd_prf_vld & agu1_rd_prf_mask;
  assign agu1_ask0 = ~agu1_op0_ask;
  assign agu1_ask1 = agu1_op0_ask;

  assign sdu1_op1_ask = sdu1_rd_prf_vld & sdu1_rd_prf_mask;
  assign sdu1_ask0 = ~sdu1_op1_ask;
  assign sdu1_ask1 = sdu1_op1_ask;

  assign g0_conflict = (int0_ask2 & (agu1_ask1 | sdu1_ask1)) | (int0_ask1 & agu1_ask1 & sdu1_ask1);

  assign g0_sel_int0 = ~g0_robin_q;
  assign g0_sel_ls1 = g0_robin_q;

  always_ff @(posedge clk) begin : group0_arbiter_dff
    if (rst) begin
      g0_robin_q <= 1'b0;
    end else begin
      if (g0_conflict) begin
        g0_robin_q <= ~g0_robin_q;
      end
    end
  end

  // Group 1
  logic g1_robin_q;

  logic int1_op0_ask;
  logic int1_op1_ask;
  logic int1_ask0, int1_ask1, int1_ask2;

  logic agu0_op0_ask;
  logic agu0_ask0, agu0_ask1;

  logic sdu0_op1_ask;
  logic sdu0_ask0, sdu0_ask1;

  logic g1_conflict;
  logic g1_sel_int1;
  logic g1_sel_ls0;

  assign int1_op0_ask = int1_rd_prf_vld & int1_rd_prf_mask[0];
  assign int1_op1_ask = int1_rd_prf_vld & int1_rd_prf_mask[1];
  assign int1_ask0 = ~int1_op1_ask & ~int1_op1_ask;
  assign int1_ask1 = int1_op0_ask ^ int1_op1_ask;
  assign int1_ask2 = int1_op0_ask & int1_op1_ask;


  assign agu0_op0_ask = agu0_rd_prf_vld & agu0_rd_prf_mask;
  assign agu0_ask0 = ~agu0_op0_ask;
  assign agu0_ask1 = agu0_op0_ask;

  assign sdu0_op1_ask = sdu0_rd_prf_vld & sdu0_rd_prf_mask;
  assign sdu0_ask0 = ~sdu0_op1_ask;
  assign sdu0_ask1 = sdu0_op1_ask;

  assign g1_conflict = (int1_ask2 & (agu0_ask1 | sdu0_ask1)) | (int1_ask1 & agu0_ask1 & sdu0_ask1);

  assign g1_sel_int1 = ~g1_robin_q;
  assign g1_sel_ls0 = g1_robin_q;

  always_ff @(posedge clk) begin : group1_arbiter_dff
    if (rst) begin
      g1_robin_q <= 1'b0;
    end else begin
      if (g1_conflict) begin
        g1_robin_q <= ~g1_robin_q;
      end
    end
  end

  // Group 2
  logic g2_robin_q;

  logic int2_op0_ask;
  logic int2_op1_ask;
  logic int2_ask0, int2_ask1, int2_ask2;

  logic br0_op0_ask;
  logic br0_op1_ask;
  logic br0_ask0, br0_ask1, br0_ask2;

  logic g2_conflict;
  logic g2_sel_int2;
  logic g2_sel_br0;

  assign int2_op0_ask = complex_int0_rd_prf_vld & complex_int0_rd_prf_mask[0];
  assign int2_op1_ask = complex_int0_rd_prf_vld & complex_int0_rd_prf_mask[1];
  assign int2_ask0 = ~int2_op0_ask & ~int2_op1_ask;
  assign int2_ask1 = int2_op0_ask ^ int2_op1_ask;
  assign int2_ask2 = int2_op0_ask & int2_op1_ask;

  assign br0_op0_ask = br0_rd_prf_vld & br0_rd_prf_mask[0];
  assign br0_op1_ask = br0_rd_prf_vld & br0_rd_prf_mask[1];
  assign br0_ask0 = ~br0_op0_ask & ~br0_op1_ask;
  assign br0_ask1 = br0_op0_ask ^ br0_op1_ask;
  assign br0_ask2 = br0_op0_ask & br0_op1_ask;

  assign g2_conflict = (int2_ask2 & (br0_op0_ask | br0_op1_ask)) |
        (br0_ask2 & (int2_op0_ask | int2_op1_ask));

  assign g2_sel_int2 = ~g2_robin_q;
  assign g2_sel_br0 = g2_robin_q;

  always_ff @(posedge clk) begin : group2_arbiter_dff
    if (rst) begin
      g2_robin_q <= 1'b0;
    end else begin
      if (g2_conflict) begin
        g2_robin_q <= ~g2_robin_q;
      end
    end
  end


  logic [INT_PRF_RD_PORT_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] prf_rd_port_tag;
  logic [INT_PRF_RD_PORT_COUNT-1:0][XLEN-1:0] prf_rd_port_data;

  // Port0 Arbitrary
  logic p0_sel_int0_op0, p0_sel_int0_op1, p0_sel_sdu1_op1;

  assign p0_sel_int0_op0 = g0_conflict ? (g0_sel_int0 & int0_op0_ask) : int0_op0_ask;
  assign p0_sel_int0_op1 = g0_conflict ? (g0_sel_int0 & int0_op1_ask & ~int0_op0_ask) :
        int0_op1_ask & ~int0_op0_ask;
  assign p0_sel_sdu1_op1 = g0_conflict ? (g0_sel_ls1 & sdu1_op1_ask) :
        sdu1_op1_ask & ~int0_op0_ask & ~int0_op1_ask;

  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_prf_rd_port0_ptag_onehot (
      .sel_i ({p0_sel_int0_op0, p0_sel_int0_op1, p0_sel_sdu1_op1}),
      .data_i({int0_rd_prf_tag[0], int0_rd_prf_tag[1], sdu1_rd_prf_tag}),
      .data_o(prf_rd_port_tag[0])
  );


  // Port1 Arbitrary
  logic p1_sel_agu1_op0, p1_sel_sdu1_op1, p1_sel_int0_op1;

  assign p1_sel_agu1_op0 = g0_conflict ? (g0_sel_ls1 & agu1_op0_ask) : agu1_op0_ask;
  assign p1_sel_sdu1_op1 = g0_conflict ? (g0_sel_ls1 & sdu1_op1_ask & ~agu1_op0_ask) :
        sdu1_op1_ask & ~agu1_op0_ask;
  assign p1_sel_int0_op1 = g0_conflict ? (g0_sel_int0 & int0_op1_ask) :
        int0_op1_ask & ~agu1_op0_ask & ~sdu1_op1_ask;

  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_prf_rd_port1_ptag_onehot (
      .sel_i ({p1_sel_agu1_op0, p1_sel_sdu1_op1, p1_sel_int0_op1}),
      .data_i({agu1_rd_prf_tag, sdu1_rd_prf_tag, int0_rd_prf_tag[1]}),
      .data_o(prf_rd_port_tag[1])
  );

  // Port2 Arbitrary
  logic p2_sel_int1_op0, p2_sel_int1_op1, p2_sel_sdu0_op1, p3_sel_csr_op0;

  assign p3_sel_csr_op0 = csr_rd_prf_vld;
  assign p2_sel_int1_op0 = ~p3_sel_csr_op0 & (g1_conflict ? (g1_sel_int1 & int1_op0_ask) : int1_op0_ask);
  assign p2_sel_int1_op1 = ~p3_sel_csr_op0 & (g1_conflict ? (g1_sel_int1 & int1_op1_ask & ~int1_op0_ask) :
        int1_op1_ask & ~int1_op0_ask);
  assign p2_sel_sdu0_op1 = ~p3_sel_csr_op0 & (g1_conflict ? (g1_sel_ls0 & sdu0_op1_ask) :
        sdu0_op1_ask & ~int1_op0_ask & ~int1_op1_ask);

  onehot_mux #(
      .SOURCE_COUNT(4),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_prf_rd_port2_ptag_onehot (
      .sel_i ({p2_sel_int1_op0, p2_sel_int1_op1, p2_sel_sdu0_op1, p3_sel_csr_op0}),
      .data_i({int1_rd_prf_tag[0], int1_rd_prf_tag[1], sdu0_rd_prf_tag, csr_rd_prf_tag[0]}),
      .data_o(prf_rd_port_tag[2])
  );

  // Port3 Arbitrary
  logic p3_sel_agu0_op0, p3_sel_sdu0_op1, p3_sel_int1_op1, p3_sel_csr_op1;

  assign p3_sel_csr_op1 = csr_rd_prf_vld;
  assign p3_sel_agu0_op0 = ~p3_sel_csr_op1 & (g1_conflict ? (g1_sel_ls0 & agu0_op0_ask) : agu0_op0_ask);
  assign p3_sel_sdu0_op1 = ~p3_sel_csr_op1 & (g1_conflict ? (g1_sel_ls0 & sdu0_op1_ask & ~agu0_op0_ask) :
        sdu0_op1_ask & ~agu0_op0_ask);
  assign p3_sel_int1_op1 = ~p3_sel_csr_op1 & (g1_conflict ? (g1_sel_int1 & int1_op1_ask) :
        int1_op1_ask & ~agu0_op0_ask & ~sdu0_op1_ask);

  onehot_mux #(
      .SOURCE_COUNT(4),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_prf_rd_port3_ptag_onehot (
      .sel_i ({p3_sel_agu0_op0, p3_sel_sdu0_op1, p3_sel_int1_op1, p3_sel_csr_op1}),
      .data_i({agu0_rd_prf_tag, sdu0_rd_prf_tag, int1_rd_prf_tag[1], csr_rd_prf_tag[1]}),
      .data_o(prf_rd_port_tag[3])
  );

  // Port4 Arbitrary
  logic p4_sel_int2_op0, p4_sel_int2_op1, p4_sel_br0_op1;

  assign p4_sel_int2_op0 = g2_conflict ? (g2_sel_int2 & int2_op0_ask) : int2_op0_ask;
  assign p4_sel_int2_op1 = g2_conflict ? (g2_sel_int2 & int2_op1_ask & ~int2_op0_ask) :
        int2_op1_ask & ~int2_op0_ask;
  assign p4_sel_br0_op1 = g2_conflict ? (g2_sel_br0 & br0_op1_ask) :
        br0_op1_ask & ~int2_op0_ask & ~int2_op1_ask;

  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_prf_rd_port4_ptag_onehot (
      .sel_i ({p4_sel_int2_op0, p4_sel_int2_op1, p4_sel_br0_op1}),
      .data_i({complex_int0_rd_prf_tag[0], complex_int0_rd_prf_tag[1], br0_rd_prf_tag[1]}),
      .data_o(prf_rd_port_tag[4])
  );

  // Port5 Arbitrary    
  logic p5_sel_br0_op0, p5_sel_br0_op1, p5_sel_int2_op1;

  assign p5_sel_br0_op0 = g2_conflict ? (g2_sel_br0 & br0_op0_ask) : br0_op0_ask;
  assign p5_sel_br0_op1 = g2_conflict ? (g2_sel_br0 & br0_op1_ask & ~br0_op0_ask) :
        br0_op1_ask & ~br0_op0_ask;
  assign p5_sel_int2_op1 = g2_conflict ? (g2_sel_int2 & int2_op1_ask) :
        int2_op1_ask & ~br0_op0_ask & ~br0_op1_ask;

  onehot_mux #(
      .SOURCE_COUNT(3),
      .DATA_WIDTH  (INT_PREG_TAG_WIDTH)
  ) u_prf_rd_port5_ptag_onehot (
      .sel_i ({p5_sel_br0_op0, p5_sel_br0_op1, p5_sel_int2_op1}),
      .data_i({br0_rd_prf_tag[0], br0_rd_prf_tag[1], complex_int0_rd_prf_tag[1]}),
      .data_o(prf_rd_port_tag[5])
  );

  // Issue Queue Read PRF Data Selector
  assign int0_rd_prf_rdy = {2{~g0_conflict | g0_sel_int0}};
  assign agu1_rd_prf_rdy = ~g0_conflict | g0_sel_ls1;
  assign sdu1_rd_prf_rdy = ~g0_conflict | g0_sel_ls1;

  assign int1_rd_prf_rdy = {2{~g1_conflict | g1_sel_int1}};
  assign agu0_rd_prf_rdy = ~g1_conflict | g1_sel_ls0;
  assign sdu0_rd_prf_rdy = ~g1_conflict | g1_sel_ls0;

  assign complex_int0_rd_prf_rdy = {2{~g2_conflict | g2_sel_int2}};
  assign br0_rd_prf_rdy = {2{~g2_conflict | g2_sel_br0}};

  assign int0_rd_prf_data[0] = prf_rd_port_data[0];
  assign int0_rd_prf_data[1] = (prf_rd_port_data[0] & {XLEN{p0_sel_int0_op1}}) |
        (prf_rd_port_data[1] & {XLEN{p1_sel_int0_op1}});

  assign agu1_rd_prf_data = prf_rd_port_data[1];
  assign sdu1_rd_prf_data = (prf_rd_port_data[0] & {XLEN{p0_sel_sdu1_op1}}) |
    (prf_rd_port_data[1] & {XLEN{p1_sel_sdu1_op1}});

  assign int1_rd_prf_data[0] = prf_rd_port_data[2];
  assign int1_rd_prf_data[1] = (prf_rd_port_data[2] & {XLEN{p2_sel_int1_op1}}) |
    (prf_rd_port_data[3] & {XLEN{p3_sel_int1_op1}});

  assign agu0_rd_prf_data = prf_rd_port_data[3];
  assign sdu0_rd_prf_data = (prf_rd_port_data[2] & {XLEN{p2_sel_sdu0_op1}}) |
        (prf_rd_port_data[3] & {XLEN{p3_sel_sdu0_op1}});

  assign complex_int0_rd_prf_data[0] = prf_rd_port_data[4];
  assign complex_int0_rd_prf_data[1] = (prf_rd_port_data[4] & {XLEN{p4_sel_int2_op1}}) |
        (prf_rd_port_data[5] & {XLEN{p5_sel_int2_op1}});

  assign br0_rd_prf_data[0] = prf_rd_port_data[5];
  assign br0_rd_prf_data[1] = (prf_rd_port_data[4] & {XLEN{p4_sel_br0_op1}}) |
        (prf_rd_port_data[5] & {XLEN{p5_sel_br0_op1}});

  assign csr_rd_prf_data[0] = prf_rd_port_data[2];
  assign csr_rd_prf_data[1] = prf_rd_port_data[3];


  // Write Back
  logic [INT_PRF_WR_PORT_COUNT-1:0]                         prf_wr_port_vld;
  logic [INT_PRF_WR_PORT_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] prf_wr_port_ptag;
  logic [INT_PRF_WR_PORT_COUNT-1:0][              XLEN-1:0] prf_wr_port_data;

  logic                                                     int0_wr_prf_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         int0_wr_prf_ptag;
  logic [                 XLEN-1:0]                         int0_wr_prf_data;
  logic                                                     int0_wr_prf_rdy;

  logic                                                     int1_wr_prf_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         int1_wr_prf_ptag;
  logic [                 XLEN-1:0]                         int1_wr_prf_data;
  logic                                                     int1_wr_prf_rdy;

  logic                                                     complex_int0_wr_prf_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         complex_int0_wr_prf_ptag;
  logic [                 XLEN-1:0]                         complex_int0_wr_prf_data;
  logic                                                     complex_int0_wr_prf_rdy;

  logic                                                     ls0_wr_prf_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         ls0_wr_prf_ptag;
  logic [                 XLEN-1:0]                         ls0_wr_prf_data;
  logic                                                     ls0_wr_prf_rdy;

  logic                                                     ls1_wr_prf_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         ls1_wr_prf_ptag;
  logic [                 XLEN-1:0]                         ls1_wr_prf_data;
  logic                                                     ls1_wr_prf_rdy;

  logic                                                     br0_wr_prf_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         br0_wr_prf_ptag;
  logic [                 XLEN-1:0]                         br0_wr_prf_data;
  logic                                                     br0_wr_prf_rdy;

  logic                                                     csr_insn_wb_vld;
  logic [   INT_PREG_TAG_WIDTH-1:0]                         csr_insn_wb_prd;
  logic [                 XLEN-1:0]                         csr_insn_wb_data;

  assign ls0_wr_prf_vld            = lsu_prf_wr_vld_i[0];
  assign ls0_wr_prf_ptag           = lsu_prf_wr_ptag_i[0];
  assign ls0_wr_prf_data           = lsu_prf_wr_data_i[0];

  assign ls1_wr_prf_vld            = lsu_prf_wr_vld_i[1];
  assign ls1_wr_prf_ptag           = lsu_prf_wr_ptag_i[1];
  assign ls1_wr_prf_data           = lsu_prf_wr_data_i[1];

  /* Write Back Arbiter */
  // Port0 : INT0 
  assign prf_wr_port_vld[0]        = int0_wr_prf_vld;
  assign prf_wr_port_ptag[0]       = int0_wr_prf_ptag;
  assign prf_wr_port_data[0]       = int0_wr_prf_data;
  assign int0_wr_prf_rdy           = 1'b1;
  // Port1 : INT1
  assign prf_wr_port_vld[1]        = int1_wr_prf_vld;
  assign prf_wr_port_ptag[1]       = int1_wr_prf_ptag;
  assign prf_wr_port_data[1]       = int1_wr_prf_data;
  assign int1_wr_prf_rdy           = 1'b1;
  // Port2 : COMPLEX_INT0
  assign prf_wr_port_vld[2]        = complex_int0_wr_prf_vld;
  assign prf_wr_port_ptag[2]       = complex_int0_wr_prf_ptag;
  assign prf_wr_port_data[2]       = complex_int0_wr_prf_data;
  assign complex_int0_wr_prf_rdy   = 1'b1;
  // Port3 : LSU0
  assign prf_wr_port_vld[3]        = ls0_wr_prf_vld;
  assign prf_wr_port_ptag[3]       = ls0_wr_prf_ptag;
  assign prf_wr_port_data[3]       = ls0_wr_prf_data;
  assign ls0_wr_prf_rdy            = 1'b1;
  // Port4 : LSU1
  assign prf_wr_port_vld[4]        = csr_insn_wb_vld | ls1_wr_prf_vld;
  assign prf_wr_port_ptag[4]       = csr_insn_wb_vld ? csr_insn_wb_prd : ls1_wr_prf_ptag;
  assign prf_wr_port_data[4]       = csr_insn_wb_vld ? csr_insn_wb_data : ls1_wr_prf_data;
  assign ls1_wr_prf_rdy            = 1'b1;
  // Port5 : BR0
  assign prf_wr_port_vld[5]        = br0_wr_prf_vld;
  assign prf_wr_port_ptag[5]       = br0_wr_prf_ptag;
  assign prf_wr_port_data[5]       = br0_wr_prf_data;
  assign br0_wr_prf_rdy            = 1'b1;

  assign fu_cdb_wakeup_vld[6]      = lsu_cdb_wakeup_vld_i[0];
  assign fu_cdb_wakeup_prf_tag[6]  = lsu_cdb_wakeup_ptag_i[0];
  assign fu_cdb_forward_vld[6]     = lsu_prf_wr_vld_i[0];
  assign fu_cdb_forward_prf_tag[6] = lsu_prf_wr_ptag_i[0];
  assign fu_cdb_forward_data[6]    = lsu_prf_wr_data_i[0];

  assign fu_cdb_wakeup_vld[7]      = lsu_cdb_wakeup_vld_i[1];
  assign fu_cdb_wakeup_prf_tag[7]  = lsu_cdb_wakeup_ptag_i[1];
  assign fu_cdb_forward_vld[7]     = lsu_prf_wr_vld_i[1];
  assign fu_cdb_forward_prf_tag[7] = lsu_prf_wr_ptag_i[1];
  assign fu_cdb_forward_data[7]    = lsu_prf_wr_data_i[1];


  int_ctrl_pipe #(
      .IQ_ENTRY_COUNT(INT0_IQ_DEPTH)
  ) u_int_ctrl_pipe_0 (
      .disp_alu_iq_vld_i(disp_int0_iq_vld_i),
      .disp_alu_iq_rob_tag_i(disp_int0_iq_rob_tag_i),
      .disp_alu_iq_opcode_i(disp_int0_iq_opcode_i),
      .disp_alu_iq_rs1_rdy_i(disp_int0_iq_rs1_rdy),
      .disp_alu_iq_rs2_rdy_i(disp_int0_iq_rs2_rdy),
      .disp_alu_iq_use_rs1_i(disp_int0_iq_use_rs1_i),
      .disp_alu_iq_use_rs2_i(disp_int0_iq_use_rs2_i),
      .disp_alu_iq_use_rd_i(disp_int0_iq_use_rd_i),
      .disp_alu_iq_phy_rs1_i(disp_int0_iq_phy_rs1_i),
      .disp_alu_iq_phy_rs2_i(disp_int0_iq_phy_rs2_i),
      .disp_alu_iq_phy_rd_i(disp_int0_iq_phy_rd_i),
      .disp_alu_iq_imm_i(disp_int0_iq_imm_i),
      .disp_alu_iq_use_imm_i(disp_int0_iq_use_imm_i),
      .disp_alu_iq_use_pc_i(disp_int0_iq_use_pc_i),
      .disp_alu_iq_rdy_o(disp_int0_iq_rdy_o),
      .issue_rd_prf_vld_o(int0_rd_prf_vld),
      .issue_rd_prf_mask_o(int0_rd_prf_mask),
      .issue_rd_prf_tag_o(int0_rd_prf_tag),
      .issue_rd_prf_rdy_i(int0_rd_prf_rdy),
      .rob_tag_o(alu_issue_rob_tag_o[0]),
      .rob_inst_pc_i(alu_issue_pc_i[0]),
      .prf_data_i(int0_rd_prf_data),
      .wr_prf_vld_o(int0_wr_prf_vld),
      .wr_prf_tag_o(int0_wr_prf_ptag),
      .wr_prf_data_o(int0_wr_prf_data),
      .wr_prf_rdy_i(int0_wr_prf_rdy),
      .wb_rob_vld_o(rob_complete_vld_o[0]),
      .wb_rob_tag_o(rob_complete_tag_o[0]),
      .forward_wakeup_vld_o(fu_cdb_wakeup_vld[0]),
      .forward_wakeup_prf_tag_o(fu_cdb_wakeup_prf_tag[0]),
      .forward_data_vld_o(fu_cdb_forward_vld[0]),
      .forward_data_prf_tag_o(fu_cdb_forward_prf_tag[0]),
      .forward_data_o(fu_cdb_forward_data[0]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  int_ctrl_pipe #(
      .IQ_ENTRY_COUNT(INT1_IQ_DEPTH)
  ) u_int_ctrl_pipe_1 (
      .disp_alu_iq_vld_i(disp_int1_iq_vld_i),
      .disp_alu_iq_rob_tag_i(disp_int1_iq_rob_tag_i),
      .disp_alu_iq_opcode_i(disp_int1_iq_opcode_i),
      .disp_alu_iq_rs1_rdy_i(disp_int1_iq_rs1_rdy),
      .disp_alu_iq_rs2_rdy_i(disp_int1_iq_rs2_rdy),
      .disp_alu_iq_use_rs1_i(disp_int1_iq_use_rs1_i),
      .disp_alu_iq_use_rs2_i(disp_int1_iq_use_rs2_i),
      .disp_alu_iq_use_rd_i(disp_int1_iq_use_rd_i),
      .disp_alu_iq_phy_rs1_i(disp_int1_iq_phy_rs1_i),
      .disp_alu_iq_phy_rs2_i(disp_int1_iq_phy_rs2_i),
      .disp_alu_iq_phy_rd_i(disp_int1_iq_phy_rd_i),
      .disp_alu_iq_imm_i(disp_int1_iq_imm_i),
      .disp_alu_iq_use_imm_i(disp_int1_iq_use_imm_i),
      .disp_alu_iq_use_pc_i(disp_int1_iq_use_pc_i),
      .disp_alu_iq_rdy_o(disp_int1_iq_rdy_o),
      .issue_rd_prf_vld_o(int1_rd_prf_vld),
      .issue_rd_prf_mask_o(int1_rd_prf_mask),
      .issue_rd_prf_tag_o(int1_rd_prf_tag),
      .issue_rd_prf_rdy_i(int1_rd_prf_rdy),
      .rob_tag_o(alu_issue_rob_tag_o[1]),
      .rob_inst_pc_i(alu_issue_pc_i[1]),
      .prf_data_i(int1_rd_prf_data),
      .wr_prf_vld_o(int1_wr_prf_vld),
      .wr_prf_tag_o(int1_wr_prf_ptag),
      .wr_prf_data_o(int1_wr_prf_data),
      .wr_prf_rdy_i(int1_wr_prf_rdy),
      .wb_rob_vld_o(rob_complete_vld_o[1]),
      .wb_rob_tag_o(rob_complete_tag_o[1]),
      .forward_wakeup_vld_o(fu_cdb_wakeup_vld[1]),
      .forward_wakeup_prf_tag_o(fu_cdb_wakeup_prf_tag[1]),
      .forward_data_vld_o(fu_cdb_forward_vld[1]),
      .forward_data_prf_tag_o(fu_cdb_forward_prf_tag[1]),
      .forward_data_o(fu_cdb_forward_data[1]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );


  complex_int_ctrl_pipe #(
      .IQ_ENTRY_COUNT(COMPLEX_INT0_IQ_DEPTH)
  ) u_complex_int_ctrl_pipe_0 (
      .disp_int_iq_vld_i(disp_complex_int0_iq_vld_i),
      .disp_int_iq_rob_tag_i(disp_complex_int0_iq_rob_tag_i),
      .disp_int_iq_fu_type_i(disp_complex_int0_iq_fu_type_i),
      .disp_int_iq_opcode_i(disp_complex_int0_iq_opcode_i),
      .disp_int_iq_rs1_rdy_i(disp_complex_int0_iq_rs1_rdy),
      .disp_int_iq_rs2_rdy_i(disp_complex_int0_iq_rs2_rdy),
      .disp_int_iq_use_rs1_i(disp_complex_int0_iq_use_rs1_i),
      .disp_int_iq_use_rs2_i(disp_complex_int0_iq_use_rs2_i),
      .disp_int_iq_use_rd_i(disp_complex_int0_iq_use_rd_i),
      .disp_int_iq_phy_rs1_i(disp_complex_int0_iq_phy_rs1_i),
      .disp_int_iq_phy_rs2_i(disp_complex_int0_iq_phy_rs2_i),
      .disp_int_iq_phy_rd_i(disp_complex_int0_iq_phy_rd_i),
      .disp_int_iq_imm_i(disp_complex_int0_iq_imm_i),
      .disp_int_iq_use_imm_i(disp_complex_int0_iq_use_imm_i),
      .disp_int_iq_use_pc_i(disp_complex_int0_iq_use_pc_i),
      .disp_int_iq_rdy_o(disp_complex_int0_iq_rdy_o),
      .issue_rd_prf_vld_o(complex_int0_rd_prf_vld),
      .issue_rd_prf_mask_o(complex_int0_rd_prf_mask),
      .issue_rd_prf_tag_o(complex_int0_rd_prf_tag),
      .issue_rd_prf_rdy_i(complex_int0_rd_prf_rdy),
      .rob_tag_o(alu_issue_rob_tag_o[2]),
      .rob_inst_pc_i(alu_issue_pc_i[2]),
      .prf_data_i(complex_int0_rd_prf_data),
      .wr_prf_vld_o(complex_int0_wr_prf_vld),
      .wr_prf_tag_o(complex_int0_wr_prf_ptag),
      .wr_prf_data_o(complex_int0_wr_prf_data),
      .wr_prf_rdy_i(complex_int0_wr_prf_rdy),
      .wb_rob_vld_o(rob_complete_vld_o[2]),
      .wb_rob_tag_o(rob_complete_tag_o[2]),
      .alu_forward_wakeup_vld_o(fu_cdb_wakeup_vld[2]),
      .alu_forward_wakeup_prf_tag_o(fu_cdb_wakeup_prf_tag[2]),
      .mul_forward_wakeup_vld_o(fu_cdb_wakeup_vld[3]),
      .mul_forward_wakeup_prf_tag_o(fu_cdb_wakeup_prf_tag[3]),
      .div_forward_wakeup_vld_o(fu_cdb_wakeup_vld[4]),
      .div_forward_wakeup_prf_tag_o(fu_cdb_wakeup_prf_tag[4]),
      .alu_forward_data_vld_o(fu_cdb_forward_vld[2]),
      .alu_forward_data_prf_tag_o(fu_cdb_forward_prf_tag[2]),
      .alu_forward_data_o(fu_cdb_forward_data[2]),
      .mul_forward_data_vld_o(fu_cdb_forward_vld[3]),
      .mul_forward_data_prf_tag_o(fu_cdb_forward_prf_tag[3]),
      .mul_forward_data_o(fu_cdb_forward_data[3]),
      .div_forward_data_vld_o(fu_cdb_forward_vld[4]),
      .div_forward_data_prf_tag_o(fu_cdb_forward_prf_tag[4]),
      .div_forward_data_o(fu_cdb_forward_data[4]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  br_ctrl_pipe #(
      .IQ_ENTRY_COUNT(BR0_IQ_DEPTH)
  ) u_br_ctrl_pipe_0 (
      .disp_br_iq_vld_i(disp_br0_iq_vld_i),
      .disp_br_iq_rob_tag_i(disp_br0_iq_rob_tag_i),
      .disp_br_iq_brq_tag_i(disp_br0_iq_brq_tag_i),
      .disp_br_iq_opcode_i(disp_br0_iq_opcode_i),
      .disp_br_iq_rs1_rdy_i(disp_br0_iq_rs1_rdy),
      .disp_br_iq_rs2_rdy_i(disp_br0_iq_rs2_rdy),
      .disp_br_iq_use_rs1_i(disp_br0_iq_use_rs1_i),
      .disp_br_iq_use_rs2_i(disp_br0_iq_use_rs2_i),
      .disp_br_iq_use_rd_i(disp_br0_iq_use_rd_i),
      .disp_br_iq_phy_rs1_i(disp_br0_iq_phy_rs1_i),
      .disp_br_iq_phy_rs2_i(disp_br0_iq_phy_rs2_i),
      .disp_br_iq_phy_rd_i(disp_br0_iq_phy_rd_i),
      .disp_br_iq_imm_i(disp_br0_iq_imm_i),
      .disp_br_iq_rdy_o(disp_br0_iq_rdy_o),
      .issue_rd_prf_vld_o(br0_rd_prf_vld),
      .issue_rd_prf_mask_o(br0_rd_prf_mask),
      .issue_rd_prf_tag_o(br0_rd_prf_tag),
      .issue_rd_prf_rdy_i(br0_rd_prf_rdy),
      .rob_tag_o(bru_issue_rob_tag_o[0]),
      .brq_tag_o(bru_issue_brq_tag_o[0]),
      .inst_pc_i(bru_issue_pc_i[0]),
      .is_rvc_i(bru_issue_is_rvc_i[0]),
      .btq_tag_i(bru_issue_btq_tag_i[0]),
      .pred_taken_i(bru_issue_pred_taken_i[0]),
      .pred_target_i(bru_issue_pred_target_i[0]),
      .prf_data_i(br0_rd_prf_data),
      .wr_prf_vld_o(br0_wr_prf_vld),
      .wr_prf_tag_o(br0_wr_prf_ptag),
      .wr_prf_data_o(br0_wr_prf_data),
      .wr_prf_rdy_i(br0_wr_prf_rdy),
      .wb_rob_vld_o(rob_complete_vld_o[3]),
      .wb_rob_tag_o(rob_complete_tag_o[3]),
      .bru_resolve_vld_o(bru_resolve_vld_o[0]),
      .bru_resolve_pc_o(bru_resolve_pc_o[0]),
      .bru_resolve_taken_o(bru_resolve_taken_o[0]),
      .bru_resolve_mispred_o(bru_resolve_mispred_o[0]),
      .bru_resolve_target_o(bru_resolve_target_o[0]),
      .bru_resolve_rob_tag_o(bru_resolve_rob_tag_o[0]),
      .bru_resolve_brq_tag_o(bru_resolve_brq_tag_o[0]),
      .bru_resolve_btq_tag_o(bru_resolve_btq_tag_o[0]),
      .forward_wakeup_vld_o(fu_cdb_wakeup_vld[5]),
      .forward_wakeup_prf_tag_o(fu_cdb_wakeup_prf_tag[5]),
      .forward_data_vld_o(fu_cdb_forward_vld[5]),
      .forward_data_prf_tag_o(fu_cdb_forward_prf_tag[5]),
      .forward_data_o(fu_cdb_forward_data[5]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  agu_ctrl_pipe #(
      .IQ_ENTRY_COUNT(AGU0_IQ_DEPTH)
  ) u_agu_ctrl_pipe_0 (
      .disp_agu_iq_vld_i(disp_agu0_iq_vld_i),
      .disp_agu_iq_rob_tag_i(disp_agu0_iq_rob_tag_i),
      .disp_agu_iq_lsq_tag_i(disp_agu0_iq_lsq_tag_i),
      .disp_agu_iq_fu_type_i(disp_agu0_iq_fu_type_i),
      .disp_agu_iq_opcode_i(disp_agu0_iq_opcode_i),
      .disp_agu_iq_rs1_rdy_i(disp_agu0_iq_rs1_rdy),
      .disp_agu_iq_phy_rs1_i(disp_agu0_iq_phy_rs1_i),
      .disp_agu_iq_phy_rd_i(disp_agu0_iq_phy_rd_i),
      .disp_agu_iq_imm_i(disp_agu0_iq_imm_i),
      .disp_agu_iq_rdy_o(disp_agu0_iq_rdy_o),
      .issue_rd_prf_vld_o(agu0_rd_prf_vld),
      .issue_rd_prf_mask_o(agu0_rd_prf_mask),
      .issue_rd_prf_tag_o(agu0_rd_prf_tag),
      .issue_rd_prf_rdy_i(agu0_rd_prf_rdy),
      .prf_data_i(agu0_rd_prf_data),
      .agu_iq_issue_vld_o(agu_lsu_issue_vld_o[0]),
      .agu_iq_issue_excp_vld_o(agu_lsu_issue_excp_vld_o[0]),
      .agu_iq_issue_rob_tag_o(agu_lsu_issue_rob_tag_o[0]),
      .agu_iq_issue_lsq_tag_o(agu_lsu_issue_lsq_tag_o[0]),
      .agu_iq_issue_iq_tag_o(agu_lsu_issue_iq_tag_o[0]),
      .agu_iq_issue_fu_type_o(agu_lsu_issue_fu_type_o[0]),
      .agu_iq_issue_opcode_o(agu_lsu_issue_opcode_o[0]),
      .agu_iq_issue_vaddr_o(agu_lsu_issue_vaddr_o[0]),
      .agu_iq_issue_phy_rd_o(agu_lsu_issue_phy_rd_o[0]),
      .agu_iq_issue_rdy_i(agu_lsu_issue_rdy_i[0]),
      .lsu_iq_commit_vld_i(lsu_iq_commit_vld_i[0]),
      .lsu_iq_commit_reactivate_i(lsu_iq_commit_reactivate_i[0]),
      .lsu_iq_commit_tag_i(lsu_iq_commit_tag_i[0]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  agu_ctrl_pipe #(
      .IQ_ENTRY_COUNT(AGU1_IQ_DEPTH)
  ) u_agu_ctrl_pipe_1 (
      .disp_agu_iq_vld_i(disp_agu1_iq_vld_i),
      .disp_agu_iq_rob_tag_i(disp_agu1_iq_rob_tag_i),
      .disp_agu_iq_lsq_tag_i(disp_agu1_iq_lsq_tag_i),
      .disp_agu_iq_fu_type_i(disp_agu1_iq_fu_type_i),
      .disp_agu_iq_opcode_i(disp_agu1_iq_opcode_i),
      .disp_agu_iq_rs1_rdy_i(disp_agu1_iq_rs1_rdy),
      .disp_agu_iq_phy_rs1_i(disp_agu1_iq_phy_rs1_i),
      .disp_agu_iq_phy_rd_i(disp_agu1_iq_phy_rd_i),
      .disp_agu_iq_imm_i(disp_agu1_iq_imm_i),
      .disp_agu_iq_rdy_o(disp_agu1_iq_rdy_o),
      .issue_rd_prf_vld_o(agu1_rd_prf_vld),
      .issue_rd_prf_mask_o(agu1_rd_prf_mask),
      .issue_rd_prf_tag_o(agu1_rd_prf_tag),
      .issue_rd_prf_rdy_i(agu1_rd_prf_rdy),
      .prf_data_i(agu1_rd_prf_data),
      .agu_iq_issue_vld_o(agu_lsu_issue_vld_o[1]),
      .agu_iq_issue_excp_vld_o(agu_lsu_issue_excp_vld_o[1]),
      .agu_iq_issue_rob_tag_o(agu_lsu_issue_rob_tag_o[1]),
      .agu_iq_issue_lsq_tag_o(agu_lsu_issue_lsq_tag_o[1]),
      .agu_iq_issue_iq_tag_o(agu_lsu_issue_iq_tag_o[1]),
      .agu_iq_issue_fu_type_o(agu_lsu_issue_fu_type_o[1]),
      .agu_iq_issue_opcode_o(agu_lsu_issue_opcode_o[1]),
      .agu_iq_issue_vaddr_o(agu_lsu_issue_vaddr_o[1]),
      .agu_iq_issue_phy_rd_o(agu_lsu_issue_phy_rd_o[1]),
      .agu_iq_issue_rdy_i(agu_lsu_issue_rdy_i[1]),
      .lsu_iq_commit_vld_i(lsu_iq_commit_vld_i[1]),
      .lsu_iq_commit_reactivate_i(lsu_iq_commit_reactivate_i[1]),
      .lsu_iq_commit_tag_i(lsu_iq_commit_tag_i[1]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  sdu_ctrl_pipe #(
      .IQ_ENTRY_COUNT(SDU0_IQ_DEPTH)
  ) u_sdu_ctrl_pipe_0 (
      .disp_sdu_iq_vld_i(disp_sdu0_iq_vld_i),
      .disp_sdu_iq_rob_tag_i(disp_sdu0_iq_rob_tag_i),
      .disp_sdu_iq_stq_tag_i(disp_sdu0_iq_stq_tag_i),
      .disp_sdu_iq_rs2_rdy_i(disp_sdu0_iq_rs2_rdy),
      .disp_sdu_iq_phy_rs2_i(disp_sdu0_iq_phy_rs2_i),
      .disp_sdu_iq_rdy_o(disp_sdu0_iq_rdy_o),
      .issue_rd_prf_vld_o(sdu0_rd_prf_vld),
      .issue_rd_prf_mask_o(sdu0_rd_prf_mask),
      .issue_rd_prf_tag_o(sdu0_rd_prf_tag),
      .issue_rd_prf_rdy_i(sdu0_rd_prf_rdy),
      .prf_data_i(sdu0_rd_prf_data),
      .store_data_vld_o(sdu_lsu_issue_vld_o[0]),
      .store_data_stq_tag_o(sdu_lsu_issue_stq_tag_o[0]),
      .store_data_o(sdu_lsu_issue_data_o[0]),
      .store_data_rdy_i(sdu_lsu_issue_rdy_i[0]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  sdu_ctrl_pipe #(
      .IQ_ENTRY_COUNT(SDU1_IQ_DEPTH)
  ) u_sdu_ctrl_pipe_1 (
      .disp_sdu_iq_vld_i(disp_sdu1_iq_vld_i),
      .disp_sdu_iq_rob_tag_i(disp_sdu1_iq_rob_tag_i),
      .disp_sdu_iq_stq_tag_i(disp_sdu1_iq_stq_tag_i),
      .disp_sdu_iq_rs2_rdy_i(disp_sdu1_iq_rs2_rdy),
      .disp_sdu_iq_phy_rs2_i(disp_sdu1_iq_phy_rs2_i),
      .disp_sdu_iq_rdy_o(disp_sdu1_iq_rdy_o),
      .issue_rd_prf_vld_o(sdu1_rd_prf_vld),
      .issue_rd_prf_mask_o(sdu1_rd_prf_mask),
      .issue_rd_prf_tag_o(sdu1_rd_prf_tag),
      .issue_rd_prf_rdy_i(sdu1_rd_prf_rdy),
      .prf_data_i(sdu1_rd_prf_data),
      .store_data_vld_o(sdu_lsu_issue_vld_o[1]),
      .store_data_stq_tag_o(sdu_lsu_issue_stq_tag_o[1]),
      .store_data_o(sdu_lsu_issue_data_o[1]),
      .store_data_rdy_i(sdu_lsu_issue_rdy_i[1]),
      .cdb_wakeup_vld_i(cdb_iq_wakeup_vld),
      .cdb_wakeup_ptag_i(cdb_iq_wakeup_prf_tag),
      .cdb_forward_ctrl_vld_i(cdb_iq_forward_ctrl_vld),
      .cdb_forward_ctrl_ptag_i(cdb_iq_forward_ctrl_prf_tag),
      .cdb_forward_vld_i(cdb_iq_forward_data_vld),
      .cdb_forward_ptag_i(cdb_iq_forward_data_prf_tag),
      .cdb_forward_data_i(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  int_prf #(
      .XLEN(XLEN),
      .REGISTER_COUNT(INT_PREG_COUNT),
      .ALLOC_PORT_COUNT(INT_PRF_ALLOC_PORT_COUNT),
      .BUSY_TABLE_READ_PORT(INT_PRF_DISP_PORT_COUNT),
      .READ_PORT_COUNT(INT_PRF_RD_PORT_COUNT),
      .WRITE_PORT_COUNT(INT_PRF_WR_PORT_COUNT),
      .OUTPUT_LATENCY(0)
  ) u_int_prf (
      .alloc_prf_vld_i(rn_int_prf_alloc_vld_i),
      .alloc_prf_tag_i(rn_int_prf_alloc_tag_i),
      .disp_inst_src_ptag_i(disp_bt_init_src_ptag),
      .disp_inst_src_rdy_o(disp_bt_init_src_rdy),
      .rd_prf_vld_i({INT_PRF_RD_PORT_COUNT{1'b1}}),
      .rd_prf_tag_i(prf_rd_port_tag),
      .rd_prf_data_o(prf_rd_port_data),
      .wr_prf_vld_i(prf_wr_port_vld),
      .wr_prf_tag_i(prf_wr_port_ptag),
      .wr_prf_data_i(prf_wr_port_data),
      .clk(clk),
      .rst(rst)
  );
  rvh_cdb u_rvh_cdb (
      .fu_cdb_wakeup_vld_i(fu_cdb_wakeup_vld),
      .fu_cdb_wakeup_prf_tag_i(fu_cdb_wakeup_prf_tag),
      .fu_cdb_forward_vld_i(fu_cdb_forward_vld),
      .fu_cdb_forward_prf_tag_i(fu_cdb_forward_prf_tag),
      .fu_cdb_forward_data_i(fu_cdb_forward_data),
      .cdb_iq_wakeup_vld_o(cdb_iq_wakeup_vld),
      .cdb_iq_wakeup_prf_tag_o(cdb_iq_wakeup_prf_tag),
      .cdb_iq_forward_ctrl_vld_o(cdb_iq_forward_ctrl_vld),
      .cdb_iq_forward_ctrl_prf_tag_o(cdb_iq_forward_ctrl_prf_tag),
      .cdb_iq_forward_data_vld_o(cdb_iq_forward_data_vld),
      .cdb_iq_forward_data_prf_tag_o(cdb_iq_forward_data_prf_tag),
      .cdb_iq_forward_data_o(cdb_iq_forward_data),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  csr_ctrl_pipe u_csr_ctrl_pipe (
      .hart_id_i(hart_id_i),
      .boot_address_i(boot_address_i),
      .priv_lvl_o(priv_lvl_o),
      .tvm_o(tvm_o),
      .tw_o(tw_o),
      .tsr_o(tsr_o),
      .mstatus_o(mstatus_o),
      .satp_o(satp_o),
      .fs_o(fs_o),
      .fflags_o(fflags_o),
      .frm_o(frm_o),
      .fprec_o(fprec_o),
      .rn_csr_ir_disp_vld_i(rn_csr_ir_disp_vld_i),
      .rn_csr_ir_disp_pc_i(rn_csr_ir_disp_pc_i),
      .rn_csr_ir_disp_rob_tag_i(rn_csr_ir_disp_rob_tag_i),
      .rn_csr_ir_disp_minor_op_i(rn_csr_ir_disp_minor_op_i),
      .rn_csr_ir_disp_use_rs1_i(rn_csr_ir_disp_use_rs1_i),
      .rn_csr_ir_disp_phy_rs1_i(rn_csr_ir_disp_phy_rs1_i),
      .rn_csr_ir_disp_use_rs2_i(rn_csr_ir_disp_use_rs2_i),
      .rn_csr_ir_disp_phy_rs2_i(rn_csr_ir_disp_phy_rs2_i),
      .rn_csr_ir_disp_use_imm_i(rn_csr_ir_disp_use_imm_i),
      .rn_csr_ir_disp_imm_i(rn_csr_ir_disp_imm_i),
      .rn_csr_ir_disp_phy_rd_i(rn_csr_ir_disp_phy_rd_i),
      .rn_csr_ir_disp_csr_addr_i(rn_csr_ir_disp_csr_addr_i),
      .issue_rd_prf_vld_o(csr_rd_prf_vld),
      .issue_rd_prf_mask_o(csr_rd_prf_mask),
      .issue_rd_prf_tag_o(csr_rd_prf_tag),
      .issue_rd_prf_data_i(csr_rd_prf_data),
      .csr_insn_wb_vld_o(csr_insn_wb_vld),
      .csr_insn_wb_prd_o(csr_insn_wb_prd),
      .csr_insn_wb_data_o(csr_insn_wb_data),
      .csr_insn_cb_vld_o(csr_insn_cb_vld_o),
      .csr_insn_cb_rob_tag_o(csr_insn_cb_rob_tag_o),
      .csr_insn_re_vld_o(csr_insn_re_vld_o),
      .csr_insn_re_rob_tag_o(csr_insn_re_rob_tag_o),
      .csr_insn_re_cause_o(csr_insn_re_cause_o),
      .csr_insn_re_tval_o(csr_insn_re_tval_o),
      .retire_insn_vld_i(retire_insn_vld_i),
      .retire_insn_pc_i(retire_insn_pc_i),
      .retire_insn_is_br_i(retire_insn_is_br_i),
      .retire_insn_mispred_i(retire_insn_mispred_i),
      .retire_excp_vld_i(retire_excp_vld_i),
      .retire_excp_pc_i(retire_excp_pc_i),
      .retire_excp_is_rvc_i(retire_excp_is_rvc_i),
      .retire_excp_tval_i(retire_excp_tval_i),
      .retire_excp_cause_i(retire_excp_cause_i),
      .csr_redirect_vld_o(csr_redirect_vld_o),
      .csr_redirect_pc_o(csr_redirect_pc_o),
      .csr_redirect_target_o(csr_redirect_target_o),
      .rob_empty_i(rob_empty_i),
      .oldest_insn_rob_tag_i(oldest_insn_rob_tag_i),
      .interrupt_sw_i(interrupt_sw_i),
      .interrupt_timer_i(interrupt_timer_i),
      .interrupt_ext_i(interrupt_ext_i),
      .pmp_cfg_vld_o(pmp_cfg_vld_o),
      .pmp_cfg_tag_o(pmp_cfg_tag_o),
      .pmp_cfg_wr_data_o(pmp_cfg_wr_data_o),
      .pmp_cfg_rd_data_i(pmp_cfg_rd_data_i),
      .pmp_addr_vld_o(pmp_addr_vld_o),
      .pmp_addr_tag_o(pmp_addr_tag_o),
      .pmp_addr_wr_data_o(pmp_addr_wr_data_o),
      .pmp_addr_rd_data_i(pmp_addr_rd_data_i),
      .flush_icache_req_o(flush_icache_req_o),
      .flush_icache_gnt_i(flush_icache_gnt_i),
      .flush_dcache_req_o(flush_dcache_req_o),
      .flush_dcache_gnt_i(flush_dcache_gnt_i),
      .flush_itlb_req_o(flush_itlb_req_o),
      .flush_itlb_use_vpn_o(flush_itlb_use_vpn_o),
      .flush_itlb_use_asid_o(flush_itlb_use_asid_o),
      .flush_itlb_vpn_o(flush_itlb_vpn_o),
      .flush_itlb_asid_o(flush_itlb_asid_o),
      .flush_itlb_gnt_i(flush_itlb_gnt_i),
      .flush_dtlb_req_o(flush_dtlb_req_o),
      .flush_dtlb_use_vpn_o(flush_dtlb_use_vpn_o),
      .flush_dtlb_use_asid_o(flush_dtlb_use_asid_o),
      .flush_dtlb_vpn_o(flush_dtlb_vpn_o),
      .flush_dtlb_asid_o(flush_dtlb_asid_o),
      .flush_dtlb_gnt_i(flush_dtlb_gnt_i),
      .flush_stlb_req_o(flush_stlb_req_o),
      .flush_stlb_use_vpn_o(flush_stlb_use_vpn_o),
      .flush_stlb_use_asid_o(flush_stlb_use_asid_o),
      .flush_stlb_vpn_o(flush_stlb_vpn_o),
      .flush_stlb_asid_o(flush_stlb_asid_o),
      .flush_stlb_gnt_i(flush_stlb_gnt_i),
      .interrupt_ack_o(interrupt_ack_o),
      .interrupt_cause_o(interrupt_cause_o),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

endmodule : rvh_iew
