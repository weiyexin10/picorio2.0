module csr_ctrl_pipe
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
  import riscv_pkg::*;
(
    input  logic [          XLEN-1:0] hart_id_i,
    input  logic [   VADDR_WIDTH-1:0] boot_address_i,
    // CSR Status
    output logic [PRIV_LVL_WIDTH-1:0] priv_lvl_o,
    output logic                      tvm_o,
    output logic                      tw_o,
    output logic                      tsr_o,
    output logic [          XLEN-1:0] mstatus_o,
    output logic [          XLEN-1:0] satp_o,
    // Float point
    output logic [               1:0] fs_o,
    output logic [               4:0] fflags_o,
    output logic [               2:0] frm_o,
    output logic [               6:0] fprec_o,

    // Rename -> CSR Issue Register
    input logic                          rn_csr_ir_disp_vld_i,
    input logic [       VADDR_WIDTH-1:0] rn_csr_ir_disp_pc_i,
    input logic [     ROB_TAG_WIDTH-1:0] rn_csr_ir_disp_rob_tag_i,
    input logic [      CSR_OP_WIDTH-1:0] rn_csr_ir_disp_minor_op_i,
    input logic                          rn_csr_ir_disp_use_rs1_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] rn_csr_ir_disp_phy_rs1_i,
    input logic                          rn_csr_ir_disp_use_rs2_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] rn_csr_ir_disp_phy_rs2_i,
    input logic                          rn_csr_ir_disp_use_imm_i,
    input logic [                   4:0] rn_csr_ir_disp_imm_i,
    input logic [INT_PREG_TAG_WIDTH-1:0] rn_csr_ir_disp_phy_rd_i,
    input logic [                  11:0] rn_csr_ir_disp_csr_addr_i,

    // IQ -> Issue Ctrl : Issue Request
    output logic                               issue_rd_prf_vld_o,
    output logic [1:0]                         issue_rd_prf_mask_o,
    output logic [1:0][INT_PREG_TAG_WIDTH-1:0] issue_rd_prf_tag_o,
    input  logic [1:0][              XLEN-1:0] issue_rd_prf_data_i,

    // Write Back
    output logic                          csr_insn_wb_vld_o,
    output logic [INT_PREG_TAG_WIDTH-1:0] csr_insn_wb_prd_o,
    output logic [              XLEN-1:0] csr_insn_wb_data_o,

    output logic                     csr_insn_cb_vld_o,
    output logic [ROB_TAG_WIDTH-1:0] csr_insn_cb_rob_tag_o,

    output logic                        csr_insn_re_vld_o,
    output logic [   ROB_TAG_WIDTH-1:0] csr_insn_re_rob_tag_o,
    output logic [EXCP_CAUSE_WIDTH-1:0] csr_insn_re_cause_o,
    output logic [ EXCP_TVAL_WIDTH-1:0] csr_insn_re_tval_o,

    // Commit Port
    input  logic [    RETIRE_WIDTH-1:0]                  retire_insn_vld_i,
    input  logic [    RETIRE_WIDTH-1:0][VADDR_WIDTH-1:0] retire_insn_pc_i,
    input  logic [    RETIRE_WIDTH-1:0]                  retire_insn_is_br_i,
    input  logic [    RETIRE_WIDTH-1:0]                  retire_insn_mispred_i,
    // Exception Port
    input  logic                                         retire_excp_vld_i,
    input  logic [     VADDR_WIDTH-1:0]                  retire_excp_pc_i,
    input  logic                                         retire_excp_is_rvc_i,
    input  logic [ EXCP_TVAL_WIDTH-1:0]                  retire_excp_tval_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                  retire_excp_cause_i,
    // Redirect Port
    output logic                                         csr_redirect_vld_o,
    output logic [     VADDR_WIDTH-1:0]                  csr_redirect_pc_o,
    output logic [     VADDR_WIDTH-1:0]                  csr_redirect_target_o,

    // Interrupt
    input logic interrupt_sw_i,
    input logic interrupt_timer_i,
    input logic interrupt_ext_i,


    // R/W pmp config
    output logic                          pmp_cfg_vld_o,
    output logic [ PMP_CFG_TAG_WIDTH-1:0] pmp_cfg_tag_o,
    output logic [              XLEN-1:0] pmp_cfg_wr_data_o,
    input  logic [              XLEN-1:0] pmp_cfg_rd_data_i,
    // R/W pmp addr
    output logic                          pmp_addr_vld_o,
    output logic [PMP_ADDR_TAG_WIDTH-1:0] pmp_addr_tag_o,
    output logic [              XLEN-1:0] pmp_addr_wr_data_o,
    input  logic [              XLEN-1:0] pmp_addr_rd_data_i,

    output logic interrupt_ack_o,
    output logic [EXCP_CAUSE_WIDTH-1:0] interrupt_cause_o,
    // Execute FENCE.I : Flush Icache
    output logic flush_icache_req_o,
    input logic flush_icache_gnt_i,
    // Execute FENCE.I : Flush Dcache
    output logic flush_dcache_req_o,
    input logic flush_dcache_gnt_i,
    // Execute SFENCE.VMA : Flush ITLB
    output logic flush_itlb_req_o,
    output logic flush_itlb_use_vpn_o,
    output logic flush_itlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_itlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_itlb_asid_o,
    input logic flush_itlb_gnt_i,
    // Execute SFENCE.VMA : Flush DTLB
    output logic flush_dtlb_req_o,
    output logic flush_dtlb_use_vpn_o,
    output logic flush_dtlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_dtlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_dtlb_asid_o,
    input logic flush_dtlb_gnt_i,
    // Execute SFENCE.VMA : Flush STLB
    output logic flush_stlb_req_o,
    output logic flush_stlb_use_vpn_o,
    output logic flush_stlb_use_asid_o,
    output logic [VPN_WIDTH-1:0] flush_stlb_vpn_o,
    output logic [ASID_WIDTH-1:0] flush_stlb_asid_o,
    input logic flush_stlb_gnt_i,

    input logic rob_empty_i,
    input logic [ROB_TAG_WIDTH-1:0] oldest_insn_rob_tag_i,

    input logic flush_i,

    input clk,
    input rst
);

  logic i0_clk_en;

  logic i0_vld_q;
  logic [VADDR_WIDTH-1:0] i0_pc_d, i0_pc_q;
  logic [ROB_TAG_WIDTH-1:0] i0_rob_tag_d, i0_rob_tag_q;
  logic [CSR_OP_WIDTH-1:0] i0_minor_op_d, i0_minor_op_q;
  logic i0_use_imm_d, i0_use_imm_q;
  logic [4:0] i0_imm_d, i0_imm_q;
  logic i0_use_rs1_d, i0_use_rs1_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i0_phy_rs1_d, i0_phy_rs1_q;
  logic i0_use_rs2_d, i0_use_rs2_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i0_phy_rs2_d, i0_phy_rs2_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i0_phy_rd_d, i0_phy_rd_q;
  logic [11:0] i0_csr_addr_d, i0_csr_addr_q;

  logic i1_clk_en;

  logic rs1_is_zero, rs2_is_zero;

  logic i1_fire;
  logic i1_vld_q;
  logic [VADDR_WIDTH-1:0] i1_pc_d, i1_pc_q;
  logic [ROB_TAG_WIDTH-1:0] i1_rob_tag_d, i1_rob_tag_q;
  logic [CSR_OP_WIDTH-1:0] i1_minor_op_d, i1_minor_op_q;
  logic [INT_PREG_TAG_WIDTH-1:0] i1_phy_rd_d, i1_phy_rd_q;
  logic [XLEN-1:0] i1_operand0_d, i1_operand0_q;
  logic [XLEN-1:0] i1_operand1_d, i1_operand1_q;
  logic [11:0] i1_csr_addr_d, i1_csr_addr_q;
  logic i1_rs1_is_zero_d, i1_rs1_is_zero_q;
  logic i1_rs2_is_zero_d, i1_rs2_is_zero_q;

  logic csr_insn_vld;
  logic [ROB_TAG_WIDTH-1:0] fence_req_rob_tag;
  logic [VADDR_WIDTH-1:0] fence_req_pc;
  // FENCE.I
  logic fence_i_req_vld;
  // SFENCE.VMA
  logic sfence_vma_req_vld;
  logic sfence_vma_req_use_asid;
  logic sfence_vma_req_use_vpn;
  logic [VPN_WIDTH-1:0] sfence_vma_req_vpn;
  logic [ASID_WIDTH-1:0] sfence_vma_req_asid;
  
  logic csr_cb_vld;
  logic [ROB_TAG_WIDTH-1:0] csr_cb_tag;
  logic csr_redirect_vld;
  logic [VADDR_WIDTH-1:0] csr_redirect_pc;
  logic [VADDR_WIDTH-1:0] csr_redirect_target;

  logic fence_cb_vld;
  logic [ROB_TAG_WIDTH-1:0] fence_cb_tag;
  logic fence_redirect_vld;
  logic [VADDR_WIDTH-1:0] fence_redirect_pc;
  logic [VADDR_WIDTH-1:0] fence_redirect_target;

  assign i0_clk_en = rn_csr_ir_disp_vld_i;
  assign i0_pc_d = rn_csr_ir_disp_pc_i;
  assign i0_rob_tag_d = rn_csr_ir_disp_rob_tag_i;
  assign i0_minor_op_d = rn_csr_ir_disp_minor_op_i;
  assign i0_use_imm_d = rn_csr_ir_disp_use_imm_i;
  assign i0_imm_d = rn_csr_ir_disp_imm_i;
  assign i0_use_rs1_d = rn_csr_ir_disp_use_rs1_i;
  assign i0_phy_rs1_d = rn_csr_ir_disp_phy_rs1_i;
  assign i0_use_rs2_d = rn_csr_ir_disp_use_rs2_i;
  assign i0_phy_rs2_d = rn_csr_ir_disp_phy_rs2_i;
  assign i0_phy_rd_d = rn_csr_ir_disp_phy_rd_i;
  assign i0_csr_addr_d = rn_csr_ir_disp_csr_addr_i;


  assign rs1_is_zero = i0_phy_rs1_q == 0;
  assign rs2_is_zero = i0_phy_rs2_q == 0;

  assign issue_rd_prf_vld_o = i1_fire;
  assign issue_rd_prf_mask_o[0] = i0_use_rs1_q & ~rs1_is_zero;
  assign issue_rd_prf_mask_o[1] = i0_use_rs2_q & ~rs2_is_zero;
  assign issue_rd_prf_tag_o[0] = i0_phy_rs1_q;
  assign issue_rd_prf_tag_o[1] = i0_phy_rs2_q;

  assign i1_clk_en = i1_fire;
  assign i1_fire = i0_vld_q & (~rob_empty_i & (oldest_insn_rob_tag_i == i0_rob_tag_q)) & ~flush_i;
  assign i1_pc_d = i0_pc_q;
  assign i1_rob_tag_d = i0_rob_tag_q;
  assign i1_minor_op_d = i0_minor_op_q;
  assign i1_phy_rd_d = i0_phy_rd_q;
  assign i1_csr_addr_d = i0_csr_addr_q;
  assign i1_rs1_is_zero_d = rs1_is_zero;
  assign i1_rs2_is_zero_d = rs2_is_zero;

  always_comb begin : i1_operand0_sel
    casez ({
      i0_use_rs1_q, rs1_is_zero, i0_use_imm_q
    })
      3'b11?:  i1_operand0_d = {XLEN{1'b0}};
      3'b10?:  i1_operand0_d = issue_rd_prf_data_i[0];
      3'b0?1:  i1_operand0_d = {{XLEN - 5{1'b0}}, i0_imm_q};
      default: i1_operand0_d = {XLEN{1'b0}};
    endcase
  end

  always_comb begin : i1_operand1_sel
    casez ({
      i0_use_rs2_q, rs2_is_zero
    })
      2'b10:   i1_operand1_d = issue_rd_prf_data_i[1];
      default: i1_operand1_d = {XLEN{1'b0}};
    endcase
  end

  always_ff @(posedge clk) begin : i0_vld_dff
    if (rst) begin
      i0_vld_q <= 1'b0;
    end else begin
      if (rn_csr_ir_disp_vld_i) begin
        i0_vld_q <= 1'b1;
      end
      if (i1_fire) begin
        i0_vld_q <= 1'b0;
      end
      if (flush_i) begin
        i0_vld_q <= 1'b0;
      end
    end
  end

  always_ff @(posedge clk) begin : i0_payload_dff
    if (i0_clk_en) begin
      i0_pc_q <= i0_pc_d;
      i0_rob_tag_q <= i0_rob_tag_d;
      i0_minor_op_q <= i0_minor_op_d;
      i0_use_imm_q <= i0_use_imm_d;
      i0_imm_q <= i0_imm_d;
      i0_use_rs1_q <= i0_use_rs1_d;
      i0_phy_rs1_q <= i0_phy_rs1_d;
      i0_use_rs2_q <= i0_use_rs2_d;
      i0_phy_rs2_q <= i0_phy_rs2_d;
      i0_phy_rd_q <= i0_phy_rd_d;
      i0_csr_addr_q <= i0_csr_addr_d;
    end
  end

  always_ff @(posedge clk) begin : i1_vld_dff
    if (rst) begin
      i1_vld_q <= 1'b0;
    end else begin
      if (i1_fire) begin
        i1_vld_q <= 1'b1;
      end else begin
        i1_vld_q <= 1'b0;
      end
    end
  end

  always_ff @(posedge clk) begin : i1_payload_dff
    if (i1_clk_en) begin
      i1_pc_q <= i1_pc_d;
      i1_rob_tag_q <= i1_rob_tag_d;
      i1_operand0_q <= i1_operand0_d;
      i1_operand1_q <= i1_operand1_d;
      i1_minor_op_q <= i1_minor_op_d;
      i1_phy_rd_q <= i1_phy_rd_d;
      i1_csr_addr_q <= i1_csr_addr_d;
      i1_rs1_is_zero_q <= i1_rs1_is_zero_d;
      i1_rs2_is_zero_q <= i1_rs2_is_zero_d;
    end
  end

  assign csr_insn_vld = i1_vld_q & ((i1_minor_op_q != CSR_FENCEI) & (i1_minor_op_q != CSR_SFENCE_VMA));
  assign fence_req_rob_tag = i1_rob_tag_q;
  assign fence_req_pc = i1_pc_q;
  // FENCE.I
  assign fence_i_req_vld = i1_vld_q & i1_minor_op_q == CSR_FENCEI;
  // SFENCE.VMA
  assign sfence_vma_req_vld = i1_vld_q & i1_minor_op_q == CSR_SFENCE_VMA;
  assign sfence_vma_req_use_asid = ~i1_rs2_is_zero_q;
  assign sfence_vma_req_use_vpn = ~i1_rs1_is_zero_q;
  assign sfence_vma_req_vpn = i1_operand0_q[VADDR_WIDTH-1 -: VPN_WIDTH];
  assign sfence_vma_req_asid = i1_operand1_q[ASID_WIDTH-1:0];

  assign csr_insn_cb_vld_o = csr_cb_vld | fence_cb_vld;
  assign csr_insn_cb_rob_tag_o = ({ROB_TAG_WIDTH{csr_cb_vld}} & csr_cb_tag) |
    ({ROB_TAG_WIDTH{fence_cb_vld}} & fence_cb_tag);

  assign csr_redirect_vld_o = csr_redirect_vld | fence_redirect_vld;
  assign csr_redirect_pc_o =  ({VADDR_WIDTH{csr_redirect_vld}} & csr_redirect_pc) | 
    ({VADDR_WIDTH{fence_redirect_vld}} & fence_redirect_pc);
  assign csr_redirect_target_o =  ({VADDR_WIDTH{csr_redirect_vld}} & csr_redirect_target) | 
    ({VADDR_WIDTH{fence_redirect_vld}} & fence_redirect_target);

  fence_unit u_fence_unit (
      .fence_req_rob_tag_i(fence_req_rob_tag),
      .fence_req_pc_i(fence_req_pc),
      .fence_i_req_vld_i(fence_i_req_vld),
      .sfence_vma_req_vld_i(sfence_vma_req_vld),
      .sfence_vma_req_use_asid_i(sfence_vma_req_use_asid),
      .sfence_vma_req_use_vpn_i(sfence_vma_req_use_vpn),
      .sfence_vma_req_vpn_i(sfence_vma_req_vpn),
      .sfence_vma_req_asid_i(sfence_vma_req_asid),
      .flush_icache_req_o(flush_icache_req_o),
      .flush_icache_gnt_i(flush_icache_gnt_i),
      .flush_dcache_req_o(flush_dcache_req_o),
      .flush_dcache_gnt_i(flush_dcache_gnt_i),
      .flush_itlb_req_o(flush_itlb_req_o),
      .flush_itlb_use_vpn_o(flush_itlb_use_vpn_o),
      .flush_itlb_use_asid_o(flush_itlb_use_asid_o),
      .flush_itlb_vpn_o(flush_itlb_vpn_o),
      .flush_itlb_asid_o(flush_itlb_asid_o),
      .flush_itlb_gnt_i(flush_itlb_gnt_i),
      .flush_dtlb_req_o(flush_dtlb_req_o),
      .flush_dtlb_use_vpn_o(flush_dtlb_use_vpn_o),
      .flush_dtlb_use_asid_o(flush_dtlb_use_asid_o),
      .flush_dtlb_vpn_o(flush_dtlb_vpn_o),
      .flush_dtlb_asid_o(flush_dtlb_asid_o),
      .flush_dtlb_gnt_i(flush_dtlb_gnt_i),
      .flush_stlb_req_o(flush_stlb_req_o),
      .flush_stlb_use_vpn_o(flush_stlb_use_vpn_o),
      .flush_stlb_use_asid_o(flush_stlb_use_asid_o),
      .flush_stlb_vpn_o(flush_stlb_vpn_o),
      .flush_stlb_asid_o(flush_stlb_asid_o),
      .flush_stlb_gnt_i(flush_stlb_gnt_i),
      .rob_cb_vld_o(fence_cb_vld),
      .rob_cb_tag_o(fence_cb_tag),
      .redirect_vld_o(fence_redirect_vld),
      .redirect_pc_o(fence_redirect_pc),
      .redirect_target_o(fence_redirect_target),
      .clk(clk),
      .rst(rst)
  );


  csr_regfile u_csr_regfile (
      .hart_id_i(hart_id_i),
      .boot_address_i(boot_address_i),
      .priv_lvl_o(priv_lvl_o),
      .tvm_o(tvm_o),
      .tw_o(tw_o),
      .tsr_o(tsr_o),
      .mstatus_o(mstatus_o),
      .satp_o(satp_o),
      .fs_o(fs_o),
      .fflags_o(fflags_o),
      .frm_o(frm_o),
      .fprec_o(fprec_o),
      .csr_insn_vld_i(csr_insn_vld),
      .csr_insn_pc_i(i1_pc_q),
      .csr_insn_rob_tag_i(i1_rob_tag_q),
      .csr_insn_op_i(i1_minor_op_q),
      .csr_insn_addr_i(i1_csr_addr_q),
      .csr_insn_prd_i(i1_phy_rd_q),
      .csr_insn_operand0_i(i1_operand0_q),
      .csr_insn_wb_vld_o(csr_insn_wb_vld_o),
      .csr_insn_wb_prd_o(csr_insn_wb_prd_o),
      .csr_insn_wb_data_o(csr_insn_wb_data_o),
      .csr_insn_cb_vld_o(csr_cb_vld),
      .csr_insn_cb_rob_tag_o(csr_cb_tag),
      .csr_insn_re_vld_o(csr_insn_re_vld_o),
      .csr_insn_re_rob_tag_o(csr_insn_re_rob_tag_o),
      .csr_insn_re_cause_o(csr_insn_re_cause_o),
      .csr_insn_re_tval_o(csr_insn_re_tval_o),
      .retire_insn_vld_i(retire_insn_vld_i),
      .retire_insn_pc_i(retire_insn_pc_i),
      .retire_insn_is_br_i(retire_insn_is_br_i),
      .retire_insn_mispred_i(retire_insn_mispred_i),
      .retire_excp_vld_i(retire_excp_vld_i),
      .retire_excp_pc_i(retire_excp_pc_i),
      .retire_excp_is_rvc_i(retire_excp_is_rvc_i),
      .retire_excp_tval_i(retire_excp_tval_i),
      .retire_excp_cause_i(retire_excp_cause_i),
      .csr_redirect_vld_o(csr_redirect_vld),
      .csr_redirect_pc_o(csr_redirect_pc),
      .csr_redirect_target_o(csr_redirect_target),
      .interrupt_sw_i(interrupt_sw_i),
      .interrupt_timer_i(interrupt_timer_i),
      .interrupt_ext_i(interrupt_ext_i),
      .pmp_cfg_vld_o(pmp_cfg_vld_o),
      .pmp_cfg_tag_o(pmp_cfg_tag_o),
      .pmp_cfg_wr_data_o(pmp_cfg_wr_data_o),
      .pmp_cfg_rd_data_i(pmp_cfg_rd_data_i),
      .pmp_addr_vld_o(pmp_addr_vld_o),
      .pmp_addr_tag_o(pmp_addr_tag_o),
      .pmp_addr_wr_data_o(pmp_addr_wr_data_o),
      .pmp_addr_rd_data_i(pmp_addr_rd_data_i),
      .interrupt_ack_o(interrupt_ack_o),
      .interrupt_cause_o(interrupt_cause_o),
      .clk(clk),
      .rst(rst)
  );


endmodule
