module agu_iq_entry
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
    import rvh_rcu_pkg::*;
#(
    parameter int unsigned ENTRY_TAG_WIDTH = 4,
    parameter int unsigned LOCAL_TAG = 0
) (
    // Dispatch
    input  logic                                                  disp_vld_i,
    input  logic [     ROB_TAG_WIDTH-1:0]                         disp_rob_tag_i,
    input  logic [     LSQ_TAG_WIDTH-1:0]                         disp_lsq_tag_i,
    input  logic [    LSU_TYPE_WIDTH-1:0]                         disp_fu_type_i,
    input  logic [      LSU_OP_WIDTH-1:0]                         disp_opcode_i,
    input  logic                                                  disp_rs1_rdy_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0]                         disp_phy_rs1_i,
    input  logic [INT_PREG_TAG_WIDTH-1:0]                         disp_phy_rd_i,
    input  logic [                  11:0]                         disp_imm_i,
    // Issue
    input  logic                                                  issue_vld_i,
    output logic [     ROB_TAG_WIDTH-1:0]                         issue_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                         issue_lsq_tag_o,
    output logic [   ENTRY_TAG_WIDTH-1:0]                         issue_iq_tag_o,
    output logic [    LSU_TYPE_WIDTH-1:0]                         issue_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                         issue_opcode_o,
    output logic [  INT_CDB_ID_WIDTH-1:0]                         issue_rs1_cdb_id_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         issue_phy_rs1_o,
    output logic [    PREG_TAG_WIDTH-1:0]                         issue_phy_rd_o,
    output logic [                  11:0]                         issue_imm_o,
    // Status
    output logic                                                  issue_rdy_o,
    input  logic                                                  entry_vld_i,
    // Commit
    input  logic                                                  commit_vld_i,
    input  logic                                                  commit_reactivate_i,
    // Early Wakeup
    input  logic [ INT_CDB_BUS_WIDTH-1:0]                         wakeup_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] wakeup_ptag_i,
    // Forward
    input  logic [ INT_CDB_BUS_WIDTH-1:0]                         forward_vld_i,
    input  logic [ INT_CDB_BUS_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] forward_ptag_i,

    input logic flush_i,

    input clk,
    input rst
);
    logic forward_clk_en;
    logic disp_clk_en;

    // Issue Queue Entry Payload
    logic issued_q;
    logic [ROB_TAG_WIDTH-1:0] rob_tag_q;
    logic [LSQ_TAG_WIDTH-1:0] lsq_tag_q;
    logic [LSU_TYPE_WIDTH-1:0] fu_type_q;
    logic [LSU_OP_WIDTH-1:0] opcode_q;
    logic [INT_CDB_ID_WIDTH-1:0] rs1_cdb_id_d,rs1_cdb_id_q;
    logic [INT_PREG_TAG_WIDTH-1:0] phy_rs1_q;
    logic rs1_rdy_d,rs1_rdy_q;
    logic [PREG_TAG_WIDTH-1:0] phy_rd_q;
    logic [11:0] imm_q;

    assign disp_clk_en        = disp_vld_i;
    assign forward_clk_en     = (|forward_vld_i) | (|wakeup_vld_i);

    assign issue_rdy_o        = entry_vld_i & ~issued_q & rs1_rdy_d;

    assign issue_rob_tag_o    = rob_tag_q;
    assign issue_lsq_tag_o    = lsq_tag_q;
    assign issue_iq_tag_o     = LOCAL_TAG[ENTRY_TAG_WIDTH-1:0];
    assign issue_fu_type_o    = fu_type_q;
    assign issue_opcode_o     = opcode_q;
    assign issue_rs1_cdb_id_o = rs1_cdb_id_d;
    assign issue_phy_rs1_o    = phy_rs1_q;
    assign issue_phy_rd_o     = phy_rd_q;
    assign issue_imm_o        = imm_q;

    always_comb begin
        rs1_rdy_d = rs1_rdy_q;
        rs1_cdb_id_d = rs1_cdb_id_q;
        for (int i = 0; i < INT_CDB_BUS_WIDTH; i++) begin
            if (wakeup_vld_i[i] & (phy_rs1_q == wakeup_ptag_i[i])) begin
                rs1_rdy_d = 1'b1;
                rs1_cdb_id_d = i[INT_CDB_ID_WIDTH-1:0];
            end
        end
        for (int i = 0; i < INT_CDB_BUS_WIDTH; i++) begin
            if (forward_vld_i[i] & (phy_rs1_q == forward_ptag_i[i])) begin
                rs1_rdy_d = 1'b1;
                rs1_cdb_id_d = i[INT_CDB_ID_WIDTH-1:0];
            end
        end
        if (disp_vld_i) begin
            rs1_cdb_id_d = {INT_CDB_ID_WIDTH{1'b0}};
            rs1_rdy_d    = disp_rs1_rdy_i;
        end
    end


    always_ff @(posedge clk) begin : issued_dff
        if (rst) begin
            issued_q <= 1'b0;
        end else begin
            if (disp_vld_i) begin
                issued_q <= 1'b0;
            end else if (issue_vld_i) begin
                issued_q <= 1'b1;
            end else if (commit_vld_i & commit_reactivate_i) begin
                issued_q <= 1'b0;
            end
        end
    end

    always_ff @(posedge clk) begin : rs1_ctrl_dff
        if(forward_clk_en | disp_clk_en) begin
            rs1_cdb_id_q <= rs1_cdb_id_d;
            rs1_rdy_q <= rs1_rdy_d;
        end
    end

    always_ff @(posedge clk) begin : entry_payload_dff
        if (disp_clk_en) begin
            rob_tag_q <= disp_rob_tag_i;
            lsq_tag_q <= disp_lsq_tag_i;
            fu_type_q <= disp_fu_type_i;
            opcode_q  <= disp_opcode_i;
            phy_rs1_q <= disp_phy_rs1_i;
            phy_rd_q  <= disp_phy_rd_i;
            imm_q     <= disp_imm_i;
        end
    end


endmodule : agu_iq_entry
