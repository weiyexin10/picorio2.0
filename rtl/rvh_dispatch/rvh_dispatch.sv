module rvh_dispatch
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
(
    // Rename -> Dispatch Buffer(INT)
    input  logic [      RENAME_WIDTH-1:0]                         rn_int_disp_vld_i,
`ifdef SIMULATION
    input  logic [      RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_int_disp_pc_i,
`endif
    input  logic [      RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_int_disp_rob_tag_i,
    input  logic [      RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_int_disp_brq_tag_i,
    input  logic [      RENAME_WIDTH-1:0][    INT_TYPE_WIDTH-1:0] rn_int_disp_major_op_i,
    input  logic [      RENAME_WIDTH-1:0][      INT_OP_WIDTH-1:0] rn_int_disp_minor_op_i,
    input  logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_rs1_i,
    input  logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_rs2_i,
    input  logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_rd_i,
    input  logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs1_i,
    input  logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs2_i,
    input  logic [      RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rd_i,
    input  logic [      RENAME_WIDTH-1:0][                  31:0] rn_int_disp_imm_i,
    input  logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_imm_i,
    input  logic [      RENAME_WIDTH-1:0]                         rn_int_disp_use_pc_i,
    output logic                                                  rn_int_disp_rdy_o,
    // Rename -> Dispatch Buffer(LS)
    input  logic [      RENAME_WIDTH-1:0]                         rn_ls_disp_vld_i,
`ifdef SIMULATION
    input  logic [      RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_ls_disp_pc_i,
`endif
    input  logic [      RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_ls_disp_rob_tag_i,
    input  logic [      RENAME_WIDTH-1:0][    LSU_TYPE_WIDTH-1:0] rn_ls_disp_major_op_i,
    input  logic [      RENAME_WIDTH-1:0][      LSU_OP_WIDTH-1:0] rn_ls_disp_minor_op_i,
    input  logic [      RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs1_i,
    input  logic [      RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs2_i,
    input  logic [      RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rd_i,
    input  logic [      RENAME_WIDTH-1:0][                  11:0] rn_ls_disp_imm_i,
    input  logic [      RENAME_WIDTH-1:0]                         rn_ls_disp_is_fence_i,
    output logic                                                  rn_ls_disp_rdy_o,
    // LS Router -> AGU IQ0
    output logic                                                  disp_agu0_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_agu0_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_agu0_iq_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                         disp_agu0_iq_lsq_tag_o,
    output logic [    LSU_TYPE_WIDTH-1:0]                         disp_agu0_iq_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                         disp_agu0_iq_opcode_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu0_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu0_iq_phy_rd_o,
    output logic [                  11:0]                         disp_agu0_iq_imm_o,
    input  logic                                                  disp_agu0_iq_rdy_i,
    // LS Router -> SDU IQ0
    output logic                                                  disp_sdu0_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_sdu0_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_sdu0_iq_rob_tag_o,
    output logic [     STQ_TAG_WIDTH-1:0]                         disp_sdu0_iq_stq_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_sdu0_iq_phy_rs2_o,
    input  logic                                                  disp_sdu0_iq_rdy_i,
    // LS Router -> AGU IQ1
    output logic                                                  disp_agu1_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_agu1_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_agu1_iq_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                         disp_agu1_iq_lsq_tag_o,
    output logic [    LSU_TYPE_WIDTH-1:0]                         disp_agu1_iq_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                         disp_agu1_iq_opcode_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu1_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_agu1_iq_phy_rd_o,
    output logic [                  11:0]                         disp_agu1_iq_imm_o,
    input  logic                                                  disp_agu1_iq_rdy_i,
    // LS Router -> SDU IQ1
    output logic                                                  disp_sdu1_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_sdu1_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_sdu1_iq_rob_tag_o,
    output logic [     STQ_TAG_WIDTH-1:0]                         disp_sdu1_iq_stq_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_sdu1_iq_phy_rs2_o,
    input  logic                                                  disp_sdu1_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(BR)
    output logic                                                  disp_br_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_br_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_br_iq_rob_tag_o,
    output logic [     BRQ_TAG_WIDTH-1:0]                         disp_br_iq_brq_tag_o,
    output logic [      BRU_OP_WIDTH-1:0]                         disp_br_iq_opcode_o,
    output logic                                                  disp_br_iq_use_rs1_o,
    output logic                                                  disp_br_iq_use_rs2_o,
    output logic                                                  disp_br_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_br_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_br_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_br_iq_phy_rd_o,
    output logic [                  31:0]                         disp_br_iq_imm_o,
    input  logic                                                  disp_br_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(ALU0)
    output logic                                                  disp_alu0_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_alu0_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_alu0_iq_rob_tag_o,
    output logic [      ALU_OP_WIDTH-1:0]                         disp_alu0_iq_opcode_o,
    output logic                                                  disp_alu0_iq_use_rs1_o,
    output logic                                                  disp_alu0_iq_use_rs2_o,
    output logic                                                  disp_alu0_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu0_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu0_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu0_iq_phy_rd_o,
    output logic [                  31:0]                         disp_alu0_iq_imm_o,
    output logic                                                  disp_alu0_iq_use_imm_o,
    output logic                                                  disp_alu0_iq_use_pc_o,
    input  logic                                                  disp_alu0_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(ALU1)
    output logic                                                  disp_alu1_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_alu1_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_alu1_iq_rob_tag_o,
    output logic [      ALU_OP_WIDTH-1:0]                         disp_alu1_iq_opcode_o,
    output logic                                                  disp_alu1_iq_use_rs1_o,
    output logic                                                  disp_alu1_iq_use_rs2_o,
    output logic                                                  disp_alu1_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu1_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu1_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu1_iq_phy_rd_o,
    output logic [                  31:0]                         disp_alu1_iq_imm_o,
    output logic                                                  disp_alu1_iq_use_imm_o,
    output logic                                                  disp_alu1_iq_use_pc_o,
    input  logic                                                  disp_alu1_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(Complex ALU2)  
    output logic                                                  disp_alu2_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_alu2_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_alu2_iq_rob_tag_o,
    output logic [                   1:0]                         disp_alu2_iq_fu_type_o,
    output logic [      ALU_OP_WIDTH-1:0]                         disp_alu2_iq_opcode_o,
    output logic                                                  disp_alu2_iq_use_rs1_o,
    output logic                                                  disp_alu2_iq_use_rs2_o,
    output logic                                                  disp_alu2_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu2_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu2_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu2_iq_phy_rd_o,
    output logic [                  31:0]                         disp_alu2_iq_imm_o,
    output logic                                                  disp_alu2_iq_use_imm_o,
    output logic                                                  disp_alu2_iq_use_pc_o,
    input  logic                                                  disp_alu2_iq_rdy_i,
    // disp -> LSQ
    output logic [    LSU_DISP_WIDTH-1:0]                         disp_lsq_alloc_vld_o,
    output logic [    LSU_DISP_WIDTH-1:0][    LSU_TYPE_WIDTH-1:0] disp_lsq_alloc_fu_type_o,
    output logic [    LSU_DISP_WIDTH-1:0][      LSU_OP_WIDTH-1:0] disp_lsq_alloc_minor_op_o,
    output logic [    LSU_DISP_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] disp_lsq_alloc_rob_tag_o,
    output logic [    LSU_DISP_WIDTH-1:0]                         disp_lsq_alloc_is_fence_o,
    input  logic [    LSU_DISP_WIDTH-1:0][     LSQ_TAG_WIDTH-1:0] disp_lsq_alloc_lsq_tag_i,
    input  logic [    LSU_DISP_WIDTH-1:0]                         disp_lsq_alloc_rdy_i,

    input logic flush_i,
    input       clk,
    input       rst
);

  // Dispatch Buffer(LS) -> LS Router
  logic [LSU_DISP_WIDTH-1:0] ls_disp_iq_vld;
  logic [LSU_DISP_WIDTH-1:0] ls_disp_router_vld;
`ifdef SIMULATION
  logic [LSU_DISP_WIDTH-1:0][VADDR_WIDTH-1:0] ls_disp_iq_pc;
`endif
  logic [LSU_DISP_WIDTH-1:0][ ROB_TAG_WIDTH-1:0] ls_disp_iq_rob_tag;
  logic [LSU_DISP_WIDTH-1:0][LSU_TYPE_WIDTH-1:0] ls_disp_iq_major_op;
  logic [LSU_DISP_WIDTH-1:0][  LSU_OP_WIDTH-1:0] ls_disp_iq_minor_op;
  logic [LSU_DISP_WIDTH-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rs1;
  logic [LSU_DISP_WIDTH-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rs2;
  logic [LSU_DISP_WIDTH-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rd;
  logic [LSU_DISP_WIDTH-1:0][              11:0] ls_disp_iq_imm;
  logic [LSU_DISP_WIDTH-1:0]                     ls_disp_iq_is_fence;
  logic [LSU_DISP_WIDTH-1:0]                     ls_disp_iq_rdy;

  logic [LSU_DISP_WIDTH-1:0]                     ls_disp_iq_fire;

  // Dispatch Buffer(INT) -> INT Router
  logic [INT_DISP_WIDTH-1:0]                     int_disp_iq_vld;
`ifdef SIMULATION
  logic [INT_DISP_WIDTH-1:0][VADDR_WIDTH-1:0] int_disp_iq_pc;
`endif
  logic [INT_DISP_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] int_disp_iq_rob_tag;
  logic [INT_DISP_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] int_disp_iq_brq_tag;
  logic [INT_DISP_WIDTH-1:0][    INT_TYPE_WIDTH-1:0] int_disp_iq_major_op;
  logic [INT_DISP_WIDTH-1:0][      INT_OP_WIDTH-1:0] int_disp_iq_minor_op;
  logic [INT_DISP_WIDTH-1:0]                         int_disp_iq_use_rs1;
  logic [INT_DISP_WIDTH-1:0]                         int_disp_iq_use_rs2;
  logic [INT_DISP_WIDTH-1:0]                         int_disp_iq_use_rd;
  logic [INT_DISP_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rs1;
  logic [INT_DISP_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rs2;
  logic [INT_DISP_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rd;
  logic [INT_DISP_WIDTH-1:0][                  31:0] int_disp_iq_imm;
  logic [INT_DISP_WIDTH-1:0]                         int_disp_iq_use_imm;
  logic [INT_DISP_WIDTH-1:0]                         int_disp_iq_use_pc;
  logic [INT_DISP_WIDTH-1:0]                         int_disp_iq_rdy;


  assign ls_disp_router_vld = ls_disp_iq_vld & disp_lsq_alloc_rdy_i;
  assign ls_disp_iq_fire = ls_disp_iq_vld & ls_disp_iq_rdy;

  generate
    for (genvar i = 0; i < LSU_DISP_WIDTH; i++) begin
      assign disp_lsq_alloc_vld_o[i] = ls_disp_iq_fire[i];
      assign disp_lsq_alloc_fu_type_o[i] = ls_disp_iq_major_op[i];
      assign disp_lsq_alloc_rob_tag_o[i] = ls_disp_iq_rob_tag[i];
      assign disp_lsq_alloc_is_fence_o[i] = ls_disp_iq_is_fence[i];
      assign disp_lsq_alloc_minor_op_o[i] = ls_disp_iq_minor_op[i];
    end
  endgenerate



  ls_disp_buffer u_ls_disp_buffer (
      .rn_ls_disp_vld_i(rn_ls_disp_vld_i),
`ifdef SIMULATION
      .rn_ls_disp_pc_i(rn_ls_disp_pc_i),
`endif
      .rn_ls_disp_rob_tag_i(rn_ls_disp_rob_tag_i),
      .rn_ls_disp_major_op_i(rn_ls_disp_major_op_i),
      .rn_ls_disp_minor_op_i(rn_ls_disp_minor_op_i),
      .rn_ls_disp_phy_rs1_i(rn_ls_disp_phy_rs1_i),
      .rn_ls_disp_phy_rs2_i(rn_ls_disp_phy_rs2_i),
      .rn_ls_disp_phy_rd_i(rn_ls_disp_phy_rd_i),
      .rn_ls_disp_imm_i(rn_ls_disp_imm_i),
      .rn_ls_disp_is_fence_i(rn_ls_disp_is_fence_i),
      .rn_ls_disp_rdy_o(rn_ls_disp_rdy_o),
      .ls_disp_iq_vld_o(ls_disp_iq_vld),
`ifdef SIMULATION
      .ls_disp_iq_pc_o(ls_disp_iq_pc),
`endif
      .ls_disp_iq_rob_tag_o(ls_disp_iq_rob_tag),
      .ls_disp_iq_major_op_o(ls_disp_iq_major_op),
      .ls_disp_iq_minor_op_o(ls_disp_iq_minor_op),
      .ls_disp_iq_phy_rs1_o(ls_disp_iq_phy_rs1),
      .ls_disp_iq_phy_rs2_o(ls_disp_iq_phy_rs2),
      .ls_disp_iq_phy_rd_o(ls_disp_iq_phy_rd),
      .ls_disp_iq_imm_o(ls_disp_iq_imm),
      .ls_disp_iq_is_fence_o(ls_disp_iq_is_fence),
      .ls_disp_iq_rdy_i(ls_disp_iq_rdy),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

  ls_disp_router u_ls_disp_router (
      .ls_disp_iq_vld_i(ls_disp_router_vld),
`ifdef SIMULATION
      .ls_disp_iq_pc_i(ls_disp_iq_pc),
`endif
      .ls_disp_iq_rob_tag_i(ls_disp_iq_rob_tag),
      .ls_disp_iq_lsq_tag_i(disp_lsq_alloc_lsq_tag_i),
      .ls_disp_iq_major_op_i(ls_disp_iq_major_op),
      .ls_disp_iq_minor_op_i(ls_disp_iq_minor_op),
      .ls_disp_iq_phy_rs1_i(ls_disp_iq_phy_rs1),
      .ls_disp_iq_phy_rs2_i(ls_disp_iq_phy_rs2),
      .ls_disp_iq_phy_rd_i(ls_disp_iq_phy_rd),
      .ls_disp_iq_imm_i(ls_disp_iq_imm),
      .ls_disp_iq_rdy_o(ls_disp_iq_rdy),
      .disp_agu0_iq_vld_o(disp_agu0_iq_vld_o),
`ifdef SIMULATION
      .disp_agu0_iq_pc_o(disp_agu0_iq_pc_o),
`endif
      .disp_agu0_iq_rob_tag_o(disp_agu0_iq_rob_tag_o),
      .disp_agu0_iq_lsq_tag_o(disp_agu0_iq_lsq_tag_o),
      .disp_agu0_iq_fu_type_o(disp_agu0_iq_fu_type_o),
      .disp_agu0_iq_opcode_o(disp_agu0_iq_opcode_o),
      .disp_agu0_iq_phy_rs1_o(disp_agu0_iq_phy_rs1_o),
      .disp_agu0_iq_phy_rd_o(disp_agu0_iq_phy_rd_o),
      .disp_agu0_iq_imm_o(disp_agu0_iq_imm_o),
      .disp_agu0_iq_rdy_i(disp_agu0_iq_rdy_i),
      .disp_sdu0_iq_vld_o(disp_sdu0_iq_vld_o),
`ifdef SIMULATION
      .disp_sdu0_iq_pc_o(disp_sdu0_iq_pc_o),
`endif
      .disp_sdu0_iq_rob_tag_o(disp_sdu0_iq_rob_tag_o),
      .disp_sdu0_iq_stq_tag_o(disp_sdu0_iq_stq_tag_o),
      .disp_sdu0_iq_phy_rs2_o(disp_sdu0_iq_phy_rs2_o),
      .disp_sdu0_iq_rdy_i(disp_sdu0_iq_rdy_i),
      .disp_agu1_iq_vld_o(disp_agu1_iq_vld_o),
`ifdef SIMULATION
      .disp_agu1_iq_pc_o(disp_agu1_iq_pc_o),
`endif
      .disp_agu1_iq_rob_tag_o(disp_agu1_iq_rob_tag_o),
      .disp_agu1_iq_lsq_tag_o(disp_agu1_iq_lsq_tag_o),
      .disp_agu1_iq_fu_type_o(disp_agu1_iq_fu_type_o),
      .disp_agu1_iq_opcode_o(disp_agu1_iq_opcode_o),
      .disp_agu1_iq_phy_rs1_o(disp_agu1_iq_phy_rs1_o),
      .disp_agu1_iq_phy_rd_o(disp_agu1_iq_phy_rd_o),
      .disp_agu1_iq_imm_o(disp_agu1_iq_imm_o),
      .disp_agu1_iq_rdy_i(disp_agu1_iq_rdy_i),
      .disp_sdu1_iq_vld_o(disp_sdu1_iq_vld_o),
`ifdef SIMULATION
      .disp_sdu1_iq_pc_o(disp_sdu1_iq_pc_o),
`endif
      .disp_sdu1_iq_rob_tag_o(disp_sdu1_iq_rob_tag_o),
      .disp_sdu1_iq_stq_tag_o(disp_sdu1_iq_stq_tag_o),
      .disp_sdu1_iq_phy_rs2_o(disp_sdu1_iq_phy_rs2_o),
      .disp_sdu1_iq_rdy_i(disp_sdu1_iq_rdy_i)
  );

  int_disp_buffer u_int_disp_buffer (
      .rn_int_disp_vld_i(rn_int_disp_vld_i),
`ifdef SIMULATION
      .rn_int_disp_pc_i(rn_int_disp_pc_i),
`endif
      .rn_int_disp_rob_tag_i(rn_int_disp_rob_tag_i),
      .rn_int_disp_brq_tag_i(rn_int_disp_brq_tag_i),
      .rn_int_disp_major_op_i(rn_int_disp_major_op_i),
      .rn_int_disp_minor_op_i(rn_int_disp_minor_op_i),
      .rn_int_disp_use_rs1_i(rn_int_disp_use_rs1_i),
      .rn_int_disp_use_rs2_i(rn_int_disp_use_rs2_i),
      .rn_int_disp_use_rd_i(rn_int_disp_use_rd_i),
      .rn_int_disp_phy_rs1_i(rn_int_disp_phy_rs1_i),
      .rn_int_disp_phy_rs2_i(rn_int_disp_phy_rs2_i),
      .rn_int_disp_phy_rd_i(rn_int_disp_phy_rd_i),
      .rn_int_disp_imm_i(rn_int_disp_imm_i),
      .rn_int_disp_use_imm_i(rn_int_disp_use_imm_i),
      .rn_int_disp_use_pc_i(rn_int_disp_use_pc_i),
      .rn_int_disp_rdy_o(rn_int_disp_rdy_o),
      .int_disp_iq_vld_o(int_disp_iq_vld),
`ifdef SIMULATION
      .int_disp_iq_pc_o(int_disp_iq_pc),
`endif
      .int_disp_iq_rob_tag_o(int_disp_iq_rob_tag),
      .int_disp_iq_brq_tag_o(int_disp_iq_brq_tag),
      .int_disp_iq_major_op_o(int_disp_iq_major_op),
      .int_disp_iq_minor_op_o(int_disp_iq_minor_op),
      .int_disp_iq_use_rs1_o(int_disp_iq_use_rs1),
      .int_disp_iq_use_rs2_o(int_disp_iq_use_rs2),
      .int_disp_iq_use_rd_o(int_disp_iq_use_rd),
      .int_disp_iq_phy_rs1_o(int_disp_iq_phy_rs1),
      .int_disp_iq_phy_rs2_o(int_disp_iq_phy_rs2),
      .int_disp_iq_phy_rd_o(int_disp_iq_phy_rd),
      .int_disp_iq_imm_o(int_disp_iq_imm),
      .int_disp_iq_use_imm_o(int_disp_iq_use_imm),
      .int_disp_iq_use_pc_o(int_disp_iq_use_pc),
      .int_disp_iq_rdy_i(int_disp_iq_rdy),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );


  int_disp_router u_int_disp_router (
      .int_disp_iq_vld_i(int_disp_iq_vld),
`ifdef SIMULATION
      .int_disp_iq_pc_i(int_disp_iq_pc),
`endif
      .int_disp_iq_rob_tag_i(int_disp_iq_rob_tag),
      .int_disp_iq_brq_tag_i(int_disp_iq_brq_tag),
      .int_disp_iq_major_op_i(int_disp_iq_major_op),
      .int_disp_iq_minor_op_i(int_disp_iq_minor_op),
      .int_disp_iq_use_rs1_i(int_disp_iq_use_rs1),
      .int_disp_iq_use_rs2_i(int_disp_iq_use_rs2),
      .int_disp_iq_use_rd_i(int_disp_iq_use_rd),
      .int_disp_iq_phy_rs1_i(int_disp_iq_phy_rs1),
      .int_disp_iq_phy_rs2_i(int_disp_iq_phy_rs2),
      .int_disp_iq_phy_rd_i(int_disp_iq_phy_rd),
      .int_disp_iq_imm_i(int_disp_iq_imm),
      .int_disp_iq_use_imm_i(int_disp_iq_use_imm),
      .int_disp_iq_use_pc_i(int_disp_iq_use_pc),
      .int_disp_iq_rdy_o(int_disp_iq_rdy),
      .disp_br_iq_vld_o(disp_br_iq_vld_o),
`ifdef SIMULATION
      .disp_br_iq_pc_o(disp_br_iq_pc_o),
`endif
      .disp_br_iq_rob_tag_o(disp_br_iq_rob_tag_o),
      .disp_br_iq_brq_tag_o(disp_br_iq_brq_tag_o),
      .disp_br_iq_opcode_o(disp_br_iq_opcode_o),
      .disp_br_iq_use_rs1_o(disp_br_iq_use_rs1_o),
      .disp_br_iq_use_rs2_o(disp_br_iq_use_rs2_o),
      .disp_br_iq_use_rd_o(disp_br_iq_use_rd_o),
      .disp_br_iq_phy_rs1_o(disp_br_iq_phy_rs1_o),
      .disp_br_iq_phy_rs2_o(disp_br_iq_phy_rs2_o),
      .disp_br_iq_phy_rd_o(disp_br_iq_phy_rd_o),
      .disp_br_iq_imm_o(disp_br_iq_imm_o),
      .disp_br_iq_rdy_i(disp_br_iq_rdy_i),
      .disp_alu0_iq_vld_o(disp_alu0_iq_vld_o),
`ifdef SIMULATION
      .disp_alu0_iq_pc_o(disp_alu0_iq_pc_o),
`endif
      .disp_alu0_iq_rob_tag_o(disp_alu0_iq_rob_tag_o),
      .disp_alu0_iq_opcode_o(disp_alu0_iq_opcode_o),
      .disp_alu0_iq_use_rs1_o(disp_alu0_iq_use_rs1_o),
      .disp_alu0_iq_use_rs2_o(disp_alu0_iq_use_rs2_o),
      .disp_alu0_iq_use_rd_o(disp_alu0_iq_use_rd_o),
      .disp_alu0_iq_phy_rs1_o(disp_alu0_iq_phy_rs1_o),
      .disp_alu0_iq_phy_rs2_o(disp_alu0_iq_phy_rs2_o),
      .disp_alu0_iq_phy_rd_o(disp_alu0_iq_phy_rd_o),
      .disp_alu0_iq_imm_o(disp_alu0_iq_imm_o),
      .disp_alu0_iq_use_imm_o(disp_alu0_iq_use_imm_o),
      .disp_alu0_iq_use_pc_o(disp_alu0_iq_use_pc_o),
      .disp_alu0_iq_rdy_i(disp_alu0_iq_rdy_i),
      .disp_alu1_iq_vld_o(disp_alu1_iq_vld_o),
`ifdef SIMULATION
      .disp_alu1_iq_pc_o(disp_alu1_iq_pc_o),
`endif
      .disp_alu1_iq_rob_tag_o(disp_alu1_iq_rob_tag_o),
      .disp_alu1_iq_opcode_o(disp_alu1_iq_opcode_o),
      .disp_alu1_iq_use_rs1_o(disp_alu1_iq_use_rs1_o),
      .disp_alu1_iq_use_rs2_o(disp_alu1_iq_use_rs2_o),
      .disp_alu1_iq_use_rd_o(disp_alu1_iq_use_rd_o),
      .disp_alu1_iq_phy_rs1_o(disp_alu1_iq_phy_rs1_o),
      .disp_alu1_iq_phy_rs2_o(disp_alu1_iq_phy_rs2_o),
      .disp_alu1_iq_phy_rd_o(disp_alu1_iq_phy_rd_o),
      .disp_alu1_iq_imm_o(disp_alu1_iq_imm_o),
      .disp_alu1_iq_use_imm_o(disp_alu1_iq_use_imm_o),
      .disp_alu1_iq_use_pc_o(disp_alu1_iq_use_pc_o),
      .disp_alu1_iq_rdy_i(disp_alu1_iq_rdy_i),
      .disp_alu2_iq_vld_o(disp_alu2_iq_vld_o),
`ifdef SIMULATION
      .disp_alu2_iq_pc_o(disp_alu2_iq_pc_o),
`endif
      .disp_alu2_iq_rob_tag_o(disp_alu2_iq_rob_tag_o),
      .disp_alu2_iq_fu_type_o(disp_alu2_iq_fu_type_o),
      .disp_alu2_iq_opcode_o(disp_alu2_iq_opcode_o),
      .disp_alu2_iq_use_rs1_o(disp_alu2_iq_use_rs1_o),
      .disp_alu2_iq_use_rs2_o(disp_alu2_iq_use_rs2_o),
      .disp_alu2_iq_use_rd_o(disp_alu2_iq_use_rd_o),
      .disp_alu2_iq_phy_rs1_o(disp_alu2_iq_phy_rs1_o),
      .disp_alu2_iq_phy_rs2_o(disp_alu2_iq_phy_rs2_o),
      .disp_alu2_iq_phy_rd_o(disp_alu2_iq_phy_rd_o),
      .disp_alu2_iq_imm_o(disp_alu2_iq_imm_o),
      .disp_alu2_iq_use_imm_o(disp_alu2_iq_use_imm_o),
      .disp_alu2_iq_use_pc_o(disp_alu2_iq_use_pc_o),
      .disp_alu2_iq_rdy_i(disp_alu2_iq_rdy_i)
  );


endmodule : rvh_dispatch
