module int_disp_router
  import rvh_pkg::*;
  import riscv_pkg::*;
  import uop_encoding_pkg::*;
(

    // Rename -> Dispatch Buffer(INT)
    input  logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_vld_i,
`ifdef SIMULATION
    input  logic [    INT_DISP_WIDTH-1:0][       VADDR_WIDTH-1:0] int_disp_iq_pc_i,
`endif
    input  logic [    INT_DISP_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] int_disp_iq_rob_tag_i,
    input  logic [    INT_DISP_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] int_disp_iq_brq_tag_i,
    input  logic [    INT_DISP_WIDTH-1:0][    INT_TYPE_WIDTH-1:0] int_disp_iq_major_op_i,
    input  logic [    INT_DISP_WIDTH-1:0][      INT_OP_WIDTH-1:0] int_disp_iq_minor_op_i,
    input  logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_use_rs1_i,
    input  logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_use_rs2_i,
    input  logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_use_rd_i,
    input  logic [    INT_DISP_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rs1_i,
    input  logic [    INT_DISP_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rs2_i,
    input  logic [    INT_DISP_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rd_i,
    input  logic [    INT_DISP_WIDTH-1:0][                  31:0] int_disp_iq_imm_i,
    input  logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_use_imm_i,
    input  logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_use_pc_i,
    output logic [    INT_DISP_WIDTH-1:0]                         int_disp_iq_rdy_o,
    // Dispatch Buffer(INT) -> IQ(BR)
    output logic                                                  disp_br_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_br_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_br_iq_rob_tag_o,
    output logic [     BRQ_TAG_WIDTH-1:0]                         disp_br_iq_brq_tag_o,
    output logic [      BRU_OP_WIDTH-1:0]                         disp_br_iq_opcode_o,
    output logic                                                  disp_br_iq_use_rs1_o,
    output logic                                                  disp_br_iq_use_rs2_o,
    output logic                                                  disp_br_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_br_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_br_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_br_iq_phy_rd_o,
    output logic [                  31:0]                         disp_br_iq_imm_o,
    input  logic                                                  disp_br_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(ALU0)
    output logic                                                  disp_alu0_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_alu0_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_alu0_iq_rob_tag_o,
    output logic [      ALU_OP_WIDTH-1:0]                         disp_alu0_iq_opcode_o,
    output logic                                                  disp_alu0_iq_use_rs1_o,
    output logic                                                  disp_alu0_iq_use_rs2_o,
    output logic                                                  disp_alu0_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu0_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu0_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu0_iq_phy_rd_o,
    output logic [                  31:0]                         disp_alu0_iq_imm_o,
    output logic                                                  disp_alu0_iq_use_imm_o,
    output logic                                                  disp_alu0_iq_use_pc_o,
    input  logic                                                  disp_alu0_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(ALU1)
    output logic                                                  disp_alu1_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_alu1_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_alu1_iq_rob_tag_o,
    output logic [      ALU_OP_WIDTH-1:0]                         disp_alu1_iq_opcode_o,
    output logic                                                  disp_alu1_iq_use_rs1_o,
    output logic                                                  disp_alu1_iq_use_rs2_o,
    output logic                                                  disp_alu1_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu1_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu1_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu1_iq_phy_rd_o,
    output logic [                  31:0]                         disp_alu1_iq_imm_o,
    output logic                                                  disp_alu1_iq_use_imm_o,
    output logic                                                  disp_alu1_iq_use_pc_o,
    input  logic                                                  disp_alu1_iq_rdy_i,
    // Dispatch Buffer(INT) -> IQ(Complex ALU2)  
    output logic                                                  disp_alu2_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                         disp_alu2_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                         disp_alu2_iq_rob_tag_o,
    output logic [                   1:0]                         disp_alu2_iq_fu_type_o,
    output logic [      ALU_OP_WIDTH-1:0]                         disp_alu2_iq_opcode_o,
    output logic                                                  disp_alu2_iq_use_rs1_o,
    output logic                                                  disp_alu2_iq_use_rs2_o,
    output logic                                                  disp_alu2_iq_use_rd_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu2_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu2_iq_phy_rs2_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                         disp_alu2_iq_phy_rd_o,
    output logic [                  31:0]                         disp_alu2_iq_imm_o,
    output logic                                                  disp_alu2_iq_use_imm_o,
    output logic                                                  disp_alu2_iq_use_pc_o,
    input  logic                                                  disp_alu2_iq_rdy_i
);

  localparam int unsigned SRC_ID_WIDTH = $clog2(INT_DISP_WIDTH);

  logic [INT_DISP_WIDTH-1:0] des_rdy;
  logic [INT_DISP_WIDTH-1:0] src_vld;
  logic [INT_DISP_WIDTH-1:0][INT_DISP_WIDTH-1:0] src_des_en;
  logic [INT_DISP_WIDTH-1:0] success;
  logic [INT_DISP_WIDTH-1:0][INT_DISP_WIDTH-1:0] src_des_sel_mask;
  logic [INT_DISP_WIDTH-1:0][SRC_ID_WIDTH-1:0] src_id;
  logic [INT_DISP_WIDTH-1:0][SRC_ID_WIDTH-1:0] src_des_sel_id;
  logic [INT_DISP_WIDTH-1:0] des_selected;

  assign src_vld    = int_disp_iq_vld_i;
  assign des_rdy[0] = disp_br_iq_rdy_i;
  assign des_rdy[1] = disp_alu0_iq_rdy_i;
  assign des_rdy[2] = disp_alu1_iq_rdy_i;
  assign des_rdy[3] = disp_alu2_iq_rdy_i;

  assign int_disp_iq_rdy_o = success;

  always_comb begin : gen_src_des_en
    for (int src = 0; src < INT_DISP_WIDTH; src++) begin
      src_des_en[src][0] = des_rdy[0] & (int_disp_iq_major_op_i[src] == INT_BRU_TYPE);
      src_des_en[src][1] = des_rdy[1] & (int_disp_iq_major_op_i[src] == INT_ALU_TYPE);
      src_des_en[src][2] = des_rdy[2] & (int_disp_iq_major_op_i[src] == INT_ALU_TYPE);
      src_des_en[src][3] = des_rdy[3] &
                (int_disp_iq_major_op_i[src] == INT_ALU_TYPE | int_disp_iq_major_op_i[src] ==
                 INT_MUL_TYPE | int_disp_iq_major_op_i[src] == INT_DIV_TYPE);
    end
  end

  generate
    for (genvar i = 0; i < INT_DISP_WIDTH; i++) begin
      assign src_id[i] = i[SRC_ID_WIDTH-1:0];
      assign des_selected[i] = |src_des_sel_mask[i];
    end
  endgenerate


  // Output 
  assign disp_br_iq_vld_o = int_disp_iq_vld_i[src_des_sel_id[0]] & des_selected[0];
`ifdef SIMULATION
  assign disp_br_iq_pc_o = int_disp_iq_pc_i[src_des_sel_id[0]];
`endif
  assign disp_br_iq_rob_tag_o = int_disp_iq_rob_tag_i[src_des_sel_id[0]];
  assign disp_br_iq_brq_tag_o = int_disp_iq_brq_tag_i[src_des_sel_id[0]];
  assign disp_br_iq_opcode_o = int_disp_iq_minor_op_i[src_des_sel_id[0]];
  assign disp_br_iq_use_rs1_o = int_disp_iq_use_rs1_i[src_des_sel_id[0]];
  assign disp_br_iq_use_rs2_o = int_disp_iq_use_rs2_i[src_des_sel_id[0]];
  assign disp_br_iq_use_rd_o = int_disp_iq_use_rd_i[src_des_sel_id[0]];
  assign disp_br_iq_phy_rs1_o = int_disp_iq_phy_rs1_i[src_des_sel_id[0]];
  assign disp_br_iq_phy_rs2_o = int_disp_iq_phy_rs2_i[src_des_sel_id[0]];
  assign disp_br_iq_phy_rd_o = int_disp_iq_phy_rd_i[src_des_sel_id[0]];
  assign disp_br_iq_imm_o = int_disp_iq_imm_i[src_des_sel_id[0]];

  assign disp_alu0_iq_vld_o = int_disp_iq_vld_i[src_des_sel_id[1]] & des_selected[1];
`ifdef SIMULATION
  assign disp_alu0_iq_pc_o = int_disp_iq_pc_i[src_des_sel_id[1]];
`endif
  assign disp_alu0_iq_rob_tag_o = int_disp_iq_rob_tag_i[src_des_sel_id[1]];
  assign disp_alu0_iq_opcode_o = int_disp_iq_minor_op_i[src_des_sel_id[1]];
  assign disp_alu0_iq_use_rs1_o = int_disp_iq_use_rs1_i[src_des_sel_id[1]];
  assign disp_alu0_iq_use_rs2_o = int_disp_iq_use_rs2_i[src_des_sel_id[1]];
  assign disp_alu0_iq_use_rd_o = int_disp_iq_use_rd_i[src_des_sel_id[1]];
  assign disp_alu0_iq_phy_rs1_o = int_disp_iq_phy_rs1_i[src_des_sel_id[1]];
  assign disp_alu0_iq_phy_rs2_o = int_disp_iq_phy_rs2_i[src_des_sel_id[1]];
  assign disp_alu0_iq_phy_rd_o = int_disp_iq_phy_rd_i[src_des_sel_id[1]];
  assign disp_alu0_iq_imm_o = int_disp_iq_imm_i[src_des_sel_id[1]];
  assign disp_alu0_iq_use_imm_o = int_disp_iq_use_imm_i[src_des_sel_id[1]];
  assign disp_alu0_iq_use_pc_o = int_disp_iq_use_pc_i[src_des_sel_id[1]];

  assign disp_alu1_iq_vld_o = int_disp_iq_vld_i[src_des_sel_id[2]] & des_selected[2];
`ifdef SIMULATION
  assign disp_alu1_iq_pc_o = int_disp_iq_pc_i[src_des_sel_id[2]];
`endif
  assign disp_alu1_iq_rob_tag_o = int_disp_iq_rob_tag_i[src_des_sel_id[2]];
  assign disp_alu1_iq_opcode_o = int_disp_iq_minor_op_i[src_des_sel_id[2]];
  assign disp_alu1_iq_use_rs1_o = int_disp_iq_use_rs1_i[src_des_sel_id[2]];
  assign disp_alu1_iq_use_rs2_o = int_disp_iq_use_rs2_i[src_des_sel_id[2]];
  assign disp_alu1_iq_use_rd_o = int_disp_iq_use_rd_i[src_des_sel_id[2]];
  assign disp_alu1_iq_phy_rs1_o = int_disp_iq_phy_rs1_i[src_des_sel_id[2]];
  assign disp_alu1_iq_phy_rs2_o = int_disp_iq_phy_rs2_i[src_des_sel_id[2]];
  assign disp_alu1_iq_phy_rd_o = int_disp_iq_phy_rd_i[src_des_sel_id[2]];
  assign disp_alu1_iq_imm_o = int_disp_iq_imm_i[src_des_sel_id[2]];
  assign disp_alu1_iq_use_imm_o = int_disp_iq_use_imm_i[src_des_sel_id[2]];
  assign disp_alu1_iq_use_pc_o = int_disp_iq_use_pc_i[src_des_sel_id[2]];


  assign disp_alu2_iq_vld_o = int_disp_iq_vld_i[src_des_sel_id[3]] & des_selected[3];
`ifdef SIMULATION
  assign disp_alu2_iq_pc_o = int_disp_iq_pc_i[src_des_sel_id[3]];
`endif
  assign disp_alu2_iq_rob_tag_o = int_disp_iq_rob_tag_i[src_des_sel_id[3]];
  assign disp_alu2_iq_fu_type_o = int_disp_iq_major_op_i[src_des_sel_id[3]];
  assign disp_alu2_iq_opcode_o = int_disp_iq_minor_op_i[src_des_sel_id[3]];
  assign disp_alu2_iq_use_rs1_o = int_disp_iq_use_rs1_i[src_des_sel_id[3]];
  assign disp_alu2_iq_use_rs2_o = int_disp_iq_use_rs2_i[src_des_sel_id[3]];
  assign disp_alu2_iq_use_rd_o = int_disp_iq_use_rd_i[src_des_sel_id[3]];
  assign disp_alu2_iq_phy_rs1_o = int_disp_iq_phy_rs1_i[src_des_sel_id[3]];
  assign disp_alu2_iq_phy_rs2_o = int_disp_iq_phy_rs2_i[src_des_sel_id[3]];
  assign disp_alu2_iq_phy_rd_o = int_disp_iq_phy_rd_i[src_des_sel_id[3]];
  assign disp_alu2_iq_imm_o = int_disp_iq_imm_i[src_des_sel_id[3]];
  assign disp_alu2_iq_use_imm_o = int_disp_iq_use_imm_i[src_des_sel_id[3]];
  assign disp_alu2_iq_use_pc_o = int_disp_iq_use_pc_i[src_des_sel_id[3]];

  generate
    for (genvar i = 0; i < INT_DISP_WIDTH; i++) begin
      onehot_mux #(
          .SOURCE_COUNT(INT_DISP_WIDTH),
          .DATA_WIDTH  (SRC_ID_WIDTH)
      ) u_sel_id_onehot_mux (
          .sel_i (src_des_sel_mask[i]),
          .data_i(src_id),
          .data_o(src_des_sel_id[i])
      );
    end
  endgenerate

  inorder_router #(
      .SRC_COUNT(INT_DISP_WIDTH),
      .DES_COUNT(INT_DISP_WIDTH)
  ) u_inorder_router (
      .src_vld_i(src_vld),
      .src_des_en_i(src_des_en),
      .success_o(success),
      .src_des_sel_o(src_des_sel_mask)
  );


endmodule : int_disp_router
