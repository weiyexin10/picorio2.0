module ls_disp_router
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
(
    // Dispatch Buffer(LS) -> LS Router
    input  logic [    LSU_DISP_WIDTH-1:0]                     ls_disp_iq_vld_i,
`ifdef SIMULATION
    input  logic [    LSU_DISP_WIDTH-1:0][   VADDR_WIDTH-1:0] ls_disp_iq_pc_i,
`endif
    input  logic [    LSU_DISP_WIDTH-1:0][ ROB_TAG_WIDTH-1:0] ls_disp_iq_rob_tag_i,
    input  logic [    LSU_DISP_WIDTH-1:0][ LSQ_TAG_WIDTH-1:0] ls_disp_iq_lsq_tag_i,
    input  logic [    LSU_DISP_WIDTH-1:0][LSU_TYPE_WIDTH-1:0] ls_disp_iq_major_op_i,
    input  logic [    LSU_DISP_WIDTH-1:0][  LSU_OP_WIDTH-1:0] ls_disp_iq_minor_op_i,
    input  logic [    LSU_DISP_WIDTH-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rs1_i,
    input  logic [    LSU_DISP_WIDTH-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rs2_i,
    input  logic [    LSU_DISP_WIDTH-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rd_i,
    input  logic [    LSU_DISP_WIDTH-1:0][              11:0] ls_disp_iq_imm_i,
    output logic [    LSU_DISP_WIDTH-1:0]                     ls_disp_iq_rdy_o,
    // LS Router -> AGU IQ0
    output logic                                              disp_agu0_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                     disp_agu0_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                     disp_agu0_iq_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                     disp_agu0_iq_lsq_tag_o,
    output logic [    LSU_TYPE_WIDTH-1:0]                     disp_agu0_iq_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                     disp_agu0_iq_opcode_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                     disp_agu0_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                     disp_agu0_iq_phy_rd_o,
    output logic [                  11:0]                     disp_agu0_iq_imm_o,
    input  logic                                              disp_agu0_iq_rdy_i,
    // LS Router -> SDU IQ0
    output logic                                              disp_sdu0_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                     disp_sdu0_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                     disp_sdu0_iq_rob_tag_o,
    output logic [     STQ_TAG_WIDTH-1:0]                     disp_sdu0_iq_stq_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                     disp_sdu0_iq_phy_rs2_o,
    input  logic                                              disp_sdu0_iq_rdy_i,
    // LS Router -> AGU IQ1
    output logic                                              disp_agu1_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                     disp_agu1_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                     disp_agu1_iq_rob_tag_o,
    output logic [     LSQ_TAG_WIDTH-1:0]                     disp_agu1_iq_lsq_tag_o,
    output logic [    LSU_TYPE_WIDTH-1:0]                     disp_agu1_iq_fu_type_o,
    output logic [      LSU_OP_WIDTH-1:0]                     disp_agu1_iq_opcode_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                     disp_agu1_iq_phy_rs1_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                     disp_agu1_iq_phy_rd_o,
    output logic [                  11:0]                     disp_agu1_iq_imm_o,
    input  logic                                              disp_agu1_iq_rdy_i,
    // LS Router -> SDU IQ1
    output logic                                              disp_sdu1_iq_vld_o,
`ifdef SIMULATION
    output logic [       VADDR_WIDTH-1:0]                     disp_sdu1_iq_pc_o,
`endif
    output logic [     ROB_TAG_WIDTH-1:0]                     disp_sdu1_iq_rob_tag_o,
    output logic [     STQ_TAG_WIDTH-1:0]                     disp_sdu1_iq_stq_tag_o,
    output logic [INT_PREG_TAG_WIDTH-1:0]                     disp_sdu1_iq_phy_rs2_o,
    input  logic                                              disp_sdu1_iq_rdy_i
);

    logic ls_slot0_can_issue_ld, ls_slot0_can_issue_st;
    logic ls_slot1_can_issue_ld, ls_slot1_can_issue_st;

    logic disp_slot0_is_ld, disp_slot0_is_st;
    logic disp_slot1_is_ld, disp_slot1_is_st;

    logic disp_slot0_to_ls_slot0, disp_slot0_to_ls_slot1;
    logic disp_slot1_to_ls_slot1;

    logic disp_slot0_rdy;
    logic disp_slot1_rdy;

    assign ls_slot0_can_issue_ld = disp_agu0_iq_rdy_i;
    assign ls_slot0_can_issue_st = disp_agu0_iq_rdy_i & disp_sdu0_iq_rdy_i;

    assign ls_slot1_can_issue_ld = disp_agu1_iq_rdy_i;
    assign ls_slot1_can_issue_st = disp_agu1_iq_rdy_i & disp_sdu1_iq_rdy_i;

    assign disp_slot0_is_ld = ls_disp_iq_major_op_i[0] == LSU_LD_TYPE;
    assign disp_slot0_is_st = ls_disp_iq_major_op_i[0] == LSU_ST_TYPE;

    assign disp_slot1_is_ld = ls_disp_iq_major_op_i[1] == LSU_LD_TYPE;
    assign disp_slot1_is_st = ls_disp_iq_major_op_i[1] == LSU_ST_TYPE;

    assign disp_slot0_to_ls_slot0 = (disp_slot0_is_ld & ls_slot0_can_issue_ld) |
        (disp_slot0_is_st & ls_slot0_can_issue_st);
    assign disp_slot0_to_ls_slot1 = ~disp_slot0_to_ls_slot0 &
        ((disp_slot0_is_ld & ls_slot1_can_issue_ld) | (disp_slot0_is_st & ls_slot1_can_issue_st));
    assign disp_slot1_to_ls_slot1 = disp_slot0_to_ls_slot0 &
        ((disp_slot1_is_ld & ls_slot1_can_issue_ld) | (disp_slot1_is_st & ls_slot1_can_issue_st));

    assign disp_slot0_rdy = disp_slot0_to_ls_slot0 | disp_slot0_to_ls_slot1;
    assign disp_slot1_rdy = disp_slot1_to_ls_slot1;

    // LS slot0
    assign disp_agu0_iq_vld_o = disp_slot0_to_ls_slot0 & ls_disp_iq_vld_i[0] & (~disp_slot0_is_st | (ls_disp_iq_minor_op_i[0] != STU_FENCE));
`ifdef SIMULATION
    assign disp_agu0_iq_pc_o = ls_disp_iq_pc_i[0];
`endif
    assign disp_agu0_iq_rob_tag_o = ls_disp_iq_rob_tag_i[0];
    assign disp_agu0_iq_lsq_tag_o = ls_disp_iq_lsq_tag_i[0];
    assign disp_agu0_iq_fu_type_o = ls_disp_iq_major_op_i[0];
    assign disp_agu0_iq_opcode_o = ls_disp_iq_minor_op_i[0];
    assign disp_agu0_iq_phy_rs1_o = ls_disp_iq_phy_rs1_i[0];
    assign disp_agu0_iq_phy_rd_o = ls_disp_iq_phy_rd_i[0];
    assign disp_agu0_iq_imm_o = ls_disp_iq_imm_i[0];

    assign disp_sdu0_iq_vld_o = disp_slot0_to_ls_slot0 & ls_disp_iq_vld_i[0] & disp_slot0_is_st & (ls_disp_iq_minor_op_i[0] != STU_FENCE);
`ifdef SIMULATION
    assign disp_sdu0_iq_pc_o = ls_disp_iq_pc_i[0];
`endif
    assign disp_sdu0_iq_rob_tag_o = ls_disp_iq_rob_tag_i[0];
    assign disp_sdu0_iq_stq_tag_o = ls_disp_iq_lsq_tag_i[0];
    assign disp_sdu0_iq_phy_rs2_o = ls_disp_iq_phy_rs2_i[0];

    // LS slot1
    assign disp_agu1_iq_vld_o = (disp_slot0_to_ls_slot1 & (ls_disp_iq_vld_i[0] & (~disp_slot0_is_st | (ls_disp_iq_minor_op_i[0] != STU_FENCE)))) |
                                (disp_slot1_to_ls_slot1 & (ls_disp_iq_vld_i[1] & (~disp_slot1_is_st | (ls_disp_iq_minor_op_i[1] != STU_FENCE))));
`ifdef SIMULATION
    assign disp_agu1_iq_pc_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_pc_i[0] : ls_disp_iq_pc_i[1];
`endif
    assign disp_agu1_iq_rob_tag_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_rob_tag_i[0] :
        ls_disp_iq_rob_tag_i[1];
    assign disp_agu1_iq_lsq_tag_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_lsq_tag_i[0] :
        ls_disp_iq_lsq_tag_i[1];
    assign disp_agu1_iq_fu_type_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_major_op_i[0] :
        ls_disp_iq_major_op_i[1];
    assign disp_agu1_iq_opcode_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_minor_op_i[0] :
        ls_disp_iq_minor_op_i[1];
    assign disp_agu1_iq_phy_rs1_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_phy_rs1_i[0] :
        ls_disp_iq_phy_rs1_i[1];
    assign disp_agu1_iq_phy_rd_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_phy_rd_i[0] :
        ls_disp_iq_phy_rd_i[1];
    assign disp_agu1_iq_imm_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_imm_i[0] : ls_disp_iq_imm_i[1];

    assign disp_sdu1_iq_vld_o = (disp_slot0_to_ls_slot1 & (ls_disp_iq_vld_i[0] & disp_slot0_is_st & (ls_disp_iq_minor_op_i[0] != STU_FENCE))) |
                                (disp_slot1_to_ls_slot1 & (ls_disp_iq_vld_i[1] & disp_slot1_is_st & (ls_disp_iq_minor_op_i[1] != STU_FENCE)));
`ifdef SIMULATION
    assign disp_sdu1_iq_pc_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_pc_i[0] : ls_disp_iq_pc_i[1];
`endif
    assign disp_sdu1_iq_rob_tag_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_rob_tag_i[0] :
        ls_disp_iq_rob_tag_i[1];
    assign disp_sdu1_iq_stq_tag_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_lsq_tag_i[0] :
        ls_disp_iq_lsq_tag_i[1];
    assign disp_sdu1_iq_phy_rs2_o = disp_slot0_to_ls_slot1 ? ls_disp_iq_phy_rs2_i[0] :
        ls_disp_iq_phy_rs2_i[1];

    assign ls_disp_iq_rdy_o[0] = ls_disp_iq_vld_i[0] ? disp_slot0_rdy : 1'b0;
    assign ls_disp_iq_rdy_o[1] = ls_disp_iq_vld_i[1] ? disp_slot1_rdy : 1'b0;

endmodule : ls_disp_router
