module int_disp_buffer
  import rvh_pkg::*;
  import uop_encoding_pkg::*;
#(
    localparam int unsigned ENQ_COUNT = RENAME_WIDTH,
    localparam int unsigned DEQ_COUNT = INT_DISP_WIDTH
) (
    // Rename -> Dispatch Buffer(INT)
    input  logic [ENQ_COUNT-1:0]                         rn_int_disp_vld_i,
`ifdef SIMULATION
    input  logic [ENQ_COUNT-1:0][       VADDR_WIDTH-1:0] rn_int_disp_pc_i,
`endif
    input  logic [ENQ_COUNT-1:0][     ROB_TAG_WIDTH-1:0] rn_int_disp_rob_tag_i,
    input  logic [ENQ_COUNT-1:0][     BRQ_TAG_WIDTH-1:0] rn_int_disp_brq_tag_i,
    input  logic [ENQ_COUNT-1:0][    INT_TYPE_WIDTH-1:0] rn_int_disp_major_op_i,
    input  logic [ENQ_COUNT-1:0][      INT_OP_WIDTH-1:0] rn_int_disp_minor_op_i,
    input  logic [ENQ_COUNT-1:0]                         rn_int_disp_use_rs1_i,
    input  logic [ENQ_COUNT-1:0]                         rn_int_disp_use_rs2_i,
    input  logic [ENQ_COUNT-1:0]                         rn_int_disp_use_rd_i,
    input  logic [ENQ_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs1_i,
    input  logic [ENQ_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rs2_i,
    input  logic [ENQ_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] rn_int_disp_phy_rd_i,
    input  logic [ENQ_COUNT-1:0][                  31:0] rn_int_disp_imm_i,
    input  logic [ENQ_COUNT-1:0]                         rn_int_disp_use_imm_i,
    input  logic [ENQ_COUNT-1:0]                         rn_int_disp_use_pc_i,
    output logic                                         rn_int_disp_rdy_o,

    // Dispatch Buffer(INT) -> IQ
    output logic [DEQ_COUNT-1:0]                         int_disp_iq_vld_o,
`ifdef SIMULATION
    output logic [DEQ_COUNT-1:0][       VADDR_WIDTH-1:0] int_disp_iq_pc_o,
`endif
    output logic [DEQ_COUNT-1:0][     ROB_TAG_WIDTH-1:0] int_disp_iq_rob_tag_o,
    output logic [DEQ_COUNT-1:0][     BRQ_TAG_WIDTH-1:0] int_disp_iq_brq_tag_o,
    output logic [DEQ_COUNT-1:0][    INT_TYPE_WIDTH-1:0] int_disp_iq_major_op_o,
    output logic [DEQ_COUNT-1:0][      INT_OP_WIDTH-1:0] int_disp_iq_minor_op_o,
    output logic [DEQ_COUNT-1:0]                         int_disp_iq_use_rs1_o,
    output logic [DEQ_COUNT-1:0]                         int_disp_iq_use_rs2_o,
    output logic [DEQ_COUNT-1:0]                         int_disp_iq_use_rd_o,
    output logic [DEQ_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rs1_o,
    output logic [DEQ_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rs2_o,
    output logic [DEQ_COUNT-1:0][INT_PREG_TAG_WIDTH-1:0] int_disp_iq_phy_rd_o,
    output logic [DEQ_COUNT-1:0][                  31:0] int_disp_iq_imm_o,
    output logic [DEQ_COUNT-1:0]                         int_disp_iq_use_imm_o,
    output logic [DEQ_COUNT-1:0]                         int_disp_iq_use_pc_o,
    input  logic [DEQ_COUNT-1:0]                         int_disp_iq_rdy_i,


    input logic flush_i,

    input clk,
    input rst
);

  typedef struct packed {
`ifdef SIMULATION
    logic [VADDR_WIDTH-1:0]    pc;
`endif
    logic [ROB_TAG_WIDTH-1:0]  rob_tag;
    logic [BRQ_TAG_WIDTH-1:0]  brq_tag;
    logic [INT_TYPE_WIDTH-1:0] major_op;
    logic [INT_OP_WIDTH-1:0]   minor_op;
    logic                      use_rs1;
    logic                      use_rs2;
    logic                      use_rd;
    logic [PREG_TAG_WIDTH-1:0] phy_rs1;
    logic [PREG_TAG_WIDTH-1:0] phy_rs2;
    logic [PREG_TAG_WIDTH-1:0] phy_rd;
    logic [31:0]               imm;
    logic                      use_imm;
    logic                      use_pc;
  } int_disp_t;

  logic [RENAME_WIDTH-1:0] enq_rdy;
  int_disp_t [RENAME_WIDTH-1:0] rn_int_disp_payload;


  logic [INT_DISP_WIDTH-1:0] int_disp_iq_vld;
  logic [INT_DISP_WIDTH-1:0] int_disp_iq_fire;
  int_disp_t [INT_DISP_WIDTH-1:0] int_disp_iq_payload;


  assign rn_int_disp_rdy_o = enq_rdy[0];

  always_comb begin : create_new_entry
    for (int i = 0; i < ENQ_COUNT; i++) begin
`ifdef SIMULATION
      rn_int_disp_payload[i].pc = rn_int_disp_pc_i[i];
`endif
      rn_int_disp_payload[i].rob_tag  = rn_int_disp_rob_tag_i[i];
      rn_int_disp_payload[i].brq_tag  = rn_int_disp_brq_tag_i[i];
      rn_int_disp_payload[i].major_op = rn_int_disp_major_op_i[i];
      rn_int_disp_payload[i].minor_op = rn_int_disp_minor_op_i[i];
      rn_int_disp_payload[i].use_rs1  = rn_int_disp_use_rs1_i[i];
      rn_int_disp_payload[i].use_rs2  = rn_int_disp_use_rs2_i[i];
      rn_int_disp_payload[i].use_rd   = rn_int_disp_use_rd_i[i];
      rn_int_disp_payload[i].phy_rs1  = rn_int_disp_phy_rs1_i[i];
      rn_int_disp_payload[i].phy_rs2  = rn_int_disp_phy_rs2_i[i];
      rn_int_disp_payload[i].phy_rd   = rn_int_disp_phy_rd_i[i];
      rn_int_disp_payload[i].imm      = rn_int_disp_imm_i[i];
      rn_int_disp_payload[i].use_imm  = rn_int_disp_use_imm_i[i];
      rn_int_disp_payload[i].use_pc   = rn_int_disp_use_pc_i[i];
    end
  end

  always_comb begin : deq_entry
    for (int i = 0; i < ENQ_COUNT; i++) begin
`ifdef SIMULATION
      int_disp_iq_pc_o[i] = int_disp_iq_payload[i].pc;
`endif
      int_disp_iq_rob_tag_o[i]  = int_disp_iq_payload[i].rob_tag;
      int_disp_iq_brq_tag_o[i]  = int_disp_iq_payload[i].brq_tag;
      int_disp_iq_major_op_o[i] = int_disp_iq_payload[i].major_op;
      int_disp_iq_minor_op_o[i] = int_disp_iq_payload[i].minor_op;
      int_disp_iq_use_rs1_o[i]  = int_disp_iq_payload[i].use_rs1;
      int_disp_iq_use_rs2_o[i]  = int_disp_iq_payload[i].use_rs2;
      int_disp_iq_use_rd_o[i]   = int_disp_iq_payload[i].use_rd;
      int_disp_iq_phy_rs1_o[i]  = int_disp_iq_payload[i].phy_rs1;
      int_disp_iq_phy_rs2_o[i]  = int_disp_iq_payload[i].phy_rs2;
      int_disp_iq_phy_rd_o[i]   = int_disp_iq_payload[i].phy_rd;
      int_disp_iq_imm_o[i]      = int_disp_iq_payload[i].imm;
      int_disp_iq_use_imm_o[i]  = int_disp_iq_payload[i].use_imm;
      int_disp_iq_use_pc_o[i]   = int_disp_iq_payload[i].use_pc;
    end
  end

  mp_fifo #(
      .payload_t(int_disp_t),
      .ENQUEUE_WIDTH(ENQ_COUNT),
      .DEQUEUE_WIDTH(DEQ_COUNT),
      .DEPTH(INT_DISP_QUEUE_DEPTH),
      .MUST_TAKEN_ALL(1'b1)
  ) mp_fifo_u (
      .enqueue_vld_i(rn_int_disp_vld_i),
      .enqueue_payload_i(rn_int_disp_payload),
      .enqueue_rdy_o(enq_rdy),
      .dequeue_vld_o(int_disp_iq_vld_o),
      .dequeue_payload_o(int_disp_iq_payload),
      .dequeue_rdy_i(int_disp_iq_rdy_i),
      .flush_i(flush_i),
      .clk(clk),
      .rst(rst)
  );

endmodule : int_disp_buffer
