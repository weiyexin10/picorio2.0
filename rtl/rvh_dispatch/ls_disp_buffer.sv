module ls_disp_buffer
    import rvh_pkg::*;
    import uop_encoding_pkg::*;
#(
    localparam int unsigned ENQ_COUNT = RENAME_WIDTH,
    localparam int unsigned DEQ_COUNT = LSU_DISP_WIDTH
) (
    // Rename -> Dispatch Buffer(LS)
    input  logic [ENQ_COUNT-1:0]                     rn_ls_disp_vld_i,
`ifdef SIMULATION
    input  logic [ENQ_COUNT-1:0][   VADDR_WIDTH-1:0] rn_ls_disp_pc_i,
`endif
    input  logic [ENQ_COUNT-1:0][ ROB_TAG_WIDTH-1:0] rn_ls_disp_rob_tag_i,
    input  logic [ENQ_COUNT-1:0][LSU_TYPE_WIDTH-1:0] rn_ls_disp_major_op_i,
    input  logic [ENQ_COUNT-1:0][  LSU_OP_WIDTH-1:0] rn_ls_disp_minor_op_i,
    input  logic [ENQ_COUNT-1:0][PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs1_i,
    input  logic [ENQ_COUNT-1:0][PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rs2_i,
    input  logic [ENQ_COUNT-1:0][PREG_TAG_WIDTH-1:0] rn_ls_disp_phy_rd_i,
    input  logic [ENQ_COUNT-1:0][              11:0] rn_ls_disp_imm_i,
    input  logic [ENQ_COUNT-1:0]                     rn_ls_disp_is_fence_i,
    output logic                                     rn_ls_disp_rdy_o,
    // Dispatch Buffer(LS) -> IQ
    output logic [DEQ_COUNT-1:0]                     ls_disp_iq_vld_o,
`ifdef SIMULATION
    output logic [DEQ_COUNT-1:0][   VADDR_WIDTH-1:0] ls_disp_iq_pc_o,
`endif
    output logic [DEQ_COUNT-1:0][ ROB_TAG_WIDTH-1:0] ls_disp_iq_rob_tag_o,
    output logic [DEQ_COUNT-1:0][LSU_TYPE_WIDTH-1:0] ls_disp_iq_major_op_o,
    output logic [DEQ_COUNT-1:0][  LSU_OP_WIDTH-1:0] ls_disp_iq_minor_op_o,
    output logic [DEQ_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rs1_o,
    output logic [DEQ_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rs2_o,
    output logic [DEQ_COUNT-1:0][PREG_TAG_WIDTH-1:0] ls_disp_iq_phy_rd_o,
    output logic [DEQ_COUNT-1:0][              11:0] ls_disp_iq_imm_o,
    output logic [DEQ_COUNT-1:0]                     ls_disp_iq_is_fence_o,
    input  logic [DEQ_COUNT-1:0]                     ls_disp_iq_rdy_i,


    input logic flush_i,

    input clk,
    input rst
);

    typedef struct packed {
`ifdef SIMULATION
        logic [VADDR_WIDTH-1:0]    pc;
`endif
        logic [ROB_TAG_WIDTH-1:0]  rob_tag;
        logic                      major_op;
        logic [LSU_OP_WIDTH-1:0]   minor_op;
        logic [PREG_TAG_WIDTH-1:0] phy_rs1;
        logic [PREG_TAG_WIDTH-1:0] phy_rs2;
        logic [PREG_TAG_WIDTH-1:0] phy_rd;
        logic [11:0]               imm;
        logic                      is_fence;
    } ls_disp_t;

    ls_disp_t [ENQ_COUNT-1:0] rn_ls_disp_payload;

    ls_disp_t [DEQ_COUNT-1:0] ls_disp_iq_payload;
    
    logic [ENQ_COUNT-1:0] enq_rdy;

    assign rn_ls_disp_rdy_o = enq_rdy[0];

    always_comb begin : create_new_entry
        for (int i = 0; i < ENQ_COUNT; i++) begin
`ifdef SIMULATION
            rn_ls_disp_payload[i].pc = rn_ls_disp_pc_i[i];
`endif
            rn_ls_disp_payload[i].rob_tag  = rn_ls_disp_rob_tag_i[i];
            rn_ls_disp_payload[i].major_op = rn_ls_disp_major_op_i[i];
            rn_ls_disp_payload[i].minor_op = rn_ls_disp_minor_op_i[i];
            rn_ls_disp_payload[i].phy_rs1  = rn_ls_disp_phy_rs1_i[i];
            rn_ls_disp_payload[i].phy_rs2  = rn_ls_disp_phy_rs2_i[i];
            rn_ls_disp_payload[i].phy_rd   = rn_ls_disp_phy_rd_i[i];
            rn_ls_disp_payload[i].imm      = rn_ls_disp_imm_i[i];
            rn_ls_disp_payload[i].is_fence = rn_ls_disp_is_fence_i[i];
        end
    end

    always_comb begin : dispatch_entry
        for (int i = 0; i < DEQ_COUNT; i++) begin
`ifdef SIMULATION
            ls_disp_iq_pc_o[i]       = ls_disp_iq_payload[i].pc;
`endif
            ls_disp_iq_rob_tag_o[i]  = ls_disp_iq_payload[i].rob_tag;
            ls_disp_iq_major_op_o[i] = ls_disp_iq_payload[i].major_op;
            ls_disp_iq_minor_op_o[i] = ls_disp_iq_payload[i].minor_op;
            ls_disp_iq_phy_rs1_o[i]  = ls_disp_iq_payload[i].phy_rs1;
            ls_disp_iq_phy_rs2_o[i]  = ls_disp_iq_payload[i].phy_rs2;
            ls_disp_iq_phy_rd_o[i]   = ls_disp_iq_payload[i].phy_rd;
            ls_disp_iq_imm_o[i]      = ls_disp_iq_payload[i].imm;
            ls_disp_iq_is_fence_o[i] = ls_disp_iq_payload[i].is_fence;
        end
    end


    mp_fifo #(
        .payload_t(ls_disp_t),
        .ENQUEUE_WIDTH(RENAME_WIDTH),
        .DEQUEUE_WIDTH(DEQ_COUNT),
        .DEPTH(LSU_DISP_QUEUE_DEPTH),
        .MUST_TAKEN_ALL(1'b1)
    ) mp_fifo_u (
        .enqueue_vld_i(rn_ls_disp_vld_i),
        .enqueue_payload_i(rn_ls_disp_payload),
        .enqueue_rdy_o(enq_rdy),
        .dequeue_vld_o(ls_disp_iq_vld_o),
        .dequeue_payload_o(ls_disp_iq_payload),
        .dequeue_rdy_i(ls_disp_iq_rdy_i),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );


endmodule : ls_disp_buffer
