module bp_pipe
  import rvh_pkg::*;
  import btb_typedef::BTB_N_ENTRY;
#(
) (
    input logic                                             req_valid_b0_i,
    input logic                                             req_is_older_bank_i,
    input logic[FTQ_TAG_LEN-1:0]                            req_ftq_tag_i,
    input logic [VADDR_WIDTH-1:0]                           req_pc_b0_i,
    output logic                                            req_ready_o,

    // bp pipe <> ftq           
    output logic                                            vld_b1_o,
    output logic                                            is_older_bank_b1_o,
    output logic [VADDR_WIDTH-1:0]                          pc_b1_o,
    output logic [VADDR_WIDTH-1:0]                          next_pc_b1_o,
    output logic[FTQ_TAG_LEN-1:0]                           ftq_tag_b1_o,
    output logic                                            is_taken_b1_o,
    output logic                                            is_truncate_b1_o,
    output logic[$clog2(FETCH_WIDTH/16)-1:0]                offset_b1_o,
    output logic                                            fix_ghist_b1_o,
    output logic                                            saw_taken_br_b1_o,
    output logic                                            saw_not_taken_br_b1_o,
    output ras_typedef::ras_pred_t                          ras_pred_b1_o,
    output ras_typedef::ras_pred_t                          ras_pred_b1_ff_o,
    input ras_typedef::ras_meta_t                           ras_meta_b1_i,
    input logic [VADDR_WIDTH-1:0]                           ras_target_b1_i,
    input logic                                             other_bpp_vld_b1_i,
    input logic                                             invalidate_b1_i,
    input ghist_info_t                                      new_ghist_info_b1_i,
    input logic [GHIST_LEN-1:0]                             ghist_b1_i,
    input logic                                             ready_b1_i,

    output logic                                            vld_b2_o,
    output logic                                            is_older_bank_b2_o,
    output logic [VADDR_WIDTH-1:0]                          pc_b2_o,
    output logic [VADDR_WIDTH-1:0]                          next_pc_b2_o,
    output logic[FTQ_TAG_LEN-1:0]                           ftq_tag_b2_o,
    output logic                                            is_taken_b2_o,
    output logic                                            is_truncate_b2_o,
    output logic[$clog2(FETCH_WIDTH/16)-1:0]                offset_b2_o,
    output ghist_info_t                                     ghist_info_b2_o,
    output logic                                            saw_taken_br_b2_o,
    output logic                                            saw_not_taken_br_b2_o,
    input  logic                                            older_bank_saw_not_taken_br_b2_i,
    input  logic                                            other_bpp_vld_b2_i,
    input logic                                             invalidate_b2_i,
    input ghist_info_t                                      new_ghist_info_b2_i,

    output ras_typedef::ras_pred_t                          ras_pred_b2_o,
    output ras_typedef::ras_pred_t                          ras_pred_b2_ff_o,
    output ras_typedef::ras_meta_t                          ras_meta_b2_o,
    output ubtb_typedef::ubtb_meta_t                        ubtb_meta_b2_o,
    output bht_typedef::bht_meta_t                          bht_meta_b2_o,
    output tage_typedef::composed_tage_pred_meta_t          composed_tage_meta_b2_o,
    output btb_typedef::btb_meta_t                          btb_meta_b2_o,
    output loop_typedef::loop_meta_t                        loop_meta_b2_o,

    input logic                                             ready_b2_i,

    // update
    input logic                                                        update_valid_i,
    input logic [PREDS_PER_LINE-1:0]                                   update_pred_valid_i,
    input logic                                                        update_truncate_i,
    input logic [PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0]       update_cfi_offset_vec_i,
    input logic [PREDS_PER_LINE-1:0]                                   update_cfi_taken_vec_i,
    input logic                                   [   VADDR_WIDTH-1:0] update_req_pc_i,
    input logic                                   [   VADDR_WIDTH-1:0] update_target_i,
    input logic                                   [PREDS_PER_LINE-1:0] update_cfi_mask_i,
    input logic                                   [PREDS_PER_LINE-1:0] update_rvc_mask_i,
    input logic                                   [PREDS_PER_LINE-1:0] update_call_mask_i,
    input logic                                                        update_is_misalign_i,
    input logic                                   [PREDS_PER_LINE-1:0] update_ret_mask_i,
    input logic                                   [PREDS_PER_LINE-1:0] update_br_mask_i,
    input tage_typedef::composed_tage_pred_meta_t                      update_composed_tage_meta_i,
    input btb_typedef::btb_meta_t                                      update_btb_meta_i,
    input ubtb_typedef::ubtb_meta_t                                    update_ubtb_meta_i,
    input bht_typedef::bht_meta_t                                      update_bht_meta_i,
    input loop_typedef::loop_meta_t                                    update_loop_meta_i,
    input [GHIST_LEN-1:0]                                              update_ghist_i,

    input logic                                                         redirect_b2_i,

    input logic                                                         fetch_redirect_i,
    input logic                                                         fetch_redirect_taken_i,
    input logic                                                         be_redirect_i,
    input logic                                                         be_redirect_taken_i,

    input logic                                                         clk,
    input logic                                                         rst
);
    // pipeline
    logic [VADDR_WIDTH-1:0]                         pc_b0, pc_b1, pc_b2;
    ghist_info_t                                    ghist_info_b2;
    ghist_info_t                                    pred_ghist_info_b2;
    logic[FTQ_TAG_LEN-1:0]                          ftq_tag_b1, ftq_tag_b2;
    logic                                           b0_flush, b1_flush, b2_flush;
    logic                                           b0_redirect, b1_redirect, b2_redirect;
    logic                                           b0_ready, b1_ready, b2_ready;
    logic                                           b0_valid, b1_valid, b2_valid;
    
    logic                                           saw_misalign_br_b2;

    logic                                           req_is_older_bank_b1, req_is_older_bank_b2;
    logic                                           pred_taken_b1, pred_taken_b1_b2;
    logic [VADDR_WIDTH-1:0]                         pred_target_b1, pred_target_b1_b2;
    logic [PRED_OFFSET_WIDTH-1:0]                   pred_offset_b1, pred_offset_b1_b2;
    
    logic                                           pred_valid_b1;
    logic                                           pred_truncate_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_invalidate_mask_b1;
    logic                                           pred_invalidate_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_taken_mask_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_cfi_mask_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_br_mask_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_call_mask_b1;
    logic                                           pred_is_misalign_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_ret_mask_b1;
    logic [PREDS_PER_LINE-1:0]                      pred_target_valid_mask_b1;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_br_banks_b1;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_taken_br_banks_b1;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_call_banks_b1;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_ret_banks_b1;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_taken_banks_b1;

    logic                                           pred_has_br_b1;
    logic                                           pred_has_taken_br_b1;
    logic                                           pred_has_taken_call_b1;
    logic                                           pred_has_taken_ret_b1;
    logic                                           pred_has_taken_b1;
    
    logic                                           pred_taken_b2;
    logic [VADDR_WIDTH-1:0]                         pred_target_b2;
    logic [PRED_OFFSET_WIDTH-1:0]                   pred_offset_b2;
    logic [VADDR_WIDTH-1:0]                         fix_ghist_target_b2;
    logic [PREDS_PER_LINE-1:0][VADDR_WIDTH-1:0]     pred_targets_b2;
    
    logic                                           pred_valid_b2;
    logic                                           pred_truncate_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_invalidate_mask_b2;
    logic                                           pred_invalidate_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_cfi_mask_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_taken_mask_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_br_mask_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_call_mask_b2;
    logic                                           pred_is_misalign_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_ret_mask_b2;
    logic [PREDS_PER_LINE-1:0]                      pred_target_valid_mask_b2;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_br_banks_b2;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_taken_br_banks_b2;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_call_banks_b2;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_ret_banks_b2;
    logic [$clog2(PREDS_PER_LINE)-1:0]              pred_taken_banks_b2;

    logic                                           pred_has_br_b2;
    logic                                           pred_has_taken_br_b2;
    logic                                           pred_has_taken_call_b2;
    logic                                           pred_has_taken_ret_b2;
    logic                                           pred_has_taken_b2;

    logic                                           saw_taken_br_b1, saw_not_taken_br_b1;
    logic                                           saw_taken_br_b2, saw_not_taken_br_b2;

    logic [VADDR_WIDTH-1:0]                         redirect_target_b2;
    // logic                       redirect_b2;
    // logic                       fix_pred_b2;
    // logic                       fix_ghist_b2;
////////////////////////////bpu/////////////////////////////////
    logic                       ubtb_ready_b0;
    logic                       bht_ready_b0;
    tage_typedef::composed_tage_pred_t[1:0] composed_tage_pred_b2;
    tage_typedef::composed_tage_pred_t      used_tage_pred_b2;
    tage_typedef::composed_tage_pred_meta_t[1:0] composed_tage_meta_b2;
    tage_typedef::composed_tage_pred_meta_t      used_tage_meta_b2;
    btb_typedef::btb_pred_t btb_pred_b2;
    btb_typedef::btb_meta_t btb_meta_b2;
    ubtb_typedef::ubtb_pred_t ubtb_pred_b1, ubtb_pred_b1_b2;
    ubtb_typedef::ubtb_meta_t ubtb_meta_b1, ubtb_meta_b1_b2;
    bht_typedef::bht_pred_t bht_pred_b1, bht_pred_b1_b2;
    bht_typedef::bht_meta_t bht_meta_b1, bht_meta_b1_b2;
    logic [PREDS_PER_LINE-1:0]  loop_pred_b2;
    loop_typedef::loop_meta_t   loop_meta_b2;
    ras_typedef::ras_meta_t     ras_meta_b1_b2;

    logic [VADDR_WIDTH-1:0]     ras_target_b1_b2;

    logic                                               ubtb_evict_valid;
    logic [VADDR_WIDTH-1:0]                             ubtb_evict_tag, ubtb_evict_idx;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_pred_valid;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_target_valid;
    logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]       ubtb_evict_pred_targets;
    logic [PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0]   ubtb_evict_pred_offsets;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_pred_br_mask;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_pred_cfi_mask;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_pred_rvc_mask;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_pred_call_mask;
    logic                                               ubtb_evict_pred_is_misalign;
    logic                                               ubtb_evict_pred_is_truncate;
    logic [PREDS_PER_LINE-1:0]                          ubtb_evict_pred_ret_mask;
    
////////////////////////////pipe/////////////////////////////////
    assign pc_b0 = req_pc_b0_i;
    assign req_ready_o = b1_ready;

    assign b1_redirect = redirect_b2_i | fetch_redirect_i | be_redirect_i;
    assign b2_redirect = fetch_redirect_i | be_redirect_i;

    assign b1_ready = ready_b1_i;
    assign b2_ready = ready_b2_i;

    always_ff @(posedge clk) begin
        if (rst) begin
            b1_valid <= 0;
        end 
        else if (b1_redirect) begin
            if (b1_ready) begin
                b1_valid <= req_valid_b0_i;
            end
            else begin
                b1_valid <= 0;
            end
        end
        else if (~b1_ready & b2_ready) begin
            b1_valid <= 0;
        end
        else if (b1_ready) begin
            b1_valid <= req_valid_b0_i;
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            b2_valid <= 0;
        end 
        else if (b2_redirect) begin
            b2_valid <= 0;
        end
        else if (~b2_ready & ready_b2_i) begin
            b2_valid <= 0;
        end
        else if (b2_ready) begin
            if (b1_redirect) begin
                b2_valid <= 0;
            end
            else begin
                b2_valid <= ~invalidate_b1_i & b1_valid;
            end
        end
    end

    always_ff @(posedge clk) begin
        if (b1_ready) begin
            pc_b1 <= pc_b0;
            ftq_tag_b1 <= req_ftq_tag_i;
            is_older_bank_b1_o <= req_is_older_bank_i;
            ras_pred_b1_ff_o <= ras_pred_b1_o;
        end
    end

    always_ff @(posedge clk) begin
        if (b2_ready) begin
            pc_b2 <= pc_b1;
            ghist_info_b2 <= new_ghist_info_b1_i;
            ftq_tag_b2 <= ftq_tag_b1;
            ubtb_pred_b1_b2 <= ubtb_pred_b1;
            ubtb_meta_b1_b2 <= ubtb_meta_b1;
            bht_pred_b1_b2 <= bht_pred_b1;
            bht_meta_b1_b2 <= bht_meta_b1;
            pred_taken_b1_b2 <= pred_taken_b1;
            pred_target_b1_b2 <= pred_target_b1;
            pred_offset_b1_b2 <= pred_offset_b1;
            ras_meta_b1_b2 <= ras_meta_b1_i;
            ras_target_b1_b2 <= ras_target_b1_i;
            is_older_bank_b2_o <= is_older_bank_b1_o;
            ras_pred_b2_ff_o <= ras_pred_b2_o;
        end
    end

////////////////////////////b1//////////////////////////////////
    ubtb #
    (
        .IDX_LSB($clog2(FETCH_WIDTH / 8)),
        .IDX_WIDTH($clog2(BTB_N_ENTRY))
    ) ubtb_u (
        .req_pc_i(pc_b0),
        .req_ready_o(ubtb_ready_b0),

        .pred_o(ubtb_pred_b1),
        .ubtb_meta_o(ubtb_meta_b1),

        .update_valid_i(update_valid_i),
        .update_pred_valid_i(update_pred_valid_i),
        .update_truncate_i(update_truncate_i),
        .update_req_pc_i(update_req_pc_i),
        .update_target_i(update_target_i),
        .update_pred_meta_i(update_ubtb_meta_i),
        .update_br_mask_i(update_br_mask_i),
        .update_cfi_taken_vec_i(update_cfi_taken_vec_i),
        .update_cfi_offset_vec_i(update_cfi_offset_vec_i),
        .update_cfi_mask_i(update_cfi_mask_i),
        .update_rvc_mask_i(update_rvc_mask_i),
        .update_call_mask_i(update_call_mask_i),
        .update_is_misalign_i(update_is_misalign_i),
        .update_ret_mask_i(update_ret_mask_i),

        .evict_valid_o(ubtb_evict_valid),
        .evict_tag_o(ubtb_evict_tag),
        .evict_idx_o(ubtb_evict_idx),
        .evict_pred_valid_o(ubtb_evict_pred_valid),
        .evict_target_valid_o(ubtb_evict_target_valid),
        .evict_pred_offsets_o(ubtb_evict_pred_offsets),
        .evict_pred_targets_o(ubtb_evict_pred_targets),
        .evict_pred_br_mask_o(ubtb_evict_pred_br_mask),
        .evict_pred_cfi_mask_o(ubtb_evict_pred_cfi_mask),
        .evict_pred_rvc_mask_o(ubtb_evict_pred_rvc_mask),
        .evict_pred_call_mask_o(ubtb_evict_pred_call_mask),
        .evict_pred_is_misalign_o(ubtb_evict_pred_is_misalign),
        .evict_pred_ret_mask_o(ubtb_evict_pred_ret_mask),
        .evict_pred_is_truncate_o(ubtb_evict_pred_is_truncate),

        .b1_ready_i(b1_ready),

        .clk(clk),
        .rst(rst)
    );

    
    bht bht_u (
        .req_pc_i(pc_b0),
        .req_ready_o(bht_ready_b0),

        .pred_o(bht_pred_b1),
        .bht_meta_o(bht_meta_b1),

        .update_valid_i(update_valid_i),
        .update_pred_valid_i(update_pred_valid_i),
        .update_req_pc_i(update_req_pc_i),
        .update_target_i(update_target_i),
        .update_pred_meta_i(update_bht_meta_i),
        .update_cfi_mask_i(update_cfi_mask_i),
        .update_cfi_taken_vec_i(update_cfi_taken_vec_i),

        .b1_ready_i(b1_ready),

        .clk(clk),
        .rst(rst)
    );

    // collect the prediction 1-cycle predictor
    always_comb begin
        for (int i = 0; i < PREDS_PER_LINE; i++) begin
            
            pred_invalidate_mask_b1[i] = ubtb_pred_b1.pred_valid[i] & ubtb_pred_b1.cfi_mask[i] &
                                    (ubtb_pred_b1.offset[i] < pc_b1[$clog2(FETCH_WIDTH/8)-1:1]);

            pred_taken_mask_b1[i] = ((bht_pred_b1.valid[i] & bht_pred_b1.taken[i] & ubtb_pred_b1.br_mask[i]) |
                                       (ubtb_pred_b1.cfi_mask[i] & ~ubtb_pred_b1.br_mask[i])) & 
                                    ubtb_pred_b1.pred_valid[i] & ubtb_pred_b1.cfi_mask[i];

            pred_cfi_mask_b1[i] = ubtb_pred_b1.pred_valid[i] & ubtb_pred_b1.cfi_mask[i];

            pred_br_mask_b1[i] = ubtb_pred_b1.pred_valid[i] & ubtb_pred_b1.br_mask[i];
            
            pred_call_mask_b1[i] = ubtb_pred_b1.pred_valid[i] & ubtb_pred_b1.call_mask[i];
            
            pred_ret_mask_b1[i] = ubtb_pred_b1.pred_valid[i] & ubtb_pred_b1.ret_mask[i];

            pred_target_valid_mask_b1[i] = ubtb_pred_b1.target_valid[i];
        end
    end

    assign pred_invalidate_b1 = |pred_invalidate_mask_b1;
    assign pred_valid_b1 = b1_valid & ~pred_invalidate_b1;

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_taken_br_banks_priority_encoder_b1 (
        .sel_i(pred_br_mask_b1 & pred_taken_mask_b1),
        .id_vld_o(pred_has_taken_br_b1),
        .id_o(pred_taken_br_banks_b1)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_br_banks_priority_encoder_b1 (
        .sel_i(pred_br_mask_b1),
        .id_vld_o(pred_has_br_b1),
        .id_o(pred_br_banks_b1)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_call_banks_priority_encoder_b1 (
        .sel_i(pred_call_mask_b1 & pred_taken_mask_b1),
        .id_vld_o(pred_has_taken_call_b1),
        .id_o(pred_call_banks_b1)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_ret_banks_priority_encoder_b1 (
        .sel_i(pred_ret_mask_b1 & pred_taken_mask_b1),
        .id_vld_o(pred_has_taken_ret_b1),
        .id_o(pred_ret_banks_b1)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_taken_banks_priority_encoder_b1 (
        .sel_i(pred_taken_mask_b1),
        .id_vld_o(pred_has_taken_b1),
        .id_o(pred_taken_banks_b1)
    );
    

    assign saw_taken_br_b1 = (pred_taken_br_banks_b1 == pred_taken_banks_b1) & 
                                pred_has_taken_br_b1 & pred_valid_b1;  // whether the first taken insn is a branch
    
    always_comb begin
        if (pred_has_br_b1 & pred_has_taken_b1) begin
            saw_not_taken_br_b1 = (pred_br_banks_b1 < pred_taken_banks_b1) & pred_valid_b1;
        end
        else if (pred_has_br_b1) begin
            saw_not_taken_br_b1 = pred_valid_b1;
        end
        else begin
            saw_not_taken_br_b1 = 0;
        end
    end

    assign ras_pred_b1_o.is_call = (pred_call_banks_b1 == pred_taken_banks_b1) & 
                                pred_has_taken_call_b1 & pred_has_taken_b1 & pred_valid_b1;
    
    assign ras_pred_b1_o.is_ret = (pred_ret_banks_b1 == pred_taken_banks_b1) & 
                                pred_has_taken_ret_b1 & pred_has_taken_b1 & pred_valid_b1;
    
    assign ras_pred_b1_o.call_ret_pc_lob = pred_offset_b1;
    assign ras_pred_b1_o.is_rvc_call_ret = ubtb_pred_b1.pred_valid[pred_taken_banks_b1] & (pred_has_taken_b1 ? ubtb_pred_b1.rvc_mask[pred_taken_banks_b1] : ubtb_pred_b1.rvc_mask[PREDS_PER_LINE-1]);

    assign pred_taken_b1 = pred_valid_b1 & pred_has_taken_b1 & (ras_pred_b1_o.is_ret | pred_target_valid_mask_b1[pred_taken_banks_b1]);
    
    always_comb begin
        pred_truncate_b1 = 0;
        if (pred_valid_b1 & ubtb_pred_b1.is_truncate) begin
            pred_truncate_b1 = 1;
            if (ubtb_pred_b1.rvc_mask[PREDS_PER_LINE-1]) begin
                if (&ubtb_pred_b1.offset[PREDS_PER_LINE-1]) begin
                    pred_truncate_b1 = 0;
                end
            end
            else begin
                if (&ubtb_pred_b1.offset[PREDS_PER_LINE-1:1]) begin
                    pred_truncate_b1 = 0;
                end
            end
        end
    end

    always_comb begin
        if (pred_has_taken_b1) begin
            pred_offset_b1 = ubtb_pred_b1.offset[pred_taken_banks_b1];
        end
        else if (pred_truncate_b1) begin
            pred_offset_b1 = ubtb_pred_b1.offset[PREDS_PER_LINE-1];
        end
        else begin
            pred_offset_b1 = {PRED_OFFSET_WIDTH{1'b1}};
        end
    end

    always_comb begin
        if (pred_taken_b1) begin
            if (ras_pred_b1_o.is_ret) begin
                pred_target_b1 = ras_target_b1_i;
            end
            else begin
                // pred_target_b1 = {pc_b1[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], pred_offset_b1, 1'b0};
                pred_target_b1 = {pc_b1[VADDR_WIDTH-1:BR_OFFSET_WIDTH+1], ubtb_pred_b1.target[pred_taken_banks_b1][BR_OFFSET_WIDTH-1:0], 1'b0};
            end
        end
        else if (pred_truncate_b1) begin
            if (ubtb_pred_b1.rvc_mask[PREDS_PER_LINE-1]) begin
                pred_target_b1 = {pc_b1[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], ubtb_pred_b1.offset[PREDS_PER_LINE-1] + 1'b1, 1'b0};
            end
            else begin
                pred_target_b1 = {pc_b1[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], ubtb_pred_b1.offset[PREDS_PER_LINE-1] + 2'd2, 1'b0};
            end
        end
        else begin
            pred_target_b1 = {pc_b1[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
        end
    end

    // output
    assign vld_b1_o = b1_valid;
    assign pc_b1_o = pc_b1;
    assign is_taken_b1_o = pred_taken_b1;
    assign is_truncate_b1_o = pred_truncate_b1;
    assign offset_b1_o = pred_offset_b1;
    assign fix_ghist_b1_o = b1_valid;
    assign saw_taken_br_b1_o = saw_taken_br_b1;
    assign saw_not_taken_br_b1_o = saw_not_taken_br_b1;
    assign next_pc_b1_o = pred_target_b1;
    assign ftq_tag_b1_o = ftq_tag_b1;
    
////////////////////////////b2/////////////////////////////////

    composed_tage composed_tage_u (
          .req_pc_i(pc_b0),

          .req_ghist_i(ghist_b1_i),
          .bht_valid_i(bht_pred_b1_b2.valid),
          .bht_pred_i (bht_pred_b1_b2.taken),
    
          .pred_o(composed_tage_pred_b2),
          .pred_meta_o(composed_tage_meta_b2),

          .update_valid_i(update_valid_i),
          .update_pred_valid_i(update_pred_valid_i),
          .update_cfi_taken_vec_i(update_cfi_taken_vec_i),
          .update_req_pc_i(update_req_pc_i),
          .update_target_i(update_target_i),
          .update_pred_meta_i(update_composed_tage_meta_i),
          .update_ghist_i(update_ghist_i),
          .update_br_mask_i(update_br_mask_i),
    
          .f1_ready_i(b1_ready),
          .f2_ready_i(b2_ready),
    
          .clk(clk),
          .rst(rst)
      );


    btb btb_u (
        .req_pc_i(pc_b0),
        .req_ghist_i('0),
        .req_ready_o(),

        .ubtb_valid_i(|ubtb_pred_b1_b2.pred_valid),
        .ubtb_target_valid_i(ubtb_pred_b1_b2.target_valid),
        .ubtb_pred_valid_i(ubtb_pred_b1_b2.pred_valid),
        .ubtb_pred_offsets_i(ubtb_pred_b1_b2.offset),
        .ubtb_pred_targets_i(ubtb_pred_b1_b2.target),
        .ubtb_pred_br_mask_i(ubtb_pred_b1_b2.br_mask),
        .ubtb_pred_cfi_mask_i(ubtb_pred_b1_b2.cfi_mask),
        .ubtb_pred_rvc_mask_i(ubtb_pred_b1_b2.rvc_mask),
        .ubtb_pred_call_mask_i(ubtb_pred_b1_b2.call_mask),
        .ubtb_pred_is_misalign_i(ubtb_pred_b1_b2.is_misalign),
        .ubtb_pred_ret_mask_i(ubtb_pred_b1_b2.ret_mask),
        .ubtb_pred_is_truncate_i(ubtb_pred_b1_b2.is_truncate),

        .pred_o(btb_pred_b2),
        .btb_meta_o(btb_meta_b2),

        // evict from ubtb
        .ubtb_evict_i(ubtb_evict_valid),
        .ubtb_evict_tag_i(ubtb_evict_tag),
        .ubtb_evict_idx_i(ubtb_evict_idx),
        .ubtb_evict_target_valid_i(ubtb_evict_target_valid),
        .ubtb_evict_pred_valid_i(ubtb_evict_pred_valid),
        .ubtb_evict_offsets_i(ubtb_evict_pred_offsets),
        .ubtb_evict_targets_i(ubtb_evict_pred_targets),
        .ubtb_evict_br_mask_i(ubtb_evict_pred_br_mask),
        .ubtb_evict_cfi_mask_i(ubtb_evict_pred_cfi_mask),
        .ubtb_evict_rvc_mask_i(ubtb_evict_pred_rvc_mask),
        .ubtb_evict_call_mask_i(ubtb_evict_pred_call_mask),
        .ubtb_evict_is_misalign_i(ubtb_evict_pred_is_misalign),
        .ubtb_evict_ret_mask_i(ubtb_evict_pred_ret_mask),
        .ubtb_evict_is_truncate_i(ubtb_evict_pred_is_truncate),

        .f1_ready_i(b1_ready),
        .f2_ready_i(b2_ready),

        .clk(clk),
        .rst(rst)
  );

//     loop loop_u(
//     .req_valid_b2_i(b2_valid),
//     .req_pc_b2_i(pc_b2),

//     // data for pred
//     .pred_b2_i(composed_tage_pred_b2.taken),

//     .pred_o(loop_pred_b2),
//     .meta_o(loop_meta_b2),
    
//     .f3_redirect_valid_i(fetch_redirect_i),
//     .f3_redirect_taken_i(fetch_redirect_taken_i),
//     .f3_redirect_pc_lob_i(redirect_pc_lob_f3_i),
//     .f3_redirect_meta_i(loop_meta_b2_b3),
//     .be_redirect_valid_i(be_redirect_i),
//     .be_redirect_taken_i(be_redirect_taken_i),
//     .be_redirect_pc_lob_i(redirect_pc_lob_i),
//     .be_redirect_meta_i(redirect_loop_meta_be_i),

//     //update
//     .update_valid_i(update_valid_i),
//     .update_pc_i(update_pc_i),
//     .update_br_mask_i(update_br_mask_i),
//     .update_taken_i(update_cfi_taken_i),
//     .update_meta_i(update_loop_meta_i),

//     .b3_ready_i(b3_ready),
  
//     .clk(clk),
//     .rst(rst)
// );
    assign loop_meta_b2 = 0;
    assign loop_pred_b2 = 0;

  // collect pred from 1-cycle predictors
    assign used_tage_pred_b2 =  vld_b2_o & ~is_older_bank_b2_o ? 
                                (older_bank_saw_not_taken_br_b2_i /*| saw_misalign_br_b2*/ ? composed_tage_pred_b2[1] : composed_tage_pred_b2[0]) :
                                composed_tage_pred_b2[0];
    
    assign used_tage_meta_b2 = vld_b2_o & ~is_older_bank_b2_o ? 
                                (older_bank_saw_not_taken_br_b2_i/* | saw_misalign_br_b2*/ ? composed_tage_meta_b2[1] : composed_tage_meta_b2[0]) :
                                composed_tage_meta_b2[0];

    always_comb begin
        for (int i = 0; i < PREDS_PER_LINE; i++) begin

            pred_invalidate_mask_b2[i] = btb_pred_b2.cfi_mask[i] & btb_pred_b2.pred_valid[i] &
                                    (btb_pred_b2.offset[i] < pc_b2[$clog2(FETCH_WIDTH/8)-1:1]);

            pred_taken_mask_b2[i] = ((used_tage_pred_b2.valid[i] & used_tage_pred_b2.taken[i] & btb_pred_b2.br_mask[i]) |
                                        (~btb_pred_b2.br_mask[i] & btb_pred_b2.cfi_mask[i])) & btb_pred_b2.pred_valid[i];

            pred_cfi_mask_b2[i] = btb_pred_b2.pred_valid[i] & btb_pred_b2.cfi_mask[i];

            pred_br_mask_b2[i] = btb_pred_b2.pred_valid[i] & btb_pred_b2.br_mask[i];

            pred_call_mask_b2[i] = btb_pred_b2.pred_valid[i] & btb_pred_b2.call_mask[i];

            pred_ret_mask_b2[i] = btb_pred_b2.pred_valid[i] & btb_pred_b2.ret_mask[i];

            pred_target_valid_mask_b2[i] = btb_pred_b2.target_valid[i];
        end
    end

    assign pred_invalidate_b2 = |pred_invalidate_mask_b2;
    assign pred_valid_b2 = b2_valid & ~pred_invalidate_b2;

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_taken_br_banks_priority_encoder_b2 (
        .sel_i(pred_br_mask_b2 & pred_taken_mask_b2),
        .id_vld_o(pred_has_taken_br_b2),
        .id_o(pred_taken_br_banks_b2)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_br_banks_priority_encoder_b2 (
        .sel_i(pred_br_mask_b2),
        .id_vld_o(pred_has_br_b2),
        .id_o(pred_br_banks_b2)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_call_banks_priority_encoder_b2 (
        .sel_i(pred_call_mask_b2 & pred_taken_mask_b2),
        .id_vld_o(pred_has_taken_call_b2),
        .id_o(pred_call_banks_b2)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_ret_banks_priority_encoder_b2 (
        .sel_i(pred_ret_mask_b2 & pred_taken_mask_b2),
        .id_vld_o(pred_has_taken_ret_b2),
        .id_o(pred_ret_banks_b2)
    );

    priority_encoder
    #(
        .SEL_WIDTH(PREDS_PER_LINE)
    ) u_taken_banks_priority_encoder_b2 (
        .sel_i(pred_taken_mask_b2),
        .id_vld_o(pred_has_taken_b2),
        .id_o(pred_taken_banks_b2)
    );


    assign saw_taken_br_b2 = (pred_taken_br_banks_b2 == pred_taken_banks_b2) & 
                                pred_has_taken_br_b2 & pred_valid_b2;  // whether the first taken insn is a branch
    
    always_comb begin
        if (pred_has_br_b2 & pred_has_taken_b2) begin
            saw_not_taken_br_b2 = (pred_br_banks_b2 < pred_taken_banks_b2) & pred_valid_b2;
        end
        else if (pred_has_br_b2) begin
            saw_not_taken_br_b2 = pred_valid_b2;
        end
        else begin
            saw_not_taken_br_b2 = 0;
        end
    end

    assign ras_pred_b2_o.is_call = (pred_call_banks_b2 == pred_taken_banks_b2) & 
                                pred_has_taken_call_b2 & pred_valid_b2;
    
    assign ras_pred_b2_o.is_ret = (pred_ret_banks_b2 == pred_taken_banks_b2) & 
                                pred_has_taken_ret_b2 & pred_valid_b2;
    
    assign ras_pred_b2_o.call_ret_pc_lob = pred_offset_b2;
    assign ras_pred_b2_o.is_rvc_call_ret = pred_has_taken_b2 ? btb_pred_b2.rvc_mask[pred_taken_banks_b2] : btb_pred_b2.rvc_mask[PREDS_PER_LINE-1];

    assign pred_taken_b2 = pred_valid_b2 & pred_has_taken_b2 & (ras_pred_b2_o.is_ret | pred_target_valid_mask_b2[pred_taken_banks_b2]);

    always_comb begin
        pred_truncate_b2 = 0;
        if (pred_valid_b2 & btb_pred_b2.is_truncate) begin
            pred_truncate_b2 = 1;
            if (btb_pred_b2.rvc_mask[PREDS_PER_LINE-1]) begin
                if (&btb_pred_b2.offset[PREDS_PER_LINE-1]) begin
                    pred_truncate_b2 = 0;
                end
            end
            else begin
                if (&btb_pred_b2.offset[PREDS_PER_LINE-1:1]) begin
                    pred_truncate_b2 = 0;
                end
            end
        end
    end


    always_comb begin
        if (pred_has_taken_b2) begin
            pred_offset_b2 = btb_pred_b2.offset[pred_taken_banks_b2];
        end
        else if (pred_truncate_b2) begin
            pred_offset_b2 = btb_pred_b2.offset[PREDS_PER_LINE-1];
        end
        else begin
            pred_offset_b2 = {PRED_OFFSET_WIDTH{1'b1}};
        end
    end

    always_comb begin
        if (pred_taken_b2) begin
            if (ras_pred_b2_o.is_ret) begin
                pred_target_b2 = ras_target_b1_b2;
            end
            else begin
                // pred_target_b2 = {pc_b2[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], pred_offset_b2, 1'b0};
                pred_target_b2 = {pc_b2[VADDR_WIDTH-1:BR_OFFSET_WIDTH+1], btb_pred_b2.target[pred_taken_banks_b2][BR_OFFSET_WIDTH-1:0], 1'b0};
            end
        end
        else if (pred_truncate_b2) begin
            if (btb_pred_b2.rvc_mask[PREDS_PER_LINE-1]) begin
                pred_target_b2 = {pc_b2[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], btb_pred_b2.offset[PREDS_PER_LINE-1] + 1'b1, 1'b0};
            end
            else begin
                pred_target_b2 = {pc_b2[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], btb_pred_b2.offset[PREDS_PER_LINE-1] + 2'd2, 1'b0};
            end
        end
        else begin
            pred_target_b2 = {pc_b2[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)] + 1'b1, {$clog2(FETCH_WIDTH/8){1'b0}}};
        end
    end

    assign vld_b2_o = b2_valid;
    assign pc_b2_o = pc_b2;
    assign ftq_tag_b2_o = ftq_tag_b2;
    assign is_taken_b2_o = pred_taken_b2;
    assign is_truncate_b2_o = pred_truncate_b2;
    assign offset_b2_o = pred_offset_b2;
    assign ghist_info_b2_o = ghist_info_b2;
    assign saw_not_taken_br_b2_o = saw_not_taken_br_b2;
    assign saw_taken_br_b2_o = saw_taken_br_b2;
    assign next_pc_b2_o = pred_target_b2;
    assign ras_meta_b2_o = ras_meta_b1_b2;
    assign saw_misalign_br_b2 = btb_pred_b2.is_misalign & pred_br_mask_b2[0] & pred_valid_b2;

    assign ubtb_meta_b2_o = ubtb_meta_b1_b2;
    assign bht_meta_b2_o = bht_meta_b1_b2;
    assign composed_tage_meta_b2_o = used_tage_meta_b2;
    assign btb_meta_b2_o = btb_meta_b2;
    assign loop_meta_b2_o = loop_meta_b2;



`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (~rst) begin
            if (pred_valid_b1) begin
                for (int i = 0; i < PREDS_PER_LINE; i++) begin
                    if (pred_cfi_mask_b1[i] & (&ubtb_pred_b1.offset[i]) & (~ubtb_pred_b1.rvc_mask[i])) begin
                        $fatal("the info of misalign cfi should not write at the former fetch packet!");
                    end
                end
            end
            if (update_valid_i) begin
                for (int i = 0; i < PREDS_PER_LINE; i++) begin
                    if ((((update_pred_valid_i >> i) & 1'b1) == 0) & ((update_pred_valid_i >> i) != 0)) begin
                        $fatal("update_pred_valid_i error");
                    end
                end
            end
        end
    end
`endif


endmodule
