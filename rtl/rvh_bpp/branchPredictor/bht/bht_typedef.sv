package bht_typedef;

  localparam VADDR_WIDTH = 39;
  localparam FETCH_WIDTH = 128;

  localparam PREDS_PER_LINE = 2;

  localparam BHT_N_ENTRY = 128;
  localparam BHT_CTR_SZ = 2;

typedef struct packed {
    logic [PREDS_PER_LINE-1:0] taken;
    logic [PREDS_PER_LINE-1:0] valid;
} bht_pred_t;

typedef struct packed {
    logic [PREDS_PER_LINE-1:0][BHT_CTR_SZ-1:0] ctrs;
    logic [$clog2(BHT_N_ENTRY)-1:0] idx;
  } bht_meta_t;

endpackage : bht_typedef
