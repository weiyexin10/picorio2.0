module bht
    import bht_typedef::*;
#(

)
(
    input logic [VADDR_WIDTH-1:0]           req_pc_i,
    output logic                            req_ready_o,

    output bht_pred_t                       pred_o,
    output bht_meta_t                       bht_meta_o,
    
    // update
    input logic                             update_valid_i,
    input logic     [PREDS_PER_LINE-1:0]    update_pred_valid_i,
    input logic     [VADDR_WIDTH-1:0]       update_req_pc_i,
    input logic     [VADDR_WIDTH-1:0]       update_target_i,
    input bht_meta_t                        update_pred_meta_i,
    input logic [PREDS_PER_LINE-1:0]        update_cfi_mask_i,
    input logic[PREDS_PER_LINE-1:0]         update_cfi_taken_vec_i,

    input logic                             b1_ready_i,

    input logic                             clk,
    input logic                             rst
);

localparam IDX_HASH_CHUNK_LEN = $clog2(BHT_N_ENTRY);
localparam IDX_HASH_CHUNKS = ((VADDR_WIDTH - 1) % IDX_HASH_CHUNK_LEN == 0) ? (VADDR_WIDTH - 1) / IDX_HASH_CHUNK_LEN : (VADDR_WIDTH - 1) / IDX_HASH_CHUNK_LEN + 1;

logic [PREDS_PER_LINE-1:0][BHT_CTR_SZ-1:0]  update_ctrs_ori, update_ctrs_new;
logic [$clog2(BHT_N_ENTRY)-1:0]             bht_idx_s0, bht_idx_s1, update_bht_idx;

logic                                       ram_we;

logic                                       hold_rslt_ff;
bht_pred_t                                  pred_ff;
bht_meta_t                                  bht_meta_ff;
bht_pred_t                                  pred;
bht_meta_t                                  bht_meta;

function automatic logic [IDX_HASH_CHUNK_LEN-1:0] bht_get_idx (
        input [VADDR_WIDTH-1:0]                pc_i
    );
    begin
        bht_get_idx = 0;
        for (int i = 0; i < IDX_HASH_CHUNKS - 1; i++) begin
            bht_get_idx ^= pc_i[1 + i * IDX_HASH_CHUNK_LEN+:IDX_HASH_CHUNK_LEN];
        end
        bht_get_idx ^= pc_i[VADDR_WIDTH-1 : 1 + (IDX_HASH_CHUNKS - 1) * IDX_HASH_CHUNK_LEN];
    end
endfunction

assign bht_idx_s0 = bht_get_idx(req_pc_i);// ^ req_ghist_i;
// update
assign update_bht_idx = bht_get_idx(update_req_pc_i);

always_comb begin
    for (int i=0; i<PREDS_PER_LINE; i++) begin
        pred.taken[i] = ((bht_meta.ctrs[i] + 1'b1) >> (BHT_CTR_SZ-1)) & 1'b1;
    end
end

assign ram_we = update_valid_i;
assign req_ready_o = 1;

logic [BHT_N_ENTRY-1:0][PREDS_PER_LINE-1:0][BHT_CTR_SZ-1:0] bht_ram;
logic [BHT_N_ENTRY-1:0]                                     valid_ram;

always_ff @(posedge clk) begin
    if (rst) begin
        valid_ram <= 0;
    end
    else if (ram_we) begin
        valid_ram[update_bht_idx] <= 1'b1;
    end
end
always_comb begin
    pred.valid = {PREDS_PER_LINE{valid_ram[bht_idx_s1]}};
end

always_ff @(posedge clk) begin
    if (rst) begin
        bht_ram <= 0;
    end
    else if (ram_we) begin
        for (int i = 0; i < PREDS_PER_LINE; i++) begin
            bht_ram[update_bht_idx][i] <= update_ctrs_new[i];
        end
    end
end
always_comb begin
    for (int i = 0; i < PREDS_PER_LINE; i++) begin
        bht_meta.ctrs[i] = bht_ram[bht_idx_s1][i];
    end
end

always_comb begin
    for (int i = 0; i < PREDS_PER_LINE; i++) begin
        update_ctrs_ori[i] = bht_ram[update_bht_idx][i];
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        bht_idx_s1 <= 0;
    end
    else begin
        bht_idx_s1 <= bht_idx_s0;
    end
end
assign bht_meta.idx = bht_idx_s1;

always_ff @(posedge clk) begin
    if (rst) begin
        hold_rslt_ff <= 0;
    end
    else begin
        hold_rslt_ff <= (~b1_ready_i);
    end
end

always_ff @(posedge clk) begin
    if (~hold_rslt_ff) begin
        pred_ff <= pred;
        bht_meta_ff <= bht_meta;
    end
end

assign pred_o = hold_rslt_ff ? pred_ff : pred;
assign bht_meta_o = hold_rslt_ff ? bht_meta_ff : bht_meta;

always_comb begin
    update_ctrs_new = update_ctrs_ori;
    for (int i=0; i<PREDS_PER_LINE; i++) begin
        if (update_pred_valid_i[i]) begin // there is a branch insn
            if (update_cfi_taken_vec_i[i]) begin     // the last insn is a taken cfi
                if (~((&update_ctrs_ori[i][BHT_CTR_SZ-1:1]) & (~update_ctrs_ori[i][0]))) begin // update_ctrs_ori != 11...10
                    update_ctrs_new[i] = update_ctrs_ori[i] + 1'b1;
                end
            end
            else begin                      // there is no taken cfi
                if (~(&update_ctrs_ori[i])) begin
                    update_ctrs_new[i] = update_ctrs_ori[i] - 1'b1;
                end
            end
        end
    end
end


endmodule
