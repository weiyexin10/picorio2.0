module tage
    import tage_typedef::*;
#(
parameter int unsigned NO = 0,
parameter int unsigned HIST_LEN_TAB[TAGE_TABLES] = '{5, 9, 15, 25, 44, 130},
parameter int unsigned TAG_LEN_TAB[TAGE_TABLES] = '{7, 7, 8, 8, 9, 9},
parameter int unsigned N_SETS_TAB[TAGE_TABLES] = '{128, 128, 256, 256, 128, 128}
)
(
    input logic     [VADDR_WIDTH-1:0]    req_pc_i,
    // input logic                         req_valid_i,
    input logic     [GHIST_LEN-1:0]              req_ghist_i,
//    input f1_pred_t [PREDS_PER_LINE-1:0] f1_pred_at_f2_i,
    
    input  logic                          bht_valid_i,
    input  logic                          bht_pred_i,

    output tage_pred_t [1:0]                   pred_o,
    output tage_pred_meta_t [1:0]              pred_meta_o,
    
    input logic                             update_valid_i,
    input logic     [VADDR_WIDTH-1:0]        update_req_pc_i,
    input logic     [GHIST_LEN-1:0]         update_ghist_i,
    input tage_pred_meta_t                  update_pred_meta_i,
    
    input logic                             update_is_taken_i,
    // input logic                             update_is_mispred_i,

    input logic                             f1_ready_i,
    input logic                             f2_ready_i,
    
    input logic                         clk,
    input logic                         rst
);

function [$clog2(TAGE_TABLES)-1:0] priority_encoder (
    input [TAGE_TABLES-1:0]                in
    );
    begin
        priority_encoder = 0;
        for (int i=TAGE_TABLES-1; i>=0; i--) begin
            if (in[i]) begin
                priority_encoder = i;
            end
        end
    end
endfunction

tage_table_output_t [TAGE_TABLES-1:0][1:0]      tab_pred_out_raw;
tage_table_output_t [1:0][TAGE_TABLES-1:0]      tab_pred_out;

// update
logic [TAGE_TABLES-1:0]                    tab_update_mask;
logic [TAGE_TABLES-1:0]                    tab_update_taken;
logic [TAGE_TABLES-1:0]                    tab_update_alloc;
logic [TAGE_TABLES-1:0][2:0]               tab_update_old_ctr;
logic [TAGE_TABLES-1:0]                    tab_update_u_mask;
logic [TAGE_TABLES-1:0][1:0]               tab_update_u;

logic [6:0]                                         alloc_lfsr; 
logic [1:0][TAGE_TABLES-1:0]                             altpred_vec;
logic [1:0]                                              altpred, final_altpred;
logic [1:0][TAGE_TABLES-1:0]                             pred_taken_vec;
logic [1:0]                                              pred_taken;

logic [1:0]                                              provided;
logic [1:0][$clog2(TAGE_TABLES)-1:0]                     provider;

///////////////////////////////////////////////////////////////////////////////
logic [1:0]                                              _final_altpred;
logic [1:0]                                              _provided;
logic [1:0][$clog2(TAGE_TABLES)-1:0]                     _provider;
logic [1:0]                                              _pred_taken;
///////////////////////////////////////////////////////////////////////////////
logic [1:0][TAGE_TABLES-1:0] allocatable_slots;
logic [1:0][$clog2(TAGE_TABLES)-1:0] first_allocatable_table, masked_allocatable_table, alloc_table;

// update
// logic [$clog2(PREDS_PER_LINE)-1:0]    update_which_bank;
logic [$clog2(TAGE_TABLES)-1:0] update_provider;
logic                           update_is_mispred;
logic [$clog2(TAGE_TABLES)-1:0]  update_alloc_which_tab;

logic                               req_valid_i;
assign req_valid_i = 1'b1;

// assign update_which_bank = update_pc_i[$clog2(FETCH_WIDTH/8/PREDS_PER_LINE)+:$clog2(PREDS_PER_LINE)];

tage_lfsr lfsr_u(    
    .lfsr(alloc_lfsr),
    .en(f1_ready_i),
    .clk(clk),
    .rst(rst)
);

generate for (genvar i=0; i<TAGE_TABLES; i++) begin : TABLES
    tage_table 
    #(
        .N_SETS(N_SETS_TAB[i]),
        .HIST_LEN(HIST_LEN_TAB[i]),
        .TAG_LEN(TAG_LEN_TAB[i]),
        .TABLE_NO(i)
    ) table_u
    (
        .req_valid_i(req_valid_i),
        .req_pc_i(req_pc_i),
        .req_ghist_i(req_ghist_i),
        
        .table_out_o(tab_pred_out_raw[i]),
        
        // update
        .update_valid_i(tab_update_mask[i]),
        .update_taken_i(tab_update_taken[i]),
        .update_alloc_i(tab_update_alloc[i]),
        .update_old_ctr_i(tab_update_old_ctr[i]),
        .update_req_pc_i(update_req_pc_i),
        .update_ghist_i(update_ghist_i),
        .update_u_valid_i(tab_update_u_mask[i]),
        .update_u_i(tab_update_u[i]),
        .f1_ready_i(f1_ready_i),
        .f2_ready_i(f2_ready_i),
        
        .clk(clk),
        .rst(rst)
    );
  end // block: TABLES
endgenerate

generate
    for (genvar bank = 0; bank < 2; bank++) begin
        for (genvar tab = 0; tab < TAGE_TABLES; tab++) begin
            assign tab_pred_out[bank][tab] = tab_pred_out_raw[tab][bank];
        end
    end
endgenerate

generate 
    for (genvar bank = 0; bank < 2; bank++) begin : gen_banks_output

        assign altpred_vec[bank][0] = (bht_valid_i & bht_pred_i);
        for (genvar i = 1; i < TAGE_TABLES; i++) begin : TAB_ALTPRED_RSLT
            always_comb begin
                if (tab_pred_out[bank][i].valid) begin
                    if ((tab_pred_out[bank][i].u == 0) & ((tab_pred_out[bank][i].ctr == {1'b0, {(TAGE_CTR_LEN-1){1'b1}}}) | (tab_pred_out[bank][i].ctr == {1'b1, {(TAGE_CTR_LEN-1){1'b0}}}))) begin
                        altpred_vec[bank][i] = altpred_vec[bank][i-1];
                    end
                    else begin
                        altpred_vec[bank][i] = pred_taken_vec[bank][i - 1'b1];
                    end
                end
                else begin
                    altpred_vec[bank][i] = altpred_vec[bank][i-1];
                end
            end
        end

        always_comb begin
            if (tab_pred_out[bank][0].valid) begin
                if ((tab_pred_out[bank][0].u == 0) & ((tab_pred_out[bank][0].ctr == {1'b0, {(TAGE_CTR_LEN-1){1'b1}}}) | (tab_pred_out[bank][0].ctr == {1'b1, {(TAGE_CTR_LEN-1){1'b0}}}))) begin
                    pred_taken_vec[bank][0] = altpred_vec[bank][0];
                end
                else begin
                    pred_taken_vec[bank][0] = tab_pred_out[bank][0].ctr[TAGE_CTR_LEN-1];
                end
            end
            else begin
                pred_taken_vec[bank][0] = altpred_vec[bank][0];
            end
        end

        for (genvar i = 1; i < TAGE_TABLES; i++) begin : TAB_PRED_RSLT
            always_comb begin
                if (tab_pred_out[bank][i].valid) begin
                    if ((tab_pred_out[bank][i].u == 0) & ((tab_pred_out[bank][i].ctr == {1'b0, {(TAGE_CTR_LEN-1){1'b1}}}) | (tab_pred_out[bank][i].ctr == {1'b1, {(TAGE_CTR_LEN-1){1'b0}}}))) begin
                        pred_taken_vec[bank][i] = altpred_vec[bank][i-1];
                    end
                    else begin
                        pred_taken_vec[bank][i] = tab_pred_out[bank][i].ctr[TAGE_CTR_LEN-1];
                    end
                end
                else begin
                    pred_taken_vec[bank][i] = pred_taken_vec[bank][i - 1'b1];
                end
            end
        end
        

        always_comb begin
            provided[bank] = 0;
            provider[bank] = 0;
            for (int i = 0; i < TAGE_TABLES; i++) begin
                if (tab_pred_out[bank][i].valid) begin
                    provided[bank] = 1'b1;
                    provider[bank] = i[$clog2(TAGE_TABLES)-1:0];
                end
            end
        end

        always_comb begin
            if (~provided[bank]) begin
                final_altpred[bank] = bht_valid_i & bht_pred_i;
            end
            else if (provider[bank] == 0) begin
                final_altpred[bank] = bht_valid_i & bht_pred_i;
            end
            else begin
                final_altpred[bank] = altpred_vec[bank][provider[bank]];
            end
        end

        // assign final_altpred = provided ? altpred_vec[provider - 1'b1] : altpred_vec[TAGE_TABLES-1];
        assign pred_taken[bank] = pred_taken_vec[bank][TAGE_TABLES-1];
    end
endgenerate
///////////////////////////////////////////////////////////////////////////////
// // make prediction
// always_comb begin

//     altpred = 0;//f1_pred_at_f2_i[i].taken;
//     _final_altpred = 0;//f1_pred_at_f2_i[i].taken;
//     _provided = 0;
//     _provider = 0;
//     _pred_taken = 0;//f1_pred_at_f2_i[i].taken;
    
//     for (int j=0; j<TAGE_TABLES; j++) begin
//         if (tab_pred_out[j].valid) begin // hit
//             if ((tab_pred_out[j].ctr == {1'b0, {(TAGE_CTR_LEN-1){1'b1}}}) | (tab_pred_out[j].ctr == {1'b1, {(TAGE_CTR_LEN-1){1'b0}}})) begin
//                 _pred_taken = altpred;
//             end
//             else begin
//                 _pred_taken = tab_pred_out[j].ctr[TAGE_CTR_LEN-1];
//             end
//             _final_altpred = altpred;
//         end
//         _provided = _provided || tab_pred_out[j].valid;
//         _provider = tab_pred_out[j].valid ? j[$clog2(TAGE_TABLES)-1:0] : _provider;
//         altpred = tab_pred_out[j].valid ? tab_pred_out[j].ctr[TAGE_CTR_LEN-1] : altpred;
//     end

// end

// always_ff @(posedge clk) begin
//     if (!rst) begin
//         if (_final_altpred != final_altpred) begin
//             $display("[%d]: bank[%d] final_altpred different!", $time(), NO);
//             $finish();
//         end
//         if (_provided != provided) begin
//             $display("[%d]: bank[%d] provided different!", $time(), NO);
//             $finish();
//         end
//         if (_provider != provider) begin
//             $display("[%d]: bank[%d] provider different!", $time(), NO);
//             $finish();
//         end
//         if (_pred_taken != pred_taken) begin
//             $display("[%d]: bank[%d] pred_taken different!", $time(), NO);
//             $finish();
//         end
//     end
// end

///////////////////////////////////////////////////////////////////////////////
// determine allocatable table in case of misprediction
generate 
    for (genvar bank = 0; bank < 2; bank++) begin : gen_banks_alloc
        always_comb begin
            allocatable_slots[bank] = 0;
            first_allocatable_table[bank] = 0;
            masked_allocatable_table[bank] = 0;
            alloc_table[bank] = 0;

            // Create a mask of tables which did not hit our query, and also contain useless entries and also uses a longer history than the provider
            for (int k=0; k<TAGE_TABLES; k++) begin
                allocatable_slots[bank][k] =  (k > provider[bank]) /*&& provided*/ && ((!tab_pred_out[bank][k].valid) || (tab_pred_out[bank][k].u == 0));
            end
            
            first_allocatable_table[bank] = priority_encoder(allocatable_slots[bank]);
            masked_allocatable_table[bank] = priority_encoder(allocatable_slots[bank] & alloc_lfsr);
            alloc_table[bank] = allocatable_slots[bank][masked_allocatable_table[bank]] ? masked_allocatable_table[bank] : first_allocatable_table[bank];
        end

        // read prediction
        always_comb begin
            pred_o[bank].taken = provided[bank] ? pred_taken[bank] : bht_pred_i;
            pred_o[bank].valid = provided[bank] | bht_valid_i;
            
            pred_meta_o[bank].provider_valid = provided[bank];
            pred_meta_o[bank].provider = provider[bank];
            pred_meta_o[bank].alt_differs = pred_taken[bank] != final_altpred[bank];
            pred_meta_o[bank].provider_ctr = tab_pred_out[bank][provider[bank]].ctr;
            pred_meta_o[bank].taken = pred_taken[bank];
            pred_meta_o[bank].valid = provided[bank];
            pred_meta_o[bank].allocate_valid = allocatable_slots[bank] != 0;
            pred_meta_o[bank].allocate = alloc_table[bank];
        end

        always_comb begin
            for (int i=0; i<TAGE_TABLES; i++) begin
                pred_meta_o[bank].ubits[i] = tab_pred_out[bank][i].u;
            end
        end
    end
endgenerate

// update
// mispred
always_comb begin
    update_is_mispred = 1;
    if (update_pred_meta_i.valid) begin
        update_is_mispred = update_pred_meta_i.taken != update_is_taken_i;
    end
end

always_comb begin
    tab_update_mask = 0;
    tab_update_u_mask = 0;
    tab_update_u = 0;
    tab_update_taken = 0;
    tab_update_old_ctr = 0;
    tab_update_alloc = 0;
    update_alloc_which_tab = 0;

    update_provider = update_pred_meta_i.provider;
    if (update_valid_i) begin

        if (update_pred_meta_i.provider_valid) begin
            tab_update_mask[update_provider] = 1;
            tab_update_u_mask[update_provider] = 1;
            if (update_pred_meta_i.alt_differs) begin
                if (update_is_mispred) begin
                    tab_update_u[update_provider] = |update_pred_meta_i.ubits[update_provider] ? update_pred_meta_i.ubits[update_provider] - 1'b1 : 1'b0;
                end
                else begin
                    tab_update_u[update_provider] = &update_pred_meta_i.ubits[update_provider] ? update_pred_meta_i.ubits[update_provider] :  update_pred_meta_i.ubits[update_provider] + 1'b1;
                end
            end
            else begin
                tab_update_u[update_provider] = update_pred_meta_i.ubits[update_provider];
            end
            tab_update_taken[update_provider] = update_is_taken_i;
            tab_update_old_ctr[update_provider] = update_pred_meta_i.provider_ctr;
            tab_update_alloc[update_provider] = 0;
        end

        if (update_is_mispred) begin
            update_alloc_which_tab = update_pred_meta_i.allocate;
            if (update_pred_meta_i.allocate_valid) begin
                tab_update_mask[update_alloc_which_tab] = 1;
                tab_update_taken[update_alloc_which_tab] = update_is_taken_i;
                tab_update_alloc[update_alloc_which_tab] = 1;
                tab_update_u_mask[update_alloc_which_tab] = 1;
                tab_update_u[update_alloc_which_tab] = 0;
            end
            else begin
                update_provider = update_pred_meta_i.provider;
                for (int i=0; i<TAGE_TABLES; i++) begin
                    if (update_pred_meta_i.provider_valid && (i > update_provider)) begin
                        tab_update_u_mask[i] = 1;
                        tab_update_u[i] = |update_pred_meta_i.ubits[i] ? update_pred_meta_i.ubits[i] - 1'b1 : 1'b0;;
                    end
                end
            end
        end

    end
end

endmodule




