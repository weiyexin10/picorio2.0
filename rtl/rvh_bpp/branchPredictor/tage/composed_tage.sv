module composed_tage
    import tage_typedef::*;
#(
)
(
    input logic     [VADDR_WIDTH-1:0]                   req_pc_i,
    input logic     [GHIST_LEN-1:0]                     req_ghist_i,

    input  logic    [PREDS_PER_LINE-1:0]                bht_valid_i,
    input  logic    [PREDS_PER_LINE-1:0]                bht_pred_i,

    output composed_tage_pred_t[1:0]                    pred_o,
    output composed_tage_pred_meta_t[1:0]               pred_meta_o,

    input logic                                         update_valid_i,
    input logic  [PREDS_PER_LINE-1:0]                   update_pred_valid_i,
    input logic[PREDS_PER_LINE-1:0]                     update_cfi_taken_vec_i,
    input logic     [VADDR_WIDTH-1:0]                   update_req_pc_i,
    input logic     [GHIST_LEN-1:0]                     update_ghist_i,
    input logic     [VADDR_WIDTH-1:0]                   update_target_i,
    input composed_tage_pred_meta_t                     update_pred_meta_i,

    input logic [PREDS_PER_LINE-1:0]                    update_br_mask_i,

    input logic                                         f1_ready_i,
    input logic                                         f2_ready_i,

    input logic                                         clk,
    input logic                                         rst
);

tage_pred_t [PREDS_PER_LINE-1:0][1:0]           preds;
tage_pred_meta_t [PREDS_PER_LINE-1:0][1:0]      metas;
logic [$clog2(PREDS_PER_LINE)-1:0]              first_updated_bank, last_updated_bank;
logic [PREDS_PER_LINE-1:0]                      update_per_bank, update_taken_per_bank;

generate 
    for (genvar bank = 0; bank < 2; bank++) begin : gen_pred
        always_comb begin
            for (int i=0; i<PREDS_PER_LINE; i++) begin
                pred_o[bank].valid[i] = preds[i][bank].valid;
                pred_o[bank].taken[i] = preds[i][bank].taken;
            end
        end

        assign pred_meta_o[bank].final_taken = pred_o[bank].taken;
        assign pred_meta_o[bank].final_valid = pred_o[bank].valid;
        for (genvar i=0; i<PREDS_PER_LINE; i++) begin : gen_meta
            assign pred_meta_o[bank].tage_pred_metas[i] = metas[i][bank];
        end
    end
endgenerate

always_comb begin
    for (int i = 0; i < PREDS_PER_LINE; i++) begin
        update_per_bank[i] = update_valid_i & update_br_mask_i[i] & update_pred_valid_i[i];
        update_taken_per_bank[i] = update_cfi_taken_vec_i[i];
    end
end

generate for (genvar i=0; i<PREDS_PER_LINE; i++) begin : TAGE_BANKS
    tage #(.NO(i))tage_u (
        .req_pc_i(req_pc_i),
        .req_ghist_i(req_ghist_i),
        .bht_valid_i(bht_valid_i[i]),
        .bht_pred_i(bht_pred_i[i]),
        
        .pred_o(preds[i]),
        .pred_meta_o(metas[i]),
        
        .update_valid_i(update_per_bank[i]),
        .update_req_pc_i(update_req_pc_i),
        .update_ghist_i(update_ghist_i),
        .update_pred_meta_i(update_pred_meta_i.tage_pred_metas[i]),

        .update_is_taken_i(update_taken_per_bank[i]),

        .f1_ready_i(f1_ready_i),
        .f2_ready_i(f2_ready_i),
        
        .clk(clk),
        .rst(rst)
    );
end
endgenerate


endmodule
