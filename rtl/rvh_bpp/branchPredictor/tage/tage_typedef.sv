package tage_typedef;

  localparam VADDR_WIDTH = 39;
  localparam FETCH_WIDTH = 128;
  localparam PREDS_PER_LINE = 2;
  localparam GHIST_LEN    = 130;
  
  localparam TAGE_TABLES = 6;
  localparam TAGE_CTR_LEN = 3;
  localparam TAGE_UBIT_LEN = 2;
  localparam TAGE_MAX_TAG_LEN = 12;
  localparam TAGE_CLEAR_UBIT_PERIOD = 2048;

  typedef struct packed {
    logic       valid;
    logic [1:0] u;
    logic [2:0] ctr;
  } tage_table_output_t;

  typedef struct packed {
    logic valid;
    logic taken;
  } tage_pred_t;

  typedef struct packed {
    logic                                 provider_valid;
    logic [$clog2(TAGE_TABLES)-1:0]      provider;
    logic                                 alt_differs;
    logic [TAGE_TABLES-1:0][1:0]          ubits;
    logic [2:0]                           provider_ctr;
    logic                                 allocate_valid;
    logic [$clog2(TAGE_TABLES)-1:0]      allocate;
    // temporary added for debug
    logic                                 valid;
    logic                                 taken;
  } tage_pred_meta_t;

  typedef struct packed {
    logic [PREDS_PER_LINE-1:0] valid;
    logic [PREDS_PER_LINE-1:0] taken;
  } composed_tage_pred_t;

  typedef struct packed {
    tage_pred_meta_t [PREDS_PER_LINE-1:0] tage_pred_metas;
    logic [PREDS_PER_LINE-1:0] final_valid;
    logic [PREDS_PER_LINE-1:0] final_taken;
  } composed_tage_pred_meta_t;
  
endpackage : tage_typedef
