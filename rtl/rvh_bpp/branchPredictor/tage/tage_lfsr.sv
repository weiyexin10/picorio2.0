module tage_lfsr
(    // x^7 + x^6 + 1
    output logic [6:0]  lfsr,
    input logic         en,
    input               clk,
    input               rst
);

always_ff@(posedge clk) begin
    if (rst) begin
        lfsr <= 7'b1111111;
    end
    else begin 
        if (en) begin
            lfsr[6:1] <= lfsr[5:0];
            lfsr[0] <= lfsr[6] ^ lfsr[5];
        end
    end
end



endmodule