module tage_table
    import tage_typedef::*;
#(
parameter int unsigned N_SETS = 128,
parameter int unsigned HIST_LEN = 2,
parameter int unsigned TAG_LEN = 7,
parameter int unsigned TABLE_NO = 0,
parameter int unsigned N_IDX_CHUNKS = (HIST_LEN+$clog2(N_SETS)-1)/$clog2(N_SETS),
parameter int unsigned N_TAG_CHUNKS = (HIST_LEN+TAG_LEN-1)/TAG_LEN
)
(
    input logic                         req_valid_i,
    input logic [VADDR_WIDTH-1:0]        req_pc_i,
    input logic [GHIST_LEN-1:0]          req_ghist_i,
    
    // data for pred
    output tage_table_output_t[1:0]  table_out_o,
    
    // update
    input logic                         update_valid_i,
    input logic                         update_taken_i,
    input logic                         update_alloc_i,
    input logic [TAGE_CTR_LEN-1:0]     update_old_ctr_i,
    input logic [VADDR_WIDTH-1:0]        update_req_pc_i,
    input logic [GHIST_LEN-1:0]          update_ghist_i,
    input logic                         update_u_valid_i,
    input logic [TAGE_UBIT_LEN-1:0]    update_u_i,

    input logic                             f1_ready_i,
    input logic                             f2_ready_i,
  
    input logic                         clk,
    input logic                         rst
);

localparam IDX_HIST_CHUNK_LEN = HIST_LEN < $clog2(N_SETS) ? HIST_LEN : $clog2(N_SETS);
localparam TAG_HIST_CHUNK_LEN = HIST_LEN < TAG_LEN ? HIST_LEN : TAG_LEN;

localparam N_IDX_HIST_EXT = IDX_HIST_CHUNK_LEN < $clog2(N_SETS) ? $clog2(N_SETS) / IDX_HIST_CHUNK_LEN + 1 : 1;
localparam N_TAG_HIST_EXT = TAG_HIST_CHUNK_LEN < $clog2(N_SETS) ? $clog2(N_SETS) / TAG_HIST_CHUNK_LEN + 1 : 1;

logic [N_SETS-1:0]                          valid_vec;
logic [N_SETS-1:0][TAG_LEN-1:0]             tag_vec;
logic [N_SETS-1:0][TAGE_CTR_LEN-1:0]        ctr_vec;
logic [N_SETS-1:0][1:0]                     ubit_hi_vec;
logic [N_SETS-1:0][1:0]                     ubit_lo_vec;

logic [1:0][GHIST_LEN-1:0]          req_ghist;
logic [VADDR_WIDTH-1:0]             req_pc_s1;
logic [GHIST_LEN-1:0]               req_ghist_s1;

logic [1:0][N_IDX_CHUNKS-1:0][IDX_HIST_CHUNK_LEN-1:0]   idx_hist_chunks_s1;
logic [1:0][N_TAG_CHUNKS-1:0][TAG_HIST_CHUNK_LEN-1:0]   tag_hist_chunks_s1;
logic [1:0][IDX_HIST_CHUNK_LEN-1:0]                     idx_history_s1;
logic [1:0][TAG_HIST_CHUNK_LEN-1:0]                     tag_history_s1;

logic [N_IDX_CHUNKS-1:0][IDX_HIST_CHUNK_LEN-1:0]        idx_hist_chunks_update;
logic [N_TAG_CHUNKS-1:0][TAG_HIST_CHUNK_LEN-1:0]        tag_hist_chunks_update;
logic [IDX_HIST_CHUNK_LEN-1:0]                          idx_history_update;
logic [TAG_HIST_CHUNK_LEN-1:0]                          tag_history_update;

logic                       hold_rslt_ff;
logic [1:0][$clog2(N_SETS)-1:0]  idx_s1, _idx_s2, idx_s2, idx_s2_ff;
logic [1:0][TAG_LEN-1:0]         tag_s1, _tag_s2, tag_s2, tag_s2_ff;

logic [$clog2(N_SETS)-1:0]  idx_update;
logic [TAG_LEN-1:0]         tag_update;

logic [1:0] tab_valid_s2, _tab_valid_s2, tab_valid_s2_ff;
logic [1:0][TAG_LEN-1:0]  tab_tag_s2, _tab_tag_s2, tab_tag_s2_ff;
logic [1:0][TAGE_CTR_LEN-1:0]  tab_ctr_s2, _tab_ctr_s2, tab_ctr_s2_ff;
logic [1:0][TAGE_UBIT_LEN-1:0]  tab_ubit_s2, _tab_ubit_s2, tab_ubit_s2_ff;
logic [1:0]hit_s1, hit_s2;

logic [TAGE_CTR_LEN-1:0] ctr_update;

// clear u bit
logic [$clog2(TAGE_CLEAR_UBIT_PERIOD)+$clog2(N_SETS):0] clear_ubit_ctr;
logic doing_clear_ubit, doing_clear_ubit_hi, doing_clear_ubit_lo;
logic [$clog2(N_SETS)-1:0] clear_ubit_idx;


//******************stage 0***************************************
always_ff @(posedge clk) begin
    if (f1_ready_i) begin
        req_pc_s1 <= req_pc_i;
    end
end

//******************stage 1***************************************
assign req_ghist_s1 = req_ghist_i;
assign req_ghist[0] = req_ghist_s1;
assign req_ghist[1] = req_ghist_s1 << 1;

generate 
    for (genvar bank = 0; bank < 2; bank++) begin : gen_banks_s1
        always_comb begin
            for (int i = 0; i < N_IDX_CHUNKS - 1; i++) begin
                idx_hist_chunks_s1[bank][i] = req_ghist[bank][i*IDX_HIST_CHUNK_LEN+:IDX_HIST_CHUNK_LEN];
            end
            idx_hist_chunks_s1[bank][N_IDX_CHUNKS - 1] = req_ghist[bank][GHIST_LEN-1:(N_IDX_CHUNKS - 1) * IDX_HIST_CHUNK_LEN];
        end

        always_comb begin
            for (int i=0; i<N_TAG_CHUNKS - 1; i++) begin
                tag_hist_chunks_s1[bank][i] = req_ghist[bank][i*TAG_HIST_CHUNK_LEN+:TAG_HIST_CHUNK_LEN];
            end
            tag_hist_chunks_s1[bank][N_TAG_CHUNKS - 1] = req_ghist[bank][GHIST_LEN-1:(N_TAG_CHUNKS - 1) * TAG_HIST_CHUNK_LEN];
        end

        always_comb begin
            idx_history_s1[bank] = 0;
            for (int i=0; i<N_IDX_CHUNKS; i++) begin
                idx_history_s1[bank] ^= idx_hist_chunks_s1[bank][i];
            end
        end

        always_comb begin
            tag_history_s1[bank] = 0;
            for (int i=0; i<N_TAG_CHUNKS; i++) begin
                tag_history_s1[bank] ^= tag_hist_chunks_s1[bank][i];
            end
        end

        assign idx_s1[bank] = (req_pc_s1 >> 1) ^ {N_IDX_HIST_EXT{idx_history_s1[bank]}};
        assign tag_s1[bank] = ((req_pc_s1 >> 1) >> $clog2(N_SETS)) ^ {N_TAG_HIST_EXT{tag_history_s1[bank]}};
    end
endgenerate


always_ff @(posedge clk) begin
    if (rst) begin
        valid_vec <= 0;
    end
    else begin
        if (update_valid_i) begin
            valid_vec[idx_update] <= 1;
            _tab_valid_s2[0] <= (idx_update == idx_s1[0]) ? 1'b1 : valid_vec[idx_s1[0]];
            _tab_valid_s2[1] <= (idx_update == idx_s1[1]) ? 1'b1 : valid_vec[idx_s1[1]];
        end
        else begin
            _tab_valid_s2[0] <= valid_vec[idx_s1[0]];
            _tab_valid_s2[1] <= valid_vec[idx_s1[1]];
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        ctr_vec <= 0;
    end
    else begin
        if (update_valid_i) begin
            ctr_vec[idx_update] <= ctr_update;
            _tab_ctr_s2[0] <= (idx_update == idx_s1[0]) ? ctr_update : ctr_vec[idx_s1[0]];
            _tab_ctr_s2[1] <= (idx_update == idx_s1[1]) ? ctr_update : ctr_vec[idx_s1[1]];
        end
        else begin
            _tab_ctr_s2[0] <= ctr_vec[idx_s1[0]];
            _tab_ctr_s2[1] <= ctr_vec[idx_s1[1]];
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        ubit_hi_vec <= 0;
    end
    else begin
        if ((update_valid_i | update_u_valid_i) | doing_clear_ubit_hi) begin
            ubit_hi_vec[doing_clear_ubit_hi ? clear_ubit_idx : idx_update] <= doing_clear_ubit_hi ? 1'b0 : update_u_i[1];
            _tab_ubit_s2[0][1] <= (idx_update == idx_s1[0]) ? (doing_clear_ubit_hi ? 1'b0 : update_u_i[1]) : ubit_hi_vec[idx_s1[0]];
            _tab_ubit_s2[1][1] <= (idx_update == idx_s1[1]) ? (doing_clear_ubit_hi ? 1'b0 : update_u_i[1]) : ubit_hi_vec[idx_s1[1]];
        end
        else begin
            _tab_ubit_s2[0][1] <= ubit_hi_vec[idx_s1[0]];
            _tab_ubit_s2[1][1] <= ubit_hi_vec[idx_s1[1]];
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        ubit_lo_vec <= 0;
    end
    else begin
        if ((update_valid_i | update_u_valid_i) | doing_clear_ubit_lo) begin
            ubit_lo_vec[doing_clear_ubit_lo ? clear_ubit_idx : idx_update] <= doing_clear_ubit_lo ? 1'b0 : update_u_i[0];
            _tab_ubit_s2[0][0] <= (idx_update == idx_s1[0]) ? (doing_clear_ubit_lo ? 1'b0 : update_u_i[0]) : ubit_lo_vec[idx_s1[0]];
            _tab_ubit_s2[1][0] <= (idx_update == idx_s1[1]) ? (doing_clear_ubit_lo ? 1'b0 : update_u_i[0]) : ubit_lo_vec[idx_s1[1]];
        end
        else begin
            _tab_ubit_s2[0][0] <= ubit_lo_vec[idx_s1[0]];
            _tab_ubit_s2[1][0] <= ubit_lo_vec[idx_s1[1]];
        end
    end
end

always_ff @(posedge clk) begin
    if (update_valid_i) begin
        tag_vec[idx_update] <= tag_update;
        _tab_tag_s2[0] <= (idx_update == idx_s1[0]) ? tag_update : tag_vec[idx_s1[0]];
        _tab_tag_s2[1] <= (idx_update == idx_s1[1]) ? tag_update : tag_vec[idx_s1[1]];
    end
    else begin
        _tab_tag_s2[0] <= tag_vec[idx_s1[0]];
        _tab_tag_s2[1] <= tag_vec[idx_s1[1]];
    end
end

//******************stage 2***************************************
generate 
    for (genvar bank = 0; bank < 2; bank++) begin : gen_banks_s2
        always_ff @(posedge clk) begin
            if (rst) begin
                _idx_s2[bank] <= 0;
                _tag_s2[bank] <= 0;
            end
            else if (f2_ready_i) begin
                _idx_s2[bank] <= idx_s1[bank];
                _tag_s2[bank] <= tag_s1[bank];
            end
        end

        always_ff @(posedge clk) begin
            if (~hold_rslt_ff) begin
                tab_valid_s2_ff[bank] <= _tab_valid_s2[bank];
                tab_tag_s2_ff[bank] <= _tab_tag_s2[bank];
                tab_ctr_s2_ff[bank] <= _tab_ctr_s2[bank];
                tab_ubit_s2_ff[bank] <= _tab_ubit_s2[bank];
                idx_s2_ff[bank] <= _idx_s2[bank];
                tag_s2_ff[bank] <= _tag_s2[bank];
            end
        end

        assign tab_valid_s2[bank] = hold_rslt_ff ? tab_valid_s2_ff[bank] : _tab_valid_s2[bank];
        assign tab_tag_s2[bank] = hold_rslt_ff ? tab_tag_s2_ff[bank] : _tab_tag_s2[bank];
        assign tab_ctr_s2[bank] = hold_rslt_ff ? tab_ctr_s2_ff[bank] : _tab_ctr_s2[bank];
        assign tab_ubit_s2[bank] = hold_rslt_ff ? tab_ubit_s2_ff[bank] : _tab_ubit_s2[bank];
        assign idx_s2[bank] = hold_rslt_ff ? idx_s2_ff[bank] : _idx_s2[bank];
        assign tag_s2[bank] = hold_rslt_ff ? tag_s2_ff[bank] : _tag_s2[bank];
        assign hit_s2[bank] = tab_valid_s2[bank] & (tab_tag_s2[bank] == tag_s2[bank]);
    end
endgenerate

always_ff @(posedge clk) begin
    if (rst) begin
        hold_rslt_ff <= 0;
    end
    else begin
        hold_rslt_ff <= ~f2_ready_i;
    end
end
//******************output***************************************
generate 
    for (genvar bank = 0; bank < 2; bank++) begin
        assign table_out_o[bank].valid = hit_s2[bank];
        assign table_out_o[bank].u = tab_ubit_s2[bank];
        assign table_out_o[bank].ctr = tab_ctr_s2[bank];
    end
endgenerate

//*****************clear u bit***********************************
always_ff @(posedge clk) begin
    if (rst) begin
        clear_ubit_ctr <= 0;
    end
    else begin
        clear_ubit_ctr <= clear_ubit_ctr + 1'b1;
    end
end

assign doing_clear_ubit = &clear_ubit_ctr[$clog2(TAGE_CLEAR_UBIT_PERIOD)-1:0];
assign doing_clear_ubit_hi = doing_clear_ubit & (clear_ubit_ctr[$clog2(TAGE_CLEAR_UBIT_PERIOD)+$clog2(N_SETS)]);
assign doing_clear_ubit_lo = doing_clear_ubit & (~(clear_ubit_ctr[$clog2(TAGE_CLEAR_UBIT_PERIOD)+$clog2(N_SETS)]));
assign clear_ubit_idx = clear_ubit_ctr >> $clog2(TAGE_CLEAR_UBIT_PERIOD);


//*****************do update****************************************
always_comb begin
    for (int i = 0; i < N_IDX_CHUNKS - 1; i++) begin
        idx_hist_chunks_update[i] = update_ghist_i[i*IDX_HIST_CHUNK_LEN+:IDX_HIST_CHUNK_LEN];
    end
    idx_hist_chunks_update[N_IDX_CHUNKS - 1] = update_ghist_i[GHIST_LEN-1:(N_IDX_CHUNKS - 1) * IDX_HIST_CHUNK_LEN];
end

always_comb begin
    for (int i=0; i<N_TAG_CHUNKS - 1; i++) begin
        tag_hist_chunks_update[i] = update_ghist_i[i*TAG_HIST_CHUNK_LEN+:TAG_HIST_CHUNK_LEN];
    end
    tag_hist_chunks_update[N_TAG_CHUNKS - 1] = update_ghist_i[GHIST_LEN-1:(N_TAG_CHUNKS - 1) * TAG_HIST_CHUNK_LEN];
end

always_comb begin
    idx_history_update = 0;
    for (int i=0; i<N_IDX_CHUNKS; i++) begin
         idx_history_update ^= idx_hist_chunks_update[i];
    end
end

always_comb begin
    tag_history_update = 0;
    for (int i=0; i<N_TAG_CHUNKS; i++) begin
         tag_history_update ^= tag_hist_chunks_update[i];
    end
end

assign idx_update = (update_req_pc_i >> 1) ^ {N_IDX_HIST_EXT{idx_history_update}};
assign tag_update = ((update_req_pc_i >> 1) >> $clog2(N_SETS)) ^ {N_TAG_HIST_EXT{tag_history_update}};

always_comb begin
    if (update_alloc_i) begin
        if (update_taken_i) begin
            ctr_update = {1'b1, {(TAGE_CTR_LEN-1){1'b0}}};
        end
        else begin
            ctr_update = {1'b0, {(TAGE_CTR_LEN-1){1'b1}}};
        end
    end
    else begin
        if (update_taken_i) begin
            ctr_update = (~(&update_old_ctr_i)) ? update_old_ctr_i + 1'b1 : update_old_ctr_i;
        end
        else begin
            ctr_update = (|update_old_ctr_i) ? update_old_ctr_i - 1'b1 : update_old_ctr_i;
        end
    end
end

endmodule
