module ubtb
    import ubtb_typedef::*;
#(
    parameter int unsigned IDX_LSB = $clog2(FETCH_WIDTH / 8),
    parameter int unsigned IDX_WIDTH = 0
)
(
    // input logic                         req_valid_i,
    input logic [VADDR_WIDTH-1:0]                           req_pc_i,
    output logic                                            req_ready_o,

    output ubtb_pred_t                                      pred_o,
    output ubtb_meta_t                                      ubtb_meta_o,

    // update               
    input logic                                             update_valid_i,
    input logic    [PREDS_PER_LINE-1:0]                     update_pred_valid_i,
    input logic                                             update_truncate_i,
    input logic     [VADDR_WIDTH-1:0]                       update_req_pc_i,
    input logic     [VADDR_WIDTH-1:0]                       update_target_i,
    input ubtb_meta_t                                       update_pred_meta_i,
    input logic[PREDS_PER_LINE-1:0]                         update_cfi_taken_vec_i,
    input logic[PREDS_PER_LINE-1:0][$clog2(FETCH_WIDTH/16)-1:0] update_cfi_offset_vec_i,
    input logic    [PREDS_PER_LINE-1:0]                     update_cfi_mask_i,
    input logic    [PREDS_PER_LINE-1:0]                     update_br_mask_i,
    input logic    [PREDS_PER_LINE-1:0]                     update_rvc_mask_i,
    input logic    [PREDS_PER_LINE-1:0]                     update_call_mask_i,
    input logic    [PREDS_PER_LINE-1:0]                     update_ret_mask_i,
    input logic                                             update_is_misalign_i,
    // input logic                             update_is_mispred_i,

    // evict
    output logic                                            evict_valid_o,
    output logic [VADDR_WIDTH-1:0]                          evict_tag_o,
    output logic [VADDR_WIDTH-1:0]                          evict_idx_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_pred_valid_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_target_valid_o,
    output logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]    evict_pred_targets_o,
    output logic [PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0]evict_pred_offsets_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_pred_br_mask_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_pred_cfi_mask_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_pred_rvc_mask_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_pred_call_mask_o,
    output logic [PREDS_PER_LINE-1:0]                       evict_pred_ret_mask_o,
    output logic                                            evict_pred_is_misalign_o,
    output logic                                            evict_pred_is_truncate_o,

    input logic                                             b1_ready_i,

    input logic                                             clk,
    input logic                                             rst
);

function automatic logic [UBTB_TAG_LEN-1:0] ubtb_get_tag (
    input [VADDR_WIDTH-1:0]                pc_i
    );
    begin
    ubtb_get_tag = pc_i[UBTB_TAG_LSB+:UBTB_TAG_LEN];
    end
endfunction

localparam HWORDS_PER_PRED = FETCH_WIDTH/16/PREDS_PER_LINE;

// localparam UBTB_TAG_LEN = VADDR_WIDTH - $clog2(FETCH_WIDTH/8);
logic [UBTB_N_ENTRY-1:0][UBTB_TAG_LEN-1:0]                          tag_cam;
logic [UBTB_N_ENTRY-1:0]                                            tag_valid;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        br_mask_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        cfi_mask_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        rvc_mask_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        call_mask_ram;
logic [UBTB_N_ENTRY-1:0]                                            is_misalign_ram;
logic [UBTB_N_ENTRY-1:0]                                            is_truncate_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        ret_mask_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0] offset_ram;
logic [UBTB_N_ENTRY-1:0][IDX_WIDTH-1:0]                             btb_idx_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        pred_valid_ram;

logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                        target_valid_ram;
logic [UBTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]     target_ram;

logic [UBTB_N_ENTRY-1:0]                                            req_cmp_mask_s1;
logic [UBTB_TAG_LEN-1:0]                                            req_tag_s0, req_tag_s1;
logic [$clog2(UBTB_N_ENTRY)-1:0]                                    req_index_s0, req_index_s1;
logic                                                               req_hit_s0, req_hit_s1;
logic [PREDS_PER_LINE-1:0]                                          pred_valid_s1;
logic [PREDS_PER_LINE-1:0]                                          target_valid_s1;
logic [PREDS_PER_LINE-1:0]                                          br_mask_s1;
logic [PREDS_PER_LINE-1:0]                                          cfi_mask_s1;
logic [PREDS_PER_LINE-1:0]                                          rvc_mask_s1;
logic [PREDS_PER_LINE-1:0]                                          call_mask_s1;
logic                                                               is_misalign_s1;
logic                                                               is_truncate_s1;
logic [PREDS_PER_LINE-1:0]                                          ret_mask_s1;

// logic                                                               update_hit_u0, update_hit_u1;
// logic [$clog2(UBTB_N_ENTRY)-1:0]                                    update_hit_idx_u0, update_hit_idx_u1;
// logic [UBTB_TAG_LEN-1:0]                                            update_tag_u0, update_tag_u1;
// logic [$clog2(UBTB_N_ENTRY)-1:0]                                    update_new_entry_idx_u1;
// logic [PREDS_PER_LINE-1:0]                                          update_cfi_mask_u0, update_cfi_mask_u1;
logic [UBTB_N_ENTRY-1:0]                                            update_cmp_mask;
logic                                                               update_hit;
logic [$clog2(UBTB_N_ENTRY)-1:0]                                    update_hit_idx;
logic [UBTB_TAG_LEN-1:0]                                            update_tag;
logic [$clog2(UBTB_N_ENTRY)-1:0]                                    update_new_entry_idx;

ubtb_pred_t                                                         pred_s1;
ubtb_meta_t                                                         ubtb_meta_s1;

logic                                                               update_fire;

//*****************s0 stage***********************************
// always_comb begin
//     req_tag_s0 = 0;
//     for (int i = 0; i < (VADDR_WIDTH - UBTB_TAG_LSB) / UBTB_TAG_LEN; i++) begin
//         req_tag_s0 ^= req_pc_i[i * UBTB_TAG_LEN + UBTB_TAG_LSB +: UBTB_TAG_LEN];
//     end
// end

// assign req_tag_s0 = req_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)];
assign req_tag_s0 = ubtb_get_tag(req_pc_i);
assign req_ready_o = 1;

always_ff @(posedge clk) begin
    if (b1_ready_i) begin
        req_tag_s1 <= req_tag_s0;
    end
end
//*****************s1 stage***********************************
// search in cam
always_comb begin
    for (int i = 0; i < UBTB_N_ENTRY; i++) begin
        req_cmp_mask_s1[i] = (tag_cam[i] == req_tag_s1) & tag_valid[i];
    end
end

priority_encoder #(
    .SEL_WIDTH(UBTB_N_ENTRY)
) ubtb_req_tag_cmp_priority_encoder (
    .sel_i(req_cmp_mask_s1),
    .id_vld_o(req_hit_s1),
    .id_o(req_index_s1)
);

assign pred_s1.target = target_ram[req_index_s1];
assign pred_s1.offset = offset_ram[req_index_s1];
assign br_mask_s1 = br_mask_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};
assign cfi_mask_s1 = cfi_mask_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};
assign rvc_mask_s1 = rvc_mask_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};
assign call_mask_s1 = call_mask_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};
assign ret_mask_s1 = ret_mask_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};
assign is_misalign_s1 = is_misalign_ram[req_index_s1] & req_hit_s1;
assign is_truncate_s1 = is_truncate_ram[req_index_s1] & req_hit_s1;
assign pred_valid_s1 = pred_valid_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};
assign target_valid_s1 = target_valid_ram[req_index_s1] & {PREDS_PER_LINE{req_hit_s1}};

assign ubtb_meta_s1.entry_no = req_index_s1;
assign ubtb_meta_s1.hit = req_hit_s1;
assign pred_s1.pred_valid = pred_valid_s1;
assign pred_s1.target_valid = target_valid_s1;
assign pred_s1.is_misalign = is_misalign_s1;
assign pred_s1.is_truncate = is_truncate_s1;
assign pred_s1.br_mask = br_mask_s1;
assign pred_s1.cfi_mask = cfi_mask_s1;
assign pred_s1.rvc_mask = rvc_mask_s1;
assign pred_s1.call_mask = call_mask_s1;
assign pred_s1.ret_mask = ret_mask_s1;

assign pred_o = pred_s1;
assign ubtb_meta_o = ubtb_meta_s1;


//*****************update or alloc***********************************
assign update_fire = update_valid_i & (|update_cfi_mask_i);

// search in cam
always_comb begin
    for (int i = 0; i < UBTB_N_ENTRY; i++) begin
        update_cmp_mask[i] = (tag_cam[i] == update_tag) & tag_valid[i];
    end
end

priority_encoder #(
    .SEL_WIDTH(UBTB_N_ENTRY)
) ubtb_update_tag_cmp_priority_encoder (
    .sel_i(update_cmp_mask),
    .id_vld_o(update_hit),
    .id_o(update_hit_idx)
);

assign update_tag = ubtb_get_tag(update_req_pc_i);

always_ff @(posedge clk) begin
    if (rst) begin
        pred_valid_ram <= 0;
    end
    else if (update_fire ) begin
        if (update_hit) begin
            pred_valid_ram[update_hit_idx] <= update_pred_valid_i;
        end
        else begin
            pred_valid_ram[update_new_entry_idx] <= update_pred_valid_i;
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        target_valid_ram <= 0;
    end
    else if (update_fire) begin
        if (update_hit) begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                if (update_pred_valid_i[i] & update_cfi_taken_vec_i[i]) begin
                    target_valid_ram[update_hit_idx][i] <= 1'b1;
                end 
            end
        end
        else begin
            target_valid_ram[update_new_entry_idx] <= 0;
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                if (update_pred_valid_i[i] & update_cfi_taken_vec_i[i]) begin
                    target_valid_ram[update_new_entry_idx][i] <= 1'b1;
                end 
            end
        end
    end
end

always_ff @(posedge clk) begin
    if (update_fire) begin
        if (update_hit) begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                if (update_pred_valid_i[i] & update_cfi_taken_vec_i[i]) begin
                    target_ram[update_hit_idx][i] <= update_target_i[BR_OFFSET_WIDTH:1];
                end
            end
        end
        else begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                if (update_pred_valid_i[i] & update_cfi_taken_vec_i[i]) begin
                    target_ram[update_new_entry_idx][i] <= update_target_i[BR_OFFSET_WIDTH:1];
                end
            end
        end
    end
end

always_ff @(posedge clk) begin
    if (update_fire ) begin
        if (update_hit) begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                offset_ram[update_hit_idx][i] = update_cfi_offset_vec_i[i];
            end
        end
        else begin
            for (int i = 0; i < PREDS_PER_LINE; i++) begin
                offset_ram[update_new_entry_idx][i] = update_cfi_offset_vec_i[i];
            end
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        br_mask_ram <= 0;
        cfi_mask_ram <= 0;
        rvc_mask_ram <= 0;
        call_mask_ram <= 0;
        is_misalign_ram <= 0;
        is_truncate_ram <= 0;
        ret_mask_ram <= 0;
    end
    else if (update_fire ) begin
        if (update_hit) begin
            br_mask_ram[update_hit_idx] <= update_br_mask_i;
            cfi_mask_ram[update_hit_idx] <= update_cfi_mask_i;
            rvc_mask_ram[update_hit_idx] <= update_rvc_mask_i;
            call_mask_ram[update_hit_idx] <= update_call_mask_i;
            is_misalign_ram[update_hit_idx] <= update_is_misalign_i;
            is_truncate_ram[update_hit_idx] <= update_truncate_i;
            ret_mask_ram[update_hit_idx] <= update_ret_mask_i;
        end
        else begin
            br_mask_ram[update_new_entry_idx] <= update_br_mask_i;
            cfi_mask_ram[update_new_entry_idx] <= update_cfi_mask_i;
            rvc_mask_ram[update_new_entry_idx] <= update_rvc_mask_i;
            call_mask_ram[update_new_entry_idx] <= update_call_mask_i;
            is_misalign_ram[update_new_entry_idx] <= update_is_misalign_i;
            is_truncate_ram[update_new_entry_idx] <= update_truncate_i;
            ret_mask_ram[update_new_entry_idx] <= update_ret_mask_i;
        end
    end
end

always_ff @(posedge clk) begin
    if (update_fire ) begin
        if (~update_hit) begin
            btb_idx_ram[update_new_entry_idx] <= update_req_pc_i[IDX_LSB+:IDX_WIDTH];
        end
    end
end



always_ff @(posedge clk) begin
    if (rst) begin
        update_new_entry_idx <= 0;
    end
    else if (update_fire & (~update_hit)) begin
        update_new_entry_idx <= update_new_entry_idx + 1'b1;
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        for (int i=0; i<UBTB_N_ENTRY; i++) begin
            tag_valid[i] <= 0;
            tag_cam[i] <= 0;
        end
    end
    else begin
        if (update_fire) begin
            // alloc
            if (!update_hit) begin
                tag_valid[update_new_entry_idx] <= 1;
                tag_cam[update_new_entry_idx] <= update_tag;
            end
        end
    end
end

// evict
assign evict_valid_o = update_fire & ~update_hit & (tag_valid[update_new_entry_idx]);
assign evict_tag_o = tag_cam[update_new_entry_idx];
assign evict_pred_valid_o = pred_valid_ram[update_new_entry_idx];
assign evict_target_valid_o = target_valid_ram[update_new_entry_idx];
assign evict_pred_targets_o = target_ram[update_new_entry_idx];
assign evict_pred_offsets_o = offset_ram[update_new_entry_idx];
assign evict_pred_br_mask_o = br_mask_ram[update_new_entry_idx];
assign evict_pred_cfi_mask_o = cfi_mask_ram[update_new_entry_idx];
assign evict_pred_rvc_mask_o = rvc_mask_ram[update_new_entry_idx];
assign evict_pred_call_mask_o = call_mask_ram[update_new_entry_idx];
assign evict_pred_is_misalign_o = is_misalign_ram[update_new_entry_idx];
assign evict_pred_is_truncate_o = is_truncate_ram[update_new_entry_idx];
assign evict_pred_ret_mask_o = ret_mask_ram[update_new_entry_idx];
assign evict_idx_o = btb_idx_ram[update_new_entry_idx];
endmodule
