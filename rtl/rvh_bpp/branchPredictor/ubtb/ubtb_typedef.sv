package ubtb_typedef;

    localparam VADDR_WIDTH = 39;
    localparam FETCH_WIDTH = 128;
    localparam BR_OFFSET_WIDTH = 20;

    localparam PREDS_PER_LINE = 2;
    localparam UBTB_N_ENTRY = 16;
    localparam UBTB_N_WAYS = 1;
    localparam UBTB_TAG_LSB = 1;
    localparam UBTB_TAG_LEN = VADDR_WIDTH - UBTB_TAG_LSB;

    localparam PRED_OFFSET_WIDTH = $clog2(FETCH_WIDTH / 16);

    typedef struct packed {
        logic hit;
        logic [$clog2(UBTB_N_ENTRY)-1:0] entry_no;
    } ubtb_meta_t;

    typedef struct packed {
        logic [PREDS_PER_LINE-1:0]                          pred_valid;
        logic [PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0]   offset;
        logic [PREDS_PER_LINE-1:0]                          br_mask;
        logic [PREDS_PER_LINE-1:0]                          cfi_mask;
        logic [PREDS_PER_LINE-1:0]                          call_mask;
        logic [PREDS_PER_LINE-1:0]                          ret_mask;
        logic [PREDS_PER_LINE-1:0]                          rvc_mask;
        logic                                               is_misalign;
        logic                                               is_truncate;

        logic [PREDS_PER_LINE-1:0]                          target_valid;
        logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]       target;
    } ubtb_pred_t;

endpackage : ubtb_typedef
