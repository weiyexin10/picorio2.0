package ibp_typedef;

    localparam FETCH_WIDTH = 128;
    localparam VADDR_WIDTH = 39;

    localparam IBP_N_SZ = 16;

    typedef struct packed {
        logic                           is_hit;
        logic [$clog2(IBP_N_SZ)-1:0]    idx;
    } ibp_meta_t;

    typedef struct packed {
        logic                               is_call;
        logic                               is_ret;
        logic [$clog2(FETCH_WIDTH/16)-1:0]  call_ret_pc_lob;
        logic                               is_rvc_call_ret;
    } ibp_pred_t;



endpackage : ibp_typedef
