module loop
    import loop_typedef::*;
#(
)
(
    input logic                         req_valid_b2_i,
    input logic [VADDR_WIDTH-1:0]       req_pc_b2_i,

    // data for pred
    input logic [PREDS_PER_LINE-1:0]    pred_b2_i,

    output logic [PREDS_PER_LINE-1:0]   pred_o,
    output loop_meta_t                  meta_o,
    
    // redirect
    input logic                         f3_redirect_valid_i,
    input logic                         f3_redirect_taken_i,
    input logic [$clog2(FETCH_WIDTH/8):0] f3_redirect_pc_lob_i,
    input loop_meta_t                   f3_redirect_meta_i,
    input logic                         be_redirect_valid_i,
    input logic                         be_redirect_taken_i,
    input logic [$clog2(FETCH_WIDTH/8):0] be_redirect_pc_lob_i,
    input loop_meta_t                   be_redirect_meta_i,

    // update
    input logic                         update_valid_i,
    input logic     [VADDR_WIDTH-1:0]   update_pc_i,
    input logic    [FETCH_WIDTH/16-1:0] update_br_mask_i,
    input logic                         update_taken_i,
    input loop_meta_t                   update_meta_i,

    input logic                         b3_ready_i,
  
    input logic                         clk,
    input logic                         rst
);

logic [PREDS_PER_LINE-1:0]    update_br_taken_mask;
logic [PREDS_PER_LINE-1:0]    update_valid_mask;
logic [PREDS_PER_LINE-1:0]    redirect_mask_f3;
logic [PREDS_PER_LINE-1:0]    redirect_mask_be;
logic [PREDS_PER_LINE-1:0]    repair_old_mask_f3, repair_old_mask_be;
logic [PREDS_PER_LINE-1:0]    repair_new_mask_f3, repair_new_mask_be;
logic [PREDS_PER_LINE-1:0][$clog2(N_SETS)-1:0]  column_idx;
logic [PREDS_PER_LINE-1:0][TAG_WIDTH-1:0]       column_tag;

/*
       br taken insn
taken_mask: ↓        
-----------------------------------------
| 0  | 0  | 1  | 0  | 0  | 0  | 0  | 0  |
-----------------------------------------

repair_mask:          ↓        
-----------------------------------------
| 0  | 0  | 0  | 0  | 0  | 1  | 1  | 1  |
-----------------------------------------
*/
always_comb begin
    repair_old_mask_f3 = 0;
    repair_new_mask_f3 = 0;
    repair_old_mask_be = 0;
    repair_new_mask_be = 0;

    redirect_mask_f3 = 0;
    redirect_mask_be = 0;

    update_br_taken_mask = 0;
    update_valid_mask = 0;
    for (int i = 0; i < PREDS_PER_LINE; i++) begin
        if (i == update_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
            update_br_taken_mask[i] = update_br_mask_i[i] & update_taken_i;
        end
        if (i <= update_pc_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
            update_valid_mask[i] = update_valid_i;
        end

        if (f3_redirect_valid_i & (i == f3_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1])) begin
            redirect_mask_f3[i] = 1'b1;
        end
        if (be_redirect_valid_i & (i == be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1])) begin
            redirect_mask_be[i] = 1'b1;
        end

        if (i > f3_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
            repair_old_mask_f3[i] = f3_redirect_valid_i & ~be_redirect_valid_i;
        end
        else if (i < f3_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
            repair_new_mask_f3[i] = f3_redirect_valid_i & ~be_redirect_valid_i;
        end

        if (i > be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
            repair_old_mask_be[i] = be_redirect_valid_i;
        end
        else if (i < be_redirect_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:1]) begin
            repair_new_mask_be[i] = be_redirect_valid_i;
        end
    end
end

assign meta_o.tag = column_tag[0];
assign meta_o.idx = column_idx[0];

loop_column loop_column_u0 (
    .req_valid_b2_i(req_valid_b2_i & (req_pc_b2_i[$clog2(FETCH_WIDTH/8)-1:1] == 0)),
    .req_pc_b2_i(req_pc_b2_i),

    // data for pred
    .pred_b2_i(pred_b2_i[0]),

    .final_pred_o(pred_o[0]),

    .meta_o(meta_o.loop_column_metas[0]),
    .idx_o(column_idx[0]),
    .tag_o(column_tag[0]),

    .f3_redirect_valid_i(redirect_mask_f3[0]),
    .f3_redirect_taken_i(f3_redirect_taken_i),
    .f3_redirect_tag_i(f3_redirect_meta_i.tag),
    .f3_redirect_idx_i(f3_redirect_meta_i.idx),
    .f3_redirect_meta_i(f3_redirect_meta_i.loop_column_metas[0]),
    .f3_repair_old_valid_i(repair_old_mask_f3[0]),
    .f3_repair_new_valid_i(repair_new_mask_f3[0]),

    .be_redirect_valid_i(redirect_mask_be[0]),
    .be_redirect_taken_i(be_redirect_taken_i),
    .be_redirect_tag_i(be_redirect_meta_i.tag),
    .be_redirect_idx_i(be_redirect_meta_i.idx),
    .be_redirect_meta_i(be_redirect_meta_i.loop_column_metas[0]),
    .be_repair_old_valid_i(repair_old_mask_be[0]),
    .be_repair_new_valid_i(repair_new_mask_be[0]),
    
    
    // update
    .update_valid_i(update_valid_mask[0] & update_br_mask_i[0]),
    .update_br_taken_i(update_br_taken_mask[0]),
    .update_idx_i(update_meta_i.idx),
    .update_tag_i(update_meta_i.tag),
    .update_meta_i(update_meta_i.loop_column_metas[0]),
`ifdef SIMULATION
    .update_pc_i(update_pc_i),
`endif

    .b3_ready_i(b3_ready_i),

    .clk(clk),
    .rst(rst)
);

generate for (genvar i=1; i<PREDS_PER_LINE; i++) begin : LOOP_BANKS
    loop_column loop_column_u (
        .req_valid_b2_i(req_valid_b2_i & (i >= req_pc_b2_i[$clog2(FETCH_WIDTH/8)-1:1]) & ~(|pred_o[i-1:0])),
        .req_pc_b2_i(req_pc_b2_i),

        // data for pred
        .pred_b2_i(pred_b2_i[i]),

        .final_pred_o(pred_o[i]),

        .meta_o(meta_o.loop_column_metas[i]),
        .idx_o(column_idx[i]),
        .tag_o(column_tag[i]),

        .f3_redirect_valid_i(redirect_mask_f3[i]),
        .f3_redirect_taken_i(f3_redirect_taken_i),
        .f3_redirect_tag_i(f3_redirect_meta_i.tag),
        .f3_redirect_idx_i(f3_redirect_meta_i.idx),
        .f3_redirect_meta_i(f3_redirect_meta_i.loop_column_metas[i]),
        .f3_repair_old_valid_i(repair_old_mask_f3[i]),
        .f3_repair_new_valid_i(repair_new_mask_f3[i]),

        .be_redirect_valid_i(redirect_mask_be[i]),
        .be_redirect_taken_i(be_redirect_taken_i),
        .be_redirect_tag_i(be_redirect_meta_i.tag),
        .be_redirect_idx_i(be_redirect_meta_i.idx),
        .be_redirect_meta_i(be_redirect_meta_i.loop_column_metas[i]),
        .be_repair_old_valid_i(repair_old_mask_be[i]),
        .be_repair_new_valid_i(repair_new_mask_be[i]),
        
        
        // update
        .update_valid_i(update_valid_mask[i] & update_br_mask_i[i]),
        .update_br_taken_i(update_br_taken_mask[i]),
        .update_idx_i(update_meta_i.idx),
        .update_tag_i(update_meta_i.tag),
        .update_meta_i(update_meta_i.loop_column_metas[i]),
`ifdef SIMULATION
        .update_pc_i(update_pc_i),
`endif

        .b3_ready_i(b3_ready_i),
    
        .clk(clk),
        .rst(rst)
    );

end
endgenerate

endmodule
