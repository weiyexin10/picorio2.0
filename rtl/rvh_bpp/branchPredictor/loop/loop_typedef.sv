package loop_typedef;

  localparam VADDR_WIDTH = 39;
  localparam FETCH_WIDTH = 128;
  localparam PREDS_PER_LINE = 8;
  localparam CNT_WIDTH = 10;
  localparam N_SETS = 16;
  localparam TAG_WIDTH = 10;
  localparam CONF_WIDTH = 3;
  localparam AGE_WIDTH = 3;
  localparam N_WAYS = 4;


  typedef struct packed {
    logic     hit;
    logic     valid;
    logic     pred;
    logic     ori_pred;
    logic     final_pred;
    logic [N_SETS-1:0][CNT_WIDTH-1:0]   s_cnt;
    logic [CNT_WIDTH-1:0]               s_cnt_new;
  } loop_column_meta_t;

  typedef struct packed {
    logic [TAG_WIDTH-1:0]                   tag;
    logic [$clog2(N_SETS)-1:0]              idx;
    loop_column_meta_t [PREDS_PER_LINE-1:0] loop_column_metas;
  } loop_meta_t;


  typedef struct packed {
    logic                   valid;
    logic [TAG_WIDTH-1:0]   tag;
    logic [CONF_WIDTH-1:0]  conf;
    logic [AGE_WIDTH-1:0]   age;
    logic [CNT_WIDTH-1:0]   p_cnt;
    logic [CNT_WIDTH-1:0]   c_cnt;
    logic [CNT_WIDTH-1:0]   s_cnt;
    logic                   dir;
  } loop_entry_t;
  
endpackage : loop_typedef
