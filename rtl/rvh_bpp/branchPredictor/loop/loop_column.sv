module loop_column
    import loop_typedef::*;
#(
)
(
    input logic                         req_valid_b2_i,
    input logic [VADDR_WIDTH-1:0]       req_pc_b2_i,

    // data for pred
    input                               pred_b2_i,

    output logic                        final_pred_o,
    output loop_column_meta_t           meta_o,
    output logic [$clog2(N_SETS)-1:0]   idx_o,
    output logic [TAG_WIDTH-1:0]        tag_o,
    
    // redirect
    input logic                         f3_redirect_valid_i,
    input logic                         f3_redirect_taken_i,
    input logic [TAG_WIDTH-1:0]         f3_redirect_tag_i,
    input logic [$clog2(N_SETS)-1:0]    f3_redirect_idx_i,
    input loop_column_meta_t            f3_redirect_meta_i,
    input logic                         f3_repair_old_valid_i,
    input logic                         f3_repair_new_valid_i,

    input logic                         be_redirect_valid_i,
    input logic                         be_redirect_taken_i,
    input logic [TAG_WIDTH-1:0]         be_redirect_tag_i,
    input logic [$clog2(N_SETS)-1:0]    be_redirect_idx_i,
    input loop_column_meta_t            be_redirect_meta_i,
    input logic                         be_repair_old_valid_i,
    input logic                         be_repair_new_valid_i,


    // update
    input logic                         update_valid_i,
    input logic                         update_br_taken_i,
    input logic [TAG_WIDTH-1:0]         update_tag_i,
    input logic [$clog2(N_SETS)-1:0]    update_idx_i,
    input loop_column_meta_t            update_meta_i,
`ifdef SIMULATION
    input logic [VADDR_WIDTH-1:0]       update_pc_i,
`endif

    input logic                         b3_ready_i,
  
    input logic                         clk,
    input logic                         rst
);

loop_entry_t [N_WAYS-1:0][N_SETS-1:0]   entries;
loop_entry_t                entry_b2, f3_red_entry, be_red_entry, update_entry, update_w_entry;
logic [$clog2(N_WAYS)-1:0]  way_b2, way_red_f3, way_red_be, way_update, update_w_way, age_zero_way;
logic                       hit_b2, hit_red_f3, hit_red_be, hit_update;
logic [$clog2(N_SETS)-1:0]  idx_b2, idx_b3, update_idx;
logic [CNT_WIDTH-1:0]       s_cnt_b2, s_cnt_b3, s_cnt_recover;
logic [TAG_WIDTH-1:0]       tag_b2, tag_b3, update_tag;
logic                       update_en, alloc_en;
logic                       recover_en;
logic                       age_zero_exist;

logic                       f3_red_mispred;
logic                       be_red_mispred;


assign update_idx = update_idx_i;
assign update_tag = update_tag_i;

assign idx_b2 = req_pc_b2_i[$clog2(FETCH_WIDTH/8)+:$clog2(N_SETS)];
assign tag_b2 = req_pc_b2_i[$clog2(FETCH_WIDTH/8)+$clog2(N_SETS)+:TAG_WIDTH];

always_comb begin
    way_b2 = 0;
    hit_b2 = 0;
    for (int i = 0; i < N_WAYS; i++) begin
        if (entries[i][idx_b2].tag == tag_b2) begin
            hit_b2 = 1;
            way_b2 = i;
        end
    end
end

assign entry_b2 = entries[way_b2][idx_b2];

always_comb begin
    if (recover_en & (((be_redirect_idx_i == idx_b2) & hit_red_be) | ((f3_redirect_idx_i == idx_b2) & hit_red_f3))) begin
        s_cnt_b2 = s_cnt_recover;
    end
    else if (update_en & (update_idx == idx_b2)) begin
        s_cnt_b2 = update_w_entry.s_cnt;
    end
    else if (alloc_en & (update_idx == idx_b2)) begin
        s_cnt_b2 = 0;
    end
    else begin
        s_cnt_b2 = entry_b2.s_cnt;
    end
end

// output 
always_comb begin
    for (int i = 0; i < N_SETS; i++) begin
        meta_o.s_cnt[i] = entries[way_b2][i].s_cnt;
    end
end

always_comb begin
    meta_o.s_cnt_new = s_cnt_b2;
    if (req_valid_b2_i & meta_o.hit) begin
        meta_o.s_cnt_new = s_cnt_b2 + 1'b1;
        if (s_cnt_b2 + 1'b1 >= entry_b2.p_cnt) begin
            meta_o.s_cnt_new = 0;
        end
    end
end

assign meta_o.hit = hit_b2;
assign meta_o.valid = meta_o.hit & (&entry_b2.conf);
assign meta_o.ori_pred = pred_b2_i;
assign meta_o.final_pred = final_pred_o;
assign meta_o.pred = (s_cnt_b2 + 1'b1 == entry_b2.p_cnt) ? ~entry_b2.dir : entry_b2.dir;

always_comb begin
    final_pred_o = pred_b2_i;
    if (meta_o.hit & (entry_b2.conf != 0)) begin
        if (s_cnt_b2 + 1'b1 == entry_b2.p_cnt) begin
            final_pred_o = ~entry_b2.dir;
        end
        else begin
            final_pred_o = entry_b2.dir;
        end  
    end
end
assign idx_o = idx_b2;
assign tag_o = tag_b2;

// update during commit
always_comb begin
    way_update = 0;
    hit_update = 0;
    for (int i = 0; i < N_WAYS; i++) begin
        if (entries[i][update_idx].tag == update_tag) begin
            hit_update = 1;
            way_update = i;
        end
    end
end

always_comb begin
    age_zero_way = 0;
    age_zero_exist = 0;
    for (int i = 0; i < N_WAYS; i++) begin
        if (entries[i][update_idx].age == 0) begin
            age_zero_exist = 1;
            age_zero_way = i;
        end
    end
end

assign update_entry = entries[way_update][update_idx];

always_comb begin
    update_w_entry = update_entry;
    update_en = 0;
    alloc_en = 0;
    if (update_valid_i) begin
        if (update_meta_i.hit) begin
            if (hit_update) begin
                if (update_meta_i.valid & (update_br_taken_i ^ update_meta_i.pred)) begin // pred valid & mispred --> free the entry
                    update_w_entry.conf = 0;
                    update_w_entry.age = 0;
                    // update_w_entry.s_cnt = 0;
                    update_w_entry.c_cnt = 0;
                    update_en = 1;
                end
                else begin
                    update_w_entry.c_cnt = update_entry.c_cnt + 1'b1;
                    update_en = 1;

                    if (update_meta_i.valid & (update_br_taken_i == update_meta_i.pred) & (update_meta_i.pred ^ update_meta_i.ori_pred)) begin // loop correct & tage mispred --> age++
                        update_w_entry.age = &update_entry.age ? update_entry.age : update_entry.age + 1'b1;
                    end                    

                    if ((update_entry.p_cnt != 0) & (update_entry.c_cnt + 1'b1 > update_entry.p_cnt)) begin   // treat like the 1st encounter of the loop
                        update_w_entry.p_cnt = 0;
                        update_w_entry.conf = 0;
                    end
                    
                    if (update_br_taken_i ^ update_entry.dir) begin
                        if ((update_entry.p_cnt != 0) & (update_entry.c_cnt + 1'b1 == update_entry.p_cnt)) begin
                            update_w_entry.conf = &update_entry.conf ? update_entry.conf : update_entry.conf + 1'b1;
                            if (update_entry.p_cnt < 5) begin // just do not predict when the loop count is 1 or 2, free the entry
                                update_w_entry.dir = update_br_taken_i;
                                update_w_entry.p_cnt = 0;
                                update_w_entry.age = 0;
                                update_w_entry.c_cnt = 0;
                                // update_w_entry.s_cnt = 0;
                            end
                        end
                        else begin
                            if (update_entry.p_cnt == 0) begin     // first complete loop or first encount the loop
                                update_w_entry.conf = 0;
                                update_w_entry.p_cnt = update_entry.c_cnt + 1'b1;
                                // update_w_entry.s_cnt = 0;
                            end
                            else begin  // not the same number of iterations as last time: free the entry
                                update_w_entry.p_cnt = 0;
                                update_w_entry.conf = 0;
                            end
                        end
                        update_w_entry.c_cnt = 0;
                        
                    end
                    if (update_br_taken_i ^ update_meta_i.final_pred) begin // finally_mispredicted
                        // update_w_entry.s_cnt = update_w_entry.c_cnt;
                    end
                end
            end
        end
        else if (update_br_taken_i ^ update_meta_i.final_pred) begin
            update_w_entry.tag = update_tag;
            update_w_entry.conf = 0;
            update_w_entry.age = {AGE_WIDTH{1'b1}};
            // update_w_entry.s_cnt = 0;
            update_w_entry.c_cnt = 0;
            update_w_entry.p_cnt = 0;
            update_w_entry.dir = ~update_br_taken_i;
            alloc_en = 1;
        end
    end
end

assign f3_red_entry = entries[way_red_f3][f3_redirect_idx_i];
assign be_red_entry = entries[way_red_be][be_redirect_idx_i];


always_comb begin
    way_red_f3 = 0;
    hit_red_f3 = 0;
    for (int i = 0; i < N_WAYS; i++) begin
        if (entries[i][f3_redirect_idx_i].tag == f3_redirect_tag_i) begin
            hit_red_f3 = 1;
            way_red_f3 = i;
        end
    end
end

always_comb begin
    way_red_be = 0;
    hit_red_be = 0;
    for (int i = 0; i < N_WAYS; i++) begin
        if (entries[i][be_redirect_idx_i].tag == be_redirect_tag_i) begin
            hit_red_be = 1;
            way_red_be = i;
        end
    end
end

assign f3_red_mispred = f3_redirect_valid_i & (f3_redirect_taken_i ^ f3_redirect_meta_i.pred);
assign be_red_mispred = be_redirect_valid_i & (be_redirect_taken_i ^ be_redirect_meta_i.pred);

assign recover_en = be_red_mispred | f3_red_mispred | be_repair_old_valid_i | be_repair_new_valid_i | f3_repair_old_valid_i | f3_repair_new_valid_i;
always_comb begin
    s_cnt_recover = 0;
    if (be_red_mispred) begin
        s_cnt_recover = 0;
    end
    else if (f3_red_mispred) begin
        s_cnt_recover = 0;
    end
    // recover
    else if (be_repair_old_valid_i | be_repair_new_valid_i) begin
        s_cnt_recover = be_redirect_meta_i[be_redirect_idx_i];
        if (be_repair_new_valid_i) begin
            s_cnt_recover = be_redirect_meta_i.s_cnt_new;
        end
    end
    else if (f3_repair_old_valid_i | f3_repair_new_valid_i) begin
        s_cnt_recover = f3_redirect_meta_i.s_cnt[f3_redirect_idx_i];
        if (f3_repair_new_valid_i) begin
            s_cnt_recover = f3_redirect_meta_i.s_cnt_new;
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        entries <= 0;
    end
    else begin
        
         // update & alloc
        if (update_en) begin
            entries[way_update][update_idx] <= update_w_entry;
        end
        else if (alloc_en) begin
            if (age_zero_exist) begin
                entries[age_zero_way][update_idx] <= update_w_entry;
            end
            else begin
`ifdef SIMULATION
                $display("[%d] loop full @pc: %08x", $time(), update_pc_i);
`endif
                for (int i = 0; i < N_WAYS; i++) begin
                    entries[i][update_idx].age = |entries[i][update_idx].age ? entries[i][update_idx].age - 1'b1 : 0;
                end
            end
        end

        // mispred
        if (be_red_mispred & hit_red_be) begin
            for (int i = 0; i < N_SETS; i++) begin
                entries[way_red_be][i].s_cnt <= be_redirect_meta_i.s_cnt[i];
            end
            entries[way_red_be][be_redirect_idx_i].s_cnt <= 0;
        end
        else if (f3_red_mispred & hit_red_f3) begin
            for (int i = 0; i < N_SETS; i++) begin
                entries[way_red_f3][i].s_cnt <= f3_redirect_meta_i.s_cnt[i];
            end
            entries[way_red_f3][f3_redirect_idx_i].s_cnt <= 0;
        end
        // recover
        else if ((be_repair_old_valid_i | be_repair_new_valid_i) & hit_red_be) begin
            for (int i = 0; i < N_SETS; i++) begin
                entries[way_red_be][i].s_cnt <= be_redirect_meta_i.s_cnt[i];
            end
            if (be_repair_new_valid_i) begin
                entries[way_red_be][be_redirect_idx_i].s_cnt <= be_redirect_meta_i.s_cnt_new;
            end
        end
        else if ((f3_repair_old_valid_i | f3_repair_new_valid_i) & hit_red_f3) begin
            for (int i = 0; i < N_SETS; i++) begin
                entries[way_red_f3][i].s_cnt <= f3_redirect_meta_i.s_cnt[i];
            end
            if (f3_repair_new_valid_i) begin
                entries[way_red_f3][f3_redirect_idx_i].s_cnt <= f3_redirect_meta_i.s_cnt_new;
            end
        end
       
        // speculative update
        if (req_valid_b2_i & b3_ready_i) begin 
            if (meta_o.hit & (entry_b2.p_cnt != 0)) begin
                entries[way_b2][idx_b2].s_cnt <= s_cnt_b2 + 1'b1;
                if (s_cnt_b2 + 1'b1 >= entry_b2.p_cnt) begin
                    entries[way_b2][idx_b2].s_cnt <= 0;
                end
            end
        end
    end
end

// `ifdef SIMULATION
// always_ff @(posedge clk) begin
//     if (be_redirect_valid_i) begin
//         if (be_redirect_meta_i.pred ^ be_redirect_meta_i.ori_pred) begin
//             if (be_redirect_meta_i.pred == be_redirect_taken_i) begin
//                 $display("[%d] loop diff at pc: %08x, actually: %s correct: LOOP", $time(), update_pc_i, be_redirect_taken_i ? " T" : "NT");
//             end
//             else begin
//                 $display("[%d] loop diff at pc: %08x, actually: %s correct: TAGE", $time(), update_pc_i, be_redirect_taken_i ? " T" : "NT");
//             end
//         end
//     end
// end
// `endif
endmodule
