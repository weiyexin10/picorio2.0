package ubtb_typedef;

  localparam VADDR_WIDTH = 39;
  localparam FETCH_WIDTH = 128;

  


  function [BTB_TAG_LEN-1:0] btb_get_tag (
      input [VADDR_WIDTH-1:0]                pc_i
      );
      begin
        btb_get_tag = 0;
        for (int i = 0; i < (VADDR_WIDTH - BTB_TAG_LSB) / BTB_TAG_LEN; i++)
            btb_get_tag ^= pc_i[(BTB_TAG_LSB + (i*BTB_TAG_LEN))+:BTB_TAG_LEN];
      end
  endfunction

  typedef struct packed {
    logic hit;
    logic [$clog2(UBTB_N_ENTRY)-1:0] entry_no;
  } ubtb_meta_t;
  typedef struct packed {
    logic [PREDS_PER_LINE-1:0] valid;
    logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0] target;
    logic [PREDS_PER_LINE-1:0] br_mask;
    logic [PREDS_PER_LINE-1:0] cfi_mask;
  } ubtb_pred_t;

endpackage : ubtb_typedef