module ras
    import ras_typedef::*;
#(

) (
    // b1 stage
    input logic [VADDR_WIDTH-1:0]               pc_b1_i,
    input ras_pred_t                            ras_pred_b1_i,
    output logic [VADDR_WIDTH-1:0]              ret_target_b1_o,

    output ras_meta_t                           ras_meta_b1_o,

    // b2 stage     
    // fix b1's prediction in ras       
    input logic                                 fix_ras_valid_b2_i,
    input logic [VADDR_WIDTH-1:0]               pc_b2_i,
    input ras_pred_t                            ras_pred_b2_i,
    input ras_meta_t                            ras_meta_b2_i,

    // f3 stage
    // read ret's target from ras according to meta data
    input ras_meta_t[1:0]                       ras_meta_banks_f3_i,
    output logic [1:0][VADDR_WIDTH-1:0]         ret_target_banks_f3_o,
    // fix former prediction in ras     
    input logic                                 fix_ras_valid_f3_i,
    input logic                                 fix_ptr_f3_i,
    input logic[VADDR_WIDTH-1:0]                pc_f3_i,
    input ras_pred_t                            ras_pred_f3_i,

    //fix from be
    input logic                                 redirect_valid_i,
    input logic [$clog2(FETCH_WIDTH/8):0]       redirect_insn_pc_lob_i,
    input ras_pred_t                            redirect_ras_pred_i,
    input ras_meta_t                            redirect_ras_meta_i,

    input logic                                 req_ready_i,

    input logic clk,
    input logic rst
);

    

    logic [$clog2(RAS_N_SZ)-1:0]                        top;
    logic [        RAS_N_SZ-1:0][VADDR_WIDTH-1:0]       ras_stack;
    ras_meta_t                                          ras_meta_b1;
    ras_meta_t                                          ras_meta_f3;
    logic                                               recover_from_redirect;
    logic                                               use_pred_meta;
    ras_meta_t                                          meta_before_redirect;
    logic [$clog2(FETCH_WIDTH/8)-1:0]                   redirect_insn_pc_lob;

    assign ras_meta_f3 = ras_meta_banks_f3_i[fix_ptr_f3_i];
    logic [VADDR_WIDTH-1:0]  ret_addr_f3;
    logic [VADDR_WIDTH-1:0]  ret_addr_b2;
    logic [VADDR_WIDTH-1:0]  ret_addr_b1;
    assign ret_addr_f3 = {pc_f3_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], ras_pred_f3_i.call_ret_pc_lob, 1'b0} + (ras_pred_f3_i.is_rvc_call_ret ? 3'd2 : 3'd4);
    assign ret_addr_b2 = {pc_b2_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], ras_pred_b2_i.call_ret_pc_lob, 1'b0} + (ras_pred_b2_i.is_rvc_call_ret ? 3'd2 : 3'd4);
    assign ret_addr_b1 = {pc_b1_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)], ras_pred_b1_i.call_ret_pc_lob, 1'b0} + (ras_pred_b1_i.is_rvc_call_ret ? 3'd2 : 3'd4);

    assign redirect_insn_pc_lob = redirect_insn_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:0];

    always_ff @(posedge clk) begin
        if (rst) begin
            top <= 0;
        end
        else begin
            if (redirect_valid_i) begin
                if ((redirect_insn_pc_lob >> 1) < redirect_ras_pred_i.call_ret_pc_lob) begin
                    top <= redirect_ras_meta_i.top;
                end
                else begin
                    if (redirect_ras_pred_i.is_call) begin
                        top <= redirect_ras_meta_i.top + 1'b1;
                    end
                    else if (redirect_ras_pred_i.is_ret) begin
                        top <= redirect_ras_meta_i.top - 1'b1;
                    end
                    else begin
                        top <= redirect_ras_meta_i.top;
                    end
                end
            end
            else if (fix_ras_valid_f3_i) begin
                if (ras_pred_f3_i.is_call) begin
                    top <= ras_meta_f3.top + 1'b1;
                end
                else if (ras_pred_f3_i.is_ret) begin
                    top <= ras_meta_f3.top - 1'b1;
                end
                else begin
                    top <= ras_meta_f3.top;
                end
            end
            else if (fix_ras_valid_b2_i) begin
                if (ras_pred_b2_i.is_call) begin
                    top <= ras_meta_b2_i.top + 1'b1;
                end
                else if (ras_pred_b2_i.is_ret) begin
                    top <= ras_meta_b2_i.top - 1'b1;
                end
                else begin
                    top <= ras_meta_b2_i.top;
                end
            end
            else if (req_ready_i) begin
                if (ras_pred_b1_i.is_call) begin
                    top <= top + 1'b1;
                end
                else if (ras_pred_b1_i.is_ret) begin
                    top <= top - 1'b1;
                end
            end
        end
    end

    always_ff @(posedge clk) begin
        if (redirect_valid_i) begin
            meta_before_redirect <= redirect_ras_meta_i;
        end
        else if (fix_ras_valid_f3_i) begin
            meta_before_redirect <= ras_meta_f3;
        end
        else if (fix_ras_valid_b2_i) begin
            meta_before_redirect <= ras_meta_b2_i;
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            recover_from_redirect <= 1'b0;
            use_pred_meta <= 1'b0;
        end
        else begin
            if (recover_from_redirect) begin
                if (req_ready_i) begin
                    recover_from_redirect <= 1'b0;
                    if (redirect_valid_i) begin
                        recover_from_redirect <= 1'b1;
                        use_pred_meta <= redirect_ras_pred_i.is_call | redirect_ras_pred_i.is_ret;
                    end
                    else if (fix_ras_valid_f3_i) begin
                        recover_from_redirect <= 1'b1;
                        use_pred_meta <= ras_pred_f3_i.is_call | ras_pred_f3_i.is_ret;
                    end
                    else if (fix_ras_valid_b2_i) begin
                        recover_from_redirect <= 1'b1;
                        use_pred_meta <= ras_pred_b2_i.is_call | ras_pred_b2_i.is_ret;
                    end
                end
            end
            else begin
                if (redirect_valid_i) begin
                    recover_from_redirect <= 1'b1;
                    use_pred_meta <= redirect_ras_pred_i.is_call | redirect_ras_pred_i.is_ret;
                end
                else if (fix_ras_valid_f3_i) begin
                    recover_from_redirect <= 1'b1;
                    use_pred_meta <= ras_pred_f3_i.is_call | ras_pred_f3_i.is_ret;
                end
                else if (fix_ras_valid_b2_i) begin
                    recover_from_redirect <= 1'b1;
                    use_pred_meta <= ras_pred_b2_i.is_call | ras_pred_b2_i.is_ret;
                end
            end
        end
    end

    always_ff @(posedge clk) begin
        if (rst) begin
            for (int i = 0; i < RAS_N_SZ; i++) begin
                ras_stack[i] <= 39'h80000000;
            end
        end
        else if (redirect_valid_i) begin
            ras_stack[redirect_ras_meta_i.top - 1'b1] <= redirect_ras_meta_i.top_member;
        end
        else if (fix_ras_valid_f3_i) begin
            ras_stack[ras_meta_f3.top - 1'b1] <= ras_meta_f3.top_member;
            if (ras_pred_f3_i.is_call) begin
                ras_stack[ras_meta_f3.top] <= ret_addr_f3;
            end
        end
        else if (fix_ras_valid_b2_i) begin
            ras_stack[ras_meta_b2_i.top - 1'b1] <= ras_meta_b2_i.top_member;
            if (ras_pred_b2_i.is_call) begin
                ras_stack[ras_meta_b2_i.top] <= ret_addr_b2;
            end
        end
        else if (req_ready_i & ras_pred_b1_i.is_call) begin
            ras_stack[top] <= ret_addr_b1;
        end
    end

    assign ras_meta_b1.top = top;
    assign ras_meta_b1.top_member = ras_stack[top - 1'b1];
    assign ret_target_b1_o = (recover_from_redirect & ~use_pred_meta) ? ras_stack[meta_before_redirect.top - 1'b1] : ras_stack[top - 1'b1];
    assign ras_meta_b1_o = (recover_from_redirect & ~use_pred_meta) ? meta_before_redirect : ras_meta_b1;
    assign ret_target_banks_f3_o[0] = ras_stack[ras_meta_banks_f3_i[0].top - 1'b1];
    assign ret_target_banks_f3_o[1] = ras_stack[ras_meta_banks_f3_i[1].top - 1'b1];

`ifndef SYNTHESIS
    always_ff @(posedge clk) begin
        if (ras_pred_f3_i.is_call & ras_pred_f3_i.is_ret) begin
            $display("there are instruction that is call & return");
            $finish();
        end
    end
`endif


endmodule



// module ras
//   import ras_typedef::*;
// #(

// ) (
//     input  logic                              read_valid_i,
//     input logic [$clog2(FETCH_WIDTH/8):0]     ret_insn_pc_lob_i,
//     output logic      [VADDR_WIDTH-1:0]       read_target_o,
//     output ras_meta_t                         ras_meta_o,

//     input logic                               write_valid_i,
//     input logic [$clog2(FETCH_WIDTH/8):0]     call_insn_pc_lob_i,
//     input logic [VADDR_WIDTH-1:0]             write_target_i,

//     input logic                               cancel_i,

//     //fix
//     input logic                               redirect_valid_i,
//     input logic [$clog2(FETCH_WIDTH/8):0]     redirect_insn_pc_lob_i,
//     input ras_meta_t                          redirect_ras_meta_i,

//     input logic clk,
//     input logic rst
// );

//   logic [$clog2(RAS_N_SZ)-1:0]                        top, next;
//   logic [        RAS_N_SZ-1:0][$clog2(RAS_N_SZ)-1:0]  front_stack;
//   logic [        RAS_N_SZ-1:0][VADDR_WIDTH-1:0]       ras_stack;

//   always_ff @(posedge clk) begin
//     if (rst) begin
//       top <= 0;
//       next <= 0;
//     end
//     else begin
// `ifndef SYNTHESIS
//       assert((write_valid_i & read_valid_i) != 1);
// `endif
//       if (redirect_valid_i) begin
//         if (redirect_insn_pc_lob_i[$clog2(FETCH_WIDTH/8)-1:0] < redirect_ras_meta_i.pc_lob[$clog2(FETCH_WIDTH/8)-1:0]) begin
//           top <= redirect_ras_meta_i.top;
//           next <= redirect_ras_meta_i.next;
//         end
//         else begin
//           if (redirect_ras_meta_i.is_call) begin
//             next <= redirect_ras_meta_i.next + 1'b1;
//             top <= redirect_ras_meta_i.next;
//           end
//           else if (redirect_ras_meta_i.is_ret) begin
//             next <= redirect_ras_meta_i.next;
//             top <= redirect_ras_meta_i.new_top;
//           end
//           else begin
//             top <= redirect_ras_meta_i.top;
//             next <= redirect_ras_meta_i.next;
//           end
//         end
//       end
//       else if (write_valid_i & ~cancel_i) begin
//         top <= next;
//         next <= next + 1'b1;
//       end
//       else if (read_valid_i & ~cancel_i) begin
//         top <= front_stack[top];
//       end
//     end
//   end

//   always_ff @(posedge clk) begin
//     if (rst) begin
//       front_stack <= 0;
//       for (int i = 0; i < RAS_N_SZ; i++) begin
//         ras_stack[i] <= 39'h80000000;
//       end
//     end
//     else if (write_valid_i & ~cancel_i & ~redirect_valid_i) begin
//       ras_stack[next] <= write_target_i;
//       front_stack[next] <= top;
//     end
//   end

//   always_comb begin
//     read_target_o = ras_stack[top];
//     ras_meta_o.next = next;
//     ras_meta_o.top = top;
//     ras_meta_o.is_call = write_valid_i & ~cancel_i;
//     ras_meta_o.is_ret = read_valid_i & ~cancel_i;
//     ras_meta_o.new_top = front_stack[top];
//     ras_meta_o.pc_lob = write_valid_i ? call_insn_pc_lob_i : ret_insn_pc_lob_i;
//   end

// endmodule

