package ras_typedef;

    localparam FETCH_WIDTH = 128;
    localparam VADDR_WIDTH = 39;

    localparam RAS_N_SZ = 16;

    typedef struct packed {
        logic [$clog2(RAS_N_SZ)-1:0]    top;
        logic [VADDR_WIDTH-1:0]         top_member;
    } ras_meta_t;

    typedef struct packed {
        logic                               is_call;
        logic                               is_ret;
        logic [$clog2(FETCH_WIDTH/16)-1:0]  call_ret_pc_lob;
        logic                               is_rvc_call_ret;
    } ras_pred_t;

    function automatic logic ras_pred_incompatible (input ras_pred_t curr_pred,  input ras_pred_t last_stage_pred);
    begin
            ras_pred_incompatible = 1'b1;
            if (curr_pred.is_call & last_stage_pred.is_call & 
                    (curr_pred.call_ret_pc_lob == last_stage_pred.call_ret_pc_lob) &
                    (curr_pred.is_rvc_call_ret == last_stage_pred.is_rvc_call_ret)) begin
                ras_pred_incompatible = 1'b0;
            end
            else if (curr_pred.is_ret & last_stage_pred.is_ret) begin
                ras_pred_incompatible = 1'b0;
            end
            else if (~(curr_pred.is_ret | last_stage_pred.is_ret | curr_pred.is_call | last_stage_pred.is_call)) begin
                ras_pred_incompatible = 1'b0;
            end
        end
    endfunction


endpackage : ras_typedef
