package btb_typedef;

    localparam VADDR_WIDTH        = 39;
    localparam FETCH_WIDTH      = 128;
    localparam BR_OFFSET_WIDTH = 20;

    localparam PREDS_PER_LINE    = 2;
    localparam BTB_N_ENTRY      = 128;
    localparam BTB_N_WAYS       = 4;


    localparam BTB_TAG_LSB = 1;
    localparam BTB_TAG_LEN = VADDR_WIDTH - BTB_TAG_LSB;
    localparam PRED_OFFSET_WIDTH = $clog2(FETCH_WIDTH / 16);

    // function [BTB_TAG_LEN-1:0] btb_get_tag (
    //   input [VADDR_WIDTH-1:0]                pc_i
    //   );
    //   begin
    //     btb_get_tag = 0;
    //     for (int i = 0; i < (VADDR_WIDTH - BTB_TAG_LSB) / BTB_TAG_LEN; i++)
    //         btb_get_tag ^= pc_i[(BTB_TAG_LSB + (i*BTB_TAG_LEN))+:BTB_TAG_LEN];
    //   end
    // endfunction

    typedef struct packed {
        logic hit;
        logic [$clog2(BTB_N_ENTRY)-1:0] idx;
        logic [$clog2(BTB_N_WAYS)-1:0] which_way;
`ifdef SIMULATION
        logic [VADDR_WIDTH-1:0]         base_pc;
`endif
    } btb_meta_t;

    typedef struct packed {
        logic [PREDS_PER_LINE-1:0] pred_valid;
        logic [PREDS_PER_LINE-1:0] br_mask;
        logic [PREDS_PER_LINE-1:0] cfi_mask;
        logic [PREDS_PER_LINE-1:0] rvc_mask;
        logic [PREDS_PER_LINE-1:0] call_mask;
        logic [PREDS_PER_LINE-1:0] ret_mask;
        logic [PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0] offset;
        logic                       is_misalign;
        logic                       is_truncate;

        logic [PREDS_PER_LINE-1:0]                      target_valid;
        logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]   target;
    } btb_pred_t;

endpackage : btb_typedef
