module btb
    import btb_typedef::*;
#(

)
(
    input logic [VADDR_WIDTH-1:0]                           req_pc_i,
    input logic     [63:0]                                  req_ghist_i,
    output logic                                            req_ready_o,

    // pred from ubtb
    input logic                                             ubtb_valid_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_pred_valid_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_target_valid_i,
    input logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]     ubtb_pred_targets_i,
    input logic [PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0] ubtb_pred_offsets_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_pred_br_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_pred_cfi_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_pred_rvc_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_pred_call_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_pred_ret_mask_i,
    input logic                                             ubtb_pred_is_misalign_i,
    input logic                                             ubtb_pred_is_truncate_i,

    output btb_pred_t                                       pred_o,
    output btb_meta_t                                       btb_meta_o,
    // evict from ubtb
    input logic                                             ubtb_evict_i,
    input logic [VADDR_WIDTH-1:0]                           ubtb_evict_tag_i,
    input logic                                             ubtb_evict_is_misalign_i,
    input logic                                             ubtb_evict_is_truncate_i,
    input logic [VADDR_WIDTH-1:0]                           ubtb_evict_idx_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_pred_valid_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_target_valid_i,
    input logic [PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]     ubtb_evict_targets_i,
    input logic [PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0] ubtb_evict_offsets_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_br_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_cfi_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_rvc_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_call_mask_i,
    input logic [PREDS_PER_LINE-1:0]                        ubtb_evict_ret_mask_i,

    input logic                                             f1_ready_i,
    input logic                                             f2_ready_i,

    input logic                                             clk,
    input logic                                             rst
);

localparam HWORDS_PER_PRED = FETCH_WIDTH/16/PREDS_PER_LINE;

function automatic logic  [BTB_TAG_LEN-1:0] btb_get_tag (
    input [VADDR_WIDTH-1:0]                pc_i
    );
    begin
    btb_get_tag = pc_i[BTB_TAG_LSB+:BTB_TAG_LEN];
    end
endfunction

logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         pred_valid_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         br_mask_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         cfi_mask_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         rvc_mask_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         call_mask_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         ret_mask_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][BTB_TAG_LEN-1:0]                            tag_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0]                         target_valid_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]      target_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0][PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0]  offset_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0]                                             is_misalign_ram;
logic [BTB_N_WAYS-1:0][BTB_N_ENTRY-1:0]                                             is_truncate_ram;

logic [BTB_TAG_LEN-1:0]                                                             req_tag_s0, req_tag_s1;
logic [$clog2(BTB_N_ENTRY)-1:0]                                                     req_index_s0, req_index_s1, write_idx;
logic                                                                               req_hit_s1, evict_hit;
logic [$clog2(BTB_N_WAYS)-1:0]                                                      which_way_s1, evict_hit_way;

logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          pred_valids;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          target_valids;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0][BR_OFFSET_WIDTH:0]                       targets;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0][PRED_OFFSET_WIDTH-1:0]                   offsets;
logic [BTB_N_WAYS-1:0][BTB_TAG_LEN-1:0]                                             tags, evict_ori_tags;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          br_masks;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          cfi_masks;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          rvc_masks;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          call_masks;
logic [BTB_N_WAYS-1:0][PREDS_PER_LINE-1:0]                                          ret_masks;
logic [BTB_N_WAYS-1:0]                                                              is_misaligns;
logic [BTB_N_WAYS-1:0]                                                              is_truncates;


logic                               hold_rslt_ff;
btb_pred_t                          pred, pred_s1, pred_ff, pred_s2;
btb_meta_t                          btb_meta, btb_meta_s1, btb_meta_ff, btb_meta_s2;

logic [$clog2(BTB_N_WAYS)-1:0]      alloc_which_way, write_which_way;
logic                               ram_we;

// generate
//     for (genvar i=0; i<BTB_N_WAYS; i++) begin : BTB_SET
//         for (genvar j=0; j<PREDS_PER_LINE; j++) begin : BTB_DATA
//             pseudo_dual_ram 
//             #(
//                 .WIDTH(BR_OFFSET_WIDTH+1),
//                 .DEPTH(BTB_N_ENTRY)
//             ) target_ram_way
//             (
//                 .ra(req_index_s0),
//                 .re(req_valid_i),
//                 .rd(targets[i][j]),
                
//                 .wa(write_idx),
//                 .we(ram_we & (write_which_bank == j) & (write_which_way == i)),
//                 .wd(ubtb_evict_offsets_i[j]),
                
//                 .rst(rst),
//                 .clk(clk)
//             );
//         end
//     end
// endgenerate

assign write_idx = ubtb_evict_idx_i[$clog2(BTB_N_ENTRY)-1:0];
assign write_which_way = evict_hit ? evict_hit_way : alloc_which_way;

always_comb begin
    for (int i = 0; i < BTB_N_WAYS; i++) begin
        evict_ori_tags[i] = tag_ram[i][write_idx];
    end
end

always_comb begin
    evict_hit = 0;
    evict_hit_way = 0;
    for (int i=0; i<BTB_N_WAYS; i++) begin
        if (ubtb_evict_i & (evict_ori_tags[i] == ubtb_evict_tag_i)) begin
            evict_hit = 1;
            evict_hit_way = i;
        end
    end
end


always_ff @(posedge clk) begin
    if (rst) begin
        tag_ram <= 0;
    end
    else if (ram_we & ~evict_hit) begin
        tag_ram[write_which_way][write_idx] <= ubtb_evict_tag_i;
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        is_misalign_ram <= 0;
    end
    else if (ram_we & ~evict_hit) begin
        is_misalign_ram[write_which_way][write_idx] <= ubtb_evict_is_misalign_i;
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        is_truncate_ram <= 0;
    end
    else if (ram_we & ~evict_hit) begin
        is_truncate_ram[write_which_way][write_idx] <= ubtb_evict_is_truncate_i;
    end
end

always_ff @(posedge clk) begin
    if (ram_we) begin
        for (int i = 0; i < PREDS_PER_LINE; i++) begin
            offset_ram[write_which_way][write_idx][i] <= ubtb_evict_offsets_i[i];
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        target_valid_ram <= 0;
    end
    else begin
        if (ram_we) begin
            if (evict_hit) begin
                target_valid_ram[write_which_way][write_idx] <= target_valid_ram[write_which_way][write_idx] | ubtb_evict_target_valid_i;
            end
            else begin
                target_valid_ram[write_which_way][write_idx] <= ubtb_evict_target_valid_i;
            end
        end
    end
end

always_ff @(posedge clk) begin
    if (ram_we) begin
        for (int i = 0; i < PREDS_PER_LINE; i++) begin
            if (ubtb_evict_target_valid_i[i]) begin
                target_ram[write_which_way][write_idx][i] <= ubtb_evict_targets_i[i];
            end
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        pred_valid_ram <= 0;
    end
    else begin
        if (ram_we) begin
            if (evict_hit) begin
                pred_valid_ram[write_which_way][write_idx] <= pred_valid_ram[write_which_way][write_idx] | ubtb_evict_pred_valid_i;
            end
            else begin
                pred_valid_ram[write_which_way][write_idx] <= ubtb_evict_pred_valid_i;
            end
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        br_mask_ram <= 0;
    end
    else begin
        if (ram_we) begin
            br_mask_ram[write_which_way][write_idx] <= ubtb_evict_br_mask_i;
        end
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        cfi_mask_ram <= 0;
    end
    else if (ram_we) begin
        cfi_mask_ram[write_which_way][write_idx] <= ubtb_evict_cfi_mask_i;
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        rvc_mask_ram <= 0;
    end
    else if (ram_we) begin
        rvc_mask_ram[write_which_way][write_idx] <= ubtb_evict_rvc_mask_i;
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        call_mask_ram <= 0;
    end
    else if (ram_we) begin
        call_mask_ram[write_which_way][write_idx] <= ubtb_evict_call_mask_i;
    end
end

always_ff @(posedge clk) begin
    if (rst) begin
        ret_mask_ram <= 0;
    end
    else if (ram_we) begin
        ret_mask_ram[write_which_way][write_idx] <= ubtb_evict_ret_mask_i;
    end
end

//*****************s0 stage***********************************
assign req_index_s0 = req_pc_i[$clog2(FETCH_WIDTH/8)+:$clog2(BTB_N_ENTRY)];// ^ req_ghist_i;
// always_comb begin
//     req_tag_s0 = 0;
//     for (int i = 0; i < (VADDR_WIDTH - BTB_TAG_LSB) / BTB_TAG_LEN; i++)
//         req_tag_s0 ^= req_pc_i[(BTB_TAG_LSB + (i*BTB_TAG_LEN))+:BTB_TAG_LEN];
// end

// assign req_tag_s0 = req_pc_i[VADDR_WIDTH-1:$clog2(FETCH_WIDTH/8)];
assign req_tag_s0 = btb_get_tag(req_pc_i);
//*****************s1 stage***********************************
always_ff @(posedge clk) begin
    if (rst) begin
        req_index_s1 <= 0;
        req_tag_s1 <= 0;
    end
    else if (f1_ready_i) begin
        req_index_s1 <= req_index_s0;
        req_tag_s1 <= req_tag_s0;
    end
end


always_ff @(posedge clk) begin
    for (int i = 0; i < BTB_N_WAYS; i++) begin
        br_masks[i] <= br_mask_ram[i][req_index_s0];
        cfi_masks[i] <= cfi_mask_ram[i][req_index_s0];
        rvc_masks[i] <= rvc_mask_ram[i][req_index_s0];
        call_masks[i] <= call_mask_ram[i][req_index_s0];
        ret_masks[i] <= ret_mask_ram[i][req_index_s0];
        tags[i] <= tag_ram[i][req_index_s0];
        is_misaligns[i] <= is_misalign_ram[i][req_index_s0];
        is_truncates[i] <= is_truncate_ram[i][req_index_s0];
        pred_valids[i] <= pred_valid_ram[i][req_index_s0];
        target_valids[i] <= target_valid_ram[i][req_index_s0];
        offsets[i] <= offset_ram[i][req_index_s0];
        targets[i] <= target_ram[i][req_index_s0];
    end
end



//*****************s1 stage***********************************
// output
always_comb begin
    req_hit_s1 = 0;
    which_way_s1 = 0;
    for (int i=0; i<BTB_N_WAYS; i++) begin
        if (tags[i] == req_tag_s1) begin
            req_hit_s1 = 1;
            which_way_s1 = i;
        end
    end
end

// plru
plru_with_invalid
#(
    .N_WAYS(BTB_N_WAYS),
    .N_SETS(BTB_N_ENTRY)
) u_btb_plru (
    .touch_valid_i(ram_we),
    .touch_addr_i(write_idx),
    .touch_way_i(write_which_way),

    .invalid_i(1'b0/*update_hit*/),
    .invalid_addr_i('0),
    .invalid_way_i('0),

    .alloc_way_o(alloc_which_way),

    .clk(clk),
    .rst(rst)
);

assign pred.pred_valid = {PREDS_PER_LINE{req_hit_s1}} & pred_valids[which_way_s1];
assign pred.target_valid = {PREDS_PER_LINE{req_hit_s1}} & target_valids[which_way_s1];
assign pred.target = targets[which_way_s1];
assign pred.offset = offsets[which_way_s1];
assign pred.br_mask = br_masks[which_way_s1];
assign pred.cfi_mask = cfi_masks[which_way_s1];
assign pred.rvc_mask = rvc_masks[which_way_s1];
assign pred.call_mask = call_masks[which_way_s1];
assign pred.ret_mask = ret_masks[which_way_s1];
assign pred.is_misalign = is_misaligns[which_way_s1];
assign pred.is_truncate = is_truncates[which_way_s1];

assign btb_meta.hit = req_hit_s1;
assign btb_meta.idx = req_index_s1;
assign btb_meta.which_way = which_way_s1;

always_ff @(posedge clk) begin
    if (rst) begin
        hold_rslt_ff <= 0;
    end
    else begin
        hold_rslt_ff <= ~f1_ready_i;
    end
end

always_ff @(posedge clk) begin
    if (~hold_rslt_ff) begin
        pred_ff <= pred;
        btb_meta_ff <= btb_meta;
    end
end

assign pred_s1 = hold_rslt_ff ? pred_ff : pred;
assign btb_meta_s1 = hold_rslt_ff ? btb_meta_ff : btb_meta;
//*****************s2 stage***********************************
always_ff @(posedge clk) begin
    if (f2_ready_i) begin
        pred_s2 <= pred_s1;
        btb_meta_s2 <= btb_meta_s1;
    end
end

always_comb begin
    for (int i = 0; i < PREDS_PER_LINE; i++) begin
        if (ubtb_target_valid_i[i]) begin
            pred_o.target_valid[i] = ubtb_target_valid_i[i];
            pred_o.target[i] = ubtb_pred_targets_i[i];
        end
        else begin
            pred_o.target_valid[i] = pred_s2.target_valid[i];
            pred_o.target[i] = pred_s2.target[i];
        end
    end
end

always_comb begin
    if (ubtb_valid_i) begin
        pred_o.pred_valid = ubtb_pred_valid_i;
    end
    else if (btb_meta_s2.hit) begin
        pred_o.pred_valid = pred_s2.pred_valid;
    end
    else begin
        pred_o.pred_valid = 0;
    end
end

always_comb begin
    if (ubtb_valid_i) begin
        pred_o.is_misalign = ubtb_pred_is_misalign_i;
        pred_o.is_truncate = ubtb_pred_is_truncate_i;
    end
    else if (btb_meta_s2.hit) begin
        pred_o.is_misalign = pred_s2.is_misalign;
        pred_o.is_truncate = pred_s2.is_truncate;
    end
    else begin
        pred_o.is_misalign = 0;
        pred_o.is_truncate = 0;
    end
end

always_comb begin
    for (int i = 0; i < PREDS_PER_LINE; i++) begin
        if (ubtb_valid_i) begin
            pred_o.br_mask[i] = ubtb_pred_br_mask_i[i];
            pred_o.cfi_mask[i] = ubtb_pred_cfi_mask_i[i];
            pred_o.rvc_mask[i] = ubtb_pred_rvc_mask_i[i];
            pred_o.call_mask[i] = ubtb_pred_call_mask_i[i];
            pred_o.ret_mask[i] = ubtb_pred_ret_mask_i[i];
            pred_o.offset[i] = ubtb_pred_offsets_i[i];
        end
        else if (btb_meta_s2.hit) begin
            pred_o.br_mask[i] = pred_s2.br_mask[i];
            pred_o.cfi_mask[i] = pred_s2.cfi_mask[i];
            pred_o.rvc_mask[i] = pred_s2.rvc_mask[i];
            pred_o.call_mask[i] = pred_s2.call_mask[i];
            pred_o.ret_mask[i] = pred_s2.ret_mask[i];
            pred_o.offset[i] = pred_s2.offset[i];
        end
        else begin
            pred_o.br_mask[i] = 0;
            pred_o.cfi_mask[i] = 0;
            pred_o.rvc_mask[i] = 0;
            pred_o.call_mask[i] = 0;
            pred_o.ret_mask[i] = 0;
            pred_o.offset[i] = 0;
        end
    end
end

assign btb_meta_o = btb_meta_s2;

assign ram_we = ubtb_evict_i;
assign req_ready_o = 1;





`ifdef SIMULATION
    logic[VADDR_WIDTH-1:0] base_pc_s1;
    always_ff @(posedge clk) begin
        if (rst) begin
            base_pc_s1<= 0;
        end
        else if (f1_ready_i) begin
            base_pc_s1 <= req_pc_i;
        end
    end
    assign btb_meta.base_pc = base_pc_s1;

    // always_ff @(posedge clk) begin
    //     if (btb_meta_s2.hit & ubtb_valid_i) begin
    //         for (int i = 0; i < PREDS_PER_LINE; i++) begin
    //             if ((i >= mask_vld_ptr_s2) & (i >= ubtb_pred_mask_vld_ptr_i)) begin
    //                 if ((ubtb_pred_br_mask_i[i] != pred_s2.br_mask[i]) | (ubtb_pred_cfi_mask_i[i] != pred_s2.cfi_mask[i])) begin
    //                     $display("(%d): Assert error!", $time());
    //                     $finish();
    //                 end
    //             end
    //         end
    //     end
    // end


`endif

endmodule
