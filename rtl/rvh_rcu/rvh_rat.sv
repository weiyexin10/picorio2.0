module rvh_rat #(
    parameter  int unsigned RENAME_WIDTH   = 4,
    parameter  int unsigned COMMIT_WIDTH   = 4,
    parameter  int unsigned ISA_REG_COUNT  = 32,
    parameter  int unsigned PHY_REG_COUNT  = 128,
    parameter  int unsigned ZERO_REG       = 1,
    localparam int unsigned IREG_TAG_WIDTH = $clog2(ISA_REG_COUNT),
    localparam int unsigned PREG_TAG_WIDTH = $clog2(PHY_REG_COUNT)
) (
    // Rename
    input  logic [RENAME_WIDTH-1:0]                     rename_vld_i,
    input  logic [RENAME_WIDTH-1:0][IREG_TAG_WIDTH-1:0] rename_rd_i,
    input  logic [RENAME_WIDTH-1:0][PREG_TAG_WIDTH-1:0] rename_prd_i,
    // Read RAT
    input  logic [RENAME_WIDTH-1:0][IREG_TAG_WIDTH-1:0] isa_rs1_i,
    input  logic [RENAME_WIDTH-1:0][IREG_TAG_WIDTH-1:0] isa_rs2_i,
    input  logic [RENAME_WIDTH-1:0][IREG_TAG_WIDTH-1:0] isa_rd_i,
    output logic [RENAME_WIDTH-1:0][PREG_TAG_WIDTH-1:0] phy_rs1_o,
    output logic [RENAME_WIDTH-1:0][PREG_TAG_WIDTH-1:0] phy_rs2_o,
    output logic [RENAME_WIDTH-1:0][PREG_TAG_WIDTH-1:0] lphy_rd_o,
    // Commit
    input  logic [COMMIT_WIDTH-1:0]                     commit_vld_i,
    input  logic [COMMIT_WIDTH-1:0][IREG_TAG_WIDTH-1:0] commit_rd_i,
    input  logic [COMMIT_WIDTH-1:0][PREG_TAG_WIDTH-1:0] commit_prd_i,
    // Recovery
    input  logic                                        do_recovery_i,


    input clk,
    input rst
);

    logic [ISA_REG_COUNT-1:0][PREG_TAG_WIDTH-1:0] arch_rat_dff;
    logic [ISA_REG_COUNT-1:0][PREG_TAG_WIDTH-1:0] rat_dff;

    always_comb begin : read_rat_resolve_conflict
        for(int i = 0; i < RENAME_WIDTH; i++) begin
            phy_rs1_o[i] = rat_dff[isa_rs1_i[i]];
            phy_rs2_o[i] = rat_dff[isa_rs2_i[i]];
            lphy_rd_o[i] = rat_dff[isa_rd_i[i]];
        end
        for(int i = 0 ; i < RENAME_WIDTH; i++) begin
            for(int j = i ; j < RENAME_WIDTH; j++) begin
                if(rename_vld_i[i] & (rename_rd_i[i] == isa_rs1_i[j]) & (i != j)) begin
                    phy_rs1_o[j] = rename_prd_i[i]; 
                end
                if(rename_vld_i[i] & (rename_rd_i[i] == isa_rs2_i[j]) & (i != j)) begin
                    phy_rs2_o[j] = rename_prd_i[i]; 
                end
                if(rename_vld_i[i] & (rename_rd_i[i] == isa_rd_i[j]) & (i != j)) begin
                    lphy_rd_o[j] = rename_prd_i[i]; 
                end
            end
        end
    end

    always_ff @(posedge clk) begin : rat_dff_update
        if (rst) begin
            for (int i = 0; i < ISA_REG_COUNT; i++) begin
                rat_dff[i] <= i[PREG_TAG_WIDTH-1:0];
            end
        end else begin
            if(do_recovery_i) begin
                rat_dff <= arch_rat_dff;
            end else begin
                for (int i = 0; i < RENAME_WIDTH; i++) begin
                    if (rename_vld_i[i] & (~ZERO_REG | (rename_rd_i[i] != 0))) begin
                        rat_dff[rename_rd_i[i]] <= rename_prd_i[i];
                    end
                end
            end
        end
    end


    always_ff @(posedge clk) begin : arch_rat_dff_update
        if (rst) begin
            for (int i = 0; i < ISA_REG_COUNT; i++) begin
                arch_rat_dff[i] <= i[PREG_TAG_WIDTH-1:0];
            end
        end else begin
            for (int i = 0; i < COMMIT_WIDTH; i++) begin
                if (commit_vld_i[i] & (~ZERO_REG | (commit_rd_i[i] != 0))) begin
                    arch_rat_dff[commit_rd_i[i]] <= commit_prd_i[i];
                end
            end
        end
    end

endmodule
