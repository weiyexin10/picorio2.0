module prf_freelist #(
    parameter  int unsigned ALLOC_WIDTH    = 4,
    parameter  int unsigned DEALLOC_WIDTH  = 4,
    parameter  int unsigned ISA_REG_COUNT  = 32,
    parameter  int unsigned PREG_COUNT     = 128,
    localparam int unsigned PREG_TAG_WIDTH = $clog2(PREG_COUNT),
    localparam int unsigned FL_ENTRY_COUNT = PREG_COUNT - ISA_REG_COUNT

) (
    // Allocate
    input  logic [DEALLOC_WIDTH-1:0]                     alloc_vld_i,
    output logic [DEALLOC_WIDTH-1:0][PREG_TAG_WIDTH-1:0] alloc_preg_tag_o,
    output logic                                         alloc_rdy_o,
    // Deallocate
    input  logic [  ALLOC_WIDTH-1:0]                     dealloc_vld_i,
    input  logic [  ALLOC_WIDTH-1:0][PREG_TAG_WIDTH-1:0] dealloc_preg_tag_i,

    input logic do_recovery,

    input clk,
    input rst
);


    localparam int unsigned FL_ENTRY_PTR_WIDTH = $clog2(FL_ENTRY_COUNT);
    localparam int unsigned FL_ENTRY_CNT_WIDTH = $clog2(FL_ENTRY_COUNT + 1);

    logic [    FL_ENTRY_COUNT-1:0][PREG_TAG_WIDTH-1:0] fl_dff;
    logic [       ALLOC_WIDTH-1:0][FL_ENTRY_PTR_WIDTH-1:0] fl_head_ptr;
    logic [     DEALLOC_WIDTH-1:0][FL_ENTRY_PTR_WIDTH-1:0] fl_tail_ptr;
    logic [FL_ENTRY_CNT_WIDTH-1:0]                         fl_used_cnt;

    logic                                                  fl_alloc_rdy;
    logic [       ALLOC_WIDTH-1:0]                         fl_alloc_fire;
    logic [     DEALLOC_WIDTH-1:0]                         fl_dealloc_fire;



    assign fl_alloc_rdy  = FL_ENTRY_COUNT - fl_used_cnt >= ALLOC_WIDTH;
    assign fl_alloc_fire = alloc_vld_i & {ALLOC_WIDTH{fl_alloc_rdy}};


    generate
        for (genvar i = 0; i < ALLOC_WIDTH; i++) begin
            assign alloc_preg_tag_o[i] = alloc_vld_i[i] ? fl_dff[fl_head_ptr[i]] : {PREG_TAG_WIDTH{1'b0}};
        end
    endgenerate
    assign alloc_rdy_o = fl_alloc_rdy;


    assign fl_dealloc_fire = dealloc_vld_i;


    always_ff @(posedge clk) begin : fl_dff_update
        if (rst) begin
            for (int i = 0; i < FL_ENTRY_COUNT; i++) begin
                fl_dff[i] <= i[FL_ENTRY_PTR_WIDTH-1:0] + 32;
            end
        end else begin
            for (int i = 0; i < DEALLOC_WIDTH; i++) begin
                if (fl_dealloc_fire[i]) begin
                    fl_dff[fl_tail_ptr[i]] <= dealloc_preg_tag_i[i];
                end
            end
        end
    end

    usage_manager #(
        .ENTRY_COUNT(FL_ENTRY_COUNT),
        .ENQ_WIDTH(ALLOC_WIDTH),
        .DEQ_WIDTH(DEALLOC_WIDTH),
        .FLAG_EN(0),
        .INIT_IS_FULL(1),
        .COMB_DEQ_EN(1),
        .COMB_ENQ_EN(1)
    ) u_usage_manage (
        .enq_fire_i(fl_dealloc_fire),
        .deq_fire_i(fl_alloc_fire),
        .head_o(fl_head_ptr),
        .tail_o(fl_tail_ptr),
        .avail_cnt_o(fl_used_cnt),
        .flush_i(do_recovery),
        .clk(clk),
        .rst(rst)
    );

// `ifdef SIMULATION
//     always_comb begin : assertion
//         for (int i = 0; i < ALLOC_WIDTH; i++) begin
//             if(fl_alloc_fire[i] & ~rst) begin
//                 assert (alloc_preg_tag_o[i] != 0)
//                 else begin
//                     $error("Cycle:%d: Allocate Zero Register", $time());
//                     $finish();
//                 end            end
//         end
//         for (int i = 0; i < DEALLOC_WIDTH; i++) begin
//             if(fl_dealloc_fire[i] & ~rst) begin
//                 assert (dealloc_preg_tag_i[i] != 0) 
//                 else begin
//                     $error("Cycle:%d: Deallocate Zero Register", $time());
//                     $finish();
//                 end
//             end
//         end
//     end
// `endif

endmodule
