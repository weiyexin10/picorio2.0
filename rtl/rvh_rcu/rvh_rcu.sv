module rvh_rcu
    import rvh_pkg::*;
    import riscv_pkg::*;
(
    // Allocate
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_vld_i,
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_excp_i,
    input  logic [ EXCP_TVAL_WIDTH-1:0]                        rn_rcu_alloc_inst_excp_tval_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                        rn_rcu_alloc_inst_excp_cause_i,
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_control_flow_i,
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_is_rvc_i,
    input  logic [    RENAME_WIDTH-1:0][      VADDR_WIDTH-1:0] rn_rcu_alloc_inst_pc_i,
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_is_br_i,
    input  logic [    RENAME_WIDTH-1:0][    BTQ_TAG_WIDTH-1:0] rn_rcu_alloc_inst_btq_tag_i,
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_pred_taken_i,
    input  logic [    RENAME_WIDTH-1:0][      VADDR_WIDTH-1:0] rn_rcu_alloc_inst_pred_target_i,
    input  logic [    RENAME_WIDTH-1:0]                        rn_rcu_alloc_inst_use_rd_i,
    input  logic [    RENAME_WIDTH-1:0][ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rd_i,
    input  logic [    RENAME_WIDTH-1:0][ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rs1_i,
    input  logic [    RENAME_WIDTH-1:0][ISA_REG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_isa_rs2_i,
    output logic [    RENAME_WIDTH-1:0][   PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rs1_o,
    output logic [    RENAME_WIDTH-1:0][   PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rs2_o,
    output logic [    RENAME_WIDTH-1:0][   PREG_TAG_WIDTH-1:0] rn_rcu_alloc_inst_phy_rd_o,
    output logic [    RENAME_WIDTH-1:0][    ROB_TAG_WIDTH-1:0] rn_rcu_alloc_inst_rob_tag_o,
    output logic [    RENAME_WIDTH-1:0][    BRQ_TAG_WIDTH-1:0] rn_rcu_alloc_inst_brq_tag_o,
    output logic                                               rn_rcu_alloc_rcu_rdy_o,

    // Issue
    input  logic [       ALU_COUNT-1:0][   ROB_TAG_WIDTH-1:0] alu_issue_rob_tag_i,
    output logic [       ALU_COUNT-1:0][     VADDR_WIDTH-1:0] alu_issue_pc_o,
    input  logic [       BRU_COUNT-1:0][   ROB_TAG_WIDTH-1:0] bru_issue_rob_tag_i,
    output logic [       BRU_COUNT-1:0][     VADDR_WIDTH-1:0] bru_issue_pc_o,
    output logic [       BRU_COUNT-1:0]                       bru_issue_is_rvc_o,
    output logic [       BRU_COUNT-1:0][   BTQ_TAG_WIDTH-1:0] bru_issue_btq_tag_o,
    input  logic [       BRU_COUNT-1:0][   BRQ_TAG_WIDTH-1:0] bru_issue_brq_tag_i,
    output logic [       BRU_COUNT-1:0]                       bru_issue_pred_taken_o,
    output logic [       BRU_COUNT-1:0][     VADDR_WIDTH-1:0] bru_issue_pred_target_o,
    // Write Back : Clear Busy
    input  logic [    ROB_CB_WIDTH-1:0]                       wb_rob_cb_vld_i,
    input  logic [    ROB_CB_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] wb_rob_cb_tag_i,
    // Write Back : Branch Resolve
    input  logic [    ROB_BR_WIDTH-1:0]                       wb_rob_br_vld_i,
    input  logic [    ROB_BR_WIDTH-1:0]                       wb_rob_br_mispred_i,
    input  logic [    ROB_BR_WIDTH-1:0]                       wb_rob_br_is_taken_i,
    input  logic [    ROB_BR_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] wb_rob_br_rob_tag_i,
    input  logic [    ROB_BR_WIDTH-1:0][   BRQ_TAG_WIDTH-1:0] wb_rob_br_brq_tag_i,
    input  logic [    ROB_BR_WIDTH-1:0][     VADDR_WIDTH-1:0] wb_rob_br_target_i,
    // Write Back : Report Exception
    input  logic [    ROB_RE_WIDTH-1:0]                       wb_rob_re_vld_i,
    input  logic [    ROB_RE_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] wb_rob_re_tag_i,
    input  logic [    ROB_RE_WIDTH-1:0][EXCP_CAUSE_WIDTH-1:0] wb_rob_re_cause_i,
    input  logic [    ROB_RE_WIDTH-1:0][ EXCP_TVAL_WIDTH-1:0] wb_rob_re_tval_i,
    // Redirect fetch
    output logic                                              redirect_vld_o,
    output logic [     VADDR_WIDTH-1:0]                       redirect_target_o,
    output logic [     VADDR_WIDTH-1:0]                       redirect_pc_o,
    output logic [   BTQ_TAG_WIDTH-1:0]                       redirect_btq_tag_o,
    output logic                                              redirect_is_taken_o,
    // Deallocate 
    output logic [    RETIRE_WIDTH-1:0]                       retire_insn_vld_o,
    output logic [    RETIRE_WIDTH-1:0][     VADDR_WIDTH-1:0] retire_insn_pc_o,
    output logic [    RETIRE_WIDTH-1:0]                       retire_insn_is_br_o,
    output logic [    RETIRE_WIDTH-1:0]                       retire_insn_mispred_o,
    output logic [    RETIRE_WIDTH-1:0][   BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag_o,
    // Exception
    output logic                                              retire_excp_vld_o,
    output logic [     VADDR_WIDTH-1:0]                       retire_excp_pc_o,
    output logic                                              retire_excp_is_rvc_o,
    output logic [ EXCP_TVAL_WIDTH-1:0]                       retire_excp_tval_o,
    output logic [EXCP_CAUSE_WIDTH-1:0]                       retire_excp_cause_o,
    output logic [   BTQ_TAG_WIDTH-1:0]                       retire_excp_btq_tag_o,
    // Wake up
    output logic                                              rob_empty_o,
    output logic [   ROB_TAG_WIDTH-1:0]                       oldest_insn_rob_tag_o,
    // Boardcast : Non-Resolved Branch Ptr
    output logic                                              brq_empty_o,
    output logic [   ROB_TAG_WIDTH-1:0]                       oldest_br_rob_tag_o,
    // Flush backend
    output logic                                              flush_be_vld_o,

    input clk,
    input rst
);

    logic rob_alloc_rdy, int_fl_alloc_rdy, brq_alloc_rdy;
    logic                                                rcu_alloc_rdy;
    logic [    RENAME_WIDTH-1:0]                         rcu_alloc_fire;

    logic [    RENAME_WIDTH-1:0]                         rn_brq_alloc_inst_vld;
    logic [    RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_brq_alloc_inst_rob_tag;
    logic [    RENAME_WIDTH-1:0]                         rn_brq_alloc_inst_pred_taken;
    logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_brq_alloc_inst_pred_target;
    logic [    RENAME_WIDTH-1:0][     BRQ_TAG_WIDTH-1:0] rn_brq_alloc_inst_brq_tag;

    // rn -> int fl
    logic [    RENAME_WIDTH-1:0]                         int_fl_alloc_vld;
    logic [    RENAME_WIDTH-1:0][INT_PREG_TAG_WIDTH-1:0] int_fl_alloc_ptag;

    // rn -> int_rat
    logic [    RENAME_WIDTH-1:0]                         int_rat_rename_vld;
    logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] int_rat_rename_rs1;
    logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] int_rat_rename_rs2;
    logic [    RENAME_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] int_rat_rename_rd;
    logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] int_rat_rename_prs1;
    logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] int_rat_rename_prs2;
    logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] int_rat_rename_lprd;
    logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] int_rat_rename_prd;


    // rn -> rob
    logic [    RENAME_WIDTH-1:0]                         rn_rob_alloc_vld;
    logic [    RENAME_WIDTH-1:0]                         rn_rob_alloc_excp;
    logic [    RENAME_WIDTH-1:0]                         rn_rob_alloc_control_flow;
    logic [    RENAME_WIDTH-1:0]                         rn_rob_alloc_is_rvc;
    logic [    RENAME_WIDTH-1:0]                         rn_rob_alloc_is_br;
    logic [    RENAME_WIDTH-1:0][     BTQ_TAG_WIDTH-1:0] rn_rob_alloc_btq_tag;
    logic [    RENAME_WIDTH-1:0][       VADDR_WIDTH-1:0] rn_rob_alloc_pc;
    logic [    RENAME_WIDTH-1:0][    REG_TYPE_WIDTH-1:0] rn_rob_alloc_rd_type;
    logic [    RENAME_WIDTH-1:0][                   4:0] rn_rob_alloc_isa_rd;
    logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rob_alloc_phy_rd;
    logic [    RENAME_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rn_rob_alloc_lphy_rd;
    logic [ EXCP_TVAL_WIDTH-1:0]                         rn_rob_alloc_excp_tval;
    logic [EXCP_CAUSE_WIDTH-1:0]                         rn_rob_alloc_excp_cause;
    logic [    RENAME_WIDTH-1:0][     ROB_TAG_WIDTH-1:0] rn_rob_alloc_rob_tag;

    // rob -> int_fl
    logic [    RETIRE_WIDTH-1:0]                         rob_int_fl_commit_vld;
    logic [    RETIRE_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rob_int_fl_commit_lphy_rd;
    // rob -> int_rat
    logic [    RETIRE_WIDTH-1:0]                         rob_int_rat_commit_vld;
    logic [    RETIRE_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] rob_int_rat_commit_isa_rd;
    logic [    RETIRE_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] rob_int_rat_commit_phy_rd;
    // Commit
    logic [    RETIRE_WIDTH-1:0]                         retire_insn_vld;
    logic [    RETIRE_WIDTH-1:0][       VADDR_WIDTH-1:0] retire_insn_pc;
    logic [    RETIRE_WIDTH-1:0]                         retire_insn_is_br;
    logic [    RETIRE_WIDTH-1:0]                         retire_insn_mispred;
    logic [    RETIRE_WIDTH-1:0][     BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag;
    logic [    RETIRE_WIDTH-1:0][    REG_TYPE_WIDTH-1:0] retire_insn_rd_type;
    logic [    RETIRE_WIDTH-1:0][ ISA_REG_TAG_WIDTH-1:0] retire_insn_isa_rd;
    logic [    RETIRE_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] retire_insn_phy_rd;
    logic [    RETIRE_WIDTH-1:0][    PREG_TAG_WIDTH-1:0] retire_insn_lphy_rd;

    assign rcu_alloc_rdy = rob_alloc_rdy & int_fl_alloc_rdy & brq_alloc_rdy;

    assign rcu_alloc_fire = rn_rcu_alloc_inst_vld_i & {RENAME_WIDTH{rcu_alloc_rdy}};

    // rn -> brq
    assign rn_brq_alloc_inst_vld = rcu_alloc_fire & rn_rcu_alloc_inst_is_br_i;
    assign rn_brq_alloc_inst_rob_tag = rn_rob_alloc_rob_tag;
    assign rn_brq_alloc_inst_pred_taken = rn_rcu_alloc_inst_pred_taken_i;
    assign rn_brq_alloc_inst_pred_target = rn_rcu_alloc_inst_pred_target_i;

    // rn -> int_rat
    assign int_rat_rename_vld = int_fl_alloc_vld;
    assign int_rat_rename_rs1 = rn_rcu_alloc_inst_isa_rs1_i;
    assign int_rat_rename_rs2 = rn_rcu_alloc_inst_isa_rs2_i;
    assign int_rat_rename_rd = rn_rcu_alloc_inst_isa_rd_i;
    assign int_rat_rename_prd = int_fl_alloc_ptag;

    // rn -> int fl
    generate
        for (genvar i = 0; i < RENAME_WIDTH; i++) begin
            assign int_fl_alloc_vld[i] =
        rcu_alloc_fire[i] & (rn_rcu_alloc_inst_use_rd_i[i] & (|rn_rcu_alloc_inst_isa_rd_i[i]));
        end
    endgenerate

    // rn -> rob
    assign rn_rob_alloc_vld = rcu_alloc_fire;
    assign rn_rob_alloc_excp = rn_rcu_alloc_inst_excp_i;
    assign rn_rob_alloc_control_flow = rn_rcu_alloc_inst_control_flow_i;
    assign rn_rob_alloc_is_rvc = rn_rcu_alloc_inst_is_rvc_i;
    assign rn_rob_alloc_is_br = rn_rcu_alloc_inst_is_br_i;
    assign rn_rob_alloc_btq_tag = rn_rcu_alloc_inst_btq_tag_i;
    assign rn_rob_alloc_pc = rn_rcu_alloc_inst_pc_i;
    generate
        for (genvar i = 0; i < RENAME_WIDTH; i++) begin
            assign rn_rob_alloc_rd_type[i] = int_fl_alloc_vld[i] ? riscv_pkg::TYPE_INT :
                riscv_pkg::TYPE_NONE;
        end
    endgenerate
    assign rn_rob_alloc_isa_rd = rn_rcu_alloc_inst_isa_rd_i;
    assign rn_rob_alloc_phy_rd = int_rat_rename_prd;
    assign rn_rob_alloc_lphy_rd = int_rat_rename_lprd;
    assign rn_rob_alloc_excp_tval = rn_rcu_alloc_inst_excp_tval_i;
    assign rn_rob_alloc_excp_cause = rn_rcu_alloc_inst_excp_cause_i;

    // Allocate Output 
    assign rn_rcu_alloc_inst_phy_rs1_o = int_rat_rename_prs1;
    assign rn_rcu_alloc_inst_phy_rs2_o = int_rat_rename_prs2;
    assign rn_rcu_alloc_inst_phy_rd_o = int_rat_rename_prd;
    assign rn_rcu_alloc_inst_rob_tag_o = rn_rob_alloc_rob_tag;
    assign rn_rcu_alloc_inst_brq_tag_o = rn_brq_alloc_inst_brq_tag;
    assign rn_rcu_alloc_rcu_rdy_o = rcu_alloc_rdy;

    // rob -> int rat
    generate
        for (genvar i = 0; i < RETIRE_WIDTH; i++) begin
            assign rob_int_rat_commit_vld[i] = retire_insn_vld[i] &
                (retire_insn_rd_type[i] == riscv_pkg::TYPE_INT);
        end
    endgenerate
    assign rob_int_rat_commit_isa_rd = retire_insn_isa_rd;
    assign rob_int_rat_commit_phy_rd = retire_insn_phy_rd;

    // rob -> int fl
    assign rob_int_fl_commit_vld     = rob_int_rat_commit_vld;
    assign rob_int_fl_commit_lphy_rd = retire_insn_lphy_rd;


    // Retire output
    assign retire_insn_vld_o         = retire_insn_vld;
    assign retire_insn_pc_o          = retire_insn_pc;
    assign retire_insn_is_br_o       = retire_insn_is_br;
    assign retire_insn_mispred_o     = retire_insn_mispred;
    assign retire_insn_btq_tag_o     = retire_insn_btq_tag;


    rvh_rat #(
        .RENAME_WIDTH(RENAME_WIDTH),
        .COMMIT_WIDTH(RETIRE_WIDTH),
        .ISA_REG_COUNT(32),
        .PHY_REG_COUNT(INT_PREG_COUNT),
        .ZERO_REG(1)
    ) u_rvh_int_rat (
        .rename_vld_i(int_rat_rename_vld),
        .rename_rd_i(int_rat_rename_rd),
        .rename_prd_i(int_rat_rename_prd),
        .isa_rs1_i(int_rat_rename_rs1),
        .isa_rs2_i(int_rat_rename_rs2),
        .isa_rd_i(int_rat_rename_rd),
        .phy_rs1_o(int_rat_rename_prs1),
        .phy_rs2_o(int_rat_rename_prs2),
        .lphy_rd_o(int_rat_rename_lprd),
        .commit_vld_i(rob_int_rat_commit_vld),
        .commit_rd_i(rob_int_rat_commit_isa_rd),
        .commit_prd_i(rob_int_rat_commit_phy_rd),
        .do_recovery_i(flush_be_vld_o),
        .clk(clk),
        .rst(rst)
    );

    prf_freelist #(
        .ALLOC_WIDTH(RENAME_WIDTH),
        .DEALLOC_WIDTH(RETIRE_WIDTH),
        .ISA_REG_COUNT(32),
        .PREG_COUNT(INT_PREG_COUNT)
    ) u_int_prf_freelist (
        .alloc_vld_i(int_fl_alloc_vld),
        .alloc_preg_tag_o(int_fl_alloc_ptag),
        .alloc_rdy_o(int_fl_alloc_rdy),
        .dealloc_vld_i(rob_int_fl_commit_vld),
        .dealloc_preg_tag_i(rob_int_fl_commit_lphy_rd),
        .do_recovery(flush_be_vld_o),
        .clk(clk),
        .rst(rst)
    );

    rvh_brq u_rvh_brq (
        .rn_brq_alloc_inst_vld_i(rn_brq_alloc_inst_vld),
        .rn_brq_alloc_inst_rob_tag_i(rn_brq_alloc_inst_rob_tag),
        .rn_brq_alloc_inst_pred_taken_i(rn_brq_alloc_inst_pred_taken),
        .rn_brq_alloc_inst_pred_target_i(rn_brq_alloc_inst_pred_target),
        .rn_brq_alloc_inst_brq_tag_o(rn_brq_alloc_inst_brq_tag),
        .rn_brq_alloc_inst_rdy_o(brq_alloc_rdy),
        .iq_brq_issue_brq_tag_i(bru_issue_brq_tag_i),
        .iq_brq_issue_pred_taken_o(bru_issue_pred_taken_o),
        .iq_brq_issue_pred_target_o(bru_issue_pred_target_o),
        .bru_brq_resolve_vld_i(wb_rob_br_vld_i),
        .bru_brq_resolve_mispred_i(wb_rob_br_mispred_i),
        .bru_brq_resolve_brq_tag_i(wb_rob_br_brq_tag_i),
        .brq_empty_o(brq_empty_o),
        .oldest_br_rob_tag_o(oldest_br_rob_tag_o),
        .flush_i(flush_be_vld_o),
        .clk(clk),
        .rst(rst)
    );

    rvh_rob u_rvh_rob (
        .rn_rob_alloc_vld_i(rn_rob_alloc_vld),
        .rn_rob_alloc_excp_i(rn_rob_alloc_excp),
        .rn_rob_alloc_is_rvc_i(rn_rob_alloc_is_rvc),
        .rn_rob_alloc_is_br_i(rn_rob_alloc_is_br),
        .rn_rob_alloc_control_flow_i(rn_rob_alloc_control_flow),
        .rn_rob_alloc_btq_tag_i(rn_rob_alloc_btq_tag),
        .rn_rob_alloc_pc_i(rn_rob_alloc_pc),
        .rn_rob_alloc_rd_type_i(rn_rob_alloc_rd_type),
        .rn_rob_alloc_isa_rd_i(rn_rob_alloc_isa_rd),
        .rn_rob_alloc_phy_rd_i(rn_rob_alloc_phy_rd),
        .rn_rob_alloc_lphy_rd_i(rn_rob_alloc_lphy_rd),
        .rn_rob_alloc_excp_tval_i(rn_rob_alloc_excp_tval),
        .rn_rob_alloc_excp_cause_i(rn_rob_alloc_excp_cause),
        .rn_rob_alloc_rob_tag_o(rn_rob_alloc_rob_tag),
        .rn_rob_alloc_rdy_o(rob_alloc_rdy),
        .alu_issue_rob_tag_i(alu_issue_rob_tag_i),
        .alu_issue_pc_o(alu_issue_pc_o),
        .bru_issue_rob_tag_i(bru_issue_rob_tag_i),
        .bru_issue_pc_o(bru_issue_pc_o),
        .bru_issue_is_rvc_o(bru_issue_is_rvc_o),
        .bru_issue_btq_tag_o(bru_issue_btq_tag_o),
        .wb_rob_cb_vld_i(wb_rob_cb_vld_i),
        .wb_rob_cb_tag_i(wb_rob_cb_tag_i),
        .wb_rob_br_vld_i(wb_rob_br_vld_i),
        .wb_rob_br_mispred_i(wb_rob_br_mispred_i),
        .wb_rob_br_is_taken_i(wb_rob_br_is_taken_i),
        .wb_rob_br_tag_i(wb_rob_br_rob_tag_i),
        .wb_rob_br_target_i(wb_rob_br_target_i),
        .wb_rob_re_vld_i(wb_rob_re_vld_i),
        .wb_rob_re_tag_i(wb_rob_re_tag_i),
        .wb_rob_re_cause_i(wb_rob_re_cause_i),
        .wb_rob_re_tval_i(wb_rob_re_tval_i),
        .redirect_vld_o(redirect_vld_o),
        .redirect_target_o(redirect_target_o),
        .redirect_pc_o(redirect_pc_o),
        .redirect_btq_tag_o(redirect_btq_tag_o),
        .redirect_is_taken_o(redirect_is_taken_o),
        .retire_insn_vld_o(retire_insn_vld),
        .retire_insn_pc_o(retire_insn_pc),
        .retire_insn_is_br_o(retire_insn_is_br),
        .retire_insn_mispred_o(retire_insn_mispred),
        .retire_insn_btq_tag_o(retire_insn_btq_tag),
        .retire_insn_rd_type_o(retire_insn_rd_type),
        .retire_insn_isa_rd_o(retire_insn_isa_rd),
        .retire_insn_phy_rd_o(retire_insn_phy_rd),
        .retire_insn_lphy_rd_o(retire_insn_lphy_rd),
        .retire_excp_vld_o(retire_excp_vld_o),
        .retire_excp_pc_o(retire_excp_pc_o),
        .retire_excp_is_rvc_o(retire_excp_is_rvc_o),
        .retire_excp_tval_o(retire_excp_tval_o),
        .retire_excp_cause_o(retire_excp_cause_o),
        .retire_excp_btq_tag_o(retire_excp_btq_tag_o),
        .rob_empty_o(rob_empty_o),
        .oldest_insn_rob_tag_o(oldest_insn_rob_tag_o),
        .flush_be_vld_o(flush_be_vld_o),
        .clk(clk),
        .rst(rst)
    );
endmodule : rvh_rcu
