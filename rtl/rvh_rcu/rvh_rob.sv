module rvh_rob
    import rvh_pkg::*;
    import rvh_rcu_pkg::*;
    import riscv_pkg::*;
(
    // Allocate
    input  logic [    RENAME_WIDTH-1:0]                       rn_rob_alloc_vld_i,
    input  logic [    RENAME_WIDTH-1:0]                       rn_rob_alloc_excp_i,
    input  logic [    RENAME_WIDTH-1:0]                       rn_rob_alloc_control_flow_i,
    input  logic [    RENAME_WIDTH-1:0]                       rn_rob_alloc_is_rvc_i,
    input  logic [    RENAME_WIDTH-1:0]                       rn_rob_alloc_is_br_i,
    input  logic [    RENAME_WIDTH-1:0][   BTQ_TAG_WIDTH-1:0] rn_rob_alloc_btq_tag_i,
    input  logic [    RENAME_WIDTH-1:0][     VADDR_WIDTH-1:0] rn_rob_alloc_pc_i,
    input  logic [    RENAME_WIDTH-1:0][  REG_TYPE_WIDTH-1:0] rn_rob_alloc_rd_type_i,
    input  logic [    RENAME_WIDTH-1:0][                 4:0] rn_rob_alloc_isa_rd_i,
    input  logic [    RENAME_WIDTH-1:0][  PREG_TAG_WIDTH-1:0] rn_rob_alloc_phy_rd_i,
    input  logic [    RENAME_WIDTH-1:0][  PREG_TAG_WIDTH-1:0] rn_rob_alloc_lphy_rd_i,
    input  logic [ EXCP_TVAL_WIDTH-1:0]                       rn_rob_alloc_excp_tval_i,
    input  logic [EXCP_CAUSE_WIDTH-1:0]                       rn_rob_alloc_excp_cause_i,
    output logic [    RENAME_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] rn_rob_alloc_rob_tag_o,
    output logic                                              rn_rob_alloc_rdy_o,
    // Issue 
    input  logic [       ALU_COUNT-1:0][   ROB_TAG_WIDTH-1:0] alu_issue_rob_tag_i,
    output logic [       ALU_COUNT-1:0][     VADDR_WIDTH-1:0] alu_issue_pc_o,
    input  logic [       BRU_COUNT-1:0][   ROB_TAG_WIDTH-1:0] bru_issue_rob_tag_i,
    output logic [       BRU_COUNT-1:0][     VADDR_WIDTH-1:0] bru_issue_pc_o,
    output logic [       BRU_COUNT-1:0]                       bru_issue_is_rvc_o,
    output logic [       BRU_COUNT-1:0][   BTQ_TAG_WIDTH-1:0] bru_issue_btq_tag_o,
    // Write Back : Clear Busy
    input  logic [    ROB_CB_WIDTH-1:0]                       wb_rob_cb_vld_i,
    input  logic [    ROB_CB_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] wb_rob_cb_tag_i,
    // Write Back : Misprediction
    input  logic [    ROB_BR_WIDTH-1:0]                       wb_rob_br_vld_i,
    input  logic [    ROB_BR_WIDTH-1:0]                       wb_rob_br_mispred_i,
    input  logic [    ROB_BR_WIDTH-1:0]                       wb_rob_br_is_taken_i,
    input  logic [    ROB_BR_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] wb_rob_br_tag_i,
    input  logic [    ROB_BR_WIDTH-1:0][     VADDR_WIDTH-1:0] wb_rob_br_target_i,
    // Write Back : Report Exception
    input  logic [    ROB_RE_WIDTH-1:0]                       wb_rob_re_vld_i,
    input  logic [    ROB_RE_WIDTH-1:0][   ROB_TAG_WIDTH-1:0] wb_rob_re_tag_i,
    input  logic [    ROB_RE_WIDTH-1:0][EXCP_CAUSE_WIDTH-1:0] wb_rob_re_cause_i,
    input  logic [    ROB_RE_WIDTH-1:0][ EXCP_TVAL_WIDTH-1:0] wb_rob_re_tval_i,
    // Redirect fetch
    output logic                                              redirect_vld_o,
    output logic [     VADDR_WIDTH-1:0]                       redirect_target_o,
    output logic [     VADDR_WIDTH-1:0]                       redirect_pc_o,
    output logic [   BTQ_TAG_WIDTH-1:0]                       redirect_btq_tag_o,
    output logic                                              redirect_is_taken_o,
    // Deallocate 
    output logic [    RETIRE_WIDTH-1:0]                       retire_insn_vld_o,
    output logic [    RETIRE_WIDTH-1:0][     VADDR_WIDTH-1:0] retire_insn_pc_o,
    output logic [    RETIRE_WIDTH-1:0]                       retire_insn_is_br_o,
    output logic [    RETIRE_WIDTH-1:0]                       retire_insn_mispred_o,
    output logic [    RETIRE_WIDTH-1:0][   BTQ_TAG_WIDTH-1:0] retire_insn_btq_tag_o,
    output logic [    RETIRE_WIDTH-1:0][  REG_TYPE_WIDTH-1:0] retire_insn_rd_type_o,
    output logic [    RETIRE_WIDTH-1:0][                 4:0] retire_insn_isa_rd_o,
    output logic [    RETIRE_WIDTH-1:0][  PREG_TAG_WIDTH-1:0] retire_insn_phy_rd_o,
    output logic [    RETIRE_WIDTH-1:0][  PREG_TAG_WIDTH-1:0] retire_insn_lphy_rd_o,
    // Exception
    output logic                                              retire_excp_vld_o,
    output logic [     VADDR_WIDTH-1:0]                       retire_excp_pc_o,
    output logic [   BTQ_TAG_WIDTH-1:0]                       retire_excp_btq_tag_o,
    output logic                                              retire_excp_is_rvc_o,
    output logic [ EXCP_TVAL_WIDTH-1:0]                       retire_excp_tval_o,
    output logic [EXCP_CAUSE_WIDTH-1:0]                       retire_excp_cause_o,
    // Wake up
    output logic                                              rob_empty_o,
    output logic [ROB_TAG_WIDTH-1:0]                          oldest_insn_rob_tag_o,
    // Flush backend
    output logic                                              flush_be_vld_o,
    input clk,
    input rst
);

    localparam int unsigned ENTRY_ENQ_WIDTH = RENAME_WIDTH / ROB_BLOCK_PER_ENTRY;
    localparam int unsigned ENTRY_DEQ_WIDTH = RETIRE_WIDTH / ROB_BLOCK_PER_ENTRY;
    localparam int unsigned ENTRY_CNT_WIDHT = $clog2(ROB_ENTRY_COUNT + 1);
    localparam int unsigned ROB_CF_SRC_COUNT = ROB_BR_WIDTH + ROB_RE_WIDTH;

    // Rob Status
    logic idle2resume, resume2idle;
    logic rob_empty;
    rob_status_t rob_status;
    rob_tag_t rob_resume_tag;
    logic [EXCP_TVAL_WIDTH-1:0] rob_excp_tval;
    logic [EXCP_CAUSE_WIDTH-1:0] rob_excp_cause;
    rob_entry_t [ROB_ENTRY_COUNT-1:0] rob_dff;
    rob_ptr_t [ENTRY_ENQ_WIDTH-1:0] rob_tail_ptr;
    rob_ptr_t [ENTRY_DEQ_WIDTH-1:0] rob_head_ptr;
    logic [ENTRY_CNT_WIDHT-1:0] rob_avail_cnt;

    // Allocate 
    logic rob_enq_rdy;
    logic [ENTRY_ENQ_WIDTH-1:0][ROB_BLOCK_PER_ENTRY-1:0] insn_enq_vld;
    logic [ENTRY_ENQ_WIDTH-1:0] entry_enq_vld;
    logic [ENTRY_ENQ_WIDTH-1:0] entry_enq_fire;
    rob_entry_t [ENTRY_ENQ_WIDTH-1:0] enq_entry;

    // Deallocate
    logic [RETIRE_WIDTH-1:0] deq_insn_vld_flatten;
    logic [RETIRE_WIDTH-1:0] deq_insn_done_flatten;
    logic [RETIRE_WIDTH-1:0] deq_insn_excp_flatten;
    logic [RETIRE_WIDTH-1:0] deq_insn_mispred_flatten;

    logic [ENTRY_DEQ_WIDTH-1:0][ROB_BLOCK_PER_ENTRY-1:0] deq_insn_vld;

    logic [RETIRE_WIDTH-1:0] insn_deq_vld_flatten;
    logic [ENTRY_DEQ_WIDTH-1:0][ROB_BLOCK_PER_ENTRY-1:0] insn_deq_vld;
    logic [ENTRY_DEQ_WIDTH-1:0] entry_deq_fire;
    rob_entry_t [ENTRY_DEQ_WIDTH-1:0] deq_entry;


    // Issue
    rob_tag_t [ALU_COUNT-1:0] alu_issue_rob_tag;
    rob_tag_t [BRU_COUNT-1:0] bru_issue_rob_tag;

    // Write back : clear busy
    logic [ROB_CB_WIDTH-1:0] rob_cb_vld;
    rob_tag_t [ROB_CB_WIDTH-1:0] rob_cb_tag;

    // Write back : branch redirect
    logic [ROB_BR_WIDTH-1:0] rob_br_vld;
    logic [ROB_BR_WIDTH-1:0] rob_br_is_taken;
    rob_tag_t [ROB_BR_WIDTH-1:0] rob_br_tag;
    logic [ROB_BR_WIDTH-1:0][VADDR_WIDTH-1:0] rob_br_target;

    // Write back : report excp
    logic [ROB_RE_WIDTH-1:0] rob_re_vld;
    logic [ROB_RE_WIDTH-1:0][VADDR_WIDTH-1:0] rob_re_pc;
    rob_tag_t [ROB_RE_WIDTH-1:0] rob_re_tag;
    logic [ROB_RE_WIDTH-1:0][EXCP_CAUSE_WIDTH-1:0] rob_re_cause;
    logic [ROB_RE_WIDTH-1:0][EXCP_TVAL_WIDTH-1:0] rob_re_tval;

    // Change Control Flow
    logic [ROB_CF_SRC_COUNT-1:0][ROB_CF_SRC_COUNT-1:0] cf_mask;
    logic [ROB_CF_SRC_COUNT-1:0] cf_sel;
    logic [ROB_CF_SRC_COUNT-1:0] cf_vld;
    logic [ROB_CF_SRC_COUNT-1:0] cf_is_taken;
    logic [ROB_CF_SRC_COUNT-1:0] cf_redirect_vld;
    logic [ROB_CF_SRC_COUNT-1:0] cf_excp;
    logic [ROB_CF_SRC_COUNT-1:0][EXCP_CAUSE_WIDTH-1:0] cf_excp_cause;
    logic [ROB_CF_SRC_COUNT-1:0][EXCP_TVAL_WIDTH-1:0] cf_excp_tval;
    rob_tag_t [ROB_CF_SRC_COUNT-1:0] cf_tag;
    logic [ROB_CF_SRC_COUNT-1:0][VADDR_WIDTH-1:0] cf_target;


    logic [ROB_OFFSET_WIDTH-1:0] head_insn_offset;
    rob_tag_t head_insn_rob_tag;
    logic head_insn_vld;
    logic head_insn_is_excp;

    // control flow Handler
    logic [RENAME_WIDTH-1:0] fe_excp_mask;
    logic [RENAME_WIDTH-1:0] fe_cf_mask;
    logic [RENAME_WIDTH-1:0] fe_rob_sel_mask;
    logic fe_excp_vld;
    logic fe_cf_vld;
    logic [EXCP_TVAL_WIDTH-1:0] fe_excp_tval;
    rob_tag_t fe_cf_rob_tag;
    logic [EXCP_CAUSE_WIDTH-1:0] fe_excp_cause;

    logic be_cf_vld;
    logic be_cf_is_taken;
    logic be_cf_redirect_vld;
    logic be_cf_excp;
    logic [EXCP_CAUSE_WIDTH-1:0] be_cf_excp_cause;
    logic [EXCP_TVAL_WIDTH-1:0] be_cf_excp_tval;
    rob_tag_t be_cf_tag;
    logic [VADDR_WIDTH-1:0] be_cf_target;


    logic [RETIRE_WIDTH-1:0] retire_mispred_br_mask;
    logic retire_mispred_br;
    logic retire_exception;
    logic flush_rob;



    // Allocate
    always_comb begin : gen_enq_entry
        for (int entry = 0; entry < ENTRY_ENQ_WIDTH; entry++) begin
            for (int block = 0; block < ROB_BLOCK_PER_ENTRY; block++) begin
                insn_enq_vld[entry][block] = rn_rob_alloc_vld_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].pc_base =
                    get_pc_base(rn_rob_alloc_pc_i[entry*ROB_BLOCK_PER_ENTRY]);
                enq_entry[entry].insn[block].vld =
                    insn_enq_vld[entry][block];
                enq_entry[entry].insn[block].excp =
                    rn_rob_alloc_excp_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].done =
                    rn_rob_alloc_excp_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].mispred = 1'b0;
                enq_entry[entry].insn[block].is_br =
                    rn_rob_alloc_is_br_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].btq_tag =
                    rn_rob_alloc_btq_tag_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].is_rvc =
                    rn_rob_alloc_is_rvc_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].pc_offset =
                    get_pc_offset(rn_rob_alloc_pc_i[entry*ROB_BLOCK_PER_ENTRY+block]);
                enq_entry[entry].insn[block].rd_type =
                    rn_rob_alloc_rd_type_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].isa_rd =
                    rn_rob_alloc_isa_rd_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].phy_rd =
                    rn_rob_alloc_phy_rd_i[entry*ROB_BLOCK_PER_ENTRY+block];
                enq_entry[entry].insn[block].lphy_rd =
                    rn_rob_alloc_lphy_rd_i[entry*ROB_BLOCK_PER_ENTRY+block];
                rn_rob_alloc_rob_tag_o[entry*ROB_BLOCK_PER_ENTRY+block] = {
                    rob_tail_ptr[entry], block[ROB_OFFSET_WIDTH-1:0]
                };
            end
            entry_enq_vld[entry]  = |insn_enq_vld[entry];
            entry_enq_fire[entry] = entry_enq_vld[entry] & rob_enq_rdy;
        end
    end
    assign rob_enq_rdy = (rob_status == ROB_IDLE) & (rob_avail_cnt >= ENTRY_ENQ_WIDTH) & ~flush_rob;
    assign rn_rob_alloc_rdy_o = rob_enq_rdy;


    // Issue
    always_comb begin : alu_insn_pc
        for (int i = 0; i < ALU_COUNT; i++) begin
            alu_issue_pc_o[i] = {
                rob_dff[alu_issue_rob_tag[i].index].pc_base,
                rob_dff[alu_issue_rob_tag[i].index].insn[alu_issue_rob_tag[i].block].pc_offset,
                1'b0
            };
        end
    end

    always_comb begin : bru_insn_pc
        for (int i = 0; i < BRU_COUNT; i++) begin
            bru_issue_pc_o[i] = {
                rob_dff[bru_issue_rob_tag[i].index].pc_base,
                rob_dff[bru_issue_rob_tag[i].index].insn[bru_issue_rob_tag[i].block].pc_offset,
                1'b0
            };
            bru_issue_is_rvc_o[i] =
                rob_dff[bru_issue_rob_tag[i].index].insn[bru_issue_rob_tag[i].block].is_rvc;
            bru_issue_btq_tag_o[i] =
                rob_dff[bru_issue_rob_tag[i].index].insn[bru_issue_rob_tag[i].block].btq_tag;
        end
    end


    // Deallocate
    always_comb begin
        for (int entry = 0; entry < ENTRY_DEQ_WIDTH; entry++) begin
            for (int block = 0; block < ROB_BLOCK_PER_ENTRY; block++) begin
                deq_insn_vld[entry][block] = deq_entry[entry].insn[block].vld;
                deq_insn_vld_flatten[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].vld;
                deq_insn_done_flatten[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].done;
                deq_insn_excp_flatten[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].excp;
                deq_insn_mispred_flatten[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].mispred;
                insn_deq_vld[entry][block] = insn_deq_vld_flatten[entry*ROB_BLOCK_PER_ENTRY+block];
            end
        end
    end

    always_comb begin
        for (int entry = 0; entry < ENTRY_DEQ_WIDTH; entry++) begin
            deq_entry[entry] = rob_dff[rob_head_ptr[entry].index];
            entry_deq_fire[entry] = (&(~(insn_deq_vld[entry] ^ deq_insn_vld[entry]))) &
                ((ROB_ENTRY_COUNT - rob_avail_cnt) > entry);
        end
    end

    generate
        for (genvar i = 0; i < RETIRE_WIDTH; i++) begin : gen_inorder_commit_vld
            if (i == 0) begin
                assign insn_deq_vld_flatten[0] =
                    deq_insn_vld_flatten[0] & deq_insn_done_flatten[0] & ~deq_insn_excp_flatten[0];
            end else begin
                assign insn_deq_vld_flatten[i] =
                    ((insn_deq_vld_flatten[i-1] & ~deq_insn_mispred_flatten[i-1])
                     | ~deq_insn_vld_flatten[i-1]) & deq_insn_vld_flatten[i] &
                    deq_insn_done_flatten[i] & ~deq_insn_excp_flatten[i];
            end
        end
    endgenerate

    always_comb begin : retire_output
        for (int entry = 0; entry < ENTRY_DEQ_WIDTH; entry++) begin
            for (int block = 0; block < ROB_BLOCK_PER_ENTRY; block++) begin
                retire_insn_vld_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    insn_deq_vld[entry][block] & ~rob_empty;
                retire_insn_pc_o[entry*ROB_BLOCK_PER_ENTRY+block] = {
                    deq_entry[entry].pc_base, deq_entry[entry].insn[block].pc_offset, 1'b0
                };
                retire_insn_is_br_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].is_br;
                retire_insn_mispred_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].mispred;
                retire_insn_btq_tag_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].btq_tag;
                retire_insn_rd_type_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].rd_type;
                retire_insn_isa_rd_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].isa_rd;
                retire_insn_phy_rd_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].phy_rd;
                retire_insn_lphy_rd_o[entry*ROB_BLOCK_PER_ENTRY+block] =
                    deq_entry[entry].insn[block].lphy_rd;
            end
        end
    end


    // Issue
    assign alu_issue_rob_tag = alu_issue_rob_tag_i;
    assign bru_issue_rob_tag = bru_issue_rob_tag_i;

    // Write back : clear busy
    assign rob_cb_vld = wb_rob_cb_vld_i;
    assign rob_cb_tag = wb_rob_cb_tag_i;

    // Write back : report Exception
    assign rob_re_vld = wb_rob_re_vld_i;
    assign rob_re_tag = wb_rob_re_tag_i;
    assign rob_re_cause = wb_rob_re_cause_i;
    assign rob_re_tval = wb_rob_re_tval_i;
    always_comb begin
        for (int i = 0; i < ROB_RE_WIDTH; i++) begin
            rob_re_pc[i] = {
                rob_dff[rob_re_tag[i].index].pc_base,
                rob_dff[rob_re_tag[i].index].insn[rob_re_tag[i].block].pc_offset,
                1'b0
            };
        end
    end

    // Write back : branch redirect
    assign rob_br_vld = wb_rob_br_vld_i & wb_rob_br_mispred_i;
    assign rob_br_is_taken = wb_rob_br_vld_i & wb_rob_br_is_taken_i;
    assign rob_br_tag = wb_rob_br_tag_i;
    assign rob_br_target = wb_rob_br_target_i;


    // generate control flow
    generate
        for (genvar i = 0; i < ROB_BR_WIDTH; i++) begin
            assign cf_vld[i]          = rob_br_vld[i];
            assign cf_is_taken[i]     = rob_br_is_taken[i];
            assign cf_redirect_vld[i] = 1'b1;
            assign cf_target[i]       = rob_br_target[i];
            assign cf_tag[i]          = rob_br_tag[i];
            assign cf_excp[i]         = 1'b0;
            assign cf_excp_cause[i]   = {EXCP_CAUSE_WIDTH{1'b0}};
            assign cf_excp_tval[i]    = {EXCP_TVAL_WIDTH{1'b0}};
        end
        for (genvar i = 0; i < ROB_RE_WIDTH; i++) begin
            assign cf_vld[i+ROB_BR_WIDTH]          = rob_re_vld[i];
            assign cf_redirect_vld[i+ROB_BR_WIDTH] = 1'b0;
            assign cf_is_taken[i+ROB_BR_WIDTH]     = 1'b0;
            assign cf_target[i+ROB_BR_WIDTH]       = rob_re_pc[i];
            assign cf_tag[i+ROB_BR_WIDTH]          = rob_re_tag[i];
            assign cf_excp[i+ROB_BR_WIDTH]         = 1'b1;
            assign cf_excp_cause[i+ROB_BR_WIDTH]   = rob_re_cause[i];
            assign cf_excp_tval[i+ROB_BR_WIDTH]    = rob_re_tval[i];
        end
    endgenerate
    
    always_comb begin
        for (int i = 0; i < ROB_CF_SRC_COUNT; i++) begin
            for (int j = 0; j < ROB_CF_SRC_COUNT; j++) begin
                if (i == j) begin
                    cf_mask[i][j] = cf_vld[i] & (rob_tag_is_older(cf_tag[i], rob_resume_tag) | (rob_status == ROB_IDLE));
                end else begin
                    cf_mask[i][j] = (~cf_vld[j] | rob_tag_is_older(cf_tag[i], cf_tag[j]));
                end
            end
            cf_sel[i] = &cf_mask[i];
        end
    end

    assign be_cf_vld = |cf_sel;

    // Redirect
    assign redirect_vld_o = be_cf_vld & be_cf_redirect_vld;
    assign redirect_target_o = be_cf_target;
    assign redirect_is_taken_o = be_cf_vld & be_cf_is_taken;
    assign redirect_pc_o = {
        rob_dff[be_cf_tag.index].pc_base,
        rob_dff[be_cf_tag.index].insn[be_cf_tag.block].pc_offset,
        1'b0
    };
    assign redirect_btq_tag_o = rob_dff[be_cf_tag.index].insn[be_cf_tag.block].btq_tag;
    // Report Exception

    assign fe_excp_mask = rn_rob_alloc_vld_i & rn_rob_alloc_excp_i;
    assign fe_cf_mask = rn_rob_alloc_vld_i & rn_rob_alloc_control_flow_i;
    assign fe_excp_vld = |fe_excp_mask;
    assign fe_cf_vld = |fe_cf_mask;
    assign fe_excp_cause = rn_rob_alloc_excp_cause_i;
    assign fe_excp_tval = rn_rob_alloc_excp_tval_i;



    assign rob_empty = rob_avail_cnt == ROB_ENTRY_COUNT;
    assign idle2resume = (rob_status == ROB_IDLE) & (fe_cf_vld | fe_excp_vld | be_cf_vld);
    assign resume2idle = (rob_status == ROB_WAIT_FOR_RESUME) & rob_empty;

    // head insn
    assign head_insn_is_excp = deq_entry[0].insn[head_insn_offset].excp;

    assign retire_excp_vld_o = head_insn_vld & head_insn_is_excp;
    assign retire_excp_pc_o = {
        deq_entry[0].pc_base, deq_entry[0].insn[head_insn_offset].pc_offset, 1'b0
    };
    assign retire_excp_btq_tag_o = deq_entry[0].insn[head_insn_offset].btq_tag;
    assign retire_excp_is_rvc_o = deq_entry[0].insn[head_insn_offset].is_rvc;
    assign retire_excp_cause_o = rob_excp_cause;
    assign retire_excp_tval_o = rob_excp_tval;

    assign rob_empty_o = rob_empty;
    assign oldest_insn_rob_tag_o = {rob_head_ptr[0],head_insn_offset};

    // Rob resume handler
    assign retire_mispred_br_mask = insn_deq_vld_flatten & deq_insn_mispred_flatten;
    assign retire_mispred_br = |retire_mispred_br_mask;
    assign retire_exception = head_insn_vld & head_insn_is_excp;
    assign flush_be_vld_o = resume2idle;
    assign flush_rob = retire_mispred_br | retire_exception;


    always_ff @(posedge clk) begin : rob_status_fsm_dff
        if (rst) begin
            rob_status <= ROB_IDLE;
        end else begin
            if (idle2resume) begin
                rob_status <= ROB_WAIT_FOR_RESUME;
            end else if (resume2idle) begin
                rob_status <= ROB_IDLE;
            end
        end
    end

    always_ff @(posedge clk) begin : excp_dff_update
        if(be_cf_vld & be_cf_excp) begin
            rob_excp_cause <= be_cf_excp_cause;
            rob_excp_tval  <= be_cf_excp_tval;
        end else if(fe_excp_vld) begin
            rob_excp_cause <= fe_excp_cause;
            rob_excp_tval  <= fe_excp_tval;
        end
    end
    
    always_ff @(posedge clk) begin : rob_resume_tag_dff
        if(be_cf_vld) begin
            rob_resume_tag <= be_cf_tag;
        end else if(fe_excp_vld | fe_cf_vld) begin
            rob_resume_tag <= fe_cf_rob_tag;
        end
    end

    always_ff @(posedge clk) begin : rob_dff_update
        if (rst) begin
            for (int entry = 0; entry < ROB_ENTRY_COUNT; entry++) begin
                for (int block = 0; block < ROB_BLOCK_PER_ENTRY; block++) begin
                    rob_dff[entry].insn[block].vld <= 1'b0;
                end
            end
        end else begin
            for (int i = 0; i < ENTRY_ENQ_WIDTH; i++) begin
                if (entry_enq_vld[i]) begin
                    rob_dff[rob_tail_ptr[i].index] <= enq_entry[i];
                end
            end
            for (int i = 0; i < ROB_CB_WIDTH; i++) begin
                if (rob_cb_vld[i]) begin
                    rob_dff[rob_cb_tag[i].index].insn[rob_cb_tag[i].block].done <= 1'b1;
                end
            end
            for (int i = 0; i < ROB_RE_WIDTH; i++) begin
                if (rob_re_vld[i]) begin
                    rob_dff[rob_re_tag[i].index].insn[rob_re_tag[i].block].excp <= 1'b1;
                    rob_dff[rob_re_tag[i].index].insn[rob_re_tag[i].block].done <= 1'b1;
                end
            end
            for (int i = 0; i < ROB_BR_WIDTH; i++) begin
                if (rob_br_vld[i]) begin
                    rob_dff[rob_br_tag[i].index].insn[rob_br_tag[i].block].mispred <= 1'b1;
                end
            end
            for (int i = 0; i < ENTRY_DEQ_WIDTH; i++) begin
                for (int j = 0; j < ROB_BLOCK_PER_ENTRY; j++) begin
                    if (insn_deq_vld[i][j]) begin
                        rob_dff[rob_head_ptr[i].index].insn[j].vld <= 1'b0;
                    end
                end
            end
            if (flush_rob) begin
                for (int entry = 0; entry < ROB_ENTRY_COUNT; entry++) begin
                    for (int block = 0; block < ROB_BLOCK_PER_ENTRY; block++) begin
                        rob_dff[entry].insn[block].vld <= 1'b0;
                    end
                end
            end
        end
    end



    one_hot_priority_encoder #(
        .SEL_WIDTH(RENAME_WIDTH)
    ) u_fe_cf_tag_one_hot_priority(
        .sel_i(fe_excp_mask | fe_cf_mask),
        .sel_o(fe_rob_sel_mask)
    );

    onehot_mux #(
        .SOURCE_COUNT(RENAME_WIDTH),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_fe_excp_rob_tag_onehot_mux (
        .sel_i (fe_rob_sel_mask),
        .data_i(rn_rob_alloc_rob_tag_o),
        .data_o(fe_cf_rob_tag)
    );

    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_cf_rob_tag_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_tag),
        .data_o(be_cf_tag)
    );

    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (1)
    ) u_be_excp_vld_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_excp),
        .data_o(be_cf_excp)
    );

    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (EXCP_CAUSE_WIDTH)
    ) u_be_excp_cause_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_excp_cause),
        .data_o(be_cf_excp_cause)
    );

    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (EXCP_TVAL_WIDTH)
    ) u_be_excp_tval_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_excp_tval),
        .data_o(be_cf_excp_tval)
    );


    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (1)
    ) u_be_redirect_vld_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_redirect_vld),
        .data_o(be_cf_redirect_vld)
    );

    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (1)
    ) u_be_is_taken_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_is_taken),
        .data_o(be_cf_is_taken)
    );

    onehot_mux #(
        .SOURCE_COUNT(ROB_CF_SRC_COUNT),
        .DATA_WIDTH  (VADDR_WIDTH)
    ) u_be_cf_target_onehot_mux (
        .sel_i (cf_sel),
        .data_i(cf_target),
        .data_o(be_cf_target)
    );

    priority_encoder #(
        .SEL_WIDTH(ROB_BLOCK_PER_ENTRY)
    ) u_head_insn_priority_encoder (
        .sel_i(deq_insn_vld_flatten[ROB_BLOCK_PER_ENTRY-1:0]),
        .id_vld_o(head_insn_vld),
        .id_o(head_insn_offset)
    );

    usage_manager #(
        .ENTRY_COUNT(ROB_ENTRY_COUNT),
        .ENQ_WIDTH(ENTRY_ENQ_WIDTH),
        .DEQ_WIDTH(ENTRY_DEQ_WIDTH),
        .FLAG_EN(1),
        .INIT_IS_FULL(0),
        .COMB_DEQ_EN(0),
        .COMB_ENQ_EN(1)
    ) u_usage_manager (
        .enq_fire_i(entry_enq_fire),
        .deq_fire_i(entry_deq_fire),
        .head_o(rob_head_ptr),
        .tail_o(rob_tail_ptr),
        .avail_cnt_o(rob_avail_cnt),
        .flush_i(flush_rob),
        .clk(clk),
        .rst(rst)
    );

endmodule : rvh_rob
