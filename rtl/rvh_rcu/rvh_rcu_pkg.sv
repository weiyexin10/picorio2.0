package rvh_rcu_pkg;
    import rvh_pkg::*;
    import riscv_pkg::*;

    typedef enum logic[1:0] {
        ROB_IDLE            = 0, 
        ROB_WAIT_FOR_RESUME = 1,
        ROB_WAIT_CSR_RESUME = 2
    } rob_status_t;
    
    typedef struct packed {
        logic vld;
        logic excp;
        logic done;
        logic mispred;
        
        logic is_br;
        logic[BTQ_TAG_WIDTH-1:0] btq_tag;
        
        logic is_rvc;
        logic [ROB_PC_OFFSET_WIDTH-1:0] pc_offset;
        
        logic [REG_TYPE_WIDTH-1:0] rd_type;
        logic [4:0] isa_rd;
        logic [PREG_TAG_WIDTH-1:0] phy_rd;
        logic [PREG_TAG_WIDTH-1:0] lphy_rd;
    } rob_block_t;
        
    typedef struct packed {
        logic[ROB_PC_BASE_WIDTH-1:0]         pc_base;
        rob_block_t[ROB_BLOCK_PER_ENTRY-1:0] insn;
    } rob_entry_t;
    
    
    typedef struct packed {
        logic flag;
        logic [ROB_INDEX_WIDTH-1:0] index;
    } rob_ptr_t;

    typedef struct packed {
        logic flag;
        logic [ROB_INDEX_WIDTH-1:0] index;
        logic [ROB_OFFSET_WIDTH-1:0] block;
    } rob_tag_t;
    
    
    function automatic [ROB_PTR_WIDTH-1:0] rob_ptr_plus_one;
        input rob_ptr_t input_ptr;
        begin
            if (ROB_ENTRY_COUNT == 2 ** ROB_INDEX_WIDTH) begin
                rob_ptr_plus_one = input_ptr + 1'b1;
            end else begin
                if (input_ptr.index == ROB_ENTRY_COUNT - 1) begin
                    rob_ptr_plus_one = {~input_ptr.flag, {(ROB_INDEX_WIDTH) {1'b0}}};
                end else begin
                    rob_ptr_plus_one = input_ptr + 1'b1;
                end
            end
        end
    endfunction

    function automatic [ROB_PC_OFFSET_WIDTH-1:0] get_pc_offset;
        input [VADDR_WIDTH-1:0] pc_i;
        begin
            get_pc_offset = pc_i[1 +: ROB_PC_OFFSET_WIDTH];
        end
    endfunction : get_pc_offset
    
    function automatic [ROB_PC_BASE_WIDTH-1:0] get_pc_base;
        input [VADDR_WIDTH-1:0] pc_i;
        begin 
            get_pc_base = pc_i[(VADDR_WIDTH-1) -: ROB_PC_BASE_WIDTH];
        end
    endfunction : get_pc_base
    
    function automatic [0:0] rob_tag_is_older;
        input rob_tag_t tag1;
        input rob_tag_t tag2;
        logic id2_gt_id1;
        begin
            id2_gt_id1 = {tag1.index,tag1.block} < {tag2.index,tag2.block};
            rob_tag_is_older = ((tag1.flag ^ tag2.flag) ? ~id2_gt_id1 : id2_gt_id1);
        end
    endfunction : rob_tag_is_older
    

endpackage


