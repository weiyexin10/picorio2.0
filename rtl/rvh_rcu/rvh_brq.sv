// Branch reorder queue
module rvh_brq
    import rvh_pkg::*;
(
    // Rename -> BRV : Allocate
    input  logic [ RENAME_WIDTH-1:0]                    rn_brq_alloc_inst_vld_i,
    input  logic [ RENAME_WIDTH-1:0][ROB_TAG_WIDTH-1:0] rn_brq_alloc_inst_rob_tag_i,
    input  logic [ RENAME_WIDTH-1:0]                    rn_brq_alloc_inst_pred_taken_i,
    input  logic [ RENAME_WIDTH-1:0][  VADDR_WIDTH-1:0] rn_brq_alloc_inst_pred_target_i,
    output logic [ RENAME_WIDTH-1:0][BRQ_TAG_WIDTH-1:0] rn_brq_alloc_inst_brq_tag_o,
    output logic                                        rn_brq_alloc_inst_rdy_o,
    // IQ -> BRQ : Issue
    input  logic [    BRU_COUNT-1:0][BRQ_TAG_WIDTH-1:0] iq_brq_issue_brq_tag_i,
    output logic [    BRU_COUNT-1:0]                    iq_brq_issue_pred_taken_o,
    output logic [    BRU_COUNT-1:0][  VADDR_WIDTH-1:0] iq_brq_issue_pred_target_o,
    // Bru -> BRQ : Resolve
    input  logic [    BRU_COUNT-1:0]                    bru_brq_resolve_vld_i,
    input  logic [    BRU_COUNT-1:0]                    bru_brq_resolve_mispred_i,
    input  logic [    BRU_COUNT-1:0][BRQ_TAG_WIDTH-1:0] bru_brq_resolve_brq_tag_i,
    // Boardcast : Non-Resolved Branch Ptr
    output logic                                        brq_empty_o,
    output logic [ROB_TAG_WIDTH-1:0]                    oldest_br_rob_tag_o,

    input logic flush_i,

    input clk,
    input rst
);

    localparam int unsigned ENTRY_CNT_WIDTH = $clog2(BRQ_ENTRY_COUNT) + 1;

    typedef struct packed {
        logic resovled;
        logic [ROB_TAG_WIDTH-1:0] rob_tag;
        logic pred_taken;
        logic [VADDR_WIDTH-1:0] pred_target;
    } brq_entry_t;


    brq_entry_t [BRQ_ENTRY_COUNT-1:0] entry_dff;

    logic [BRQ_ENTRY_COUNT-1:0] entry_vld;
    logic [BRQ_ENTRY_COUNT-1:0] oldest_mask;
    logic [BRQ_ENTRY_COUNT-1:0][ROB_TAG_WIDTH-1:0] entry_rob_tag;

    logic [RENAME_WIDTH-1:0][BRQ_ENTRY_COUNT-1:0] avail_entry_mask;
    logic [ENTRY_CNT_WIDTH-1:0] avail_cnt;

    logic [BRQ_ENTRY_COUNT-1:0][BRQ_TAG_WIDTH-1:0] entry_tag;
    logic [RENAME_WIDTH-1:0][BRQ_ENTRY_COUNT-1:0] enq_entry_mask;
    logic [RENAME_WIDTH-1:0][BRQ_TAG_WIDTH-1:0] enq_entry_tag;

    logic [BRU_COUNT-1:0] deq_vld;


    always_comb begin
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            if (i == 0) begin
                avail_entry_mask[i] = ~entry_vld;
            end else begin
                avail_entry_mask[i] = avail_entry_mask[i-1] & ~enq_entry_mask[i-1];
            end
        end
    end

    generate
        for (genvar i = 0; i < BRU_COUNT; i++) begin
            assign iq_brq_issue_pred_taken_o[i] = entry_dff[iq_brq_issue_brq_tag_i[i]].pred_taken;
            assign iq_brq_issue_pred_target_o[i] = entry_dff[iq_brq_issue_brq_tag_i[i]].pred_target;
        end
    endgenerate

    generate
        for (genvar i = 0; i < BRQ_ENTRY_COUNT; i++) begin
            assign entry_rob_tag[i] = entry_dff[i].rob_tag;
            assign entry_tag[i] = i[BRQ_TAG_WIDTH-1:0];
        end
    endgenerate

    assign deq_vld = bru_brq_resolve_vld_i & ~bru_brq_resolve_mispred_i;

    assign rn_brq_alloc_inst_rdy_o = avail_cnt >= RENAME_WIDTH;
    assign rn_brq_alloc_inst_brq_tag_o = enq_entry_tag;

    assign brq_empty_o = ~(|entry_vld);


    always_ff @(posedge clk) begin : brq_entry_dff_update
        for (int i = 0; i < RENAME_WIDTH; i++) begin
            if (rn_brq_alloc_inst_vld_i[i]) begin
                entry_dff[enq_entry_tag[i]].resovled <= 1'b0;
                entry_dff[enq_entry_tag[i]].rob_tag <= rn_brq_alloc_inst_rob_tag_i[i];
                entry_dff[enq_entry_tag[i]].pred_taken <= rn_brq_alloc_inst_pred_taken_i[i];
                entry_dff[enq_entry_tag[i]].pred_target <= rn_brq_alloc_inst_pred_target_i[i];
            end
        end
    end

    age_order_selector #(
        .ENTRY_COUNT(BRQ_ENTRY_COUNT),
        .ENQ_WIDTH  (RENAME_WIDTH),
        .DEQ_WIDTH  (BRU_COUNT),
        .SEL_WIDTH  (1)
    ) u_age_order_selector (
        .enq_vld_i(rn_brq_alloc_inst_vld_i),
        .enq_tag_i(enq_entry_tag),
        .deq_vld_i(deq_vld),
        .deq_tag_i(bru_brq_resolve_brq_tag_i),
        .vld_mask_o(entry_vld),
        .sel_mask_i(entry_vld),
        .sel_oldest_mask_o(oldest_mask),
        .flush_i(flush_i),
        .clk(clk),
        .rst(rst)
    );

    onehot_mux #(
        .SOURCE_COUNT(BRQ_ENTRY_COUNT),
        .DATA_WIDTH  (ROB_TAG_WIDTH)
    ) u_onehot_mux (
        .sel_i (oldest_mask),
        .data_i(entry_rob_tag),
        .data_o(oldest_br_rob_tag_o)
    );

    one_counter #(
        .DATA_WIDTH(BRQ_ENTRY_COUNT)
    ) u_one_counter (
        .data_i(avail_entry_mask[0]),
        .cnt_o (avail_cnt)
    );

    generate
        for (genvar i = 0; i < RENAME_WIDTH; i++) begin
            one_hot_priority_encoder #(
                .SEL_WIDTH(BRQ_ENTRY_COUNT)
            ) u_alloc_mask_one_hot_priority_encoder (
                .sel_i(avail_entry_mask[i]),
                .sel_o(enq_entry_mask[i])
            );
            onehot_mux #(
                .SOURCE_COUNT(BRQ_ENTRY_COUNT),
                .DATA_WIDTH  (BRQ_TAG_WIDTH)
            ) u_alloc_tag_onehot_mux (
                .sel_i (enq_entry_mask[i]),
                .data_i(entry_tag),
                .data_o(enq_entry_tag[i])
            );
        end
    endgenerate


endmodule : rvh_brq
